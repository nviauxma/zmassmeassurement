// test reading several files asynchronously 

#include "NtupleAnalysisUtils/NTAUTopLevelIncludes.h"
#include "TStopwatch.h"

/// Test program to check if we can correctly read and fill from vector branches

// ntuple class for our test
class TestTree : public NtupleBranchMgr{
public:
    TestTree (TTree* t):
        NtupleBranchMgr(t),
        mass("mass",t,this),
        cutVar1("cutVar1",t,this),
        vecVar1("vecVar1",t,this){

    }
    NtupleBranch<double> mass;
    NtupleBranch<int> cutVar1; 
    NtupleBranch<std::vector<int>> vecVar1; 

};

// method to fill some random dummy data
void writeTestTree(const std::string fname, double mean, double sigma, Long64_t nevt, int seed){

    TFile* myFile = new TFile(fname.c_str(),"READ"); 
    if (myFile){
        TTree* found = nullptr;
        myFile->GetObject("TestTree", found);
        if (found && found->GetEntries() == nevt){
            std::cout <<" It seems input already exists - not rerunning it"<<std::endl;
            myFile->Close();
            return;
        }
        myFile->Close();
    }
    else {
        std::cout <<" Test input file does not yet exist, will prepare one"<<std::endl;
    }
    myFile = new TFile(fname.c_str(),"RECREATE"); 
    TTree* myTree = new TTree("TestTree","TestTree");
    myTree->SetDirectory(myFile);

    // note: no need for branch() etc calls, all done for us 
    TestTree tree(myTree); 

    // prepare to populate the tree with random values
    TRandom3 m_rdm;
    m_rdm.SetSeed(seed); // fix seed to get reproducible results
    std::vector<int> vec;
    for (Long64_t entry = 0; entry < nevt; ++entry){
        // almost as before, we just talk to the tree NtupleBranches
        tree.mass.set(m_rdm.Gaus(mean, sigma));
        tree.cutVar1.set(m_rdm.Uniform(-1,4));
        size_t rsize = m_rdm.Uniform(7);
        tree.vecVar1.outVec().clear();
        tree.vecVar1.outVec().reserve(rsize);
        for (size_t k = 0; k < rsize; ++k){
            tree.vecVar1.outVec().push_back(m_rdm.Uniform(-10,40));
        }
        tree.fill();  // here we could just as well call myTree->Fill(); 
    }
    // and write to disk
    myFile->Write();
    myFile->Close();
}

int main (int, char**){

    // write dummy data...
    // prepare a sample 
    std::cout << " =========== Generating test input (may take a while) ===========" <<std::endl;
    Sample<TestTree> bigSample;
    std::string fname {"Input.VecBranch.root"};
    writeTestTree(fname,10 ,5,10000000,0);
    Sample<TestTree> sample{fname,"TestTree"};

    std::cout << " =========== Preparing analysis ===========" <<std::endl;
    
    // define a simple selection
    Selection<TestTree> sel ([](TestTree &t){
        return t.cutVar1() >=0; 
    });

    // define a plot fill instruction
    PlotFillInstruction<TH1D, TestTree> fillFromVec(
                 [](TH1D* h, TestTree &t){
            std::string printStatement {"Vec var has size "}; 
            printStatement+=std::to_string(t.vecVar1().size()); 
            printStatement+=" with content {"; 
            for (size_t p = 0; p < t.vecVar1.size(); ++p){
               h->Fill(t.vecVar1(p));
               printStatement+=std::to_string(t.vecVar1(p))+"  "; 
            }
            printStatement+="}"; 
            std::cout <<printStatement<<std::endl;
     });

    // define histogram binnings
    TH1D vecplot ("vecPlot","dummy mass",50,-10,40);


    // set the histo filler to single thread operation 
    HistoFiller::getDefaultFiller()->setFillMode(HistoFillMode::singleThread);

    std::vector<Plot<TH1D>> plots_single; 
    plots_single.push_back(Plot<TH1D> {RunHistoFiller(&vecplot, sample, sel, fillFromVec),PlotFormat()});

    // now time the first event loop, on one thread
    std::cout << " =========== Filling on 1 thread ===========" <<std::endl;
    plots_single.front().populate();


    return 0;

}
