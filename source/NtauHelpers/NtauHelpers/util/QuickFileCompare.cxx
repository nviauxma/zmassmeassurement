#include <NtauHelpers/HistoUtils.h>
#include <NtauHelpers/Utils.h>
int main(int argc, char* argv[]) {
    std::string ref_file{""}, test_file{""}, ref_title{"Reference"}, test_title{"Test"}, out_dir{"QuickComparePlots/"};
    bool ref_check{false}, skip_matching{false};
    for (int k = 1; k < argc; ++k) {
        std::string current_arg(argv[k]);
        /// Pipe the name of the first input file or the reference file
        if ((current_arg == "--firstFile" || current_arg == "--refFile") && k + 1 < argc) {
            ref_file = argv[k + 1];
            ++k;
            /// Pipe the name of the second file or the test file
        } else if ((current_arg == "--secondFile" || current_arg == "--testFile") && k + 1 < argc) {
            test_file = argv[k + 1];
            ++k;
        }
        /// Title to be displayed in the legend for the reference file
        else if ((current_arg == "--firstTitle" || current_arg == "--refTitle") && k + 1 < argc) {
            ref_title = argv[k + 1];
            ++k;
        }
        /// Title to be displayed in the legend for the test file
        else if ((current_arg == "--secondTitle" || current_arg == "--testTitle") && k + 1 < argc) {
            test_title = argv[k + 1];
            ++k;
        }
        /// Change directory to pipe the output files to
        else if (current_arg == "--outDir" && k + 1 < argc) {
            out_dir = argv[k + 1];
            ++k;
        }
        /// If this flag is set to true. Both files should return
        /// identical results. Otherwise the code returns a failure as EXIT code
        else if (current_arg == "--refCheck") {
            ref_check = true;
        } else if (current_arg == "--skipMatching") {
            skip_matching = true;
        } else {
            std::cerr << "Unknown argument " << current_arg << std::endl;
            std::cout << "Usage of QuickFileCompare " << std::endl;
            std::cout << " --firstFile / --refFile <path>       : Path to the first ROOT file for comparison " << std::endl;
            std::cout << " --secondFile / --testFile <path>     : Path of the second ROOT file for comparison " << std::endl;
            std::cout << " --firstTitle / --refTitle <string>   : Specify string to be drawn on the legend for the reference file (By "
                         "default file name is taken) "
                      << std::endl;
            std::cout << " --secondTitle / --testTitle <string> : Specify string to be drawn on the legend for the test file (By default "
                         "file name is taken) "
                      << std::endl;
            std::cout << " --outDir <path>                      : Set the output directory for all plots " << std::endl;
            std::cout << " --refCheck                           : It's checked whether the histogram contents between both file match. "
                         "Otherwise the programme ends with EXIT_FAILURE "
                      << std::endl;
            std::cout << " --skipMatching                       : Skip plots which completely agree " << std::endl;
            return EXIT_FAILURE;
        }
    }

    std::shared_ptr<TFile> ref_data_file = PlotUtils::Open(ref_file);
    std::shared_ptr<TFile> test_data_file = PlotUtils::Open(test_file);
    if (!ref_data_file || !test_data_file) {
        std::cerr << " Failed to open one of the input files " << std::endl;
        return EXIT_FAILURE;
    }
    const std::vector<std::string> content_ref = PlotUtils::getAllObjectsFromDir<TH1>(ref_data_file.get());
    const std::vector<std::string> content_test = PlotUtils::getAllObjectsFromDir<TH1>(test_data_file.get());

    if (content_ref.empty() || content_test.empty()) {
        std::cerr << "It seems that one of the two files is corrupted. Please check" << std::endl;
        return EXIT_FAILURE;
    }

    /// Check that the content of the two files is actually the same
    std::vector<std::string> not_common;
    std::copy_if(content_ref.begin(), content_ref.end(), not_common.end(),
                 [&content_test](const std::string& test_str) { return !isElementInList(content_test, test_str); });
    std::copy_if(content_test.begin(), content_test.end(), not_common.end(),
                 [&content_ref](const std::string& test_str) { return !isElementInList(content_ref, test_str); });
    if (not_common.empty()) {
        std::cout << "The files " << ref_file << " and " << test_file << " coincide in terms of TH1 content. " << std::endl;
    } else {
        std::cerr << "The following histograms are only stored in one of the two files " << std::endl;
        for (const std::string& not_present : not_common) {
            std::cerr << "   -- " << not_present << std::endl;
            return EXIT_FAILURE;
        }
    }

    SetAtlasStyle();

    CanvasOptions canvas_opts = CanvasOptions()
                                    .RatioAxis(AxisConfig().Title("Ratio to " + ref_title).ExtraTitleOffset(0.4).Min(0.9).Max(1.005))
                                    .YAxis(AxisConfig().Title("Entries").ExtraTitleOffset(0.4).TopPadding(0.7))
                                    // .XAxis(AxisConfig().ExtraTitleOffset(1.3))
                                    .LabelStatusTag("Internal")
                                    .OutputDir(out_dir);

    auto multi_page = PlotUtils::startMultiPagePdfFile("All quick plots", canvas_opts);

    std::vector<PlotContent<TH1>> all_plots;
    int exit_code = EXIT_SUCCESS;
    for (const std::string& variable : content_ref) {
        std::shared_ptr<TH1> h_reference = load_histo(variable, ref_data_file);
        std::shared_ptr<TH1> h_test = load_histo(variable, test_data_file);
        if (!h_reference || !h_test) {
            std::cerr << "ERROR - What's going on here? The histogram " << variable << " should actually be defined in both cases"
                      << std::endl;
            exit_code = EXIT_FAILURE;
            continue;
        }
        if (h_reference->GetDimension() != h_test->GetDimension()) {
            std::cout << "WARNING - The dimension of the opononymous histograms " << variable << " do not coincide" << std::endl;
            if (ref_check) exit_code = EXIT_FAILURE;
            continue;
        }
        if (!same_binning(h_reference, h_test)) {
            std::cout << "WARNING: Actually the binning of the variable " << variable << " has changed" << std::endl;
            if (ref_check) exit_code = EXIT_FAILURE;
            continue;
        }
        if (ref_check || skip_matching) {
            std::shared_ptr<TH1> diff_clone = clone(h_reference);
            diff_clone->Add(h_test.get(), -1);
            int max_bin = GetMaximumBin(diff_clone);
            int min_bin = GetMinimumBin(diff_clone);
            if (std::abs(diff_clone->GetBinContent(max_bin)) > DBL_EPSILON || std::abs(diff_clone->GetBinContent(min_bin)) > DBL_EPSILON) {
                if (ref_check) {
                    std::cerr << "ERROR - The variable " << variable << " differs at a maximum of "
                              << std::max(std::abs(diff_clone->GetBinContent(max_bin)), std::abs(diff_clone->GetBinContent(min_bin)))
                              << " please check" << std::endl;
                    exit_code = EXIT_FAILURE;
                }
            } else if (skip_matching)
                continue;
        }

        if (h_reference->GetDimension() != 1) {
            std::cout << "INFO: Skip to plot the variable " << variable << " as it has dimension " << h_reference->GetDimension()
                      << std::endl;
            continue;
        }

        all_plots +=
            PlotContent<TH1>{{Plot<TH1>(CopyExisting(*h_reference),
                                        PlotFormat().LegendTitle(ref_title).LegendOption("PL").MarkerStyle(kFullTriangleUp).Color(kBlack)),
                              Plot<TH1>(CopyExisting(*h_test),
                                        PlotFormat().LegendTitle(test_title).LegendOption("PL").MarkerStyle(kFullTriangleDown).Color(kRed))

                             },
                             {RatioEntry(1, 0)},
                             {"Quick plot dump", "reference:  " + ref_file, "test: " + test_file, "variable: " + variable

                             },
                             variable,
                             multi_page,
                             canvas_opts};
    }

    for (auto& pc : all_plots) {
        if (count_non_empty(pc)) DefaultPlotting::draw1D(pc);
    }

    return exit_code;
}