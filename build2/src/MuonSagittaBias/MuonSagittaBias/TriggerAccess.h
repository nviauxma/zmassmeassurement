#ifndef MUONSAGITTABIAS__TriggerAccess__H
#define MUONSAGITTABIAS__TriggerAccess__H

#include "MuonSagittaBias/SagittaBiasTree.h"
#include "NtauHelpers/HistoBinConfig.h"

/// Helper class to simply access
/// the relevant trigger information from a SagittaBiasTree

class TriggerAccess {
public:
    /// Helper method to create the comprehensive trigger list from
    /// a SagittaBiasTree instance
    static std::vector<TriggerAccess> make_full_trig_list(SagittaBiasTree& t);
    /// Helper method to generate the histogram template

    static PlotUtils::HistoBinConfig extract_binning(TTree* tree);

    ///     Constructor
    ///         trigger_item: Name of the considered trigger item as it is used
    ///                      in the online analysis. i.e. HLT_mu24..
    ///         SagittaBiasTree&: Instance of the SagittaBiasTree to be used for the access
    TriggerAccess(const std::string& trigger_item, SagittaBiasTree& t);
    TriggerAccess(const TriggerAccess&) = default;
    TriggerAccess(TriggerAccess&&) = default;

    ///     Returns the trigger item given in the constructor
    std::string trigger() const;
    ///  Returns whether the trigger passed the decision
    bool pass_decision();
    ///  Returns whether the trigger could be matched to one of the legs
    ///     in terms of dR
    bool pass_matching_dR();

    /// Returns whether the positive lepton is matched
    bool pos_lep_matched_dR();
    /// Returns whether the negative lepton is matched
    bool neg_lep_matched_dR();

    /// Returns the prescale of the trigger
    float prescale();

private:
    std::string m_item{};
    NtupleBranch<bool>& m_decision;
    NtupleBranch<bool>& m_negLep_match;
    NtupleBranch<bool>& m_posLep_match;
    NtupleBranch<float>* m_prescale{nullptr};
};

///   Helper class to evaluate whether the event passes the
///   trigger for a given run number interval
class TrigSelectionBranch : public DerivedVirtualBranch<bool, SagittaBiasTree> {
public:
    /// Constructor for the single-lepton trigger
    ///  tree: Bias tree to read off the trigger
    ///  trig_name: Name of the corresponding trigger
    ///  pt_threshold: Offline threshold to be applied on top of the dR matching requirement

    TrigSelectionBranch(SagittaBiasTree* tree, const std::string& trig_name, float pt_threshold);

    TrigSelectionBranch(SagittaBiasTree* tree, const std::string& trig_name, const float low_pt_threshold, const float high_pt_threshold);

    /// Return true whether the event is accepted by the trigger decision or not
    bool is_accepted(SagittaBiasTree& tree);

    /// Name of the trigger
    std::string trigger() const;

    /// Returns true if the trigger is a single lepton trigger
    bool is_single_lepton() const;

    /// pt threshold to be applied to the lower leg
    float low_threshold() const;
    /// pt threshold to be applied to the leg with higher momentum
    float high_threshold() const;

    /// Unique identifier to perform an easier matching with the
    /// prescaling stream
    const ObjectID& id() const;
    /// Returns the TriggerAccess
    TriggerAccess& access();

private:
    /// Access manager to read off the per event decision from the tree
    TriggerAccess m_access;
    /// Minimum pt threshold on the lower trigger leg
    const float m_low_pt{0.};
    /// Maximum pt trheshold on the higher trigger
    const float m_high_pt{0.};
    /// Flag which decides whether the trigger is a single lepton trigger or not
    bool m_is_single_lepton{false};

    ObjectID m_obj_id{trigger()};
};

#endif