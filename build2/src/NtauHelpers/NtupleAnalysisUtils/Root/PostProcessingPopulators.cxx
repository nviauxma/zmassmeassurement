#include "NtupleAnalysisUtils/Plot/PostProcessingPopulators.h"

ExtractEfficiency::ExtractEfficiency( const IPopulatorSource<TEfficiency> & inputEff):
    m_inputEff(inputEff.clonePopulator()){
}
std::shared_ptr<TH1D> ExtractEfficiency::populate(){

    auto eff = m_inputEff->populate();
    if (!eff){
        std::cerr << "ExtractEfficiency can not be populated since input is a null TEfficiency!"<<std::endl;
        return nullptr; 
    }
    return PlotUtils::extractEfficiency(eff.get()); 

}
std::shared_ptr<IPlotPopulator<TH1D>> ExtractEfficiency::clonePopulator() const {
    return std::make_shared<ExtractEfficiency>(*m_inputEff);
}



ExtractEfficiency2D::ExtractEfficiency2D( const IPopulatorSource<TEfficiency> & inputEff):
    m_inputEff(inputEff.clonePopulator()){
}
std::shared_ptr<TH2D> ExtractEfficiency2D::populate(){

    auto eff = m_inputEff->populate();
    if (!eff){
        std::cerr << "ExtractEfficiency2D can not be populated since input is a null TEfficiency!"<<std::endl;
        return nullptr; 
    }
    // the TEfficiency members are TH2D, in spite of the return type being a TH1*, so we can simply dynamic_cast here 
    std::shared_ptr<TH2D> effHisto{dynamic_cast<TH2D*>(eff->GetTotalHistogram()->Clone())};

    // now we populate the effi histo 
    effHisto->Reset();
    for (int k = 1; k < effHisto->GetNbinsX()+1; ++k){
        for (int l = 1; l < effHisto->GetNbinsY()+1; ++l){
            int globalBin = eff->GetGlobalBin(k,l); 
            effHisto->SetBinContent(k,l,eff->GetEfficiency(globalBin)); 
            effHisto->SetBinError(k,l,0.5 * (eff->GetEfficiencyErrorUp(globalBin) + eff->GetEfficiencyErrorLow(globalBin))); 
        }
    }
    return effHisto;
}
std::shared_ptr<IPlotPopulator<TH2D>> ExtractEfficiency2D::clonePopulator() const {
    return std::make_shared<ExtractEfficiency2D>(*m_inputEff);
}

