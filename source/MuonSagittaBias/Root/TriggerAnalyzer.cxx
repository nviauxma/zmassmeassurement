// Please add your package name here
#include <FourMomUtils/xAODP4Helpers.h>
#include <Math/GenVector/VectorUtil.h>
#include <MuonSagittaBias/TriggerAnalyzer.h>
#include <MuonSagittaBias/Utils.h>
#include <xAODMuon/Muon.h>
namespace {
    constexpr float MeVtoGeV = 1.e-3;
    constexpr float muon_mass = 105.6 * 1.e-3;

}  // namespace

TriggerAnalyzer::TriggerAnalyzer(TTree* t) :
    SagittaBiasTree(t),
    only_one_trig_fired(
        [](TriggerAnalyzer& t) -> bool {
            return std::count_if(t.triggers.begin(), t.triggers.end(), [](TriggerAccess& acc) { return acc.pass_decision(); }) <= 1;
        },
        this)

{
    m_has_rnd_run_number = is_mc() && randomRunNumber.existsInTree();
}
TriggerAnalyzer::~TriggerAnalyzer() {}
bool TriggerAnalyzer::is_mc() const { return gen_weight.is_mc(); }
bool TriggerAnalyzer::has_random_run_number() const { return m_has_rnd_run_number; }
float TriggerAnalyzer::event_weight() { return gen_weight(); }

float TriggerAnalyzer::pt(bool pos_lep) { return (pos_lep ? posLep_pt() : negLep_pt()) * MeVtoGeV; }
float TriggerAnalyzer::eta(bool pos_lep) { return (pos_lep ? posLep_eta() : negLep_eta()); }
float TriggerAnalyzer::phi(bool pos_lep) { return (pos_lep ? posLep_phi() : negLep_phi()); }
int TriggerAnalyzer::q(bool pos_lep) { return (pos_lep ? posLep_q() : negLep_q()); }
float TriggerAnalyzer::invariant_m() { return diLep_m() * MeVtoGeV; }
float TriggerAnalyzer::d0_sig(bool pos_lep) { return pos_lep ? posLep_d0Err() : negLep_d0Err(); }
float TriggerAnalyzer::d0(bool pos_lep) { return pos_lep ? posLep_d0() : negLep_d0(); }
float TriggerAnalyzer::z0(bool pos_lep) { return pos_lep ? posLep_z0() : negLep_z0(); }

bool TriggerAnalyzer::is_valid() {
    const float dilep_mass = invariant_m();
    if (dilep_mass < 0) return false;

    const float pos_eta = std::abs(eta(LepCharge::Plus));
    const float neg_eta = std::abs(eta(LepCharge::Minus));
    /// Exclude any lepton within the region where the muon spectrometer is partially instrumented
    if (std::min(pos_eta, neg_eta) < 0.1) return false;
    /// Exclude events where one of the candidates is outside the ID acceptance
    if (std::max(pos_eta, neg_eta) > 2.5) return false;

    /// Apply cuts ID quality cuts for the ID and primary track
    if (!negLepIDTrk_passIdCuts() || !posLepIDTrk_passIdCuts()) return false;

    return true;
}

int TriggerAnalyzer::run_number() { return has_random_run_number() ? randomRunNumber() : runNumber(); }
int TriggerAnalyzer::period_year() {
    const int run_num = run_number();
    /// Numbers taken from PyAmi via the PeriodRunConverter script
    /// python NtauHelpers/ClusterSubmisison/python/PeriodRunConverter.py
    constexpr int first_run_16 = 296939;
    constexpr int last_run_16D3 = 302919;
    /// constexpr int first_run_16D4 = 302956;

    constexpr int first_run_17 = 324320;
    constexpr int first_run_18 = 348197;
    if (is_mc() && !has_random_run_number()) {
        /// Simulation campaign representing year 15-16
        constexpr int run_number_mc16a = 284500;
        /// Simulation campaign representing year 17
        constexpr int run_number_mc16d = 300000;
        /// Simulation campaign representing year 18
        constexpr int run_number_mc16e = 310000;

        switch (run_num) {
            case run_number_mc16a: return data_period::data16;
            case run_number_mc16d: return data_period::data17;
            case run_number_mc16e: return data_period::data18;
        }
    } else {
        if (run_num < first_run_16)
            return data_period::data15;
        else if (run_num <= last_run_16D3)
            return data_period::data16_AD3;
        else if (run_num < first_run_17)
            return data_period::data16_D4I4;
        else if (run_num < first_run_18)
            return data_period::data17;
    }
    return data_period::data18;
}
std::string TriggerAnalyzer::period_to_string(int period) {
    if (period & data_period::data15) return "data15";
    if (period & data_period::data16) return "data16";
    if (period & data_period::data17) return "data17";
    if (period & data_period::data18) return "data18";

    return "UNKNOWN";
}