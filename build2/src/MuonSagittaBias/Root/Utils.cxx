#include <MuonSagittaBias/DataTemplateHolder.h>
#include <MuonSagittaBias/SagittaBiasAnalyzer.h>
#include <MuonSagittaBias/Utils.h>
#include <TF1.h>
#include <TFitResult.h>
namespace {
    constexpr unsigned int max_fit_iterations = 50;
    constexpr double max_chi2_cut = 20.;
}  // namespace
namespace MuonTrk {

    int calib_constants::to_trk_type(const std::string& str) { return to_type(str); }

    bool calib_constants::has_bias() const { return sagitta_bias != 0. || radial_bias != 0. || magnetic_bias != 0.; }
    bool calib_constants::has_resolution() const { return res_const != 0. || res_lin != 0. || res_quad != 0. || res_hyp != 0.; }
    calib_constants::calib_constants(int _trk_type) : trk_type{_trk_type} {}
    calib_constants::calib_constants(const std::string& calib_str) {
        /// Preliminary fix to work with the first produced file
        std::vector<std::string> tokens = tokenize(calib_str, "_");
        if (tokens.empty()) return;
        extract_value(tokens, "SAGBIAS", sagitta_bias);
        extract_value(tokens, "RADBIAS", radial_bias);
        extract_value(tokens, "MAGBIAS", magnetic_bias);
        sagitta_bias *= 1.e-3;
        magnetic_bias *= 1.e-3;
        radial_bias *= 1.e-3;
        extract_value(tokens, "SIGMA", res_const);
        extract_value(tokens, "SIGMALIN", res_lin);
        extract_value(tokens, "SIGMAQUAD", res_quad);
        extract_value(tokens, "SIGMAHYP", res_hyp);

        trk_type = to_type(tokens[0]);
    }
    void calib_constants::extract_value(const std::vector<std::string>& tokens, const std::string& key, float& put_val_to) const {
        std::vector<std::string>::const_iterator itr = std::find(tokens.begin(), tokens.end(), key);
        if (itr == tokens.end()) return;
        ++itr;
        if (itr == tokens.end()) return;
        const std::string& vec_ele = (*itr);
        put_val_to = std::atof(vec_ele.c_str());
    }
    bool calib_constants::operator!=(const calib_constants& other) const { return !(*this == other); }
    bool calib_constants::operator==(const calib_constants& other) const {
        return other.sagitta_bias == sagitta_bias && other.magnetic_bias == magnetic_bias && other.radial_bias == radial_bias &&
               other.res_const == res_const && other.res_lin == res_lin && other.res_quad == res_quad && other.res_hyp == res_hyp &&
               (other.trk_type & TrackType::OrdinaryTrks) == (trk_type & TrackType::OrdinaryTrks);
    }
    bool calib_constants::operator<(const calib_constants& b) const {
        const calib_constants& a = (*this);
        if (a.trk_type != b.trk_type) return a.trk_type < b.trk_type;
        if (a.sagitta_bias != b.sagitta_bias) return a.sagitta_bias < b.sagitta_bias;
        if (a.magnetic_bias != b.magnetic_bias) return a.magnetic_bias < b.magnetic_bias;
        if (a.radial_bias != b.radial_bias) return a.radial_bias < b.radial_bias;
        return a.res_const < b.res_const;
    }
    std::string calib_constants::to_string() const {
        return MuonTrk::to_string(trk_type & MuonTrk::TrackType::OrdinaryTrks) + "_SAGBIAS_" + std::to_string(sagitta_bias * 1.e3) +
               "_RADBIAS_" + std::to_string(radial_bias * 1.e3) + "_MAGBIAS_" + std::to_string(magnetic_bias * 1.e3) + "_SIGMA_" +
               std::to_string(res_const) + "_SIGMALIN_" + std::to_string(res_lin) + "_SIGMAQUAD_" + std::to_string(res_quad) +
               "_SIGMAHYP_" + std::to_string(res_hyp);
    }
    std::string calib_constants::to_title() const {
        if (res_const == 0.) {
            return Form("%s (#deltas=%.3f [TeV^{-1}])", MuonTrk::to_title(trk_type & TrackType::OrdinaryTrks).c_str(), sagitta_bias * 1.e3);
        }
        return Form("%s (#delta=%.3f [TeV^{-1}], #sigma=%.2f)", MuonTrk::to_title(trk_type & TrackType::OrdinaryTrks).c_str(),
                    sagitta_bias * 1.e3, res_const);
    }

    ///###################################
    ///     TrackType string helpers
    ///###################################
    std::string to_string(int trk_type) {
        if (trk_type == TrackType::TruthTrk) return "TRU";
        if (trk_type == TrackType::Primary) return "PR";
        if (trk_type == TrackType::IDTrk) return "ID";
        if (trk_type == TrackType::MSTrk) return "MS";
        if (trk_type == TrackType::METrk) return "ME";
        if (trk_type == TrackType::MSOETrk) return "MSOE";
        if (trk_type == TrackType::CmbTrk) return "CB";
        int injected = SagittaBiasAnalyzer::inject_from_type(trk_type);

        if (injected != -1) {
            const calib_constants& c = SagittaBiasAnalyzer::sagitta_injections()[injected];
            return c.to_string();
        }
        return "UNKNOWN";
    }
    int to_type(const std::string& trk_type) {
        if (trk_type.find("TRU") == 0) return TrackType::TruthTrk;
        if (trk_type.find("PR") == 0) return TrackType::Primary;
        if (trk_type.find("ID") == 0) return TrackType::IDTrk;
        if (trk_type.find("MSOE") == 0) return TrackType::MSOETrk;
        if (trk_type.find("MS") == 0) return TrackType::MSTrk;
        if (trk_type.find("ME") == 0) return TrackType::METrk;
        if (trk_type.find("CB") == 0) return TrackType::CmbTrk;
        return 0;
    }
    std::string to_title(int trk_type) {
        if (trk_type == TrackType::Primary) return "#mu-track";
        if (trk_type == TrackType::IDTrk) return "ID-track";
        if (trk_type == TrackType::MSTrk) return "MS-track";
        if (trk_type == TrackType::METrk) return "ME-track";
        if (trk_type == TrackType::MSOETrk) return "MSOE-track";
        if (trk_type == TrackType::CmbTrk) return "Combined-track";
        if (trk_type == TrackType::TruthTrk) return "Gen. #mu";
        int injected = SagittaBiasAnalyzer::inject_from_type(trk_type);
        if (injected != -1) {
            const SagittaBiasAnalyzer::calib_constants& c = SagittaBiasAnalyzer::sagitta_injections()[injected];
            return c.to_title();
        }
        return "Unknown";
    }
}  // namespace MuonTrk

std::shared_ptr<TGraph> extract_chi2(const std::vector<std::shared_ptr<DataTemplateHolder>>& mc_templates,
                                     const DataTemplateHolder& data_template) {
    return data_template.extract_chi2(mc_templates);
}

FitResult fit_parabola(const std::shared_ptr<TGraph>& chi2graph) {
    FitResult res;
    if (chi2graph->GetN() == 0) return res;
    const double x_min = chi2graph->GetX()[0];
    const double x_max = chi2graph->GetX()[chi2graph->GetN() - 1];
    ///
    const unsigned int nDoF = chi2graph->GetN() - 3;

    /// Find out minimum and maximum
    double y_min{DBL_MAX}, y_max{-DBL_MAX}, x_at_min{0.};

    /// Determine first the minimum as ideally should only one exist
    for (int i = 0; i < chi2graph->GetN(); ++i) {
        const double x = chi2graph->GetX()[i];
        const double y = chi2graph->GetY()[i];
        y_max = std::max(y_max, y);
        if (y_min > y) {
            y_min = y;
            x_at_min = x;
        }
    }
    const double dx_low = std::abs(x_min - x_at_min);
    const double dx_high = std::abs(x_max - x_at_min);
    const double dx = std::max(dx_high, dx_low);
    const double approx_a = y_max / dx;

    std::unique_ptr<TF1> parfit =
        std::make_unique<TF1>(PlotUtils::RandomString(25).c_str(), "[0]+ [1]*(x - [2])**2", x_min, x_max);  //  TF1::EAddToList::kNo);

    // parfit->SetParameter(0, y_min);
    // parfit->SetParameter(1, approx_a);

    parfit->SetParLimits(2, x_min, x_max);
    parfit->SetParameter(2, x_at_min);

    /// parametrization c + a^{2} (x-b)^{2}
    /// where b represents the minimum value
    /// We can constrain the parameter by
    ///  y_max = a^{2}(x - x_min)^{2} -->
    /// sqrt(y_max) / (x_max-min) = a is already a good approximator
    // parfit->SetParLimits(0, 0., y_min * 2.);
    // parfit->SetParLimits(1, 0., approx_a *2.);

    std::cout << parfit->GetExpFormula() << "  Before fit: [2]: " << parfit->GetParameter(2) << " [1]: " << parfit->GetParameter(1)
              << "[0]:  " << parfit->GetParameter(0) << "\t";

    TFitResultPtr fit_res = chi2graph->Fit(parfit.get(), "WQS", "");
    double prev_chi2 = (fit_res->Chi2() / nDoF);
    unsigned int iter = 0;
    while (iter < max_fit_iterations) {
        fit_res = chi2graph->Fit(parfit.get(), "WQS", "");
        double curr_chi2 = fit_res->Chi2() / nDoF;
        if (prev_chi2 < 1. && curr_chi2 < 1. && prev_chi2 >= curr_chi2) break;
        prev_chi2 = curr_chi2;
        ++iter;
    }
    res.offset = parfit->GetParameter(0);
    res.slope = parfit->GetParameter(1);
    res.x_shift = parfit->GetParameter(2);
    res.succeed = IsFinite(res.offset) && IsFinite(res.slope);
    /// Now calculate the 1 sigma interval
    ///    [1](x- [2])^{2}-1 =0
    ///    [1]x^{2} -2*[1]*[2]x + [2]*[2]-1 = 0
    res.one_sigma = 1. / std::sqrt(res.slope);
    const double deltaChi2_plus = res.x_shift + res.one_sigma;
    const double deltaChi2_minus = res.x_shift - res.one_sigma;
    res.chi2 = fit_res->Chi2();
    res.nDoF = nDoF;
    res.num_iter = iter;
    res.fit_graph = std::make_shared<TGraph>();
    res.fit_graph->GetXaxis()->SetTitle(chi2graph->GetXaxis()->GetTitle());
    res.fit_graph->GetYaxis()->SetTitle(chi2graph->GetYaxis()->GetTitle());

    for (int i = 0; i < chi2graph->GetN(); ++i) {
        const double x = chi2graph->GetX()[i];
        res.fit_graph->SetPoint(res.fit_graph->GetN(), x, parfit->Eval(x));
    }
    std::cout << "     After fit ---  [2]: " << res.x_shift << " [1]: " << res.slope << "  [0]: " << res.offset << " chi2: " << res.chi2
              << "  \t ";
    std::cout << "Confidence interval " << deltaChi2_minus << " -- " << deltaChi2_plus;
    std::cout << std::endl;

    return res;
}

void set_2D_color_palette() {
    static std::array<Int_t, 20000> colors{};
    static Bool_t initialized = kFALSE;

    if (!initialized) {
        std::array<Double_t, 7> Red{1.00, 0.85, 0.6, 0.9, 0.8, 0.5, 0.2}, Green{1.00, 0.85, 0.2, 0.3, 0.7, 0.7, 0.8},
            Blue{1.00, 1.00, 0.8, 0.3, 0.2, 0.2, 0.1}, Length{0.00, 0.05, 0.2, 0.4, 0.6, 0.8, 1.0};

        Int_t FI = TColor::CreateGradientColorTable(Red.size(), Length.data(), Red.data(), Green.data(), Blue.data(), colors.size());
        for (unsigned int i = 0; i < colors.size(); ++i) colors[i] = FI + i;
        initialized = kTRUE;
    }
    gStyle->SetPalette(colors.size(), colors.data());
}

void draw_2D_plot(PlotContent<TH1>& pc) {
    set_2D_color_palette();
    pc.populateAll();
    CanvasOptions opt = pc.getCanvasOptions();
    for (Plot<TH1>& plt : pc.getPlots()) {
        std::shared_ptr<TH1> h = plt.getSharedHisto();
        /// Skip everything which is empty
        if (PlotUtils::isOverflowBin(h.get(), GetMaximumBin(h.get())) && PlotUtils::isOverflowBin(h.get(), GetMinimumBin(h.get())))
            continue;

        std::shared_ptr<TCanvas> can = std::make_shared<TCanvas>(PlotUtils::RandomString(50).c_str(), "can", 800, 600);

        can->SetTopMargin(0.15);
        can->SetRightMargin(0.18);
        can->SetLeftMargin(0.12);
        can->cd();

        h->SetMinimum(0);
        h->GetYaxis()->SetTitleOffset(0.9 * h->GetYaxis()->GetTitleOffset());
        h->SetContour(20000);
        h->Draw("colz");

        std::vector<TLatex*> labels;
        if (opt.DoAtlasLabel()) { labels += PlotUtils::drawAtlas(can->GetLeftMargin(), 0.92, opt.LabelLumiTag()); }
        labels += PlotUtils::drawLumiSqrtS(can->GetLeftMargin(), 0.87, opt.LabelLumiTag(), opt.LabelSqrtsTag());
        double y = 0.92;
        double dy = opt.OtherLabelStepY();
        for (auto& lbl : pc.getLabels()) {
            labels += PlotUtils::drawTLatex(0.5, y, lbl);
            y -= dy;
        }
        PlotUtils::saveCanvas(can, pc.getFileName() + "_" + plt.plotFormat().LegendTitle(), opt);
        if (pc.getMultiPagePdfHandle()) pc.getMultiPagePdfHandle()->saveCanvas(can);
    }
}