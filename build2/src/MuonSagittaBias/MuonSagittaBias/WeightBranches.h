#ifndef WEIGHT_BRANCHES_H
#define WEIGHT_BRANCHES_H
#include <MuonSagittaBias/SagittaBiasTree.h>
#include <NtauHelpers/MuonTPMetaData.h>

class TriggerPreScaleItem;
class TriggerAccess;
class SagittaBiasAnalyzer;

//// Branch to normalize the simulation to 1 inverse femto barn
class GenWeightBranch : public DerivedVirtualBranch<double, SagittaBiasTree> {
public:
    GenWeightBranch(SagittaBiasTree* tree);

    double get_weight(SagittaBiasTree& t);

    bool is_mc() const;

    static unsigned int lhe_variation();
    static void set_lhe_variation(unsigned int _var);

private:
    MuonTP::NormalizationDataBase* m_norm_db{nullptr};
    std::shared_ptr<MuonTP::MetaDataStore> m_current_weighter{nullptr};
    bool m_is_mc{false};
    NtupleBranch<double>* m_ev_weight{nullptr};
    static unsigned int s_lhe_variation;
};
/// Branch to reweight the simulation to the pileup distribution
class PileUpWeightBranch : public DerivedVirtualBranch<double, SagittaBiasTree> {
public:
    PileUpWeightBranch(SagittaBiasTree* tree);

    double get_weight(SagittaBiasTree& t);

    bool has_prw() const;

private:
    MuonTP::NormalizationDataBase* m_norm_db{nullptr};
    std::shared_ptr<MuonTP::MonteCarloPeriodHandler> m_current_period{nullptr};
    std::shared_ptr<MuonTP::MetaDataStore> m_run_weighter{nullptr};
    std::shared_ptr<MuonTP::MetaDataStore> m_all_weighter{nullptr};
    bool m_has_prw{false};
    double m_cached_reweight{1.};
};

class TrigPreScaleWeightBranch : public DerivedVirtualBranch<double, SagittaBiasAnalyzer> {
public:
    TrigPreScaleWeightBranch(SagittaBiasAnalyzer* tree);
    virtual ~TrigPreScaleWeightBranch();
    double get_weight(SagittaBiasAnalyzer& t);

    static bool set_prescale_strategy(int strat);

private:
    static int s_strategy;
    /// Flag whether the input is simulation or not
    std::vector<std::shared_ptr<const TriggerPreScaleItem>> m_calib_items;
};
#endif