{
 "metadata": {
  "orig_nbformat": 2,
  "kernelspec": {
   "name": "python3",
   "display_name": "Python 3",
   "language": "python"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2,
 "cells": [
  {
   "source": [
    "# Welcome to part 3 of the NtupleAnalysisUtils Tutorial! \n",
    "\n",
    "In this part, we will learn how to use `NtupleAnalysisUtils` to access information from or write it to ROOT NTuples. \n",
    "\n",
    "This is fairly independent of the first two parts. \n",
    "\n",
    "### Notebook setup\n",
    "\n",
    "The following lines are *only* needed within the Jupyter world to set up the package. They are not needed in normal usage (unless you wish to use NtupleAnalysisUtils within a Swan notebook)"
   ],
   "cell_type": "markdown",
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%alias getFromEOS if [[ -e %l ]]; then echo \"Obtaining %l via cp\"; cp -r %l . ;  else echo \"Obtaining %l via xrdcp\" ; xrdcp -r --force root://eosuser.cern.ch/%l .  ; fi "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import ROOT"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "getFromEOS /eos/user/g/goblirsc/NtupleAnalysisUtils/CI_Artifacts/lib/libNtupleAnalysisUtils.so"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "getFromEOS /eos/user/g/goblirsc/NtupleAnalysisUtils/CI_Artifacts/include"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%cpp\n",
    "#pragma cling add_include_path(\"include/\")\n",
    "gSystem->Load(\"libNtupleAnalysisUtils\");"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%cpp\n",
    "#include \"NtupleAnalysisUtils/NTAUTopLevelIncludes.h\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%cpp\n",
    "SetAtlasStyle();"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "getFromEOS /eos/user/g/goblirsc/NtupleAnalysisUtils/Tutorials/DemoTree.root"
   ]
  },
  {
   "source": [
    "### Known limitations of this notebook\n",
    "\n",
    "This notebook lives in cling, which is an interpreter for C++ syntax provided with ROOT. \n",
    "\n",
    "As a consequence, we can not provide the real workflow of coding - compiling - running you would be using in practice. \n",
    "On the positive side, with the interpreted flavour it is very easy to manipulate things in place and experiment! \n",
    "\n",
    "Another consequence is that all C++ cells are wrapped in a \"%%cpp\" 'cell magic', which makes ROOT interpret them. \n",
    "This is of course another thing you would not do in 'real life'. \n",
    "\n",
    "I tried to provide compiled versions of most of the examples within the [examples](https://gitlab.cern.ch/goblirsc/NtupleAnalysisUtils/-/blob/master/examples/) folder of the repository - so you can still try these out in the standard workflow, just without the inline documentation! \n",
    "\n",
    "## Important - Requirements \n",
    "---\n",
    "\n",
    "Another thing to be aware of is that, since we are loading a pre-compiled C++ library, the **version of ROOT** you set up **SWAN** with must match the ROOT version that was used to compile the compiled C++ library provided for this tutorial. \n",
    "\n",
    "The library will always be based on the latest AnalysisBase. At the time of writing, this is **ROOT 6.20.06**, which you can get by **setting up version \"97a\" of the stack in the initial SWAN window**. \n",
    "\n",
    "---\n",
    "You can find the \"correct\" LCG release (stack version) for each ROOT by looking at [this page](http://lcginfo.cern.ch/pkg/ROOT/) and clicking on the version you need. \n",
    "\n",
    "If the versions do not match, the line above in Cell number 3 should print an error. If you don't see one, things are probably fine! \n"
   ],
   "cell_type": "markdown",
   "metadata": {}
  },
  {
   "source": [
    "# The Ntuple classes - when you want to talk to (T)Trees\n",
    "\n",
    "As the name of the package already implies, one main use case is to interact with ROOT TTrees in a convenient way. \n",
    "    \n",
    "For doing so, a set of useful classes exist which allow us to read or write branches from a Tree, avoiding the usual ROOT boilerplate code. \n",
    "\n",
    "We even have an equivalent to ROOT's `MakeClass` to make an arbitrary tree instantly parseable with `NtupleAnalysisUtils!`\n",
    "\n",
    "To show off these features, we will use a real-life analysis ntuple from an ATLAS measurement, containing around 80 thousand entries with information at both particle and reconstruction level in a total of 397 branches. "
   ],
   "cell_type": "markdown",
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "outputPrepend"
    ]
   },
   "outputs": [],
   "source": [
    "%%cpp \n",
    "TFile* fin = TFile::Open(\"DemoTree.root\", \"READ\");\n",
    "TTree* theTree = dynamic_cast<TTree*>(fin->Get(\"SM4L_Nominal\")); \n",
    "theTree->Print();\n",
    "std::cout << theTree->GetEntries() << \" Entries\"<<std::endl;"
   ]
  },
  {
   "source": [
    "Just to remind ourselves how this is done in standalone ROOT, let's write some boilerplate code to see the first ten entries of the \"m4l\" branch, which is a detector-level mass observable, and the Monte-Carlo event weight.  \n",
    "\n",
    "We should also check if this is actually filled with a measured value, rather than a dummy - we can use the \"passReco_SR\" branch to do that, which marks entries passing a pre-selection where the mass is well-defined. \n",
    "\n",
    "Let's look at a few events. \n",
    "\n",
    "In ROOT, first, we have to book some local variables, one to match each branch, and then link them to the tree via `SetBranchAddress`. We would in principle also need to use `SetBranchStatus` to avoid reading branches we don't care about, but let's skip this for the purpose of this tutorial. "
   ],
   "cell_type": "markdown",
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%cpp\n",
    "float m4l = 0;\n",
    "float weight = 0;\n",
    "char passReco_SR = false; \n",
    "theTree->SetBranchAddress(\"m4l\",&m4l);\n",
    "theTree->SetBranchAddress(\"weight\",&weight);\n",
    "theTree->SetBranchAddress(\"passReco_SR\",&passReco_SR);\n"
   ]
  },
  {
   "source": [
    "Now, we can loop over the tree. We call `GetEntry`, and our helper variables will be set to the tree's current values for the given entry. We'll print the result for the first 10 events: "
   ],
   "cell_type": "markdown",
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%cpp\n",
    "\n",
    "Long64_t nentries = theTree->GetEntries(); \n",
    "for (Long64_t k =0; k < 10; ++k){\n",
    "    theTree->GetEntry(k); \n",
    "    if (!passReco_SR) std::cout << \"fail reco cut \"<<std::endl; \n",
    "    else std::cout << \"Mass \"<< m4l << \" and weight \" << weight << std::endl; \n",
    "}"
   ]
  },
  {
   "source": [
    "Now, let's rewind and start to use some features of NtupleAnalysisUtils. \n",
    "\n",
    "## Working with individual branches - the NtupleBranch class\n",
    "\n",
    "To \"talk to\" an individual branch, NtupleAnalysisUtils provides a templated `NtupleBranch` class. \n",
    "\n",
    "This is instantiated by giving it a *branch name* and a *tree to read from*. \n",
    "\n",
    "It will internally take care of all the boilerplate code like \"SetBranchAddress\" or \"SetBranchStatus\". \n",
    "\n",
    "Normally, you would not use isolated instances of the `NtupleBranch` - it stands to reason that in most applications, you would want to work with a wrapper object representing the whole tree at once! This does exist, and we will get to it in a little bit. \n",
    "\n",
    "But for now, let's show the most simple case of accessing just a few branches on their own (slightly cumbersome).\n",
    "\n",
    "We do this using `NtupleBranch` objects - for the printout we want, we need three of them: "
   ],
   "cell_type": "markdown",
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%cpp\n",
    "NtupleBranch<float> ntb_weight(\"weight\",theTree); \n",
    "NtupleBranch<float> ntb_m4l(\"m4l\",theTree); \n",
    "NtupleBranch<char> ntb_passReco_SR(\"passReco_SR\",theTree); "
   ]
  },
  {
   "source": [
    "No `SetBranchAddress`  etc is needed.\n",
    "\n",
    "Now, let's loop again. Notice how when using bare branch objects, we need to call `getEntry` for each individually. \n",
    "We will see how to avoid this later!   "
   ],
   "cell_type": "markdown",
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%cpp\n",
    "for (Long64_t k =0; k < 10; ++k){\n",
    "    ntb_weight.getEntry(k); \n",
    "    ntb_m4l.getEntry(k); \n",
    "    ntb_passReco_SR.getEntry(k); \n",
    "    if (!ntb_passReco_SR()) std::cout << \"fail reco cut \"<<std::endl; \n",
    "    else std::cout << \"Mass \"<< ntb_m4l() << \" and weight \" << ntb_weight() << std::endl; \n",
    "}"
   ]
  },
  {
   "source": [
    "And we see that we can reproduce the plain ROOT result. Notice how we accessed the content of the branches using \"()\". This works for every kind of branch. \n",
    "\n",
    "The `getEntry` method of a branch ensures that we load the correct entry of the underlying TBranch. \n",
    "\n",
    "Now, let's read a vector branch! In plain ROOT, this is a bit ugly. We need to remember that ROOT expects **a pointer to a vector**, otherwise it will produce garbage results. So we first have to book one (empty). \n"
   ],
   "cell_type": "markdown",
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%cpp \n",
    "std::vector<float>* lepton_pt = nullptr;\n",
    "theTree->SetBranchAddress(\"lepton_pt\", &lepton_pt);"
   ]
  },
  {
   "source": [
    "Then, in our event loop, we dereference the pointer, and pray! "
   ],
   "cell_type": "markdown",
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%cpp \n",
    "theTree->GetEntry(3);\n",
    "for (auto & pt : *lepton_pt){\n",
    "    std::cout << pt<<std::endl;\n",
    "}"
   ]
  },
  {
   "source": [
    "In NtupleAnalysisUtils, you don't need to remember any special semantics for vector, array, string, ... branches. This is handled in the background. We can simply use the same syntax as in the earlier case: \n",
    "\n"
   ],
   "cell_type": "markdown",
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%cpp\n",
    "/// this behaves to the user just like any other branch.\n",
    "/// No need to remember pointer semantics or book local objects. \n",
    "NtupleBranch<std::vector<float>> ntb_lepton_pt(\"lepton_pt\",theTree); "
   ]
  },
  {
   "source": [
    "There is a number of ways of accessing the content of our vector branch: \n",
    "* We can get the entire vector \n",
    "* We can also perform element-wise access \n",
    "* And even range-based loops! \n",
    "Let's show all of them: \n",
    "\n",
    "First, we will access the full vector via the `()` operator we used earlier. Notice how this works exactly as for the other branches we encountered already: "
   ],
   "cell_type": "markdown",
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%cpp\n",
    "ntb_lepton_pt.getEntry(3); \n",
    "\n",
    "// The () operator we know from the other branches above also works as expected!\n",
    "const std::vector<float> & pt = ntb_lepton_pt();  // here avoid a vector-copy by using a const ref\n",
    "for (auto & pt : pt){\n",
    "    std::cout << pt<<std::endl;\n",
    "}"
   ]
  },
  {
   "source": [
    "The second method, element-wise access, uses the `()` operator with an unsigned integer argument (the index).\n",
    "Our vector branch also has a `size()` method to tell us how far we need to loop.  "
   ],
   "cell_type": "markdown",
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%cpp\n",
    "for (size_t k =0; k < ntb_lepton_pt.size(); ++k){\n",
    "    std::cout << ntb_lepton_pt(k)<<std::endl;\n",
    "}"
   ]
  },
  {
   "source": [
    "Finally, the vector branch is directly iterable like the vector it represents! \n",
    "\n",
    "So we can do the following: "
   ],
   "cell_type": "markdown",
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%cpp\n",
    "for (auto & pt : ntb_lepton_pt){\n",
    "    std::cout << pt<<std::endl;\n",
    "}"
   ]
  },
  {
   "source": [
    "A similar class also exists for branches containing C-arrays, which are a **huge pain** to access in default ROOT. \n",
    "Our example file does not contain such, but the corresponding `NtupleBranch`es behave just like the vector case. \n",
    "\n",
    "ROOT veterans may know that `std::string` also represents a special case in plain ROOT, requiring a pointer-based interaction like a vector. The `NtupleBranch<std::string>` on the contrary is consistent with the plain data type branches in behaviour. \n",
    "\n",
    "## Using branches for output\n",
    "\n",
    "You can also use an NtupleBranch to **write** an Ntuple. \n",
    "\n",
    "To do so, simply use the `set` method to set the desired content. When you fill the connected ROOT tree, the content will automatically be written! \n",
    "\n",
    "Let's write ourselves a small tree to show this: "
   ],
   "cell_type": "markdown",
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%cpp\n",
    "\n",
    "TFile* fout = new TFile(\"testOut.root\",\"RECREATE\");\n",
    "TTree* tout = new TTree(\"outTree\",\"Treeeeeee\"); "
   ]
  },
  {
   "source": [
    "We'll book an integer and a vector branch. No SetDirectory, no cd(), no Branch() needed. \n",
    "\n",
    "Notice how the same branch constructor can be used both for reading and writing! "
   ],
   "cell_type": "markdown",
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "%%cpp\n",
    "NtupleBranch<int> myInt(\"someInt\",tout);\n",
    "NtupleBranch<std::vector<float>> myFloatVec(\"floatVec\",tout);"
   ]
  },
  {
   "source": [
    "Now, we use the `set` method to give our branches content. Calling `set` will automatically make the branch handle the boilerplate for ROOT output writing in the background: "
   ],
   "cell_type": "markdown",
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%cpp\n",
    "myInt.set(1);\n",
    "myFloatVec.set({0.5,0.75,1.});\n",
    "tout->Fill(); "
   ]
  },
  {
   "source": [
    "of course, writing a branch doesn't prevent you from checking its content: "
   ],
   "cell_type": "markdown",
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%cpp\n",
    "std::cout << myInt()<<std::endl;\n",
    "std::cout << myFloatVec(1)<<std::endl;"
   ]
  },
  {
   "source": [
    " the vector branch can also be operated on like a vector, in case we wish to fill it incrementally: "
   ],
   "cell_type": "markdown",
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%cpp \n",
    "myFloatVec.clear();\n",
    "myFloatVec.push_back(0.6);\n",
    "myFloatVec.push_back(0.8);\n",
    "myFloatVec.push_back(1.2);\n",
    "myInt.set(2);\n",
    "tout->Fill(); "
   ]
  },
  {
   "source": [
    "Let's add two more entries to our tree and then write it."
   ],
   "cell_type": "markdown",
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%cpp\n",
    "myFloatVec.set({2});\n",
    "myInt.set(3);\n",
    "tout->Fill(); \n",
    "\n",
    "myFloatVec.set({3});\n",
    "myInt.set(4);\n",
    "tout->Fill(); \n",
    "\n",
    "tout->Write();"
   ]
  },
  {
   "source": [
    "We can scan the tree before we close the file to confirm that the content written matches our input: \n"
   ],
   "cell_type": "markdown",
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%cpp\n",
    "tout->Scan(\"someInt : floatVec\");\n",
    "fout->Close();"
   ]
  },
  {
   "source": [
    "That was easy, wasn't it? \n",
    "\n",
    "And we can also copy branches across trees! This can be useful for post-processing.\n",
    "\n",
    "Let's demonstrate by skimming out a few branches from our big ntuple into a new tree.\n",
    "\n",
    "First, we book the tree to write into "
   ],
   "cell_type": "markdown",
   "metadata": {}
  },
  {
   "source": [
    "%%cpp\n",
    "TFile* fout = new TFile(\"testOut.root\",\"RECREATE\");\n",
    "TTree* tout = new TTree(\"outTree\",\"Treeeeeee\"); "
   ],
   "cell_type": "code",
   "metadata": {},
   "execution_count": null,
   "outputs": []
  },
  {
   "source": [
    "Now, let's book three branches to write out - the ones we printed in the past. \n",
    "\n",
    "For consistency, we will give them the same names as in the original NTuple - but this is purely optional, we could also have renamed them! "
   ],
   "cell_type": "markdown",
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%cpp\n",
    "\n",
    "NtupleBranch<float> m4l(\"m4l\",tout);\n",
    "NtupleBranch<float> weight(\"weight\",tout);\n",
    "NtupleBranch<char> passReco_SR(\"passReco_SR\",tout);\n"
   ]
  },
  {
   "source": [
    "Now, we'll loop over the ntuple, and set our output branches to the content of the branches we made earlier to **read** from the ntuple. This is done using a special `set` method, where we pass the address of a branch to copy from: "
   ],
   "cell_type": "markdown",
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%cpp\n",
    "for (Long64_t k = 0; k < 10; ++k){\n",
    "    /// Remember these from earlier? They are our reading-branches \n",
    "    ntb_weight.getEntry(k); \n",
    "    ntb_m4l.getEntry(k); \n",
    "    ntb_passReco_SR.getEntry(k); \n",
    "    /// here we tell our output branches to copy their friends from the other tree\n",
    "    m4l.set(&ntb_m4l); \n",
    "    weight.set(&ntb_weight); \n",
    "    passReco_SR.set(&ntb_passReco_SR);    \n",
    "    /// and write to the new tree\n",
    "    tout->Fill();    \n",
    "}"
   ]
  },
  {
   "source": [
    "The new tree will now contain a subset of the branches of the original, for the first 10 entries:"
   ],
   "cell_type": "markdown",
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%cpp\n",
    "tout->Scan(\"passReco_SR : m4l : weight\");\n",
    "fout->Close();"
   ]
  },
  {
   "source": [
    "# Packaging many branches - the NtupleBranchMgr\n",
    "\n",
    "In the examples above, you may have noticed that it is a bit inconvenient to talk to all the branches one by one! \n",
    "\n",
    "For this reason, the [NtupleBranchMgr](https://gitlab.cern.ch/goblirsc/NtupleAnalysisUtils/-/blob/master/NtupleAnalysisUtils/Ntuple/NtupleBranchMgr.h) is provided as a wrapper object that can be used to communicate with the entirety of a Tree. \n",
    "\n",
    "This is the recommended way of interacting with TTrees in user code. \n",
    "\n",
    "## Basic usage - a container for branches\n",
    "\n",
    "The `NtupleBranchMgr` is at first glance a very primitive class - it just acts as a packaging for a number of `NtupleBranch` objects. You can set the entry number for all of them simultaneously, and hand the thing around as a function argument for parsing trees (which makes all of the tree content available to the function).\n",
    "\n",
    "Let's demonstrate by writing a basic version for our analysis ntuple. Here, we only need make it aware of a subset of the content we are interested in. The intended usage is to write a class for \"your\" NTuple inheriting from `NtupleBranchMgr`: "
   ],
   "cell_type": "markdown",
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%cpp -d\n",
    "\n",
    "/// The concrete tree is represented by a class inheriting from the NtupleBranchMgr. \n",
    "class MyTree : public NtupleBranchMgr{\n",
    "public: \n",
    "    /// The constructor should take the tree to write to or read from \n",
    "    MyTree(TTree* t): \n",
    "        NtupleBranchMgr(t){\n",
    "        /// this will auto-discover all other branches in this Ntuple! \n",
    "        getMissedBranches(t); \n",
    "    } \n",
    "    /// specify the branches as public members for easy access \n",
    "    // the \"this\" argument in the constructor makes them synchronise with this manager object. \n",
    "    NtupleBranch<float>               m4l{\"m4l\",m_tree,this}; \n",
    "    NtupleBranch<float>               weight{\"weight\",m_tree,this}; \n",
    "    NtupleBranch<char>                passReco_SR{\"passReco_SR\",m_tree,this}; \n",
    "    NtupleBranch<std::vector<float>>  lepton_pt{\"lepton_pt\",m_tree,this}; \n",
    "};"
   ]
  },
  {
   "source": [
    "Now, we can use our new class to parse the ntuple again.\n",
    "\n",
    "Notice that we now have zero remaining boilerplate code! "
   ],
   "cell_type": "markdown",
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%cpp\n",
    "\n",
    "MyTree t(theTree); \n",
    "\n",
    "for (Long64_t k =0; k < 10; ++k){\n",
    "    t.getEntry(k); \n",
    "    if (!t.passReco_SR()) std::cout << \"fail reco cut \"<<std::endl; \n",
    "    else {\n",
    "        std::cout << \"Mass \"<< t.m4l() << \" and weight \" << t.weight() << std::endl; \n",
    "        std::cout << \"Lepton pt: \"<<std::endl;\n",
    "        for (auto pt : t.lepton_pt){\n",
    "                std::cout << \"     \"<<pt<<std::endl; \n",
    "        }\n",
    "    }\n",
    "}"
   ]
  },
  {
   "source": [
    "Fun fact: The ntuple class also works in pyROOT: "
   ],
   "cell_type": "markdown",
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f = ROOT.TFile.Open(\"DemoTree.root\", \"READ\")\n",
    "theTree = f.Get(\"SM4L_Nominal\")\n",
    "myTree = ROOT.MyTree(theTree) \n",
    "for i in range(10): \n",
    "    myTree.getEntry(i)\n",
    "    if not myTree.passReco_SR():\n",
    "        print(\"fail reco cut \") \n",
    "    else: \n",
    "        print (\"Mass \",myTree.m4l(),\" and weight \",myTree.weight())\n",
    "        print (\"Lepton pt\") \n",
    "        for p in myTree.lepton_pt(): \n",
    "            print (p)\n",
    "f.Close()\n"
   ]
  },
  {
   "source": [
    "This is starting to look at lot more handy than the ROOT code we started from! \n",
    "\n",
    "## Branch discovery and Ntuple exploration\n",
    "\n",
    "But it turns out the NtupleBranchMgr can do a lot more! \n",
    "For example, it has **automatic branch discovery**. \n",
    "\n",
    "Our Ntuple contains another branch, `pt4l`. We did not specify this when we wrote our class.\n",
    "\n",
    "Did our branch manager find it? And can we read it? "
   ],
   "cell_type": "markdown",
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%cpp\n",
    "std::cout << \"Do we have pt4l? \"<<t.hasBranchInTree(\"pt4l\")<<std::endl; "
   ]
  },
  {
   "source": [
    "We can access this branch without having to add an explicit `NtupleBranch` member to the `MyTree` class. \n",
    "\n",
    "This 'on the fly' reading functionality is however intended mainly for rarely used branches, as it involves additional overhead (each call triggers a search for the specified branch): \n",
    " "
   ],
   "cell_type": "markdown",
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%cpp\n",
    "t.getEntry(3); \n",
    "float thePt4l = 0;\n",
    "/// access using the () operator - first arg is the name of the branch, second the variable to attempt to fill. \n",
    "t(\"pt4l\",thePt4l);\n",
    "std::cout << \"I read pt4l = \"<<thePt4l<<std::endl;"
   ]
  },
  {
   "source": [
    "In addition to this one-off lookup, we can also make the branch manager generate a branch object for us - this can then be re-used without additional overhead. This is done using the `getBranch` method, providing the branch type and name we are looking for"
   ],
   "cell_type": "markdown",
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%cpp\n",
    "t.getEntry(3); \n",
    "NtupleBranch<float>* br_pt4l = t.getBranch<float>(\"pt4l\");\n",
    "std::cout << \"I read pt4l from the branch: \"<<(*br_pt4l)()<<std::endl;"
   ]
  },
  {
   "source": [
    "The branch manager is good at book-keeping - for example, it can give you all branches with a specific prefix and / or suffix. Think of collecting all your systematics! \n",
    "\n",
    "For example, let's see which branches starting with 'weight_var' there are in our analysis ntuple."
   ],
   "cell_type": "markdown",
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%cpp\n",
    "for (auto & nameAndBranch : t.getBranchesStartingWith(\"weight_var\")){\n",
    "    std::cout << \"found a weight variation branch \"<<nameAndBranch.first <<std::endl;\n",
    "}"
   ]
  },
  {
   "source": [
    "This functionality would for example allow us to write a systematics loop without having to know in advance which variations have been saved to the current ntuple file. \n",
    "\n",
    "And good news regarding the I/O overhead when being 'aware' of un-used branches: The accessor branches automatically generated in the background **do not read anything and cause zero I/O overhead unless actually actively accessed!** \n",
    "\n",
    "### Code-generation\n",
    "\n",
    "Now... writing this class is still a bit of manual work, isn't it? \n",
    "\n",
    "Good that we don't actually need to do it! \n",
    "\n",
    "NtupleAnalysisUtils comes with a [**code generator**](https://gitlab.cern.ch/goblirsc/NtupleAnalysisUtils/-/blob/master/util/generateBranchManager.cxx) to take care of this for us. \n",
    "Just run the included `generateBranchManager` program, and it will write an appropriate class for you, ready to copy paste into your user package.\n",
    "The arguments to the program are: \n",
    "   * The file to take the tree from\n",
    "   * The name of the tree to read\n",
    "   * The class name to give the automatically generated class\n",
    "\n",
    "Here, since we are in a CINT session, we will do the same from within C++: \n",
    "\n",
    "To demonstrate that we are not cheating, we first remove any files with the name of the class we want to generate: "
   ],
   "cell_type": "markdown",
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# prove that we are not cheating! \n",
    "%rm MyTree.* \n",
    "%ls MyTree.*"
   ]
  },
  {
   "source": [
    "now, we run the exactly same thing that generateBranchManager would do:\n",
    "\n"
   ],
   "cell_type": "markdown",
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%cpp\n",
    "/// discover all branches we missed so far \n",
    "t.getMissedBranches(theTree);\n",
    "/// now auto-generate code for a class with all known branches.\n",
    "/// Place it in the current directory (second arg). \n",
    "t.GenerateManagerClass(\"MyFullTree\");"
   ]
  },
  {
   "source": [
    "And there it is, ready to copy into an analysis package of your liking! \n",
    "Let's take a look: "
   ],
   "cell_type": "markdown",
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "outputPrepend"
    ]
   },
   "outputs": [],
   "source": [
    "%cat MyFullTree.h"
   ]
  },
  {
   "source": [
    "See how this looks very similar to what we wrote by hand before? The tree can be plugged into your package and used right away, zero code changes needed. \n",
    "\n",
    "The class is header-only, so we can easily include it. Let's do so for the following. "
   ],
   "cell_type": "markdown",
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%cpp\n",
    "#include \"MyFullTree.h\""
   ]
  },
  {
   "source": [
    "## Derived virtual branches\n",
    "\n",
    "Sometimes, the branches as they are in an ntuple are not ideally adapted for our use case. \n",
    "\n",
    "For example, an observable we frequently need may not be directly included, but take some calculation. \n",
    "Imagine an NTuple might store Px, Py, Pz, and M of an object, while we need the transverse momentum.\n",
    "Or imagine we trained a fancy new neural network, and now want to have its value in our ntuple without having to run a new production. \n",
    "\n",
    "To avoid having to juggle helper methods calculating observables on the fly, there is the `DerivedVirtualBranch` class.\n",
    "\n",
    "This is designed to look and feel like a 'real' branch - but internally, when we access it, it will perform a pre-defined calculation on the content of the input tree and return the result. \n",
    "\n",
    "It also will cache the result, so that the calculation needs only to be performed once per entry of the tree - this minimises the overhead as far as possible. \n",
    "\n",
    "Let's look at an example, where we use such a branch to access the Z-momentum of our four-lepton system. We store Pt, Eta, Phi and M. \n",
    "\n",
    "The 'best practice' way of adding derived branches is to write another `NtupleBranchMgr`-derived class inheriting from the auto-generated \"original\" ntuple class and adding the virtual branches. This avoids the 'hen and egg' problem of being able a virtual branch that uses information from itself ;-)"
   ],
   "cell_type": "markdown",
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%cpp -d\n",
    "\n",
    "class MyTreeWithExtraBranch: public MyFullTree{\n",
    "public: \n",
    "\n",
    "    /// virtual branches don't need any further initialisation in the constructor\n",
    "    MyTreeWithExtraBranch(TTree* t): MyFullTree(t){}\n",
    "    \n",
    "    // the derived branch is initialised with a function to calculate its output\n",
    "    // and an input to read the information from. \n",
    "    DerivedVirtualBranch<float, MyFullTree> pZ4l{[](MyFullTree &t){\n",
    "        // add a printout to indicate when the cache is being updated. \n",
    "        std::cout << \" [[[now calculating pZ4l internally]]] \"<<std::endl;\n",
    "        TLorentzVector lv;\n",
    "        lv.SetPtEtaPhiM(t.pt4l(), t.eta4l(), t.phi4l(), t.m4l()); \n",
    "        return lv.Pz();     \n",
    "    }, this}; \n",
    "\n",
    "};"
   ]
  },
  {
   "source": [
    "Time to test it. We'll ask for our derived branch twice and see how the caching kicks in for the second call. "
   ],
   "cell_type": "markdown",
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%cpp\n",
    "// create an instance of our (extended) class\n",
    "MyTreeWithExtraBranch tExtra{theTree};\n",
    "tExtra.getEntry(3); \n",
    "// now this can be accessed as if it was a real branch in the NTuple\n",
    "std::cout << \"pZ 4l is \"<<tExtra.pZ4l()<<std::endl;\n",
    "// call again in the same entry - no recalculation \n",
    "std::cout << \"pZ 4l is in the second call is \"<<tExtra.pZ4l()<<std::endl;"
   ]
  },
  {
   "source": [
    "\n",
    "In this way, you can make your input ntuple behave as if it had many more branches than are actually stored - or reduce the ntuple size for a given amount of branches available for the end user. \n",
    "The overhead is lower than with functions continuously re-calculating the same thing as well."
   ],
   "cell_type": "markdown",
   "metadata": {}
  },
  {
   "source": [
    "## Branch access by name - for rarely used branches, or string-based interfaces\n",
    "There is one more way to access ntuple contents.\n",
    "\n",
    "Sometimes, we would like to access branches that are not part of our tree class (for example because they are only written in a single special production and we don't want to replace our class) in a way more efficient than asking the branch manager, as done above.\n",
    "\n",
    "Or, we want to provide a way of specifying which branches to draw via strings in a command line interface.\n",
    "\n",
    "You might also want to access a branch of a certain name on arbitrarily typed input trees, without knowing their precise content.\n",
    "\n",
    "To do this, there is an `NtupleBranchAccessByName` class. It is initialised with a branch name, and can then be pointed at an `NtupleBranchMgr` to retrieve the branch of the requested name (if it was autodetected).\n",
    "\n",
    "For example, let's use our minimal `MyTree` from earlier and read the 4-lepton rapidity:"
   ],
   "cell_type": "markdown",
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%cpp\n",
    "\n",
    "MyTree myTree(theTree);\n",
    "myTree.getEntry(3); \n",
    "// ntuple branch access by name - needs a branch name to read\n",
    "NtupleBranchAccessByName<float> y4l (\"y4l\"); \n",
    "float result = 0; \n",
    "// first read from our basic self written tree. Notice that the return of the method \n",
    "// tells us if the access was succesful, while the content is written into the variable\n",
    "// passed as reference in the second argument. \n",
    "if (y4l.getVal(myTree,result)){\n",
    "    std::cout << \"I was able to read \"<<result<<std::endl;\n",
    "}\n",
    "// and we can use the same handle to read a branch by name from a different tree class, without templates etc\n",
    "if (y4l.getVal(tExtra,result)){\n",
    "    std::cout << \"I was also able to read from the other tree \"<<result<<std::endl;\n",
    "}"
   ]
  },
  {
   "source": [
    "There is extra 'syntactic overhead' from the 'return-via-reference' pattern and checking the read result, but it is important to check this since the access by name can be invoked on any tree, without a guarantee for the sought-after branch to actually be there!\n",
    "\n",
    "## Bootstrap weights\n",
    "\n",
    "As colleagues of mine might know, I am fond of the bootstrap method. \n",
    "This method involves assigning events (assumed to be represented by tree entries) random, poisson-distributed weights with a mean value of 1, and consistent for every 're-use' of the same unique event, to obtain an ensemble of varied versions of the same sample which allow to approximate statistical uncertainties together with their correlations.\n",
    "\n",
    "Naturally, the `NtupleBranchMgr` is able to generate bootstrap toys for you! \n",
    "You can\n",
    "* Set the number of weights to generate per entry via `setNBootstrapWeights`\n",
    "* Generate the weights (for the current entry) via `generateBootstrapWeights`\n",
    "* Re-seed the bootstrap generator with `seedBootStrap`\n",
    "and of course\n",
    "* access the i-th bootstrap weight with `getBootstrapWeight`\n",
    "\n",
    "Let's try this: "
   ],
   "cell_type": "markdown",
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%cpp\n",
    "myTree.getEntry(0);\n",
    "myTree.setNBootstrapWeights(5); /// ask for five weights per event to be provided. \n",
    "myTree.seedBootStrap(1234); /// in reality, we would use something unique and reproducible for each sample, for example DSID or run/event number \n",
    "myTree.generateBootstrapWeights(); \n",
    "std::cout << myTree.getBootstrapWeight(0)<<\" \"<<myTree.getBootstrapWeight(1)<<\" \"<<myTree.getBootstrapWeight(2)<<\" \"<<myTree.getBootstrapWeight(3)<<\" \"<<myTree.getBootstrapWeight(4)<<\" \"<<std::endl;\n",
    "\n",
    "myTree.getEntry(1);\n",
    "myTree.generateBootstrapWeights(); \n",
    "std::cout << myTree.getBootstrapWeight(0)<<\" \"<<myTree.getBootstrapWeight(1)<<\" \"<<myTree.getBootstrapWeight(2)<<\" \"<<myTree.getBootstrapWeight(3)<<\" \"<<myTree.getBootstrapWeight(4)<<\" \"<<std::endl;\n",
    "\n",
    "myTree.getEntry(2);\n",
    "myTree.generateBootstrapWeights(); \n",
    "std::cout << myTree.getBootstrapWeight(0)<<\" \"<<myTree.getBootstrapWeight(1)<<\" \"<<myTree.getBootstrapWeight(2)<<\" \"<<myTree.getBootstrapWeight(3)<<\" \"<<myTree.getBootstrapWeight(4)<<\" \"<<std::endl;\n",
    "\n",
    "myTree.getEntry(3);\n",
    "myTree.generateBootstrapWeights(); \n",
    "std::cout << myTree.getBootstrapWeight(0)<<\" \"<<myTree.getBootstrapWeight(1)<<\" \"<<myTree.getBootstrapWeight(2)<<\" \"<<myTree.getBootstrapWeight(3)<<\" \"<<myTree.getBootstrapWeight(4)<<\" \"<<std::endl;"
   ]
  },
  {
   "source": [
    "# What next? \n",
    "\n",
    "Congratulations, if you are reading this you went through a fairly long part covering the tree I/O of the package. \n",
    "\n",
    "Next, we will learn how we can combine the Ntuple-I/O with our `Plot` class, and how we can automatise histogramming from ntuples as part of a `Populator`. "
   ],
   "cell_type": "markdown",
   "metadata": {}
  }
 ]
}