#ifndef MUONSAGITTABIAS_ASYMMETRYPLOTS_IXX
#define MUONSAGITTABIAS_ASYMMETRYPLOTS_IXX

#include <NtauHelpers/HistoUtils.h>
#include <NtauHelpers/MuonUtils.h>

template <class InputHistoType>
AsymmetryPlot<InputHistoType>::AsymmetryPlot(const std::string& plot_name, const IPopulatorSource<InputHistoType>& forwardMuons,
                                             const IPopulatorSource<InputHistoType>& backwardMuons) :
    m_forward(forwardMuons.clonePopulator()), m_backward(backwardMuons.clonePopulator()), m_plot_name(plot_name) {}

/// this will populate the upstream sources and then perform a ratio calculation
template <class InputHistoType> std::shared_ptr<TH1> AsymmetryPlot<InputHistoType>::populate() {
    auto forward_plot = m_forward->populate();
    auto backward_plot = m_backward->populate();
    if (forward_plot->GetEntries() == 0 || backward_plot->GetEntries() == 0) { return clone(forward_plot, true); }
    std::shared_ptr<TH1> sum_plot = clone(forward_plot);
    sum_plot->Add(backward_plot.get());

    std::shared_ptr<TH1> final_plot = clone(forward_plot);
    final_plot->Add(backward_plot.get(), -1);
    final_plot->Divide(sum_plot.get());
    return final_plot;
}
template <class InputHistoType> std::shared_ptr<IPlotPopulator<TH1>> AsymmetryPlot<InputHistoType>::clonePopulator() const {
    return std::make_shared<AsymmetryPlot>(m_plot_name, *m_forward, *m_backward);
}

template <class InputHistoType> std::string AsymmetryPlot<InputHistoType>::plot_name() const { return m_plot_name; }

template <class InputHistoType>
AsymmetryPlot<InputHistoType> make_asymmetry_plot(const std::string& plot_name, Sample<SagittaBiasAnalyzer> sample,
                                                  Selection<SagittaBiasAnalyzer> plot_sel,
                                                  PlotFillInstructionWithRef<InputHistoType, SagittaBiasAnalyzer> fillInstruction,
                                                  int trk_type

) {
    Selection<SagittaBiasAnalyzer> pos_theta{[trk_type](SagittaBiasAnalyzer& t) { return t.cosThetaCS(trk_type) > 0; }};
    Selection<SagittaBiasAnalyzer> neg_theta{[trk_type](SagittaBiasAnalyzer& t) { return t.cosThetaCS(trk_type) < 0; }};
    Selection<SagittaBiasAnalyzer> is_valid{[trk_type](SagittaBiasAnalyzer& t) { return t.is_valid(trk_type); }};

    Selection<SagittaBiasAnalyzer> forward_sel = is_valid && plot_sel && pos_theta;
    Selection<SagittaBiasAnalyzer> backward_sel = is_valid && plot_sel && neg_theta;
    Plot<InputHistoType> forward_plot{RunHistoFiller{sample, forward_sel, fillInstruction}};
    Plot<InputHistoType> backward_plot{RunHistoFiller{sample, backward_sel, fillInstruction}};

    return AsymmetryPlot<InputHistoType>{plot_name, forward_plot, backward_plot};
}

#endif
