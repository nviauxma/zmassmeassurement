# CMake generated Testfile for 
# Source directory: /afs/cern.ch/user/n/nviauxma/ZmassCalibration3/source
# Build directory: /afs/cern.ch/user/n/nviauxma/ZmassCalibration3/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("MuonSagittaBias")
subdirs("NtauHelpers/ClusterSubmission")
subdirs("NtauHelpers/NtauHelpers")
subdirs("NtauHelpers/NtupleAnalysisUtils")
subdirs("NtauHelpers/NtupleAnalysisUtils/NtupleAnalysisUtils_tutorial")
