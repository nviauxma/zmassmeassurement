#ifndef NTAUHELPERS_CONTAINERSELECTION_IXX
#define NTAUHELPERS_CONTAINERSELECTION_IXX
#include <NtauHelpers/ContainerSelection.h>
#include <NtupleAnalysisUtils/NTAUTopLevelIncludes.h>

template <class ProcessThis>
ContainerSelection<ProcessThis>::ContainerSelection(const std::function<size_t(ProcessThis&)>& size_function,
                                                    const std::function<bool(ProcessThis&, size_t)>& sel_function,
                                                    const std::string& sel_name, const std::string& sel_title) :
    m_name{sel_name}, m_title{sel_title}, m_container_size{size_function}, m_cut_func{sel_function} {}
template <class ProcessThis> std::string ContainerSelection<ProcessThis>::name() const { return m_name; }
template <class ProcessThis> std::string ContainerSelection<ProcessThis>::title() const { return m_title; }

template <class ProcessThis> void ContainerSelection<ProcessThis>::set_title(const std::string& new_title) { m_title = new_title; }

template <class ProcessThis> const std::function<size_t(ProcessThis&)>& ContainerSelection<ProcessThis>::size() const {
    return m_container_size;
}
/// Returns the actual selection
template <class ProcessThis> const std::function<bool(ProcessThis&, size_t)>& ContainerSelection<ProcessThis>::obj_cut() const {
    return m_cut_func;
}

template <class ProcessThis>
ContainerSelection<ProcessThis> operator&&(const ContainerSelection<ProcessThis>& a, const ContainerSelection<ProcessThis>& b) {
    if (a.name() > b.name()) return b && a;
    std::function<size_t(ProcessThis&)> size_a = a.size();
    /// For now we assume that a and b are pointing to the same vector
    /// std::function<size_t(ProcessThis&)> size_b = b.size();
    std::function<bool(ProcessThis&, size_t)> sel_a = a.obj_cut();
    std::function<bool(ProcessThis&, size_t)> sel_b = b.obj_cut();
    return ContainerSelection<ProcessThis>{size_a, [sel_a, sel_b](ProcessThis& t, size_t i) { return sel_a(t, i) && sel_b(t, i); },
                                           a.name() + "__AND__" + b.name(), a.title() + " #wedge " + b.title()};
}
template <class ProcessThis>
ContainerSelection<ProcessThis> operator||(const ContainerSelection<ProcessThis>& a, const ContainerSelection<ProcessThis>& b) {
    if (a.name() > b.name()) return b || a;
    std::function<size_t(ProcessThis&)> size_a = a.size();
    /// For now we assume that a and b are pointing to the same vector
    /// std::function<size_t(ProcessThis&)> size_b = b.size();
    std::function<bool(ProcessThis&, size_t)> sel_a = a.obj_cut();
    std::function<bool(ProcessThis&, size_t)> sel_b = b.obj_cut();
    return ContainerSelection<ProcessThis>{size_a, [sel_a, sel_b](ProcessThis& t, size_t i) { return sel_a(t, i) || sel_b(t, i); },
                                           a.name() + "__OR__" + b.name(), a.title() + " #veww " + b.title()};
}

/// Ceates a new selection as the logical NOT of the input object/
template <class ProcessThis> ContainerSelection<ProcessThis> operator!(const ContainerSelection<ProcessThis>& to_invert) {
    std::function<bool(ProcessThis&, size_t)> obj_cut = to_invert.obj_cut();
    return ContainerSelection<ProcessThis>{to_invert.size(), [obj_cut](ProcessThis& t, size_t i) { return !obj_cut(t, i); },
                                           "NOT__" + to_invert.name(), "!" + to_invert.name()};
}

#endif
