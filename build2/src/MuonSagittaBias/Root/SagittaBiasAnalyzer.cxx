// Please add your package name here
#include <FourMomUtils/xAODP4Helpers.h>
#include <Math/GenVector/VectorUtil.h>
#include <MuonSagittaBias/PseudoCollinsSoperUtils.h>
#include <MuonSagittaBias/SagittaBiasAnalyzer.h>
#include <MuonSagittaBias/SagittaInjector.h>
#include <MuonSagittaBias/TriggerAccess.h>
#include <MuonSagittaBias/Utils.h>
#include <NtauHelpers/TriggerHelper.h>
#include <xAODMuon/Muon.h>
namespace {
    constexpr float MeVtoGeV = 1.e-3;
    constexpr float GeVtoMeV = 1.e3;
    constexpr float muon_mass = 105.6 * 1.e-3;

    // https://pdg.lbl.gov/2020/listings/rpp2020-list-z-boson.pdf
    constexpr float Z_Mass = 91.187;
    constexpr float JPsi_Mass = 3.09690;
    constexpr float Upsilon_Mass = 9.4603;

    // Trigger matching pt thresholds
    constexpr float trigmatch_thresh_mu4 = 4. * 1.05;
    constexpr float trigmatch_thresh_mu6 = 6. * 1.05;
    constexpr float trigmatch_thresh_mu8 = 8. * 1.05;
    constexpr float trigmatch_thresh_mu10 = 10. * 1.05;
    constexpr float trigmatch_thresh_mu11 = 11. * 1.05;
    constexpr float trigmatch_thresh_mu14 = 14. * 1.05;
    constexpr float trigmatch_thresh_mu20 = 20. * 1.05;
    constexpr float trigmatch_thresh_mu22 = 22. * 1.05;
    constexpr float trigmatch_thresh_mu24 = 24. * 1.05;
    constexpr float trigmatch_thresh_mu26 = 26. * 1.05;
    constexpr float trigmatch_thresh_mu40 = 40. * 1.05;
    constexpr float trigmatch_thresh_mu50 = 50. * 1.05;

    using data_period = TriggerHelper::data_period;
}  // namespace

std::atomic<bool> SagittaBiasAnalyzer::s_constructor_called = false;
std::vector<SagittaBiasAnalyzer::calib_constants> SagittaBiasAnalyzer::s_sagitta_inject_params{};
///
////// Static member functions
///
void SagittaBiasAnalyzer::add_sagitta_injection(calib_constants calib) {
    if (s_constructor_called) {
        std::cerr << "SagittaBiasAnalyzer::add_sagitta_injection() -- WARNING: An instance of the class has already been made "
                  << std::endl;
        return;
    }
    if (calib.has_resolution()) calib.trk_type |= MuonTrk::TrackType::DetectorResolution;
    if (calib.sagitta_bias) calib.trk_type |= MuonTrk::TrackType::SagittaBias;
    if (calib.magnetic_bias) calib.trk_type |= MuonTrk::TrackType::MagneticBias;
    if (calib.radial_bias) calib.trk_type |= MuonTrk::TrackType::RadialBias;

    /// Avoid duplications and quit the method silently
    if (!(calib.trk_type & MuonTrk::TrackType::BiasedTrk) ||
        std::find(s_sagitta_inject_params.begin(), s_sagitta_inject_params.end(), calib) != s_sagitta_inject_params.end())
        return;
    s_sagitta_inject_params.emplace_back(std::move(calib));
    std::sort(s_sagitta_inject_params.begin(), s_sagitta_inject_params.end());
}
const std::vector<SagittaBiasAnalyzer::calib_constants>& SagittaBiasAnalyzer::sagitta_injections() { return s_sagitta_inject_params; }

std::vector<int> SagittaBiasAnalyzer::injection_types() {
    std::vector<int> s_types;
    const int n_injectors = sagitta_injections().size();
    s_types.reserve(n_injectors);
    for (int i = 0; i < n_injectors; ++i) { s_types.push_back((i << MuonTrk::TrackType::NTypes) | sagitta_injections()[i].trk_type); }
    return s_types;
}
int SagittaBiasAnalyzer::inject_from_type(int trk_type) {
    if (!(trk_type & MuonTrk::TrackType::BiasedTrk)) return -1;
    return trk_type >> MuonTrk::TrackType::NTypes;
}

int SagittaBiasAnalyzer::smallest_inject_idx(int trk_type) {
    int smallest_idx = 0;
    for (const calib_constants& calib : sagitta_injections()) {
        if (calib.trk_type & trk_type) return smallest_idx;
        ++smallest_idx;
    }

    return -1;
}
int SagittaBiasAnalyzer::largest_inject_idx(int trk_type) {
    int largest_idx{0};
    bool found{false};
    for (const calib_constants& calib : sagitta_injections()) {
        if (calib.trk_type & trk_type)
            found = true;
        else if (found)
            return largest_idx - 1;
        ++largest_idx;
    }
    return largest_idx - 1;
}
int SagittaBiasAnalyzer::num_injections(int trk_type) { return largest_inject_idx(trk_type) - smallest_inject_idx(trk_type); }

///
///
///
bool SagittaBiasAnalyzer::is_mc() const { return gen_weight.is_mc(); }
bool SagittaBiasAnalyzer::has_random_run_number() const { return m_has_rnd_run_number; }
SagittaBiasAnalyzer::SagittaBiasAnalyzer(TTree* t) :
    SagittaBiasTree(t),
    event_weight{[](SagittaBiasAnalyzer& t) { return t.gen_weight() * t.prescale_weight(); }, this},
    prescale_weight{this},
    trig_decision{[](SagittaBiasAnalyzer& t) -> bool {
                      TrigSelVector& trig_list = t.potential_triggers();
                      /// For the moment do not reject the event if the trigger list is empty
                      if (trig_list.empty()) return true;
                      for (TrigSelPtr& ptr : trig_list) {
                          if (ptr->is_accepted(t)) return true;
                      }
                      return false;
                  },
                  this},
    m_injectors{} {
    s_constructor_called = true;
    for (const calib_constants& calib : sagitta_injections()) { m_injectors.emplace_back(std::make_unique<SagittaInjector>(*this, calib)); }

    m_has_rnd_run_number = is_mc() && randomRunNumber.existsInTree();

    /// Setup the trigger configuration stream
    setup_z_triggers();
    setup_jpsi_triggers();
    m_def_trigs.clear();
}

SagittaBiasAnalyzer::~SagittaBiasAnalyzer() = default;
SagittaInjector* SagittaBiasAnalyzer::get_injector(int trk_type) {
    const int n = inject_from_type(trk_type);
    if (n == -1 || ((trk_type & MuonTrk::TrackType::TruthTrk) && !is_mc())) return nullptr;
    return m_injectors[n].get();
}

float SagittaBiasAnalyzer::pseudo_mass(int trk_type, bool pos_lep) {
    if (trk_type == MuonTrk::TrackType::Primary)
        return (pos_lep ? pseudoMass_posLep() : pseudoMass_negLep()) * MeVtoGeV;
    else if (trk_type == MuonTrk::TrackType::IDTrk)
        return (pos_lep ? pseudoMassIDTrk_posLep() : pseudoMassIDTrk_negLep()) * MeVtoGeV;
    else if (trk_type == MuonTrk::TrackType::METrk)
        return (pos_lep ? pseudoMassMETrk_posLep() : pseudoMassMETrk_negLep()) * MeVtoGeV;
    else if (is_mc() && trk_type == MuonTrk::TrackType::TruthTrk)
        return (pos_lep ? pseudoMassTruthTrk_posLep() : pseudoMassTruthTrk_negLep()) * MeVtoGeV;

    SagittaInjector* inject = get_injector(trk_type);
    if (inject) return (pos_lep ? inject->pseudo_mass_pos() : inject->pseudo_mass_neg());
    return -1;
}
float SagittaBiasAnalyzer::pt(int trk_type, bool pos_lep) {
    if (trk_type == MuonTrk::TrackType::Primary)
        return (pos_lep ? posLep_pt() : negLep_pt()) * MeVtoGeV;
    else if (trk_type == MuonTrk::TrackType::IDTrk)
        return (pos_lep ? posLepIDTrk_pt() : negLepIDTrk_pt()) * MeVtoGeV;
    else if (trk_type == MuonTrk::TrackType::METrk)
        return (pos_lep ? posLepMETrk_pt() : negLepMETrk_pt()) * MeVtoGeV;
    else if (is_mc() && trk_type == MuonTrk::TrackType::TruthTrk)
        return (pos_lep ? posLepTruthTrk_pt() : negLepTruthTrk_pt()) * MeVtoGeV;
    SagittaInjector* inject = get_injector(trk_type);
    if (inject) return (pos_lep ? inject->pos_lep() : inject->neg_lep()).Pt();

    return -1;
}
float SagittaBiasAnalyzer::eta(int trk_type, bool pos_lep) {
    if (trk_type == MuonTrk::TrackType::Primary)
        return (pos_lep ? posLep_eta() : negLep_eta());
    else if (trk_type == MuonTrk::TrackType::IDTrk)
        return (pos_lep ? posLepIDTrk_eta() : negLepIDTrk_eta());
    else if (trk_type == MuonTrk::TrackType::METrk)
        return (pos_lep ? posLepMETrk_eta() : negLepMETrk_eta());
    else if (is_mc() && trk_type == MuonTrk::TrackType::TruthTrk)
        return (pos_lep ? posLepTruthTrk_eta() : negLepTruthTrk_eta());
    SagittaInjector* inject = get_injector(trk_type);
    if (inject) return (pos_lep ? inject->pos_lep() : inject->neg_lep()).Eta();
    return -100;
}
float SagittaBiasAnalyzer::phi(int trk_type, bool pos_lep) {
    if (trk_type == MuonTrk::TrackType::Primary)
        return (pos_lep ? posLep_phi() : negLep_phi());
    else if (trk_type == MuonTrk::TrackType::IDTrk)
        return (pos_lep ? posLepIDTrk_phi() : negLepIDTrk_phi());
    else if (trk_type == MuonTrk::TrackType::METrk)
        return (pos_lep ? posLepMETrk_phi() : negLepMETrk_phi());
    else if (is_mc() && trk_type == MuonTrk::TrackType::TruthTrk)
        return (pos_lep ? posLepTruthTrk_phi() : negLepTruthTrk_phi());
    SagittaInjector* inject = get_injector(trk_type);
    if (inject) return phi(inject->trk_type() & MuonTrk::TrackType::OrdinaryTrks, pos_lep);
    return 10.;
}
int SagittaBiasAnalyzer::q(int trk_type, bool pos_lep) {
    if (trk_type == MuonTrk::TrackType::Primary)
        return (pos_lep ? posLep_q() : negLep_q());
    else if (trk_type == MuonTrk::TrackType::IDTrk)
        return (pos_lep ? posLepIDTrk_q() : negLepIDTrk_q());
    else if (trk_type == MuonTrk::TrackType::METrk)
        return (pos_lep ? posLepMETrk_q() : negLepMETrk_q());
    else if (is_mc() && trk_type == MuonTrk::TrackType::TruthTrk)
        return (pos_lep ? posLepTruthTrk_q() : negLepTruthTrk_q());
    SagittaInjector* inject = get_injector(trk_type);
    if (inject) return q(inject->trk_type() & MuonTrk::TrackType::OrdinaryTrks, pos_lep);

    return 0;
}
float SagittaBiasAnalyzer::q_over_p(int trk_type, bool pos_lep) {
    return q(trk_type, pos_lep) / (std::cosh(eta(trk_type, pos_lep)) * pt(trk_type, pos_lep) * GeVtoMeV);
}
float SagittaBiasAnalyzer::thetaCS(int trk_type, bool pos_lep) {
    if (trk_type == MuonTrk::TrackType::Primary)
        return pos_lep ? posLep_thetaCS() : negLep_thetaCS();
    else if (trk_type == MuonTrk::TrackType::IDTrk)
        return pos_lep ? posLepIDTrk_thetaCS() : negLepIDTrk_thetaCS();
    else if (trk_type == MuonTrk::TrackType::METrk)
        return pos_lep ? posLepMETrk_thetaCS() : negLepMETrk_thetaCS();
    else if (is_mc() && trk_type == MuonTrk::TrackType::TruthTrk)
        return pos_lep ? posLepTruthTrk_thetaCS() : negLepTruthTrk_thetaCS();
    SagittaInjector* inject = get_injector(trk_type);
    if (inject) return (pos_lep ? inject->pos_lep_CS() : inject->neg_lep_CS()).Theta();

    return 10 * M_PI;
}
float SagittaBiasAnalyzer::phiCS(int trk_type, bool pos_lep) {
    if (trk_type == MuonTrk::TrackType::Primary)
        return pos_lep ? posLep_phiCS() : negLep_phiCS();
    else if (trk_type == MuonTrk::TrackType::IDTrk)
        return pos_lep ? posLepIDTrk_phiCS() : negLepIDTrk_phiCS();
    else if (trk_type == MuonTrk::TrackType::METrk)
        return pos_lep ? posLepMETrk_phiCS() : negLepMETrk_phiCS();
    else if (is_mc() && trk_type == MuonTrk::TrackType::TruthTrk)
        return pos_lep ? posLepTruthTrk_phiCS() : negLepTruthTrk_phiCS();
    SagittaInjector* inject = get_injector(trk_type);
    if (inject) return (pos_lep ? inject->pos_lep_CS() : inject->neg_lep_CS()).Phi();

    return 10 * M_PI;
}
float SagittaBiasAnalyzer::cosThetaCS(int trk_type, bool pos_lep) { return std::cos(thetaCS(trk_type, pos_lep)); }

float SagittaBiasAnalyzer::invariant_m(int trk_type) {
    if (trk_type == MuonTrk::TrackType::Primary)
        return diLep_m() * MeVtoGeV;
    else if (trk_type == MuonTrk::TrackType::IDTrk)
        return diLepIDTrk_m() * MeVtoGeV;
    else if (trk_type == MuonTrk::TrackType::METrk)
        return diLepMETrk_m() * MeVtoGeV;
    else if (is_mc() && trk_type == MuonTrk::TrackType::TruthTrk)
        return diLepTruthTrk_m() * MeVtoGeV;
    SagittaInjector* inject = get_injector(trk_type);
    if (inject && is_valid(trk_type)) return inject->inv_p4().M();
    return -1;
}
bool SagittaBiasAnalyzer::is_valid(int trk_type) { return invariant_m(trk_type & MuonTrk::TrackType::OrdinaryTrks) > 0; }

float SagittaBiasAnalyzer::invariant_pt(int trk_type) {
    if (trk_type == MuonTrk::TrackType::Primary)
        return diLep_pt() * MeVtoGeV;
    else if (trk_type == MuonTrk::TrackType::IDTrk)
        return diLepIDTrk_pt() * MeVtoGeV;
    else if (trk_type == MuonTrk::TrackType::METrk)
        return diLepMETrk_pt() * MeVtoGeV;
    else if (is_mc() && trk_type == MuonTrk::TrackType::TruthTrk)
        return diLepTruthTrk_pt() * MeVtoGeV;
    SagittaInjector* inject = get_injector(trk_type);
    if (inject) return inject->inv_p4().Pt();
    return -1;
}

float SagittaBiasAnalyzer::invariant_Y(int trk_type) {
    if (trk_type == MuonTrk::TrackType::Primary)
        return diLep_pt() * MeVtoGeV;
    else if (trk_type == MuonTrk::TrackType::IDTrk)
        return diLepIDTrk_pt() * MeVtoGeV;
    else if (trk_type == MuonTrk::TrackType::METrk)
        return diLepMETrk_pt() * MeVtoGeV;
    else if (is_mc() && trk_type == MuonTrk::TrackType::TruthTrk)
        return diLepTruthTrk_pt() * MeVtoGeV;
    SagittaInjector* inject = get_injector(trk_type);
    if (inject) return 0.5*std::log((inject->inv_p4().E()+inject->inv_p4().Pz())/(inject->inv_p4().E()-inject->inv_p4().Pz()));
    return -1;
}



float SagittaBiasAnalyzer::invariant_eta(int trk_type) {
    if (trk_type == MuonTrk::TrackType::Primary)
        return diLep_eta();
    else if (trk_type == MuonTrk::TrackType::IDTrk)
        return diLepIDTrk_eta();
    else if (trk_type == MuonTrk::TrackType::METrk)
        return diLepMETrk_eta();
    else if (is_mc() && trk_type == MuonTrk::TrackType::TruthTrk)
        return diLepTruthTrk_eta();
    SagittaInjector* inject = get_injector(trk_type);
    if (inject) return inject->inv_p4().Eta();

    return -10 * M_PI;
}
float SagittaBiasAnalyzer::invariant_phi(int trk_type) {
    if (trk_type == MuonTrk::TrackType::Primary)
        return diLep_phi();
    else if (trk_type == MuonTrk::TrackType::IDTrk)
        return diLepIDTrk_phi();
    else if (trk_type == MuonTrk::TrackType::METrk)
        return diLepMETrk_phi();
    else if (is_mc() && trk_type == MuonTrk::TrackType::TruthTrk)
        return diLepTruthTrk_phi();
    SagittaInjector* inject = get_injector(trk_type);
    if (inject) return inject->inv_p4().Phi();
    return -10 * M_PI;
}
float SagittaBiasAnalyzer::d0_sig(int trk_type, bool pos_lep) {
    if (trk_type & MuonTrk::TrackType::Primary) {
        return pos_lep ? posLep_d0Err() : negLep_d0Err();
    } else if (trk_type & MuonTrk::TrackType::IDTrk) {
        return pos_lep ? posLepIDTrk_d0Err() : negLepIDTrk_d0Err();
    } else if (trk_type & MuonTrk::TrackType::METrk) {
        return pos_lep ? posLepMETrk_d0Err() : negLepMETrk_d0Err();
    }
    return 5.e3;
}
float SagittaBiasAnalyzer::d0(int trk_type, bool pos_lep) {
    if (trk_type & MuonTrk::TrackType::Primary) {
        return pos_lep ? posLep_d0() : negLep_d0();
    } else if (trk_type & MuonTrk::TrackType::IDTrk) {
        return pos_lep ? posLepIDTrk_d0() : negLepIDTrk_d0();
    } else if (trk_type & MuonTrk::TrackType::METrk) {
        return pos_lep ? posLepMETrk_d0() : negLepMETrk_d0();
    }
    return 5.e3;
}
float SagittaBiasAnalyzer::z0(int trk_type, bool pos_lep) {
    if (trk_type & MuonTrk::TrackType::Primary) {
        return pos_lep ? posLep_z0() : negLep_z0();
    } else if (trk_type & MuonTrk::TrackType::IDTrk) {
        return pos_lep ? posLepIDTrk_z0() : negLepIDTrk_z0();
    } else if (trk_type & MuonTrk::TrackType::METrk) {
        return pos_lep ? posLepMETrk_z0() : negLepMETrk_z0();
    }
    return 5.e3;
}

float SagittaBiasAnalyzer::delta_phi(int trk_type) {
    return std::abs(xAOD::P4Helpers::deltaPhi(phi(trk_type, LepCharge::Plus), phi(trk_type, LepCharge::Minus)));
}
float SagittaBiasAnalyzer::delta_z0(int trk_type) { return std::abs(z0(trk_type, LepCharge::Plus) - z0(trk_type, LepCharge::Minus)); }
float SagittaBiasAnalyzer::delta_d0(int trk_type) { return std::abs(d0(trk_type, LepCharge::Plus) - d0(trk_type, LepCharge::Minus)); }
float SagittaBiasAnalyzer::theta(int trk_type, bool pos_lep) { return 2. * std::atan(std::exp(-eta(trk_type, pos_lep))); }
float SagittaBiasAnalyzer::sinTheta(int trk_type, bool pos_lep) {
    const float lep_eta = eta(trk_type, pos_lep);
    return (lep_eta > 0. ? 1. : -1.) / std::cosh(lep_eta);
}

bool SagittaBiasAnalyzer::pass_base_kine_sel(int trk_type, const std::pair<float, float>& mass_window) {
    const float inv_m = invariant_m(trk_type);
    if (inv_m < 0) return false;
    if (mass_window.first < mass_window.second && (mass_window.first > inv_m || inv_m > mass_window.second)) return false;
    const float pos_eta = std::abs(eta(trk_type, LepCharge::Plus));
    const float neg_eta = std::abs(eta(trk_type, LepCharge::Minus));
    /// Exclude any lepton within the region where the muon spectrometer is partially instrumented
    if (std::min(pos_eta, neg_eta) < 0.1) return false;

    /// Exclude events where one of the candidates is outside the ID acceptance
    if (std::max(pos_eta, neg_eta) > 2.5) return false;
    /// Apply cuts ID quality cuts for the ID and primary track
    if (trk_type & (MuonTrk::TrackType::IDTrk | MuonTrk::TrackType::Primary)) {
        if (!negLepIDTrk_passIdCuts() || !posLepIDTrk_passIdCuts()) return false;
    }
    /// Cut on pT/m to select low pT region
    if (invariant_pt(trk_type)/inv_m > 1./6.) return false;
    /// Determine the trigger selection according to the
    /// closeness of the invariant di-lepton mass to the
    /// mass of the three resonances
    const float d_Z = std::abs(inv_m - Z_Mass);
    const float d_Jpsi = std::abs(inv_m - JPsi_Mass);
    const float d_Upsilon = std::abs(inv_m - Upsilon_Mass);

    /// The event has the most similarity to a Z candidate event
    if (d_Z < d_Jpsi && d_Z < d_Upsilon) {
        /// D0 significance cut of 3
        if (std::max(std::abs(d0_sig(trk_type, LepCharge::Plus)), std::abs(d0_sig(trk_type, LepCharge::Minus))) > 3.) return false;
        if (std::max(std::abs(z0(trk_type, LepCharge::Plus) * sinTheta(trk_type, LepCharge::Plus)),
                     std::abs(z0(trk_type, LepCharge::Minus) * sinTheta(trk_type, LepCharge::Minus))) > 0.5)
            return false;
    }
    /// The event is on the J psi resonance
    else if (d_Jpsi < d_Upsilon && d_Jpsi < d_Z) {}
    return trig_decision();
}

/// The random run and random lumi block numbers are saved together in the n-tuples.
int SagittaBiasAnalyzer::run_number() { return has_random_run_number() ? randomRunNumber() : runNumber(); }
int SagittaBiasAnalyzer::lumi_block_number() { return has_random_run_number() ? randomLumiBlockNumber() : lumiblock(); }
int SagittaBiasAnalyzer::period_year() {
    const int run_num = run_number();
    if (is_mc() && !has_random_run_number()) {
        if (TriggerHelper::is_mc16a(run_num)) return data_period::data16;
        if (TriggerHelper::is_mc16d(run_num)) return data_period::data17;
        if (TriggerHelper::is_mc16e(run_num)) return data_period::data18;
    }
    return TriggerHelper::get_period_year(run_num);
}

SagittaBiasAnalyzer::TrigSelVector& SagittaBiasAnalyzer::potential_triggers() {
    constexpr int trk_type = MuonTrk::TrackType::Primary;
    const float inv_m = invariant_m(trk_type);
    const float d_Z = std::abs(inv_m - Z_Mass);
    const float d_Jpsi = std::abs(inv_m - JPsi_Mass);
    const float d_Upsilon = std::abs(inv_m - Upsilon_Mass);
    const int year = period_year();

    /// Z boson stream
    if (d_Z < d_Jpsi && d_Z < d_Upsilon) {
        for (auto& trig_itr : m_trigs_z) {
            if ((trig_itr.first & year) == year) return trig_itr.second;
        }
    }
    /// Jpsi stream
    else if (d_Jpsi < d_Upsilon && d_Jpsi < d_Z) {
        for (auto& trig_itr : m_trigs_jpsi) {
            if ((trig_itr.first & year) == year) return trig_itr.second;
        }
    }
    static SagittaBiasAnalyzer::TrigSelVector dummy_vector{};
    return dummy_vector;
}

double SagittaBiasAnalyzer::sin_theta_rho_2(int trk_type, bool tree_m, bool massive_mu) {
    // Set initial value for local variable:
    double my_theta2_val = 0.0;

    // Create zero-filled 4-vectors as objects of the TLorentsVector class (already imported):
    using FourVector = CollinsSoper::PolarVector4D;
    FourVector p_plus{pt(trk_type, LepCharge::Plus), eta(trk_type, LepCharge::Plus), phi(trk_type, LepCharge::Plus),
                      massive_mu * muon_mass},
        p_minus{pt(trk_type, LepCharge::Minus), eta(trk_type, LepCharge::Minus), phi(trk_type, LepCharge::Minus), massive_mu * muon_mass};

    // Write formula the defines my_theta2_val:
    const double scaling = p_minus.E() / p_plus.E();
    my_theta2_val =
        (scaling * p_plus.Perp2() + p_minus.Perp2() / scaling - 2. * (p_plus.Vect().Dot(p_minus.Vect()) - p_plus.Pz() * p_minus.Pz())) /
        std::pow(tree_m ? invariant_m(trk_type) : (p_plus + p_minus).M(), 2);

    // some checks: (by Johannes)
    if (false && std::abs(my_theta2_val - sin_theta_rho_2_alpha(trk_type)) > 1.e-2) {
        std::cout << "Stonjek Stonjek Stonjek" << std::endl;
        std::cout << "Positive lepton:  " << p_plus.Pt() << " eta: " << p_plus.Eta() << " phi: " << p_plus.Phi() << std::endl;
        std::cout << "Negative lepton: " << p_minus.Pt() << "  eta: " << p_minus.Eta() << " phi: " << p_minus.Phi() << std::endl;
        std::cout << "Sin theta^{2}_{rho}: " << my_theta2_val << " alternative approach: " << sin_theta_rho_2_alpha(trk_type) << std::endl;
        std::cout << "Ingredients: " << std::endl;
        std::cout << "    --- scaling: " << scaling << std::endl;
        std::cout << "     --- scaling*p_plus: " << scaling * p_plus.Perp2() << std::endl;
        std::cout << "     --- p_minus / scaling: " << p_minus.Perp2() / scaling << std::endl;
        std::cout << "     --- dot Product " << p_plus.Vect().Dot(p_minus.Vect()) << std::endl;
        std::cout << "     --- alpha Carolina: " << std::cos(alpha(trk_type)) << std::endl;
        std::cout << "     --- alpha ROOT: " << ROOT::Math::VectorUtil::CosTheta(p_plus, p_minus) << std::endl;
        std::cout << "     --- invariant mass Tree: " << invariant_m(trk_type) << std::endl;
        std::cout << "     --- invariant mass TVect: " << (p_plus + p_minus).M() << std::endl;
        throw std::runtime_error("Stonjek");
    }
    return my_theta2_val;
}
double SagittaBiasAnalyzer::invariant_m_mu_mass(int trk_type, bool massive_mu) {
    using FourVector = CollinsSoper::PolarVector4D;
    FourVector p_plus{pt(trk_type, LepCharge::Plus), eta(trk_type, LepCharge::Plus), phi(trk_type, LepCharge::Plus),
                      massive_mu * muon_mass},
        p_minus{pt(trk_type, LepCharge::Minus), eta(trk_type, LepCharge::Minus), phi(trk_type, LepCharge::Minus), massive_mu * muon_mass};
    return (p_plus + p_minus).M();
}

double SagittaBiasAnalyzer::alpha(int trk_type) {
    double my_alpha = 0.0;

    const double theta_plus = theta(trk_type, LepCharge::Plus);
    const double theta_minus = theta(trk_type, LepCharge::Minus);
    const double dot_product =
        std::sin(theta_plus) * std::sin(theta_minus) * std::cos(delta_phi(trk_type)) + std::cos(theta_plus) * std::cos(theta_minus);
    my_alpha = std::acos(std::min(1., std::max(-1., dot_product)));  //.f tells to interprete as a float
    return my_alpha;
}

double SagittaBiasAnalyzer::sin_theta_rho_2_alpha(int trk_type) {
    double my_sin2_alpha = 0.0;
    const double sin_theta_plus = std::sin(theta(trk_type, LepCharge::Plus));
    const double sin_theta_minus = std::sin(theta(trk_type, LepCharge::Minus));
    const double cos_delta_phi = std::cos(delta_phi(trk_type));
    const double cos_alpha = std::cos(alpha(trk_type));
    my_sin2_alpha = (std::pow(sin_theta_plus, 2) + std::pow(sin_theta_minus, 2) - 2. * sin_theta_plus * sin_theta_minus * cos_delta_phi) /
                    (2. * (1. - cos_alpha));

    return my_sin2_alpha;
}

const CollinsSoper::Vector4D& SagittaBiasAnalyzer::lep_p4_pseudo_CS(int trk_type, bool pos_lep) {
    const int key = pos_lep == LepCharge::Plus ? trk_type : -trk_type;
    std::map<int, std::unique_ptr<CollinsSoper::PseudoCollinsSoperFrame>>::iterator itr = m_pseud_cs.find(key);
    if (itr == m_pseud_cs.end()) {
        m_pseud_cs[key] = std::make_unique<CollinsSoper::PseudoCollinsSoperFrame>(*this, trk_type, pos_lep);
        return lep_p4_pseudo_CS(trk_type, pos_lep);
    }
    return itr->second->neg_lep_CS();
}

unsigned short SagittaBiasAnalyzer::lep_quality(bool pos_lep) { return pos_lep ? posLep_quality() : negLep_quality(); }

bool SagittaBiasAnalyzer::pass_id_wp(SagittaBiasAnalyzer::MuonWP wp, bool pos_lep) {
    if (wp == MuonWP::Loose) return lep_quality(pos_lep) <= xAOD::Muon::Loose;
    if (wp == MuonWP::Medium) return lep_quality(pos_lep) <= xAOD::Muon::Medium;
    if (wp == MuonWP::Tight) return lep_quality(pos_lep) <= xAOD::Muon::Tight;
    if (wp == MuonWP::LowPt) return pos_lep ? posLep_isLowPt() : negLep_isLowPt();
    if (wp == MuonWP::HighPt) return pos_lep ? posLep_isHighPt() : negLep_isHighPt();
    return false;
}

double SagittaBiasAnalyzer::err_q_over_p(int trk_type, bool pos_lep) {
    if (trk_type == MuonTrk::TrackType::IDTrk) {
        return std::sqrt(std::abs(pos_lep ? posLepIDTrk_cov_qOverP() : negLepIDTrk_cov_qOverP()));
    } else if (trk_type == MuonTrk::TrackType::METrk) {
    }
    return FLT_MAX;
}

unsigned char SagittaBiasAnalyzer::numberOfTRTHits(int trk_type, bool pos_lep) {
    if (trk_type == MuonTrk::TrackType::IDTrk) { return pos_lep ? posLepIDTrk_numberOfTRTHits() : negLepIDTrk_numberOfTRTHits(); }
    return 255;
}
unsigned char SagittaBiasAnalyzer::numberOfTRTOutliers(int trk_type, bool pos_lep) {
    if (trk_type == MuonTrk::TrackType::IDTrk) { return pos_lep ? posLepIDTrk_numberOfTRTOutliers() : negLepIDTrk_numberOfTRTOutliers(); }
    return 255;
}

///
///
///

SagittaBiasAnalyzer::TrigSelPtr SagittaBiasAnalyzer::SINGLE_TRIGGER(const std::string& trig_item, const float match_thresh) {
    TrigSelVector::iterator itr =
        std::find_if(m_def_trigs.begin(), m_def_trigs.end(), [&trig_item](const TrigSelPtr& trig) { return trig->trigger() == trig_item; });
    if (itr == m_def_trigs.end()) {
        TrigSelPtr ptr = std::make_shared<TrigSelectionBranch>(this, trig_item, match_thresh);
        m_def_trigs.push_back(ptr);
        return ptr;
    }
    if (std::abs((*itr)->low_threshold() - match_thresh) > FLT_EPSILON || !(*itr)->is_single_lepton()) {
        throw std::runtime_error("SINGLE_TRIGGER() -- " + trig_item + " has already been setup with " +
                                 std::to_string((*itr)->low_threshold()) + " -- " + std::to_string((*itr)->high_threshold()) +
                                 " but this time " + std::to_string(match_thresh) + " is requested. Please check");
    }
    return (*itr);
}

SagittaBiasAnalyzer::TrigSelPtr SagittaBiasAnalyzer::DILEP_TRIGGER(const std::string& trig_item, const float low_thresh,
                                                                   const float high_thresh) {
    TrigSelVector::iterator itr =
        std::find_if(m_def_trigs.begin(), m_def_trigs.end(), [&trig_item](const TrigSelPtr& trig) { return trig->trigger() == trig_item; });
    if (itr == m_def_trigs.end()) {
        TrigSelPtr ptr = std::make_shared<TrigSelectionBranch>(this, trig_item, low_thresh, high_thresh);
        m_def_trigs.push_back(ptr);
        return ptr;
    }
    if (std::abs((*itr)->low_threshold() - low_thresh) > FLT_EPSILON || std::abs((*itr)->high_threshold() - high_thresh) > FLT_EPSILON ||

        (*itr)->is_single_lepton()) {
        throw std::runtime_error("DILEP_TRIGGER() -- " + trig_item + " has already been setup with " +
                                 std::to_string((*itr)->low_threshold()) + " -- " + std::to_string((*itr)->high_threshold()) +
                                 " but this time " + std::to_string(low_thresh) + "-" + std::to_string(high_thresh) +
                                 " is requested. Please check");
    }
    return (*itr);
}

void SagittaBiasAnalyzer::setup_z_triggers() {
    std::map<int, TrigSelVector>& trig_map = m_trigs_z;

    constexpr int period_15 = data_period::data15;
    trig_map[period_15] += SINGLE_TRIGGER("HLT_mu20_iloose_L1MU15", trigmatch_thresh_mu20);
    trig_map[period_15] += DILEP_TRIGGER("HLT_mu22_mu8noL1", trigmatch_thresh_mu8, trigmatch_thresh_mu22);
    trig_map[period_15] += DILEP_TRIGGER("HLT_2mu10", trigmatch_thresh_mu10, trigmatch_thresh_mu10);
    trig_map[period_15] += SINGLE_TRIGGER("HLT_mu40", trigmatch_thresh_mu40);
    trig_map[period_15] += DILEP_TRIGGER("HLT_2mu14", trigmatch_thresh_mu14, trigmatch_thresh_mu14);
    trig_map[period_15] += SINGLE_TRIGGER("HLT_mu50", trigmatch_thresh_mu50);

    constexpr int period16AD =
        data_period::data16 & (data_period::periodA | data_period::periodB | data_period::periodC | data_period::periodD);
    constexpr int period16DE = (data_period::data16 & (~period16AD)) | data_period::year16;
    /// HLT_mu24_ivarmedium

    /// constexpr int last_run_16D3 = 302919;
    /// constexpr int first_run_16D4 = 302956;
    trig_map[period16AD] += DILEP_TRIGGER("HLT_mu22_mu8noL1", trigmatch_thresh_mu8, trigmatch_thresh_mu22);
    trig_map[period16DE] += DILEP_TRIGGER("HLT_mu22_mu8noL1", trigmatch_thresh_mu8, trigmatch_thresh_mu22);

    trig_map[period16AD] += DILEP_TRIGGER("HLT_2mu14", trigmatch_thresh_mu14, trigmatch_thresh_mu14);
    trig_map[period16DE] += DILEP_TRIGGER("HLT_2mu14", trigmatch_thresh_mu14, trigmatch_thresh_mu14);

    trig_map[period16AD] += SINGLE_TRIGGER("HLT_mu24_ivarmedium", trigmatch_thresh_mu24);
    trig_map[period16DE] += SINGLE_TRIGGER("HLT_mu26_ivarmedium", trigmatch_thresh_mu26);

    trig_map[period16AD] += SINGLE_TRIGGER("HLT_mu50", trigmatch_thresh_mu50);
    trig_map[period16DE] += SINGLE_TRIGGER("HLT_mu50", trigmatch_thresh_mu50);

    constexpr int period1718 = data_period::data17 | data_period::data18;
    // Single muon trigger does not change for the other years
    trig_map[period1718] += SINGLE_TRIGGER("HLT_mu26_ivarmedium", trigmatch_thresh_mu26);
    trig_map[period1718] += SINGLE_TRIGGER("HLT_mu50", trigmatch_thresh_mu50);
    trig_map[period1718] += DILEP_TRIGGER("HLT_mu22_mu8noL1", trigmatch_thresh_mu8, trigmatch_thresh_mu22);
    trig_map[period1718] += DILEP_TRIGGER("HLT_2mu14", trigmatch_thresh_mu14, trigmatch_thresh_mu14);

    for (auto& trig_itr : trig_map) {
        std::sort(trig_itr.second.begin(), trig_itr.second.end(),
                  [](const TrigSelPtr& a, const TrigSelPtr& b) { return a->id() < b->id(); });
    }
}

void SagittaBiasAnalyzer::setup_jpsi_triggers() {
    std::map<int, TrigSelVector>& trig_map = m_trigs_jpsi;
    constexpr int period_15 = data_period::data15;

    /// Average prescale of 1.93. Think about rescaling the data?
    trig_map[period_15] += DILEP_TRIGGER("HLT_mu6_mu4_bJpsimumu", trigmatch_thresh_mu4, trigmatch_thresh_mu6);
    /// Average prescale of 2.72.
    trig_map[period_15] += DILEP_TRIGGER("HLT_2mu4_bJpsimumu", trigmatch_thresh_mu4, trigmatch_thresh_mu4);
    /// Almost never prescaled
    trig_map[period_15] += DILEP_TRIGGER("HLT_mu20_msonly_mu6noL1_msonly_nscan05", trigmatch_thresh_mu6, trigmatch_thresh_mu20);
    /// Almost never prescaled
    trig_map[period_15] += DILEP_TRIGGER("HLT_2mu10", trigmatch_thresh_mu10, trigmatch_thresh_mu10);
    /// Do not consider HLT_mu20_2mu0noL1_JpsimumuFS as this one requires a 3 muon event
    ///  This trigger has an average prescale of 55.58.. We need to investigate the changes in the spectrum
    trig_map[period_15] += DILEP_TRIGGER("HLT_mu4_mu4_idperf_bJpsimumu_noid", trigmatch_thresh_mu4, trigmatch_thresh_mu4);
    /// Do not consider HLT_mu20_2mu4_JpsimumuL2 as this one is a tri-lepton trigger
    /// This trigger might generate peaks in the jpsi muon spectrum
    trig_map[period_15] += SINGLE_TRIGGER("HLT_mu20_iloose_L1MU15", trigmatch_thresh_mu20);
    trig_map[period_15] += DILEP_TRIGGER("HLT_2mu14", trigmatch_thresh_mu14, trigmatch_thresh_mu14);
    /// Extremely large prescale of 270
    trig_map[period_15] += SINGLE_TRIGGER("HLT_mu4_bJpsi_Trkloose", trigmatch_thresh_mu4);

    constexpr int period_16 = data_period::data16;
    /// Prescale essentially 1.
    trig_map[period_16] += DILEP_TRIGGER("HLT_mu10_mu6_bJpsimumu_delayed", trigmatch_thresh_mu6, trigmatch_thresh_mu10);
    /// Prescale of 15 on average
    trig_map[period_16] += DILEP_TRIGGER("HLT_mu6_mu4_bJpsimumu_Lxy0_delayed", trigmatch_thresh_mu4, trigmatch_thresh_mu6);
    /// How is a prescale of 0.86 possible?
    trig_map[period_16] += DILEP_TRIGGER("HLT_mu6_mu4_bBmumuxv2_delayed", trigmatch_thresh_mu4, trigmatch_thresh_mu6);
    /// Prescaling is onish
    trig_map[period_16] += DILEP_TRIGGER("HLT_mu10_mu6_bBmumuxv2_delayed", trigmatch_thresh_mu6, trigmatch_thresh_mu10);
    /// Prescale on averge 16
    trig_map[period_16] += DILEP_TRIGGER("HLT_mu6_mu4_bJpsimumu_L1BPH_dash_2M8_dash_MU6MU4_BPH_dash_0DR15_dash_MU6MU4",
                                         trigmatch_thresh_mu4, trigmatch_thresh_mu6);
    /// Prescale on avergage 15
    trig_map[period_16] += DILEP_TRIGGER("HLT_mu6_mu4_bJpsimumu_delayed", trigmatch_thresh_mu4, trigmatch_thresh_mu6);
    /// Prescale on avergage 2.6
    trig_map[period_16] += DILEP_TRIGGER("HLT_mu6_mu4_bJpsimumu_Lxy0_delayed_L1BPH_dash_2M8_dash_MU6MU4_BPH_dash_0DR15_dash_MU6MU4",
                                         trigmatch_thresh_mu4, trigmatch_thresh_mu6);
    /// Do not consider HLT_mu6_mu4_bJpsimumu_L12MU4_dash_B as this has a prescale of 1k
    /// Prescale on average 3.7

    trig_map[period_16] +=
        DILEP_TRIGGER("HLT_2mu6_bJpsimumu_L1BPH_dash_2M9_dash_2MU6_BPH_dash_2DR15_dash_2MU6", trigmatch_thresh_mu6, trigmatch_thresh_mu6);
    /// Prescale on average 23
    trig_map[period_16] += DILEP_TRIGGER("HLT_mu6_mu4_bJpsimumu", trigmatch_thresh_mu4, trigmatch_thresh_mu6);
    /// Prescale of 5.02258
    trig_map[period_16] += DILEP_TRIGGER("HLT_mu10_mu6_bJpsimumu", trigmatch_thresh_mu6, trigmatch_thresh_mu10);
    /// How?
    ///  DILEP_TRIGGER(HLT_mu20_2mu0noL1_JpsimumuFS, trigmatch_thresh_mu6,trigmatch_thresh_mu10);
    trig_map[period_16] += DILEP_TRIGGER("HLT_mu20_nomucomb_mu6noL1_nscan03", trigmatch_thresh_mu6, trigmatch_thresh_mu20);
    /// Skip 3-lepton trigger HLT_mu20_2mu4_JpsimumuL2
    trig_map[period_16] += DILEP_TRIGGER("HLT_mu22_mu8noL1", trigmatch_thresh_mu8, trigmatch_thresh_mu22);

    trig_map[period_16] += DILEP_TRIGGER("HLT_mu6_mu4_bJpsimumu_delayed_L1BPH_dash_2M8_dash_MU6MU4_BPH_dash_0DR15_dash_MU6MU4",
                                         trigmatch_thresh_mu4, trigmatch_thresh_mu6);
    trig_map[period_16] += DILEP_TRIGGER("HLT_mu6_mu4_bBmumux_BsmumuPhi_delayed_L1BPH_dash_2M8_dash_MU6MU4_BPH_dash_0DR15_dash_MU6MU4",
                                         trigmatch_thresh_mu4, trigmatch_thresh_mu6);
    trig_map[period_16] += DILEP_TRIGGER("HLT_2mu14", trigmatch_thresh_mu14, trigmatch_thresh_mu14);
    /// Precale of 70 on average.. Could be dangerous
    trig_map[period_16] += DILEP_TRIGGER("HLT_mu4_mu4_idperf_bJpsimumu_noid", trigmatch_thresh_mu4, trigmatch_thresh_mu4);

    constexpr int period_17 = data_period::data17;
    /// Prescale roughly 1
    trig_map[period_17] +=
        DILEP_TRIGGER("HLT_2mu6_bJpsimumu_L1BPH_dash_2M9_dash_2MU6_BPH_dash_2DR15_dash_2MU6", trigmatch_thresh_mu6, trigmatch_thresh_mu6);

    trig_map[period_17] += DILEP_TRIGGER("HLT_2mu6_bJpsimumu_Lxy0_L1BPH_dash_2M9_dash_2MU6_BPH_dash_2DR15_dash_2MU6", trigmatch_thresh_mu6,
                                         trigmatch_thresh_mu6);
    trig_map[period_17] += DILEP_TRIGGER("HLT_mu11_mu6_bDimu", trigmatch_thresh_mu6, trigmatch_thresh_mu11);

    trig_map[period_17] += DILEP_TRIGGER("HLT_mu11_mu6_bJpsimumu", trigmatch_thresh_mu6, trigmatch_thresh_mu11);

    trig_map[period_17] += DILEP_TRIGGER("HLT_mu11_mu6_bDimu_Lxy0", trigmatch_thresh_mu6, trigmatch_thresh_mu11);

    trig_map[period_17] += DILEP_TRIGGER("HLT_mu11_mu6_bJpsimumu_Lxy0", trigmatch_thresh_mu6, trigmatch_thresh_mu11);

    trig_map[period_17] += DILEP_TRIGGER("HLT_2mu6_bBmumux_BpmumuKp_L1BPH_dash_2M9_dash_2MU6_BPH_dash_2DR15_dash_2MU6",
                                         trigmatch_thresh_mu6, trigmatch_thresh_mu6);

    trig_map[period_17] += DILEP_TRIGGER("HLT_mu11_mu6_bBmumuxv2", trigmatch_thresh_mu6, trigmatch_thresh_mu11);

    trig_map[period_17] += DILEP_TRIGGER("HLT_mu11_mu6_bBmumux_BpmumuKp", trigmatch_thresh_mu6, trigmatch_thresh_mu11);

    /// Do not consider HLT_mu20_2mu4_JpsimumuL2 as this one requires three leptons
    /// Same holds for HLT_mu20_2mu2noL1_JpsimumuFS
    trig_map[period_17] += DILEP_TRIGGER("HLT_mu22_mu8noL1", trigmatch_thresh_mu8, trigmatch_thresh_mu22);
    /// Do not use HLT_mu11_2mu4noL1_bNocut_L1MU11_2MU6 as this is a three lepton trigger
    /// Trigger having a prescale of 100
    trig_map[period_17] += DILEP_TRIGGER("HLT_mu4_mu4_idperf_bJpsimumu_noid", trigmatch_thresh_mu4, trigmatch_thresh_mu4);
    trig_map[period_17] += DILEP_TRIGGER("HLT_mu6_mu4_bBmumux_BsmumuPhi_L1BPH_dash_2M9_dash_MU6MU4_BPH_dash_0DR15_dash_MU6MU4",
                                         trigmatch_thresh_mu4, trigmatch_thresh_mu6);

    trig_map[period_17] += DILEP_TRIGGER("HLT_2mu14", trigmatch_thresh_mu14, trigmatch_thresh_mu14);

    constexpr int period_18 = data_period::data18;

    trig_map[period_18] +=
        DILEP_TRIGGER("HLT_2mu6_bJpsimumu_L1BPH_dash_2M9_dash_2MU6_BPH_dash_2DR15_dash_2MU6", trigmatch_thresh_mu6, trigmatch_thresh_mu6);
    /// Prescale of 1.
    trig_map[period_18] += DILEP_TRIGGER("HLT_2mu6_bJpsimumu_Lxy0_L1BPH_dash_2M9_dash_2MU6_BPH_dash_2DR15_dash_2MU6", trigmatch_thresh_mu6,
                                         trigmatch_thresh_mu6);
    /// Prescale of 10...
    trig_map[period_18] += DILEP_TRIGGER("HLT_mu6_mu4_bJpsimumu_Lxy0_L1BPH_dash_2M9_dash_MU6MU4_BPH_dash_0DR15_dash_MU6MU4",
                                         trigmatch_thresh_mu4, trigmatch_thresh_mu6);
    /// Prescale of 1.
    trig_map[period_18] += DILEP_TRIGGER("HLT_mu11_mu6_bDimu", trigmatch_thresh_mu6, trigmatch_thresh_mu11);
    trig_map[period_18] += DILEP_TRIGGER("HLT_mu11_mu6_bDimu_L1LFV_dash_MU11", trigmatch_thresh_mu6, trigmatch_thresh_mu11);
    trig_map[period_18] += DILEP_TRIGGER("HLT_mu11_mu6_bJpsimumu", trigmatch_thresh_mu6, trigmatch_thresh_mu11);
    trig_map[period_18] += DILEP_TRIGGER("HLT_mu11_mu6_bJpsimumu_L1LFV_dash_MU11", trigmatch_thresh_mu6, trigmatch_thresh_mu11);

    trig_map[period_18] += DILEP_TRIGGER("HLT_2mu6_bBmumux_BpmumuKp_L1BPH_dash_2M9_dash_2MU6_BPH_dash_2DR15_dash_2MU6",
                                         trigmatch_thresh_mu6, trigmatch_thresh_mu6);
    trig_map[period_18] += DILEP_TRIGGER("HLT_mu11_mu6_bDimu_Lxy0", trigmatch_thresh_mu6, trigmatch_thresh_mu11);
    trig_map[period_18] += DILEP_TRIGGER("HLT_mu11_mu6_bJpsimumu_Lxy0", trigmatch_thresh_mu6, trigmatch_thresh_mu11);
    trig_map[period_18] += DILEP_TRIGGER("HLT_mu11_mu6_bDimu_Lxy0_L1LFV_dash_MU11", trigmatch_thresh_mu6, trigmatch_thresh_mu11);
    trig_map[period_18] += DILEP_TRIGGER("HLT_mu11_mu6_bJpsimumu_Lxy0_L1LFV_dash_MU11", trigmatch_thresh_mu6, trigmatch_thresh_mu11);
    trig_map[period_18] += DILEP_TRIGGER("HLT_mu11_mu6_bBmumuxv2", trigmatch_thresh_mu6, trigmatch_thresh_mu11);
    trig_map[period_18] += DILEP_TRIGGER("HLT_mu11_mu6_bBmumuxv2_L1LFV_dash_MU11", trigmatch_thresh_mu6, trigmatch_thresh_mu11);
    trig_map[period_18] += DILEP_TRIGGER("HLT_mu11_mu6_bBmumux_BpmumuKp", trigmatch_thresh_mu6, trigmatch_thresh_mu11);
    trig_map[period_18] += DILEP_TRIGGER("HLT_mu11_mu6_bBmumux_BpmumuKp_L1LFV_dash_MU11", trigmatch_thresh_mu6, trigmatch_thresh_mu11);
    // HLT_mu20_2mu4_JpsimumuL2
    // HLT_mu20_2mu2noL1_JpsimumuFS
    trig_map[period_18] += DILEP_TRIGGER("HLT_mu6_mu4_bJpsimumu_L1BPH_dash_2M9_dash_MU6MU4_BPH_dash_0DR15_dash_MU6MU4",
                                         trigmatch_thresh_mu4, trigmatch_thresh_mu6);
    trig_map[period_18] += DILEP_TRIGGER("HLT_mu22_mu8noL1", trigmatch_thresh_mu8, trigmatch_thresh_mu22);
    trig_map[period_18] += DILEP_TRIGGER("HLT_2mu14", trigmatch_thresh_mu14, trigmatch_thresh_mu14);
    trig_map[period_18] += DILEP_TRIGGER("HLT_mu6_mu4_bBmumux_BsmumuPhi_L1BPH_dash_2M9_dash_MU6MU4_BPH_dash_0DR15_dash_MU6MU4",
                                         trigmatch_thresh_mu4, trigmatch_thresh_mu6);
    for (auto& trig_itr : trig_map) {
        std::sort(trig_itr.second.begin(), trig_itr.second.end(),
                  [](const TrigSelPtr& a, const TrigSelPtr& b) { return a->id() < b->id(); });
    }
}
