#include <TCanvas.h>
#include <TF1.h>
#include <TFile.h>
#include <TGraphErrors.h>
#include <TH1F.h>
#include <TString.h>

// minuit
#include <iomanip>
#include <iostream>

#include "Math/Factory.h"
#include "Math/Functor.h"
#include "Math/Minimizer.h"
#include "TError.h"
#include "TRandom2.h"

//#include "AtlasStyle.h"

using namespace std;

const int etab = 17;  // eta bins
const int mb = 53;    // mass bins
const int order = 4;  // order of the interpolation

// mass bins range
int mbmin = 18;  // 9; //0; //20;
int mbmax = 28;  // 38;//52; //40;

// eta bins
// double etabins[etab+1] = {-2.5, -2.0, -1.05, -0.1, 0, 0.1, 1.05, 1.3, 2.0, 2.5};
double etabins[etab + 1] = {-2.5, -2.0, -1.3, -1.05, -0.9, -0.7, -0.5, -0.3, -0.1, 0.1, 0.3, 0.5, 0.7, 0.9, 1.05, 1.3, 2.0, 2.5};

// double biasmap[etab] = {0.051, 0.071, 0.031, 0.031, 0.031, 0.031, 0.031, 0.071, 0.051};
double biasmap[etab] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};

// int nbs = 105;
// double biases[nbs+1] = {-0.105, -0.103, -0.101, -0.099, -0.097, -0.095, -0.093, -0.091, -0.089, -0.087, -0.085, -0.083, -0.081, -0.079,
// -0.077, -0.075, -0.073, -0.071, -0.069, -0.067, -0.065, -0.063, -0.061, -0.059, -0.057, -0.055, -0.053, -0.051, -0.049, -0.047, -0.045,
// -0.043, -0.041, -0.039, -0.037, -0.035, -0.033, -0.031, -0.029, -0.027, -0.025, -0.023, -0.021, -0.019, -0.017, -0.015, -0.013, -0.011,
// -0.009, -0.007, -0.005, -0.003, -0.001, 0.001, 0.003, 0.005, 0.007, 0.009, 0.011, 0.013, 0.015, 0.017, 0.019, 0.021, 0.023, 0.025, 0.027,
// 0.029, 0.031, 0.033, 0.035, 0.037, 0.039, 0.041, 0.043, 0.045, 0.047, 0.049, 0.051, 0.053, 0.055, 0.057, 0.059, 0.061, 0.063, 0.065,
// 0.067, 0.069, 0.071, 0.073, 0.075, 0.077, 0.079, 0.081, 0.083, 0.085, 0.087, 0.089, 0.091, 0.093, 0.095, 0.097, 0.099, 0.101, 0.103,
// 0.105};

const int nbs = 42;
double biases[nbs + 1] = {-0.21, -0.20, -0.19, -0.18, -0.17, -0.16, -0.15, -0.14, -0.13, -0.12, -0.11, -0.10, -0.09, -0.08, -0.07,
                          -0.06, -0.05, -0.04, -0.03, -0.02, -0.01, 0.00,  0.01,  0.02,  0.03,  0.04,  0.05,  0.06,  0.07,  0.08,
                          0.09,  0.10,  0.11,  0.12,  0.13,  0.14,  0.15,  0.16,  0.17,  0.18,  0.19,  0.20,  0.21};

//(pseudo)data
double dat[etab][mb];
double err[etab][mb];

class poly {
private:
    vector<double> pars;

public:
    poly(){};
    poly(vector<double> ps) : pars(ps){};

    double eval(double c) {
        double f = 0;
        double pow = 1.;
        for (unsigned int p = 0; p < pars.size(); p++) {
            f += pars[p] * pow;
            pow *= c;
        }

        return f;
    }

    // double eval(double c1, double c2)
    //{
    //  double c = sqrt(c1*c1+c2*c2)/sqrt(2.);
    //  return eval(c);
    //}
};

// interpolatory polynomials
poly deltapolys[etab][mb];

// chi-square function to be minimised
double chi2(const double *di) {
    double c2 = 0.;

    for (int ieta = 0; ieta < etab; ieta++) {
        if (ieta == 8) continue;

        for (int im = mbmin; im <= mbmax; im++) {
            if (deltapolys[ieta][im].eval(di[ieta]) != deltapolys[ieta][im].eval(di[ieta])) {
                cout << "nan in deltapolys " << endl;
                continue;
            }
            if (dat[ieta][im] != dat[ieta][im]) {
                cout << "nan in data " << endl;
                continue;
            }
            if (err[ieta][im] != err[ieta][im]) {
                cout << "nan in err " << endl;
                continue;
            }

            if (err[ieta][im] != 0) c2 += pow(deltapolys[ieta][im].eval(di[ieta]) - dat[ieta][im], 2) / pow(err[ieta][im], 2);
        }
    }
    return c2;
}

// Build a TGraph storing bin content as a function of the injected bias, to be interpolated
TGraphErrors *maketgraph(TFile *f, double etal, double etau, int mbin) {
    char stretal[10], stretau[10];
    sprintf(stretal, "%.2f", etal);
    sprintf(stretau, "%.2f", etau);

    TString gname = Form("m%d_eta[%s_%s]", mbin, stretal, stretau);

    TGraphErrors *g = new TGraphErrors();
    g->SetName(gname);

    for (int ibias = 0; ibias < nbs; ibias++) {
        char strbias[10];
        sprintf(strbias, "%.6f", biases[ibias]);

        TString nameTemplneg =
            Form("ID/PseudoMass/All__NegLepEta/KinSel_etaMuMinus_inRANGE_%s_%s/MLL_61_121/Template_BIAS_%s", stretal, stretau, strbias);
        TString nameTemplpos =
            Form("ID/PseudoMass/All__PosLepEta/KinSel_etaMuPlus_inRANGE_%s_%s/MLL_61_121/Template_BIAS_%s", stretal, stretau, strbias);
        // cout << "nameTempl " << nameTempl << endl;

        TH1D *hn = (TH1D *)f->Get(nameTemplneg);
        TH1D *hp = (TH1D *)f->Get(nameTemplpos);
        double valy = (hp->GetBinContent(mbin) - hn->GetBinContent(mbin)) / (hp->GetBinContent(mbin) + hn->GetBinContent(mbin));
        // double valx = hn->GetBinCenter(mbin);
        // double err = hn->GetBinError(mbin);

        // cout << " val " << biases[ibias] << "  " << valy << " err " << err << endl;

        g->SetPoint(g->GetN(), biases[ibias], valy);
        // g->SetPointError(g->GetN(), 0, err);
    }
    return g;
}

int main(int argc, char *argv[]) {
    if (argc < 2) {
        printf("Standard usage   : %s <file_data> <file_mc>\n", argv[0]);
        printf("Alternative usage: %s <file_mc> <file_data>\n", argv[0]);
        return 0;
    }

    string fname = argv[1];
    TFile *f = new TFile(fname.c_str());
    if (f->IsZombie()) {
        printf("Error opening: %s\n", fname.c_str());
        return 0;
    }

    string fname_data = argv[2];
    TFile *fd = new TFile(fname.c_str());
    if (fd->IsZombie()) {
        printf("Error opening: %s\n", fname_data.c_str());
        return 0;
    }

    char func[10];
    sprintf(func, "pol%i", order);

    TFile *fout = new TFile("fitoutput.root", "RECREATE");

    // Perform the interpolation for each bin of eta as a function of delta
    for (int ieta = 0; ieta < etab; ieta++) {
        // Skip eta region -0.1 < eta < 0.1
        if (ieta == 8) continue;

        // loop on mass bins
        for (int im = mbmin; im <= mbmax; im++) {
            TGraphErrors *g = maketgraph(f, etabins[ieta], etabins[ieta + 1], im + 1);
            if (!g) continue;

            // fit
            TF1 *parfit = new TF1("ParFit", func);
            g->Fit(parfit, "WQ", "", g->GetX()[1], g->GetX()[g->GetN() - 1]);

            fout->cd();
            g->Write();
            // fout->Close();

            // store the fit results
            vector<double> pars;
            for (int p = 0; p <= order; p++)
                // pars.push_back(parfit->GetParameter(order-p));
                pars.push_back(parfit->GetParameter(p));

            deltapolys[ieta][im] = poly(pars);
            cout << ieta << "  " << im << endl;
            printf("%f + %f x + %f x^2 + %f x^3 \n", pars[0], pars[1], pars[2], pars[3]);
        }
    }

    // Read the (pseudo)data and fill the data matrix
    TFile *fdata = new TFile(fname_data.c_str());

    for (int ieta = 0; ieta < etab; ieta++) {
        // Skip eta region -0.1 < eta < 0.1
        if (ieta == 8) continue;

        char stretal[10], stretau[10];
        sprintf(stretal, "%.2f", etabins[ieta]);
        sprintf(stretau, "%.2f", etabins[ieta + 1]);

        char strbias[10];
        sprintf(strbias, "%.6f", biasmap[ieta]);

        TString namenominalneg =
            Form("ID/PseudoMass/All__NegLepEta/KinSel_etaMuMinus_inRANGE_%s_%s/MLL_61_121/Template_BIAS_%s", stretal, stretau, strbias);
        TString namenominalpos =
            Form("ID/PseudoMass/All__PosLepEta/KinSel_etaMuPlus_inRANGE_%s_%s/MLL_61_121/Template_BIAS_%s", stretal, stretau, strbias);

        // cout << namenominal << endl;

        TH1D *hp = (TH1D *)fdata->Get(namenominalpos);
        TH1D *hn = (TH1D *)fdata->Get(namenominalneg);
        if (!hp) continue;
        if (!hn) continue;

        // TString hname = Form("eta%s_%s_bias_%s",stretal,stretau,strbias);
        TString hname = Form("eta%d", ieta);

        TH1D *hdiff = (TH1D *)hp->Clone();
        TH1D *hsum = (TH1D *)hp->Clone();

        hdiff->Add(hn, -1);
        hsum->Add(hn, 1);

        TH1D *href = (TH1D *)hdiff->Clone();
        href->Divide(hsum);

        href->SetLineColor(kBlack);
        href->SetMarkerColor(kBlack);

        fout->cd();
        href->SetName(hname);
        href->Write();

        // loop on mass bins
        for (int im = mbmin; im <= mbmax; im++) {
            dat[ieta][im] = href->GetBinContent(im + 1);
            err[ieta][im] = href->GetBinError(im + 1);
            // err[ieta][im]  = sqrt(hp->GetBinContent(im+1)+hn->GetBinContent(im+1));
            cout << " eta " << ieta << " m " << im << " : " << dat[ieta][im] << "+/-" << err[ieta][im] << "  "
                 << deltapolys[ieta][im].eval(0.) << endl;
        }
    }

    // minuit minimisation
    // create minimizer giving a name and a name (optionally) for the specific
    // algorithm
    // possible choices are:
    //     minName                  algoName
    // Minuit /Minuit2             Migrad, Simplex,Combined,Scan  (default is Migrad)
    //  Minuit2                     Fumili2
    //  Fumili
    //  GSLMultiMin                ConjugateFR, ConjugatePR, BFGS,
    //                              BFGS2, SteepestDescent
    //  GSLMultiFit
    //   GSLSimAn

    const char *minName = "Minuit2";
    const char *algoName = "";
    // int randomSeed = -1;

    // Genetic
    ROOT::Math::Minimizer *min = ROOT::Math::Factory::CreateMinimizer(minName, algoName);

    // set tolerance, etc...
    min->SetMaxFunctionCalls(1000000);  // for Minuit/Minuit2
    min->SetMaxIterations(10000);       // for GSL
    // min->SetTolerance(0.001);
    min->SetPrintLevel(1);

    // create function wrapper for minmizer
    // a IMultiGenFunction type
    ROOT::Math::Functor c2f(&chi2, etab);

    // step for minimisation
    double step[etab];
    for (int ieta = 0; ieta < etab; ieta++) step[ieta] = 0.001;

    step[8] = 0.00;

    // starting point
    TRandom2 r(123456);
    double variable[etab];
    for (int ieta = 0; ieta < etab; ieta++) variable[ieta] = r.Uniform(-0.2, 0.2);

    variable[8] = 0.00;

    min->SetFunction(c2f);

    // Set the free variables to be minimized!
    for (int ieta = 0; ieta < etab; ieta++) {
        char varname[20];
        sprintf(varname, "d%i", ieta);
        min->SetVariable(ieta, varname, variable[ieta], step[ieta]);
    }

    // do the minimization
    min->Minimize();
    min->Hesse();

    // get Minos errors
    cout << "Errors from MINOS" << endl;
    const double *xs = min->X();
    min->SetPrintLevel(0);
    double ep[etab];
    double em[etab];
    for (int ieta = 0; ieta < etab; ieta++) {
        double errm, errp;
        min->GetMinosError(ieta, errm, errp);
        ep[ieta] = errp;
        em[ieta] = errm;
        printf("d%i = %f +%f %f \n", ieta, xs[ieta], errp, errm);
    }

    /*
    cout << "Correlation Matrix" << endl;
    cout << setw(5) << " ";
    for (int ieta1 = 0; ieta1 < etab; ieta1++)
      cout << setw(14) << "c" << ieta1;
    cout << endl;
    for (int ieta1 = 0; ieta1 < etab; ieta1++)
      {
        cout << setw(5) << "c" << ieta1;
        for (int ieta2 = 0; ieta2 < etab; ieta2++)
         cout << setw(15) << min->CovMatrix(ieta1, ieta2)/min->Errors()[ieta1]/min->Errors()[ieta2];
        cout << endl;
      }
    */

    // print results
    // SetAtlasStyle();
    TCanvas *cnv = new TCanvas("delta", "", 800, 600);
    // double etabins[25]= {-2.47,-2.3, -2, -1.82, -1.55, -1.37, -1.2, -1, -0.8, -0.6 ,-0.4, -0.2, 0, 0.2, 0.4 , 0.6 , 0.8 , 1 , 1.2 , 1.37
    // , 1.55 , 1.82 , 2 , 2.3 , 2.47};
    TH1F *deta = new TH1F("dh", "", etab, etabins);
    for (int ieta = 0; ieta < etab; ieta++) {
        deta->SetBinContent(ieta + 1, -xs[ieta]);
        deta->SetBinError(ieta + 1, (ep[ieta] - em[ieta]) / 2.);
    }

    deta->SetXTitle("#eta");
    deta->SetYTitle("#delta");

    deta->SetStats(0);
    deta->SetLineColor(kBlue + 1);
    deta->SetMarkerStyle(20);
    deta->SetMarkerColor(kBlue + 1);
    deta->SetMarkerSize(0);

    deta->SetMinimum(-0.1);
    deta->SetMaximum(0.1);

    deta->Draw("HIST E1");
    cnv->Print("delta.pdf");

    fout->Close();
    return 0;
}
