#ifndef MUONTP_METADATATREEREADER_H
#define MUONTP_METADATATREEREADER_H

#include <TTree.h>

#include <iostream>
#include <memory>
#include <set>
#include <string>
#include <vector>

namespace MuonTP {
    class WeightHandler;
    class MetaDataStore {
    public:
        MetaDataStore(unsigned int ID, bool isData);
        // Setter functions

        void AddTotalEvents(Long64_t TotEvents);
        void AddProcessedEvents(Long64_t ProcessedEvents);

        void AddSumW(double SumW);
        void AddSumW2(double SumW2);

        void SetRunNumber(unsigned int runNumber);
        void SetLHEIndex(unsigned int lhe_idx);

        void ReserveSpaceTotalLumi(unsigned int N);
        void ReserveSpaceProcessedLumi(unsigned int N);
        void InsertTotalLumiBlock(unsigned int BCID);
        void InsertProcessedLumiBlock(unsigned int BCID);

        void SetPileUpLuminosity(double prwLumi);

        void SetxSectionInfo(double xSection, double kFactor, double Efficiency, const std::string& smpName);
        void SetLHEWeightName(const std::string& w_name);
        void Lock();

        // Getter functions
        bool isData() const;
        bool isDerivedAOD() const;
        void setDerivedAOD(bool B);

        unsigned int runNumber() const;
        unsigned int DSID() const;
        unsigned int LheVariation() const;

        double SumW() const;
        double SumW2() const;

        double xSection() const;
        double kFactor() const;
        double FilterEfficiency() const;
        double prwLuminosity() const;

        Long64_t TotalEvents() const;
        Long64_t ProcessedEvents() const;

        const std::vector<unsigned int>& TotalLumiBlocks() const;
        const std::vector<unsigned int>& ProcessedLumiBlocks() const;

        // Sample name from the PMG tool
        std::string SampleName() const;
        /// LHE variation name from the meta data
        std::string WeightName() const;

        ~MetaDataStore() = default;

        double Normalization() const;
        double finalWeight() const;

    private:
        bool m_isData{false};
        bool m_isDerivedAOD{false};
        unsigned int m_DSID{0};
        unsigned int m_runNumber{0};

        double m_xSection{0};
        double m_kFactor{0};
        double m_Efficiency{0};

        double m_SumW{0};
        double m_SumW2{0};

        double m_prwLumi{0};

        Long64_t m_TotalEvents{0};
        Long64_t m_ProcessedEvents{0};

        std::vector<unsigned int> m_TotalLumiBlocks;
        std::vector<unsigned int> m_ProcessedLumiBlocks;

        double m_finalWeight{0};

        /// Sample name
        std::string m_name{0};

        /// LHE variation
        unsigned int m_lhe{0};
        std::string m_weight_name{};

        bool m_Locked{false};
    };

    class MonteCarloPeriodHandler {
    public:
        MonteCarloPeriodHandler(unsigned int DSID, unsigned int lhe_index);

        unsigned int DSID() const;
        unsigned int LheVariation() const;
        std::vector<unsigned int> getMCcampaigns() const;
        std::shared_ptr<MetaDataStore> getHandler(unsigned int run = -1) const;
        std::shared_ptr<MetaDataStore> insertHandler(unsigned int run);

        void AddTotalEvents(Long64_t TotEvents, unsigned int run = -1);
        void AddProcessedEvents(Long64_t ProcessedEvents, unsigned int run = -1);
        void AddSumW(double SumW, unsigned int run = -1);
        void AddSumW2(double SumW2, unsigned int run = -1);

        void SetxSectionInfo(double xSection, double kFactor, double Efficiency, const std::string& smpName);
        void SetLHEWeightName(const std::string& w_name);

        void SetPileUpLuminosity(double prwLumi, unsigned int run = -1);
        double prwTotalLuminosity() const;

        void Lock();
        void PinCrossSection();

    private:
        MonteCarloPeriodHandler(const MonteCarloPeriodHandler&) = delete;
        void operator=(const MonteCarloPeriodHandler&) = delete;
        std::vector<std::shared_ptr<MetaDataStore>> m_periods;
        std::shared_ptr<MetaDataStore> m_summary_period;
        bool m_locked;
    };

    class NormalizationDataBase {
    public:
        static NormalizationDataBase* getDataBase();
        static void resetDataBase();

        double getNormalization(unsigned int DSID, unsigned int lhe_var);
        double getNormTimesXsec(unsigned int DSID, unsigned int lhe_var);
        bool init(const std::vector<std::string>& FileList);

        void PromptMetaDataTree();

        bool isData() const;
        bool isInitialized() const;

        // Information about the processed runs
        std::vector<unsigned int> GetRunNumbers() const;
        const std::vector<unsigned int>& GetProcessedLumiBlocks(unsigned int runNumber);
        const std::vector<unsigned int>& GetTotalLumiBlocks(unsigned int runNumber);

        Long64_t GetTotalEvents(unsigned int ID);
        Long64_t GetProcessedEvents(unsigned int ID);

        // Some information about the MetaData in MC
        std::vector<unsigned int> GetListOfMCSamples() const;

        unsigned int GetNumLheVariations(unsigned int dsid) const;

        unsigned int GetNumMCsamples() const;
        std::shared_ptr<MonteCarloPeriodHandler> getPeriodHandler(unsigned int DSID, unsigned int LheVar) const;

        double GetSumW(unsigned int DSID, unsigned int LheVar);
        double GetSumW2(unsigned int DSID, unsigned int LheVar);

        double GetxSection(unsigned int DSID);
        double GetFilterEfficiency(unsigned int DSID);
        double GetkFactor(unsigned int DSID);
        std::string GetSampleName(unsigned int DSID);

        ~NormalizationDataBase();
        void LoadxSections(const std::string& xSecDir);

    private:
        static NormalizationDataBase* m_Inst;
        NormalizationDataBase();
        NormalizationDataBase(const NormalizationDataBase&) = delete;
        void operator=(const NormalizationDataBase&) = delete;

        void PromptMCMetaDataTree();
        void PromptRunMetaDataTree();
        bool ReadTree(TTree* t);
        bool ReadMCTree(TTree* t);
        bool ReadDataTree(TTree* t);
        void LockStores();

    protected:
        enum DSIDStatus { Present, Updated, Failed };

    private:
        DSIDStatus GetDSIDStatus(unsigned int DSID, unsigned int lheVar);
        DSIDStatus GetRunStatus(unsigned int runNumber);

        std::vector<std::shared_ptr<MetaDataStore>> m_DB;
        std::vector<std::shared_ptr<MonteCarloPeriodHandler>> m_mc_DB;

        std::shared_ptr<MetaDataStore> m_ActMeta;
        std::shared_ptr<MonteCarloPeriodHandler> m_ActMCMeta;
        bool m_init;
        WeightHandler* m_Weighter;
        bool m_xSecLoaded;
    };
}  // namespace MuonTP
#endif
