# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/afs/cern.ch/user/n/nviauxma/ZmassCalibration3/source/NtauHelpers/NtupleAnalysisUtils/test/TestBranchRead.cxx" "/afs/cern.ch/user/n/nviauxma/ZmassCalibration3/NtauHelpers/NtupleAnalysisUtils/CMakeFiles/NtupleAnalysisUtils_TestBranchRead.dir/test/TestBranchRead.cxx.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ATLAS"
  "ATLAS_GAUDI_V21"
  "GAUDI_V20_COMPAT"
  "HAVE_64_BITS"
  "HAVE_GAUDI_PLUGINSVC"
  "HAVE_PRETTY_FUNCTION"
  "PACKAGE_VERSION=\"NtupleAnalysisUtils-00-00-00\""
  "PACKAGE_VERSION_UQ=NtupleAnalysisUtils-00-00-00"
  "ROOTCORE_RELEASE_SERIES=25"
  "XAOD_ANALYSIS"
  "__IDENTIFIER_64BIT__"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "source/NtauHelpers/NtupleAnalysisUtils"
  "/cvmfs/atlas-nightlies.cern.ch/repo/sw/21.2_AthAnalysis_x86_64-centos7-gcc8-opt/sw/lcg/releases/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/afs/cern.ch/user/n/nviauxma/ZmassCalibration3/NtauHelpers/NtupleAnalysisUtils/CMakeFiles/NtupleAnalysisUtils.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
