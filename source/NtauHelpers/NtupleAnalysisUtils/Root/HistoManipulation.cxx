#include "NtupleAnalysisUtils/Helpers/HistoManipulation.h" 
#include <TH2Poly.h>

float  PlotUtils::normaliseToBinWidth(TH1* h, float target_width){
    if (h == 0){
        return 0;
    }
    if (target_width<0){
        target_width = h->GetXaxis()->GetBinWidth(1);
    }

    for (int k = 1; k < h->GetNbinsX()+1; ++k){
        float width = h->GetXaxis()->GetBinWidth(k);
        float SF = target_width/width; 
        h->SetBinContent(k, SF*h->GetBinContent(k));
        h->SetBinError(k, SF*h->GetBinError(k));
    }
    h->SetMaximum(h->GetMaximum() / h->GetBinWidth(h->GetMaximumBin()) * target_width);
    return target_width;
}

void PlotUtils::normaliseToIntegral(TH1* h, double target_integral, bool multiplyByWidth, bool includeOverflow){
    double integral = h->Integral( (includeOverflow ? 0 : 1), 
                                    h->GetNbinsX()+ (includeOverflow ? 1 : 0),(multiplyByWidth ? "WIDTH" : "")); 
    if (integral!=0) h->Scale(target_integral/integral);
}

bool PlotUtils::isAlphaNumeric(const TH1 *histo){
    return isAlphaNumeric(histo->GetXaxis()) || 
           (histo->GetDimension() >= 2 && isAlphaNumeric(histo->GetYaxis())) || 
           (histo->GetDimension() == 3 && isAlphaNumeric(histo->GetZaxis()));
}
 bool PlotUtils::isAlphaNumeric(const TAxis* axis){
    for (int b = 1; b < axis->GetNbins(); ++b){
        std::string b_label{axis->GetBinLabel(b)};
        if (b_label.size()) return true;
    }
    return false;
 }    
bool PlotUtils::isOverflowBin(const TH1 *Histo, int bin) {
    int x{-1}, y{-1}, z{-1};
    Histo->GetBinXYZ(bin, x, y, z);
    if (isOverflowBin(Histo->GetXaxis(), x)) return true;
    if (Histo->GetDimension() >= 2 && isOverflowBin(Histo->GetYaxis(), y)) return true;
    if (Histo->GetDimension() == 3 && isOverflowBin(Histo->GetZaxis(), z)) return true;
    return false;
}
bool PlotUtils::isOverflowBin(const TAxis *a, int bin) { return bin <= 0 || bin >= a->GetNbins() + 1; }

 unsigned int PlotUtils::getNbins(const TH1 *Histo) {
    if (!Histo) return 0;
    if (Histo->GetDimension() == 2) {
        const TH2Poly* poly = dynamic_cast<const TH2Poly *>(Histo);
        if (poly) return poly->GetNumberOfBins() + 1;
    }
    unsigned int N = Histo->GetNbinsX() + 2;
    if (Histo->GetDimension() >= 2) N *= (Histo->GetNbinsY() + 2);
    if (Histo->GetDimension() == 3) N *= (Histo->GetNbinsZ() + 2);
    return N;
}

void PlotUtils::shiftOverflows(TH1* H){
    if (isAlphaNumeric(H)) return;
    /// Small helper function to shift the bin along the axis
    std::function<int(TAxis*, int)> shift_bin = [](TAxis* A, int bin) {
        if (bin == 0) return 1;
        return bin - isOverflowBin(A, bin);
    };
    /// backup the Entries as any SetBinContent operation
    /// increments this variable by one unit
    const Long64_t ent = H->GetEntries();
    for (int b = getNbins(H) - 1; b >= 0; --b) {
        /// We do not need to take care of the visible bins
        if (!isOverflowBin(H, b)) continue;
        int x{0}, y{0}, z{0};
        H->GetBinXYZ(b, x, y, z);
        /// Determine the target X. In case of 1D y and z are zero        
        int t_x(shift_bin(H->GetXaxis(), x)), t_y(y), t_z(z);
        /// 2D or 3D histograms
        if (H->GetDimension() > 1) t_y = shift_bin(H->GetYaxis(), y);
        if (H->GetDimension() > 2) t_z = shift_bin(H->GetZaxis(), z);
        /// By construction this bin cannot be the same 
        int target_bin = H->GetBin(t_x, t_y, t_z);
        /// Add the bin contents and errors
        H->SetBinContent(target_bin, H->GetBinContent(target_bin) + H->GetBinContent(b));
        H->SetBinError(target_bin, std::hypot(H->GetBinError(target_bin), H->GetBinError(b)));
        /// Remove the contents from the histogram
        H->SetBinContent(b, 0);
        H->SetBinError(b, 0);
    }
    H->SetEntries(ent);
}


TH1* PlotUtils::getRatio(const TEfficiency* num, const TEfficiency* den, EfficiencyMode errmode){
    TH1* num1 = dynamic_cast<TH1*>(num->GetPassedHistogram()->Clone());
    TH1* num2 = dynamic_cast<TH1*>(den->GetPassedHistogram()->Clone());
    TH1* den1 = dynamic_cast<TH1*>(num->GetTotalHistogram()->Clone());
    TH1* den2 = dynamic_cast<TH1*>(den->GetTotalHistogram()->Clone());

    TH1* effNum = getRatio(num1, den1, PlotUtils::efficiencyErrors);
    TH1* effDen = getRatio(num2, den2, PlotUtils::efficiencyErrors);
    return getRatio(effNum, effDen, errmode);

}
TH1* PlotUtils::getRatio(const TH1* num, const TH1* den, EfficiencyMode errmode){
    if (num == 0 || den == 0){
        return 0;
    }
    TH1* ratio = dynamic_cast<TH1*> (num->Clone(Form("ratioFrom%s_and_%s",num->GetName(), den->GetName())));
    ratio->SetDirectory(0);
    bool is_alpha_num = isAlphaNumeric(num) || isAlphaNumeric(den);  
    for (int k = 0; k < num->GetNcells(); ++k){
         /// If the histogram has alpha numeric bin labels and the
        /// overflow is accessed then the bin content doubles
        if (is_alpha_num && isOverflowBin(num,k)) continue;
        double n = num->GetBinContent(k);
        double d = den->GetBinContent(k);
        double dn = num->GetBinError(k);
        double dd = den->GetBinError(k);
        if (d == 0){
            ratio->SetBinContent(k,0);
            ratio->SetBinError(k,0); 
        }
        else {
            ratio->SetBinContent(k, n/d);
            if (errmode == defaultErrors){
                ratio->SetBinError(k, sqrt((dn*dn)/(d*d) + (dd*dd*n*n/(d*d*d*d))));
            }
            else if (errmode == efficiencyErrors){
                double m = d - n; 
                double dm = sqrt(fabs(dd*dd-dn*dn));
                ratio->SetBinError(k, sqrt((n*n*dm*dm + dn*dn*m*m)/(d*d*d*d)));
            }
            if (errmode == numeratorOnlyErrors){
                ratio->SetBinError(k, dn/d);
            }
            if (errmode == denominatorOnlyErrors){
                ratio->SetBinError(k, dd*n/(d*d));
            }
        }
    }
    return ratio;
}
TH1* PlotUtils::getRatio(const TProfile* num, const TProfile* den, EfficiencyMode errmode){
    TH1* ratio = dynamic_cast<TH1*>(num->ProjectionX()->Clone());
    ratio->Reset();
    ratio->SetDirectory(0);
    for (int k = 0; k < num->GetNcells(); ++k){
        double n = num->GetBinContent(k);
        double d = den->GetBinContent(k);
        double dn = num->GetBinError(k);
        double dd = den->GetBinError(k);
        if (d == 0){
            ratio->SetBinContent(k,0);
            ratio->SetBinError(k,0); 
        }
        else {
            ratio->SetBinContent(k, n/d);
            if (errmode == defaultErrors){
                ratio->SetBinError(k, sqrt((dn*dn)/(d*d) + (dd*dd*n*n/(d*d*d*d))));
            }
            else if (errmode == efficiencyErrors){
                double m = d - n; 
                double dm = sqrt(fabs(dd*dd-dn*dn));
                ratio->SetBinError(k, sqrt((n*n*dm*dm + dn*dn*m*m)/(d*d*d*d)));
            }
            if (errmode == numeratorOnlyErrors){
                ratio->SetBinError(k, dn/d);
            }
            if (errmode == denominatorOnlyErrors){
                ratio->SetBinError(k, dd*n/(d*d));
            }
        }
    }
    return ratio;

}

TGraph* PlotUtils::getRatio(const TGraph* num, const TGraph* den, EfficiencyMode ){

    if (!num || !den){
        std::cerr << "nullptr handed to PlotUtils::getRatio"<<std::endl; 
        return nullptr; 
    }
    if (num->GetN() != den->GetN()){
        std::cerr << "n points on num and den graphs don't match in PlotUtils::getRatio"<<std::endl; 
        return nullptr; 
    }
    TGraph* theRatio = new TGraph; 
    for (int k = 0; k < num->GetN(); ++k){
        if (std::abs(num->GetX()[k] - den->GetX()[k]) > 1e-9){
            std::cout << "Points do not match in X for PlotUtils::getRatio"<<std::endl; 
            delete theRatio; 
            return nullptr;
        }
        if (den->GetY()[k] != 0){
            theRatio->SetPoint(theRatio->GetN(), den->GetX()[k], num->GetY()[k] / den->GetY()[k]);
        }
    }
    return theRatio;
}

std::shared_ptr<TH1D> PlotUtils::extractEfficiency(const TEfficiency* eff){
    
    std::shared_ptr<TH1D> effHisto{dynamic_cast<TH1D*>(eff->GetTotalHistogram()->Clone())};
    // now we populate the effi histo 
    effHisto->Reset();
    for (int k = 1; k < effHisto->GetNbinsX()+1; ++k){
        effHisto->SetBinContent(k,eff->GetEfficiency(k)); 
        /// for now, we symmetrise the errors... probably there is a nicer approach 
        effHisto->SetBinError(k,0.5 * (eff->GetEfficiencyErrorUp(k) + eff->GetEfficiencyErrorLow(k))); 
        /// catch empty bins
        if (eff->GetTotalHistogram()->GetBinContent(k) == 0){
            effHisto->SetBinContent(k,0); effHisto->SetBinError(k,0);
        }
    }
    return effHisto; 
}

Plot<TGraph> PlotUtils::getRatio(Plot<TGraph> & num, Plot<TGraph> & den, EfficiencyMode ){
    return Plot<TGraph>(CopyExisting<TGraph>(PlotUtils::getRatio(num(),den())), num.plotFormat()); 
}

TH1* PlotUtils::toCDF (const TH1* differential){
    if (!differential){
        return 0;
    }
    TH1* theCDF = dynamic_cast<TH1*>(differential->Clone(Form("CDF_%s",differential->GetName())));
    const double OneOverTotal = 1./(differential->Integral(0, differential->GetNbinsX()+1));
    const double OneOverTotalSq = OneOverTotal*OneOverTotal;
    double y=0;
    double dysquare=0;
    for (int k = 0; k < differential->GetNbinsX()+1; ++k){
        y += differential->GetBinContent(k)*OneOverTotal;
        dysquare+= differential->GetBinError(k)*differential->GetBinError(k)*OneOverTotalSq;
        theCDF->SetBinContent(k,y);
        theCDF->SetBinError(k,sqrt(dysquare));
    }
    return theCDF;
}
