
#include <MuonSagittaBias/PseudoCollinsSoperUtils.h>

namespace {
    static const CollinsSoper::Vector4D dummy_vec{};

}  // namespace
namespace CollinsSoper {
    bool PseudoCollinsSoperFrame::charge() const { return m_charge; }
    PseudoCollinsSoperFrame::PseudoCollinsSoperFrame(SagittaBiasAnalyzer& tree, int trk_type, bool pos_lep) :
        m_parent{tree}, m_charge{pos_lep}, m_trk_type{trk_type} {}
    int PseudoCollinsSoperFrame::trk_type() const { return m_trk_type; }
    void PseudoCollinsSoperFrame::update() {
        if (m_last_ev == m_parent.getCurrEntry()) return;
        m_last_ev = m_parent.getCurrEntry();
        if (!m_parent.is_valid(trk_type())) {
            m_cs_frame.reset();
            m_lep_plus = m_lep_minus = dummy_vec;
        }
        const float pt_plus = m_parent.pt(trk_type(), charge());
        const float pt_minus = m_parent.pt(trk_type(), charge());

        PolarVector4D pos_lep_lab{pt_plus, m_parent.eta(trk_type(), LepCharge::Plus), m_parent.phi(trk_type(), LepCharge::Plus), 0.},
            neg_lep_lab{pt_minus, m_parent.eta(trk_type(), LepCharge::Minus), m_parent.phi(trk_type(), LepCharge::Minus), 0.};

        m_cs_frame = std::make_unique<RestFrame>(pos_lep_lab, neg_lep_lab);
        m_lep_plus = m_cs_frame->toRestFrame(pos_lep_lab);
        m_lep_minus = m_cs_frame->toRestFrame(neg_lep_lab);
    }
    const Vector4D& PseudoCollinsSoperFrame::pos_lep_CS() {
        update();
        return m_lep_plus;
    }
    const Vector4D& PseudoCollinsSoperFrame::neg_lep_CS() {
        update();
        return m_lep_minus;
    }

}  // namespace CollinsSoper