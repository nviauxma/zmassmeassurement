// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME MuonSagittaBiasLibDictReflexDict
#define R__NO_DEPRECATION

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// The generated code does not explicitly qualifies STL entities
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "/afs/cern.ch/user/n/nviauxma/ZmassCalibration3/source/MuonSagittaBias/MuonSagittaBias/MuonSagittaBiasDict.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *MuonTrkcLcLcalib_constants_Dictionary();
   static void MuonTrkcLcLcalib_constants_TClassManip(TClass*);
   static void *new_MuonTrkcLcLcalib_constants(void *p = 0);
   static void *newArray_MuonTrkcLcLcalib_constants(Long_t size, void *p);
   static void delete_MuonTrkcLcLcalib_constants(void *p);
   static void deleteArray_MuonTrkcLcLcalib_constants(void *p);
   static void destruct_MuonTrkcLcLcalib_constants(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::MuonTrk::calib_constants*)
   {
      ::MuonTrk::calib_constants *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::MuonTrk::calib_constants));
      static ::ROOT::TGenericClassInfo 
         instance("MuonTrk::calib_constants", "Utils.h", 10,
                  typeid(::MuonTrk::calib_constants), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &MuonTrkcLcLcalib_constants_Dictionary, isa_proxy, 4,
                  sizeof(::MuonTrk::calib_constants) );
      instance.SetNew(&new_MuonTrkcLcLcalib_constants);
      instance.SetNewArray(&newArray_MuonTrkcLcLcalib_constants);
      instance.SetDelete(&delete_MuonTrkcLcLcalib_constants);
      instance.SetDeleteArray(&deleteArray_MuonTrkcLcLcalib_constants);
      instance.SetDestructor(&destruct_MuonTrkcLcLcalib_constants);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::MuonTrk::calib_constants*)
   {
      return GenerateInitInstanceLocal((::MuonTrk::calib_constants*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::MuonTrk::calib_constants*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *MuonTrkcLcLcalib_constants_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::MuonTrk::calib_constants*)0x0)->GetClass();
      MuonTrkcLcLcalib_constants_TClassManip(theClass);
   return theClass;
   }

   static void MuonTrkcLcLcalib_constants_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_MuonTrkcLcLcalib_constants(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::MuonTrk::calib_constants : new ::MuonTrk::calib_constants;
   }
   static void *newArray_MuonTrkcLcLcalib_constants(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::MuonTrk::calib_constants[nElements] : new ::MuonTrk::calib_constants[nElements];
   }
   // Wrapper around operator delete
   static void delete_MuonTrkcLcLcalib_constants(void *p) {
      delete ((::MuonTrk::calib_constants*)p);
   }
   static void deleteArray_MuonTrkcLcLcalib_constants(void *p) {
      delete [] ((::MuonTrk::calib_constants*)p);
   }
   static void destruct_MuonTrkcLcLcalib_constants(void *p) {
      typedef ::MuonTrk::calib_constants current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::MuonTrk::calib_constants

namespace {
  void TriggerDictionaryInitialization_libMuonSagittaBiasLibDict_Impl() {
    static const char* headers[] = {
"0",
0
    };
    static const char* includePaths[] = {
0
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "libMuonSagittaBiasLibDict dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace MuonTrk{class __attribute__((annotate("$clingAutoload$MuonSagittaBias/Utils.h")))  calib_constants;}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "libMuonSagittaBiasLibDict dictionary payload"

#ifndef HAVE_PRETTY_FUNCTION
  #define HAVE_PRETTY_FUNCTION 1
#endif
#ifndef HAVE_64_BITS
  #define HAVE_64_BITS 1
#endif
#ifndef __IDENTIFIER_64BIT__
  #define __IDENTIFIER_64BIT__ 1
#endif
#ifndef ATLAS
  #define ATLAS 1
#endif
#ifndef GAUDI_V20_COMPAT
  #define GAUDI_V20_COMPAT 1
#endif
#ifndef ATLAS_GAUDI_V21
  #define ATLAS_GAUDI_V21 1
#endif
#ifndef HAVE_GAUDI_PLUGINSVC
  #define HAVE_GAUDI_PLUGINSVC 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 25
#endif
#ifndef CLHEP_MAX_MIN_DEFINED
  #define CLHEP_MAX_MIN_DEFINED 1
#endif
#ifndef CLHEP_ABS_DEFINED
  #define CLHEP_ABS_DEFINED 1
#endif
#ifndef CLHEP_SQR_DEFINED
  #define CLHEP_SQR_DEFINED 1
#endif
#ifndef PACKAGE_VERSION
  #define PACKAGE_VERSION "MuonSagittaBias-00-00-00"
#endif
#ifndef PACKAGE_VERSION_UQ
  #define PACKAGE_VERSION_UQ MuonSagittaBias-00-00-00
#endif
#ifndef EIGEN_DONT_VECTORIZE
  #define EIGEN_DONT_VECTORIZE 1
#endif
#ifndef CLHEP_MAX_MIN_DEFINED
  #define CLHEP_MAX_MIN_DEFINED 1
#endif
#ifndef CLHEP_ABS_DEFINED
  #define CLHEP_ABS_DEFINED 1
#endif
#ifndef CLHEP_SQR_DEFINED
  #define CLHEP_SQR_DEFINED 1
#endif
#ifndef CLHEP_MAX_MIN_DEFINED
  #define CLHEP_MAX_MIN_DEFINED 1
#endif
#ifndef CLHEP_ABS_DEFINED
  #define CLHEP_ABS_DEFINED 1
#endif
#ifndef CLHEP_SQR_DEFINED
  #define CLHEP_SQR_DEFINED 1
#endif
#ifndef CLHEP_MAX_MIN_DEFINED
  #define CLHEP_MAX_MIN_DEFINED 1
#endif
#ifndef CLHEP_ABS_DEFINED
  #define CLHEP_ABS_DEFINED 1
#endif
#ifndef CLHEP_SQR_DEFINED
  #define CLHEP_SQR_DEFINED 1
#endif

#define _BACKWARD_BACKWARD_WARNING_H
// Inline headers
#ifndef MUONSAGITTABIAS_MUONSAGITTABIASDICT_H
#define MUONSAGITTABIAS_MUONSAGITTABIASDICT_H

#include "MuonSagittaBias/SagittaBiasTree.h"
#include "MuonSagittaBias/Utils.h"

#endif

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[] = {
"MuonTrk::calib_constants", payloadCode, "@",
nullptr
};
    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("libMuonSagittaBiasLibDict",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_libMuonSagittaBiasLibDict_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T> class DataVectorBase;", 1},{"template <typename T, typename BASE> class DataVector;", 1},{"namespace DataVector_detail { template <typename B> class DVLEltBase_init; }", 1},{"namespace DataVector_detail { template <typename T> class RegisterDVLEltBaseInit; }", 1},{"namespace DataVector_detail { template <typename T> class DVLEltBaseInit; }", 1}}, classesHeaders, /*hasCxxModule*/false);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_libMuonSagittaBiasLibDict_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_libMuonSagittaBiasLibDict() {
  TriggerDictionaryInitialization_libMuonSagittaBiasLibDict_Impl();
}
