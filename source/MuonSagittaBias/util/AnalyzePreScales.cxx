#include <MuonSagittaBias/ArgumentParser.h>
#include <MuonSagittaBias/TriggerAccess.h>
#include <MuonSagittaBias/TriggerAnalyzer.h>
#include <MuonSagittaBias/TriggerPreScaleDumper.h>
#include <MuonSagittaBias/Utils.h>
#include <NtauHelpers/HistoBinConfig.h>
#include <NtauHelpers/OutFileWriter.h>
#include <NtauHelpers/SelectionWithTitle.h>

int main(int argc, char* argv[]) {
    /// Standard piece of code to setup the analysis...
    ArgumentParser parser;
    parser.out_root_file("CalibFile.root").out_dir("PreScaleCalibrations").initialize_prw(true);
    if (!parser.parse_args(argc, argv)) return EXIT_FAILURE;

    std::shared_ptr<TFile> out_file{parser.out_file()};
    if (!out_file.get()) { return EXIT_FAILURE; }

    if (parser.is_mc()) {
        std::cerr << "PreScale dump only makes sense for data" << std::endl;
        return EXIT_FAILURE;
    }
    PlotUtils::HistoBinConfig trig_cfg;

    {
        std::shared_ptr<TFile> f_file = PlotUtils::Open(parser.in_files()[0]);
        TTree* t_tree{nullptr};
        f_file->GetObject(parser.tree_name().c_str(), t_tree);
        trig_cfg = TriggerAccess::extract_binning(t_tree);
    }

    // Define the list of triggers to be analyzed
    std::vector<TrigPreScaleCache> trigs_to_analyze;
    for (const auto& labels : trig_cfg.x_bin_labels()) {
        if (labels.first != 1) trigs_to_analyze.emplace_back(labels.second);
    }
    /// Now we need to find out the number of total events
    Long64_t tot_events{0}, proc_events{0};
    std::vector<std::string> in_files = parser.get_job_files();
    for (const std::string& in_file : in_files) {
        std::shared_ptr<TFile> t_file = PlotUtils::Open(in_file);
        if (!t_file) continue;
        TTree* t_obj{nullptr};
        t_file->GetObject(parser.tree_name().c_str(), t_obj);
        if (t_obj) tot_events += t_obj->GetEntries();
    }
    std::cout << "Found in total " << tot_events << " to analyze" << std::endl;
    TStopwatch watch;
    watch.Start();
    for (const std::string& in_file : in_files) {
        std::shared_ptr<TFile> t_file = PlotUtils::Open(in_file);
        if (!t_file) continue;
        TTree* t_obj{nullptr};
        t_file->GetObject(parser.tree_name().c_str(), t_obj);
        if (!t_obj) continue;

        TriggerAnalyzer anal{t_obj};
        const Long64_t ent_in_file{anal.getEntries()};
        for (Long64_t entry = 0; entry < ent_in_file; ++entry) {
            anal.getEntry(entry);
            for (TrigPreScaleCache& cache : trigs_to_analyze) { cache.fill(anal); }

            ++proc_events;
            if (!(proc_events % 10000)) {
                const double elapsed_time = watch.RealTime();
                const double progress = 100. * proc_events / tot_events;
                watch.Continue();
                std::cout << "Done processing " << proc_events << "/" << tot_events << " ( " << Form("%.1f", progress) << "%)" << std::endl;
            }
        }
    }
    std::cout << "Done the processing. Will now write the output file" << std::endl;

    /// Make sure to dump them into your output
    for (TrigPreScaleCache& cache : trigs_to_analyze) {
        cache.write(parser.out_dir());
        cache.write(out_file);
    }
    return EXIT_SUCCESS;
}
