
#include <iostream>
#include "TH1F.h"
#include "TCanvas.h"
#include "TRandom3.h"
#include "TMath.h"
#include "TStopwatch.h"
#include "NtupleAnalysisUtils/NTAUTopLevelIncludes.h"

/// test the bootstrap poisson generator 

double poissonCDF(int k){
    if (k == 0) return TMath::PoissonI(0,1);
    return TMath::PoissonI((int)k+0.1,1) + poissonCDF(k-1);
}


int main (int, char**){
    /// set up a reproducible poisson helper
    BootstrapPoissonHelper myHelper;
    myHelper.reseed(1234);

    /// roll 1M random toys
    size_t ntoys = 1000000; 

    /// fill a vector with the toys
    std::vector<double> fillthis(ntoys);
    myHelper.populateRandom(fillthis);

    /// project the vector to a histogram 
    TH1F* h1 = new TH1F("pois1","pois1",10,-0.5,9.5);
    h1->Sumw2();
    for (auto X : fillthis){
        h1->Fill(X);
    }

    /// integrate the histo into a CDF
    TH1* cdf = PlotUtils::toCDF(h1);

    /// now compare the toy-CDF to the true poisson one. 
    /// We apply a 5 sigma threshold, which should be plenty for 
    /// any systematic deviation @ 1MToys while being far from the expected
    /// amount of stat fluctuation
    for (int k = 0; k < h1->GetNbinsX(); ++k){
        /// target value = true poisson CDF
        double target = poissonCDF(k);
        /// efficiency stat error: sqrt(eff x (1-eff)) / sqrt(Ntotal) 
        double sigma = sqrt((target)*(1.-target))/sqrt((double)ntoys);  
        /// result from the toy
        double found = cdf->GetBinContent(k+1); 
        /// cut at 10 sigma
        if (std::fabs((found - target)) > 5. *sigma) return EXIT_FAILURE; 
    }

    return EXIT_SUCCESS;
}   
