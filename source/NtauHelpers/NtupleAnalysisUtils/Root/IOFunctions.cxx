#include "NtupleAnalysisUtils/Helpers/IOFunctions.h"
#include "NtupleAnalysisUtils/Helpers/StringHelpers.h"

std::string PlotUtils::getDefaultPlotDir(){
    
    // this is the default ctor, giving us "now". Just what we need... 
    TTimeStamp today;
    std::string datestring = today.AsString("c");
    std::string prefix = gSystem->ExpandPathName("$TestArea/../Plots/");
    // truncate away the hour, minute, second etc when forming the final string. 
    std::string outdir = prefix+datestring.substr(0,10);
  
    if (!createDirIfNeeded(outdir)){
        std::cerr << "Failed to create the plot dir. Will dump plots into the CWD! "<<std::endl;
        return "";
    }
    return outdir+ "/";
}
bool PlotUtils::createDirIfNeeded(const std::string & dir){ 
    int status = gSystem->mkdir(dir.c_str(),true);
    // status can be negative if the dir already exists (good!) or if the dir could not be made(bad). Complain if there is no writable outdir after
    // the operation. In that case, write to the cwd...
    // And YES, AccessPathName returns FALSE if it is able to access the path... figure!
    if (status < 0 &&  (gSystem->AccessPathName(dir.c_str(), kFileExists) || gSystem->AccessPathName(dir.c_str(), kWritePermission ))){
        std::cerr << "Failed to create the dir"<<dir<<"! "<<std::endl;
        return false;
    }
    return true;
}
void PlotUtils::saveCanvas(TCanvas* can, const std::string & fname,  const CanvasOptions & options){
    const std::vector<std::string> & outputFormats = options.OutputFormats(); 
    std::string customDir = options.OutputDir(); 
    for (const std::string & fmt : outputFormats){
        can->SaveAs(composeFileName(fname+"."+fmt,customDir).c_str());
    }
}
void PlotUtils::saveCanvas(std::shared_ptr<TCanvas> can, const std::string & fname,  const CanvasOptions & options){
    saveCanvas(can.get(), fname, options);  
}

std::shared_ptr<MultiPagePdfHandle> PlotUtils::startMultiPagePdfFile(const std::string & fname, const CanvasOptions & options){
    std::string fullFname = composeFileName(fname+".pdf", options.OutputDir()); 
    return std::make_shared<MultiPagePdfHandle>(fullFname); 
}

const std::string PlotUtils::composeFileName(const std::string & fname, const std::string & basePlotDir){
    std::string targetDir = basePlotDir;
    const std::string subdir = gSystem->DirName(fname.c_str());
    const std::string basename = gSystem->BaseName(fname.c_str());
    if (targetDir.size() == 0) targetDir = getDefaultPlotDir();
    if (subdir.size() > 0 && subdir != "."){
        targetDir+="/"+subdir+"/";
    }
    createDirIfNeeded(targetDir); 
    return targetDir+"/"+basename;
}


std::string PlotUtils::GetFilePath(const std::string &in_path) {
    if (in_path.empty()) {
        std::cerr<<"GetImportPath() --- No valid string was given "<<std::endl;
        return in_path;
    }
    /// The file is very likely on eos
    if (in_path.find("root://") == 0) { return in_path; }
    std::vector<std::string> Trials{ResolveEnviromentVariables(in_path), 
                                    in_path,
                                    // TestArea
                                    ResolveEnviromentVariables("${TestArea}/" + in_path),
                                    ResolveEnviromentVariables("${WorkDir_DIR}/data/" + in_path),
                                    ResolveEnviromentVariables("${WorkDir_DIR}/data/" + ReplaceExpInString(in_path, "data/", ""))};

    // The out of the box config does not work
    for (auto &T : Trials) {
        std::ifstream Import;
        Import.open(T);
        if (Import.good()) { 
            return ReplaceExpInString(T, "//", "/"); 
        }
    }
    std::cerr<<"GetFilePath() --- Could not find a valid file from path: "<<in_path<<std::endl;
    return std::string("");
}
TDirectory *PlotUtils::mkdir(const std::string &name, TDirectory *in_dir) {
    if (name.find("/") == std::string::npos) return in_dir;
    std::string dir_name = name.substr(0, name.find("/"));
    TDirectory *out_dir = in_dir->GetDirectory(dir_name.c_str());
    if (!out_dir) {
        in_dir->mkdir(dir_name.c_str());
        return mkdir(name, in_dir);
    }
    std::string remain = name.substr(name.find("/") + 1, std::string::npos);
    if (remain.empty() || remain.find("/") == std::string::npos) return out_dir;
    return mkdir(remain, out_dir);
}
std::shared_ptr<TFile> PlotUtils::Open(const std::string &Path) {
    static std::mutex lock_mutex;
    std::lock_guard<std::mutex> guard(lock_mutex);
    std::shared_ptr<TFile> File(TFile::Open(GetFilePath(Path).c_str(), "READ"));
    if (!File || !File->IsOpen()) {
        std::cerr<<"TFile::Open() --- Could not open file "<<Path<<std::endl;
        return std::shared_ptr<TFile>();
    }
    return File;
}
std::vector<std::string> PlotUtils::getValidFiles(const std::vector<std::string> &in_files, const std::string &tree_name, bool skip_empty) {
    std::vector<std::string> out_files;
    out_files.reserve(in_files.size());
    if (tree_name.empty()) {
        std::cerr << "ERROR: -- Empty tree name given" << std::endl;
        return out_files;
    }
    for (auto &in : in_files) {
        std::shared_ptr<TFile> f = Open(in);
        if (!f || !f->IsOpen()) continue;
        TTree *t = readFromFile<TTree>(f,tree_name);
        if (!t) continue;
        if (!skip_empty || t->GetEntries()) { out_files.push_back(in); }
        delete t;
    }
    return out_files;
}