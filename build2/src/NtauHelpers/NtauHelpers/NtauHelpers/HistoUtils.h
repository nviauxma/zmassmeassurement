#ifndef NTAUHELPERS_HISTOUTILS_H
#define NTAUHELPERS_HISTOUTILS_H

#include <NtupleAnalysisUtils/NTAUTopLevelIncludes.h>
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include <TH3.h>

/// Returns the global bin of the histogram with the highest value above the given minimum
int GetMinimumBin(const std::shared_ptr<TH1> &histo, double min_value = -DBL_MAX);
int GetMinimumBin(const TH1 *histo, double min_value = DBL_MIN);

/// Returns the global bin of the histogram with the lowest value below the given maximum
int GetMaximumBin(const std::shared_ptr<TH1> &histo, double max_value = DBL_MAX);
int GetMaximumBin(const TH1 *histo, double max_value = DBL_MAX);

/// Ensures that the given value is witihin the axis-ranges. I.e.
/// the corresponding axis edges are returned if the value exceeds the range
double pull_outside(const TAxis *A, const double value);

/// Fills the value in the histogram such that the over and underflows are pulled into the first / last bin
void pull_outside(TH1 *H, const double v);
void pull_outside(TH2 *H, const double x, const double y);
void pull_outside(TH3 *H, const double x, const double y, const double z);

//
void set_bin_labels(TAxis *a, const std::map<int, std::string> &labels);

/// Loads the histogram from a TFile and pipes it into a shared ptr. The Histogram
/// is decoupled from the TFile after operation
std::shared_ptr<TH1> load_histo(const std::string &histo_name, std::shared_ptr<TFile> file);
std::shared_ptr<TH1> load_histo(const std::string &histo_name, TFile *file);

/// Clones the histogram and pipes it into a shared_ptr. For the name a random string is choosen
std::shared_ptr<TH1> clone(const std::shared_ptr<TH1> &histo, bool reset = false);
std::shared_ptr<TH1> clone(const TH1 *histo, bool reset = false);

std::shared_ptr<TH1> combineTemplates(const std::shared_ptr<TH1> &h1, const std::shared_ptr<TH1> &h2);
std::shared_ptr<TH1> combineTemplates(const TH1 *h1, const TH1 *h2);

/// Extracts the binning from the Axis including high and low edges
std::vector<double> ExtractBinning(const TAxis *axis);

/// Returns the normalization of the bin according to the bin width of the first bin
///  I.e. BinWidth(x) / BinWidth(1)
double GetBinNormalization(const TH1 *H1, int Bin);
double GetBinNormalization(const std::shared_ptr<TH1> &H1, int Bin);

/// Integral functions which take into account that the histogram is normalized to the first bin
std::pair<double, double> IntegrateAndError(const std::shared_ptr<TH1> &Histo, unsigned int xStart = 0, int xEnd = -1,
                                            unsigned int yStart = 0, int yEnd = -1, unsigned int zStart = 0, int zEnd = -1);
std::pair<double, double> IntegrateAndError(const TH1 *Histo, unsigned int xStart = 0, int xEnd = -1, unsigned int yStart = 0,
                                            int yEnd = -1, unsigned int zStart = 0, int zEnd = -1);

/// Returns the number of visible bins
unsigned int getNbinsVis(const std::shared_ptr<TH1> &H);
unsigned int getNbinsVis(const TH1 *H);

/// Returns a vector containing the global bin numbers which are neither overlow or underflow
std::vector<int> visibileBinsVector(const std::shared_ptr<TH1> &H);
std::vector<int> visibileBinsVector(const TH1 *H);

// Projects a 2D or 3D histogram into 1D
///

std::shared_ptr<TH1> ProjectInto1D(const std::shared_ptr<TH1> &H3, unsigned int project_along, int bin1_start = 0, int bin1_end = -1,
                                   int bin2_start = 0, int bin2_end = -1);
std::shared_ptr<TH1> ProjectInto1D(const TH1 *H3, unsigned int project_along, int bin1_start = 0, int bin1_end = -1, int bin2_start = 0,
                                   int bin2_end = -1);

std::shared_ptr<TH1> ProjectInto2D(const std::shared_ptr<TH1> &H3, unsigned int project_along, int bin_start, int bin_end);
std::shared_ptr<TH1> ProjectInto2D(const TH1 *H3, unsigned int project_along, int bin_start, int bin_end);

bool IsFinite(const std::shared_ptr<TH1> &histo);
bool IsFinite(const TH1 *histo);

/// Generates a histogram from three vectors defining the bin borders. Depending on whether
/// one, two or three non-empty vectors are given, a TH*D histogram is returned. The histogram
/// assigns a random name to avoid confusion with the ROOT garbage collector
std::shared_ptr<TH1> MakeTH1(std::vector<double> BinEdgesX, std::vector<double> BinEdgesY = {}, std::vector<double> BinEdgesZ = {});

/// Returns true if the binning of two histograms is the same_binning
///     -- Dimensions are equal
///     -- The bin edges are the same for each axis
bool same_binning(const std::shared_ptr<TH1> &H1, const std::shared_ptr<TH1> &H2);
bool same_binning(const TH1 *H1, const TH1 *H2);
bool same_binning(const TAxis *a1, const TAxis *a2);

/// Copies the styling from one histogram to another
///     axis title
///     histogram title
///     LineColor / FillColor / MarkerColor
///     MarkerStyle / LineStyle / LineWidth
void copy_styling(const std::shared_ptr<TH1> &from, std::shared_ptr<TH1> to);
void copy_styling(const TH1 *from, TH1 *to);

#endif
