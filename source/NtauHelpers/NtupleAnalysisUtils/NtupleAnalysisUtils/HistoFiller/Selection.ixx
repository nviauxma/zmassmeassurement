using namespace std::placeholders;

template <class ProcessThis> Selection<ProcessThis>::Selection(std::function <bool(ProcessThis &)> toApply):
    m_cut (toApply),
    m_isDummy(false){
}  
template <class ProcessThis> Selection<ProcessThis>::Selection(const Selection<ProcessThis> & other):
    m_cut (other.m_cut),
    m_isDummy(false){
    /// copy the identifier since this is the same selection
    this->ISortableByID::setID(other.ISortableByID::getID()); 
}
template <class ProcessThis> Selection<ProcessThis>::Selection(){
    m_isDummy = true; 
    m_cut = std::function<bool(ProcessThis&)>([](ProcessThis&){return true;});
    this->ISortableByID::setID(ObjectID{"DummySelection"}); 

}
template <class ProcessThis>  inline bool Selection<ProcessThis>::operator() (ProcessThis & input) const {
    return m_cut(input);
}

template <class ProcessThis> Selection<ProcessThis> Selection<ProcessThis>::operator+ (const Selection<ProcessThis> & other) const {
    // OR with a 'return true': always returns true
    if (m_isDummy || other.m_isDummy){
        return Selection<ProcessThis>();
    }
    std::function<bool(ProcessThis &)> f1 = m_cut;
    std::function<bool(ProcessThis &)> f2 = other.m_cut;
    std::function<bool(ProcessThis &)> f_or ([f1,f2](ProcessThis &t) {return (f1(t)||f2(t));});
    auto s = Selection<ProcessThis>(f_or);
    /// assign an ID that unambiguously derives from the parent IDs. Ensures that the same combinations result in the same ID for efficiency
    s.ISortableByID::setID(ObjectID{std::vector<ObjectID>{ObjectID{"{"}, this->ISortableByID::getID(), ObjectID{"CutOr"}, other.ISortableByID::getID(),ObjectID{"}"}}}); 
    return s; 
}
template <class ProcessThis> Selection<ProcessThis> Selection<ProcessThis>::operator* (const Selection<ProcessThis> & other) const {
    // AND with a 'return true': always return the ANDed thing
    if (m_isDummy){
        return other; 
    }
    else if (other.m_isDummy){
        return Selection<ProcessThis>(*this);
    }
    std::function<bool(ProcessThis &)> f1 = m_cut;
    std::function<bool(ProcessThis &)> f2 = other.m_cut;
    std::function<bool(ProcessThis &)> f_and ([f1,f2](ProcessThis &t) {return (f1(t)&&f2(t));});
    auto s = Selection<ProcessThis>(f_and);
    /// assign an ID that unambiguously derives from the parent IDs. Ensures that the same combinations result in the same ID for efficiency
    s.ISortableByID::setID(ObjectID{std::vector<ObjectID>{ObjectID{"{"}, this->ISortableByID::getID(), ObjectID{"CutAnd"}, other.ISortableByID::getID(),ObjectID{"}"}}}); 
    return s; 
}
template <class ProcessThis> Selection<ProcessThis> Selection<ProcessThis>::operator|| (const Selection<ProcessThis> & other) const{
    return this->operator+(other);
}
template <class ProcessThis> Selection<ProcessThis> Selection<ProcessThis>::operator&& (const Selection<ProcessThis> & other) const{
    return this->operator*(other);
}
template <class ProcessThis> Selection<ProcessThis> Selection<ProcessThis>::operator! (){
    std::function<bool(ProcessThis &)> f1 = m_cut;
    std::function<bool(ProcessThis &)> f_not ([f1](ProcessThis &t) {return (!f1(t));});
    auto s = Selection<ProcessThis>(f_not);
    s.ISortableByID::setID(ObjectID{std::vector<ObjectID>{ObjectID{"{"}, ObjectID{"NOT"}, this->ISortableByID::getID(),ObjectID{"}"}}}); 
    return s; 
}

template <class ProcessThis> std::shared_ptr<ISelection> Selection<ProcessThis>::clone() const {
    return std::make_shared<Selection<ProcessThis>>(*this);
}