# Generated by CMake

if("${CMAKE_MAJOR_VERSION}.${CMAKE_MINOR_VERSION}" LESS 2.5)
   message(FATAL_ERROR "CMake >= 2.6.0 required")
endif()
cmake_policy(PUSH)
cmake_policy(VERSION 2.6...3.17)
#----------------------------------------------------------------
# Generated CMake target import file.
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Protect against multiple inclusion, which would fail when already imported targets are added once more.
set(_targetsDefined)
set(_targetsNotDefined)
set(_expectedTargets)
foreach(_expectedTarget WorkDir::MuonSagittaBiasPkg WorkDir::MuonSagittaBiasPkgPrivate WorkDir::MuonSagittaBiasLib WorkDir::AnalyzePreScales WorkDir::DumpTriggerHisto WorkDir::EvaluateTrigger WorkDir::PlotAsymmetries WorkDir::PlotPhiCS WorkDir::PlotPseudoMass WorkDir::PlotTriggerEfficiency WorkDir::QualityCriteriaAnalysis WorkDir::RadialBiasAnalysis WorkDir::SagittaFit WorkDir::ScaleFit WorkDir::TRT_selections WorkDir::TestTemplateFit WorkDir::q_over_p_analysis WorkDir::ClusterSubmissionPkg WorkDir::ClusterSubmissionPkgPrivate WorkDir::CreateBatchJobSplit WorkDir::NtauHelpersPkg WorkDir::NtauHelpersPkgPrivate WorkDir::NtauHelpersLib WorkDir::QuickFileCompare WorkDir::NtupleAnalysisUtilsPkg WorkDir::NtupleAnalysisUtilsPkgPrivate WorkDir::NtupleAnalysisUtils WorkDir::NtupleAnalysisUtils_Example WorkDir::TestCutFlow WorkDir::checkVecBranch WorkDir::generateBranchManager WorkDir::testAsyncExcHandling WorkDir::testAsynchRead WorkDir::testBranchAccessByName WorkDir::testPdgRound WorkDir::NtupleAnalysisUtils_tutorialPkg WorkDir::NtupleAnalysisUtils_tutorialPkgPrivate WorkDir::NTAU_Example_Plot_2_1_2 WorkDir::NTAU_Example_Plot_2_1_3 WorkDir::NTAU_Example_Plot_2_2)
  list(APPEND _expectedTargets ${_expectedTarget})
  if(NOT TARGET ${_expectedTarget})
    list(APPEND _targetsNotDefined ${_expectedTarget})
  endif()
  if(TARGET ${_expectedTarget})
    list(APPEND _targetsDefined ${_expectedTarget})
  endif()
endforeach()
if("${_targetsDefined}" STREQUAL "${_expectedTargets}")
  unset(_targetsDefined)
  unset(_targetsNotDefined)
  unset(_expectedTargets)
  set(CMAKE_IMPORT_FILE_VERSION)
  cmake_policy(POP)
  return()
endif()
if(NOT "${_targetsDefined}" STREQUAL "")
  message(FATAL_ERROR "Some (but not all) targets in this export set were already defined.\nTargets Defined: ${_targetsDefined}\nTargets not yet defined: ${_targetsNotDefined}\n")
endif()
unset(_targetsDefined)
unset(_targetsNotDefined)
unset(_expectedTargets)


# Compute the installation prefix relative to this file.
get_filename_component(_IMPORT_PREFIX "${CMAKE_CURRENT_LIST_FILE}" PATH)
get_filename_component(_IMPORT_PREFIX "${_IMPORT_PREFIX}" PATH)
if(_IMPORT_PREFIX STREQUAL "/")
  set(_IMPORT_PREFIX "")
endif()

# Create imported target WorkDir::MuonSagittaBiasPkg
add_library(WorkDir::MuonSagittaBiasPkg INTERFACE IMPORTED)

set_target_properties(WorkDir::MuonSagittaBiasPkg PROPERTIES
  INTERFACE_INCLUDE_DIRECTORIES "${_IMPORT_PREFIX}/src/MuonSagittaBias"
  INTERFACE_LINK_LIBRARIES "WorkDir::ClusterSubmissionPkg;WorkDir::NtauHelpersPkg;FourMomUtilsPkg;PathResolverPkg"
)

# Create imported target WorkDir::MuonSagittaBiasPkgPrivate
add_library(WorkDir::MuonSagittaBiasPkgPrivate INTERFACE IMPORTED)

# Create imported target WorkDir::MuonSagittaBiasLib
add_library(WorkDir::MuonSagittaBiasLib SHARED IMPORTED)

set_target_properties(WorkDir::MuonSagittaBiasLib PROPERTIES
  INTERFACE_COMPILE_DEFINITIONS "CLHEP_MAX_MIN_DEFINED;CLHEP_ABS_DEFINED;CLHEP_SQR_DEFINED"
  INTERFACE_INCLUDE_DIRECTORIES "\$<TARGET_PROPERTY:WorkDir::MuonSagittaBiasPkg,INTERFACE_INCLUDE_DIRECTORIES>;${_IMPORT_PREFIX}/\${LCG_RELEASE_BASE}/LCG_97a/Boost/1.72.0/x86_64-centos7-gcc8-opt/include;/cvmfs/atlas-nightlies.cern.ch/repo/sw/21.2_AthAnalysis_x86_64-centos7-gcc8-opt/2022-01-16T0340/AthAnalysisExternals/21.2.200/InstallArea/x86_64-centos7-gcc8-opt/include;${_IMPORT_PREFIX}/\${LCG_RELEASE_BASE}/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/include"
  INTERFACE_LINK_LIBRARIES "\${LCG_RELEASE_BASE}/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/lib/libCore.so;\${LCG_RELEASE_BASE}/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/lib/libTree.so;\${LCG_RELEASE_BASE}/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/lib/libRIO.so;\${LCG_RELEASE_BASE}/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/lib/libHist.so;\${LCG_RELEASE_BASE}/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/lib/libPhysics.so;\${LCG_RELEASE_BASE}/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/lib/libGpad.so;\${LCG_RELEASE_BASE}/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/lib/libRooFitCore.so;\${LCG_RELEASE_BASE}/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/lib/libRooFit.so;\${LCG_RELEASE_BASE}/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/lib/libMinuit.so;\${LCG_RELEASE_BASE}/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/lib/libGraf.so;/cvmfs/atlas-nightlies.cern.ch/repo/sw/21.2_AthAnalysis_x86_64-centos7-gcc8-opt/2022-01-16T0340/AthAnalysisExternals/21.2.200/InstallArea/x86_64-centos7-gcc8-opt/lib/libCLHEP.so;PathResolver;WorkDir::NtauHelpersLib;FourMomUtils"
  INTERFACE_SYSTEM_INCLUDE_DIRECTORIES "\${LCG_RELEASE_BASE}/LCG_97a/Boost/1.72.0/x86_64-centos7-gcc8-opt/include;/cvmfs/atlas-nightlies.cern.ch/repo/sw/21.2_AthAnalysis_x86_64-centos7-gcc8-opt/2022-01-16T0340/AthAnalysisExternals/21.2.200/InstallArea/x86_64-centos7-gcc8-opt/include;\${LCG_RELEASE_BASE}/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/include"
)

# Create imported target WorkDir::AnalyzePreScales
add_executable(WorkDir::AnalyzePreScales IMPORTED)

# Create imported target WorkDir::DumpTriggerHisto
add_executable(WorkDir::DumpTriggerHisto IMPORTED)

# Create imported target WorkDir::EvaluateTrigger
add_executable(WorkDir::EvaluateTrigger IMPORTED)

# Create imported target WorkDir::PlotAsymmetries
add_executable(WorkDir::PlotAsymmetries IMPORTED)

# Create imported target WorkDir::PlotPhiCS
add_executable(WorkDir::PlotPhiCS IMPORTED)

# Create imported target WorkDir::PlotPseudoMass
add_executable(WorkDir::PlotPseudoMass IMPORTED)

# Create imported target WorkDir::PlotTriggerEfficiency
add_executable(WorkDir::PlotTriggerEfficiency IMPORTED)

# Create imported target WorkDir::QualityCriteriaAnalysis
add_executable(WorkDir::QualityCriteriaAnalysis IMPORTED)

# Create imported target WorkDir::RadialBiasAnalysis
add_executable(WorkDir::RadialBiasAnalysis IMPORTED)

# Create imported target WorkDir::SagittaFit
add_executable(WorkDir::SagittaFit IMPORTED)

# Create imported target WorkDir::ScaleFit
add_executable(WorkDir::ScaleFit IMPORTED)

# Create imported target WorkDir::TRT_selections
add_executable(WorkDir::TRT_selections IMPORTED)

# Create imported target WorkDir::TestTemplateFit
add_executable(WorkDir::TestTemplateFit IMPORTED)

# Create imported target WorkDir::q_over_p_analysis
add_executable(WorkDir::q_over_p_analysis IMPORTED)

# Create imported target WorkDir::ClusterSubmissionPkg
add_library(WorkDir::ClusterSubmissionPkg INTERFACE IMPORTED)

set_target_properties(WorkDir::ClusterSubmissionPkg PROPERTIES
  INTERFACE_INCLUDE_DIRECTORIES "${_IMPORT_PREFIX}/src/NtauHelpers/ClusterSubmission"
  INTERFACE_LINK_LIBRARIES "POOLRootAccessPkg;xAODRootAccessPkg;PathResolverPkg"
)

# Create imported target WorkDir::ClusterSubmissionPkgPrivate
add_library(WorkDir::ClusterSubmissionPkgPrivate INTERFACE IMPORTED)

# Create imported target WorkDir::CreateBatchJobSplit
add_executable(WorkDir::CreateBatchJobSplit IMPORTED)

# Create imported target WorkDir::NtauHelpersPkg
add_library(WorkDir::NtauHelpersPkg INTERFACE IMPORTED)

set_target_properties(WorkDir::NtauHelpersPkg PROPERTIES
  INTERFACE_INCLUDE_DIRECTORIES "${_IMPORT_PREFIX}/src/NtauHelpers/NtauHelpers"
  INTERFACE_LINK_LIBRARIES "MuonIdHelpersPkg;xAODMuonPkg;PathResolverPkg;WorkDir::ClusterSubmissionPkg;WorkDir::NtupleAnalysisUtilsPkg;PMGAnalysisInterfacesPkg;MuonIdHelpersPkg;MCTruthClassifierPkg;PileupReweightingPkg;IdDictParserPkg"
)

# Create imported target WorkDir::NtauHelpersPkgPrivate
add_library(WorkDir::NtauHelpersPkgPrivate INTERFACE IMPORTED)

set_target_properties(WorkDir::NtauHelpersPkgPrivate PROPERTIES
  INTERFACE_LINK_LIBRARIES "AsgToolsPkg;AthenaBaseCompsPkg;CxxUtilsPkg;GaudiKernelPkg"
)

# Create imported target WorkDir::NtauHelpersLib
add_library(WorkDir::NtauHelpersLib SHARED IMPORTED)

set_target_properties(WorkDir::NtauHelpersLib PROPERTIES
  INTERFACE_INCLUDE_DIRECTORIES "\$<TARGET_PROPERTY:WorkDir::NtauHelpersPkg,INTERFACE_INCLUDE_DIRECTORIES>;${_IMPORT_PREFIX}/\${LCG_RELEASE_BASE}/LCG_97a/Boost/1.72.0/x86_64-centos7-gcc8-opt/include;${_IMPORT_PREFIX}/\${LCG_RELEASE_BASE}/LCG_97a/Boost/1.72.0/x86_64-centos7-gcc8-opt/include"
  INTERFACE_LINK_LIBRARIES "\${LCG_RELEASE_BASE}/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/lib/libCore.so;\${LCG_RELEASE_BASE}/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/lib/libTree.so;\${LCG_RELEASE_BASE}/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/lib/libRIO.so;\${LCG_RELEASE_BASE}/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/lib/libHist.so;\${LCG_RELEASE_BASE}/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/lib/libPhysics.so;\${LCG_RELEASE_BASE}/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/lib/libGpad.so;\${LCG_RELEASE_BASE}/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/lib/libRooFitCore.so;\${LCG_RELEASE_BASE}/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/lib/libRooFit.so;\${LCG_RELEASE_BASE}/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/lib/libMinuit.so;\${LCG_RELEASE_BASE}/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/lib/libGraf.so;\${LCG_RELEASE_BASE}/LCG_97a/Boost/1.72.0/x86_64-centos7-gcc8-opt/lib/libboost_filesystem.so;\${LCG_RELEASE_BASE}/LCG_97a/Boost/1.72.0/x86_64-centos7-gcc8-opt/lib/libboost_thread.so;-pthread;\${LCG_RELEASE_BASE}/LCG_97a/Boost/1.72.0/x86_64-centos7-gcc8-opt/lib/libboost_system.so;\${LCG_RELEASE_BASE}/LCG_97a/Boost/1.72.0/x86_64-centos7-gcc8-opt/lib/libboost_regex.so;\${LCG_RELEASE_BASE}/LCG_97a/Boost/1.72.0/x86_64-centos7-gcc8-opt/lib/libboost_chrono.so;\${LCG_RELEASE_BASE}/LCG_97a/Boost/1.72.0/x86_64-centos7-gcc8-opt/lib/libboost_date_time.so;\${LCG_RELEASE_BASE}/LCG_97a/Boost/1.72.0/x86_64-centos7-gcc8-opt/lib/libboost_atomic.so;Threads::Threads;-Wl,--copy-dt-needed-entries;PathResolver;WorkDir::NtupleAnalysisUtils;MuonIdHelpersLib;MCTruthClassifier;PileupReweightingLib;xAODMuon;PMGAnalysisInterfacesLib"
  INTERFACE_SYSTEM_INCLUDE_DIRECTORIES "\${LCG_RELEASE_BASE}/LCG_97a/Boost/1.72.0/x86_64-centos7-gcc8-opt/include;\${LCG_RELEASE_BASE}/LCG_97a/Boost/1.72.0/x86_64-centos7-gcc8-opt/include"
)

# Create imported target WorkDir::QuickFileCompare
add_executable(WorkDir::QuickFileCompare IMPORTED)

# Create imported target WorkDir::NtupleAnalysisUtilsPkg
add_library(WorkDir::NtupleAnalysisUtilsPkg INTERFACE IMPORTED)

set_target_properties(WorkDir::NtupleAnalysisUtilsPkg PROPERTIES
  INTERFACE_INCLUDE_DIRECTORIES "${_IMPORT_PREFIX}/src/NtauHelpers/NtupleAnalysisUtils"
)

# Create imported target WorkDir::NtupleAnalysisUtilsPkgPrivate
add_library(WorkDir::NtupleAnalysisUtilsPkgPrivate INTERFACE IMPORTED)

# Create imported target WorkDir::NtupleAnalysisUtils
add_library(WorkDir::NtupleAnalysisUtils SHARED IMPORTED)

set_target_properties(WorkDir::NtupleAnalysisUtils PROPERTIES
  INTERFACE_INCLUDE_DIRECTORIES "\$<TARGET_PROPERTY:WorkDir::NtupleAnalysisUtilsPkg,INTERFACE_INCLUDE_DIRECTORIES>;${_IMPORT_PREFIX}/\${LCG_RELEASE_BASE}/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/include"
  INTERFACE_LINK_LIBRARIES "\${LCG_RELEASE_BASE}/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/lib/libCore.so;\${LCG_RELEASE_BASE}/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/lib/libMathCore.so;\${LCG_RELEASE_BASE}/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/lib/libHist.so;\${LCG_RELEASE_BASE}/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/lib/libGraf.so;\${LCG_RELEASE_BASE}/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/lib/libGpad.so;\${LCG_RELEASE_BASE}/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/lib/libTree.so;\${LCG_RELEASE_BASE}/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/lib/libRIO.so;pthread"
  INTERFACE_SYSTEM_INCLUDE_DIRECTORIES "\${LCG_RELEASE_BASE}/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/include"
)

# Create imported target WorkDir::NtupleAnalysisUtils_Example
add_executable(WorkDir::NtupleAnalysisUtils_Example IMPORTED)

# Create imported target WorkDir::TestCutFlow
add_executable(WorkDir::TestCutFlow IMPORTED)

# Create imported target WorkDir::checkVecBranch
add_executable(WorkDir::checkVecBranch IMPORTED)

# Create imported target WorkDir::generateBranchManager
add_executable(WorkDir::generateBranchManager IMPORTED)

# Create imported target WorkDir::testAsyncExcHandling
add_executable(WorkDir::testAsyncExcHandling IMPORTED)

# Create imported target WorkDir::testAsynchRead
add_executable(WorkDir::testAsynchRead IMPORTED)

# Create imported target WorkDir::testBranchAccessByName
add_executable(WorkDir::testBranchAccessByName IMPORTED)

# Create imported target WorkDir::testPdgRound
add_executable(WorkDir::testPdgRound IMPORTED)

# Create imported target WorkDir::NtupleAnalysisUtils_tutorialPkg
add_library(WorkDir::NtupleAnalysisUtils_tutorialPkg INTERFACE IMPORTED)

set_target_properties(WorkDir::NtupleAnalysisUtils_tutorialPkg PROPERTIES
  INTERFACE_INCLUDE_DIRECTORIES "${_IMPORT_PREFIX}/src/NtauHelpers/NtupleAnalysisUtils/NtupleAnalysisUtils_tutorial"
)

# Create imported target WorkDir::NtupleAnalysisUtils_tutorialPkgPrivate
add_library(WorkDir::NtupleAnalysisUtils_tutorialPkgPrivate INTERFACE IMPORTED)

# Create imported target WorkDir::NTAU_Example_Plot_2_1_2
add_executable(WorkDir::NTAU_Example_Plot_2_1_2 IMPORTED)

# Create imported target WorkDir::NTAU_Example_Plot_2_1_3
add_executable(WorkDir::NTAU_Example_Plot_2_1_3 IMPORTED)

# Create imported target WorkDir::NTAU_Example_Plot_2_2
add_executable(WorkDir::NTAU_Example_Plot_2_2 IMPORTED)

if(CMAKE_VERSION VERSION_LESS 3.0.0)
  message(FATAL_ERROR "This file relies on consumers using CMake 3.0.0 or greater.")
endif()

# Load information for each installed configuration.
get_filename_component(_DIR "${CMAKE_CURRENT_LIST_FILE}" PATH)
file(GLOB CONFIG_FILES "${_DIR}/WorkDirConfig-targets-*.cmake")
foreach(f ${CONFIG_FILES})
  include(${f})
endforeach()

# Cleanup temporary variables.
set(_IMPORT_PREFIX)

# Loop over all imported files and verify that they actually exist
foreach(target ${_IMPORT_CHECK_TARGETS} )
  foreach(file ${_IMPORT_CHECK_FILES_FOR_${target}} )
    if(NOT EXISTS "${file}" )
      message(FATAL_ERROR "The imported target \"${target}\" references the file
   \"${file}\"
but this file does not exist.  Possible reasons include:
* The file was deleted, renamed, or moved to another location.
* An install or uninstall procedure did not complete successfully.
* The installation package was faulty and contained
   \"${CMAKE_CURRENT_LIST_FILE}\"
but not all the files it references.
")
    endif()
  endforeach()
  unset(_IMPORT_CHECK_FILES_FOR_${target})
endforeach()
unset(_IMPORT_CHECK_TARGETS)

# This file does not depend on other imported targets which have
# been exported from the same project but in a separate export set.

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
cmake_policy(POP)
