#include "NtupleAnalysisUtils/NTAUTopLevelIncludes.h"


int main (int, char**){

    /// create two Gaussians

    // Here we use a populator which directly gives the content a starting population
    // the first histo is based on less statistics
    Plot<TH1D> myFirstGauss(
        ConstructAndFillInPlace<TH1D>([](TH1D* h){h->FillRandom("gaus",5000);}, "num","num", 50,-5,5), 
        PlotFormat().Color(kRed).MarkerStyle(kFullSquare)
    );  
    // while the second has more
    Plot<TH1D> mySecondGauss(
        ConstructAndFillInPlace<TH1D>([](TH1D* h){h->FillRandom("gaus",20000);}, "den","den", 50,-5,5), 
        PlotFormat().Color(kBlue).MarkerStyle(kFullDotLarge)
    );  

    TCanvas* c1 = new TCanvas();
    mySecondGauss->Draw("PL");
    myFirstGauss->Draw("PLSAME");
    std::cout << mySecondGauss->Integral()<<std::endl;
    std::cout << myFirstGauss->Integral()<<std::endl;
    c1->Draw(); 
    c1->SaveAs("Example_2_2_Step1.pdf"); 

    // Note how now the ConstructAndFillInPlace populator is given to the NormaliseToUnitIntrgral as an argument. 
    // We can keep nesting populators into another to create complex logic! 
    // 
    // Think of it like nesting mathematical functions - g( f(x) ) 

    Plot<TH1D> myFirstGaussNorm(
        NormaliseToIntegral<TH1D>( 
                ConstructAndFillInPlace<TH1D>([](TH1D* h){h->FillRandom("gaus",5000);}, "num","num", 50,-5,5)
        ), PlotFormat().Color(kRed).MarkerStyle(kFullSquare) );  

    Plot<TH1D> mySecondGaussNorm(
        NormaliseToIntegral<TH1D>( 
                ConstructAndFillInPlace<TH1D>([](TH1D* h){h->FillRandom("gaus",20000);}, "den","den", 50,-5,5)
        ), PlotFormat().Color(kBlue).MarkerStyle(kFullDotLarge) );  
    // while this has more

    mySecondGaussNorm->Draw("PL");
    myFirstGaussNorm->Draw("PLSAME");
    /// proof of new norm
    c1->Clear(); 
    std::cout << mySecondGaussNorm->Integral()<<std::endl;
    std::cout << myFirstGaussNorm->Integral()<<std::endl;
    c1->Draw(); 
    c1->SaveAs("Example_2_2_Step2.pdf"); 

    
    // Take it slowly in the following, to make clear what is happening 

    // Construct a pair of populators for normalised gaussians as before 
    NormaliseToIntegral<TH1D> getTheNormalisedNum {ConstructAndFillInPlace<TH1D>([](TH1D* h){h->FillRandom("gaus", 5000);}, "num","num", 50,-5,5), 1.}; 
    NormaliseToIntegral<TH1D> getTheNormalisedDen {ConstructAndFillInPlace<TH1D>([](TH1D* h){h->FillRandom("gaus",20000);}, "den","den", 50,-5,5), 1.}; 

    // Now, we can plug these into the *CalculateRatio* populator. Can you guess what it will do? :-)
    Plot<TH1> myRatio{CalculateRatio(getTheNormalisedNum, getTheNormalisedDen), PlotFormat().Color(kBlue)};

    // draw it to prove it works 
    c1->cd(); 
    myRatio->Draw(); 
    c1->Draw(); 
    c1->SaveAs("Example_2_2_Step3.pdf"); 

    return 0; 
}   