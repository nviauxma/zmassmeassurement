#include "NtupleAnalysisUtils/Ntuple/NtupleBranch.h"


////////////////////////////////
//// Next: NtupleBranch<std::string> > 
//////////////////////////////////

NtupleBranch<std::string>::NtupleBranch(const std::string & branchName, 
                                                    TTree* tree, 
                                                    NtupleBranchMgr* mgr):
    NtupleBranchBase(branchName, tree,mgr){
        /// if we are linked to a tree, set up the tree cache as specified for this branch
        m_content = nullptr;
        if (m_tree && m_tree->GetDirectory()->GetFile() != nullptr){
            if (m_tree->GetCacheSize() != m_treeCacheSize){
                m_tree->SetCacheSize(m_treeCacheSize);    
                m_tree->StopCacheLearningPhase(); 
            }
        }
}

const std::string & NtupleBranch<std::string>::operator() () {
    return get();
}
const std::string & NtupleBranch<std::string>::get(){
    connectIfNeeded(); 
    return *m_content;
}
const std::string & NtupleBranch<std::string>::readOnce (){
    connectIfNeeded(); 
    disconnect(); 
    return *m_content;
}
void NtupleBranch<std::string>::set (std::string in){
    attachIfNeeded(); 
    *m_content=in;
}
void NtupleBranch<std::string>::set (NtupleBranch<std::string> &  in){
    attachIfNeeded(); 
    *m_content=in();
}
void NtupleBranch<std::string>::set (NtupleBranchBase*  in){
    attachIfNeeded();
    /// try to cast the input branch to our format
    NtupleBranch<std::string>* cast = dynamic_cast<NtupleBranch<std::string>*>(in);
    /// compatible! We can read from this one.  
    if (cast){
        *m_content = cast->get();
    }
    /// Incompatible. Throw an error and don't modify the content
    else {
        std::cerr << " Failed to correctly cast the branch in NtupleBranch::set! "<<std::endl;
    }
}
size_t NtupleBranch<std::string>::size() {
    return 1;
}
void NtupleBranch<std::string>::setDummy (){
    attachIfNeeded(); 
    *m_content = dummyVal<std::string>();
}
std::string NtupleBranch<std::string>::generateMemberString() const{
    std::stringstream ss; 
    ss << "NtupleBranch <"<<std::setw(30)<< std::left << "std::string>"<<m_branchName;
    return ss.str();
}
void NtupleBranch<std::string>::attachReadPayload(){
    if (m_branch) {
        /// If our pointer was already filled before, 
        /// we delete it to avoid leaking memory
        if (m_content) delete m_content;
        /// reset the ptr
        m_content = nullptr;
        /// now give the address of the pointer to ROOT 
        m_branch->SetAddress(&m_content);
    }
}
TBranch* NtupleBranch<std::string>::addPayloadAsBranch(){
    /// To branch our string, make sure the pointer points to valid memory 
    if (!m_content){
        m_content = new std::string(""); 
    }
    /// Then we can let ROOT read from the address of our pointer
    return (m_tree ? m_tree->Branch(m_branchName.c_str(), &m_content) : nullptr);
}