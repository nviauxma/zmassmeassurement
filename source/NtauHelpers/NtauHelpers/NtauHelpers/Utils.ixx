#ifndef NTAUHELPERS_UTILS_IXX
#define NTAUHELPERS_UTILS_IXX
#include <NtauHelpers/HistoUtils.h>
#include <NtauHelpers/Utils.h>

template <typename T> int max_bit(const T& number) {
    for (int bit = sizeof(number) * 8 - 1; bit >= 0; --bit) {
        if (number & (1 << bit)) return bit;
    }
    return -1;
}
template <typename T> int min_bit(const T& number) {
    for (unsigned int bit = 0; bit <= sizeof(number) * 8 - 1; ++bit) {
        if (number & (1 << bit)) return bit;
    }
    return -1;
}

template <typename T> void ClearFromDuplicates(std::vector<T>& toClear, std::function<bool(const T&, const T&)> equal) {
    std::vector<T> free;
    free.reserve(toClear.size());
    for (const auto& ele : toClear) {
        if (std::find_if(free.begin(), free.end(), [&ele, &equal](const T& test) { return equal(test, ele); }) == free.end()) {
            free.push_back(ele);
        }
    }
    free.shrink_to_fit();
    toClear = std::move(free);
}
template <typename T> void ClearFromDuplicates(std::vector<T>& toClear) {
    ClearFromDuplicates<T>(toClear, [](const T& a, const T& b) { return a == b; });
}

template <typename T> void EraseFromVector(std::vector<T>& vec, std::function<bool(const T&)> func) {
    typename std::vector<T>::iterator itr = std::find_if(vec.begin(), vec.end(), func);
    while (itr != vec.end()) {
        vec.erase(itr);
        itr = std::find_if(vec.begin(), vec.end(), func);
    }
}
template <class... Args> void Info(const std::string& method, const std::string& info_str, Args&&... args) {
    unsigned int l = method.size();
    std::cout << method.substr(0, l < MsgMethodChars ? l : MsgMethodChars) << WhiteSpaces(MsgMethodChars - l)
              << " -- INFO: " << Form(info_str.c_str(), args...) << std::endl;
}
template <class... Args> void Warning(const std::string& method, const std::string& info_str, Args&&... args) {
    unsigned int l = method.size();
    std::cout << method.substr(0, l < MsgMethodChars ? l : MsgMethodChars) << WhiteSpaces(MsgMethodChars - l)
              << " -- WARNING: " << Form(info_str.c_str(), args...) << std::endl;
}
template <class... Args> void Error(const std::string& method, const std::string& info_str, Args&&... args) {
    unsigned int l = method.size();
    std::cerr << method.substr(0, l < MsgMethodChars ? l : MsgMethodChars) << WhiteSpaces(MsgMethodChars - l)
              << " -- ERROR: " << Form(info_str.c_str(), args...) << std::endl;
}

template <typename T> bool isElementInList(const std::vector<T>& vec, const T& ele) {
    return std::find(vec.begin(), vec.end(), ele) != vec.end();
}

template <class T> unsigned int count_non_empty(PlotContent<T>& h) {
    h.populateAll();
    unsigned int n = 0;
    for (auto& pl : h.getPlots()) {
        n += (!PlotUtils::isOverflowBin(pl.getHisto(), GetMaximumBin(pl.getHisto())) ||
              !PlotUtils::isOverflowBin(pl.getHisto(), GetMinimumBin(pl.getHisto())));
    }
    return n;
}

template <typename T> bool IsFinite(const T& N) {
    if (std::isnan(N) || std::isinf(N)) return false;
    return true;
}
template <typename T> bool isEquiDistant(const std::vector<T>& vec) {
    if (vec.size() < 2) return false;
    const double first_dist = vec[1] - vec[0];
    const size_t s = vec.size();
    for (size_t it = 2; it < s; ++it) {
        if (std::abs((vec[it] - vec[it - 1]) - first_dist) >= DBL_EPSILON) return false;
    }
    return true;
}
template <typename T> size_t calculateNbins(const std::vector<T>& vec) {
    if (!isEquiDistant(vec)) return 0;
    return (vec[vec.size() - 1] - vec[0]) / (vec[1] - vec[0]);
}
template <class InputType> Sample<InputType> make_sample(const std::string& file_list, const std::string& tree_name) {
    return make_sample<InputType>(ReadList(file_list), tree_name);
}
template <class InputType> Sample<InputType> make_sample(const std::vector<std::string>& files, const std::string& tree_name) {
    Sample<InputType> smp;
    std::vector<std::string> good = PlotUtils::getValidFiles(GetPathResolvedFileList(files), tree_name);
    for (auto& f : good) { smp.addFile(f, tree_name); }
    return smp;
}
template <typename T> bool operator==(const std::vector<T>& vec1, const std::vector<T>& vec2) {
    if (vec1.size() != vec2.size()) return false;

    typename std::vector<T>::const_iterator itr_v1 = vec1.begin();
    typename std::vector<T>::const_iterator itr_v2 = vec2.begin();
    /// End iterators
    typename std::vector<T>::const_iterator const end_v1 = vec1.end();
    typename std::vector<T>::const_iterator const end_v2 = vec2.end();
    for (; itr_v1 != end_v1 && itr_v2 != end_v2; ++itr_v1, ++itr_v2) {
        if ((*itr_v1) != (*itr_v2)) return false;
    }
    return true;
}

#endif
