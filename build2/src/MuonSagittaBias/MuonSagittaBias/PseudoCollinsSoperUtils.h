#ifndef NTAU__PSEUDOCOLLINSSOPERUTILS_H
#define NTAU__PSEUDOCOLLINSSOPERUTILS_H
#include <MuonSagittaBias/CollinsSoperUtils.h>
#include <MuonSagittaBias/SagittaBiasAnalyzer.h>
namespace CollinsSoper {

    class PseudoCollinsSoperFrame {
    public:
        using LepCharge = SagittaBiasAnalyzer::LepCharge;

        PseudoCollinsSoperFrame(SagittaBiasAnalyzer& tree, int trk_type, bool pos_lep);

        int trk_type() const;

        ///
        bool charge() const;

        /// Get the shifted 4-moment in the CS frame
        const Vector4D& pos_lep_CS();
        const Vector4D& neg_lep_CS();

    private:
        void update();

        SagittaBiasAnalyzer& m_parent;

        bool m_charge{};
        int m_trk_type{0};

        /// Four momenta of the leptons in the collins soper frame
        Vector4D m_lep_plus{};
        Vector4D m_lep_minus{};

        Long64_t m_last_ev{-1};
        std::unique_ptr<RestFrame> m_cs_frame{};
    };

}  // namespace CollinsSoper

#endif