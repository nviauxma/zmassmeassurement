#ifndef NTAU__SagittaInjector__H
#define NTAU__SagittaInjector__H
#include <memory>

#include "MuonSagittaBias/CollinsSoperUtils.h"
#include "MuonSagittaBias/SagittaBiasAnalyzer.h"
#include "MuonSagittaBias/Utils.h"
///##################################################################################
///     The SagittaInjector emulates the effect on the muon four-momenta from       #
///     dependent & gloabl biases on the measured sagitta                           #
///             s = q / p.                                                          #
///     The charge dependent sagitta bias is assumed to be a global shift           #
///     parametrized as:                                                            #
///             s -> s + q*delta.                                                   #
///     where it follows that the momentum of the particle shifts by                #
///             p --> p / (1 + q * p * delta)                                       #
///                                                                                 #
///     The second bias considered is the magnetic bias. The general formula        #
///     relating the momentum and the magnetic field is given by                    #
///             pT = 0.3 B * R                                                      #
///     an uncertainty on the magnetic field parametrized  (1+m) hence              #
///     propagates to the momentum as                                               #
///             p --> p*(1+m)                                                       #
///                                                                                 #
///     The third considered bias is the radial bias assuming a global inflation    #
///     of all modules along the radial direction while longitudinal dimension  is  #
///     fixed, which is parametrized as                                             #
///               pT  --> pT*(1+m)                                                  #
///               pZ  --> pZ                                                        #
///         cot(eta)  --> cot(eta)*(1+m)                                            #
///                                                                                 #
///                                                                                 #
///     The Sagitta injector can also smear the transverse momentum of the muon     #
///     using a Gaussian (TRandom3) with mean 1 and a width expressed as a poly-    #
///     nomial in powers of pT:                                                     #
///         sigma = a + b*pT + c*pT^{2} + d/pT                                      #
///                                                                                 #
///         a: Multiple scattering                                                  #
///         b: Intrinsic resolution                                                 #
///         d: Energy loss fluctuations                                             #
class SagittaInjector {
public:
    using FourVector = CollinsSoper::Vector4D;
    SagittaInjector(SagittaBiasAnalyzer& bias_tree, const MuonTrk::calib_constants& calib_const);
    virtual ~SagittaInjector() = default;

    /// Full bit mask of the track
    int trk_mask() const;

    /// Ordinary track type
    int trk_type() const;

    /// Get the shifted 4-momenta in the rest frame
    const FourVector& pos_lep();
    const FourVector& neg_lep();
    //// Get the invariant 4-momentum from the two biased leptons
    const FourVector& inv_p4();

    /// Get the shifted 4-moment in the CS frame
    const FourVector& pos_lep_CS();
    const FourVector& neg_lep_CS();

    float pseudo_mass_pos();
    float pseudo_mass_neg();

    float sagitta_bias() const;

    float radial_bias() const;

    float magnetic_bias() const;

    float resolution() const;

private:
    void inject();
    CollinsSoper::PolarVector4D shift_sagitta(const float pt, const float eta, const float phi, const int charge) const;

    float calc_pseudo_M(const FourVector& v1, const FourVector& v2) const;
    SagittaBiasAnalyzer& m_parent_tree;
    Long64_t m_last_cache{-1};

    MuonTrk::calib_constants m_calib{};

    /// Four momenta of the leptons in the lab frame
    FourVector m_p1{};
    FourVector m_p2{};

    std::unique_ptr<CollinsSoper::RestFrame> m_frame{};

    FourVector m_p1_CS{};
    FourVector m_p2_CS{};

    float m_Pseudo_M_pos{-1};
    float m_Pseudo_M_neg{-1};
};

#endif
