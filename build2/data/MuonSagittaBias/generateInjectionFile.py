from ClusterSubmission.Utils import WriteList, ResolvePath
import ROOT
##
####  Simple script to illustrate how a sagitta injection file
##    can be generated for the analysis.

injected = []

for i in range(0, 20, 10):
    calib = ROOT.MuonTrk.calib_constants()
    calib.sagitta_bias = i * 1.e-6
    calib.trk_type = calib.to_trk_type("ID")
    calib.resolution = 0.01
    injected += [calib.to_string()]

WriteList(injected, "MuonSagittaBias/data/bias_injection_file.txt")
