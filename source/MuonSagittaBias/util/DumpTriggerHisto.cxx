#include <MuonSagittaBias/ArgumentParser.h>
#include <MuonSagittaBias/TriggerAnalyzer.h>
#include <MuonSagittaBias/TriggerPlotPruner.h>
#include <MuonSagittaBias/Utils.h>
#include <NtauHelpers/HistoBinConfig.h>
#include <NtauHelpers/OutFileWriter.h>
#include <NtauHelpers/SelectionWithTitle.h>

int main(int argc, char* argv[]) {
    std::string in_file{"trigger_selection.root"};
    std::shared_ptr<TFile> t_file{PlotUtils::Open(in_file)};
    PlotUtils::HistoBinConfig histo_template;
    for (const std::string& resonance : {"ZResonance", "JPsi"}) {
        for (int year = 15; year <= 18; ++year) {
            for (const std::string& sel : {"All", "Exclusive"}) {
                std::cout << "##############################################################################" << std::endl;
                std::cout << "Dump relevant triggers for the year 20" << year << "  " << resonance << "  " << sel << std::endl;
                std::cout << "##############################################################################" << std::endl;
                std::string plot_path = resonance + "/" + sel + "/data" + std::to_string(year) + "/";
                std::shared_ptr<TH1> h_trig = load_histo(plot_path + "Fired", t_file);
                std::shared_ptr<TH1> h_match = load_histo(plot_path + "Matches", t_file);
                std::shared_ptr<TH1> h_prescale = load_histo(plot_path + "PreScales", t_file);
                /// Histogram pointers
                Plot<TH1> plot_trig{TriggerPlotPruner{CopyExisting(*h_trig), CopyExisting(*h_trig), histo_template,
                                                      TriggerPlotPruner::ToReturn::DecisionPlot}};
                Plot<TH1> plot_match{TriggerPlotPruner{CopyExisting(*h_trig), CopyExisting(*h_match), histo_template,
                                                       TriggerPlotPruner::ToReturn::MatchPlot}};
                Plot<TH1> plot_prescale{TriggerPlotPruner{CopyExisting(*h_trig), CopyExisting(*h_prescale), histo_template,
                                                          TriggerPlotPruner::ToReturn::MatchPlot}};

                plot_trig.populate();
                plot_match.populate();
                plot_prescale.populate();

                h_trig = plot_trig.getSharedHisto();
                h_match = plot_match.getSharedHisto();
                h_prescale = plot_prescale.getSharedHisto();
                h_prescale->Divide(h_trig.get());
                size_t max_letters{0};
                const size_t largest_content = h_trig->GetBinContent(1);
                for (int bin = 2; bin < h_trig->GetNbinsX(); ++bin) {
                    max_letters = std::max(max_letters, std::string(h_trig->GetXaxis()->GetBinLabel(bin)).size());
                }
                const int n_digits = std::log10(largest_content) + 1;
                for (int bin = 1; bin < h_trig->GetNbinsX(); ++bin) {
                    std::string label{h_trig->GetXaxis()->GetBinLabel(bin)};
                    Long64_t n_trig(h_trig->GetBinContent(bin)), n_match(h_match->GetBinContent(bin));
                    double n_prescale(h_prescale->GetBinContent(bin));
                    /// What's the actual fraction of the events covered by the trigger
                    const double trig_frac = 100. * h_trig->GetBinContent(bin) / h_trig->GetBinContent(1);
                    const double matc_frac = 100. * h_match->GetBinContent(bin) / h_trig->GetBinContent(bin);

                    std::cout << " --- " << label << WhiteSpaces(max_letters - label.size()) << "     " << n_trig
                              << Form(" (%.1f", trig_frac) << "%)" << WhiteSpaces(n_digits - std::log10(n_trig)) << "     " << n_match
                              << Form(" (%.1f", matc_frac) << "%)" << WhiteSpaces(n_digits - std::log10(n_match)) << "     " << n_prescale
                              << WhiteSpaces(n_digits - std::log10(n_prescale)) << "     " << std::endl;
                }
            }
        }
        std::cout << std::endl << std::endl << std::endl << std::endl;
    }
    return EXIT_SUCCESS;
}