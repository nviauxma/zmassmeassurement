#ifndef NTAU__PlotProvider__H
#define NTAU__PlotProvider__H

#include "MuonSagittaBias/AsymmetryPlots.h"
#include "MuonSagittaBias/PhaseSpaceCuts.h"
#include "MuonSagittaBias/SagittaBiasAnalyzer.h"
#include "NtauHelpers/HistoBinConfig.h"
#include "NtauHelpers/OutFileWriter.h"
#include "NtauHelpers/Utils.h"

/// Small helper class to cache the
///
class PlotFiller {
public:
    PlotFiller(const std::function<int(const TH1*, SagittaBiasAnalyzer&, int)>& fill_func, const std::shared_ptr<TH1>& histo);
    PlotFiller(const std::function<int(const TH1*, SagittaBiasAnalyzer&, int)>& fill_func, PlotUtils::HistoBinConfig histo);

    std::function<int(const TH1*, SagittaBiasAnalyzer&, int)> get_filler() const;

    std::shared_ptr<TH1> get_template() const;

    unsigned int dimension() const;
    std::string name() const;

private:
    std::function<int(const TH1*, SagittaBiasAnalyzer&, int)> m_filler;
    std::shared_ptr<TH1> m_template;
};

///   Helper class to provide the plots for the different muon tracks
///     The Plotrpovider assembles from the sample, the fill function and the template
///     the final Plot instances including selections for forward and backward asymmetry.
///   The
class PlotProvider {
public:
    /// Basic constructor
    ///      sel_name: String naming the selection meant for the saving of the file to disk
    ///      sel_title: Selection title to be plotted on the Canvas. Can contain TLatex formatting
    ///                 characters
    ///     sel_function: Function to evaluate the phase bin selection...
    ///                   The first argument is the refernce to the tree and the second one
    ///                   is the track selection
    PlotProvider(const Sample<SagittaBiasAnalyzer>& smp, const std::function<int(const TH1*, SagittaBiasAnalyzer&, int)>& fill_func,
                 const std::shared_ptr<TH1>& histo, const PhaseSpaceCuts& cut_element);

    PlotProvider(const Sample<SagittaBiasAnalyzer>& smp, const std::function<int(const TH1*, SagittaBiasAnalyzer&, int)>& fill_func,
                 PlotUtils::HistoBinConfig histo, const PhaseSpaceCuts& cut_element);

    PlotProvider(const Sample<SagittaBiasAnalyzer>& smp, const PlotFiller& filler, const PhaseSpaceCuts& cut_element);

    /// Returns the name of the object
    std::string name() const;
    /// Returns the name of the selection only
    std::string selection_name() const;

    /// Returns the selection title of the object
    std::string title() const;
    /// Dimension of the underlying histogram
    unsigned int dimension() const;

    void add_plot(std::vector<int> trk_types, PlotWriter<TH1>& plot_writer, std::pair<float, float> mass_window = {61, 121}) const;
    void add_plot(int trk_type, PlotWriter<TH1>& plot_writer, std::pair<float, float> mass_window = {61, 121}) const;

    void add_asymmetry_plots(int trk_type, PlotWriter<TH1>& plot_writer, std::pair<float, float> mass_window = {61, 121}) const;
    void add_asymmetry_plots(std::vector<int> trk_types, PlotWriter<TH1>& plot_writer,
                             std::pair<float, float> mass_window = {61, 121}) const;

    Plot<TH1> make_forward_plot(int trk_type, std::pair<float, float> mass_window = {61., 121.}) const;
    Plot<TH1> make_backward_plot(int trk_type, std::pair<float, float> mass_window = {61., 121.}) const;
    Plot<TH1> make_plot(int trk_type, std::pair<float, float> mass_window = {61., 121.}) const;

    Plot<TH1> make_asymmetry(int trk_type, std::pair<float, float> mass_window = {61., 121.}) const;

    std::shared_ptr<TH1> get_template() const;
    /// Return the histogram filler
    std::function<int(const TH1*, SagittaBiasAnalyzer&, int)> get_filler() const;

    Sample<SagittaBiasAnalyzer> get_sample() const;

protected:
    std::string selection_name(int trk_type, std::pair<float, float> mass_window) const;

    PlotFormat generate_format(int trk_type) const;

    PlotFillInstructionWithRef<TH1, SagittaBiasAnalyzer> create_plot_filler(
        std::function<int(const TH1*, SagittaBiasAnalyzer&, int)> fill_func, int trk_type, std::shared_ptr<TH1> h_template) const;

    Selection<SagittaBiasAnalyzer> make_cut(int trk_type, std::pair<float, float> mass_window) const;
    Selection<SagittaBiasAnalyzer> make_forward_cut(int trk_type, std::pair<float, float> mass_window) const;
    Selection<SagittaBiasAnalyzer> make_backward_cut(int trk_type, std::pair<float, float> mass_window) const;

private:
    Plot<TH1> make_plot(const Selection<SagittaBiasAnalyzer>& selection, int trk_type) const;
    Sample<SagittaBiasAnalyzer> m_smp;
    PhaseSpaceCuts m_cuts;
    PlotFiller m_filler;
};

#endif
