/// Example of how to use NtupleAnalysisUtils to read / write trees. 

/// We will write and read from a tree with some selection cuts twice: 

/// Once using standard ROOT and once using the NtupleAnalyisisUtils tools.

/// Comparing side by side should give a hint of how the tools are intended 
/// to be used, and which ROOT operations are equivalent to which 
/// operations with the NtupleAnalysisUtils classes. 

#include "NtupleAnalysisUtils/NTAUTopLevelIncludes.h"

///////////////////////////
/// STEP 1: method to write ourselves a small test tree. 
///////////////////////////

void WriteRandomTree_BareROOT (Long64_t nentries, const std::string & fname){
    // create a TFile and TTree
    TFile* myFile = new TFile(fname.c_str(),"RECREATE"); 
    TTree* myTree = new TTree("Tree","Tree");
    myTree->SetDirectory(myFile);

    // now let's prepare a few branches 
    double dummyMass; 
    double dummypt; 
    int dummyChannel; 
    // vector branches are a pain! 
    std::vector<double>* dummyLeptonPt = new std::vector<double>;

    myTree->Branch("dummyMass",&dummyMass);
    myTree->Branch("dummypt",&dummypt);
    myTree->Branch("dummyChannel",&dummyChannel);
    myTree->Branch("dummyLeptonPt",&dummyLeptonPt);

    // prepare to populate the tree with random values
    TRandom3 my_rdm;
    my_rdm.SetSeed(1234); // fix seed to get reproducible results
    for (Long64_t entry = 0; entry < nentries; ++entry){
        dummyMass= my_rdm.Gaus(125,2.5);
        dummypt = my_rdm.Uniform(5.,45.); 
        dummyChannel = my_rdm.Integer(4); 
        dummyLeptonPt->clear();
        for (size_t k = 0; k < 4; ++k){
            dummyLeptonPt->push_back(my_rdm.Uniform(5,40));
        }
        myTree->Fill();
    }

    // and write to disk
    myFile->Write();
    myFile->Close();
    delete dummyLeptonPt;
} 

// for the NtupleAnalysisUtils version, we write a small helper class 
// based on the NtupleBranchMgr. 
// For your actual analysis, you can auto-generate source code for this 
// using the generateBranchManager executable. 

class MyDummyTree : public NtupleBranchMgr{

    public: 
        MyDummyTree(TTree* t): 
            NtupleBranchMgr(t), 
            // here we set the branches to their names and tell them to read from "t" and to attach themselves to this manager 
            dummyMass("dummyMass",t,this),
            dummypt("dummypt",t,this),
            dummyChannel("dummyChannel",t,this),
            dummyLeptonPt("dummyLeptonPt",t,this){
                getMissedBranches(t); // this does not do anything in this special case, but it would otherwise detect any branches in the tree that we did not declare by hand. 
        }
        // this is how we can easily make branches available within the analysis
        NtupleBranch<double> dummyMass;
        NtupleBranch<double> dummypt;
        NtupleBranch<int> dummyChannel;
        // this also works for vectors! For oldschool people who like arrays, they also work.  
        NtupleBranch<std::vector<double> > dummyLeptonPt;
};

// now fill the same dummy file as above using NtupleAnalysisUtils: 
void WriteRandomTree_AnalysisUtils (Long64_t nentries, const std::string & fname){
    // create a TFile and TTree
    TFile* myFile = new TFile(fname.c_str(),"RECREATE"); 
    TTree* myTree = new TTree("Tree","TreeAU");
    myTree->SetDirectory(myFile);

    // note: no need for branch() etc calls, all done for us 
    MyDummyTree tree(myTree); 

    // prepare to populate the tree with random values
    TRandom3 my_rdm;
    my_rdm.SetSeed(1234); // fix seed to get reproducible results
    for (Long64_t entry = 0; entry < nentries; ++entry){
        // almost as before, we just talk to the tree NtupleBranches
        tree.dummyMass.set(my_rdm.Gaus(125,2.5));
        tree.dummypt.set(my_rdm.Uniform(5.,45.));
        tree.dummyChannel.set(my_rdm.Integer(4));
        tree.dummyLeptonPt.set({my_rdm.Uniform(5,40),my_rdm.Uniform(5,40),my_rdm.Uniform(5,40),my_rdm.Uniform(5,40)});
        tree.fill();  // here we could just as well call myTree->Fill(); 
    }
    // and write to disk
    myFile->Write();
    myFile->Close();
} 

///////////////////////////
// STEP 2:  read back our file, filling 4 TH1D* histograms (for each channel) for pt > 15 and plotting them with a ratio pad.
///////////////////////////

// bare ROOT
void ReadAndPlot_BareROOT(const std::string & fname){

    // Get back our tree  
    // load our file 
    TFile* myFile = new TFile(fname.c_str(),"READ"); 
    // load our tree 
    TTree* myTree = nullptr;
    myFile->GetObject("Tree",myTree);
    if (!myTree){
        std::cerr <<"Failed to read our tree"<<std::endl;
    }

    // prepare placeholders
    double dummyMass; 
    double dummypt; 
    int dummyChannel; 
    std::vector<double>* dummyLeptonPt = nullptr;

    // set them to the tree branches
    myTree->SetBranchAddress("dummyMass",&dummyMass);
    myTree->SetBranchAddress("dummypt",&dummypt);
    myTree->SetBranchAddress("dummyChannel",&dummyChannel);
    myTree->SetBranchAddress("dummyLeptonPt",&dummyLeptonPt);

    // NOTE: Here we would also need to set the branch status 
    // for branches we do not need to 0, to speed up reading. 
    // Not used in this example since we don't have unused branches. 

    // book histograms 
    TH1D* h1 = new TH1D("Channel1","Channel1",30,110,140);
    TH1D* h2 = new TH1D("Channel2","Channel2",30,110,140);
    TH1D* h3 = new TH1D("Channel3","Channel3",30,110,140);
    TH1D* h4 = new TH1D("Channel4","Channel4",30,110,140);

    // now loop over the tree... 
    for (Long64_t entry = 0; myTree->GetEntry(entry); ++entry){

        // apply our selection - by hand 
        // note that these cuts are totally made up to demonstrate the concept
        // the way I am cutting here would in a real analysis assume that "dummyLeptonPt" is sorted in pt
        if (dummypt < 15 || dummyLeptonPt->at(0) < 20 || dummyLeptonPt->at(1) < 15 || dummyLeptonPt->at(2) < 10) continue;
        // and fill histos
        if (dummyChannel == 0) h1->Fill(dummyMass);
        if (dummyChannel == 1) h2->Fill(dummyMass);
        if (dummyChannel == 2) h3->Fill(dummyMass);
        if (dummyChannel == 3) h4->Fill(dummyMass);
    }

    // now compute ratios
    TH1D* r21 = dynamic_cast<TH1D*>(h2->Clone("r21"));
    r21->Divide(h2,h1);

    TH1D* r31 = dynamic_cast<TH1D*>(h3->Clone("r31"));
    r31->Divide(h3,h1);

    TH1D* r41 = dynamic_cast<TH1D*>(h4->Clone("r41"));
    r41->Divide(h4,h1);

    // prepare pads and canvas 
    TCanvas* can = new TCanvas("can","can",800,600);
    can->cd();
    TPad* p1 = new TPad("p1","p1",0.,0.4,1,1);
    TPad* p2 = new TPad("p2","p2",0.,0.,1,0.4);

    p2->SetTopMargin(0);
    p1->SetBottomMargin(0);

    p1->Draw();
    can->cd();
    p2->Draw();
    p1->cd();

    // always fun in ROOT: Histo formatting...
    h1->SetLineColor(kRed);
    h2->SetLineColor(kBlue);
    h3->SetLineColor(kOrange-3);
    h4->SetLineColor(kViolet+2);
    h1->SetMarkerColor(kRed);
    h2->SetMarkerColor(kBlue);
    h3->SetMarkerColor(kOrange-3);
    h4->SetMarkerColor(kViolet+2);
    h1->Draw("");
    h2->Draw("SAME");
    h3->Draw("SAME");
    h4->Draw("SAME");

    // add a legend...
    TLegend* leg = new TLegend(0.6,0.6,0.9,0.9);
    leg->AddEntry(h1,"Channel 1","PL");
    leg->AddEntry(h2,"Channel 2","PL");
    leg->AddEntry(h3,"Channel 3","PL");
    leg->AddEntry(h4,"Channel 4","PL");
    leg->SetBorderSize(0);
    leg->SetFillStyle(0);
    leg->Draw();

    // and a label...
    TLatex* tex = new TLatex(0.2,0.9,  "Hi, I am a label");
    tex->SetNDC();
    tex->Draw();


    p2->cd();
    r21->SetLineColor(kBlue);
    r31->SetLineColor(kOrange-3);
    r41->SetLineColor(kViolet+2);
    r21->SetMarkerColor(kBlue);
    r31->SetMarkerColor(kOrange-3);
    r41->SetMarkerColor(kViolet+2);
    r21->Draw("");
    r31->Draw("SAME");
    r41->Draw("SAME");

    can->SaveAs("RootBasedTest.pdf");

}

// NtupleAnalysisUtils
void ReadAndPlot_AnalysisUtils(const std::string & fname){

    // with the NtupleAnalysisUtils, the approach to doing the same as above is slightly different. 
    // Instead of interacting with ROOT ourselves and writing an event loop, we rather 
    // describe what we want to have, and then let the tools of the package 
    // take care of making this happen within ROOT. 

    // use the Sample class to define what we want to run on 
    Sample<MyDummyTree> mySample;
    mySample.addFile(fname,"Tree");    // and add our input file to its list of files - note that we could also define file-specific filtering cuts here

    // now define how our plot/s should be filled.
    // In this example, there is just one kind of plot, which is later filled for 4 different selections. 
    // this is how a typical mass plot would be implemented. 
    // Arguments: Name, fill instruction (here as lambda expression). 
    PlotFillInstruction<TH1D, MyDummyTree> fillMass ([](TH1D* h, MyDummyTree &t){ h->Fill(t.dummyMass());});

    // Now we still need to define selections! 

    // Selections are defined using the "Selection" class, which acts as packaging for an std::function handling the actual cut to apply. 
    // Logical operators are fully supported to construct complex selections from smaller building blocks, and this is widely used. 
    
    // In this line we implement the pt and lepton pt cuts. 
    // We also see how vector branches can be accessed element-wise via operator()(size_t) 
    // The way I am cutting here would in a real analysis assume that "dummyLeptonPt" is sorted in pt! 
    Selection<MyDummyTree> ptCut([](MyDummyTree &t){return (t.dummypt() > 15 && t.dummyLeptonPt(0) > 20 && t.dummyLeptonPt(1) > 15 && t.dummyLeptonPt(2) > 10);});


    // Now we use the logical operators supported by the Selection class to compine the pt cuts 
    // with cuts on the channel number. 
    // Do one such selection for each channel. 
    std::vector<Selection<MyDummyTree>> channelSelections; 
    for (int chan = 0; chan < 4; ++chan){
        channelSelections.push_back(ptCut && Selection<MyDummyTree>([chan](MyDummyTree &t){return (t.dummyChannel() == chan);}));
    }

    // Now, we still need to tell the code how to bin the histogram, and how to label the axes: 
    TH1D href("reference","dummy;mass [GeV]; a.u.",30,110,140);

    // Now we are all set. 
    // We define the actual Plots to draw in our canvas by specifying 
    //      - binning and axis labels
    //      - from what they should be filled,
    //      - which selection should be used when filling 
    //      - what should be done for the filling itselfand how ot actually fill them. 
    // using the previously defined building blocks. 
    // In addition, we can pass along formatting and legend title information to make our plots fit 
    // neatly into a canvas!   
    // 
    // Arguments: <reference histogram> <Sample> <Selection> <Plot> <Legend Title> <Legend display option> <formatting>
    Plot<TH1D> plot_Ch1 (RunHistoFiller(&href, 
        mySample, channelSelections[0], fillMass), 
        PlotFormat().MarkerStyle(kFullDotLarge).Color(kRed).FillStyle(1001).FillAlpha(0.3).LegendTitle("Channel 1").LegendOption("PL"));
    Plot<TH1D> plot_Ch2 (RunHistoFiller(&href, 
        mySample, channelSelections[1], fillMass), 
        PlotFormat().MarkerStyle(kFullDotLarge).Color(kBlue).LegendTitle("Channel 2").LegendOption("PL"));
    Plot<TH1D> plot_Ch3 (RunHistoFiller(&href, 
        mySample, channelSelections[2], fillMass), 
        PlotFormat().MarkerStyle(kFullDotLarge).Color(kOrange-3).LegendTitle("Channel 3").LegendOption("PL"));
    Plot<TH1D> plot_Ch4 (RunHistoFiller(&href, 
        mySample, channelSelections[3], fillMass), 
        PlotFormat().MarkerStyle(kFullDotLarge).Color(kViolet+2).LegendTitle("Channel 4").LegendOption("PL"));

    // [ OPTIONAL ] Populate the plots we just defined. 
    // This will trigger an event loop within a HistoFiller instance
    // which has been automatically created for us when we booked the Plots.  
    // the argument tells the plots to also directly apply the specified formatting. 
    // This will otherwise also be done for you as soon as you invoke the (), -> or getHisto
    // operators of a plot the first time in a non-const context. 

    // If you need to access the content in a const-contex, a manual call to populate() ensures 
    // your plot is ready to go!

    // plot_Ch1.populate(true);
    // plot_Ch2.populate(true);
    // plot_Ch3.populate(true);
    // plot_Ch4.populate(true);


    // Get a canvas with two pads made for us. 
    // This will also change the pad margins a bit 
    // compared to the ROOT standalone version 
    auto myMultiPadCanvas = PlotUtils::prepareTwoPadCanvas(); // the MultiPadCanvas is a simple struct containing a TCanvas* ("can") and several TPad* s ("p1","p2",...)  
    myMultiPadCanvas.getPad(0)->cd();
    
    /// here the event loop will trigger: 
    std::cout << "Expecting event loop now..."<<std::endl;
    plot_Ch1->Draw("");           // remember, the Plot class works like a smart pointer to a ROOT histo.
    std::cout << "Did you see it?"<<std::endl;
    plot_Ch2->Draw("SAME");       // So we can always fall back to doing things ROOT Style if we feel like it.
    plot_Ch3->Draw("SAME");       // And here, for drawing, there is nothing to be added to ROOT's existing functionality 
    plot_Ch4->Draw("SAME");

    // Prepare ratios
    // This will also take care of formatting, based on numerator hist.
    // Options exist for different error treatment (for example, efficiency errors)
    Plot<TH1> r21 = PlotUtils::getRatio(plot_Ch2,plot_Ch1);
    Plot<TH1> r31 = PlotUtils::getRatio(plot_Ch3,plot_Ch1);
    Plot<TH1> r41 = PlotUtils::getRatio(plot_Ch4,plot_Ch1);

    // Add the legend via plot utils.
    // Notice that we do not need to format the fill area / borders, this is done for us. 
    PlotUtils::drawLegend(std::vector<Plot<TH1D>>{plot_Ch1, plot_Ch2, plot_Ch3, plot_Ch4}, CanvasOptions().LegendStartX(0.6).LegendStartY(0.6).LegendEndX(0.9).LegendEndY(0.9));    // the method returns a TLegend*, which you could use for further tweaking of the legend. 

    // Drawing labels is also fairly easy with the PlotUtils class. Drawing is automatically done in NDC coordinates
    PlotUtils::drawTLatex(0.2,0.85, "Hi, I am a label");        // also available: DrawAtlas(), DrawLumiSqrtS() 

    // Time for the ratio pad
    myMultiPadCanvas.getPad(1)->cd();
    r21->Draw("");
    // fancy a line at ratio == 1? Also easy with the utils
    PlotUtils::drawLine(r21->GetXaxis()->GetXmin(), 1, r21->GetXaxis()->GetXmax(), 1);
    r21->Draw("SAME");
    r31->Draw("SAME");
    r41->Draw("SAME");

    // saving can also be done via PlotUtils to avoid having c_str() everywhere... 
    // supports different file formats in a single call 
    // PDF is done by default
     // normally, this will write into a "TestArea/../Plots/<date>" folder, but here we want the simple version,
    // this is done via the third argument (".")
    PlotUtils::saveCanvas(myMultiPadCanvas.getCanvas(), "AnaUtilsBasedTest",CanvasOptions().OutputDir(".").OutputFormats({"pdf","png","jpg","C"}));       
}


// NtupleAnalysisUtils, using some additional standardised plotting tools
void ReadAndPlot_AnalysisUtilsWithPlotContent(const std::string & fname){

    // This is exactly like the example above, except that we use the PlotContent class and 
    // a standard plotting method to make things really short! 

    Sample<MyDummyTree> mySample;
    mySample.addFile(fname,"Tree");   

    PlotFillInstruction<TH1D, MyDummyTree> fillMass ([](TH1D* h, MyDummyTree &t){ h->Fill(t.dummyMass());});

    PlotFillInstruction<TH1D, MyDummyTree> fillDummyLeptonPtAbove15 ([](MyDummyTree &t){return t.dummyLeptonPt().size();},  [](MyDummyTree &t, size_t index){ return t.dummyLeptonPt(index)>15;}, [](TH1D* h, MyDummyTree &t, size_t index){h->Fill(t.dummyLeptonPt(index));});


    Selection<MyDummyTree> ptCut([](MyDummyTree &t){return (t.dummypt() > 15 && t.dummyLeptonPt(0) > 20 && t.dummyLeptonPt(1) > 15 && t.dummyLeptonPt(2) > 10);});

    std::vector<Selection<MyDummyTree>> channelSelections; 
    for (int chan = 0; chan < 4; ++chan){
        channelSelections.push_back(ptCut && Selection<MyDummyTree>([chan](MyDummyTree &t){return (t.dummyChannel() == chan);}));
    }

    TH1D href("reference","dummy;mass [GeV]; a.u.",30,110,140);
    TH1D hpt("pt","dummy;dummy pt [GeV]; a.u.",40,5,45);

    /// example for writing everything into a multi-page PDF. Just call this guy 
    /// and then pass the handle to the PlotContent
    auto multiPagePdfExample = PlotUtils::startMultiPagePdfFile("MultiPageOutput"); 

    PlotContent<TH1D> myPlots (
    {
        Plot<TH1D> (RunHistoFiller(&href, 
            mySample, channelSelections[0], fillMass), 
            PlotFormat().MarkerStyle(kFullDotLarge).Color(kRed).FillStyle(1001).FillAlpha(0.3).LegendTitle("Channel 1").LegendOption("PL")),
        Plot<TH1D> (RunHistoFiller(&href, 
            mySample, channelSelections[1], fillMass), 
            PlotFormat().MarkerStyle(kFullDotLarge).Color(kBlue).LegendTitle("Channel 2").LegendOption("PL")),
        Plot<TH1D> (RunHistoFiller(&href, 
            mySample, channelSelections[2], fillMass), 
            PlotFormat().MarkerStyle(kFullDotLarge).Color(kOrange-3).LegendTitle("Channel 3").LegendOption("PL")),
        Plot<TH1D> (RunHistoFiller(&href, 
            mySample, channelSelections[3], fillMass), 
            PlotFormat().MarkerStyle(kFullDotLarge).Color(kViolet+2).LegendTitle("Channel 4").LegendOption("PL"))},
        {"Hi, I am a label"}, "AnaUtilsBasedTest_withPlotContent",multiPagePdfExample, 
            CanvasOptions().OutputDir(".").XAxis(AxisConfig().ExtraTitleOffset(1.3)));


    /// here we show how we can specify the ratios to be drawn.
    /// In this case, an empty vector means no ratios at all! 
    /// draw1D is clever enough to recognize this and avoid 
    /// an empty second pad... 
    PlotContent<TH1D> myPlotsNoRatio (
    {
        Plot<TH1D> (RunHistoFiller(&href, 
            mySample, channelSelections[0], fillMass), 
            PlotFormat().MarkerStyle(kFullDotLarge).Color(kRed).FillStyle(1001).FillAlpha(0.3).LegendTitle("Channel 1").LegendOption("PL")),
        Plot<TH1D> (RunHistoFiller(&href, 
            mySample, channelSelections[1], fillMass), 
            PlotFormat().MarkerStyle(kFullDotLarge).Color(kBlue).LegendTitle("Channel 2").LegendOption("PL")),
        Plot<TH1D> (RunHistoFiller(&href, 
            mySample, channelSelections[2], fillMass), 
            PlotFormat().MarkerStyle(kFullDotLarge).Color(kOrange-3).LegendTitle("Channel 3").LegendOption("PL")),
        Plot<TH1D> (RunHistoFiller(&href, 
            mySample, channelSelections[3], fillMass), 
            PlotFormat().MarkerStyle(kFullDotLarge).Color(kViolet+2).LegendTitle("Channel 4").LegendOption("PL"))},
            {}, /// this vector contains the list of desired ratios
        {"Hi, I am a label"}, "AnaUtilsBasedTest_withPlotContent_noRatio",multiPagePdfExample, 
            CanvasOptions().OutputDir(".").LegendStartX(-0.25).LegendEndX(-0.1));


    // here we do something new and demonstrate how to do per-entry filling in a vector 

    PlotContent<TH1D> myPlotsLeptonPtWithCut(
    {
        Plot<TH1D>(RunHistoFiller(&hpt,
            mySample, channelSelections[0], fillDummyLeptonPtAbove15), 
            PlotFormat().MarkerStyle(kFullDotLarge).Color(kRed).FillStyle(1001).FillAlpha(0.3).LegendTitle("Channel 1").LegendOption("PL")),
        Plot<TH1D> (RunHistoFiller(&hpt,
            mySample, channelSelections[1], fillDummyLeptonPtAbove15), 
            PlotFormat().MarkerStyle(kFullDotLarge).Color(kBlue).LegendTitle("Channel 2").LegendOption("PL")),
        Plot<TH1D> (RunHistoFiller(&hpt,
            mySample, channelSelections[2], fillDummyLeptonPtAbove15), 
            PlotFormat().MarkerStyle(kFullDotLarge).Color(kOrange-3).LegendTitle("Channel 3").LegendOption("PL")),
        Plot<TH1D> (RunHistoFiller(&hpt,
            mySample, channelSelections[3], fillDummyLeptonPtAbove15), 
            PlotFormat().MarkerStyle(kFullDotLarge).Color(kViolet+2).LegendTitle("Channel 4").LegendOption("PL"))},
        {"Hi, I am a label"}, "AnaUtilsBasedTest_withPlotContent_DummyLeptonPt",multiPagePdfExample, 
            CanvasOptions().OutputDir(".").LegendStartX(-0.25).LegendEndX(-0.1));

    /// Notice when running that this will NOT trigger a new event loop, 
    /// since the histos were filled already in the previous example run 

    std::cout << "Expecting event loop now..."<<std::endl;
    DefaultPlotting::draw1D(myPlots);  
    /// This one WILL trigger a loop since lepton pt is new. 
    /// It will also make you feel guilty for adding plots after the first fill ;-) 
    DefaultPlotting::draw1D(myPlotsLeptonPtWithCut);  
    DefaultPlotting::draw1D(myPlotsNoRatio);  // life can be so easy :) 

    Plot<TH1D> dummyfromfile(LoadFromFile<TH1D>("some name", " some obj"), PlotFormat());
}



int main (int, char**){

    // load the ATLAS Style (also supplied in NtupleAnalysisUtils)
    SetAtlasStyle();

    // Write the example tree twice 
    WriteRandomTree_BareROOT(1000, "DummyTestTree_fromROOT.root");
    WriteRandomTree_AnalysisUtils(1000, "DummyTestTree_fromAnalysisUtils.root");
    // read it back and plot it
    ReadAndPlot_BareROOT("DummyTestTree_fromROOT.root");
    ReadAndPlot_AnalysisUtils("DummyTestTree_fromAnalysisUtils.root");
    ReadAndPlot_AnalysisUtilsWithPlotContent("DummyTestTree_fromAnalysisUtils.root");
    

    return 0;
}
