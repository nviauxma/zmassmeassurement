#include <NtauHelpers/PermutationUtils.h>
#include <NtauHelpers/Utils.h>
//##################################################################
//                      DrawElementsFromSet
//##################################################################
DrawElementsFromSet::DrawElementsFromSet(unsigned int N, unsigned int M, unsigned int offset) :
    m_N(N), m_M(M), m_offset(offset), m_size(0), m_n_minus1(nullptr), m_borders() {
    /// Use the identity (N, M) = (N, N-M)
    ///  (N, K) = sum_{i=K)^{N-1} (I, K)
    m_borders.reserve(N - M);
    for (unsigned int i = N - 1; i >= M - 1; --i) {
        m_size += binomial(i, M - 1);
        m_borders.push_back(m_size);
        if (i == 0) break;
    }
    if (N != M && M > 1) m_n_minus1 = std::make_shared<DrawElementsFromSet>(N - 1, M - 1, 0);
}
unsigned int DrawElementsFromSet::get_zeroth(unsigned int ele) const {
    for (unsigned int k = 0; k < m_borders.size(); ++k) {
        if (m_borders[k] > ele) return k;
    }
    throw std::range_error(Form("(%u, %u) element %u is out of range.", n(), m(), ele));
    return -1;
}
unsigned int DrawElementsFromSet::value_in_tuple(unsigned int ele, unsigned int pos) const {
    return m_offset + value_in_tuple(ele, pos, 0);
}
unsigned int DrawElementsFromSet::value_in_tuple(unsigned int ele, unsigned int pos, unsigned int min) const {
    if (pos >= m()) { throw std::range_error(Form("You can only draw %u elements. Why do you try %u?", m(), pos)); }
    /// Special case where N out of N elements are drawn. There is only one permutation possible
    if (n() == m()) return pos;
    unsigned int zero = get_zeroth(ele);
    if (pos == 0) { return zero + min; }
    unsigned int it_to_next = m_borders[0] - (m_borders[zero] - ele);
    return m_n_minus1->value_in_tuple(it_to_next, pos - 1, min + 1);
}
unsigned int DrawElementsFromSet::n() const { return m_N; }
unsigned int DrawElementsFromSet::m() const { return m_M; }
unsigned int DrawElementsFromSet::size() const { return m_size; }
unsigned int DrawElementsFromSet::offset() const { return m_offset; }
std::vector<unsigned int> DrawElementsFromSet::draw(unsigned int ele) const {
    if (ele >= size()) {
        throw std::range_error(
            Form("There are only %u combinations to draw %u elements out of %u, while you're asking for the %u-th element.", size(), n(),
                 m(), ele + 1));
    }
    std::vector<unsigned int> x(m(), 0);
    for (unsigned int i = 0; i < m(); ++i) { x[i] = value_in_tuple(ele, i); }
    return x;
}
