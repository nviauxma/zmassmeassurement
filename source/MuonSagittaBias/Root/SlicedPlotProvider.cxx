#include <MuonSagittaBias/SlicedPlotProvider.h>
#include <MuonSagittaBias/Utils.h>

std::shared_ptr<TH1> SlicedPostProcessing::populate() {
    m_populator->populate();
    std::shared_ptr<TH1> unsliced = m_populator->getSharedHisto();
    std::shared_ptr<TH1> projected = clone(m_template, true);

    const unsigned int num_bins = PlotUtils::getNbins(m_template.get());
    for (unsigned int y_bin = 0; y_bin < num_bins; ++y_bin) {
        projected->SetBinContent(y_bin, unsliced->GetBinContent(m_slicing_bin, y_bin));
        projected->SetBinError(y_bin, unsliced->GetBinError(m_slicing_bin, y_bin));
    }
    copy_styling(m_template, projected);
    return projected;
}
std::shared_ptr<IPlotPopulator<TH1>> SlicedPostProcessing::clonePopulator() const { return std::make_shared<SlicedPostProcessing>(*this); }

SlicedPostProcessing::SlicedPostProcessing(const std::shared_ptr<TH1>& variable_template, std::shared_ptr<Plot<TH1>> populator,
                                           int bin_number) :
    m_template{variable_template}, m_populator{populator}, m_slicing_bin{bin_number} {}

SlicedPlotProvider::SlicedPlotProvider(const Sample<SagittaBiasAnalyzer>& smp,
                                       const std::function<int(const TH1*, SagittaBiasAnalyzer&, int)>& fill_func,
                                       const std::shared_ptr<TH1>& histo, const PhaseSpaceCuts& cut_element) :
    PlotProvider(smp, fill_func, histo, cut_element) {}

SlicedPlotProvider::SlicedPlotProvider(const Sample<SagittaBiasAnalyzer>& smp,
                                       const std::function<int(const TH1*, SagittaBiasAnalyzer&, int)>& fill_func,
                                       PlotUtils::HistoBinConfig histo, const PhaseSpaceCuts& cut_element) :
    PlotProvider(smp, fill_func, histo, cut_element) {}

SlicedPlotProvider::SlicedPlotProvider(const Sample<SagittaBiasAnalyzer>& smp, const PlotFiller& filler,
                                       const PhaseSpaceCuts& cut_element) :
    PlotProvider(smp, filler, cut_element) {}

std::vector<Plot<TH1>> SlicedPlotProvider::get_forward_slices(const PlotFiller& provider, int trk_type,
                                                              std::pair<float, float> mass_window) {
    return get_slices(provider, trk_type, make_forward_cut(trk_type, mass_window));
}
std::vector<Plot<TH1>> SlicedPlotProvider::get_backward_slices(const PlotFiller& provider, int trk_type,
                                                               std::pair<float, float> mass_window) {
    return get_slices(provider, trk_type, make_backward_cut(trk_type, mass_window));
}
std::vector<Plot<TH1>> SlicedPlotProvider::get_slices(const PlotFiller& provider, int trk_type, std::pair<float, float> mass_window) {
    return get_slices(provider, trk_type, make_cut(trk_type, mass_window));
}

std::vector<Plot<TH1>> SlicedPlotProvider::get_asymmetry_slices(const PlotFiller& provider, int trk_type,
                                                                std::pair<float, float> mass_window) {
    std::vector<Plot<TH1>> forward = get_forward_slices(provider, trk_type, mass_window);
    std::vector<Plot<TH1>> backward = get_backward_slices(provider, trk_type, mass_window);
    std::vector<Plot<TH1>> plots;
    for (size_t p = 0; p < forward.size(); ++p) {
        plots.emplace_back(AsymmetryPlot<TH1>{name(), forward[p], backward[p]}, generate_format(trk_type));
    }
    return plots;
}
std::vector<Plot<TH1>> SlicedPlotProvider::get_slices(const PlotFiller& provider, int trk_type, Selection<SagittaBiasAnalyzer> selection) {
    /// Retrieve the template according to which the Filler will be sliced

    std::shared_ptr<TH1> sliced_template = get_template();
    std::shared_ptr<TH1> template_of_interest = provider.get_template();

    const size_t slicing_bins = PlotUtils::getNbins(sliced_template.get());
    const size_t interes_bins = PlotUtils::getNbins(template_of_interest.get());
    const std::string tmp_name = name() + "_BinnedIn_" + provider.name();
    std::shared_ptr<TH1> combined_template =
        std::make_shared<TH2D>(tmp_name.c_str(), "", slicing_bins, 0, slicing_bins, interes_bins, 0, interes_bins);

    std::function<int(const TH1*, SagittaBiasAnalyzer&, int)> slice_func = get_filler();
    std::function<int(const TH1*, SagittaBiasAnalyzer&, int)> interest_func = provider.get_filler();
    std::function<int(const TH1*, SagittaBiasAnalyzer&, int)> common_func =
        [slice_func, interest_func, sliced_template, template_of_interest](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
            const int slice_bin = slice_func(sliced_template.get(), t, trk_type);
            const int interest_bin = interest_func(template_of_interest.get(), t, trk_type);
            return h->GetBin(slice_bin, interest_bin);
        };

    /// now pack everything into the PlotFiller
    PlotFillInstructionWithRef<TH1, SagittaBiasAnalyzer> unsliced_filler = create_plot_filler(common_func, trk_type, combined_template);

    std::shared_ptr<Plot<TH1>> unsliced_plot;

    RunHistoFiller<TH1, SagittaBiasAnalyzer> histo_populator{get_sample(), selection, unsliced_filler};
    std::map<ObjectID, std::shared_ptr<Plot<TH1>>>::const_iterator itr = m_cached_plots.find(histo_populator.getID());
    if (m_cached_plots.find(histo_populator.getID()) == m_cached_plots.end()) {
        ObjectID id = histo_populator.getID();
        unsliced_plot = std ::make_shared<Plot<TH1>>(std::move(histo_populator));
        m_cached_plots[id] = unsliced_plot;

    } else
        unsliced_plot = itr->second;

    std::vector<Plot<TH1>> out_vec;
    std::vector<int> vis_bins = visibileBinsVector(sliced_template);

    for (int bin : vis_bins) {
        out_vec.emplace_back(SlicedPostProcessing(template_of_interest, unsliced_plot, bin), generate_format(trk_type));
    }
    return out_vec;
}

std::vector<PlotContent<TH1>> SlicedPlotProvider::make_plots(const PlotFiller& provider, const std::vector<int>& trk_types,
                                                             const std::vector<RatioEntry>& ratios, std::vector<std::string> labels,
                                                             const std::string& plot_name, std::shared_ptr<MultiPagePdfHandle> multiPagePdf,
                                                             const CanvasOptions& opts, std::pair<float, float> mass_window) {
    /// Generate a map of all plots containing the corresponding track types
    std::map<int, std::vector<Plot<TH1>>> plot_map;
    for (int trk : trk_types) { plot_map[trk] = get_slices(provider, trk, mass_window); }
    return make_plots(plot_map, ratios, labels, plot_name + "_" + provider.name(), multiPagePdf, opts);
}

std::vector<PlotContent<TH1>> SlicedPlotProvider::make_asymmetry_plots(const PlotFiller& provider, const std::vector<int>& trk_types,
                                                                       const std::vector<RatioEntry>& ratios,
                                                                       std::vector<std::string> labels, const std::string& plot_name,
                                                                       std::shared_ptr<MultiPagePdfHandle> multiPagePdf,
                                                                       const CanvasOptions& opts, std::pair<float, float> mass_window) {
    /// Generate a map of all plots containing the corresponding track types
    std::map<int, std::vector<Plot<TH1>>> plot_map;
    for (int trk : trk_types) { plot_map[trk] = get_asymmetry_slices(provider, trk, mass_window); }
    return make_plots(plot_map, ratios, labels, plot_name + "_" + provider.name(), multiPagePdf, opts);
}

std::vector<PlotContent<TH1>> SlicedPlotProvider::make_plots(const std::map<int, std::vector<Plot<TH1>>>& plot_map,
                                                             const std::vector<RatioEntry>& ratios, std::vector<std::string> labels,
                                                             const std::string& plot_name, std::shared_ptr<MultiPagePdfHandle> multiPagePdf,
                                                             const CanvasOptions& opts) {
    /// Helper function for a better assignment of the plots
    auto make_plot_vec = [&plot_map](unsigned int vec_entry) {
        std::vector<Plot<TH1>> vec;
        for (const auto itr : plot_map) { vec.push_back(itr.second[vec_entry]); }
        return vec;
    };

    std::vector<PlotContent<TH1>> plots;
    std::vector<int> vis_bins = visibileBinsVector(get_template());
    for (unsigned int b = 0; b < vis_bins.size(); ++b) {
        // if (PlotUtils::isOverflowBin(get_template().get(), bin)) continue;
        plots += PlotContent<TH1>(make_plot_vec(b), ratios, labels + get_boundaries_legend(vis_bins[b]),
                                  plot_name + name() + "_" + std::to_string(vis_bins[b]), multiPagePdf, opts);
    }
    return plots;
}

std::string SlicedPlotProvider::legend_bin_boundaries(const TAxis* a, int axis_bin) const {
    if (!a || PlotUtils::isOverflowBin(a, axis_bin)) return std::string{};
    std::string axis_title{a->GetTitle()};
    if (axis_title.empty()) return axis_title;
    return Form("%s#in[%.2f;%.2f]", axis_title.substr(0, axis_title.rfind("[")).c_str(), a->GetBinLowEdge(axis_bin),
                a->GetBinUpEdge(axis_bin));
}

std::string SlicedPlotProvider::file_bin_boundaries(const TAxis* a, int axis_bin) const {
    if (!a || PlotUtils::isOverflowBin(a, axis_bin)) return std::string{};
    std::string axis_title{a->GetTitle()};
    if (axis_title.empty()) return axis_title;
    /// Remove the units
    axis_title = axis_title.substr(0, axis_title.rfind("["));
    /// Remove first all common latex symbols
    axis_title = PlotUtils::pruneUnwantedCharacters(axis_title, {" ", "/", "\\", ";",
                                                                 ","
                                                                 "_{",
                                                                 "^{", "#", "[", "]", "{", "}", "^", "@", ")"});
    axis_title = PlotUtils::ReplaceExpInString(axis_title, "+", "Plus");
    axis_title = PlotUtils::ReplaceExpInString(axis_title, "-", "Minus");
    // Upper the letter
    size_t brack_pos{0};
    while ((brack_pos = axis_title.find("(", brack_pos)) < axis_title.size() - 1) {
        ++brack_pos;
        axis_title[brack_pos] = std::toupper(axis_title[brack_pos]);
    }
    axis_title = PlotUtils::ReplaceExpInString(axis_title, "(", "");
    return Form("%s_inRANGE_%.2f_%.2f", axis_title.c_str(), a->GetBinLowEdge(axis_bin), a->GetBinUpEdge(axis_bin));
}

std::vector<std::string> SlicedPlotProvider::get_boundaries_legend(int bin) const {
    int x{0}, y{0}, z{0};
    get_template()->GetBinXYZ(bin, x, y, z);
    std::vector<std::string> legend{legend_bin_boundaries(get_template()->GetXaxis(), x),
                                    legend_bin_boundaries(dimension() > 1 ? get_template()->GetYaxis() : nullptr, y),
                                    legend_bin_boundaries(dimension() > 2 ? get_template()->GetZaxis() : nullptr, z)

    };
    EraseFromVector<std::string>(legend, [](const std::string& str) { return str.empty(); });
    return legend;
}
std::string SlicedPlotProvider::get_boundaries_file(int bin) const {
    int x{0}, y{0}, z{0};
    get_template()->GetBinXYZ(bin, x, y, z);
    std::vector<std::string> file_boundaries{file_bin_boundaries(get_template()->GetXaxis(), x),
                                             file_bin_boundaries(dimension() > 1 ? get_template()->GetYaxis() : nullptr, y),
                                             file_bin_boundaries(dimension() > 2 ? get_template()->GetZaxis() : nullptr, z)

    };
    EraseFromVector<std::string>(file_boundaries, [](const std::string& str) { return str.empty(); });

    if (file_boundaries.empty()) return "KinSel_GlobalBin_" + std::to_string(bin);
    std::string str{"KinSel_"};
    for (size_t b = 0; b < file_boundaries.size(); ++b) {
        if (b) str += "__";
        str += file_boundaries[b];
    }
    return str;
}

void SlicedPlotProvider::add_plot(const PlotFiller& provider, std::vector<int> trk_types, PlotWriter<TH1>& plot_writer,
                                  std::pair<float, float> mass_window) {
    ClearFromDuplicates(trk_types);
    for (const int type : trk_types) { add_plot(provider, type, plot_writer, mass_window); }
}

void SlicedPlotProvider::add_plot(const PlotFiller& provider, int trk_type, PlotWriter<TH1>& plot_writer,
                                  std::pair<float, float> mass_window) {
    /// Okay let's agree on a general folder strucutre to duump the fit templates
    ///  Trk_Type (Original) /<Histogram_name>/Selection/Mass_Window/Template_<Sagitta>_SIGMA_<RESOL>

    std::vector<Plot<TH1>> plots = get_slices(provider, trk_type, mass_window);

    std::vector<int> vis_bins = visibileBinsVector(get_template());
    int injected = SagittaBiasAnalyzer::inject_from_type(trk_type);
    const SagittaBiasAnalyzer::calib_constants c =
        injected != -1 ? SagittaBiasAnalyzer::sagitta_injections()[injected] : SagittaBiasAnalyzer::calib_constants{trk_type};
    const std::string histo_name = "Template_" + c.to_string().substr(c.to_string().find("_") + 1);
    const std::string slimmed_trk_name = MuonTrk::to_string(trk_type & MuonTrk::TrackType::OrdinaryTrks);

    for (unsigned int b = 0; b < vis_bins.size(); ++b) {
        const std::string pl_name = slimmed_trk_name + "/" + provider.name() + "/" + name() + "/" + get_boundaries_file(vis_bins[b]) + "/" +
                                    Form("MLL_%.0f_%.0f", mass_window.first, mass_window.second) + "/" + histo_name;
        plot_writer.add_plot(plots[b], pl_name);
    }
}

void SlicedPlotProvider::add_asymmetry_plots(const PlotFiller& provider, std::vector<int> trk_types, PlotWriter<TH1>& plot_writer,
                                             std::pair<float, float> mass_window) {
    ClearFromDuplicates(trk_types);
    for (const int type : trk_types) { add_asymmetry_plots(provider, type, plot_writer, mass_window); }
}

void SlicedPlotProvider::add_asymmetry_plots(const PlotFiller& provider, int trk_type, PlotWriter<TH1>& plot_writer,
                                             std::pair<float, float> mass_window) {
    /// Okay let's agree on a general folder strucutre to duump the fit templates
    ///  Trk_Type (Original) /<Histogram_name>/Selection/Mass_Window/Template_<Sagitta>_SIGMA_<RESOL>

    std::vector<int> vis_bins = visibileBinsVector(get_template());
    int injected = SagittaBiasAnalyzer::inject_from_type(trk_type);
    const SagittaBiasAnalyzer::calib_constants c =
        injected != -1 ? SagittaBiasAnalyzer::sagitta_injections()[injected] : SagittaBiasAnalyzer::calib_constants{trk_type};
    const std::string histo_name = "Template_" + c.to_string().substr(c.to_string().find("_") + 1);
    const std::string slimmed_trk_name = MuonTrk::to_string(trk_type & MuonTrk::TrackType::OrdinaryTrks);

    std::vector<Plot<TH1>> forward = get_forward_slices(provider, trk_type, mass_window);
    std::vector<Plot<TH1>> backward = get_backward_slices(provider, trk_type, mass_window);

    for (unsigned int b = 0; b < vis_bins.size(); ++b) {
        const std::string pl_name_fw = slimmed_trk_name + "/" + provider.name() + "/" + name() + "/" + get_boundaries_file(vis_bins[b]) +
                                       "/" + Form("MLL_%.0f_%.0f", mass_window.first, mass_window.second) + "/Forward/" + histo_name;
        const std::string pl_name_bw = slimmed_trk_name + "/" + provider.name() + "/" + name() + "/" + get_boundaries_file(vis_bins[b]) +
                                       "/" + Form("MLL_%.0f_%.0f", mass_window.first, mass_window.second) + "/Backward/" + histo_name;

        plot_writer.add_plot(forward[b], pl_name_fw);
        plot_writer.add_plot(backward[b], pl_name_bw);
    }
}
