#include <MuonSagittaBias/ArgumentParser.h>
#include <MuonSagittaBias/PhaseSpaceCuts.h>
#include <MuonSagittaBias/PlotProvider.h>
#include <MuonSagittaBias/SagittaBiasAnalyzer.h>

// STILL NEED TO NORMALIZE TO UNIT AREA
// include deltaZ0 quality cut?

int main(int argc, char* argv[]) {
    /// Standard piece of code to setup the analysis...
    ArgumentParser parser;
    parser.summary_pdf("q_over_p_plots").make_root_file(false);

    if (!parser.parse_args(argc, argv)) return EXIT_FAILURE;

    Sample<SagittaBiasAnalyzer> sample_to_process = parser.prepare_sample<SagittaBiasAnalyzer>();

    CanvasOptions canvas_opts = CanvasOptions()
                                    .RatioAxis(AxisConfig().Title("efficiency").ExtraTitleOffset(0.1))
                                    .YAxis(AxisConfig().Title("Entries").ExtraTitleOffset(0.4).TopPadding(0.7))
                                    // .XAxis(AxisConfig().ExtraTitleOffset(1.3))
                                    .LabelStatusTag("Simulation Internal")
                                    .OutputDir(parser.out_dir());

    std::shared_ptr<MultiPagePdfHandle> multi_page = PlotUtils::startMultiPagePdfFile(parser.summary_pdf(), canvas_opts);

    PhaseSpaceCuts all_pairs("all_pairs", "all_pairs", [](SagittaBiasAnalyzer&, int) -> bool { return true; });
    //&& t.delta_z0(trk_type) < 1 && t.delta_z0(trk_type) < 1

    std::vector<PlotFiller> fillers;

    fillers += PlotFiller(
        [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
            return h->GetXaxis()->FindBin(std::abs(t.err_q_over_p(trk_type, true) / t.q_over_p(trk_type, true)));
        },
        PlotUtils::HistoBinConfig()
            .name("fractional_err_q_over_p_pos")
            .x_title(Form("(q/p err) / (q/p)   (pos)"))
            .x_min(0)
            .x_max(0.1)
            .x_num_bins(1000));

    fillers += PlotFiller(
        [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
            return h->GetXaxis()->FindBin(std::abs(t.err_q_over_p(trk_type, false) / t.q_over_p(trk_type, false)));
        },
        PlotUtils::HistoBinConfig()
            .name("fractional_err_q_over_p_neg")
            .x_title(Form("(q/p err) / (q/p)   (neg)"))
            .x_min(0)
            .x_max(0.1)
            .x_num_bins(1000));

    std::vector<PlotContent<TH1>> plot_contents;
    for (const PlotFiller& fill : fillers) {
        PlotProvider all_prov{sample_to_process, fill, all_pairs};

        Plot<TH1> all_pl = all_prov.make_plot(MuonTrk::TrackType::IDTrk, {2.7, 3.5});
        all_pl.plotFormat().Color(kBlack).LegendTitle("All events");

        plot_contents += PlotContent<TH1>{{all_pl}, {"q/p significance"}, "selections_" + fill.name(), multi_page, canvas_opts};
    }

    for (auto& pc : plot_contents) {
        if (count_non_empty(pc)) DefaultPlotting::draw1D(pc);
    }

    return EXIT_SUCCESS;
}
