#ifndef NTAUHELPERS_PLOTPSTPROCESSOR_IXX
#define NTAUHELPERS_PLOTPSTPROCESSOR_IXX
#include "NtauHelpers/HistoUtils.h"
#include "NtupleAnalysisUtils/Helpers/HistoManipulation.h"
#include "NtupleAnalysisUtils/Plot/PostProcessingPopulators.h"

///#####################################################
///                 OverFlowPuller
///#####################################################

/// inputSource can be either a Plot or another populator.
template <class HistoType>
OverFlowPuller<HistoType>::OverFlowPuller(const Plot<HistoType>& inputPlot) : m_input{inputPlot.getPopulator()} {}

template <class HistoType>
OverFlowPuller<HistoType>::OverFlowPuller(const IPopulatorSource<HistoType>& inputSource) : m_input{inputSource.clonePopulator()} {}

template <class HistoType> std::shared_ptr<HistoType> OverFlowPuller<HistoType>::populate() {
    std::shared_ptr<HistoType> histo = m_input->populate();
    histo = std::dynamic_pointer_cast<HistoType>(clone(histo));
    PlotUtils::shiftOverflows(histo.get());
    return histo;
}

template <class HistoType> std::shared_ptr<IPlotPopulator<HistoType>> OverFlowPuller<HistoType>::clonePopulator() const {
    return std::make_shared<OverFlowPuller<HistoType>>(*this);
}

///######################################################
///             RmsCalculator
///######################################################
/// Takes a 2D or 3D histogram and projects the RMS into N-1 dimensions. The axis
/// used for the calculation of the RMS is the z-axis and y-axis for the 3D and 2D histograms
template <class HistoType> RmsCalculator<HistoType>::RmsCalculator(const Plot<HistoType>& plot) : m_input{plot.getPopulator()} {}
template <class HistoType>
RmsCalculator<HistoType>::RmsCalculator(const IPopulatorSource<HistoType>& populator) : m_input{populator.clonePopulator()} {}
template <class HistoType> std::shared_ptr<HistoType> RmsCalculator<HistoType>::populate() {
    /// Retrieve first the histogram
    std::shared_ptr<HistoType> histo = m_input->populate();
    /// Do nothing if the user did not undersand the concept of the RMS
    if (histo->GetDimension() == 1) { return histo; }
    std::shared_ptr<HistoType> output_histo;
    /// 2D histogram
    if (histo->GetDimension() == 2) {
        output_histo = MakeTH1(ExtractBinning(histo->GetXaxis()));
        for (int bin_x = 0; bin_x <= histo->GetNbinsX(); ++bin_x) {
            double rms{0.}, num_bins{0.};
            for (int bin_y = 0; bin_y <= histo->GetNbinsY(); ++bin_y) {
                double content = histo->GetBinContent(bin_x, bin_y);
                if (std::abs(content) <= DBL_EPSILON) continue;
                num_bins += content;
                rms += std::pow(content * histo->GetYaxis()->GetBinCenter(bin_y), 2);
            }
            if (!num_bins) continue;
            rms = std::sqrt(rms / num_bins);
            output_histo->SetBinContent(bin_x, rms);
        }
    } else {
        output_histo = MakeTH1(ExtractBinning(histo->GetXaxis()), ExtractBinning(histo->GetYaxis()));
        for (int bin_x = 0; bin_x <= histo->GetNbinsX(); ++bin_x) {
            for (int bin_y = 0; bin_y <= histo->GetNbinsY(); ++bin_y) {
                double rms{0.}, num_bins{0.};
                for (int bin_z = 0; bin_z <= histo->GetNbinsZ(); ++bin_z) {
                    double content = histo->GetBinContent(bin_x, bin_y, bin_z);
                    if (std::abs(content) <= DBL_EPSILON) continue;
                    num_bins += content;
                    rms += std::pow(content * histo->GetZaxis()->GetBinCenter(bin_z), 2);
                }
                if (!num_bins) continue;
                rms = std::sqrt(rms / num_bins);
                output_histo->SetBinContent(bin_x, bin_y, rms);
            }
        }
    }
    copy_styling(histo, output_histo);
    return output_histo;
}
template <class HistoType> std::shared_ptr<IPlotPopulator<HistoType>> RmsCalculator<HistoType>::clonePopulator() const {
    return std::make_shared<RmsCalculator>(*this);
}

///######################################################
///             MeanCalculator
///######################################################

template <class HistoType> MeanCalculator<HistoType>::MeanCalculator(const Plot<HistoType>& plot) : m_input{plot.getPopulator()} {}
template <class HistoType>
MeanCalculator<HistoType>::MeanCalculator(const IPlotPopulator<HistoType>& plot) : m_input{plot.clonePopulator} {}
template <class HistoType> std::shared_ptr<HistoType> MeanCalculator<HistoType>::populate() {
    std::shared_ptr<HistoType> histo = m_input->populate();
    if (histo->GetDimension() == 1) return histo;
    std::shared_ptr<HistoType> output_histo;
    ///
    if (histo->GetDimension() == 2) {
        output_histo = MakeTH1(ExtractBinning(histo->GetXaxis()));
        for (int bin_x = 0; bin_x <= histo->GetNbinsX(); ++bin_x) {
            double rms{0.}, num_bins{0.};
            for (int bin_y = 0; bin_y <= histo->GetNbinsY(); ++bin_y) {
                double content = histo->GetBinContent(bin_x, bin_y);
                if (std::abs(content) <= DBL_EPSILON) continue;
                num_bins += content;
                rms += content * histo->GetYaxis()->GetBinCenter(bin_y);
            }
            if (!num_bins) continue;
            rms = rms / num_bins;
            output_histo->SetBinContent(bin_x, rms);
        }
    } else {
        output_histo = MakeTH1(ExtractBinning(histo->GetXaxis()), ExtractBinning(histo->GetYaxis()));
        for (int bin_x = 0; bin_x <= histo->GetNbinsX(); ++bin_x) {
            for (int bin_y = 0; bin_y <= histo->GetNbinsY(); ++bin_y) {
                double rms{0.}, num_bins{0.};
                for (int bin_z = 0; bin_z <= histo->GetNbinsZ(); ++bin_z) {
                    double content = histo->GetBinContent(bin_x, bin_y, bin_z);
                    if (std::abs(content) <= DBL_EPSILON) continue;
                    num_bins += content;
                    rms += content * histo->GetZaxis()->GetBinCenter(bin_z);
                }
                if (!num_bins) continue;
                rms = rms / num_bins;
                output_histo->SetBinContent(bin_x, bin_y, rms);
            }
        }
    }
    copy_styling(histo, output_histo);
    return output_histo;
}
template <class HistoType> std::shared_ptr<IPlotPopulator<HistoType>> MeanCalculator<HistoType>::clonePopulator() const {
    return std::make_shared<MeanCalculator>(*this);
}

#endif