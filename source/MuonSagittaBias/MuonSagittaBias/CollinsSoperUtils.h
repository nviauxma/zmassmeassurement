#ifndef NTAU__COLLINSSOPERUTILS_H
#define NTAU__COLLINSSOPERUTILS_H

#include <Math/GenVector/Boost.h>
#include <Math/GenVector/LorentzRotation.h>
#include <Math/GenVector/LorentzVector.h>
#include <Math/GenVector/PtEtaPhiM4D.h>
#include <Math/GenVector/PxPyPzE4D.h>
#include <Math/GenVector/Rotation3D.h>
///#############################################################################
///     General set of helper functions needed for the sagitta bias analysis
///##############################################################################
///
namespace CollinsSoper {
    /// Four vector in polar representation using Pt, Eta, Phi, M
    using PolarVector4D = ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double>>;
    /// Four vector in polar representation using Px, Py, Pz, E
    using Vector4D = ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double>>;
    using Vector3D = ROOT::Math::DisplacementVector3D<ROOT::Math::Cartesian3D<double>>;

    using RestFrameBoost = ROOT::Math::Boost;
    using SystemRotation = ROOT::Math::Rotation3D;

    /// Returns a three vector from a given input with unit length
    template <class four_vec> Vector3D unit_vector(const four_vec& in_vec);

    /// Returns the cross product of two input vectors
    ///  c.f. https://de.wikipedia.org/wiki/Kreuzprodukt
    template <class four_vec1, class four_vec2> Vector3D cross(const four_vec1& V1, const four_vec2& V2);

    /// Returns the vector to boost the system into the rest frame
    template <class four_vec> Vector3D boost_vector(const four_vec& in_vec);

    /// Helper class to construct the Collins-Soper frame of
    /// the two escaping leptons following https://arxiv.org/pdf/1605.05450.pdf
    /// In summary the CS frame is defined as
    ///    -- Rest frame the particle decaying into the lepton pair
    ///    --- z-axis intersects the angle of the p_{A} and -p_{B}
    ///    --- y-axis is perpendicular to the beam-particle plane
    ///    --- x-axis is chosen such that x,y,z span a right-handed coordinate system
    class RestFrame {
    public:
        //  constructor using the speed up FourVectors from the ROOT math
        //  library
        RestFrame(const PolarVector4D& lep_plus, const PolarVector4D& lep_minus);
        RestFrame(const Vector4D& lep_plus, const Vector4D& lep_minus);

        const Vector4D& invariantP4() const;
        /// Transforms a given TLorentzVector into the CollinsSoper restframe

        template <class four_vec> four_vec toRestFrame(const four_vec& V) const;

    private:
        void make_coordinate_transform(const Vector4D& lep_plus, const Vector4D& lep_minus);

        /// Boosts a given P4 to the Collins soper rest frame without the rotations

        /// Cached invariant momentum
        Vector4D m_P4_ll{};
        /// Boost vector into the restframe of the decaying particle

        RestFrameBoost m_boost_rest{};
        /// Rotate the frame such that it satisfies the CS definitions
        SystemRotation m_rest_rotation{};
        SystemRotation m_lab_rotation{};
    };

    /// Returns a three vector from a given input with unit length
    template <class four_vec> Vector3D unit_vector(const four_vec& in_vec) {
        double R = in_vec.R();
        return Vector3D(in_vec.X() / R, in_vec.Y() / R, in_vec.Z() / R);
    }
    /// Returns the cross product of two input vectors
    ///  c.f. https://de.wikipedia.org/wiki/Kreuzprodukt
    template <class four_vec1, class four_vec2> Vector3D cross(const four_vec1& V1, const four_vec2& V2) {
        return Vector3D{V1.Y() * V2.Z() - V1.Z() * V2.Y(), V1.Z() * V2.X() - V1.X() * V2.Z(), V1.X() * V2.Y() - V1.Y() * V2.X()};
    }

    /// Returns the vector to boost the system into the rest frame
    template <class four_vec> Vector3D boost_vector(const four_vec& in_vec) {
        const double E = in_vec.E();
        return Vector3D{-in_vec.Px() / E, -in_vec.Py() / E, -in_vec.Pz() / E};
    }
    template <class four_vec> four_vec RestFrame::toRestFrame(const four_vec& V) const {
        four_vec copy = m_rest_rotation * (m_boost_rest * V);
        return copy;
    }

}  // namespace CollinsSoper

#endif
