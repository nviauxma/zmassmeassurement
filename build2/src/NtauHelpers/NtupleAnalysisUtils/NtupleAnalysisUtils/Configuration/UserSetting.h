#ifndef NTAU__USERSETTING__H
#define NTAU__USERSETTING__H


#include <string> 
#include <map> 
#include "TColor.h" 
#include "TStyle.h" 

/// Class to describe a user-defined setting. 
/// Used throughout NTAU for storing configuration and metadata

/// a struct that we can pass as argument to unset a given option
struct UnsetUserSetting{}; 

/// Helper class to package a given pre-defined setting 
template <typename T, class Parent> class UserSetting{
    public:
        /// constructors are only needed internally by the parent classes

        /// This constructor allows to specify a default value (rather than having an empty flag). 
        UserSetting(const T& initialVal, Parent* parent);
        /// copy-esque ctor
        UserSetting(const UserSetting<T,Parent> & other, Parent* newParent); 
        /// copy helper 
        void copyFrom(const UserSetting<T,Parent> & other, Parent* newParent); 

        /// This is a setter that returns a ref to the parent. Can be used to daisy-chain instructions on the same format. 
        /// For example, MyPlotFormat.Setting1(value1).Setting2(Value2).Setting3(Value3).<...>; 
        Parent & operator()(const T& t);

        const T& operator()() const; 
        /// allow to modify in-place (useful for axis configs)
        T & modify();
 
        /// Check if a given setting has been user-configured (or pre-set)
        bool isUserSet() const;
        
    private: 
        bool m_isUserSet{false};        
        T m_val; 
        Parent* m_parent{nullptr}; 
};


/// Also support custom, string-keyed user settings. 
/// Unlike the pre-defined settings, they will not 
/// be used by the framework classes, but can be 
/// used to pass around information conveniently in  
/// a manner similar to xAOD decorations 

template <typename T, class Parent> class CustomUserSettings {

    public:
        /// ======================================================================
        /// Option to add non-standard, user-defined information. This will not be  
        /// used by NTAU itself, but can act in a manner like xAOD decorations 
        /// to pass around additional information if needed
        /// ======================================================================
        CustomUserSettings(Parent* parent); 
        /// copy-esque ctor
        CustomUserSettings(const CustomUserSettings<T,Parent> & other, Parent* newParent); 
        /// copy helper 
        void copyFrom(const CustomUserSettings<T,Parent> & other, Parent* newParent); 
        /// Setter
        Parent & operator()(const std::string & key, const T & value);
        /// Un-setter - delete a given entry
        Parent & operator()(const std::string & key, UnsetUserSetting);

        /// Getter. Will return true if a user-defined entry matching the key exists. 
        /// In this case, will set the second arg to the corresponding stored value. 
        /// Otherwise, return false and do not change the second arg. 
        bool get(const std::string & key, T & value) const; 

    protected: 
        /// holders for custom flags 
        std::map<std::string, T> m_userVars;
        Parent* m_parent{nullptr}; 
};


#include "UserSetting.ixx"

#endif