#include <TCanvas.h>
#include <TF1.h>
#include <TFile.h>
#include <TGraphErrors.h>
#include <TH1F.h>
#include <TString.h>

// minuit
#include <iomanip>
#include <iostream>

#include "Math/Factory.h"
#include "Math/Functor.h"
#include "Math/Minimizer.h"
#include "TError.h"
#include "TRandom2.h"

//#include "AtlasStyle.h"

using namespace std;

const int etab = 25;  // eta bins
const int mb = 35;    // mass bins
const int order = 4;  // order of the interpolation

// mass bins range
int mbmin = 3;   // 8; //0; //20;
int mbmax = 10;  // 38;//52; //40;

// eta bins
double etabins[etab + 1] = {-2.5, -2.3, -2.1, -1.9, -1.7, -1.5, -1.3, -1.1, -0.9, -0.7, -0.5, -0.3, -0.1,
                            0.1,  0.3,  0.5,  0.7,  0.9,  1.1,  1.3,  1.5,  1.7,  1.9,  2.1,  2.3,  2.5};

double biasmap[etab] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};

const int nbs = 10;
double biases[nbs + 1] = {-0.5, -0.4, -0.3, -0.2, -0.1, 0.0, 0.1, 0.2, 0.3, 0.4, 0.5};

// data
double dat[etab][mb];
double err[etab][mb];

class poly {
private:
    vector<double> pars;

public:
    poly(){};
    poly(vector<double> ps) : pars(ps){};

    double eval(double c) {
        double f = 0;
        double pow = 1.;
        for (int p = 0; p < pars.size(); p++) {
            f += pars[p] * pow;
            pow *= c;
        }

        return f;
    }

    // double eval(double c1, double c2)
    //{
    //  double c = sqrt(c1*c1+c2*c2)/sqrt(2.);
    //  return eval(c);
    //}
};

// interpolatory polynomials
poly deltapolys[etab][mb];

// chi-square function to be minimised
double chi2(const double *di) {
    double c2 = 0.;

    for (int ieta = 0; ieta < etab; ieta++) {
        if (ieta == 12) continue;

        for (int im = mbmin; im <= mbmax; im++) {
            if (deltapolys[ieta][im].eval(di[ieta]) != deltapolys[ieta][im].eval(di[ieta])) {
                cout << "nan in deltapolys " << endl;
                continue;
            }
            if (dat[ieta][im] != dat[ieta][im]) {
                cout << "nan in data " << endl;
                continue;
            }
            if (err[ieta][im] != err[ieta][im]) {
                cout << "nan in err " << endl;
                continue;
            }

            if (err[ieta][im] != 0) c2 += pow(deltapolys[ieta][im].eval(di[ieta]) - dat[ieta][im], 2) / pow(err[ieta][im], 2);
        }
    }
    return c2;
}

// Build a TGraph storing bin content as a function of the injected bias, to be interpolated
TGraphErrors *maketgraph(TFile *f, double etal, double etau, int mbin) {
    char stretal[10], stretau[10];
    sprintf(stretal, "%.2f", etal);
    sprintf(stretau, "%.2f", etau);

    TString gname = Form("m%d_eta[%s_%s]", mbin, stretal, stretau);

    TGraphErrors *g = new TGraphErrors();
    g->SetName(gname);

    for (int ibias = 0; ibias < nbs; ibias++) {
        char strbias[10];
        sprintf(strbias, "%.6f", biases[ibias]);

        TString nameTemplneg = Form(
            "ID/PseudoMassMinus/All__MinusLep_Eta/KinSel_etaMuMinus_inRANGE_%s_%s/MLL_3_4/"
            "Template_SAGBIAS_0.000000_RADBIAS_0.000000_MAGBIAS_%s_SIGMA_0.000000",
            stretal, stretau, strbias);
        TString nameTemplpos = Form(
            "ID/PseudoMassPlus/All__PlusLep_Eta/KinSel_etaMuPlus_inRANGE_%s_%s/MLL_3_4/"
            "Template_SAGBIAS_0.000000_RADBIAS_0.000000_MAGBIAS_%s_SIGMA_0.000000",
            stretal, stretau, strbias);

        TH1D *hn = (TH1D *)f->Get(nameTemplneg);
        TH1D *hp = (TH1D *)f->Get(nameTemplpos);
        double valy = (hp->GetBinContent(mbin) - hn->GetBinContent(mbin)) / (hp->GetBinContent(mbin) + hn->GetBinContent(mbin));
        double valx = hn->GetBinCenter(mbin);
        double err = hn->GetBinError(mbin);

        // cout << " val " << biases[ibias] << "  " << valy << " err " << err << endl;

        g->SetPoint(g->GetN(), biases[ibias], valy);
        // g->SetPointError(g->GetN(), 0, err);
    }
    return g;
}

int main(int argc, char *argv[]) {
    if (argc < 2) {
        printf("Standard usage   : %s <file_data> <file_mc>\n", argv[0]);
        printf("Alternative usage: %s <file_mc> <file_data>\n", argv[0]);
        return 0;
    }

    string fname_data = argv[1];
    TFile *fd = new TFile(fname_data.c_str());
    if (fd->IsZombie()) {
        printf("Error opening: %s\n", fname_data.c_str());
        return 0;
    }

    string fname_mc = argv[2];
    TFile *fm = new TFile(fname_mc.c_str());
    if (fm->IsZombie()) {
        printf("Error opening: %s\n", fname_mc.c_str());
        return 0;
    }

    char func[10];
    sprintf(func, "pol%i", order);

    TFile *fout = new TFile("fitoutput.root", "RECREATE");

    // Perform the interpolation for each bin of eta as a function of delta_m
    for (int ieta = 0; ieta < etab; ieta++) {
        // Skip eta region -0.1 < eta < 0.1
        if (ieta == 12) continue;

        // loop on mass bins
        for (int im = mbmin; im <= mbmax; im++) {
            TGraphErrors *g = maketgraph(fd, etabins[ieta], etabins[ieta + 1], im + 1);
            if (!g) continue;

            // fit
            TF1 *parfit = new TF1("ParFit", func);
            g->Fit(parfit, "WQ", "", g->GetX()[1], g->GetX()[g->GetN() - 1]);

            fout->cd();
            g->Write();
            // fout->Close();

            // store the fit results
            vector<double> pars;
            for (int p = 0; p <= order; p++)
                // pars.push_back(parfit->GetParameter(order-p));
                pars.push_back(parfit->GetParameter(p));

            deltapolys[ieta][im] = poly(pars);
            // cout << "eta bin: " << ieta << " mass bin " << im << endl;
            // printf("%f + %f x + %f x^2 + %f x^3 \n", pars[0], pars[1], pars[2], pars[3]);
        }
    }

    // Read the MC central template and fill the data matrix
    TFile *fmc = new TFile(fname_mc.c_str());

    for (int ieta = 0; ieta < etab; ieta++) {
        // Skip eta region -0.1 < eta < 0.1
        if (ieta == 12) continue;

        char stretal[10], stretau[10];
        sprintf(stretal, "%.2f", etabins[ieta]);
        sprintf(stretau, "%.2f", etabins[ieta + 1]);

        char strbias[10];
        sprintf(strbias, "%.6f", biasmap[ieta]);

        TString namenominalneg = Form(
            "ID/PseudoMassMinus/All__MinusLep_Eta/KinSel_etaMuMinus_inRANGE_%s_%s/MLL_3_4/"
            "Template_SAGBIAS_0.000000_RADBIAS_0.000000_MAGBIAS_%s_SIGMA_0.000000",
            stretal, stretau, strbias);
        TString namenominalpos = Form(
            "ID/PseudoMassPlus/All__PlusLep_Eta/KinSel_etaMuPlus_inRANGE_%s_%s/MLL_3_4/"
            "Template_SAGBIAS_0.000000_RADBIAS_0.000000_MAGBIAS_%s_SIGMA_0.000000",
            stretal, stretau, strbias);

        // cout << namenominalneg << endl;
        // cout << namenominalpos << endl;

        TH1D *hp = (TH1D *)fmc->Get(namenominalpos);
        TH1D *hn = (TH1D *)fmc->Get(namenominalneg);
        if (!hp) continue;
        if (!hn) continue;

        TString hname = Form("eta%d", ieta);

        TH1D *hsum = (TH1D *)hp->Clone();
        hsum->Add(hn, 1);
        hsum->GetXaxis()->SetRange(mbmin, mbmax);
        hsum->Scale(1. / hsum->Integral());

        hsum->SetLineColor(kBlack);
        hsum->SetMarkerColor(kBlack);

        fout->cd();
        hsum->SetName(hname);
        hsum->Write();

        // loop on mass bins
        for (int im = mbmin; im <= mbmax; im++) {
            dat[ieta][im] = hsum->GetBinContent(im + 1);
            err[ieta][im] = hsum->GetBinError(im + 1);
            // err[ieta][im]  = sqrt(hp->GetBinContent(im+1)+hn->GetBinContent(im+1));
            // cout << " eta " << ieta << " m " << im << " : " << dat[ieta][im] << "+/-" << err[ieta][im]
            //<< "  " << deltapolys[ieta][im].eval(0.)
            //<< endl;
        }
    }

    // Take the error from data
    for (int ieta = 0; ieta < etab; ieta++) {
        // Skip eta region -0.1 < eta < 0.1
        if (ieta == 12) continue;

        char stretal[10], stretau[10];
        sprintf(stretal, "%.2f", etabins[ieta]);
        sprintf(stretau, "%.2f", etabins[ieta + 1]);

        char strbias[10];
        sprintf(strbias, "%.6f", biasmap[ieta]);

        TString namenominalneg = Form(
            "ID/PseudoMassMinus/All__MinusLep_Eta/KinSel_etaMuMinus_inRANGE_%s_%s/MLL_3_4/"
            "Template_SAGBIAS_0.000000_RADBIAS_0.000000_MAGBIAS_%s_SIGMA_0.000000",
            stretal, stretau, strbias);
        TString namenominalpos = Form(
            "ID/PseudoMassPlus/All__PlusLep_Eta/KinSel_etaMuPlus_inRANGE_%s_%s/MLL_3_4/"
            "Template_SAGBIAS_0.000000_RADBIAS_0.000000_MAGBIAS_%s_SIGMA_0.000000",
            stretal, stretau, strbias);

        TH1D *hp = (TH1D *)fd->Get(namenominalpos);
        TH1D *hn = (TH1D *)fd->Get(namenominalneg);

        TH1D *hsum = (TH1D *)hp->Clone();
        hsum->Add(hn, 1);
        hsum->GetXaxis()->SetRange(mbmin, mbmax);
        hsum->Scale(1. / hsum->Integral());

        // loop on mass bins
        for (int im = mbmin; im <= mbmax; im++)
            // square sum MC and data stat uncertainty
            err[ieta][im] = sqrt(pow(err[ieta][im], 2) + pow(hsum->GetBinError(im + 1), 2));
    }

    // Save plots, and m+ m- plots
    int ieta = 0;
    char stretal[10], stretau[10];
    char strbias[10];
    TString namenominalneg, namenominalpos;
    TH1D *hp, *hn;

    sprintf(stretal, "%.2f", etabins[ieta]);
    sprintf(stretau, "%.2f", etabins[ieta + 1]);

    // No bias plot
    sprintf(strbias, "%.6f", biasmap[ieta]);
    namenominalneg = Form(
        "ID/PseudoMassMinus/All__MinusLep_Eta/KinSel_etaMuMinus_inRANGE_%s_%s/MLL_3_4/"
        "Template_SAGBIAS_0.000000_RADBIAS_0.000000_MAGBIAS_%s_SIGMA_0.000000",
        stretal, stretau, strbias);
    namenominalpos = Form(
        "ID/PseudoMassPlus/All__PlusLep_Eta/KinSel_etaMuPlus_inRANGE_%s_%s/MLL_3_4/"
        "Template_SAGBIAS_0.000000_RADBIAS_0.000000_MAGBIAS_%s_SIGMA_0.000000",
        stretal, stretau, strbias);
    TH1D *hptot = (TH1D *)fm->Get(namenominalpos)->Clone();
    TH1D *hntot = (TH1D *)fm->Get(namenominalneg)->Clone();
    hptot->SetName("hptot");
    hntot->SetName("hntot");

    // Maximum bias plot
    sprintf(strbias, "%.6f", biases[0]);
    namenominalneg = Form(
        "ID/PseudoMassMinus/All__MinusLep_Eta/KinSel_etaMuMinus_inRANGE_%s_%s/MLL_3_4/"
        "Template_SAGBIAS_0.000000_RADBIAS_0.000000_MAGBIAS_%s_SIGMA_0.000000",
        stretal, stretau, strbias);
    namenominalpos = Form(
        "ID/PseudoMassPlus/All__PlusLep_Eta/KinSel_etaMuPlus_inRANGE_%s_%s/MLL_3_4/"
        "Template_SAGBIAS_0.000000_RADBIAS_0.000000_MAGBIAS_%s_SIGMA_0.000000",
        stretal, stretau, strbias);
    TH1D *hptotmxb = (TH1D *)fm->Get(namenominalpos)->Clone();
    TH1D *hntotmxb = (TH1D *)fm->Get(namenominalneg)->Clone();
    hptotmxb->SetName("hptotmxb");
    hntotmxb->SetName("hntotmxb");

    for (int ieta = 1; ieta < etab; ieta++) {
        // Skip eta region -0.1 < eta < 0.1
        if (ieta == 12) continue;

        sprintf(stretal, "%.2f", etabins[ieta]);
        sprintf(stretau, "%.2f", etabins[ieta + 1]);

        // No bias plot
        sprintf(strbias, "%.6f", biasmap[ieta]);
        namenominalneg = Form(
            "ID/PseudoMassMinus/All__MinusLep_Eta/KinSel_etaMuMinus_inRANGE_%s_%s/MLL_3_4/"
            "Template_SAGBIAS_0.000000_RADBIAS_0.000000_MAGBIAS_%s_SIGMA_0.000000",
            stretal, stretau, strbias);
        namenominalpos = Form(
            "ID/PseudoMassPlus/All__PlusLep_Eta/KinSel_etaMuPlus_inRANGE_%s_%s/MLL_3_4/"
            "Template_SAGBIAS_0.000000_RADBIAS_0.000000_MAGBIAS_%s_SIGMA_0.000000",
            stretal, stretau, strbias);
        hp = (TH1D *)fm->Get(namenominalpos);
        hn = (TH1D *)fm->Get(namenominalneg);
        hptot->Add(hp);
        hntot->Add(hn);

        // Maximum bias plot
        sprintf(strbias, "%.6f", biases[0]);
        namenominalneg = Form(
            "ID/PseudoMassMinus/All__MinusLep_Eta/KinSel_etaMuMinus_inRANGE_%s_%s/MLL_3_4/"
            "Template_SAGBIAS_0.000000_RADBIAS_0.000000_MAGBIAS_%s_SIGMA_0.000000",
            stretal, stretau, strbias);
        namenominalpos = Form(
            "ID/PseudoMassPlus/All__PlusLep_Eta/KinSel_etaMuPlus_inRANGE_%s_%s/MLL_3_4/"
            "Template_SAGBIAS_0.000000_RADBIAS_0.000000_MAGBIAS_%s_SIGMA_0.000000",
            stretal, stretau, strbias);
        hp = (TH1D *)fm->Get(namenominalpos);
        hn = (TH1D *)fm->Get(namenominalneg);
        hptotmxb->Add(hp);
        hntotmxb->Add(hn);
    }

    TH1D *hsum = (TH1D *)hptot->Clone();
    hsum->Add(hntot, 1);
    hsum->GetXaxis()->SetRange(mbmin, mbmax);
    hsum->Scale(1. / hsum->Integral());
    fout->cd();
    hsum->SetLineColor(kBlack);
    hsum->SetMarkerColor(kBlack);
    hsum->SetName("psmnob");

    TH1D *hsummxb = (TH1D *)hptotmxb->Clone();
    hsummxb->Add(hntotmxb, 1);
    hsummxb->GetXaxis()->SetRange(mbmin, mbmax);
    hsummxb->Scale(1. / hsummxb->Integral());
    fout->cd();
    hsummxb->SetLineColor(kBlack);
    hsummxb->SetMarkerColor(kBlack);
    hsummxb->SetName("psmmxb");

    hsum->Write();
    hsummxb->Write();

    // minuit minimisation
    // create minimizer giving a name and a name (optionally) for the specific
    // algorithm
    // possible choices are:
    //     minName                  algoName
    // Minuit /Minuit2             Migrad, Simplex,Combined,Scan  (default is Migrad)
    //  Minuit2                     Fumili2
    //  Fumili
    //  GSLMultiMin                ConjugateFR, ConjugatePR, BFGS,
    //                              BFGS2, SteepestDescent
    //  GSLMultiFit
    //   GSLSimAn

    const char *minName = "Minuit2";
    const char *algoName = "";
    // int randomSeed = -1;

    // Genetic
    ROOT::Math::Minimizer *min = ROOT::Math::Factory::CreateMinimizer(minName, algoName);

    // set tolerance, etc...
    min->SetMaxFunctionCalls(1000000);  // for Minuit/Minuit2
    min->SetMaxIterations(10000);       // for GSL
    // min->SetTolerance(0.001);
    min->SetPrintLevel(1);

    // create function wrapper for minmizer
    // a IMultiGenFunction type
    ROOT::Math::Functor c2f(&chi2, etab);

    // step for minimisation
    double step[etab];
    for (int ieta = 0; ieta < etab; ieta++) step[ieta] = 0.0001;

    step[12] = 0.00;

    // starting point
    TRandom2 r(123456);
    double variable[etab];
    for (int ieta = 0; ieta < etab; ieta++) variable[ieta] = r.Uniform(-0.2, 0.2);

    variable[12] = 0.00;

    min->SetFunction(c2f);

    // Set the free variables to be minimized!
    for (int ieta = 0; ieta < etab; ieta++) {
        char varname[20];
        sprintf(varname, "d%i", ieta);
        min->SetVariable(ieta, varname, variable[ieta], step[ieta]);
    }

    // do the minimization
    min->Minimize();
    min->Hesse();

    // get Minos errors
    cout << "Errors from MINOS" << endl;
    const double *xs = min->X();
    min->SetPrintLevel(0);
    double ep[etab];
    double em[etab];
    for (int ieta = 0; ieta < etab; ieta++) {
        double errm, errp;
        min->GetMinosError(ieta, errm, errp);
        ep[ieta] = errp;
        em[ieta] = errm;
        printf("d%i = %f +%f %f \n", ieta, xs[ieta], errp, errm);
    }

    cout << "dof: " << ((mbmax - mbmin) + 1) * (etab - 1) - (etab - 1) << endl;

    /*
    cout << "Correlation Matrix" << endl;
    cout << setw(5) << " ";
    for (int ieta1 = 0; ieta1 < etab; ieta1++)
      cout << setw(14) << "c" << ieta1;
    cout << endl;
    for (int ieta1 = 0; ieta1 < etab; ieta1++)
      {
        cout << setw(5) << "c" << ieta1;
        for (int ieta2 = 0; ieta2 < etab; ieta2++)
         cout << setw(15) << min->CovMatrix(ieta1, ieta2)/min->Errors()[ieta1]/min->Errors()[ieta2];
        cout << endl;
      }
    */

    // print results
    // SetAtlasStyle();
    TCanvas *cnv = new TCanvas("delta", "", 800, 600);
    // double etabins[25]= {-2.47,-2.3, -2, -1.82, -1.55, -1.37, -1.2, -1, -0.8, -0.6 ,-0.4, -0.2, 0, 0.2, 0.4 , 0.6 , 0.8 , 1 , 1.2 , 1.37
    // , 1.55 , 1.82 , 2 , 2.3 , 2.47};
    TH1F *deta = new TH1F("dh", "", etab, etabins);
    for (int ieta = 0; ieta < etab; ieta++) {
        deta->SetBinContent(ieta + 1, xs[ieta]);
        deta->SetBinError(ieta + 1, (ep[ieta] - em[ieta]) / 2.);
    }

    deta->SetXTitle("#eta");
    deta->SetYTitle("#delta_{m}");

    deta->SetStats(0);
    deta->SetLineColor(kBlue + 1);
    deta->SetMarkerStyle(20);
    deta->SetMarkerColor(kBlue + 1);
    deta->SetMarkerSize(0);

    deta->SetMinimum(-0.3);
    deta->SetMaximum(0.3);

    deta->Draw("HIST E1");
    cnv->Print("delta_m.pdf");

    fout->cd();
    deta->Write();

    fout->Close();
    return 0;
}
