#ifndef NTAU__HISTOMANIPULATION__H
#define NTAU__HISTOMANIPULATION__H

#include <memory> 

#include "TH1.h"
#include "TEfficiency.h"
#include "TGraph.h"
#include "TProfile.h"
#include "NtupleAnalysisUtils/Plot/Plot.h"

/// This header declares a set of helper methods which manipulate or convert histograms. 
/// Used extensively in the remaining tools of the package. 



namespace PlotUtils{

  /// This helper enum provides a few possible error definitions 
  /// for ratios. 
   typedef enum {
        defaultErrors = 0,          /// simple error propagation
        efficiencyErrors = 1,       /// binomial efficiency errors, assume num and den correlated
        numeratorOnlyErrors=2,      /// propagate only error on numerator
        denominatorOnlyErrors=3     /// propagate only error on denominator 
    } EfficiencyMode;


    /// normalises histogram bin contents to target_width. 
    /// If negative, normalise to first bin width
    /// Method returns the bin width that is used for normalisation 
    /// Histogram is modified in-place.
    float normaliseToBinWidth(TH1* h, float target_width = -1);

    /// normalise the passed input histogram to a target integral. 
    /// @arg target_integral: Integral after normalisation
    /// @arg multiplyByWidht: Multiply each bin by its width during integration 
    /// @arg includeOverflow: include or exclude under- and overflow bins 
    /// Histogram is modified in-place.
    void normaliseToIntegral(TH1* h, double target_integral = 1, 
                            bool multiplyByWidth=false, 
                            bool includeOverflow=false);

    /// Shifts the under/overflows into the first / last bins. 
    /// Histogram is modified in-place
    void shiftOverflows(TH1* h);

    /// Returns whether the global bin in a histogram is in the overflow or underflow
    bool isOverflowBin(const TH1 *Histo, int bin);
    /// Returns whether the bin number is zero or greater than the number of bins along this axis;
    bool isOverflowBin(const TAxis *a, int bin);

    /// Returns the total number of bins of a histogram
    ///  i.e. per dimension GetNbins() + 2
    unsigned int getNbins(const TH1 *Histo);
    
    
    /// Returns whether alpha numeric bin-labels have been set
    bool isAlphaNumeric(const TH1 *histo);
    bool isAlphaNumeric(const TAxis* axis);
    
    /// Integrate a 1D histogram into a 1D running integral. 
    /// Each bin content is equal to the integral from the first to
    /// the current bin in the original histogram. 
    /// Returns a new histogram, leaving the original invariant. 
    TH1* toCDF (const TH1* differential); 

    /// =====================================================
    /// Methods for ratio calculation 
    /// =====================================================

    /// These methods allow to obtain a Ratio of two histograms. 
    /// The errmode argument determines how the errors are calculated.
    /// The convention is to always return a TH1* pointer

    /// First, a fallback for unsupported types. Specialisation for supported types below. 
    /// This fallback will try to perform a TH1 ratio if both args are convertible, 
    /// otherwise it will return a null pointer. 
    template <class T, class S>  TH1* getRatio(const T* num, const S* den, EfficiencyMode errors = defaultErrors);

    /// Specialisations for ratios between efficiencies and profiles 
    TH1* getRatio(const TEfficiency* num, const TEfficiency* den, EfficiencyMode errmode = defaultErrors);
    TH1* getRatio(const TProfile* num, const TProfile* den, EfficiencyMode errmode = defaultErrors);

    /// Specialisation for ratios between TH1-alikes
    TH1* getRatio(const TH1* num, const TH1* den, EfficiencyMode errmode = defaultErrors);

    /// Specialisation for ratios between graph-alikes
    TGraph* getRatio(const TGraph* num, const TGraph* den, EfficiencyMode errmode = defaultErrors);

    /// Template for working with Plot objects
    /// Note that it is likely cleaner to instead use a populator to obtain the ratio! 
    template <typename H> Plot<TH1> getRatio(Plot<H> & num, Plot<H> & den, EfficiencyMode errmode = defaultErrors);

    /// Support for ratios between Plot objects - for graphs. 
    Plot<TGraph> getRatio(Plot<TGraph> & num, Plot<TGraph> & den, EfficiencyMode errmode = defaultErrors);

    /// This will generate a TH1D filled with the efficiency returned by the 
    /// TEfficiency argument. Histogram errors correspond to the average
    /// of the up- and down-errors reported by the TEfficiency. 
    std::shared_ptr<TH1D> extractEfficiency(const TEfficiency* eff); 

}

#include "HistoManipulation.ixx"

#endif // NTAU__HISTOMANIPULATION__H