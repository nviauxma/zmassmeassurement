#include <MuonSagittaBias/PlotProvider.h>
#include <MuonSagittaBias/Utils.h>
namespace {
    static const std::vector<int> colour_map{
        kBlack, kCyan - 2, kOrange, kGreen + 1, kRed, kBlue,
    };
    static const std::vector<int> marker_map{
        kFullTriangleUp, kFullTriangleDown, kFullDoubleDiamond, kFullDiamond, kFullFourTrianglesPlus,
    };
    int get_value(const std::vector<int>& map, int key) {
        static std::vector<int> key_holders;

        std::vector<int>::const_iterator itr = std::find(key_holders.begin(), key_holders.end(), key);
        if (itr == key_holders.end()) {
            key_holders.emplace_back(key);
            return get_value(map, key);
        }
        size_t pos = itr - key_holders.begin();
        if (pos < map.size()) return map[pos];
        return 0;
    }
}  // namespace

///###########################################################################
///                         PlotFiller
///###########################################################################
PlotFiller::PlotFiller(const std::function<int(const TH1*, SagittaBiasAnalyzer&, int)>& fill_func, PlotUtils::HistoBinConfig histo) :
    m_filler(fill_func), m_template(histo.make_histo()) {}

PlotFiller::PlotFiller(const std::function<int(const TH1*, SagittaBiasAnalyzer&, int)>& fill_func, const std::shared_ptr<TH1>& histo) :
    m_filler(fill_func), m_template(histo) {}

std::function<int(const TH1*, SagittaBiasAnalyzer&, int)> PlotFiller::get_filler() const { return m_filler; }
std::shared_ptr<TH1> PlotFiller::get_template() const { return m_template; }
unsigned int PlotFiller::dimension() const { return m_template->GetDimension(); }
std::string PlotFiller::name() const { return m_template->GetName(); }
///###########################################################################
///                         PlotProvider
///###########################################################################
PlotProvider::PlotProvider(const Sample<SagittaBiasAnalyzer>& smp, const PlotFiller& filler, const PhaseSpaceCuts& cut_element) :
    m_smp(smp), m_cuts(cut_element), m_filler(filler) {}

PlotProvider::PlotProvider(const Sample<SagittaBiasAnalyzer>& smp,
                           const std::function<int(const TH1*, SagittaBiasAnalyzer&, int)>& fill_func, PlotUtils::HistoBinConfig histo,
                           const PhaseSpaceCuts& cut_element) :
    PlotProvider(smp, fill_func, histo.make_histo(), cut_element) {}

PlotProvider::PlotProvider(const Sample<SagittaBiasAnalyzer>& smp,
                           const std::function<int(const TH1*, SagittaBiasAnalyzer&, int)>& fill_func, const std::shared_ptr<TH1>& histo,
                           const PhaseSpaceCuts& cut_element) :
    PlotProvider(smp, PlotFiller{fill_func, histo}, cut_element) {}
std::string PlotProvider::name() const { return m_cuts.name() + (m_cuts.name().empty() ? "" : "__") + get_template()->GetName(); }
std::string PlotProvider::selection_name() const { return m_cuts.name(); }
std::string PlotProvider::title() const { return m_cuts.title(); }
unsigned int PlotProvider::dimension() const { return get_template()->GetDimension(); }
std::function<int(const TH1*, SagittaBiasAnalyzer&, int)> PlotProvider::get_filler() const { return m_filler.get_filler(); }
std::shared_ptr<TH1> PlotProvider::get_template() const { return m_filler.get_template(); }
Sample<SagittaBiasAnalyzer> PlotProvider::get_sample() const { return m_smp; }

PlotFillInstructionWithRef<TH1, SagittaBiasAnalyzer> PlotProvider::create_plot_filler(
    std::function<int(const TH1*, SagittaBiasAnalyzer&, int)> fill_func, int trk_type, std::shared_ptr<TH1> h_template) const {
    PlotFillInstructionWithRef<TH1, SagittaBiasAnalyzer> plot_filler{
        [fill_func, trk_type](TH1* h, SagittaBiasAnalyzer& t) {
            int bin_number = fill_func(h, t, trk_type);
            h->SetBinContent(bin_number, h->GetBinContent(bin_number) + t.event_weight());
            h->SetBinError(bin_number, std::hypot(h->GetBinError(bin_number), t.event_weight()));
        },
        h_template};
    plot_filler.setID(h_template->GetName() + MuonTrk::to_string(trk_type));
    return plot_filler;
}
Selection<SagittaBiasAnalyzer> PlotProvider::make_cut(int trk_type, std::pair<float, float> mass_window) const {
    return m_cuts.make_cut(trk_type, mass_window);
}
Plot<TH1> PlotProvider::make_plot(const Selection<SagittaBiasAnalyzer>& selection, int trk_type) const {
    return Plot<TH1>(
        GenericPostProcessing<TH1, TH1>(RunHistoFiller(get_sample(), selection, create_plot_filler(get_filler(), trk_type, get_template())),
                                        [](std::shared_ptr<TH1> h) {
                                            PlotUtils::shiftOverflows(h.get());
                                            return h;
                                        }),
        generate_format(trk_type));
}
PlotFormat PlotProvider::generate_format(int trk_type) const {
    return PlotFormat()
        .LegendTitle(MuonTrk::to_title(trk_type))
        .LegendOption("PL")
        .MarkerStyle(get_value(marker_map, trk_type))
        .Color(get_value(colour_map, trk_type));
}
std::string PlotProvider::selection_name(int trk_type, std::pair<float, float> mass_window) const {
    return m_cuts.selection_name(trk_type, mass_window);
}
Plot<TH1> PlotProvider::make_forward_plot(int trk_type, std::pair<float, float> mass_window) const {
    return make_plot(make_forward_cut(trk_type, mass_window), trk_type);
}
Selection<SagittaBiasAnalyzer> PlotProvider::make_forward_cut(int trk_type, std::pair<float, float> mass_window) const {
    Selection<SagittaBiasAnalyzer> forward{[trk_type](SagittaBiasAnalyzer& t) {
        float theta_cs = t.thetaCS(trk_type);
        return -M_PI_2 <= theta_cs && theta_cs <= M_PI_2;
    }};
    forward.setID(MuonTrk::to_string(trk_type) + "__Forward");
    Selection<SagittaBiasAnalyzer> full_sel = forward && m_cuts.make_cut(trk_type, mass_window);
    return full_sel;
}
Selection<SagittaBiasAnalyzer> PlotProvider::make_backward_cut(int trk_type, std::pair<float, float> mass_window) const {
    Selection<SagittaBiasAnalyzer> backward{[trk_type](SagittaBiasAnalyzer& t) {
        float theta_cs = t.thetaCS(trk_type);
        return ((-M_PI_2 > theta_cs) || (theta_cs > M_PI_2));
    }};
    backward.setID(MuonTrk::to_string(trk_type) + "__Backward");
    Selection<SagittaBiasAnalyzer> full_sel = backward && m_cuts.make_cut(trk_type, mass_window);
    return full_sel;
}
Plot<TH1> PlotProvider::make_backward_plot(int trk_type, std::pair<float, float> mass_window) const {
    return make_plot(make_backward_cut(trk_type, mass_window), trk_type);
}
Plot<TH1> PlotProvider::make_plot(int trk_type, std::pair<float, float> mass_window) const {
    Selection<SagittaBiasAnalyzer> full_sel = m_cuts.make_cut(trk_type, mass_window);
    // full_sel.setID(selection_name(trk_type, mass_window) + "__Inclusive");
    return make_plot(full_sel, trk_type);
}

Plot<TH1> PlotProvider::make_asymmetry(int trk_type, std::pair<float, float> mass_window) const {
    return Plot<TH1>(AsymmetryPlot<TH1>{name(), make_forward_plot(trk_type, mass_window), make_backward_plot(trk_type, mass_window)},
                     generate_format(trk_type));
}
void PlotProvider::add_plot(std::vector<int> trk_types, PlotWriter<TH1>& plot_writer, std::pair<float, float> mass_window) const {
    ClearFromDuplicates(trk_types);
    for (const int& trk : trk_types) { add_plot(trk, plot_writer, mass_window); }
}
void PlotProvider::add_plot(int trk_type, PlotWriter<TH1>& plot_writer, std::pair<float, float> mass_window) const {
    int injected = SagittaBiasAnalyzer::inject_from_type(trk_type);

    const SagittaBiasAnalyzer::calib_constants c =
        injected != -1 ? SagittaBiasAnalyzer::sagitta_injections()[injected] : SagittaBiasAnalyzer::calib_constants{trk_type};
    const std::string histo_name = "Template_" + c.to_string().substr(c.to_string().find("_") + 1);
    const std::string slimmed_trk_name = MuonTrk::to_string(trk_type & MuonTrk::TrackType::OrdinaryTrks);
    const std::string pl_name = slimmed_trk_name + "/" + std::string{get_template()->GetName()} + "/" + m_cuts.name() +
                                "/KinSel_Inclusive/" + Form("MLL_%.0f_%.0f", mass_window.first, mass_window.second) + "/" + histo_name;
    plot_writer.add_plot(make_plot(trk_type, mass_window), pl_name);
}

void PlotProvider::add_asymmetry_plots(std::vector<int> trk_types, PlotWriter<TH1>& plot_writer,
                                       std::pair<float, float> mass_window) const {
    ClearFromDuplicates(trk_types);
    for (const int& trk : trk_types) { add_asymmetry_plots(trk, plot_writer, mass_window); }
}

void PlotProvider::add_asymmetry_plots(int trk_type, PlotWriter<TH1>& plot_writer, std::pair<float, float> mass_window) const {
    int injected = SagittaBiasAnalyzer::inject_from_type(trk_type);

    const SagittaBiasAnalyzer::calib_constants c =
        injected != -1 ? SagittaBiasAnalyzer::sagitta_injections()[injected] : SagittaBiasAnalyzer::calib_constants{trk_type};

    const std::string histo_name = "Template_" + c.to_string().substr(c.to_string().find("_") + 1);
    const std::string slimmed_trk_name = MuonTrk::to_string(trk_type & MuonTrk::TrackType::OrdinaryTrks);
    const std::string fw_name = slimmed_trk_name + "/" + std::string{get_template()->GetName()} + "/" + m_cuts.name() + "/Forward/" +
                                Form("MLL_%.0f_%.0f", mass_window.first, mass_window.second) + "/" + histo_name;
    const std::string bw_name = slimmed_trk_name + "/" + std::string{get_template()->GetName()} + "/" + m_cuts.name() + "/Backward/" +
                                Form("MLL_%.0f_%.0f", mass_window.first, mass_window.second) + "/" + histo_name;
    plot_writer.add_plot(make_forward_plot(trk_type, mass_window), fw_name);
    plot_writer.add_plot(make_backward_plot(trk_type, mass_window), bw_name);
}
