#include <CLHEP/Random/JamesRandom.h>
#include <CLHEP/Random/RandGauss.h>
#include <Math/GenVector/VectorUtil.h>
#include <MuonSagittaBias/SagittaInjector.h>

///#############################################################################
///     General set of helper functions needed for the sagitta bias analysis
///##############################################################################
namespace {
    constexpr float muon_mass = 105.6 * 1.e-3;
    static const SagittaInjector::FourVector null_vec;
}  // namespace
float SagittaInjector::sagitta_bias() const { return m_calib.sagitta_bias; }
float SagittaInjector::radial_bias() const { return m_calib.radial_bias; }
float SagittaInjector::magnetic_bias() const { return m_calib.magnetic_bias; }
int SagittaInjector::trk_type() const { return m_calib.trk_type & MuonTrk::OrdinaryTrks; }
int SagittaInjector::trk_mask() const { return m_calib.trk_type; }

float SagittaInjector::resolution() const { return m_calib.res_const; }

CollinsSoper::PolarVector4D SagittaInjector::shift_sagitta(const float pt, const float eta, const float phi, const int charge) const {
    const double cosh_eta = std::cosh(eta);
    const double P = pt * cosh_eta;

    double P_shifted{P};
    if (trk_mask() & MuonTrk::TrackType::SagittaBias) { P_shifted /= (1. + charge * sagitta_bias() * P); }

    if (trk_mask() & MuonTrk::TrackType::MagneticBias) { P_shifted *= (1. + magnetic_bias()); }
    float Pt_shifted = P_shifted / cosh_eta;
    float mu_eta{eta};

    if (trk_mask() & MuonTrk::TrackType::RadialBias) {
        const double P_z = std::sqrt(std::max(0., std::pow(P_shifted, 2) - std::pow(Pt_shifted, 2)));
        /// Radial bias transformations
        ///     p_{T} --> p_{T}(1+ rho)
        ///     tan (theta) = p_{Z} /p_{T}
        Pt_shifted *= (1. + radial_bias());
        const double theta = std::atan2(Pt_shifted, P_z);
        mu_eta = -std::log(std::tan(theta / 2));
    }

    if (trk_mask() & MuonTrk::TrackType::DetectorResolution) {
        const unsigned int seed = m_parent_tree.eventNumber() + std::abs(eta * phi + charge) * 1.e5;
        TRandom3 random{seed};
        const float sigma = std::abs(m_calib.res_const + m_calib.res_lin * Pt_shifted + m_calib.res_quad * std::pow(Pt_shifted, 2) +
                                     m_calib.res_hyp / Pt_shifted);
        Pt_shifted *= random.Gaus(1., sigma);
    }

    CollinsSoper::PolarVector4D v_shifted{Pt_shifted, mu_eta, phi, muon_mass};
    return v_shifted;
}
float SagittaInjector::calc_pseudo_M(const FourVector& p1, const FourVector& p2) const {
    const float M2 = 2. * p1.P() * p1.Pt() * (1. - ROOT::Math::VectorUtil::CosTheta(p1, p2)) / std::sqrt(1. - std::pow(p2.Z() / p2.R(), 2));
    return std::sqrt(M2);
}

SagittaInjector::SagittaInjector(SagittaBiasAnalyzer& bias_tree, const MuonTrk::calib_constants& constants) :
    m_parent_tree{bias_tree}, m_calib{constants} {}

void SagittaInjector::inject() {
    if (m_parent_tree.getCurrEntry() == m_last_cache) return;
    m_last_cache = m_parent_tree.getCurrEntry();
    if (!m_parent_tree.is_valid(trk_type())) {
        m_p1 = m_p2 = m_p1_CS = m_p2_CS = null_vec;
        m_frame.reset();
        return;
    }
    m_p1 = shift_sagitta(m_parent_tree.pt(trk_type(), SagittaBiasAnalyzer::LepCharge::Plus),
                         m_parent_tree.eta(trk_type(), SagittaBiasAnalyzer::LepCharge::Plus),
                         m_parent_tree.phi(trk_type(), SagittaBiasAnalyzer::LepCharge::Plus), 1);
    m_p2 = shift_sagitta(m_parent_tree.pt(trk_type(), SagittaBiasAnalyzer::LepCharge::Minus),
                         m_parent_tree.eta(trk_type(), SagittaBiasAnalyzer::LepCharge::Minus),
                         m_parent_tree.phi(trk_type(), SagittaBiasAnalyzer::LepCharge::Minus), -1);
    m_Pseudo_M_neg = calc_pseudo_M(m_p2, m_p1);
    m_Pseudo_M_pos = calc_pseudo_M(m_p1, m_p2);
    m_frame = std::make_unique<CollinsSoper::RestFrame>(m_p1, m_p2);
    /// Calculate the quantities in the collins sopper frame
    m_p1_CS = m_frame->toRestFrame(m_p1);
    m_p2_CS = m_frame->toRestFrame(m_p2);
}

/// Get the shifted 4-momenta in the rest frame
const SagittaInjector::FourVector& SagittaInjector::pos_lep() {
    inject();
    return m_p1;
}
const SagittaInjector::FourVector& SagittaInjector::neg_lep() {
    inject();
    return m_p2;
}
const SagittaInjector::FourVector& SagittaInjector::inv_p4() {
    inject();
    return m_frame ? m_frame->invariantP4() : m_p1;
}
const SagittaInjector::FourVector& SagittaInjector::pos_lep_CS() {
    inject();
    return m_p1_CS;
}
const SagittaInjector::FourVector& SagittaInjector::neg_lep_CS() {
    inject();
    return m_p2_CS;
}
float SagittaInjector::pseudo_mass_pos() {
    inject();
    return m_Pseudo_M_pos;
}
float SagittaInjector::pseudo_mass_neg() {
    inject();
    return m_Pseudo_M_neg;
}
