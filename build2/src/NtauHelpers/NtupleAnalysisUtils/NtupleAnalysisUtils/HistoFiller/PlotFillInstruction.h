#ifndef NTAU_PLOTFILLINSTRUCTION__H
#define NTAU_PLOTFILLINSTRUCTION__H

#include <functional>
#include <string>
#include <memory>

#include "NtupleAnalysisUtils/HistoFiller/HistoFillerInterfaces.h"

/// The PlotFillInstruction class packages the 
/// abstract instructions for filling a plot 
/// from an input object. 
/// Essentially just a wrapper around an std::function with 
/// an identifier glued on it

template <class Fillable, class FillFrom> class PlotFillInstruction: public ISortableByID{
public:
    /// instantiate by passing a filler function (lambda / fcn ptr / functor / ...) 
    /// The function should define how to fill one entry into the fillable object (first arg) 
    /// based on the content of the object to fill from (second arg).
    /// Example: Fill a histogram based on a branch in a tree.  
    PlotFillInstruction (std::function<void(Fillable*, FillFrom &)> fillFunc);

    /// Special constructor for vectorial filling. Here, we pass three functions: 
    /// 1) A function which determines for each tree entry how many vector entries to process (e.g. check an n_leptons branch)
    /// 2) A function which checks a given index k and decided whether or not to fill for it (e.g. only fill leptons above a certain pt)
    /// 3) A function which handles the actual filling for the index k (e.g. fill the histo with the pt of one given lepton)
    PlotFillInstruction (std::function <size_t (FillFrom &)> numberOfEntriesToFill, 
                         std::function <bool(FillFrom &, size_t k)> selectionFuncPerElement,
                         std::function <void(Fillable*, FillFrom &, size_t k)> fillFuncPerElement );

    /// access to members - frequently used during the event loop 
    /// Normally, users will not need these, they are used by client code. 
    
    /// Access the full function
    inline std::function<void(Fillable*, FillFrom &)> getFillFunc() const;
    /// Pipes calls to filler method
    inline void fill(Fillable* target, FillFrom & input) const;
    /// Alternative fill signature
    inline void operator()(Fillable* target, FillFrom & input) const;

protected:
    std::function<void(Fillable*, FillFrom &)> m_fill;
};

/// For convenience, we also allow the user to directly include their desired histogram binning
/// The PlotFillInstructionWithRef class takes care of this. 
/// 
/// It has two kinds of constructors: Either we capture an existing histogram for the binning,
/// or we define the binning histogram in-place. 
///
/// For each kind, we can invoke the two PlotFillInstruction constructor signatures or extend
/// an existing PlotFillInstruction. 

template <class Fillable, class FillFrom> class PlotFillInstructionWithRef : public PlotFillInstruction<Fillable,FillFrom>{
  public:
      /// This signature is equivalent to the first PlotFillInstruction constructor, and adds a second arg for the desired binning
      PlotFillInstructionWithRef (std::function<void(Fillable*, FillFrom &)> fillFunc, 
                                  Fillable* refForFill);
      // This signature is equivalent to the second PlotFillInstruction constructor, and adds a final arg for the desired binning
      PlotFillInstructionWithRef (
                 std::function <size_t (FillFrom &)> numberOfEntriesToFill, 
                 std::function <bool(FillFrom &, size_t k)> selectionFuncPerElement,
                 std::function <void(Fillable*, FillFrom &, size_t k)> fillFuncPerElement,
                 Fillable* refForFill);

      /// This signature can retroactively extend an existing fill instruction 
      PlotFillInstructionWithRef (const PlotFillInstruction<Fillable,FillFrom> base, 
                                  Fillable* refForFill);


      /// The next set is  equivalent to the above, but uses a shared_ptr to transport the ref histo. 

      /// This signature is equivalent to the first PlotFillInstruction constructor, and adds a second arg for the desired binning
      PlotFillInstructionWithRef (std::function<void(Fillable*, FillFrom &)> fillFunc, 
                                  std::shared_ptr<Fillable> refForFill);
      // This signature is equivalent to the second PlotFillInstruction constructor, and adds a final arg for the desired binning
      PlotFillInstructionWithRef (
                 std::function <size_t (FillFrom &)> numberOfEntriesToFill, 
                 std::function <bool(FillFrom &, size_t k)> selectionFuncPerElement,
                 std::function <void(Fillable*, FillFrom &, size_t k)> fillFuncPerElement,
                 std::shared_ptr<Fillable> refForFill);

      /// This signature can retroactively extend an existing fill instruction 
      PlotFillInstructionWithRef (const PlotFillInstruction<Fillable,FillFrom> base, 
                                  std::shared_ptr<Fillable> refForFill);

      /// The following three signatures are equivalent to the above, but allow to construct the binning histogram in-place. 

      /// Equivalent to first PlotFillInstruction constructor + in-place binning histogram
      template <class... Args> PlotFillInstructionWithRef (std::function<void(Fillable*, FillFrom &)> fillFunc, 
                                                           Args&& ... args);

      /// Equivalent to second PlotFillInstruction constructor + in-place binning histogram
      template <class... Args> PlotFillInstructionWithRef (
                 std::function <size_t (FillFrom &)> numberOfEntriesToFill, 
                 std::function <bool(FillFrom &, size_t k)> selectionFuncPerElement,
                 std::function <void(Fillable*, FillFrom &, size_t k)> fillFuncPerElement,
                 Args&& ... args);
      
      /// Equivalent to extending existing PlotFillInstruction w/ in-place binning histogram
      template <class... Args> PlotFillInstructionWithRef (const PlotFillInstruction<Fillable,FillFrom> base, 
                                                           Args&& ... args);


        Fillable* getRefHist() const;
private:
    std::shared_ptr<Fillable> m_refHist;
};

#include "PlotFillInstruction.ixx"

#endif // NTAU_PLOTFILLINSTRUCTION__H
