#ifndef __NTAU__PLOT__HIFI__POPULATOR__H
#define __NTAU__PLOT__HIFI__POPULATOR__H

#include "NtupleAnalysisUtils/HistoFiller/HistoFiller.h"
#include "NtupleAnalysisUtils/HistoFiller/PlotFillInstruction.h"
#include "NtupleAnalysisUtils/HistoFiller/Sample.h"
#include "NtupleAnalysisUtils/HistoFiller/Selection.h"
#include "NtupleAnalysisUtils/Plot/IPlotPopulator.h"


/// The RunHistoFiller populator  is used for filling histograms from an input sample with the HistoFiller.
/// It will trigger an event loop when populating the plot it belongs to.
/// This loop will automatically account for *all* other histo filler populators defined
/// at the time of running, ensuring maximum efficiency.
/// 
/// For best results, make sure to define all your needed RunHistoFiller populators before 
/// you retrieve the first output. 
///
/// The RunHistoFiller populator needs to know four key items to run: 
/// a) an input sample. This is typically one or a set of trees each within a file. 
/// b) a selection to run. This is the condition for filling the object we wish to produce. 
/// c) A fill instruction. Defines how our object is filled if the selection is passed 
/// d) a binning to use for the object we wish to create. This can also be packaged with the fill instruction. 
/// In addition, it is possible to specify a custom HistoFiller instance. This is usually only needed for validation checks 

/// The class is templated by the output object type and the input object type (usually an Ntuple). 
template <class HistoType, class inputType> class RunHistoFiller: public IPlotPopulator<HistoType>, public IFillableFrom<inputType>{
public:

    /// The first constructor allows to specify the binning and the fill instruction separately 
    RunHistoFiller(   HistoType* refHisto,                   // A histo to use a reference for 
                                                             // the desired binning and axis labels
                 Sample<inputType> sample,                   // Sample this plot belongs to
                 Selection<inputType> selection,             // Selection this plot belongs to 
                 PlotFillInstruction<HistoType,inputType> fillInstruction, // Define how a fill step should look
                 HistoFiller* filler = nullptr );             // If not specified, will use the default filler. Can override to a specific filler here.

    /// The second constructor supports the 'PlotFillInstructionWithRef', which combines binning and the fill instruction in one entity 
    RunHistoFiller( Sample<inputType> sample,              // Sample this plot belongs to
                    Selection<inputType> selection,        // Selection this plot belongs to 
                    PlotFillInstructionWithRef<HistoType,inputType> fillInstruction, //  Define how a fill step should look, and the desired binning
                    HistoFiller* filler = nullptr );       // If not specified, will use the default filler. Can override to a specific filler here.

    /// The third constructor is identical for the first, but allows to directly pass 
    /// a function in place of the fill instruction - mainly for convenience 
    RunHistoFiller(   HistoType* refHisto,                   // A histo to use a reference for 
                                                             // the desired binning and axis labels
                 Sample<inputType> sample,                   // Sample this plot belongs to
                 Selection<inputType> selection,             // Selection this plot belongs to 
                 std::function<void(HistoType*, inputType &)> fillFunction, // Define how a fill step should look
                 HistoFiller* filler = nullptr );             // If not specified, will use the default filler. Can override to a specific filler here.


    /// Copy constructors.
    /// Will always clone the contained histogram. 
    RunHistoFiller(const RunHistoFiller<HistoType,inputType > & other);
    void operator= (const RunHistoFiller<HistoType,inputType > & other);  

    /// fill method as specified in the IFillable interface. Used within the histo filler, 
    /// but can also be user-called.
    /// Internally invokes the attached fill instruction. 
    virtual void Fill(inputType &  input) override;

    /// clone method - used in order to process several files simultaneously.
    /// Will populate the clone with a clone of the current histo. 
    virtual std::shared_ptr<RunHistoFiller<HistoType, inputType> > cloneBase() const;
    /// two other clones with different return types
    virtual std::shared_ptr<IFillable > clone() const override;
    virtual std::shared_ptr<IPlotPopulator<HistoType>> clonePopulator() const override;

    /// populate method (IPlotPopulator) 
    virtual std::shared_ptr<HistoType> populate() override;

    /// get the selection for which the plot should fill itself
    virtual std::shared_ptr<ISelection> getSelectionClone() const override;
    /// get the sample for which the plot should fill itself
    virtual std::shared_ptr<ISample> getSampleClone() const override;
    /// access the internal histogram
    std::shared_ptr<HistoType> getSharedHisto();

    /// Access to the plot ID and comparison operator
    virtual bool equalTo (std::shared_ptr<IFillable> & other) const override;

    /// Helper methods 
    /// reunite itself with a set of filled clones by adding them back to the parent (after clearing it)
    virtual void CollectClones(std::vector<std::shared_ptr<IFillable > > & clonesToCollect ) override;

    /// reunite itself with a single of filled clone by adding it back to the parent - protected by a mutex, will lock
    virtual void CollectClone(std::shared_ptr<IFillable > cloneToCollect ) override;

    /// reset the content 
    virtual void Reset() override;
    /// helper method
    /// check if addition is supported for this histogram type. 
    virtual bool canAdd() const override;

    /// check if this has been filled already, or set the flag. 
    virtual bool hasBeenFilled() const override;
    virtual void setHasBeenFilled(bool val=true) override;

    /// helper to register with a histo filler 
    void registerWithHistoFiller (HistoFiller* hf);

protected: 
    std::shared_ptr<HistoType>                m_internalHisto;
    Sample<inputType>                         m_sample; 
    Selection<inputType>                      m_selection; 
    PlotFillInstruction<HistoType, inputType> m_fillInstruction; 
    HistoFiller*                              m_filler;
    bool                                      m_hasBeenFilled;
    std::mutex                                m_mutex_collect; 
};


// helpers needed to allow an "addition" placeholder to be defined even for objects which can not really be added 
// to one another. 
// rely on template specialisation to implement non-trivial addition for supported cases. 

template <class Thing> void AddHistos(std::shared_ptr<Thing> , std::shared_ptr<Thing> ){
    std::cerr << "Careful, you are attempting to add two histos which can not be added! Should never call..."<<std::endl;
}
void AddHistos(std::shared_ptr<TH1> addTo, std::shared_ptr<TH1> addThis);
void AddHistos(std::shared_ptr<TH2> addTo, std::shared_ptr<TH2> addThis);
void AddHistos(std::shared_ptr<TH3> addTo, std::shared_ptr<TH3> addThis);
void AddHistos(std::shared_ptr<TH1D> addTo, std::shared_ptr<TH1D> addThis);
void AddHistos(std::shared_ptr<TH1F> addTo, std::shared_ptr<TH1F> addThis);
void AddHistos(std::shared_ptr<TH2D> addTo, std::shared_ptr<TH2D> addThis);
void AddHistos(std::shared_ptr<TH3D> addTo, std::shared_ptr<TH3D> addThis);
void AddHistos(std::shared_ptr<TH2F> addTo, std::shared_ptr<TH2F> addThis);
void AddHistos(std::shared_ptr<TProfile> addTo, std::shared_ptr<TProfile> addThis);
void AddHistos(std::shared_ptr<TEfficiency> addTo, std::shared_ptr<TEfficiency> addThis);


// helpers needed to allow an "addition" placeholder to be defined even for objects which can not really be added 
// to one another. 
// rely on template specialisation to implement non-trivial addition for supported cases. 

template <class Thing> void ResetHisto(std::shared_ptr<Thing>){
    std::cerr << "Careful, you are attempting to reset an object which can not be reset! Should never call..."<<std::endl;
}
void ResetHisto(std::shared_ptr<TH1> resetMe);
void ResetHisto(std::shared_ptr<TH2> resetMe);
void ResetHisto(std::shared_ptr<TH3> resetMe);
void ResetHisto(std::shared_ptr<TH1D> resetMe);
void ResetHisto(std::shared_ptr<TH1F> resetMe);
void ResetHisto(std::shared_ptr<TH2D> resetMe);
void ResetHisto(std::shared_ptr<TH3D> resetMe);
void ResetHisto(std::shared_ptr<TH2F> resetMe);
void ResetHisto(std::shared_ptr<TProfile> resetMe);
void ResetHisto(std::shared_ptr<TEfficiency> resetMe);

// A set of helpers, used to determine if addition is supported by a histogram type. 
// same strategy as above
template <class Thing> constexpr bool hasAddMethod(){
    return false;
}
template <> constexpr bool hasAddMethod<TH1>(){
    return true;
}
template <> constexpr bool hasAddMethod<TH1D>(){
    return true;
}
template <> constexpr bool hasAddMethod<TH1F>(){
    return true;
}
template <> constexpr bool hasAddMethod<TH2>(){
    return true;
}
template <> constexpr bool hasAddMethod<TH2D>(){
    return true;
}
template <> constexpr bool hasAddMethod<TH2F>(){
    return true;
}
template <> constexpr bool hasAddMethod<TH3>(){
    return true;
}
template <> constexpr bool hasAddMethod<TH3D>(){
    return true;
}
template <> constexpr bool hasAddMethod<TH3F>(){
    return true;
}
template <> constexpr bool hasAddMethod<TProfile>(){
    return true;
}
template <> constexpr bool hasAddMethod<TEfficiency>(){
    return true;
}

#include "HistoFillerPlotPopulator.ixx"

#endif