#include <MuonSagittaBias/SagittaBiasAnalyzer.h>
#include <MuonSagittaBias/TriggerAccess.h>
#include <MuonSagittaBias/TriggerPreScaleService.h>
#include <MuonSagittaBias/Utils.h>
#include <MuonSagittaBias/WeightBranches.h>
///#####################################################
///             GenWeightBranch
///#####################################################
unsigned int GenWeightBranch::s_lhe_variation = 0;
unsigned int GenWeightBranch::lhe_variation() { return s_lhe_variation; }
void GenWeightBranch::set_lhe_variation(unsigned int _var) { s_lhe_variation = _var; }

GenWeightBranch::GenWeightBranch(SagittaBiasTree* tree) :
    DerivedVirtualBranch([this](SagittaBiasTree& t) -> double { return get_weight(t); }, tree),
    m_norm_db{MuonTP::NormalizationDataBase::getDataBase()},
    m_is_mc{tree->mcEventWeight.existsInTree()} {
    if (!m_is_mc) return;
    std::string weight_name = !lhe_variation() ? "mcEventWeight" : "mcEventWeight_LHE_" + std::to_string(lhe_variation());
    m_ev_weight = tree->getBranch<double>(weight_name);
}
bool GenWeightBranch::GenWeightBranch::is_mc() const { return m_is_mc; }
double GenWeightBranch::get_weight(SagittaBiasTree& t) {
    if (!is_mc()) return 1.;
    if (!m_ev_weight) return 0.;
    const unsigned int dsid = t.mcChannelNumber();
    if (!m_current_weighter || m_current_weighter->DSID() != dsid) {
        std::shared_ptr<MuonTP::MonteCarloPeriodHandler> period_handler = m_norm_db->getPeriodHandler(dsid, lhe_variation());
        m_current_weighter = period_handler->getHandler();
    }
    return m_current_weighter->finalWeight() * (*m_ev_weight)();
}
///#####################################################
///             PileUpWeightBranch
///#####################################################
PileUpWeightBranch::PileUpWeightBranch(SagittaBiasTree* tree) :
    DerivedVirtualBranch([this](SagittaBiasTree& t) -> double { return get_weight(t); }, tree),
    m_norm_db{MuonTP::NormalizationDataBase::getDataBase()},
    m_has_prw{tree->prwWeight.existsInTree()} {}

bool PileUpWeightBranch::has_prw() const { return m_has_prw; }
double PileUpWeightBranch::get_weight(SagittaBiasTree& t) {
    if (!has_prw()) { return 1.; }
    const unsigned int dsid = t.mcChannelNumber();
    const unsigned int period = t.runNumber();
    if (!m_current_period || m_current_period->DSID() != dsid) {
        m_current_period = m_norm_db->getPeriodHandler(dsid, GenWeightBranch::lhe_variation());
        if (!m_current_period) return 0.;
        m_all_weighter = m_current_period->getHandler();
        if (m_current_period->getMCcampaigns().size() < 2) {
            m_run_weighter = m_all_weighter;
            m_cached_reweight = 1.;
        } else {
            m_run_weighter.reset();
        }
    }
    if (m_run_weighter != m_all_weighter) {
        if (!m_run_weighter || m_run_weighter->runNumber() != period) {
            m_run_weighter = m_current_period->getHandler(period);
            m_cached_reweight =
                (m_all_weighter->SumW() / m_run_weighter->SumW()) * (m_run_weighter->prwLuminosity() / m_all_weighter->prwLuminosity());
        }
    }
    return t.prwWeight() * m_cached_reweight;
}

///#####################################################
///             TrigPreScaleWeightBranch
///#####################################################
using TrigSelVector = SagittaBiasAnalyzer::TrigSelVector;
using TrigSelPtr = SagittaBiasAnalyzer::TrigSelPtr;
int TrigPreScaleWeightBranch::s_strategy = TriggerPreScale::Strategy::NotApplied;
bool TrigPreScaleWeightBranch::set_prescale_strategy(int strat) {
    s_strategy = strat;
    return strat == TriggerPreScale::Strategy::NotApplied || strat == TriggerPreScale::Strategy::PreScaleMC ||
           strat == TriggerPreScale::Strategy::UnPreScaleData;
}
TrigPreScaleWeightBranch::TrigPreScaleWeightBranch(SagittaBiasAnalyzer* tree) :
    DerivedVirtualBranch([this](SagittaBiasAnalyzer& t) -> double { return get_weight(t); }, tree),
    m_calib_items{TriggerPreScaleService::getService()->prescale_calibrators()} {}
double TrigPreScaleWeightBranch::get_weight(SagittaBiasAnalyzer& t) {
    if (s_strategy == TriggerPreScale::Strategy::NotApplied || (t.is_mc() && s_strategy == TriggerPreScale::Strategy::UnPreScaleData) ||
        (!t.is_mc() && s_strategy == TriggerPreScale::Strategy::PreScaleMC)) {
        return 1.;
    }

    double overall_prescale{1.};
    bool passed{false};
    /// Retrieve first the vector of potential triggers from the analysis
    ///     --- Constraint on the resonance
    ///     --- Data-taking period
    TrigSelVector& trig_candidates = t.potential_triggers();

    /// We run on data
    if (!t.is_mc()) {
        for (TrigSelPtr& trig : trig_candidates) {
            if (!trig->access().pass_decision()) { continue; }
            passed = true;
            overall_prescale *= (1. - (1. / trig->access().prescale()));
            /// Check if the last trigger was actually an unprescaled one.
            if (std::abs(overall_prescale) <= FLT_EPSILON) break;
        }
    } else {
        const int run = t.run_number();
        const int lumi_block = t.lumi_block_number();
        const std::vector<std::shared_ptr<const TriggerPreScaleItem>>::const_iterator begin = m_calib_items.begin();
        const std::vector<std::shared_ptr<const TriggerPreScaleItem>>::const_iterator end = m_calib_items.end();
        std::vector<std::shared_ptr<const TriggerPreScaleItem>>::const_iterator itr = begin;
        /// Loop over the vector of trigger candidates
        for (TrigSelPtr& trig : trig_candidates) {
            if (!trig->access().pass_decision()) continue;
            /// The basic idea behind this look-up:
            ///     Both vectors are sorted by the object id
            itr = std::find_if(itr, end, [&trig](const std::shared_ptr<const TriggerPreScaleItem>& calib_item) {
                return calib_item->id() == trig->id() || calib_item->id() > trig->id();
            });
            /// First check if there is a suitable cancdidate at all
            if (itr == end) {
                itr = begin;
                continue;
            }
            /// Then check whether the element does not exist in the list
            if ((*itr)->id() > trig->id()) { continue; }
            passed = true;
            overall_prescale *= (1. - (1. / (*itr)->get_prescale(run, lumi_block)));

            if (std::abs(overall_prescale) <= FLT_EPSILON) break;
        }
    }
    const double presc_weight = (1. - overall_prescale);
    return passed ? (t.is_mc() ? presc_weight : 1. / presc_weight) : 1.;
}
TrigPreScaleWeightBranch::~TrigPreScaleWeightBranch() = default;
