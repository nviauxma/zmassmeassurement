template <class H> PlotUtils::rawAxisRange PlotUtils::getRawRange (std::vector<Plot<H>> plots, size_t axis,const AxisConfig & axisCfg){
    double max = -std::numeric_limits<double>::max(); 
    double min = std::numeric_limits<double>::max(); 

    /// loop over all plots 
    bool fromBinning = false; 
    for (Plot<H> & plot : plots){
        
        /// For each, get the min/max
        rawAxisRange minMax{min,max,false};
        /// TODO: Understand why the standard resolution prefers the templated catch-all here
        if (plot->InheritsFrom("TH1")){
            minMax = getRawRange(dynamic_cast<TH1*>(plot()), axis, axisCfg);
        }
        else minMax = getRawRange(plot(), axis, axisCfg);

        /// replace the global min/max by the ones from this plot if larger/smaller  
        if (minMax.min < min) min = minMax.min;
        if (minMax.max > max) max = minMax.max;
        fromBinning |= minMax.fromBinning; 
    }

    /// return global min/max
    return rawAxisRange{min,max,fromBinning}; 
}

/// This is a catch-all. We try to interpret the axis as a binned one for lack of ideas. 
template <class H> PlotUtils::rawAxisRange PlotUtils::getRawRange (const H* p, size_t axis,const AxisConfig & axisCfg){
    std::cerr << "Caught a tricky case of getRawRange on a "<<p->ClassName()<<", trying the built-in axis limits!"<<std::endl;
    double thMin = axisCfg.Min();
    double thMax = axisCfg.Max();
    return rawAxisRange{  std::max(thMin, getAxis(p,axis)->GetXmin()), 
                          std::min(thMax, getAxis(p,axis)->GetXmax()), 
                          true
           }; 
}

template <class Histo> std::pair<double,double>  PlotUtils::getXRange (const std::vector<Plot<Histo>> & h, const CanvasOptions & opts){
    return updateAxisRange(getRawRange(h,0,opts.XAxis()),opts.XAxis()); 
} 
template <class Histo> std::pair<double,double>  PlotUtils::getYRange (const std::vector<Plot<Histo>> & h,const CanvasOptions & opts){
    return updateAxisRange(getRawRange(h,1,opts.YAxis()),opts.YAxis());
} 
template <class Histo> std::pair<double,double>  PlotUtils::getZRange (const std::vector<Plot<Histo>> & h,const CanvasOptions & opts){
    return updateAxisRange(getRawRange(h,2,opts.ZAxis()),opts.ZAxis());
} 

template <class Hist> std::pair<double,double>  PlotUtils::getRatioRange (const std::vector<Plot<Hist>> & histos,const CanvasOptions & opts){
    return updateAxisRange(getRawRange(histos,1,opts.RatioAxis()),opts.RatioAxis());
}

template <class H> const TAxis* PlotUtils::getAxis(const H* p, size_t axis){
    if (axis == 0) return p->GetXaxis();
    if (axis == 1) return p->GetYaxis();
    else return p->GetZaxis();
}

template <class H> std::string PlotUtils::getRatioTitle(const Plot<H> plot, const CanvasOptions & opts){
    std::string title = plot->GetYaxis()->GetTitle();
    if (opts.RatioAxis().Title.isUserSet()) title = opts.RatioAxis().Title(); 
    return title;
}
template <class H> std::string PlotUtils::getYtitle(const Plot<H> plot, const CanvasOptions & opts){
    std::string title = plot->GetYaxis()->GetTitle();
    if (opts.YAxis().Title.isUserSet()) title = opts.YAxis().Title(); 
    return title;
}
template <class H> std::string PlotUtils::getXtitle(const Plot<H> plot, const CanvasOptions & opts){
    std::string title = plot->GetXaxis()->GetTitle();
    if (opts.XAxis().Title.isUserSet()) title = opts.XAxis().Title(); 
    return title;
}
