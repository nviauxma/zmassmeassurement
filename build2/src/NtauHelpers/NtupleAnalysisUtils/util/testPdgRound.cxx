
#include "NtupleAnalysisUtils/NTAUTopLevelIncludes.h"
void testRound(PlotUtils::valWithAsymmErr v){
    auto rounded = PlotUtils::pdgRound(v);
    std::cout <<" Rounding "<<v.cen<<" +"<<v.error_up <<" / -"<<v.error_down<<" to   "<<rounded.cen<<" +"<<rounded.error_up<<" /-"<<rounded.error_down<<std::endl;
}

void testRound(std::pair<double,double> v){
    auto rounded = PlotUtils::pdgRound(v);
    std::cout <<" Rounding "<<v.first<<" +/-"<<v.second << " to   "<<rounded.first<<" +/- "<<rounded.second<<std::endl;
}

int main(int, char**){

    for (double k = 0.001; k < 1.000; k+=0.005){
        testRound({0.678, k,k - 0.001});
    }
}
