#include <MuonSagittaBias/ArgumentParser.h>
#include <MuonSagittaBias/SagittaBiasAnalyzer.h>
#include <MuonSagittaBias/TriggerPreScaleService.h>
#include <MuonSagittaBias/WeightBranches.h>
#include <NtauHelpers/LumiCalculator.h>
#include <NtauHelpers/MuonTPMetaData.h>

void ArgumentParser::print_help() const {
    std::cout << "##########################################################################################" << std::endl;
    std::cout << "   Command line arguments to run " << app_name() << std::endl;
    std::cout << "##########################################################################################" << std::endl;
    std::cout << " --dir [DIR1, DIR2, ...]         :  Use all ROOT files from the following directories    #" << std::endl;
    std::cout << " --rootFile [FILE1, FILE2, ...]  :  Pipe directly the paths of the input files to use    #" << std::endl;
    std::cout << " --fileList [LIST1, LIST2, ...]  :  Read file paths from txt files where each line re-   #" << std::endl;
    std::cout << "                                    presents another file path.                          #" << std::endl;
    std::cout << " --begin <int>                   :  Start the analysis beginning from the n-th file      #" << std::endl;
    std::cout << " --end   <int>                   :  Only consider up to the n-th file for the analysis.  #" << std::endl;
    std::cout << " --singleThread                  :  Use only one thread to process the dataset.          #" << std::endl;
    std::cout << " --nThreads <int>                :  Specifiy the number of concurrently running threads. #" << std::endl;
    std::cout << " --outDir <string>               :  Store the analysis output in directory <dir>.        #" << std::endl;
    std::cout << " --outFile <string>              :  Specify the output ROOT file name.                   #" << std::endl;
    std::cout << " --injectionConfig <string>      :  Configuration file containing all the constants to   #" << std::endl;
    std::cout << "                                    be used for sagitta, radial, magnetic bias injection #" << std::endl;
    std::cout << " --noPDFs                        :  Do not create any PDF output file.                   #" << std::endl;
    std::cout << " --noROOT                        :  Do not create any ROOT output file.                  #" << std::endl;
    std::cout << " --trigPreScaleStrategy          :  Strategy for treatment of prescaled triggers         #" << std::endl;

    std::cout << "                                     " << TriggerPreScale::NotApplied
              << " -- do nothing                                    #" << std::endl;
    std::cout << "                                     " << TriggerPreScale::PreScaleMC
              << " -- reweight simulation                           #" << std::endl;
    std::cout << "                                     " << TriggerPreScale::UnPreScaleData
              << " -- reweight data                                 #" << std::endl;
    std::cout << " --trigPreScaleCalibFile         :  Specify trigger prescale calib file to reweight MC   #" << std::endl;
    std::cout << " --trigPreScaleSelFile           :  Specify file having list of trigger considered for   #" << std::endl;
    std::cout << "                                    prescale reweighting                                 #" << std::endl;
    std::cout << " --help / -h                     :  Print this diaglogue and exit.                       #" << std::endl;
    std::cout << "##########################################################################################" << std::endl;
    std::cout << "##########################################################################################" << std::endl;
}

void ArgumentParser::reset_in_files() {
    if (m_in_files_cleared) return;
    in_files.modify().clear();
    m_in_files_cleared = true;
}
bool ArgumentParser::eval_args(const std::vector<std::string>& arg_vec) {
    if (m_args_parsed) {
        std::cout << app_name() << "  -- WARNING: arguments were already parsed " << std::endl;
        return true;
    }
    SetAtlasStyle();

    const size_t n_args = arg_vec.size();
    bool single_thread{false};
    if (!n_args) { std::cout << app_name() << " -- WARNING: No arguments were given" << std::endl; }
    for (size_t n = 0; n < n_args; ++n) {
        const std::string& current_arg = arg_vec[n];

        if (current_arg == "--begin" && n + 1 < n_args) {
            int b = atoi(arg_vec[n + 1].c_str());
            begin(b);
            ++n;
        } else if (current_arg == "--end" && n + 1 < n_args) {
            int e = atoi(arg_vec[n + 1].c_str());
            end(e);
            ++n;
        }
        /// Pipe as many directories as you want in one go
        else if (current_arg == "--dir") {
            bool passed_once{false};
            while (n + 1 < n_args && arg_vec[n + 1].find("--") != 0) {
                std::vector<std::string> read_files = ListDirectory(arg_vec[n + 1], "*.root");
                passed_once |= !read_files.empty();
                reset_in_files();
                in_files.modify() += read_files;
                ++n;
            }
            if (!passed_once) {
                std::cerr << app_name() << " -- ERROR: Please provide at least one valid directory with --dir" << std::endl;
                print_help();
                return false;
            }
        }
        /// pipe the ROOT files directly
        else if (current_arg == "--rootFile") {
            bool passed_once{false};
            while (n + 1 < n_args && arg_vec[n + 1].find("--") != 0) {
                passed_once |= !arg_vec[n + 1].empty();
                reset_in_files();
                in_files.modify() += arg_vec[n + 1];
                ++n;
            }

            if (!passed_once) {
                std::cerr << app_name() << " -- ERROR: Please provide at least one file path using the option --rootFile. " << std::endl;
                return false;
            }
        }
        /// Pipe the file lists to read
        else if (current_arg == "--fileList") {
            bool passed_once{false};
            while (n + 1 < n_args && arg_vec[n + 1].find("--") != 0) {
                std::vector<std::string> read_files = ReadList(arg_vec[n + 1]);
                passed_once |= !read_files.empty();
                reset_in_files();
                in_files.modify() += read_files;
                ++n;
            }

            if (!passed_once) {
                std::cerr << app_name() << " -- ERROR: Please provide at least one file path using the option --fileList. " << std::endl;
                print_help();
                return false;
            }

        } else if (current_arg == "--outDir" && n + 1 < n_args) {
            out_dir(arg_vec[n + 1]);
            ++n;
        } else if (current_arg == "--outFile" && n + 1 < n_args) {
            out_root_file(arg_vec[n + 1]);
            ++n;
        } else if (current_arg == "--singleThread") {
            HistoFiller::getDefaultFiller()->setFillMode(HistoFillMode::singleThread);
            single_thread = true;
        } else if (current_arg == "--nThreads" && n + 1 < n_args) {
            num_threads(std::atoi(arg_vec[n + 1].c_str()));
            ++n;
        } else if (current_arg == "--help" || current_arg == "-h") {
            print_help();
            return false;
        } else if (current_arg == "--injectionConfig" && n + 1 < n_args) {
            inject_config_file(arg_vec[n + 1]);
            ++n;
        } else if (current_arg == "--noPDFs") {
            make_pdfs(false);
        } else if (current_arg == "--noROOT") {
            make_root_file(false);
        } else if (current_arg == "--trigPreScaleStrategy" && n + 1 < n_args) {
            int b = atoi(arg_vec[n + 1].c_str());
            prescale_strategy(b);
            ++n;
        } else if (current_arg == "--trigPreScaleCalibFile" && n + 1 < n_args) {
            prescale_calib_file(arg_vec[n + 1]);
            ++n;
        } else if (current_arg == "--trigPreScaleCalibFile" && n + 1 < n_args) {
            prescale_calib_file(arg_vec[n + 1]);
            ++n;
        } else if (current_arg == "--trigPreScaleSelFile" && n + 1 < n_args) {
            prescaled_triggers(arg_vec[n + 1]);
            ++n;
        }
        // else if (!eval_custom_args(arg_vec, n)){
        // print_help();
        // return false;
        // }
        else {
            std::cerr << app_name() << " unknown argument: " << current_arg << std::endl;
            print_help();
            return false;
        }
    }

    ClearFromDuplicates(in_files.modify());
    if (in_files().empty()) {
        std::cerr << app_name() << " -- ERROR: Please provide at least one input file " << std::endl;
        print_help();
        return false;
    }
    /// Ensure that the sequence of files is always the same regardless of in
    /// which order they are piped to the parser
    std::sort(in_files.modify().begin(), in_files.modify().end());
    MuonTP::NormalizationDataBase* norm_db = MuonTP::NormalizationDataBase::getDataBase();

    /// Initialize the monte carlo data base
    if (!norm_db->init(in_files())) { return false; }

    if (!TrigPreScaleWeightBranch::set_prescale_strategy(prescale_strategy())) {
        std::cerr << app_name() << " -- ERROR: Invalid prescaling strategy was given. " << std::endl;
        print_help();
        return false;
    }

    if (prescale_strategy() != TriggerPreScale::Strategy::NotApplied) {
        TriggerPreScaleService::getService()->set_triggers(ReadList(prescaled_triggers()));
    }

    if (is_mc()) {
        norm_db->LoadxSections(cross_sec_db());
        if (prescale_strategy() == TriggerPreScale::Strategy::PreScaleMC &&
            !TriggerPreScaleService::getService()->init_service(prescale_calib_file())) {
            print_help();
            return false;
        }

        if (lhe_variation.isUserSet()) {
            std::vector<unsigned int> dsids = norm_db->GetListOfMCSamples();
            for (const unsigned int& dsid : dsids) {
                const unsigned int n_lhe_dsid = norm_db->GetNumLheVariations(dsid);
                if (n_lhe_dsid <= lhe_variation()) {
                    std::cout << app_name() << " -- ERROR: The lhe variations are set to " << lhe_variation() << " while sample "
                              << norm_db->GetSampleName(dsid) << " (" << dsid << ")  has only " << n_lhe_dsid << " variations. "
                              << std::endl;
                    print_help();
                    return false;
                }
            }
        }
    }

    if (!single_thread) HistoFiller::getDefaultFiller()->setNThreads(num_threads());
    ///
    if (!inject_config_file().empty()) {
        std::vector<std::string> sagitta_injections{ReadList(inject_config_file())};
        if (sagitta_injections.empty()) {
            std::cout << app_name() << " -- ERROR: The sagitta injection config file " << inject_config_file() << " is invalid ."
                      << std::endl;
            return false;
        }
        std::cout << "-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+" << std::endl;
        for (const std::string& inj_str : sagitta_injections) {
            MuonTrk::calib_constants calib{inj_str};
            if ((calib.trk_type & MuonTrk::TrackType::TruthTrk) && !is_mc()) continue;
            if (!calib.trk_type) continue;
            std::cout << app_name() << " -- INFO:  Add bias injection " << calib.to_string() << std::endl;
            if (!calib.has_bias() && !calib.has_resolution()) {
                if (nominal_trks.isUserSet())
                    nominal_trks.modify() |= calib.trk_type;
                else
                    nominal_trks(calib.trk_type);
            }
            SagittaBiasAnalyzer::add_sagitta_injection(calib);
        }
        std::cout << "+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-" << std::endl;
    }

    if ((initialize_prw() || lumi_calc_files.isUserSet() || prw_mc_cfg_files.isUserSet()) &&
        (!prw_mc_cfg_files().empty() || !lumi_calc_files().empty())) {
        std::cout << app_name() << " --- Initialize the prw Tool " << std::endl;
        MuonTP::LumiCalculator::getCalculator()->SetPRWlumi(lumi_calc_files());
        MuonTP::LumiCalculator::getCalculator()->SetPRWconfig(prw_mc_cfg_files());
        if (!MuonTP::LumiCalculator::getCalculator()->InitPileUpTool()) return false;
    }
    std::cout << app_name() << " --- Argument parsing succeeded. Will now start the analysis" << std::endl;
    m_args_parsed = true;
    return true;
}
std::shared_ptr<TFile> ArgumentParser::out_file() {
    if (!m_out_file) m_out_file = make_out_file(out_dir() + "/" + out_root_file());
    return m_out_file;
}
bool ArgumentParser::is_mc() const { return !MuonTP::NormalizationDataBase::getDataBase()->isData(); }
bool ArgumentParser::parse_args(int argc, char* argv[]) {
    std::vector<std::string> transformed_args;

    for (int arg = 0; arg < argc; ++arg) { transformed_args.emplace_back(argv[arg]); }
    transformed_args.reserve(argc);
    return parse_args(std::move(transformed_args));
}

std::string ArgumentParser::app_name() const { return m_app_name; }
bool ArgumentParser::parse_args(std::vector<std::string> arg_vec) {
    m_app_name = arg_vec[0];
    arg_vec.erase(arg_vec.begin());
    return eval_args(arg_vec);
}
std::vector<int> ArgumentParser::get_trk_types_ROOT_file() const {
    std::vector<int> trk_types;
    if (!make_root_file()) { return trk_types; }

    trk_types += SagittaBiasAnalyzer::injection_types();
    /// Save per default the nominal value if no injection is given
    if (trk_types.empty() || nominal_trks.isUserSet()) {
        for (auto trk : {
                 MuonTrk::TrackType::Primary,
                 MuonTrk::TrackType::IDTrk,
                 MuonTrk::TrackType::MSTrk,
                 MuonTrk::TrackType::METrk,
                 MuonTrk::TrackType::MSOETrk,
                 MuonTrk::TrackType::CmbTrk,
                 MuonTrk::TrackType::TruthTrk,
             }) {
            if (trk == MuonTrk::TrackType::TruthTrk && is_mc()) continue;
            if (trk & nominal_trks()) trk_types.push_back(trk);
        }
    }
    return trk_types;
}
std::vector<std::string> ArgumentParser::get_job_files() const {
    if (!m_args_parsed) { throw std::runtime_error("The parser has not been initialized yet. Please call parse_args first"); }
    int b = begin.isUserSet() && begin() < in_files().size() ? begin() : 0;
    int e = end.isUserSet() && end() <= in_files().size() ? end() : in_files().size();
    if (b >= e) { throw std::runtime_error(app_name() + " invalid file range to read given"); }
    std::vector<std::string> files_in_sample;
    files_in_sample.insert(files_in_sample.end(), in_files().begin() + b, in_files().begin() + e);

    return files_in_sample;
}
