
#include "NtupleAnalysisUtils/NTAUTopLevelIncludes.h"
#include <iostream>

#include "TFile.h"
#include "TH1D.h"
#include "TH2D.h"

#include <string>
#include <vector> 
using namespace PlotUtils;
int main (int , char** ){

    
    const std::string test_path = "my_testFile.root";
    /// List of histogram names to test
    std::vector<std::string> obj_to_insert {
           "StonjekHappy",
           "StonjekHoliday/Beach",
           "StonjekHoliday/AirPlane/Meal",
           "StonjekWork/Laughter/MyFavoriteJoke",
           "TrainStation/Geneva/ToMunich",
           "Airport/Bavaria/FranzJosefStrauss"
        };
    std::sort(obj_to_insert.begin(), obj_to_insert.end());
        
    const unsigned int N_obj = obj_to_insert.size();
          
    {
        std::unique_ptr<TFile> fout = std::make_unique<TFile>(test_path.c_str(),"RECREATE");
    
        
        for (unsigned int o = 0; o < N_obj; ++o ){
            const std::string& obj = obj_to_insert[o];
            TDirectory* dir = obj.find("/") == std::string::npos ? fout.get() : mkdir(obj.substr(0,obj.rfind("/")+1),fout.get());
            dir->cd();
            std::shared_ptr<TH1> H;
            if (o % 2 == 0) H = std::make_shared<TH1D>(obj.substr(obj.rfind("/")+1).c_str(), "dummy", 3,0, 2);
            else H = std::make_shared<TH2D>(obj.substr(obj.rfind("/")+1).c_str(), "dummy",3,0,2,2,0,2);
            H->Write();
        }
    }
    
    std::vector<std::string> all_histos = getAllObjectsFromFile<TH1>(test_path);
    if (all_histos.size() != N_obj){
        std::cerr<<"Recursive LS test failed as... the following histograms have been considered for test"<<std::endl;
        std::cerr<<obj_to_insert<<std::endl;
        std::cerr<<"While the following has been found "<<std::endl;
        std::cerr<<all_histos<<std::endl;
        return EXIT_FAILURE;
    }
    for (unsigned int o = 0 ; o < all_histos.size(); ++o){
        if (all_histos[o] != obj_to_insert[o]){
           std::cerr<<"The "<<o<<"-th histogram names do not match"<<std::endl;
           std::cerr<<all_histos[o]<<" vs. "<<obj_to_insert[o]<< std::endl;
           return EXIT_FAILURE;
        }
    }
    std::vector<std::string> oneD_histos = getAllObjectsFromFile<TH1D>(test_path);
    const unsigned int exp_1D = ( (N_obj - N_obj%2) /2);
    if (oneD_histos.size() != exp_1D ){
       std::cerr<<" Expected "<<exp_1D<<" TH1D while "<<oneD_histos.size()<<" were found "<<std::endl;
       return EXIT_FAILURE;
    } 
    return EXIT_SUCCESS;
}
