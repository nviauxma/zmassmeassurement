
#include "NtupleAnalysisUtils/NTAUTopLevelIncludes.h"

int main (int argc, char** argv){
    size_t n_evt = 100; 
    if (argc > 1) {
        n_evt = atoi(argv[1]);
    } 
    CutFlow my_CF;
    for (size_t p = 0; p < n_evt; ++p){
        my_CF.Fill("First Cut");
        if (p%10 == 0) my_CF.Fill("Second Cut");
        if (p % 100== 0) my_CF.Fill("Third Cut");
        if (p % 1000== 0) my_CF.Fill("Fourth Cut");
    }
    my_CF.Print();

    return 0;
}
