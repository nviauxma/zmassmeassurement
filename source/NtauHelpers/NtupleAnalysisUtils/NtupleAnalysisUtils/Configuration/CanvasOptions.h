#ifndef NTAU__CANVASOPTS__H
#define NTAU__CANVASOPTS__H 

#include "TColor.h"
#include "TAttLine.h"
#include "TAttFill.h"
#include "TH1.h"
#include "TAttMarker.h"
#include "TCanvas.h"
#include "TPad.h"
#include <string> 
#include <vector> 

#include "NtupleAnalysisUtils/Configuration/UserSetting.h"
#include "NtupleAnalysisUtils/Configuration/AxisConfig.h"
#include "NtupleAnalysisUtils/Plot/Plot.h"

/// Class to package the desired setup of a ROOT Canvas. 
/// 
/// The CanvasOptions is similar in concept to the PlotFormat,
/// but describes an entire canvas rather than a single 
/// entry on it. 
/// 
/// Each option can be set to user-specified values via operator () (value). 
/// This will return the canvas options object by reference, so you can
/// daisy-chain settings a la auto MyOpts = CanvasOptions().logy(true).logx(true).fontSize(72)
///
/// For each axis of the canvas and its pads, a separate AxisConfig is included. 
///
/// Most of the drawing tools in NtupleAnalysisUtils 
///  will make use of this internally, as will the default plotting macros.
/// 
/// For user-made plotting methods, it is up to the users to support (or not) all/some 
/// of the available options, as to their liking. 
/// 
/// As in the PlotFormat, user-specific string-keyed extra options
/// can be added and used in user-code as desired. 

class CanvasOptions{
public:
    CanvasOptions() = default;
    CanvasOptions(const CanvasOptions & other);
    void operator= (const CanvasOptions & other); 
    /// The following settings are initialised to defaults (indicated by the first argument). 

    // Stanard Canvas size
    UserSetting<double,CanvasOptions> CanSizeX{800,this};
    UserSetting<double,CanvasOptions> CanSizeY{600,this};
    
    // For label locations, the following convention applies: 
    // > Positive values: offset w.r.t the bottom or the left of the canvas.
    //   e.g. LabelX = 0.03 here translates to left pad margin + 0.03 
    //        LabelY = 0.05 here translates to bottom pad margin + 0.05
    // > Negative values: offset w.r.t the top or the right of the canvas.
    //   e.g. LabelX = - 0.03 here translates to 1. - right pad margin - 0.03 
    //        LabelY = - 0.05 here translates to top pad margin - 0.05
    // the label step is always subtracted when moving to one label to the next.
    // So, for your text to grow "up", you would assign a negative step.  

    // these are for the ATLAS blahblah label 
    UserSetting<double,CanvasOptions> AtlasLabelX        {0.03, this};
    UserSetting<double,CanvasOptions> AtlasLabelY        {-0.06, this};
    UserSetting<bool,CanvasOptions> DoAtlasLabel        {true, this};
    UserSetting<bool,CanvasOptions> DoLumiSqrtSLabel    {true, this};

    // the following control the lumi/sqrt(s) and any other labels
    UserSetting<double,CanvasOptions> OtherLabelX        {0.03, this};
    UserSetting<double,CanvasOptions> OtherLabelStartY   {-0.12, this};
    UserSetting<double,CanvasOptions> OtherLabelStepY    {0.06, this};

    // and these the legend (still same convention)
    // legendStartX and legendStartY are going to be used
    // to place the plot label of 2D plots on the canvas
    UserSetting<double,CanvasOptions> LegendStartX       {-0.4, this};
    UserSetting<double,CanvasOptions> LegendStartY       {-0.3, this};
    UserSetting<double,CanvasOptions> LegendEndX         {-0.02,this};
    UserSetting<double,CanvasOptions> LegendEndY         {-0.02,this};

    // how much of the total canvas vertical size to give to the ratio pad 
    UserSetting<double,CanvasOptions>  VerticalSplit     {0.4, this};
    /// same for left vs right split
    UserSetting<double,CanvasOptions>  HorizontalSplit     {0.5, this};
    // font size for drawn TLatex and axis labels
    UserSetting<size_t,CanvasOptions> LabelSize          { 18, this};
    UserSetting<size_t,CanvasOptions> TitleSize         { 24, this};

    // these are used to determine which labels to draw 
    UserSetting<std::string,CanvasOptions> LabelStatusTag  {"Internal",    this};
    UserSetting<std::string,CanvasOptions> LabelLumiTag    {"139 fb^{-1}", this};
    UserSetting<std::string,CanvasOptions> LabelSqrtsTag   {"13 TeV",      this};


    // allows to increase the default title offset on the axes by a fraction.
    // for example setting 0.2 here will result in 1.2 times the axis title offset
    // Set the color palette of the canvas in case of 2D plots
    UserSetting<int,CanvasOptions> ColorPalette{kViridis, this};

    /// Margins around the entirety of drawn pads / histos. Calculated w.r.t the host canvas, 
    /// not the possible sub-pads
    
    UserSetting<double,CanvasOptions> TopMargin      {0.02,   this};
    UserSetting<double,CanvasOptions> BottomMargin   {0.12,   this};
    UserSetting<double,CanvasOptions> LeftMargin     {0.12,   this};
    UserSetting<double,CanvasOptions> RightMargin    {0.02,   this};


    UserSetting<AxisConfig,CanvasOptions> XAxis{AxisConfig().TopPadding(0).BottomPadding(0), this};
    UserSetting<AxisConfig,CanvasOptions> YAxis{AxisConfig{}, this};
    UserSetting<AxisConfig,CanvasOptions> ZAxis{AxisConfig{}, this};
    UserSetting<AxisConfig,CanvasOptions> RatioAxis{AxisConfig().Symmetric(true).TopPadding(0.1).BottomPadding(0.1), this};

    // desired file formats for output 
    UserSetting<std::vector<std::string>,CanvasOptions> OutputFormats{{"pdf"},this};
    
    // allows to set the directory to save the histograms to. 
    // Default behaviour: creates a folder $TestArea/../Plots/$(date --iso)/ 
    UserSetting<std::string,CanvasOptions> OutputDir{"",this};


};


#endif
