#############################################################################################################
# THIS SCRIPT:
#   -> uploads sin_theta_rho2 vs m_mu_mu histograms, calculates mean m_mu_mu for each bin-wide 
#      sin_theta_rho2 slice using a gaussian fit, and plots it.
#   NOTE_: the procedure is repeated for different eta ranges (i.e. for muon pairs in barrel, end-cap, etc)
#
# by Carolina de Almeida Rossi
# 14/07/21
############################################################################################################

import ROOT as r
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D

r.gROOT.SetBatch(True)


year = '18'
folder_name = 'main_plots_data_'+year+"_data_MC_ratio2"
root_file_name_MC_qual = "lep_qual_MC/SagittaTemplatesMCquality4.root"
root_file_name_MC = "MC/SagittaTemplatesMC.root"
root_file_name_qual = "q4_"+year+"/SagittaTemplates"+year+"quality4.root"
root_file_name = year+"/SagittaTemplates"+year+".root"
root_file_names = [root_file_name_MC_qual,root_file_name_MC_qual, root_file_name_MC, root_file_name_qual, root_file_name_qual, root_file_name]

#SWITCHES:=========================================================================
sigma_restrictions = True #thresholds: sigma=5 for Z, sigma=0.5 for J/Psi
crystalball = False #else, Gaussian
alpha_version = False #else, classic version of sin2thetarho
both_sin_theta_rho_2_versions = False #else, only the type of fit selected 

#J/Psi switches --------------------------------------------------------------------
J_psi = True #else, Z mass range
tree_m_mu = False #invariant_m from tree, else invariant_m_mu_mass using 4-momentums
massive_m_mu = False #invariant_m_mu_mass for massive muons, else massless muons
all_invariant_m_versions = False  #careful: some drawbacks 
#-----------------------------------------------------------------------------------


#need to store mc somewhere to compare with other ones 




#Parameters for plotting:
params = {
   'font.family': 'Arial',
   'mathtext.default': 'regular',
   'axes.labelsize': 20,
   'font.size': 20,
   'legend.fontsize': 12,
   'figure.titlesize': 25,
   'font.family': 'Arial',
   'xtick.labelsize': 18,
   'ytick.labelsize': 18,
   'figure.figsize': [20,9]
   } 
plt.rcParams.update(params)


looping_3 = 0
lep_quals = ['_MC_mt','_MC_tt','_MC','_mt','_tt', '']
lep_quals_exts = ['_mt','_tt','','_mt','_tt', ''] 

all_lep_quals_mean_m = [] #  LEN 6
while looping_3 < 6: #looping over different datasets 
    lep_qual = lep_quals[looping_3]
    lep_quals_ext = lep_quals_exts[looping_3]
    root_file_name = root_file_names[looping_3]
    looping_2 = 0 #LOPPING OVER J_Psi sin_theta_rho_2 invariant mass methods
    if all_invariant_m_versions:
        tree_m_mu = True #start with tree_m_mu
    if both_sin_theta_rho_2_versions:
        alpha_version = True #start with alpha version
    if J_psi is False:
        all_invariant_m_versions = False
    
    all_masstypes_mean_m = []  #  LEN 3
    mass_method_names = []
    while looping_2 < 3: #looping over different mass regimes
        if J_psi: #TREE / MASSIVE / MASSLESS
            m_lower_threshold = 1
            sigma_threshold = 0.5
            if tree_m_mu:
                m_method = ' TREE' 
                file_name = 'Plot_Jpsi_TREE'
                mass_method_names.append(m_method)
            elif massive_m_mu:
                m_method = ' MASSIVE'
                file_name = 'Plot_Jpsi_MASSIVE'
                mass_method_names.append(m_method)
            else: #MASSLESS:
                m_method = ' MASSLESS'
                file_name = 'Plot_Jpsi_MASSLESS'
                mass_method_names.append(m_method)
            mass_range = '3_4'
            crystal_fit_params = [2,3,0.1,0.001,0.001]
        else:
            m_lower_threshold = 80
            m_method = ''
            sigma_threshold = 5
            file_name = 'Plot'
            mass_range = '61_121'
            crystal_fit_params = [3,90,3,2,4]

        all_theta_methods_mean_m = [] #  LEN 2
        looping = 0 
        theta_method_names = []
        while looping < 2: #LOOPING OVER sin_theta_rho_2 AND sin_theta_rho_2_alpha METHODS   
            # READING HISTOGRAMS =============================================================================================================
            if alpha_version:
                method = r'$\alpha$'
                theta_method_names.append(method)
                my_file = r.TFile.Open("build/Plots_"+root_file_name, "READ")
                my_hist1 = my_file.Get("ID/Sin_theta_rho_2_m_alpha_{}/All_pairs{}/KinSel_Inclusive/MLL_{}/Template_SAGBIAS_0.000000_RADBIAS_0.000000_MAGBIAS_0.000000_SIGMA_0.000000".format(file_name, lep_quals_ext, mass_range))
                my_hist0 = my_file.Get("ID/Sin_theta_rho_2_m_alpha_{}/Barrel_pairs{}/KinSel_Inclusive/MLL_{}/Template_SAGBIAS_0.000000_RADBIAS_0.000000_MAGBIAS_0.000000_SIGMA_0.000000".format(file_name,lep_quals_ext, mass_range))
                my_hist2 = my_file.Get("ID/Sin_theta_rho_2_m_alpha_{}/End_cap_pairs{}/KinSel_Inclusive/MLL_{}/Template_SAGBIAS_0.000000_RADBIAS_0.000000_MAGBIAS_0.000000_SIGMA_0.000000".format(file_name, lep_quals_ext,mass_range))
                my_hist3 = my_file.Get("ID/Sin_theta_rho_2_m_alpha_{}/Barrel_endcap_pairs{}/KinSel_Inclusive/MLL_{}/Template_SAGBIAS_0.000000_RADBIAS_0.000000_MAGBIAS_0.000000_SIGMA_0.000000".format(file_name, lep_quals_ext,mass_range))
            else:
                method = 'std'
                theta_method_names.append(method)
                my_file = r.TFile.Open("build/Plots_"+root_file_name, "READ")
                my_hist1 = my_file.Get("ID/Sin_theta_rho_2_m_{}/All_pairs{}/KinSel_Inclusive/MLL_{}/Template_SAGBIAS_0.000000_RADBIAS_0.000000_MAGBIAS_0.000000_SIGMA_0.000000".format(file_name, lep_quals_ext,mass_range))
                my_hist0 = my_file.Get("ID/Sin_theta_rho_2_m_{}/Barrel_pairs{}/KinSel_Inclusive/MLL_{}/Template_SAGBIAS_0.000000_RADBIAS_0.000000_MAGBIAS_0.000000_SIGMA_0.000000".format(file_name,lep_quals_ext,mass_range))
                my_hist2 = my_file.Get("ID/Sin_theta_rho_2_m_{}/End_cap_pairs{}/KinSel_Inclusive/MLL_{}/Template_SAGBIAS_0.000000_RADBIAS_0.000000_MAGBIAS_0.000000_SIGMA_0.000000".format(file_name,lep_quals_ext, mass_range))
                my_hist3 = my_file.Get("ID/Sin_theta_rho_2_m_{}/Barrel_endcap_pairs{}/KinSel_Inclusive/MLL_{}/Template_SAGBIAS_0.000000_RADBIAS_0.000000_MAGBIAS_0.000000_SIGMA_0.000000".format(file_name,lep_quals_ext, mass_range))
            #list of hists to loop ove in analysis and naming configs:
            hists = [my_hist0, my_hist1, my_hist2, my_hist3]


            #open figure to plot all histograms simultaneously:
            joint_plot = plt.figure('All_Eta_Selections')
            plt.title(r'All $\eta$ selections - ' + year)
            plt.xlabel(r'$sin^{2}(\theta_{\rho})$')
            plt.ylabel(r'<$m_{\mu\mu}$> (GeV)')  
            hists_titles = [r'Barrel / Barrel $\mu$-pairs ',r'All $\mu$-pairs ', r'End-cap / End-cap $\mu$-pairs ', r'Barrel / End-cap $\mu$-pairs '] 
            hists_pdf_titles = ['from_barrel_pairs_hist','from_all_pairs_hist', 'from_endcap_pairs_hist', 'from_endcap_barrel_pairs_hist']
            #============================================================================================================

            data_minus_mc_plot = plt.figure("Data_minus_MC")
            plt.title('Data - MC for every mass case' + year)
            plt.xlabel(r'$sin^{2}(\theta_{\rho})$')
            plt.ylabel(r'<$\Delta m_{\mu\mu}$> (GeV)')  



            #another on for all chi squareds:
            chi2_plot = plt.figure('Chi_squared') 
            plt.xlabel(r'$m_{\mu\mu}$ slice number')
            plt.ylabel(r'$\chi_{2}$') 

            #start m slicing, looping over all available histograms:
            all_eta_cuts_mean_m_vals = [] #  LEN 4
            for my_hist in hists: #looping over different eta cuts for each sinthtarho2 and mass method  

                print("HIST_{}-------------------------------------------------------------------------------------------------".format(hists.index(my_hist)))
                print("\n\nThere are {} sin_sqr_theta bins \n".format(my_hist.GetNbinsX()))

                if crystalball:
                    #(alpha, n, sigma, mean)
                    fit_function = r.TF1( 'Gaussian Fit Function', 'crystalball', 0, 120)
                    fit_function.SetParameters(crystal_fit_params[0],crystal_fit_params[1],crystal_fit_params[2],crystal_fit_params[3],crystal_fit_params[4])
                    index_mean=1
                    dofs = 5

                else: #Gaussian
                    #gauss(0) has params [0,1,2], y = [0]*exp(-0.5*((x-[1])/[2])**2)
                    fit_function = r.TF1( 'Gaussian Fit Function', 'gaus(0)', 0, 120)
                    index_mean=1
                    dofs = 3

                
                print("STARTING SLICING AND FITTING...\n")
                my_hist_clone = my_hist.Clone('my_hist_clone')
                sin_theta2_centers = []
                generic_yslice = my_hist_clone.ProjectionX('generic_yslice')
                mean_m_vals = []
                mean_m_errs = []
                fit_sigmas = []
                fit_sigmas_errs = []
                chi_squareds = []
                for x in range(my_hist.GetNbinsX()): #range is number of x bins
                    x_min = x
                    x_max = x + 1
                    current_xslice = my_hist.ProjectionY('current_slice',x_min,x_max)
                    no_counts = current_xslice.GetCumulative().GetMaximum()
                    
                    if no_counts <1000: 
                        #DEFINE MIN NO OF TOTAL COUNTS TO GET RID OF (ALMOST) EMPTY SLICES
                        print("{} readings in slice {} - Skip it".format(no_counts,x))
                        chi_squareds.append(0.0)
                        continue

                    else:
                        #ELSE, STORE SLICE AND FIT IT
                        print("\n\nSlice {}".format(x))
                        current_xslice.Fit(fit_function)
                        current_xslice.SetLineColor(r.kBlack)
                        mean_m = fit_function.GetParameter(index_mean)
                        if mean_m < m_lower_threshold:
                            print(mean_m,' - dimuon in wrong invariant mass range.')
                            continue                    
                        if sigma_restrictions:
                            current_sigma = fit_function.GetParameter(2)
                            if current_sigma<=sigma_threshold:
                                sin_theta2_centers.append(generic_yslice.GetBinCenter(x))
                                fit_sigmas.append(current_sigma)
                                fit_sigmas_errs.append(fit_function.GetParError(2)) 
                                mean_m_vals.append(mean_m)
                                mean_m_errs.append(fit_function.GetParError(index_mean))
                            else:
                                print('\n ################### SIGMA RESTRICTIONS APPLIED: sigma = {}\n'.format(current_sigma))
                                continue
                        else:
                            sin_theta2_centers.append(generic_yslice.GetBinCenter(x))
                            fit_sigmas.append(fit_function.GetParameter(2))
                            fit_sigmas_errs.append(fit_function.GetParError(2)) 
                            mean_m_vals.append(mean_m)
                            mean_m_errs.append(fit_function.GetParError(index_mean))

                        chi_squareds.append(fit_function.GetChisquare()/(no_counts-dofs))
                        hist_canv = r.TCanvas("Fitted Sliced Histograms", "Slice", 800,600)
                        hist_canv.cd() 
                        current_xslice.Draw("HIST E2")
                        fit_function.Draw('same')
                        hist_canv.SaveAs("analysis_CdAR/Outcome_Plots/{}/Fitted_Histogram_Slices/Slice_{}.pdf".format(hists_pdf_titles[hists.index(my_hist)],x))


                #take sin_theta2_errors to be half of the constant bin width:
                sin_theta2_errors = np.empty(len(mean_m_vals))
                sin_theta2_errors.fill(generic_yslice.GetBinWidth(0)/2)

                if len(mean_m_vals)==0:
                    print("No mean m values found for these sigma restrictions.")
                else:
                    #straight line fit: y=ax+b coeffs=[a,b]
                    coeffs, cov = np.polyfit(sin_theta2_centers, mean_m_vals, 1, cov=True)
                    fitted_m_vals = []
                    for sin in sin_theta2_centers:
                        fitted_m_vals.append(coeffs[0]*sin + coeffs[1])
                    all_eta_cuts_mean_m_vals.append((mean_m_vals,coeffs[0],coeffs[1]))#mean_m, gradient, intercept

                    #plot each case in separate figure:
                    plot = plt.figure('Hist_{}'.format(hists.index(my_hist)))
                    ax = plt.subplot(111)
                    box = ax.get_position()
                    ax.set_position([box.x0, box.y0, box.width*0.95, box.height])
                    plt.xlabel(r'$sin^{2}(\theta_{\rho})$')
                    plt.ylabel(r'<$m_{\mu\mu}$> (GeV)') 
                    plt.errorbar(sin_theta2_centers, mean_m_vals, yerr = mean_m_errs , xerr = sin_theta2_errors, label =  method + m_method + lep_qual)
                    plt.plot(sin_theta2_centers, fitted_m_vals,'--', label = r'$sin^{2}(\theta_{\rho})$ = '+ str(round(coeffs[0],4)) + u'\u00b1' +str(round(np.sqrt(cov[0,0]),4))+ r'<$m_{\mu\mu}$> +' + str(round(coeffs[1],4)) + u'\u00b1' + str(round(np.sqrt(cov[1,1]),4)) + ' ({})'.format(method+m_method+lep_qual))
                    plt.title(hists_titles[hists.index(my_hist)]+ year)
                    plt.legend(bbox_to_anchor=(1.,1.), loc = 'best')
                    plt.plot(sin_theta2_centers, mean_m_vals, 'x')
                    plt.savefig('analysis_CdAR/Outcome_Plots/'+folder_name+'/{}'.format(hists_pdf_titles[hists.index(my_hist)]))
                    #print it:
                    #print('\n\nBARREL-BARREL STRAIGHT LINE FIT: ############################# ')
                    #print("FIT: y = {}x + {}".format(*coeffs))
                    #print("COV MATRIX:", cov)


                    #add plot to joint figure:
                    plt.figure('All_Eta_Selections') 
                    ax = plt.subplot(111)
                    box = ax.get_position()
                    ax.set_position([box.x0, box.y0, box.width*0.99, box.height])
                    plt.errorbar(sin_theta2_centers, mean_m_vals, yerr = mean_m_errs , xerr = sin_theta2_errors, label = hists_titles[hists.index(my_hist)] +    method + m_method + lep_qual)
                    plt.legend(bbox_to_anchor=(1.,1.), loc = 'best')
                    plt.plot(sin_theta2_centers, mean_m_vals, 'x')

                    #plot chi squareds:
                    plt.figure('Chi_squared')
                    ax = plt.subplot(111)
                    box = ax.get_position()
                    ax.set_position([box.x0, box.y0, box.width*0.99, box.height])
                    plt.plot(chi_squareds, label = hists_titles[hists.index(my_hist)] +    method + m_method + lep_qual)
                    plt.legend(bbox_to_anchor=(1.,1.), loc = 'best')
                


                    #Special barrel-barrel analysis:##########################################
                    if hists_pdf_titles[hists.index(my_hist)]=='from_barrel_pairs_hist':
                        #FWHM sin2thetarho dependence in barrel-barrel case: (FULL THETA RANGE)
                        #works as expected: distinction between 
                        #-----close to 1, we have two back-to-back muons, and a Z boson with small dilepton pT, pT(Z) < m(Z)/2
                        #-----in the small sin2thetarho region, we have instead a boosted dilepton system, where the two leptons are not anymore back-to-back, pT(Z) > m(Z)/2
                        fwhm = []
                        fwhm_errs = []
                        for j in range(len(fit_sigmas)):
                            fwhm.append(fit_sigmas[j]*2*np.sqrt(2*np.log(2)))
                            fwhm_errs.append(fit_sigmas_errs[j]*2*np.sqrt(2*np.log(2)))
                        barrel_barrel_FWHM_sin_dependence = plt.figure('barrel_barrel_FWHM_sin_dependence')
                        ax = plt.subplot(111)
                        box = ax.get_position()
                        ax.set_position([box.x0, box.y0, box.width*0.99, box.height])
                        plt.xlabel(r'$sin^{2}(\theta_{\rho})$')
                        plt.ylabel(r'$FWHM_{<m_{\mu\mu}> (GeV)}$') 
                        plt.errorbar(sin_theta2_centers, fwhm, yerr = fwhm_errs , xerr = sin_theta2_errors, label =   method + m_method + lep_qual)
                        plt.title(r'FWHM - $sin^{2}(\theta_{\rho})$ dependence'+ year)
                        plt.legend(bbox_to_anchor=(1.,1.), loc = 'best')
                        plt.plot(sin_theta2_centers, fwhm, 'x')
                        plt.savefig('analysis_CdAR/Outcome_Plots/'+folder_name+'/{}'.format('barrel_barrel_FWHM_sin_dependence'))  
                        
                        #Look at sin2thetarho>0.5 and barrel barel only:
                        #(lengthy cutting procedure, but just making sure no mistakes if no bin at exactly .5 in the future)
                        sin_theta2_centers_cut=[]
                        mean_m_vals_cut=[]
                        mean_m_errs_cut=[]
                        for i in range(len(sin_theta2_centers)):
                            sin = sin_theta2_centers[i]
                            if sin>=0.5:
                                sin_theta2_centers_cut.append(sin)
                                mean_m_vals_cut.append(mean_m_vals[i])
                                mean_m_errs_cut.append(mean_m_errs[i])
                        sin_theta2_errors_cut = sin_theta2_errors[:len(sin_theta2_centers_cut)]


                        #plot it:
                        if J_psi is False:
                            barrel_barrel_plot = plt.figure('barrel_barrel_restricted_range_plot')
                            ax = plt.subplot(111)
                            box = ax.get_position()
                            ax.set_position([box.x0, box.y0, box.width*0.99, box.height])                        
                            plt.xlabel(r'$sin^{2}(\theta_{\rho})$')
                            plt.ylabel(r'<$m_{\mu\mu}$> (GeV)') 
                            plt.errorbar(sin_theta2_centers_cut, mean_m_vals_cut, yerr = mean_m_errs_cut , xerr = sin_theta2_errors_cut, label =   method + m_method + lep_qual)
                            plt.title(r'barrel $\mu$-pairs - Restricted Range '+ year)
                            plt.legend(bbox_to_anchor=(1.,1.), loc = 'best')
                            plt.plot(sin_theta2_centers_cut, mean_m_vals_cut, 'x')
                            plt.savefig('analysis_CdAR/Outcome_Plots/{}/{}'.format(hists_pdf_titles[hists.index(my_hist)],'barrel_barrel_restricted_range_plot'))
            
            all_theta_methods_mean_m.append(all_eta_cuts_mean_m_vals)

            if crystalball:
                plt.figure('All_Eta_Selections')
                plt.savefig('analysis_CdAR/Outcome_Plots/'+folder_name+'/All_Eta_Selections_CrystalBall')
                plt.figure('Chi_squared')
                plt.savefig('analysis_CdAR/Outcome_Plots/'+folder_name+'/Chi_Squareds_CristalBall')
            else:
                plt.figure('All_Eta_Selections')
                plt.savefig('analysis_CdAR/Outcome_Plots/'+folder_name+'/All_Eta_Selections_Gaussian')
                plt.figure('Chi_squared')
                plt.savefig('analysis_CdAR/Outcome_Plots/'+folder_name+'/Chi_Squareds_Gaussian')

            if both_sin_theta_rho_2_versions:
                looping +=1 
                if alpha_version:
                    alpha_version = False
                else:
                    alpha_version = True
            else:
                looping = 2
        
        if all_invariant_m_versions:
            looping_2 +=1
            if alpha_version:
                alpha_version = False
                tree_m_mu = True
            if tree_m_mu:
                tree_m_mu = False
                massive_m_mu = True
            elif massive_m_mu:
                massive_m_mu = False
        else:
            looping_2 = 3
        

        all_masstypes_mean_m.append(all_theta_methods_mean_m)
    all_lep_quals_mean_m.append(all_masstypes_mean_m)
    looping_3 +=1


#SOME CHECKS:
#print(len(all_lep_quals_mean_m)) #4 datasets
#print(len(all_lep_quals_mean_m[0])) #3 masstypes (unless 1 configured)
#print(len(all_lep_quals_mean_m[0][0])) #2 theta methods unless alpha off)
#print(len(all_lep_quals_mean_m[0][0][0][0])) #4 eta cuts
#print(lep_quals,mass_method_names,theta_method_names)
#different markers for each 

joint_slopes_plot = plt.figure("Slopes_Radial_Distortion_Coeff")
plt.axhline(y=0, color = 'black', linewidth =0.75, linestyle = '--')
joint_intercepts_plot = plt.figure("Intercepts_Reference_Masses")
plt.axhline(y=3.096916, color = 'black', linewidth =0.75, linestyle = '--')
plot_barrel_barrel = plt.figure("Data_minus_MC_barrel_barrel")
plot_all = plt.figure("Data_minus_MC_all_pairs")
plot_endcap_endcap = plt.figure("Data_minus_MC_endcap_endcap")
plot_barrel_endcap = plt.figure("Data_minus_MC_barrel_endcap")
all_plot_names = ["Data_minus_MC_barrel_barrel","Data_minus_MC_all_pairs","Data_minus_MC_endcap_endcap","Data_minus_MC_barrel_endcap"]
mass_markers = ['^', 'o', 's']
eta_colours = ['b','r','g','orange']
eta_labels = ['Barrel-Barrel','All pairs', 'Endcap-Endcap', 'Barrel-Endcap']

mc_vals_list = all_lep_quals_mean_m[:3] + all_lep_quals_mean_m[:3]
it_no = 1
lep_qual_lims = []
for lep_qual in range(3,len(all_lep_quals_mean_m)): #dataset
    lep_qual_vals = all_lep_quals_mean_m[lep_qual]
    mc_vals = mc_vals_list[lep_qual]
    for lep_qual_m_method in range(len(lep_qual_vals)): #mass method
        lep_qual_m_method_vals = lep_qual_vals[lep_qual_m_method]
        for lep_qual_m_method_theta in range(len(lep_qual_m_method_vals)): #sin theta method
            lep_qual_m_method_theta_vals = lep_qual_m_method_vals[lep_qual_m_method_theta]
            for lep_qual_m_method_theta_eta in range(len(lep_qual_m_method_theta_vals)): #eta cut 
                #DATA - MC PLOT -------------------------------------------------------------------------
                #mean m vals for given dataset, mass method, sin theta method and eta cut:
                my_mean_m_vals = lep_qual_m_method_theta_vals[lep_qual_m_method_theta_eta][0]
                mc_mean_m_vals = mc_vals[lep_qual_m_method][lep_qual_m_method_theta][lep_qual_m_method_theta_eta][0]
                my_plot = plt.figure(all_plot_names[lep_qual_m_method_theta_eta])
                plt.title(all_plot_names[lep_qual_m_method_theta_eta])
                plt.axhline(y=0, color = 'black', linewidth =0.75, linestyle = '--')
                diffs = []
                for k in range(min(len(my_mean_m_vals),len(mc_mean_m_vals))):
                    my_diff = my_mean_m_vals[k] - mc_mean_m_vals[k]
                    diffs.append(my_diff)
                plt.plot(diffs,'-x', label = mass_method_names[lep_qual_m_method] + '_' + theta_method_names[lep_qual_m_method_theta] + lep_quals[lep_qual])
                                
                #GRADIENTS PLOT -------------------------------------------------------------------------
                #all in same plot, perhaps different markers for different mass methods and colors for different eta cuts
                slope = lep_qual_m_method_theta_vals[lep_qual_m_method_theta_eta][1]
                mc_slope = mc_vals[lep_qual_m_method][lep_qual_m_method_theta][lep_qual_m_method_theta_eta][1] 
                plt.figure("Slopes_Radial_Distortion_Coeff")
                plt.plot(it_no,slope, marker = mass_markers[lep_qual_m_method],color =eta_colours[lep_qual_m_method_theta_eta], ms=10)           
                #INTERCEPTS PLOT -------------------------------------------------------------------------
                intercept = lep_qual_m_method_theta_vals[lep_qual_m_method_theta_eta][2]
                mc_intercept = mc_vals[lep_qual_m_method][lep_qual_m_method_theta][lep_qual_m_method_theta_eta][2]
                plt.figure("Intercepts_Reference_Masses")
                plt.plot(it_no,intercept,marker = mass_markers[lep_qual_m_method],color =eta_colours[lep_qual_m_method_theta_eta], ms=10)

                it_no +=1   
    lep_qual_lims.append(it_no)            

#FINALIZE INTERCEPTS AND GRADIENTS PLOT -------------------------------------------------------------------------
legend_elements = []
legend_labels = []
for colour in eta_colours:
    legend_elements.append(Line2D([0], [0], color=colour, lw=6)) 
    legend_labels.append(eta_labels[eta_colours.index(colour)])
for marker_type in mass_markers:
    legend_elements.append(Line2D([0], [0], color='black', marker = marker_type, ms=12))
    legend_labels.append(mass_method_names[mass_markers.index(marker_type)])

if len(lep_qual_lims) == 4:
    lep_qual_lims_labels = ['MC','MT', 'TT', 'NO LEPQUAL']
elif len(lep_qual_lims) == 3:
    lep_qual_lims_labels = ['MT', 'TT', 'NO LEPQUAL']


plt.figure("Slopes_Radial_Distortion_Coeff")
plt.title("Slopes - give the Radial Distortion Coefficient")
for lep_qual_lim in lep_qual_lims:
    plt.axvline(x = lep_qual_lim - 0.5,color = 'r', linewidth =1, linestyle = ':')
    plt.text(lep_qual_lim-1.5,0.06,lep_qual_lims_labels[lep_qual_lims.index(lep_qual_lim)],rotation=270, color = 'r')
plt.ylabel("Radial Distortion Coefficient")
plt.legend(legend_elements, legend_labels)         
plt.savefig('analysis_CdAR/Outcome_Plots/'+folder_name+'/{}'.format('Slopes_Radial_Distortion_Coeff'))

plt.figure("Intercepts_Reference_Masses")
plt.title("Intercepts - give the Reference J/Psi Invariant Mass")
for lep_qual_lim in lep_qual_lims:
    plt.axvline(x = lep_qual_lim - 0.5,color = 'r', linewidth =1, linestyle = ':')
    plt.text(lep_qual_lim-1.5,3.06,lep_qual_lims_labels[lep_qual_lims.index(lep_qual_lim)],rotation=270, color = 'r')
plt.ylabel("Reference J/Psi Invariant Mass")
plt.legend(legend_elements, legend_labels)
plt.savefig('analysis_CdAR/Outcome_Plots/'+folder_name+'/{}'.format('Intercepts_Reference_Masses'))


#FINALIZE DATA - MC PLOT ----------------------------------------------------------------------------------------
for plot_name in all_plot_names:
    plt.figure(plot_name)
    plt.legend()#plt.legend(bbox_to_anchor=(1.,1.), loc = 'best')
    plt.xlabel(r'$sin^{2}(\theta_{\rho})$')
    plt.ylabel(r'$<$m_{\mu\mu}$>_{DATA}$ - $<$m_{\mu\mu}$>_{MC}$ (GeV)') 
    plt.savefig('analysis_CdAR/Outcome_Plots/'+folder_name+'/{}'.format(plot_name))
#-----------------------------------------------------------------------------------------------------------------
#plt.show()


