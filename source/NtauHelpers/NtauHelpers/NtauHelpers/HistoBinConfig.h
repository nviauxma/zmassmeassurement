#ifndef NTAUHELPERS_HISTOBINCONFIG_H
#define NTAUHELPERS_HISTOBINCONFIG_H

#include <NtupleAnalysisUtils/Configuration/UserSetting.h>
#include <TH1.h>

#include <memory>
/// Helper class to create a histogram with any kind of binning style etc

namespace PlotUtils {

    class HistoBinConfig {
    public:
        /// Standard constructors
        HistoBinConfig() = default;
        HistoBinConfig(const HistoBinConfig&);
        HistoBinConfig& operator=(const HistoBinConfig&);

        HistoBinConfig(HistoBinConfig&&) = default;

        // Define the name of the histogram
        UserSetting<std::string, HistoBinConfig> name{"", this};

        UserSetting<std::string, HistoBinConfig> title{"", this};

        // Define the title of the x-axis
        UserSetting<std::string, HistoBinConfig> x_title{"", this};
        // Define the title of the y-axis
        UserSetting<std::string, HistoBinConfig> y_title{"", this};
        // Define the title of the z-axis
        UserSetting<std::string, HistoBinConfig> z_title{"", this};

        /// Pipe the bin edges of the x-axis
        UserSetting<std::vector<double>, HistoBinConfig> x_bins{{}, this};

        /// If the bins are not explicitly given, then the following three settings have to be set
        /// by the user
        /// Define the lower edge of the x-axis
        UserSetting<double, HistoBinConfig> x_min{-DBL_MAX, this};
        /// Define the upper edge of the x-axis
        UserSetting<double, HistoBinConfig> x_max{DBL_MAX, this};
        /// Define the segmentation
        UserSetting<unsigned int, HistoBinConfig> x_num_bins{0, this};
        /// If the bins are configured via x_min, x_max, the user can define them to be separated in a log scale
        UserSetting<bool, HistoBinConfig> x_log_binning{false, this};

        UserSetting<std::map<int, std::string>, HistoBinConfig> x_bin_labels{{}, this};

        UserSetting<std::vector<double>, HistoBinConfig> y_bins{{}, this};
        /// If the bins are not explicitly given, then the following three settings have to be set
        /// by the user
        /// Define the lower edge of the x-axis
        UserSetting<double, HistoBinConfig> y_min{-DBL_MAX, this};
        /// Define the upper edge of the x-axis
        UserSetting<double, HistoBinConfig> y_max{DBL_MAX, this};
        /// Define the segmentation
        UserSetting<unsigned int, HistoBinConfig> y_num_bins{0, this};
        /// If the bins are configured via x_min, x_max, the user can define them to be separated in a log scale
        UserSetting<bool, HistoBinConfig> y_log_binning{false, this};
        /// Bin labels of the y-axis
        UserSetting<std::map<int, std::string>, HistoBinConfig> y_bin_labels{{}, this};

        UserSetting<std::vector<double>, HistoBinConfig> z_bins{{}, this};
        /// If the bins are not explicitly given, then the following three settings have to be set
        /// by the user
        /// Define the lower edge of the x-axis
        UserSetting<double, HistoBinConfig> z_min{-DBL_MAX, this};
        /// Define the upper edge of the x-axis
        UserSetting<double, HistoBinConfig> z_max{DBL_MAX, this};
        /// Define the segmentation
        UserSetting<unsigned int, HistoBinConfig> z_num_bins{0, this};
        /// If the bins are configured via x_min, x_max, the user can define them to be separated in a log scale
        UserSetting<bool, HistoBinConfig> z_log_binning{false, this};
        /// Bin axis labels of the z-axis
        UserSetting<std::map<int, std::string>, HistoBinConfig> z_bin_labels{{}, this};

        /// Crates a histogram from the given settings
        std::shared_ptr<TH1> make_histo();
    };
}  // namespace PlotUtils
#endif
