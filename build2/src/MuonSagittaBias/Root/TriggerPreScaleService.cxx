#include <MuonSagittaBias/TriggerPreScaleDumper.h>
#include <MuonSagittaBias/TriggerPreScaleService.h>
TriggerPreScaleService* TriggerPreScaleService::m_inst = nullptr;

TriggerPreScaleService* TriggerPreScaleService::getService() {
    if (!m_inst) { m_inst = new TriggerPreScaleService(); }
    return m_inst;
}

TriggerPreScaleService::~TriggerPreScaleService() { m_inst = nullptr; }
TriggerPreScaleService::TriggerPreScaleService() = default;

bool TriggerPreScaleService::init_service(const std::string& calib_file) {
    std::cout << "TriggerPreScaleService -- start initialization. " << std::endl;

    std::shared_ptr<TFile> t_file = PlotUtils::Open(calib_file);
    if (!t_file) return false;
    for (const std::string& trig : m_triggers) { m_items.emplace_back(std::make_shared<TriggerPreScaleItem>(trig)); }
    std::sort(
        m_items.begin(), m_items.end(),
        [](const std::shared_ptr<TriggerPreScaleItem>& a, const std::shared_ptr<TriggerPreScaleItem>& b) { return a->id() < b->id(); });
    for (std::shared_ptr<TriggerPreScaleItem>& item : m_items) {
        std::cout << "  --- Consider " << item->trigger() << " for prescaling the simulation or unprescaling the data." << std::endl;
        item->initialize(t_file);
    }
    std::cout << "TriggerPreScaleService -- initialization done. " << std::endl;
    return true;
}
void TriggerPreScaleService::set_triggers(const std::vector<std::string>& trigs) {
    m_triggers = trigs;
    ClearFromDuplicates(m_triggers);
    std::sort(m_triggers.begin(), m_triggers.end());
    if (m_triggers.empty()) return;
    std::cout << "TriggerPreScaleService::set_triggers() --- The prescales of the following " << m_triggers.size()
              << "triggers are considered for reweighting" << std::endl;
    for (const std::string& trig : m_triggers) { std::cout << "  *** " << trig << std::endl; }
    std::cout << "TriggerPreScaleService::set_triggers() --- Dump done " << std::endl;
}
const std::vector<std::string>& TriggerPreScaleService::trigger_list() const { return m_triggers; }

std::vector<std::shared_ptr<const TriggerPreScaleItem>> TriggerPreScaleService::prescale_calibrators() const {
    std::vector<std::shared_ptr<const TriggerPreScaleItem>> out;
    out.reserve(m_items.size());
    for (const auto& item : m_items) out.emplace_back(item);
    return out;
}

//#############################################
//      RunPreScale
//#############################################
RunPreScale::RunPreScale(int __run, float __prescale) : m_run_number{__run}, m_prescale{__prescale} {}
bool RunPreScale::operator<(const RunPreScale& other) const {
    if (run_number() != other.run_number()) return run_number() < other.run_number();
    return prescale() < other.prescale();
}
float RunPreScale::prescale() const { return m_prescale; }
int RunPreScale::run_number() const { return m_run_number; }
void RunPreScale::add_lumi_block(int first_block, int last_block) {
    /// Check that the added luminosity blocks aren't in range
    if (!in_range(run_number(), first_block) && !in_range(run_number(), last_block)) {
        m_lumi_blocks.emplace_back(first_block, last_block);
    }
    ///
    else {
        /// Check first whether the lower end of the interval overlaps
        std::vector<std::pair<int, int>>::iterator itr = std::find_if(
            m_lumi_blocks.begin(), m_lumi_blocks.end(),
            [first_block](const std::pair<int, int>& range) { return range.first <= first_block && first_block <= range.second; });

        // We found an overlap... Extend the upper edge
        if (itr != m_lumi_blocks.end()) {
            itr->second = std::max(itr->second, last_block);
        } else {
            itr = std::find_if(m_lumi_blocks.begin(), m_lumi_blocks.end(), [last_block](const std::pair<int, int>& range) {
                return range.first <= last_block && last_block <= range.second;
            });
            /// We found an overlap with the upper interval boundary
            if (itr != m_lumi_blocks.end()) { itr->first = std::min(itr->first, first_block); }
        }
    }
}

bool RunPreScale::in_range(int run, int lumi_block) const {
    return run == run_number() &&
           (m_unique || std::find_if(m_lumi_blocks.begin(), m_lumi_blocks.end(), [lumi_block](const std::pair<int, int>& range) {
                            return range.first <= lumi_block && lumi_block <= range.second;
                        }) != m_lumi_blocks.end());
}
void RunPreScale::set_is_unique() {
    m_unique = true;
    m_lumi_blocks.clear();
}

///#########################################################
///                 TriggerPreScaleItem
///#########################################################
TriggerPreScaleItem::TriggerPreScaleItem(const std::string& trig_item) : m_item{trig_item} {}

std::string TriggerPreScaleItem::trigger() const { return m_item; }
void TriggerPreScaleItem::initialize(const std::shared_ptr<TFile>& file) {
    load_prescales(file);
    load_switch_info(file);
}
void TriggerPreScaleItem::load_prescales(const std::shared_ptr<TFile>& file) {
    if (!m_prescales.empty()) return;
    const std::string tree_path = trigger() + "/PreScales";
    TTree* t_tree{nullptr};
    file->GetObject(tree_path.c_str(), t_tree);
    /// The prescale information it not written in case of unity.
    /// No need to throw an error here
    if (!t_tree) return;

    TriggerPreScaleNtuple read_ntuple{t_tree};
    const Long64_t n_items{read_ntuple.getEntries()};
    for (Long64_t e = 0; e < n_items; ++e) {
        read_ntuple.getEntry(e);
        const int run = read_ntuple.run_number();
        const float pre_scale = read_ntuple.pre_scale();
        std::vector<RunPreScale>::iterator itr =
            std::find_if(m_prescales.begin(), m_prescales.end(), [&run, &pre_scale](const RunPreScale& scale) {
                return scale.run_number() == run && std::abs(pre_scale - scale.prescale()) < 0.1;
            });

        if (itr == m_prescales.end()) {
            m_prescales.emplace_back(run, pre_scale);
            m_prescales.back().add_lumi_block(read_ntuple.first_block(), read_ntuple.last_block());
        } else {
            itr->add_lumi_block(read_ntuple.first_block(), read_ntuple.last_block());
        }
    }
    std::sort(m_prescales.begin(), m_prescales.end());
    //// Identify the prescales which are uniquely defined for a run
    for (std::vector<RunPreScale>::iterator itr = m_prescales.begin(); itr != m_prescales.end(); ++itr) {
        const int run = itr->run_number();
        if (std::find_if(itr + 1, m_prescales.end(), [run](const RunPreScale& pre) { return pre.run_number() == run; }) ==
            m_prescales.end()) {
            itr->set_is_unique();
        } else {
            /// Jump to the next run
            itr = std::find_if(itr + 1, m_prescales.end(), [run](const RunPreScale& pre) { return pre.run_number() != run; });
            if (itr == m_prescales.end()) return;
            /// Go one back as the for loop will increment the iterator
            --itr;
        }
    }
}

void TriggerPreScaleItem::load_switch_info(const std::shared_ptr<TFile>& file) {
    const std::string tree_path = trigger() + "/LumiBlocks";
    TTree* t_tree{nullptr};
    file->GetObject(tree_path.c_str(), t_tree);
    if (!t_tree) return;
    TriggerLumiBlockNtuple read_ntuple{t_tree};

    const Long64_t n_items{read_ntuple.getEntries()};
    for (Long64_t e = 0; e < n_items; ++e) {
        read_ntuple.getEntry(e);
        m_trig_switch[read_ntuple.run_number()] |= read_ntuple.trigger_fired();
    }
}

float TriggerPreScaleItem::get_prescale(int run_number, int lumi_block) const {
    /// Perform first the check whether the trigger was active at all in a given
    /// run period
    std::map<unsigned int, bool>::const_iterator itr = m_trig_switch.find(run_number);
    if (itr == m_trig_switch.end() || !itr->second) return FLT_MAX;
    for (const RunPreScale& pre : m_prescales) {
        if (pre.in_range(run_number, lumi_block)) return pre.prescale();
    }
    return 1.;
}
const ObjectID& TriggerPreScaleItem::id() const { return m_obj_id; }
