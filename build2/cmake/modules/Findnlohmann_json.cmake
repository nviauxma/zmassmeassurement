# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#
# Wrapper around the nlohmann_jsonConfig.cmake file packaged with the
# nlohmann_json installation itself. (This file is only needed to set up the
# correct RPM dependency.)
#

# The LCG include(s).
include( LCGFunctions )

# Use the helper macro for the wrapping.
lcg_wrap_find_module( nlohmann_json )

# Set up the RPM dependency.
lcg_need_rpm( jsonmcpp FOUND_NAME nlohmann_json VERSION_NAME JSONMCPP )
