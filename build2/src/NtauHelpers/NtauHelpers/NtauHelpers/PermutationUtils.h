#ifndef NTAUHELPERS_PERMUTATIONUTILS_H
#define NTAUHELPERS_PERMUTATIONUTILS_H

#include <memory>
#include <vector>
///     Helper class to obtain all possibilities drawing m elements from a range of numbers
///         For example draw 4 random elements out of [0, 1, 2, 3, 4, 5, 6]
///

class DrawElementsFromSet {
public:
    /// Constructor
    ///  n : size of the set to draw
    ///  m : number of elements to be drawn
    /// offset: global shift of the
    DrawElementsFromSet(unsigned int n, unsigned int m, unsigned int offset = 0);

    unsigned int n() const;
    unsigned int m() const;
    /// Global offset shifting the values
    unsigned int offset() const;

    /// How many possibilities to draw m elemenets from a set of n. I.e.
    ///   the binomial n over m
    unsigned int size() const;
    /// Returns from the k-th permutation the i-th element
    ///     ele: Index of the permutation
    ///    pos:  Index of the element to be drawn
    unsigned int value_in_tuple(unsigned int ele, unsigned int pos) const;

    /// Get all combinations in one go
    std::vector<unsigned int> draw(unsigned int ele) const;

private:
    unsigned int value_in_tuple(unsigned int ele, unsigned int pos, unsigned int min) const;
    unsigned int get_zeroth(unsigned int ele) const;

    unsigned int m_N;
    unsigned int m_M;
    unsigned int m_offset;
    unsigned int m_size;
    std::shared_ptr<DrawElementsFromSet> m_n_minus1;
    std::vector<unsigned int> m_borders;
};

#endif
