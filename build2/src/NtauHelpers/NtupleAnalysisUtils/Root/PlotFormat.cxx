#include "NtupleAnalysisUtils/Configuration/PlotFormat.h" 

PlotFormat::PlotFormat(){
    m_hasUserSettings=false;
}

PlotFormat PremadePlotFormats::DataMarkers(){
    return PlotFormat().MarkerStyle(kFullDotLarge).Color(kBlack);
}

PlotFormat PremadePlotFormats::Markers1(){
    return PlotFormat().Color(kRed).MarkerStyle(kFullDotLarge);

}
PlotFormat PremadePlotFormats::Markers2(){
    return PlotFormat().Color(kBlue+1).MarkerStyle(kFullSquare);
}
PlotFormat PremadePlotFormats::Markers3(){
    return PlotFormat().Color(kOrange-3).MarkerStyle(kOpenCircle);
}
PlotFormat PremadePlotFormats::Markers4(){
    return PlotFormat().Color(kGreen+2).MarkerStyle(kOpenDiamond);
}
PlotFormat PremadePlotFormats::Markers5(){
    return PlotFormat().Color(kViolet-2).MarkerStyle(kFullTriangleDown).MarkerScale(1.3);
}
PlotFormat PremadePlotFormats::Markers6(){
    return PlotFormat().Color(kBlue-6).MarkerStyle(kOpenTriangleUp).MarkerScale(1.3);
}

PlotFormat PremadePlotFormats::Lines1(){
    return PlotFormat().Color(kBlue+1).LineWidth(2).MarkerStyle(kDot);
}
PlotFormat PremadePlotFormats::Lines2(){
    return PlotFormat().Color(kRed).LineWidth(2).MarkerStyle(kDot);
}
PlotFormat PremadePlotFormats::Lines3(){
    return PlotFormat().Color(kOrange-3).LineWidth(2).MarkerStyle(kDot);
}
PlotFormat PremadePlotFormats::Lines4(){
    return PlotFormat().Color(kGreen-2).LineWidth(2).MarkerStyle(kDot);
}
PlotFormat PremadePlotFormats::Lines5(){
    return PlotFormat().Color(kViolet-2).LineWidth(2).MarkerStyle(kDot);
}
PlotFormat PremadePlotFormats::Lines6(){
    return PlotFormat().Color(kBlue-6).LineWidth(2).MarkerStyle(kDot);
}

PlotFormat::PlotFormat(const PlotFormat & other):
    Color(other.Color, this),
    FillColor(other.FillColor, this),
    MarkerColor(other.MarkerColor, this),
    LineColor(other.LineColor, this),
    FillAlpha(other.FillAlpha, this),
    MarkerAlpha(other.MarkerAlpha, this),
    LineAlpha(other.LineAlpha, this),
    FillStyle(other.FillStyle, this),
    MarkerStyle(other.MarkerStyle, this),
    LineStyle(other.LineStyle, this),
    MarkerSize(other.MarkerSize, this),
    MarkerScale(other.MarkerScale, this),
    LineWidth(other.LineWidth, this),
    ExtraDrawOpts(other.ExtraDrawOpts, this),
    LegendOption(other.LegendOption, this),
    LegendTitle(other.LegendTitle, this),
    CustomInt(other.CustomInt,this), 
    CustomFloat(other.CustomFloat,this), 
    CustomString(other.CustomString,this){
}

    /// ============================================

void PlotFormat::operator=(const PlotFormat & other){
    Color.copyFrom(other.Color,this);
    FillColor.copyFrom(other.FillColor,this);
    MarkerColor.copyFrom(other.MarkerColor,this);
    LineColor.copyFrom(other.LineColor,this);
    FillAlpha.copyFrom(other.FillAlpha,this);
    MarkerAlpha.copyFrom(other.MarkerAlpha,this);
    LineAlpha.copyFrom(other.LineAlpha,this);
    FillStyle.copyFrom(other.FillStyle,this);
    MarkerStyle.copyFrom(other.MarkerStyle,this);
    LineStyle.copyFrom(other.LineStyle,this);
    MarkerSize.copyFrom(other.MarkerSize,this);
    MarkerScale.copyFrom(other.MarkerScale,this);
    LineWidth.copyFrom(other.LineWidth,this);
    ExtraDrawOpts.copyFrom(other.ExtraDrawOpts,this);
    LegendOption.copyFrom(other.LegendOption,this);
    LegendTitle.copyFrom(other.LegendTitle,this);
    CustomInt.copyFrom(other.CustomInt,this); 
    CustomFloat.copyFrom(other.CustomFloat,this);
    CustomString.copyFrom(other.CustomString,this);
}