

template <typename branchType> 
    NtupleBranchAccessByName<branchType>::NtupleBranchAccessByName(const std::string & branchName){
        m_branchName = branchName;
}
template <typename branchType> 
    NtupleBranchAccessByName<branchType>::NtupleBranchAccessByName(const NtupleBranchAccessByName & other){
        m_branchName = other.m_branchName;
        m_branchMap = other.m_branchMap;
}
template <typename branchType> 
    NtupleBranchAccessByName<branchType> & NtupleBranchAccessByName<branchType>::operator= (const NtupleBranchAccessByName & other){
        m_branchName = other.m_branchName;
        m_branchMap = other.m_branchMap;
        return *this;
}
template <typename branchType> 
    NtupleBranchAccessByName<branchType>::NtupleBranchAccessByName(){
        /// set to a dummy name that will hopefully NEVER
        /// exist as a real branch! 
        m_branchName = "UninitializedNtupleBranchAccessByName";   
}
template <typename branchType> 
    void NtupleBranchAccessByName<branchType>::setBranchName(const std::string & name){
        /// set the target branch name
        m_branchName = name; 
        /// and reset our cache
        m_branchMap.clear();
}
template <typename branchType> 
    std::string NtupleBranchAccessByName<branchType>::getBranchName(){
        return m_branchName;
}
template <typename branchType> 
    NtupleBranch<branchType>* NtupleBranchAccessByName<branchType>::getBranch(NtupleBranchMgr & t){
        /// we need to guard the map interaction with a mutex
        std::lock_guard<std::mutex> guard(m_mx_loadSafe);
        /// get the file corresponding to the branch manager
        TTree* theTree = t.getTree();
        /// find the file in our cache 
        auto found = m_branchMap.find(theTree);
        if (found == m_branchMap.end()){
            /// cache miss -> add to cache
            found = addBranch(t);
        }
        /// return the cache entry 
        return found->second; 
}
template <typename branchType> 
    bool NtupleBranchAccessByName<branchType>::getVal (NtupleBranchMgr &t, branchType & ret){   
        /// need another mutex to guard this 
        std::lock_guard<std::mutex> guardGetVar(m_mx_getVarSafe);
        /// try to get the branch 
        auto gotIt = getBranch(t);
        /// failed to get it -> return false
        if (!gotIt) return false; 
        /// otherwise retrieve the content and overwrite the ref 
        ret = gotIt->get();
        /// and return true for success
        return true; 
}
template <typename branchType> 
    typename std::map<TTree*, NtupleBranch<branchType>*>::iterator NtupleBranchAccessByName<branchType>::addBranch(NtupleBranchMgr & t){
        /// try to get the branch 
        auto theBranch = t.getBranch<branchType>(m_branchName);
        /// if we failed to get it, complain and cache the result for this file 
        if (!theBranch){
            std::cerr <<" Failed to find a valid branch for "<<m_branchName<<std::endl;
            return m_branchMap.emplace(t.getTree(), nullptr).first;
        }
        /// otherwise we cache the successfully found branch
        else {
            return m_branchMap.emplace(t.getTree(), theBranch).first;
        }
}