#ifndef NTAU__SlicedPlotProvider__H
#define NTAU__SlicedPlotProvider__H

#include "MuonSagittaBias/PlotProvider.h"

/// Populator to glue together the slices of the plot of interest
class SlicedPostProcessing : public IPlotPopulator<TH1> {
public:
    /// Constructor
    ///  variable_template:  Template of the TH1 histogram in which the variable of interest is binned
    ///  slicing_bin:  Number of the bin in the auxillary slicing histogram
    ///  Populator: Populator filling the variable of interest in sliced
    SlicedPostProcessing(const std::shared_ptr<TH1>& variable_template, std::shared_ptr<Plot<TH1>> populator, int bin_number);
    SlicedPostProcessing(const SlicedPostProcessing& other) = default;

    std::shared_ptr<TH1> populate() override;
    std::shared_ptr<IPlotPopulator<TH1>> clonePopulator() const override;

private:
    std::shared_ptr<TH1> m_template{};
    std::shared_ptr<Plot<TH1>> m_populator{};
    int m_slicing_bin{0};
};

/// Small helper class to cache the
///   Helper class to provide the plots for the different muon tracks
///     The Plotrpovider assembles from the sample, the fill function and the template
///     the final Plot instances including selections for forward and backward asymmetry.

class SlicedPlotProvider : public PlotProvider {
public:
    /// Basic constructor
    ///      sel_name: String naming the selection meant for the saving of the file to disk
    ///      sel_title: Selection title to be plotted on the Canvas. Can contain TLatex formatting
    ///                 characters
    ///     sel_function: Function to evaluate the phase bin selection...
    ///                   The first argument is the refernce to the tree and the second one
    ///                   is the track selection
    SlicedPlotProvider(const Sample<SagittaBiasAnalyzer>& smp,
                       const std::function<int(const TH1*, SagittaBiasAnalyzer&, int)>& sliced_fill_func, const std::shared_ptr<TH1>& histo,
                       const PhaseSpaceCuts& cut_element);

    SlicedPlotProvider(const Sample<SagittaBiasAnalyzer>& smp,
                       const std::function<int(const TH1*, SagittaBiasAnalyzer&, int)>& sliced_fill_func, PlotUtils::HistoBinConfig histo,
                       const PhaseSpaceCuts& cut_element);

    SlicedPlotProvider(const Sample<SagittaBiasAnalyzer>& smp, const PlotFiller& filler, const PhaseSpaceCuts& cut_element);

    std::vector<Plot<TH1>> get_slices(const PlotFiller& provider, int trk_type, std::pair<float, float> mass_window = {61, 121});
    std::vector<Plot<TH1>> get_forward_slices(const PlotFiller& provider, int trk_type, std::pair<float, float> mass_window = {61, 121});
    std::vector<Plot<TH1>> get_backward_slices(const PlotFiller& provider, int trk_type, std::pair<float, float> mass_window = {61, 121});
    std::vector<Plot<TH1>> get_asymmetry_slices(const PlotFiller& provider, int trk_type, std::pair<float, float> mass_window = {61, 121});

    std::vector<PlotContent<TH1>> make_plots(const PlotFiller& provider, const std::vector<int>& trk_types,
                                             const std::vector<RatioEntry>& ratios, std::vector<std::string> labels,
                                             const std::string& plot_name, std::shared_ptr<MultiPagePdfHandle> multiPagePdf = nullptr,
                                             const CanvasOptions& opts = CanvasOptions(), std::pair<float, float> mass_window = {61, 121});

    void add_plot(const PlotFiller& provider, std::vector<int> trk_types, PlotWriter<TH1>& plot_writer,
                  std::pair<float, float> mass_window = {61, 121});

    void add_plot(const PlotFiller& provider, int trk_type, PlotWriter<TH1>& plot_writer, std::pair<float, float> mass_window = {61, 121});

    std::vector<PlotContent<TH1>> make_asymmetry_plots(const PlotFiller& provider, const std::vector<int>& trk_types,
                                                       const std::vector<RatioEntry>& ratios, std::vector<std::string> labels,
                                                       const std::string& plot_name,
                                                       std::shared_ptr<MultiPagePdfHandle> multiPagePdf = nullptr,
                                                       const CanvasOptions& opts = CanvasOptions(),
                                                       std::pair<float, float> mass_window = {61, 121});

    void add_asymmetry_plots(const PlotFiller& provider, int trk_type, PlotWriter<TH1>& plot_writer,
                             std::pair<float, float> mass_window = {61, 121});
    void add_asymmetry_plots(const PlotFiller& provider, std::vector<int> trk_types, PlotWriter<TH1>& plot_writer,
                             std::pair<float, float> mass_window = {61, 121});

    std::vector<std::string> get_boundaries_legend(int bin) const;
    std::string get_boundaries_file(int bin) const;

private:
    std::string legend_bin_boundaries(const TAxis* a, int axis_bin) const;
    std::string file_bin_boundaries(const TAxis* a, int axis_bin) const;

    std::vector<Plot<TH1>> get_slices(const PlotFiller& provider, int trk_type, Selection<SagittaBiasAnalyzer> sel);

    std::vector<PlotContent<TH1>> make_plots(const std::map<int, std::vector<Plot<TH1>>>& plot_map, const std::vector<RatioEntry>& ratios,
                                             std::vector<std::string> labels, const std::string& plot_name,
                                             std::shared_ptr<MultiPagePdfHandle> multiPagePdf, const CanvasOptions& opts);

    std::map<ObjectID, std::shared_ptr<Plot<TH1>>> m_cached_plots;
};

#endif
