#ifndef ___HISTO_FILLER_H
#define ___HISTO_FILLER_H

#include "TGraph.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TH3D.h"
#include "TEfficiency.h"
#include "TTree.h"
#include "TProfile.h"
#include <vector>
#include <map>
#include <future>
#include <algorithm>

#include "NtupleAnalysisUtils/HistoFiller/Sample.h"
#include "NtupleAnalysisUtils/HistoFiller/Selection.h"
#include "NtupleAnalysisUtils/HistoFiller/HistoFillerInterfaces.h"
#include "NtupleAnalysisUtils/HistoFiller/HistoFillerHelpers.h"

/// The HistoFiller is an event loop provider 
/// used when filling Plot objects with the HistoFillerPlotPopulator. 
/// It uses a set of interface classes to be able to fill 
/// arbitrary objects from multiple dataset object types simultaneously. 
/// 
/// The HistoFiller is optimized to minimize run times by sorting 
/// attached plots by their input files and selections to avoid 
/// running on the same file or checking the same selection
/// multiple times 
///
/// Note that in the normal workflow, you as the user would not 
/// have to directly interact with this class. All the work 
/// should happen in the background. 


/// Enum to declare the histogram filling mode - either single- or multi-threaded. 
/// Default is multi-threaded. 
/// If you encounter problems, or if your machine 
/// has a slow disk / few cores, consider switching to single thread mode.
///
/// Note that you can alternatively specify the max. number of threads via the 
/// 'NTAU_NTHREADS' environment variable. 
enum class HistoFillMode {
    singleThread=0,
    multiThread=1
};


/// the HistoFiller itself. 
class HistoFiller{
public:
    /// some useful typedefs
    typedef std::shared_ptr<ISelection> MySelection;
    typedef std::shared_ptr<ISample> MySample;
    typedef std::shared_ptr<IFillable> MyFillable;
    typedef std::shared_ptr<IInputFile> MyInputFile; 
    typedef std::shared_ptr<IHistoFillTask> MyHistoFillTask; 
    typedef std::vector<std::shared_ptr<IHistoFillTask>> MyHistoFillTaskList; 

    /// trivial constructor. Almost nothing to do! 
    /// Default behaviour is multi-threaded
    HistoFiller(HistoFillMode mode = HistoFillMode::multiThread);

    /// return a default instance if needed. 
    /// This is used if the HistoFillerPlotPopulator
    /// is not provided with a user-defined instance.
    /// 
    /// Fine to use in 99% of the cases, manual specification
    /// is really only needed for debugging / benchmarking. 
    static HistoFiller*  getDefaultFiller();

    /// Access to the fill mode.
    /// Use this to configure the default filler 
    void setFillMode(HistoFillMode mode);
    /// and also a getter
    HistoFillMode getFillMode();

    /// Set the maximum number of threads which must not
    /// exceed the hardware capacity -- internally checked
    void setNThreads(unsigned int n);


    /// the following are usually not called by the user, but happen automatically 

    /// attach an input object to be taken into account when running the event loop. 
    bool attach (const IFillable & plot); 

    /// check if something is already connected. 
    bool hasPlot(const IFillable & plot); 

    /// reset methods 
    void ResetFillResults();    /// resets all results from the fill procedure. 
                                /// can be useful if you want to run the loop multiple times 
                                /// for some reason

    void DisconnectAll();       /// this triggers a total reset, making the filler forget 
                                /// about all connected objects

    /// calling this will instantiate all the plots we requested and loop over all the samples, filling the plots. 
    /// Note that this is done automatically on the first call to getOutcome, so unless you want to fill at  
    /// a specific time this does not necessarily need to be called 
    void fillPlots();

    /// retrieve the output. 
    /// Will call fillPlots() not done already. 
    /// Note that this method will return a COPY of the internal plot.
    /// This means that you can safely manipulate the returned object without changing what is stored in the filler, and then get a 
    /// second, "clean" version of the same histo using a second call of getOutcome 
    MyFillable getOutcome (const IFillable & plot);

    /// Debug printout - used for testing. 
    void print(); 
    
protected:

    // helper method for filling in multi-threading. 
    // Will internally process files from our list until we run out.
    // Returns the success / failure of the run.
    bool runProcessingThread( std::shared_ptr<ProcessingQueue> fillState) const;

    /// Determine the number of simultaneous threads to operate
    size_t getNthreads() const; 

    /// These two methods implement the event loop - either a sequential processing of files, or a parallel filling 
    /// of one file per thread in as many threads as our host system allows. 
    void fillPlotsSequentially(MyHistoFillTaskList & tasks);
    void fillPlotsParallel(MyHistoFillTaskList & tasks);

    /// Used to find a plot in our list of connected plots. 
    std::vector<MyFillable>::iterator findPlotInList(const IFillable & plot);

    /// This will rearrange the list of connected plots into a list of tasks,
    /// where each tasks represents everything to be done for one file. 
    MyHistoFillTaskList generateTasks();

    /// Our storage list of connected plots
    std::vector < MyFillable > m_connected_plots; 

    // Did we run the filling already? 
    bool m_ranFill;

    // Mode for filling
    HistoFillMode m_fillMode;

    // Can the histograms attached to this objects all be added? Required for 
    // parallel running
    bool m_canAdd;

    //	Maximum threads to run
    unsigned int m_nThreads;

};

#endif // ___HISTO_FILLER_H
