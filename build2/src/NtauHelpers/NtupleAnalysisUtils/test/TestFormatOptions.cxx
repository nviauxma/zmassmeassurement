#include "NtupleAnalysisUtils/NTAUTopLevelIncludes.h"

/// test the format arguments used in the plotting helper methods 

int main(int, char**){

    /// define a few settings we would like to apply using the draw object format wrapper
    int theAlign = 33;
    Color_t theColor = kBlue+3; 
    Font_t theFont = 72;
    Size_t theSize = 42; 
    Width_t theWidth = 5; 
    Style_t theStyle = kFullDotLarge;
    
    /// a few extra settings to test overwriting existing ones 
    Size_t theSizeAlt = 32; 
    Font_t theFontAlt = 53;

    /// define an empty format to test the default behaviour
    DrawObjectFormat of_basic{};
    /// define a user-configured format 
    DrawObjectFormat of_userSet = DrawObjectFormat().Align(theAlign).Color(theColor).Font(theFont).NDC(false).Size(theSize).Width(theWidth).Style(theStyle); 
    /// and copy a format, overwriting certain settings. 
    DrawObjectFormat of_copy = of_userSet;
    of_copy.Size(theSizeAlt).Font(theFontAlt); 

    /// Step 1: Check the "user set" flags.
    /// Should be false for the default format where nothing was user-specified
    if (of_basic.Align.isUserSet() 
        || of_basic.Color.isUserSet() 
        || of_basic.Font.isUserSet() 
        || of_basic.NDC.isUserSet() 
        || of_basic.Size.isUserSet() 
        || of_basic.Style.isUserSet() 
        || of_basic.Width.isUserSet()
    ){
        return 1; 
    }
    std::cout << "user set flags in basic format: OK"<<std::endl;

    /// should be true for the user-configured format where everything was user-specified. 
    if (!of_userSet.Align.isUserSet() 
        || !of_userSet.Color.isUserSet() 
        || !of_userSet.Font.isUserSet() 
        || !of_userSet.NDC.isUserSet() 
        || !of_userSet.Size.isUserSet() 
        || !of_userSet.Style.isUserSet() 
        || !of_userSet.Width.isUserSet()
    ){
        return 1; 
    }
    std::cout << "user set flags in user-set format: OK"<<std::endl;

    /// flags should be preserved in a copy
    if (!of_copy.Align.isUserSet() 
        || !of_copy.Color.isUserSet() 
        || !of_copy.Font.isUserSet() 
        || !of_copy.NDC.isUserSet() 
        || !of_copy.Size.isUserSet() 
        || !of_copy.Style.isUserSet() 
        || !of_copy.Width.isUserSet()
    ){
        return 1; 
    }
    std::cout << "user set flags in copied format: OK"<<std::endl;


    /// Step 2: Test the actual values stored in the flags 
    if (of_userSet.Align() != theAlign 
        || of_userSet.Font() != theFont 
        || of_userSet.Color() != theColor 
        || of_userSet.NDC() != false 
        || of_userSet.Size() != theSize 
        || of_userSet.Style() != theStyle 
        || of_userSet.Width() != theWidth) return 1;  
    std::cout << "content in user-set format: OK"<<std::endl;

    /// the copied format should also have the intended content
    if (of_copy.Align() != theAlign 
        || of_copy.Font() != theFontAlt 
        || of_copy.Color() != theColor 
        || of_copy.NDC() != false 
        || of_copy.Size() != theSizeAlt 
        || of_copy.Style() != theStyle 
        || of_copy.Width() != theWidth) return 1;  
    std::cout << "content in copied, updated format: OK"<<std::endl;

    return 0; 

}