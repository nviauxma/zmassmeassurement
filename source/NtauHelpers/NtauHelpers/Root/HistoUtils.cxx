#include <NtauHelpers/HistoUtils.h>
#include <NtauHelpers/Utils.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TH3D.h>
using namespace PlotUtils;

int GetMinimumBin(const std::shared_ptr<TH1> &histo, double min_value) { return GetMinimumBin(histo.get(), min_value); }
int GetMinimumBin(const TH1 *histo, double min_value) {
    double min = DBL_MAX;
    int min_bin = -1;
    for (int n = getNbins(histo); n > 0; --n) {
        if (histo->GetBinContent(n) < min && histo->GetBinContent(n) >= min_value) {
            min = histo->GetBinContent(n);
            min_bin = n;
        }
    }
    return min_bin;
}
int GetMaximumBin(const std::shared_ptr<TH1> &histo, double max_value) { return GetMaximumBin(histo.get(), max_value); }
int GetMaximumBin(const TH1 *histo, double max_value) {
    double max = -DBL_MAX;
    int max_bin = -1;
    for (int n = getNbins(histo); n > 0; --n) {
        if (histo->GetBinContent(n) > max && histo->GetBinContent(n) <= max_value) {
            max = histo->GetBinContent(n);
            max_bin = n;
        }
    }
    return max_bin;
}

std::shared_ptr<TH1> load_histo(const std::string &histo_name, std::shared_ptr<TFile> file) { return load_histo(histo_name, file.get()); }
std::shared_ptr<TH1> load_histo(const std::string &histo_name, TFile *file) {
    TH1 *h = nullptr;
    file->GetObject(histo_name.c_str(), h);
    if (h) h->SetDirectory(nullptr);
    return std::shared_ptr<TH1>{h};
}

std::shared_ptr<TH1> clone(const std::shared_ptr<TH1> &histo, bool reset) { return clone(histo.get(), reset); }
std::shared_ptr<TH1> clone(const TH1 *histo, bool reset) {
    if (histo == nullptr) return std::shared_ptr<TH1>();
    std::shared_ptr<TH1> cloned(dynamic_cast<TH1 *>(histo->Clone(RandomString(85).c_str())));
    if (reset) cloned->Reset();
    cloned->SetDirectory(0);
    return cloned;
}

double pull_outside(const TAxis *A, const double v) {
    if (v < A->GetBinLowEdge(1))
        return A->GetBinCenter(1);
    else if (v >= A->GetBinUpEdge(A->GetNbins()))
        return A->GetBinCenter(A->GetNbins());

    return v;
}
void pull_outside(TH1 *H, const double x) { H->Fill(pull_outside(H->GetXaxis(), x)); }
void pull_outside(TH2 *H, const double x, const double y) { H->Fill(pull_outside(H->GetXaxis(), x), pull_outside(H->GetYaxis(), y)); }
void pull_outside(TH3 *H, const double x, const double y, const double z) {
    H->Fill(pull_outside(H->GetXaxis(), x), pull_outside(H->GetYaxis(), y), pull_outside(H->GetYaxis(), z));
}

std::vector<double> ExtractBinning(const TAxis *Axis) {
    std::vector<double> Binning;
    if (Axis) {
        int N = Axis->GetNbins();
        Binning.reserve(N + 1);
        for (int i = 1; i <= N; ++i) { Binning.push_back(Axis->GetBinLowEdge(i)); }
        Binning.push_back(Axis->GetBinUpEdge(N));
    }
    ClearFromDuplicates(Binning);
    std::sort(Binning.begin(), Binning.end());
    return Binning;
}

double GetBinNormalization(const std::shared_ptr<TH1> &H1, int Bin) { return GetBinNormalization(H1.get(), Bin); }
double GetBinNormalization(const TH1 *H1, int Bin) {
    int x(-1), y(-1), z(-1);
    H1->GetBinXYZ(Bin, x, y, z);
    double norm = H1->GetXaxis()->GetBinWidth(1) / H1->GetXaxis()->GetBinWidth(x);

    if (H1->GetDimension() >= 2) { norm *= H1->GetYaxis()->GetBinWidth(1) / H1->GetYaxis()->GetBinWidth(y); }
    if (H1->GetDimension() == 3) { norm *= H1->GetZaxis()->GetBinWidth(1) / H1->GetZaxis()->GetBinWidth(z); }
    return norm;
}

std::pair<double, double> IntegrateAndError(const std::shared_ptr<TH1> &Histo, unsigned int xStart, int xEnd, unsigned int yStart, int yEnd,
                                            unsigned int zStart, int zEnd) {
    return IntegrateAndError(Histo.get(), xStart, xEnd, yStart, yEnd, zStart, zEnd);
}
std::pair<double, double> IntegrateAndError(const TH1 *Histo, unsigned int xStart, int xEnd, unsigned int yStart, int yEnd,
                                            unsigned int zStart, int zEnd) {
    if (!Histo) {
        Error("IntegrateAndError()", "Empty histogram given.");
        return std::pair<double, double>(std::nan("1"), std::nan("1"));
    }
    double integral(0), error(0);
    if (xEnd == -1) xEnd = Histo->GetNbinsX() + 2;
    if (yEnd == -1 && Histo->GetDimension() >= 2) yEnd = Histo->GetNbinsY() + 2;
    if (zEnd == -1 && Histo->GetDimension() == 3) zEnd = Histo->GetNbinsZ() + 2;

    for (int x = xStart; x < xEnd; ++x) {
        double xW = Histo->GetXaxis()->GetBinWidth(x) / Histo->GetXaxis()->GetBinWidth(1);
        if (Histo->GetDimension() == 1) {
            integral += Histo->GetBinContent(x) * xW;
            error += std::pow(Histo->GetBinError(x) * xW, 2);
        } else {
            for (int y = yStart; y < yEnd; ++y) {
                double yW = xW * (Histo->GetYaxis()->GetBinWidth(y) / Histo->GetYaxis()->GetBinWidth(1));
                if (Histo->GetDimension() == 2) {
                    integral += Histo->GetBinContent(x, y) * yW;
                    error += std::pow(Histo->GetBinError(x, y) * yW, 2);
                } else {
                    for (int z = zStart; z < zEnd; ++z) {
                        double zW = yW * (Histo->GetZaxis()->GetBinWidth(z) / Histo->GetZaxis()->GetBinWidth(1));
                        integral += Histo->GetBinContent(x, y, z) * zW;
                        error += std::pow(Histo->GetBinError(x, y, z) * zW, 2);
                    }
                }
            }
        }
    }
    return std::pair<double, double>(integral, std::sqrt(error));
}

std::shared_ptr<TH1> ProjectInto1D(const std::shared_ptr<TH1> &H3, unsigned int project_along, int bin1_start, int bin1_end, int bin2_start,
                                   int bin2_end) {
    return ProjectInto1D(H3.get(), project_along, bin1_start, bin1_end, bin2_start, bin2_end);
}
std::shared_ptr<TH1> ProjectInto1D(const TH1 *H3, unsigned int project_along, int bin1_start, int bin1_end, int bin2_start, int bin2_end) {
    const TAxis *axis_to_project = nullptr;

    if (project_along >= 3 || H3->GetDimension() < 2 || (project_along == 2 && H3->GetDimension() == 2)) {
        Error("ProjectInto1D()", Form("You're a very funny guy. Where is my %d dimension>?", project_along + 1));
        return std::shared_ptr<TH1>();
    } else if (project_along == 2) {
        axis_to_project = H3->GetZaxis();
    } else if (project_along == 1) {
        axis_to_project = H3->GetYaxis();
    } else {
        axis_to_project = H3->GetXaxis();
    }
    std::vector<double> bins_x = ExtractBinning(axis_to_project);

    std::shared_ptr<TH1> new_histo = std::make_shared<TH1D>(RandomString(25).c_str(), "Projection", bins_x.size() - 1, bins_x.data());
    for (int to = 0; to <= axis_to_project->GetNbins() + 1; ++to) {
        std::pair<double, double> integral;
        if (project_along == 2) {
            integral = IntegrateAndError(H3, bin1_start, bin1_end, bin2_start, bin2_end, to, to + 1);
        } else if (project_along == 1) {
            integral = IntegrateAndError(H3, bin1_start, bin1_end, to, to + 1, bin2_start, bin2_end);
        } else if (project_along == 0) {
            integral = IntegrateAndError(H3, to, to + 1, bin1_start, bin1_end, bin2_start, bin2_end);
        }
        new_histo->SetBinContent(to, integral.first * GetBinNormalization(new_histo, to));
        new_histo->SetBinError(to, integral.second * GetBinNormalization(new_histo, to));
    }
    new_histo->GetXaxis()->SetTitle(axis_to_project->GetTitle());
    return new_histo;
}

std::shared_ptr<TH1> MakeTH1(std::vector<double> BinEdgesX, std::vector<double> BinEdgesY, std::vector<double> BinEdgesZ) {
    ClearFromDuplicates(BinEdgesX);
    ClearFromDuplicates(BinEdgesY);
    ClearFromDuplicates(BinEdgesZ);
    std::sort(BinEdgesX.begin(), BinEdgesX.end());
    std::sort(BinEdgesY.begin(), BinEdgesY.end());
    std::sort(BinEdgesZ.begin(), BinEdgesZ.end());
    std::shared_ptr<TH1> H;
    if (!BinEdgesX.empty() && !BinEdgesY.empty() && !BinEdgesZ.empty()) {
        bool x_equi_distant = isEquiDistant(BinEdgesX);
        bool y_equi_distant = isEquiDistant(BinEdgesY);
        bool z_equi_distant = isEquiDistant(BinEdgesZ);
        if (x_equi_distant && y_equi_distant && z_equi_distant) {
            H = std::make_shared<TH3D>(RandomString(45).c_str(), "Variable binning", calculateNbins(BinEdgesX), BinEdgesX[0],
                                       BinEdgesX[BinEdgesX.size() - 1], calculateNbins(BinEdgesY), BinEdgesY[0],
                                       BinEdgesY[BinEdgesY.size() - 1], calculateNbins(BinEdgesZ), BinEdgesZ[0],
                                       BinEdgesZ[BinEdgesZ.size() - 1]);
        } else {
            H = std::make_shared<TH3D>(RandomString(45).c_str(), "Variable binning", BinEdgesX.size() - 1, BinEdgesX.data(),
                                       BinEdgesY.size() - 1, BinEdgesY.data(), BinEdgesZ.size() - 1, BinEdgesZ.data());
        }
    } else if (!BinEdgesX.empty() && !BinEdgesY.empty()) {
        bool x_equi_distant = isEquiDistant(BinEdgesX);
        bool y_equi_distant = isEquiDistant(BinEdgesY);
        if (x_equi_distant && y_equi_distant) {
            H = std::make_shared<TH2D>(RandomString(45).c_str(), "Variable binning", calculateNbins(BinEdgesX), BinEdgesX[0],
                                       BinEdgesX[BinEdgesX.size() - 1], calculateNbins(BinEdgesY), BinEdgesY[0],
                                       BinEdgesY[BinEdgesY.size() - 1]);
        } else if (x_equi_distant) {
            H = std::make_shared<TH2D>(RandomString(45).c_str(), "Variable binning", calculateNbins(BinEdgesX), BinEdgesX[0],
                                       BinEdgesX[BinEdgesX.size() - 1], BinEdgesY.size() - 1, BinEdgesY.data());
        } else if (y_equi_distant) {
            H = std::make_shared<TH2D>(RandomString(45).c_str(), "Variable binning", BinEdgesX.size() - 1, BinEdgesX.data(),
                                       calculateNbins(BinEdgesY), BinEdgesY[0], BinEdgesY[BinEdgesY.size() - 1]);
        } else {
            H = std::make_shared<TH2D>(RandomString(45).c_str(), "Variable binning", BinEdgesX.size() - 1, BinEdgesX.data(),
                                       BinEdgesY.size() - 1, BinEdgesY.data());
        }
    } else if (!BinEdgesX.empty()) {
        if (!isEquiDistant(BinEdgesX)) {
            H = std::make_shared<TH1D>(RandomString(45).c_str(), "Variable binning", BinEdgesX.size() - 1, BinEdgesX.data());
        } else {
            H = std::make_shared<TH1D>(RandomString(45).c_str(), "Fixed binning", calculateNbins(BinEdgesX), BinEdgesX[0],
                                       BinEdgesX[BinEdgesX.size() - 1]);
        }
    }
    if (H) H->SetDirectory(nullptr);
    return H;
}

std::shared_ptr<TH1> combineTemplates(const std::shared_ptr<TH1> &H1, const std::shared_ptr<TH1> &H2) {
    return combineTemplates(H1.get(), H2.get());
}
std::shared_ptr<TH1> combineTemplates(const TH1 *H1, const TH1 *H2) {
    if (!H1 || !H2 || H1->GetDimension() + H2->GetDimension() > 3) {
        Error("combineTemplates()", "Invalid input histograms were given...");
        return nullptr;
    }
    std::vector<double> x = ExtractBinning(H1->GetXaxis());
    std::vector<double> y = ExtractBinning(H1->GetDimension() == 2 ? H1->GetYaxis() : H2->GetXaxis());
    std::vector<double> z = H1->GetDimension() == 2   ? ExtractBinning(H2->GetXaxis())
                            : H2->GetDimension() == 2 ? ExtractBinning(H2->GetYaxis())
                                                      : std::vector<double>();
    std::shared_ptr<TH1> new_temp = MakeTH1(x, y, z);
    if (z.empty()) {
        new_temp->GetXaxis()->SetTitle(H1->GetXaxis()->GetTitle());
        new_temp->GetYaxis()->SetTitle(H1->GetYaxis()->GetTitle());
    } else {
        new_temp->GetXaxis()->SetTitle(H1->GetXaxis()->GetTitle());
        new_temp->GetYaxis()->SetTitle(H1->GetDimension() == 2 ? H1->GetYaxis()->GetTitle() : H2->GetXaxis()->GetTitle());
        new_temp->GetZaxis()->SetTitle(H1->GetDimension() == 2 ? H2->GetXaxis()->GetTitle() : H2->GetYaxis()->GetTitle());
    }
    return new_temp;
}

std::vector<int> visibileBinsVector(const std::shared_ptr<TH1> &H) { return visibileBinsVector(H.get()); }
std::vector<int> visibileBinsVector(const TH1 *H) {
    std::vector<int> vis_bins;
    int n_bins = PlotUtils::getNbins(H);
    vis_bins.reserve(n_bins);
    for (int b = 0; b <= n_bins; ++b) {
        if (!PlotUtils::isOverflowBin(H, b)) vis_bins.push_back(b);
    }
    return vis_bins;
}
unsigned int getNbinsVis(const std::shared_ptr<TH1> &H) { return getNbinsVis(H.get()); }
unsigned int getNbinsVis(const TH1 *H) {
    if (!H) return 0;
    unsigned int n_bins = H->GetNbinsX();
    if (H->GetDimension() >= 2) n_bins *= H->GetNbinsY();
    if (H->GetDimension() == 3) n_bins *= H->GetNbinsZ();
    return n_bins;
}

bool same_binning(const std::shared_ptr<TH1> &H1, const std::shared_ptr<TH1> &H2) { return same_binning(H1.get(), H2.get()); }

bool same_binning(const TH1 *H1, const TH1 *H2) {
    if (!H1 || !H2 || H1->GetDimension() != H2->GetDimension()) return false;
    if (!same_binning(H1->GetXaxis(), H2->GetXaxis())) return false;
    if (H1->GetDimension() >= 2 && !same_binning(H1->GetYaxis(), H2->GetYaxis())) return false;
    if (H1->GetDimension() == 3 && !same_binning(H1->GetZaxis(), H2->GetZaxis())) return false;
    return true;
}
bool same_binning(const TAxis *a1, const TAxis *a2) {
    if (!a1 || !a2 || a1->GetNbins() != a2->GetNbins()) return false;
    const std::vector<double> binning_a = ExtractBinning(a1);
    const std::vector<double> binning_b = ExtractBinning(a2);
    return binning_a == binning_b;
}
void set_bin_labels(TAxis *a, const std::map<int, std::string> &bin_labels) {
    if (!a) return;
    for (const std::pair<int, std::string> &label : bin_labels) {
        if (isOverflowBin(a, label.first)) continue;
        a->SetBinLabel(label.first, label.second.c_str());
    }
}

void copy_styling(const std::shared_ptr<TH1> &from, std::shared_ptr<TH1> to) { copy_styling(from.get(), to.get()); }
void copy_styling(const TH1 *from, TH1 *to) {
    to->GetXaxis()->SetTitle(from->GetXaxis()->GetTitle());
    to->GetYaxis()->SetTitle(from->GetYaxis()->GetTitle());
    to->GetZaxis()->SetTitle(from->GetZaxis()->GetTitle());
    to->SetTitle(from->GetTitle());
}
