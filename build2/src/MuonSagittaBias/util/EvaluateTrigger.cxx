#include <MuonSagittaBias/ArgumentParser.h>
#include <MuonSagittaBias/TriggerAnalyzer.h>
#include <MuonSagittaBias/TriggerPlotPruner.h>
#include <MuonSagittaBias/Utils.h>
#include <NtauHelpers/HistoBinConfig.h>
#include <NtauHelpers/OutFileWriter.h>
#include <NtauHelpers/SelectionWithTitle.h>

void plot_triggers(const Sample<TriggerAnalyzer>& sample_to_process, const CanvasOptions canvas_opts,
                   std::shared_ptr<MultiPagePdfHandle> multi_page, std::vector<PlotContent<TH1>>& trig_plots, PlotWriter<TH1>& out_file,
                   const SelectionWithTitle<TriggerAnalyzer>& base_cut, const std::string& sel_string) {
    const std::vector<InputFile<TriggerAnalyzer>> files = sample_to_process.getInputFiles();
    if (files.empty()) return;
    std::shared_ptr<TFile> t_file = PlotUtils::Open(files[0].getFileName());
    TTree* t_tree{nullptr};
    t_file->GetObject(files[0].getTreeName().c_str(), t_tree);

    PlotUtils::HistoBinConfig histo_template = TriggerAccess::extract_binning(t_tree);

    CanvasOptions canvas_opts_mass{canvas_opts};
    canvas_opts_mass.CanSizeX(800).CanSizeY(600);

    CanvasOptions canvas_opts_prescale{canvas_opts};
    canvas_opts_prescale.YAxis.modify().Log(true);

    /// Reset for now the x_bin_labels as it's too risky if TH1 add with labelled bins comes into play
    histo_template.x_bin_labels.modify().clear();
    /// Create the fill instruction to
    PlotFillInstructionWithRef<TH1, TriggerAnalyzer> trigger_fill{
        [](TH1* h, TriggerAnalyzer& t) {
            h->SetBinContent(1, t.event_weight() + h->GetBinContent(1));
            h->SetBinError(1, std::hypot(t.event_weight(), h->GetBinError(1)));
            for (size_t trig = 0; trig < t.triggers.size(); ++trig) {
                TriggerAccess& acc = t.triggers[trig];
                const int bin = trig + 2;
                if (acc.pass_decision()) {
                    h->SetBinContent(bin, t.event_weight() + h->GetBinContent(bin));
                    h->SetBinError(bin, std::hypot(t.event_weight(), h->GetBinError(bin)));
                }
            }
        },
        histo_template.make_histo()};

    PlotFillInstructionWithRef<TH1, TriggerAnalyzer> prescale_fill{
        [](TH1* h, TriggerAnalyzer& t) {
            for (size_t trig = 0; trig < t.triggers.size(); ++trig) {
                TriggerAccess& acc = t.triggers[trig];
                const int bin = trig + 2;
                if (acc.pass_decision()) {
                    h->SetBinContent(bin, t.event_weight() * acc.prescale() + h->GetBinContent(bin));
                    h->SetBinError(bin, std::hypot(t.event_weight(), h->GetBinError(bin)));
                }
            }
        },
        histo_template.make_histo()};

    PlotFillInstructionWithRef<TH1, TriggerAnalyzer> matching_fill{
        [](TH1* h, TriggerAnalyzer& t) {
            bool fill{false};
            for (size_t trig = 0; trig < t.triggers.size(); ++trig) {
                TriggerAccess& acc = t.triggers[trig];
                const int bin = trig + 2;
                if (acc.pass_matching_dR()) {
                    h->SetBinContent(bin, t.event_weight() + h->GetBinContent(bin));
                    h->SetBinError(bin, std::hypot(t.event_weight(), h->GetBinError(bin)));
                    fill = true;
                }
            }
            if (fill) {
                h->SetBinContent(1, t.event_weight() + h->GetBinContent(1));
                h->SetBinError(1, std::hypot(t.event_weight(), h->GetBinError(1)));
            }
        },
        histo_template.make_histo()};
    ///
    histo_template = TriggerAccess::extract_binning(t_tree);

    /// Fill in addition the boson masses to check whether a particular trigger
    /// accepts more background than another
    PlotFillInstructionWithRef<TH1, TriggerAnalyzer> z_mass_fill{
        [](TH1* h, TriggerAnalyzer& t) { h->Fill(t.invariant_m(), t.event_weight()); },
        PlotUtils::HistoBinConfig().x_num_bins(60).x_min(61).x_max(121).x_title("m_{#mu#mu} [GeV]").make_histo()

    };

    PlotFillInstructionWithRef<TH1, TriggerAnalyzer> jpsi_mass_fill{
        [](TH1* h, TriggerAnalyzer& t) { h->Fill(t.invariant_m(), t.event_weight()); },
        PlotUtils::HistoBinConfig().x_num_bins(64).x_min(2.7).x_max(3.5).x_title("m_{#mu#mu} [GeV]").make_histo()

    };

    Selection<TriggerAnalyzer> jpsi_mass_cut{[](TriggerAnalyzer& t) {
        const float m = t.invariant_m();
        return m > 2.7 && m < 3.5;
    }};

    Selection<TriggerAnalyzer> z_mass_cut{[](TriggerAnalyzer& t) {
        const float m = t.invariant_m();
        return m > 61 && m < 121;
    }};

    for (auto year : {
             TriggerAnalyzer::data_period::data15,
             TriggerAnalyzer::data_period::data16,
             TriggerAnalyzer::data_period::data17,
             TriggerAnalyzer::data_period::data18,

         }) {
        Selection<TriggerAnalyzer> run_cut{[year](TriggerAnalyzer& t) -> bool { return (year & t.period_year()); }};
        Selection<TriggerAnalyzer> comb_cut_z = run_cut && base_cut && z_mass_cut;
        Selection<TriggerAnalyzer> comb_cut_jpsi = run_cut && base_cut && jpsi_mass_cut;
        Plot<TH1> z_mass{RunHistoFiller(sample_to_process, comb_cut_z, z_mass_fill)};
        Plot<TH1> jpsi_mass{RunHistoFiller(sample_to_process, comb_cut_jpsi, jpsi_mass_fill)};

        out_file.add_plot(z_mass, "ZResonance/" + sel_string + "/" + TriggerAnalyzer::period_to_string(year) + "/Mass");
        out_file.add_plot(jpsi_mass, "JPsi/" + sel_string + "/" + TriggerAnalyzer::period_to_string(year) + "/Mass");

        Plot<TH1> trig_pl_z{RunHistoFiller(sample_to_process, comb_cut_z, trigger_fill)};
        Plot<TH1> matc_pl_z{RunHistoFiller(sample_to_process, comb_cut_z, matching_fill)};
        Plot<TH1> pres_pl_z{RunHistoFiller(sample_to_process, comb_cut_z, prescale_fill)};

        Plot<TH1> trig_pl_jpsi{RunHistoFiller(sample_to_process, comb_cut_jpsi, trigger_fill)};
        Plot<TH1> matc_pl_jpsi{RunHistoFiller(sample_to_process, comb_cut_jpsi, matching_fill)};
        Plot<TH1> pres_pl_jpsi{RunHistoFiller(sample_to_process, comb_cut_jpsi, prescale_fill)};

        out_file.add_plot(trig_pl_z, "ZResonance/" + sel_string + "/" + TriggerAnalyzer::period_to_string(year) + "/Fired");
        out_file.add_plot(matc_pl_z, "ZResonance/" + sel_string + "/" + TriggerAnalyzer::period_to_string(year) + "/Matches");
        out_file.add_plot(pres_pl_z, "ZResonance/" + sel_string + "/" + TriggerAnalyzer::period_to_string(year) + "/PreScales");

        out_file.add_plot(trig_pl_jpsi, "JPsi/" + sel_string + "/" + TriggerAnalyzer::period_to_string(year) + "/Fired");
        out_file.add_plot(matc_pl_jpsi, "JPsi/" + sel_string + "/" + TriggerAnalyzer::period_to_string(year) + "/Matches");
        out_file.add_plot(pres_pl_jpsi, "JPsi/" + sel_string + "/" + TriggerAnalyzer::period_to_string(year) + "/PreScales");

        trig_plots +=
            PlotContent<TH1>({z_mass}, {}, {"Z#rightarrow#mu#mu in " + TriggerAnalyzer::period_to_string(year), base_cut.getTitle()},
                             sel_string + "ZMass_" + TriggerAnalyzer::period_to_string(year), multi_page, canvas_opts_mass);
        trig_plots += PlotContent<TH1>({jpsi_mass}, {},
                                       {"J/#psi#rightarrow#mu#mu in " + TriggerAnalyzer::period_to_string(year), base_cut.getTitle()},
                                       sel_string + "JPsiMass_" + TriggerAnalyzer::period_to_string(year), multi_page, canvas_opts_mass);

        // Plot the trigger counts & matching efficiencies
        TriggerPlotPruner pruned_trig_jpsi{trig_pl_jpsi, matc_pl_jpsi, histo_template, TriggerPlotPruner::ToReturn::DecisionPlot};
        TriggerPlotPruner pruned_match_jpsi{trig_pl_jpsi, matc_pl_jpsi, histo_template, TriggerPlotPruner::ToReturn::MatchPlot};
        TriggerPlotPruner pruned_prescale_jpsi{trig_pl_jpsi, pres_pl_jpsi, histo_template, TriggerPlotPruner::ToReturn::MatchPlot};

        trig_plots += PlotContent<TH1>(
            {Plot<TH1>{pruned_trig_jpsi, PlotFormat().Color(kBlack).LegendTitle("all triggers").LegendOption("PL").MarkerStyle(kFullStar)},
             Plot<TH1>{pruned_match_jpsi,
                       PlotFormat().Color(kBlue).LegendTitle("matched triggers").LegendOption("PL").MarkerStyle(kFullTriangleUp)}

            },
            {RatioEntry(1, 0, PlotUtils::efficiencyErrors)

            },
            {"J/#psi#rightarrow#mu#mu in " + TriggerAnalyzer::period_to_string(year), base_cut.getTitle()},
            sel_string + "TriggerJPsi" + TriggerAnalyzer::period_to_string(year), multi_page, canvas_opts);

        TriggerPlotPruner pruned_trig_z{trig_pl_z, matc_pl_z, histo_template, TriggerPlotPruner::ToReturn::DecisionPlot};
        TriggerPlotPruner pruned_match_z{trig_pl_z, matc_pl_z, histo_template, TriggerPlotPruner::ToReturn::MatchPlot};
        TriggerPlotPruner pruned_prescale_z{trig_pl_z, pres_pl_z, histo_template, TriggerPlotPruner::ToReturn::MatchPlot};

        trig_plots += PlotContent<TH1>(
            {Plot<TH1>{pruned_trig_z, PlotFormat().Color(kBlack).LegendTitle("all triggers").LegendOption("PL").MarkerStyle(kFullStar)},
             Plot<TH1>{pruned_match_z,
                       PlotFormat().Color(kBlue).LegendTitle("matched triggers").LegendOption("PL").MarkerStyle(kFullTriangleUp)}

            },
            {RatioEntry(1, 0, PlotUtils::efficiencyErrors)

            },
            {"Z#rightarrow#mu#mu in " + TriggerAnalyzer::period_to_string(year), base_cut.getTitle()},
            sel_string + "TriggerZ_" + TriggerAnalyzer::period_to_string(year), multi_page, canvas_opts);
        /// Plot the prescales

        trig_plots += PlotContent<TH1>(
            {Plot<TH1>{CalculateRatio(pruned_prescale_z, pruned_trig_z), PlotFormat().Color(kBlack).MarkerStyle(kFullTriangleUp)}

            },
            {}, {"Z#rightarrow#mu#mu in " + TriggerAnalyzer::period_to_string(year), base_cut.getTitle()},
            sel_string + "PreScalesZ" + TriggerAnalyzer::period_to_string(year), multi_page, canvas_opts_prescale);

        trig_plots += PlotContent<TH1>(
            {Plot<TH1>{CalculateRatio(pruned_prescale_jpsi, pruned_trig_jpsi), PlotFormat().Color(kBlack).MarkerStyle(kFullTriangleUp)}

            },
            {}, {"J/#psi#rightarrow#mu#mu in " + TriggerAnalyzer::period_to_string(year), base_cut.getTitle()},
            sel_string + "PreScalesJPsi" + TriggerAnalyzer::period_to_string(year), multi_page, canvas_opts_prescale);
    }
}

int main(int argc, char* argv[]) {
    /// Standard piece of code to setup the analysis...
    ArgumentParser parser;
    parser.out_root_file("FiredTriggers.root").summary_pdf("AllFiredTriggers");
    if (!parser.parse_args(argc, argv)) return EXIT_FAILURE;

    CanvasOptions canvas_opts = CanvasOptions()
                                    .RatioAxis(AxisConfig().Title("matching efficiency").ExtraTitleOffset(0.4).Min(0.9).Max(1.005))
                                    .YAxis(AxisConfig().Title("Entries").ExtraTitleOffset(0.4).TopPadding(0.7))
                                    // .XAxis(AxisConfig().ExtraTitleOffset(1.3))
                                    .LabelStatusTag("Internal")
                                    .OutputDir(parser.out_dir())
                                    .CanSizeX(3200)
                                    .CanSizeY(1200);

    std::shared_ptr<MultiPagePdfHandle> multi_page =
        PlotUtils::startMultiPagePdfFile(parser.summary_pdf(), canvas_opts);  // summary pdf with all plots
    std::vector<PlotContent<TH1>> trig_plots;

    /// Create the output file to store all of the histograms

    PlotWriter<TH1> out_file{parser.out_file()};
    if (!out_file.get()) { return EXIT_FAILURE; }

    Sample<TriggerAnalyzer> sample_to_process = parser.prepare_sample<TriggerAnalyzer>();

    ///

    /// Basic selection cut
    SelectionWithTitle<TriggerAnalyzer> base_cut{"All triggers", [](TriggerAnalyzer&) { return true; }};
    plot_triggers(sample_to_process, canvas_opts, multi_page, trig_plots, out_file, base_cut, "All");

    /// Selection to ensure that only one trigger is fired in the event
    SelectionWithTitle<TriggerAnalyzer> exclsuive_trigger{"Exclusive triggers", [](TriggerAnalyzer& t) { return t.only_one_trig_fired(); }};

    plot_triggers(sample_to_process, canvas_opts, multi_page, trig_plots, out_file, exclsuive_trigger, "Exclusive");

    for (auto& pc : trig_plots) {
        if (count_non_empty(pc)) DefaultPlotting::draw1D(pc);
    }

    return EXIT_SUCCESS;
}
