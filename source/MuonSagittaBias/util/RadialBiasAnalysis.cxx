#include <FourMomUtils/xAODP4Helpers.h>
#include <MuonSagittaBias/ArgumentParser.h>
#include <MuonSagittaBias/AsymmetryPlots.h>
#include <MuonSagittaBias/PhaseSpaceCuts.h>
#include <MuonSagittaBias/PlotProvider.h>
#include <MuonSagittaBias/SagittaBiasAnalyzer.h>
#include <MuonSagittaBias/SlicedPlotProvider.h>
#include <MuonSagittaBias/Utils.h>
#include <NtauHelpers/OutFileWriter.h>

/*============================================================================
NOTE:
     USING TREE invariant_m TO CALCULATE sin_theta_rho_2 EVERYWHERE BUT IN THE
     2D HISTOGRAMS OF invariant_m AGAINST sin_theta_rho_2 AND OF
     sin_theta_rho_2_ALPHA: THERE, WE HAVE DIFFERENT HISTOGRAMS FOR EACH CASE
     OF INVARIANT MASS:
        1. sin_theta_rho_2 USING TREE invariant_m
        2. sin_theta_rho_2 USING 4-MOM invariant_m_mu_mass FOR MASSLESS MUONS
        3. sin_theta_rho_2 USING 4-MOM invariant_m_mu_mass FOR MASSIVE MUONS
    (SELECTIONS ALSO MADE USING TREE invariant_m)
=============================================================================*/

// start using tree mass for quality criterion

// user setup:

std::string init_filenames() {
    int data_or_mc;
    std::string year;
    std::cout << "Data or MC? [0 = data / 1 = MC] ";
    std::cin >> data_or_mc;
    if (data_or_mc == 1) {
        year = "MC";
    } else if (data_or_mc == 0) {
        std::cout << "year? (16, 17 or 18)  ";
        std::cin >> year;
    }
    return year;
}

std::string file_name(std::string year, int which_one) {
    std::string my_file_name, my_file_name2;
    my_file_name = "SagittaTemplates" + year + ".root";
    my_file_name2 = "Plots_" + year;

    if (which_one == 1) {
        return my_file_name;
    } else if (which_one == 2) {
        return my_file_name2;
    } else {
        std::string output_str = "wrong input";
        return output_str;
    }
    // std::string all_file_names [2];
    // std::array<std::string, 2> all_file_names {my_file_name, my_file_name2};
    // return all_file_names;
}  ////

///////

int main(int argc, char* argv[]) {
    ArgumentParser parser;
    if (!parser.parse_args(argc, argv)) return EXIT_FAILURE;
    if (!parser.out_dir.isUserSet() && !parser.out_root_file.isUserSet()) {
        std::cerr << "Do not want to clobber with potential existing plots. Please provide --outDir or --outFile" << std::endl;
        return EXIT_FAILURE;
    }

    PlotWriter<TH1> out_file{parser.out_file()};
    if (!out_file.get()) { return EXIT_FAILURE; }

    Sample<SagittaBiasAnalyzer> z_reco_sample = parser.prepare_sample<SagittaBiasAnalyzer>();  // loading input files (samples)

    // styling
    CanvasOptions canvas_opts = CanvasOptions()
                                    .RatioAxis(AxisConfig().Title("to truth-level").ExtraTitleOffset(0.4).Min(0.9).Max(1.005))
                                    .YAxis(AxisConfig().Title("Entries").ExtraTitleOffset(0.4).TopPadding(0.7))
                                    // .XAxis(AxisConfig().ExtraTitleOffset(1.3))
                                    .LabelStatusTag("Simulation Internal")
                                    .OutputDir(parser.out_dir());

    auto multi_page = PlotUtils::startMultiPagePdfFile(parser.summary_pdf(), canvas_opts);  // summary pdf with all plots

    std::vector<PlotContent<TH1>> radial_biases;        // which plots to include (not needed now)
    std::vector<PhaseSpaceCuts> selections;             // deciding which particles to take (particles must fulfill phase space cuts)
    std::vector<PhaseSpaceCuts> selections_for_aisles;  // special selections to investigate aisles in pt plot
    std::vector<PhaseSpaceCuts> selections_quality_criteria;

    /*selections_quality_criteria += PhaseSpaceCuts("", "All pairs_a_bottom", [](SagittaBiasAnalyzer& t, int trk_type) -> bool {
        const double my_delta_z0 = t.delta_z0(trk_type);
        const float z0_quality_cut =
        return my_delta_z0 > z0_quality_cut;
&& t.delta_z0(trk_type) < 1;
*/

    selections_for_aisles += PhaseSpaceCuts("All_a_bottom", "All pairs_a_bottom", [](SagittaBiasAnalyzer& t, int trk_type) -> bool {
        const double neg_eta = t.eta(trk_type, SagittaBiasAnalyzer::LepCharge::Minus);
        const double pos_eta = t.eta(trk_type, SagittaBiasAnalyzer::LepCharge::Plus);

        const double abs_neg_eta = std::abs(neg_eta);
        const double abs_pos_eta = std::abs(pos_eta);  // here!
        return abs_pos_eta > 0.1 && abs_pos_eta < 2.5 && abs_neg_eta > 0.1 && abs_neg_eta < 2.5 &&
               t.pt(trk_type, SagittaBiasAnalyzer::LepCharge::Plus) > 3. && t.pt(trk_type, SagittaBiasAnalyzer::LepCharge::Plus) < 10. &&
               t.pt(trk_type, SagittaBiasAnalyzer::LepCharge::Minus) > 15. && t.pt(trk_type, SagittaBiasAnalyzer::LepCharge::Minus) < 30. &&
               t.delta_z0(trk_type) < 1;
    });

    selections_for_aisles += PhaseSpaceCuts("All_a_top", "All pairs_a_top", [](SagittaBiasAnalyzer& t, int trk_type) -> bool {
        const double neg_eta = t.eta(trk_type, SagittaBiasAnalyzer::LepCharge::Minus);
        const double pos_eta = t.eta(trk_type, SagittaBiasAnalyzer::LepCharge::Plus);

        const double abs_neg_eta = std::abs(neg_eta);
        const double abs_pos_eta = std::abs(pos_eta);  // here!
        return abs_pos_eta > 0.1 && abs_pos_eta < 2.5 && abs_neg_eta > 0.1 && abs_neg_eta < 2.5 &&
               t.pt(trk_type, SagittaBiasAnalyzer::LepCharge::Minus) > 3. && t.pt(trk_type, SagittaBiasAnalyzer::LepCharge::Minus) < 10. &&
               t.pt(trk_type, SagittaBiasAnalyzer::LepCharge::Plus) > 15. && t.pt(trk_type, SagittaBiasAnalyzer::LepCharge::Plus) < 30. &&
               t.delta_z0(trk_type) < 1;
    });

    // for now keepinclusive configarition, later add ones to get eta map
    selections += PhaseSpaceCuts("All_pairs", "All pairs", [](SagittaBiasAnalyzer& t, int trk_type) -> bool {
        const double neg_eta = t.eta(trk_type, SagittaBiasAnalyzer::LepCharge::Minus);
        const double pos_eta = t.eta(trk_type, SagittaBiasAnalyzer::LepCharge::Plus);

        const double abs_neg_eta = std::abs(neg_eta);
        const double abs_pos_eta = std::abs(pos_eta);                                                                        // here!
        return abs_pos_eta > 0.1 && abs_pos_eta < 2.5 && abs_neg_eta > 0.1 && abs_neg_eta < 2.5 && t.delta_z0(trk_type) < 1; /*&&
std::min(t.pt(trk_type, SagittaBiasAnalyzer::LepCharge::Plus), t.pt(trk_type, SagittaBiasAnalyzer::LepCharge::Minus)) > 25*/
        ;  // remove 25 gev cut
    });

    // STARTING NEW SECTION TO TRY TO RESTRICT ETA------------------------------------------------------------------

    // endcap 1.17 < |eta| < 2.5
    selections += PhaseSpaceCuts("End_cap_pairs", "End_cap_pairs", [](SagittaBiasAnalyzer& t, int trk_type) -> bool {
        const double neg_eta = t.eta(trk_type, SagittaBiasAnalyzer::LepCharge::Minus);
        const double pos_eta = t.eta(trk_type, SagittaBiasAnalyzer::LepCharge::Plus);

        const double abs_neg_eta = std::abs(neg_eta);
        const double abs_pos_eta = std::abs(pos_eta);

        return abs_pos_eta > 1.17 && abs_pos_eta < 2.5 && abs_neg_eta > 1.17 && abs_neg_eta < 2.5 && t.delta_z0(trk_type) < 1 &&
               t.delta_z0(trk_type) < 1; /*&&
std::min(t.pt(trk_type, SagittaBiasAnalyzer::LepCharge::Plus), t.pt(trk_type, SagittaBiasAnalyzer::LepCharge::Minus)) > 25*/
    });

    // barrel 0.1 < |eta| < 1.17 -> use 0.1 instead like before
    selections += PhaseSpaceCuts("Barrel_pairs", "Barrel_pairs", [](SagittaBiasAnalyzer& t, int trk_type) -> bool {
        const double neg_eta = t.eta(trk_type, SagittaBiasAnalyzer::LepCharge::Minus);
        const double pos_eta = t.eta(trk_type, SagittaBiasAnalyzer::LepCharge::Plus);

        const double abs_neg_eta = std::abs(neg_eta);
        const double abs_pos_eta = std::abs(pos_eta);
        return abs_pos_eta > 0.1 && abs_pos_eta < 1.17 && abs_neg_eta > 0.1 && abs_neg_eta < 1.17 && t.delta_z0(trk_type) < 1 &&
               t.delta_z0(trk_type) < 1; /*&&
std::min(t.pt(trk_type, SagittaBiasAnalyzer::LepCharge::Plus), t.pt(trk_type, SagittaBiasAnalyzer::LepCharge::Minus)) > 25*/
    });

    // pos in barrel and neg in endcap
    selections += PhaseSpaceCuts("Barrel_endcap_pairs", "Barrel_endcap_pairs", [](SagittaBiasAnalyzer& t, int trk_type) -> bool {
        const double neg_eta = t.eta(trk_type, SagittaBiasAnalyzer::LepCharge::Minus);
        const double pos_eta = t.eta(trk_type, SagittaBiasAnalyzer::LepCharge::Plus);

        const double abs_neg_eta = std::abs(neg_eta);
        const double abs_pos_eta = std::abs(pos_eta);
        if (std::min(abs_pos_eta, abs_neg_eta) < 0.1 || std::max(abs_pos_eta, abs_neg_eta) > 2.5) return false;
        return ((abs_pos_eta < 1.17 && abs_neg_eta > 1.17) || (abs_pos_eta > 1.17 && abs_neg_eta < 1.17)) && t.delta_z0(trk_type) < 1 &&
               t.delta_z0(trk_type) < 1; /*&&
std::min(t.pt(trk_type, SagittaBiasAnalyzer::LepCharge::Plus), t.pt(trk_type, SagittaBiasAnalyzer::LepCharge::Minus)) > 25*/
    });

    // INCLUDE SIN THETA RESTRICITIONS IN SELECTIONS
    selections += PhaseSpaceCuts("theta_All_pairs", "theta_All_pairs", [](SagittaBiasAnalyzer& t, int trk_type) -> bool {
        const double neg_eta = t.eta(trk_type, SagittaBiasAnalyzer::LepCharge::Minus);
        const double pos_eta = t.eta(trk_type, SagittaBiasAnalyzer::LepCharge::Plus);

        const double abs_neg_eta = std::abs(neg_eta);
        const double abs_pos_eta = std::abs(pos_eta);
        //
        const double sin_theta_rho_2_val = t.sin_theta_rho_2(trk_type, true, true);
        const double st_hump = 0.15;
        const double end_hump = 0.55;
        //

        return abs_pos_eta > 0.1 && abs_pos_eta < 2.5 && abs_neg_eta > 0.1 && abs_neg_eta < 2.5 /*&&
           std::min(t.pt(trk_type, SagittaBiasAnalyzer::LepCharge::Plus), t.pt(trk_type, SagittaBiasAnalyzer::LepCharge::Minus)) >
           25*/ && sin_theta_rho_2_val > st_hump && sin_theta_rho_2_val < end_hump && t.delta_z0(trk_type) < 1 && t.delta_z0(trk_type) < 1;
    });

    selections += PhaseSpaceCuts("theta_End_cap_pairs", "theta_End_cap_pairs", [](SagittaBiasAnalyzer& t, int trk_type) -> bool {
        const double neg_eta = t.eta(trk_type, SagittaBiasAnalyzer::LepCharge::Minus);
        const double pos_eta = t.eta(trk_type, SagittaBiasAnalyzer::LepCharge::Plus);

        const double abs_neg_eta = std::abs(neg_eta);
        const double abs_pos_eta = std::abs(pos_eta);
        //
        const double sin_theta_rho_2_val = t.sin_theta_rho_2(trk_type, true, true);
        const double st_hump = 0.15;
        const double end_hump = 0.55;
        //

        return abs_pos_eta > 1.17 && abs_pos_eta < 2.5 && abs_neg_eta > 1.17 && abs_neg_eta < 2.5 /*&&
           std::min(t.pt(trk_type, SagittaBiasAnalyzer::LepCharge::Plus), t.pt(trk_type, SagittaBiasAnalyzer::LepCharge::Minus)) >
           25*/ && sin_theta_rho_2_val > st_hump && sin_theta_rho_2_val < end_hump && t.delta_z0(trk_type) < 1 && t.delta_z0(trk_type) < 1;
    });

    selections += PhaseSpaceCuts("theta_Barrel_pairs", "theta_Barrel_pairs", [](SagittaBiasAnalyzer& t, int trk_type) -> bool {
        const double neg_eta = t.eta(trk_type, SagittaBiasAnalyzer::LepCharge::Minus);
        const double pos_eta = t.eta(trk_type, SagittaBiasAnalyzer::LepCharge::Plus);

        const double abs_neg_eta = std::abs(neg_eta);
        const double abs_pos_eta = std::abs(pos_eta);
        ///
        const double sin_theta_rho_2_val = t.sin_theta_rho_2(trk_type, true, true);
        const double st_hump = 0.15;
        const double end_hump = 0.55;
        ///

        return abs_pos_eta > 0.1 && abs_pos_eta < 1.17 && abs_neg_eta > 0.1 && abs_neg_eta < 1.17 /*&&
           std::min(t.pt(trk_type, SagittaBiasAnalyzer::LepCharge::Plus), t.pt(trk_type, SagittaBiasAnalyzer::LepCharge::Minus)) >
           25*/ && sin_theta_rho_2_val > st_hump && sin_theta_rho_2_val < end_hump && t.delta_z0(trk_type) < 1 && t.delta_z0(trk_type) < 1;
    });

    selections +=
        PhaseSpaceCuts("theta_Barrel_endcap_pairs", "theta_Barrel_endcap_pairs", [](SagittaBiasAnalyzer& t, int trk_type) -> bool {
            const double neg_eta = t.eta(trk_type, SagittaBiasAnalyzer::LepCharge::Minus);
            const double pos_eta = t.eta(trk_type, SagittaBiasAnalyzer::LepCharge::Plus);

            const double abs_neg_eta = std::abs(neg_eta);
            const double abs_pos_eta = std::abs(pos_eta);
            ///
            const double sin_theta_rho_2_val = t.sin_theta_rho_2(trk_type, true, true);
            const double st_hump = 0.15;
            const double end_hump = 0.55;
            ///

            return (abs_pos_eta > 0.1 && abs_pos_eta < 1.17 && abs_neg_eta > 1.17 && abs_neg_eta < 2.5 /*&&
               std::min(t.pt(trk_type, SagittaBiasAnalyzer::LepCharge::Plus), t.pt(trk_type, SagittaBiasAnalyzer::LepCharge::Minus)) >
               25*/ && sin_theta_rho_2_val > st_hump && sin_theta_rho_2_val < end_hump) && std::min(abs_pos_eta, abs_neg_eta) > 0.1 &&
                   std::max(abs_neg_eta, abs_pos_eta) < 2.5 &&
                   ((abs_pos_eta < 1.17 && abs_neg_eta > 1.17) || (abs_pos_eta > 1.17 && abs_neg_eta < 1.17)) && t.delta_z0(trk_type) < 1 &&
                   t.delta_z0(trk_type) < 1;
        });

    /// END OF NEW SECTION FOR ETA SELECTION----------------------------------------------------------------------------

    std::vector<PlotProvider> slicers;

    std::vector<PlotFiller> fillers;

    std::vector<PlotFiller> fillers_JPsi;

    std::vector<PlotFiller> fillers_2d;

    std::vector<PlotFiller> fillers_2d_JPsi;

    std::vector<PlotFiller> fillers_2d_aisles;

    std::vector<PlotFiller> fillers_2d_JPsi_PSEUDOMASS;

    // for J/psi mass cut:  ===================================================================================================

    // look at some different options for the Jpsi mass:

    //  1. sin_theta_rho_2 USING TREE invariant_m
    fillers_2d_JPsi += PlotFiller(
        [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
            return h->GetBin(h->GetXaxis()->FindBin(t.sin_theta_rho_2(trk_type, true, true)),
                             h->GetYaxis()->FindBin(t.invariant_m(trk_type)));
        },
        std::make_shared<TH2D>("Sin_theta_rho_2_m_Plot_Jpsi_TREE",
                               "sin^{2}(#theta_{#rho}) against m_{#mu#mu} (TREE);sin^{2}(#theta_{#rho});m_{#mu#mu} (TREE)", 60, 0, 1., 60,
                               2.7, 3.5));

    // TH2D (const char *name, const char *title, Int_t nbinsx, const Double_t *xbins, Int_t nbinsy, const Double_t*ybins)
    // TH1D (const char *name, const char *title, Int_t nbinsx, const Double_t *xbins)

    //  2. sin_theta_rho_2 USING 4-MOM invariant_m_mu_mass FOR MASSLESS MUONS
    fillers_2d_JPsi += PlotFiller(
        [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
            return h->GetBin(h->GetXaxis()->FindBin(t.sin_theta_rho_2(trk_type, false, false)),
                             h->GetYaxis()->FindBin(t.invariant_m_mu_mass(trk_type, false)));
        },
        std::make_shared<TH2D>("Sin_theta_rho_2_m_Plot_Jpsi_MASSLESS",
                               "sin^{2}(#theta_{#rho}) against m_{#mu#mu} (MASSLESS);sin^{2}(#theta_{#rho});m_{#mu#mu} (MASSLESS)", 60, 0,
                               1., 60, 2.7, 3.5));

    //  3. sin_theta_rho_2 USING 4-MOM invariant_m_mu_mass FOR MASSIVE MUONS
    fillers_2d_JPsi += PlotFiller(
        [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
            return h->GetBin(h->GetXaxis()->FindBin(t.sin_theta_rho_2(trk_type, false, true)),
                             h->GetYaxis()->FindBin(t.invariant_m_mu_mass(trk_type, true)));
        },
        std::make_shared<TH2D>("Sin_theta_rho_2_m_Plot_Jpsi_MASSIVE",
                               "sin^{2}(#theta_{#rho}) against m_{#mu#mu} (MASSIVE);sin^{2}(#theta_{#rho});m_{#mu#mu} (MASSIVE) ", 60, 0,
                               1., 60, 2.7, 3.5));

    // CREATE 2D FILLER FOR ALPHA m_mumu--------------------------------------------------------------------------------

    //  1. sin_theta_rho_2 USING TREE invariant_m
    fillers_2d_JPsi += PlotFiller(
        [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
            return h->GetBin(h->GetXaxis()->FindBin(t.sin_theta_rho_2_alpha(trk_type)), h->GetYaxis()->FindBin(t.invariant_m(trk_type)));
        },
        std::make_shared<TH2D>("Sin_theta_rho_2_m_alpha_Plot_Jpsi_TREE",
                               "sin^{2}(#theta_{#rho#alpha}) against m_{#mu#mu} (TREE);sin^{2}(#theta_{#rho#alpha}); m_{#mu#mu} (TREE) ",
                               60, 0, 1., 60, 2.7, 3.5));

    //  2. sin_theta_rho_2 USING 4-MOM invariant_m_mu_mass FOR MASSLESS MUONS
    fillers_2d_JPsi += PlotFiller(
        [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
            return h->GetBin(h->GetXaxis()->FindBin(t.sin_theta_rho_2_alpha(trk_type)),
                             h->GetYaxis()->FindBin(t.invariant_m_mu_mass(trk_type, false)));
        },
        std::make_shared<TH2D>(
            "Sin_theta_rho_2_m_alpha_Plot_Jpsi_MASSLESS",
            "sin^{2}(#theta_{#rho#alpha}) against m_{#mu#mu} (MASSLESS) ;sin^{2}(#theta_{#rho#alpha}); m_{#mu#mu} (MASSLESS) ", 60, 0, 1.,
            60, 2.7, 3.5));

    //  3. sin_theta_rho_2 USING 4-MOM invariant_m_mu_mass FOR MASSIVE MUONS
    fillers_2d_JPsi += PlotFiller(
        [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
            return h->GetBin(h->GetXaxis()->FindBin(t.sin_theta_rho_2_alpha(trk_type)),
                             h->GetYaxis()->FindBin(t.invariant_m_mu_mass(trk_type, true)));
        },
        std::make_shared<TH2D>(
            "Sin_theta_rho_2_m_alpha_Plot_Jpsi_MASSIVE",
            "sin^{2}(#theta_{#rho#alpha}) against m_{#mu#mu} (MASSIVE);sin^{2}(#theta_{#rho#alpha}); m_{#mu#mu} (MASSIVE)", 60, 0, 1., 60,
            2.7, 3.5));

    // CREATE 2D FILLER FOR sinthetarho2 method comparison----------------------------------------------------------------
    fillers_2d_JPsi += PlotFiller(
        [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
            return h->GetBin(h->GetXaxis()->FindBin(t.sin_theta_rho_2_alpha(trk_type)),
                             h->GetYaxis()->FindBin(t.sin_theta_rho_2(trk_type, true, true)));
        },
        std::make_shared<TH2D>(
            "Sin_theta_rho_2_method_comparison_Jpsi",
            "sin^{2}(#theta_{#rho#alpha} against sin^{2}(#theta_{#rho}) ;sin^{2}(#theta_{#rho#alpha}; sin^{2}(#theta_{#rho}) ", 1000, 0,
            1.5, 1000, 0, 1.5));

    // CREATE 2D FILLER FOR invariant m method comparison----------------------------------------------------------------
    fillers_2d_JPsi += PlotFiller(
        [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
            return h->GetBin(h->GetXaxis()->FindBin(t.invariant_m_mu_mass(trk_type, false)),
                             h->GetYaxis()->FindBin(t.invariant_m(trk_type)));
        },
        std::make_shared<TH2D>("invariant_mass_method_comparison",
                               "Invariant m Method Comparison ; m_{#mu#mu} (set m_{#mu} = 105.6 MeV) ; m_{#mu#mu} (original method)", 1000,
                               2.7, 3.5, 1000, 2.7, 3.5));

    fillers_2d_JPsi += PlotFiller(
        [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
            return h->GetBin(h->GetXaxis()->FindBin(t.invariant_m_mu_mass(trk_type, true)),
                             h->GetYaxis()->FindBin(t.invariant_m(trk_type)));
        },
        std::make_shared<TH2D>("invariant_mass_method_comparison_2",
                               "Invariant m Method Comparison (part 2) ; m_{#mu#mu} (set m_{#mu} = 0.0 MeV) ; m_{#mu#mu} (original method)",
                               1000, 2.7, 3.5, 1000, 2.7, 3.5));

    // now weird aisles only:

    // CREATE 2D FILLER FOR pt comparison ---------------------------------------------------------------------------
    fillers_2d_JPsi += PlotFiller(
        [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
            return h->GetBin(h->GetXaxis()->FindBin(t.pt(trk_type, SagittaBiasAnalyzer::LepCharge::Plus)),
                             h->GetYaxis()->FindBin(t.pt(trk_type, SagittaBiasAnalyzer::LepCharge::Minus)));
        },
        std::make_shared<TH2D>("pt_comparison", "Transverse Momentum Comparison ; p_{T}^{+};p_{T}^{-}", 1000, 0, 100, 1000, 0, 100));

    // zoom into aisles:

    // CREATE 2D FILLER FOR ETA RANGE--------------------------------------------------------------------------------

    fillers_2d_JPsi += PlotFiller(
        [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
            return h->GetBin(h->GetXaxis()->FindBin(t.eta(trk_type, SagittaBiasAnalyzer::LepCharge::Minus)),
                             h->GetYaxis()->FindBin(t.eta(trk_type, SagittaBiasAnalyzer::LepCharge::Plus)));
        },
        std::make_shared<TH2D>("Eta_plus_Eta_minus_Plot_Jpsi", "Eta_plus_Eta_minus_Plot;#eta_{-}; #eta_{+}", 60, -2.5, 2.5, 60, -2.5, 2.5));

    // CREATE 2D FILLER FOR ALPHA-DELTA_PHI--------------------------------------------------------------------------------
    fillers_2d_JPsi += PlotFiller(
        [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
            return h->GetBin(h->GetXaxis()->FindBin(t.delta_phi(trk_type)), h->GetYaxis()->FindBin(t.alpha(trk_type)));
        },
        std::make_shared<TH2D>("alpha_vs_delta_phi_Plot_Jpsi", "alpha_vs_delta_phi;#delta#phi;#alpha", 60, 0, 3.15, 60, 0, 3.15));

    // CREATE 2D FILLER FOR PSEUDOMASS-ETA--------------------------------------------------------------------------------
    // need to esperiment with different binnings
    // pos lepton:
    // eta granularity: range is 5/30 bins ≈ 0.2 granularity
    fillers_2d_JPsi_PSEUDOMASS += PlotFiller(
        [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
            return h->GetBin(h->GetXaxis()->FindBin(t.eta(trk_type, true)), h->GetYaxis()->FindBin(t.pseudo_mass(trk_type, true)));
        },
        std::make_shared<TH2D>("eta_pseudomass_pos_Jpsi",
                               "#eta against pseudomass M^{+} (for mu_{+} , J/Psi);#eta (#mu_{+}); pseudomass M^{+}", 60, -2.5, 2.5, 30,
                               0.1, 10.));

    // neg lepton:
    fillers_2d_JPsi_PSEUDOMASS += PlotFiller(
        [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
            return h->GetBin(h->GetXaxis()->FindBin(t.eta(trk_type, true)), h->GetYaxis()->FindBin(t.pseudo_mass(trk_type, false)));
        },
        std::make_shared<TH2D>("eta_pseudomass_neg_Jpsi",
                               "#eta against pseudomass M^{-} (for mu_{-} , J/Psi);#eta (#mu_{-}); pseudomass M^{-}", 60, -2.5, 2.5, 30,
                               0.1, 10.));

    //------------------------------------------------------------------------------------------------------------------

    // mass filler for aisles selection
    fillers_2d_aisles += PlotFiller(
        [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
            return h->GetBin(h->GetXaxis()->FindBin(t.sin_theta_rho_2(trk_type, true, true)),
                             h->GetYaxis()->FindBin(t.invariant_m(trk_type)));
        },
        std::make_shared<TH2D>("Sin_theta_rho_2_m_Plot_Jpsi_AISLES",
                               "sin^{2}(#theta_{#rho}) against m_{#mu#mu} (TREE);sin^{2}(#theta_{#rho});m_{#mu#mu} (TREE)", 60, 0, 1., 60,
                               2.7, 3.5));

    fillers_2d_aisles += PlotFiller(
        [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
            return h->GetBin(h->GetXaxis()->FindBin(t.pt(trk_type, SagittaBiasAnalyzer::LepCharge::Plus)),
                             h->GetYaxis()->FindBin(t.pt(trk_type, SagittaBiasAnalyzer::LepCharge::Minus)));
        },
        std::make_shared<TH2D>("pt_comparison_AISLES", "Transverse Momentum Comparison ; p_{T}^{+};p_{T}^{-}", 500, 0, 100, 500, 0, 100));

    // for Z mass cut:  ===================================================================================================
    fillers_2d += PlotFiller(
        [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
            return h->GetBin(h->GetXaxis()->FindBin(t.sin_theta_rho_2(trk_type, true, true)),
                             h->GetYaxis()->FindBin(t.invariant_m(trk_type)));
        },
        std::make_shared<TH2D>("Sin_theta_rho_2_m_Plot", "sin^{2}(#theta_{#rho}) against m_{#mu#mu};sin^{2}(#theta_{#rho});m_{#mu#mu} ", 60,
                               0, 1., 30, 60, 120));

    // TH2D (const char *name, const char *title, Int_t nbinsx, const Double_t *xbins, Int_t nbinsy, const Double_t*ybins)
    // TH1D (const char *name, const char *title, Int_t nbinsx, const Double_t *xbins)
    // -> & THERE ARE DIFFERENT TYPES OF POINTERS

    // CREATE 2D FILLER FOR ALPHA m_mumu--------------------------------------------------------------------------------
    fillers_2d += PlotFiller(
        [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
            return h->GetBin(h->GetXaxis()->FindBin(t.sin_theta_rho_2_alpha(trk_type)), h->GetYaxis()->FindBin(t.invariant_m(trk_type)));
        },
        std::make_shared<TH2D>("Sin_theta_rho_2_m_alpha_Plot",
                               "sin^{2}(#theta_{#rho#alpha}) against m_{#mu#mu} ;sin^{2}(#theta_{#rho#alpha}); m_{#mu#mu} ", 60, 0, 1., 60,
                               60, 120));

    fillers_2d += PlotFiller(
        [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
            return h->GetBin(h->GetXaxis()->FindBin(t.sin_theta_rho_2_alpha(trk_type)),
                             h->GetYaxis()->FindBin(t.sin_theta_rho_2(trk_type, true, true)));
        },
        std::make_shared<TH2D>(
            "Sin_theta_rho_2_method_comparison",
            "sin^{2}(#theta_{#rho#alpha} against sin^{2}(#theta_{#rho}) ;sin^{2}(#theta_{#rho#alpha}; sin^{2}(#theta_{#rho}) ", 60, 0, 1.,
            60, 0, 1.));

    // cout << "str";
    // CREATE 2D FILLER FOR ETA RANGE--------------------------------------------------------------------------------

    fillers_2d += PlotFiller(
        [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
            return h->GetBin(h->GetXaxis()->FindBin(t.eta(trk_type, SagittaBiasAnalyzer::LepCharge::Minus)),
                             h->GetYaxis()->FindBin(t.eta(trk_type, SagittaBiasAnalyzer::LepCharge::Plus)));
        },
        std::make_shared<TH2D>("Eta_plus_Eta_minus_Plot", "Eta_plus_Eta_minus_Plot;#eta_{-}; #eta_{+}", 60, -2.5, 2.5, 60, -2.5, 2.5));

    // CREATE 2D FILLER FOR ALPHA-DELTA_PHI--------------------------------------------------------------------------------
    fillers_2d += PlotFiller(
        [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
            return h->GetBin(h->GetXaxis()->FindBin(t.delta_phi(trk_type)), h->GetYaxis()->FindBin(t.alpha(trk_type)));
        },
        std::make_shared<TH2D>("alpha_vs_delta_phi_Plot", "alpha_vs_delta_phi;#delta#phi;#alpha", 60, 0, 3.15, 60, 0, 3.15));
    //------------------------------------------------------------------------------------------------------------------

    // defining variables plotted on the histogram
    fillers += PlotFiller([](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) { return h->GetXaxis()->FindBin(t.phiCS(trk_type)); },
                          std::make_shared<TH1D>("PhiCS", "dummy;#phi^{CS} [rad];a.u.", 60, -M_PI, M_PI));
    fillers += PlotFiller(
        [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) { return h->GetXaxis()->FindBin(t.sin_theta_rho_2(trk_type, true, true)); },
        std::make_shared<TH1D>("Sin_theta_rho_2_1D_Plot", "dummy;sin^{2}(#theta_{#rho})", 60, 0, 1.5));
    fillers +=
        PlotFiller([](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) { return h->GetXaxis()->FindBin(t.invariant_m(trk_type)); },
                   std::make_shared<TH1D>("Invariant_mass_1D_Plot", "dummy;m_{#mu#mu} ", 60, 0, 120));

    fillers_JPsi += PlotFiller([](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) { return h->GetXaxis()->FindBin(t.phiCS(trk_type)); },
                               std::make_shared<TH1D>("PhiCS_JPsi", "dummy;#phi^{CS} [rad];a.u.", 60, -M_PI, M_PI));
    fillers_JPsi += PlotFiller(
        [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) { return h->GetXaxis()->FindBin(t.sin_theta_rho_2(trk_type, true, true)); },
        std::make_shared<TH1D>("Sin_theta_rho_2_1D_Plot_JPsi", "dummy;sin^{2}(#theta_{#rho})", 60, 0, 1.5));
    fillers_JPsi +=
        PlotFiller([](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) { return h->GetXaxis()->FindBin(t.invariant_m(trk_type)); },
                   std::make_shared<TH1D>("Invariant_mass_1D_Plot_JPsi", "dummy;m_{#mu#mu} ", 60, 0, 5));

    fillers_JPsi +=
        PlotFiller([](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) { return h->GetXaxis()->FindBin(t.invariant_pt(trk_type)); },
                   std::make_shared<TH1D>("Invariant_pt_1D_Plot_JPsi", "dummy;p_{T, #mu#mu} ", 1000, 0, 100));

    fillers_JPsi +=
        PlotFiller([](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) { return h->GetXaxis()->FindBin(t.pseudo_mass(trk_type, true)); },
                   std::make_shared<TH1D>("Pseudomass_+_1D_Plot_JPsi", "dummy;M^{+} ", 500, 0, 10));
    fillers_JPsi += PlotFiller(
        [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) { return h->GetXaxis()->FindBin(t.pseudo_mass(trk_type, false)); },
        std::make_shared<TH1D>("Pseudomass_-_1D_Plot_JPsi", "dummy;M^{-} ", 500, 0, 10));

    // specially important when running MC - QUALITY PARAMETERS
    fillers_JPsi += PlotFiller(
        [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) { return h->GetXaxis()->FindBin(std::abs(t.d0_sig(trk_type, true))); },
        std::make_shared<TH1D>("d0err_+_1D_Plot", "dummy;d_{0} error (#mu_{+})", 50, 0, 10));
    fillers_JPsi += PlotFiller(
        [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) { return h->GetXaxis()->FindBin(std::abs(t.d0_sig(trk_type, false))); },
        std::make_shared<TH1D>("d0err_-_1D_Plot", "dummy;d_{0} error (#mu_{-}) ", 50, 0, 10));
    fillers_JPsi +=
        PlotFiller([](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) { return h->GetXaxis()->FindBin(t.delta_z0(trk_type)); },
                   std::make_shared<TH1D>("delta_z0_1D_Plot", "dummy;#Delta z_{0} ", 500, 0, 2));
    fillers_JPsi +=
        PlotFiller([](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) { return h->GetXaxis()->FindBin(t.delta_d0(trk_type)); },
                   std::make_shared<TH1D>("delta_d0_1D_Plot", "dummy;#Delta d_{0}", 500, 0, 2));
    fillers_JPsi +=
        PlotFiller([](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) { return h->GetXaxis()->FindBin(t.pt(trk_type, true)); },
                   std::make_shared<TH1D>("pt_plus", "dummy;#p_{t+}", 500, 0, 100));
    fillers_JPsi +=
        PlotFiller([](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) { return h->GetXaxis()->FindBin(t.pt(trk_type, false)); },
                   std::make_shared<TH1D>("pt_minus", "dummy;#p_{t-}", 500, 0, 100));

    ////////

    // combines fillers (varibles) with phase space cuts###################################################

    // 1D Z fillers
    for (auto& fill : fillers) {
        for (PhaseSpaceCuts& phase_space : selections) {
            PlotProvider my_provider(z_reco_sample, fill, phase_space);
            radial_biases += PlotContent<TH1>(
                {
                    my_provider.make_plot(MuonTrk::IDTrk, {0, 150}),  //{}cut in invariant masses
                },
                {}, {"Initial Plots"}, my_provider.name(), multi_page, canvas_opts);
        }
    }

    // 2D Z fillers
    for (auto& fill : fillers_2d) {
        for (PhaseSpaceCuts& phase_space : selections) {
            PlotProvider my_provider(z_reco_sample, fill, phase_space);
            my_provider.add_plot(parser.get_trk_types_ROOT_file(), out_file, {61, 121});
        }
    }

    // 1D JPsi fillers
    for (auto& fill : fillers_JPsi) {
        for (PhaseSpaceCuts& phase_space : selections) {
            PlotProvider my_provider(z_reco_sample, fill, phase_space);
            radial_biases += PlotContent<TH1>(
                {
                    my_provider.make_plot(MuonTrk::IDTrk, {2.7, 3.5}),  //{}cut in invariant masses
                },
                {}, {"Initial Plots - J/Psi"}, my_provider.name(), multi_page, canvas_opts);
        }
    }

    // 2D J/PSI fillers
    for (auto& fill : fillers_2d_JPsi) {
        for (PhaseSpaceCuts& phase_space : selections) {
            PlotProvider my_provider(z_reco_sample, fill, phase_space);
            my_provider.add_plot(parser.get_trk_types_ROOT_file(), out_file, {2.7, 3.5});
        }
    }

    // 2D J/PSI fillers PSEUDOMASS
    for (auto& fill : fillers_2d_JPsi_PSEUDOMASS) {
        for (PhaseSpaceCuts& phase_space : selections) {
            PlotProvider my_provider(z_reco_sample, fill, phase_space);
            my_provider.add_plot(parser.get_trk_types_ROOT_file(), out_file, {0.1, 10.0});
        }
    }

    // 2D AISLES fillers
    for (auto& fill : fillers_2d_aisles) {
        for (PhaseSpaceCuts& phase_space : selections_for_aisles) {
            PlotProvider my_provider(z_reco_sample, fill, phase_space);
            my_provider.add_plot(parser.get_trk_types_ROOT_file(), out_file, {2.7, 3.5});
        }
    }

    ///
    ///         Add the pseudo mass for positive and negative charges
    ///

    std::sort(radial_biases.begin(), radial_biases.end(),
              [](const PlotContent<TH1>& a, const PlotContent<TH1>& b) { return a.getFileName() < b.getFileName(); });
    for (auto& pc : radial_biases) {
        if (count_non_empty(pc) == pc.getPlots().size()) DefaultPlotting::draw1D(pc);
    }
    return EXIT_SUCCESS;
}
