#include "NtupleAnalysisUtils/Visualisation/MultiPagePdfHandle.h"

MultiPagePdfHandle::MultiPagePdfHandle(const std::string & fname){
    m_fname = fname; 
    std::shared_ptr<TCanvas> can = std::make_shared<TCanvas>("bla","bla",800,600);
    can->SaveAs((fname+ "[").c_str());
}
MultiPagePdfHandle::~MultiPagePdfHandle(){
    std::shared_ptr<TCanvas> can = std::make_shared<TCanvas>("bla","bla",800,600);
    can->SaveAs((m_fname+ "]").c_str());
}
void MultiPagePdfHandle::saveCanvas(TCanvas* can) const{
    can->SaveAs(m_fname.c_str());    
}
void MultiPagePdfHandle::saveCanvas(std::shared_ptr<TCanvas> can) const{
    can->SaveAs(m_fname.c_str());    
}