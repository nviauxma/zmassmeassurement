/// Test some advanced branch features 

#include "NtupleAnalysisUtils/NTAUTopLevelIncludes.h"

int main(int,char**){

    TTree* tout = new TTree("outTree","aTree");

    /// a plain float
    float in_flt = 42.0;
    /// plus a string (gasp)
    std::string in_str = "pneumonoultramicroscopicsilicovolcanoconiosis";
    /// a vector
    std::vector<double> in_vec{1,2,3};
    /// and an array 
    double in_arr[3]={4.,5.,6.}; 

    

    NtupleBranch<std::string> b_str ("lovelyString",tout);
    b_str.set(in_str); 

    NtupleBranch<float > b_float ("lovelyFloat",tout);
    b_float.set(in_flt);

    NtupleBranch<std::vector<double> > b_vec ("lovelyVec",tout);
    b_vec.set(in_vec);

    NtupleBranch<double* > b_arr ("evilArray",3,tout);
    b_arr.set(in_arr);


    /// try accessing write-mode branches - should allow reading
    if (b_str() != in_str) return 1; 
    if (b_float() != in_flt) return 1; 
    if (b_vec() != in_vec) return 1; 
    if (b_arr(0) != in_arr[0]) return 1; 
    if (b_arr(1) != in_arr[1]) return 1; 
    if (b_arr(2) != in_arr[2]) return 1; 
    std::cout << "reading original values: OK "<<std::endl;

    b_str.set("Hallo!"); 
    if (b_str() != "Hallo!") return 1; 
    std::cout << "string overwriting after read: OK "<<std::endl;

    b_float.set(12.); 
    if (b_float() !=12.) return 1; 
    std::cout << "float overwriting after read: OK "<<std::endl;

    b_vec.push_back(15.); 
    if (b_vec.size() !=4 || b_vec(3) != 15.) return 1; 
    std::cout << "vector push back after read: OK "<<std::endl;

    b_vec.clear(); 
    if (b_vec.size() !=0) return 1; 
    std::cout << "vector clear after read: OK "<<std::endl;

    b_vec.set(in_vec); 
    if (b_vec() != in_vec) return 1; 
    std::cout << "vector fill in place after read: OK "<<std::endl;

    b_vec.set(2,22); 
    if (b_vec(2) != 22) return 1; 
    std::cout << "vector element-wise set OK "<<std::endl;

    std::vector<double> blub;
    for(auto & v : b_vec){
        blub.push_back(v);
    }
    if (blub != b_vec()) return 1; 
    std::cout << "vector range-based loop access: OK "<<std::endl;

    
    NtupleBranch<std::string> b_otherStr ("thievingString",tout);
    b_otherStr.set(b_str);
    if (b_otherStr() != b_str()) return 1;  
    std::cout << "String branch copy from input branch: OK "<<std::endl;




    return EXIT_SUCCESS;
}