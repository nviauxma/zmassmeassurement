#ifndef __NTAU__PLOT__BASIC_POPULATOR__H
#define __NTAU__PLOT__BASIC_POPULATOR__H

#include "NtupleAnalysisUtils/Plot/Plot.h"
#include <functional>
#include "TGraph.h"

/// The classes declared here are used for actually populating our plots. 
/// Several flavours exist as described below. 
/// They all implement the IPlotPopulator interface, which is used to talk to the 
/// Plot class. 


/// You can simply create histograms in-place.
/// All arguments will be passed directly into the ROOT constructor
template <class HistoType> class ConstructInPlace: public IPlotPopulator<HistoType>{
    public:
        /// forwards the arguments used for histogram definition to the ROOT constructor
        template <class... Args> ConstructInPlace(Args ...args);
        std::shared_ptr<HistoType> populate() override;
        std::shared_ptr<IPlotPopulator<HistoType>> clonePopulator() const override;
    private: 
        std::shared_ptr<HistoType> m_localHisto{nullptr}; 
};

// A fairly trivial populator which will package 
// already existing histograms 
template <class HistoType> class CopyExisting: public IPlotPopulator<HistoType>{
    public:
        /// note that the constructor here already clones the histo at the time of creation. 
        CopyExisting(const HistoType & toCopy);
        CopyExisting(const HistoType* toCopy);
        CopyExisting(const Plot<HistoType> & toCopy);
        CopyExisting(std::shared_ptr<HistoType> toCopy);
        /// No clone happens in populate(), since we clone at construction time 
        std::shared_ptr<HistoType> populate() override;
        std::shared_ptr<IPlotPopulator<HistoType>> clonePopulator() const override; 
    private: 
        std::shared_ptr<HistoType> m_localCopy{nullptr}; 
};

/// This version will load a histogram from an input file. 
/// Handles opening/closing of the file internally. 
template <class HistoType> class LoadFromFile: public IPlotPopulator<HistoType>{
    public:
        LoadFromFile(const std::string & fileName,  /// name of the file to read from
                     const std::string & objKey );  /// name (incl. directories) of the object to load. 
                             
        virtual std::shared_ptr<HistoType> populate() override;
        virtual std::shared_ptr<IPlotPopulator<HistoType>> clonePopulator() const override;
    protected:
        std::string m_fname;
        std::string m_objKey;
};

/// This version will create a histogram in place an populate it using a manually defined function. 
/// Can be used with fillRandom for example. But mainly intended as a fallback for cases not covered above
template <class HistoType> class ConstructAndFillInPlace: public IPlotPopulator<HistoType>{
    public:
        template <class... Args> ConstructAndFillInPlace(std::function<void(HistoType*)> filler, 
                                                         Args... args);  /// Arguments to the ROOT object constructor
        ConstructAndFillInPlace(const HistoType* hist,   /// here we pass the histogram directly 
                                std::function<void(HistoType*)> filler
                                );  
        // virtual ~ConstructAndFillInPlace();
                               
                                                        
        virtual std::shared_ptr<HistoType> populate() override;
        virtual std::shared_ptr<IPlotPopulator<HistoType>> clonePopulator() const override;
    protected:
        /// aux c-tor used for cloning
        ConstructAndFillInPlace(std::shared_ptr<HistoType> hist, 
                                std::function<void(HistoType*)> filler);
        std::shared_ptr<HistoType> m_localCopy{nullptr}; 
        std::function<void(HistoType*)> m_fillFunc;
        bool m_alreadyFilled{false}; 
};


/// helper method needed for the LoadFromFile populator 

template <class Thing> inline void SetDirIfNeeded(std::shared_ptr<Thing> item){
    item->SetDirectory(0);
}

template <> inline void SetDirIfNeeded(std::shared_ptr<TGraph>){}

#include "BasicPopulators.ixx"

#endif
