#ifndef NTAU__DATATEMPLATEHOLDER_H
#define NTAU__DATATEMPLATEHOLDER_H
#include <MuonSagittaBias/Utils.h>
#include <TGraph.h>
///   Helper class holding each template point. Either from the actual recorded data
///   and therefore representing the data to fit or from the simulated templates
///   The histogram paths have to satisfy the naming convention introduced in
///   SlicedPlotProvider and PlotProvider
///     <Trk_Type>/<Variable_name>/<Selection_name>/<BinInSelection> + <mass_window>/<Injected_parameters>
class DataTemplateHolder {
public:
    DataTemplateHolder(const std::shared_ptr<TFile>& in_file, const std::string& data_path);

    /// Name of the file from which the template is loaded
    std::string file_name() const;
    /// Path of the template variable
    std::string template_path() const;

    /// Retrieve the track_type
    int trk_type() const;
    /// Retrieve the injected bias from the template name
    float inject_bias() const;
    /// Retrieve the injected resolution from the template name
    float inject_resolution() const;

    /// Retrieve the name of the variable
    std::string variable_name() const;
    /// Primary selection
    std::string primary_selection() const;

    /// mass window
    float mll_low() const;
    float mll_high() const;

    /// Retrieve the template itself
    std::shared_ptr<TH1> get() const;
    /// Calculate the chi2 w.r.t. another histogram where as
    /// this instance is assumed to act as data
    struct Chi2TestResult {
        double chi2{0};
        double p_val{0.};
        int ndf{0};
        std::vector<double> residuals;
    };
    Chi2TestResult chi2(const DataTemplateHolder& mc_template) const;

    double ReducedChi2(const DataTemplateHolder& mc_template) const;

    /// Number of degrees of freedom. i.e. number of visible bins
    unsigned int nDoF() const;

    ///
    std::shared_ptr<TGraph> extract_chi2(const std::vector<std::shared_ptr<DataTemplateHolder>>& mc_templates) const;

    // Wrapper methods for TH1 GetBinContent & GetBinError
    double GetBinContent(int bin) const;
    double GetBinError(int bin) const;

    bool operator<(const DataTemplateHolder& other) const;
    bool is_compatible(const DataTemplateHolder& other) const;

    /// Set the integrated luminosity for the data injection
    static void set_lumi(double lumi);

    std::vector<std::string> to_title() const;

private:
    Chi2TestResult chi2(const DataTemplateHolder& mc_template, const std::shared_ptr<TH1>& data_template) const;
    double ReducedChi2(const DataTemplateHolder& mc_template, const std::shared_ptr<TH1>& data_template) const;

    //// Inputs
    std::shared_ptr<TFile> m_file{nullptr};
    std::shared_ptr<TH1> m_template{nullptr};
    std::string m_data_path{};

    /// File name
    std::string m_file_name{};
    /// Variable name
    std::string m_var_name{};
    /// Primary selection
    std::string m_prim_sel{};
    /// Calibration constants associated to the template
    MuonTrk::calib_constants m_calib{};
    /// Mass window cut
    std::pair<float, float> m_mass_window{0., 0.};

    void extract_kinematic_bin(const std::string& sel_str);
    void extract_mass_bin(const std::string& sel_str);

    /// Kinematic selection
    struct KinematicBin {
        KinematicBin(const std::string& _var, float _low, float _high);
        bool operator==(const KinematicBin& other) const;
        bool operator<(const KinematicBin& other) const;
        bool operator!=(const KinematicBin& other) const;
        std::string to_title() const;

        std::string var_name{};
        float low_cut{-FLT_MAX};
        float high_cut{FLT_MAX};
    };

    std::vector<KinematicBin> m_kine_sel{};
    /// Number of degrees of freedom represented by the number of bins in the visible range
    unsigned int m_nDoF{0};
    /// Maximum chi2 to use for the cut off
    static double s_max_chi2;
    /// integrated luminisosity in inverse fb
    static double s_int_lumi;
};

#endif