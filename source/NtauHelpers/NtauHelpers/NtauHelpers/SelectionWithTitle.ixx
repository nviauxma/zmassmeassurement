#ifndef NTAUHELPERS_SELECTIONWITH_TITLE_IXX
#define NTAUHELPERS_SELECTIONWITH_TITLE_IXX
#include <NtauHelpers/Utils.h>

// Standard constructor for everyday use - give the selection (cut) a name and something to do
template <class ProcessThis>
SelectionWithTitle<ProcessThis>::SelectionWithTitle(const std::string& title, std::function<bool(ProcessThis&)> toApply) :
    Selection<ProcessThis>(toApply), m_title(title) {}

// copy constructor
template <class ProcessThis>
SelectionWithTitle<ProcessThis>::SelectionWithTitle(const SelectionWithTitle<ProcessThis>& other) :
    Selection<ProcessThis>(other), m_title(other.m_title) {}
// default, provides a dummy selection accepting everything
template <class ProcessThis> SelectionWithTitle<ProcessThis>::SelectionWithTitle() : Selection<ProcessThis>(), m_title() {}

template <class ProcessThis> std::shared_ptr<ISelection> SelectionWithTitle<ProcessThis>::clone() const {
    return std::make_shared<SelectionWithTitle<ProcessThis>>(*this);
}
template <class ProcessThis> void SelectionWithTitle<ProcessThis>::setTitle(const std::string& title) { m_title = title; }
template <class ProcessThis> std::string SelectionWithTitle<ProcessThis>::getTitle() const { return m_title; }

#endif
