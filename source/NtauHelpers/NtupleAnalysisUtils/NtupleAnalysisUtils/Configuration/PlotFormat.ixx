template <class Histo> void PlotFormat::applyTo(Histo *H) const{
    if (!H) return; 
    
    if (FillStyle.isUserSet()){
        H->SetFillStyle(FillStyle());
        /// The fill color takes precedence over the general color 
        /// if both are set
        Color_t fillCol = Color(); 
        if (FillColor.isUserSet()) fillCol = FillColor(); 
        H->SetFillColorAlpha(fillCol, FillAlpha());  
    }
    
    Color_t markerCol = Color(); 
    /// The marker color takes precedence over the general color 
    /// if both are set
    if (MarkerColor.isUserSet()) markerCol = MarkerColor(); 
    H->SetMarkerColorAlpha(markerCol, MarkerAlpha());
    H->SetMarkerStyle(MarkerStyle()); 
    H->SetMarkerSize(MarkerScale() * MarkerSize()); 

    Color_t lineCol = Color(); 
    /// The line color takes precedence over the general color 
    /// if both are set
    if (LineColor.isUserSet()) lineCol = LineColor(); 
    H->SetLineColorAlpha(lineCol, LineAlpha());
    H->SetLineStyle(LineStyle());
    H->SetLineWidth(LineWidth());

}
