#ifndef TRIGGER_PRESCALE_SERVICE_H
#define TRIGGER_PRESCALE_SERVICE_H
#include <MuonSagittaBias/SagittaBiasTree.h>

class TriggerPreScaleItem;

class TriggerPreScaleService {
public:
    static TriggerPreScaleService* getService();
    ~TriggerPreScaleService();

    /// Set the trigger items to consider for the prescaling
    void set_triggers(const std::vector<std::string>& trigs);

    /// Initializes the service by reading the prescale information
    /// for each considered trigger from the calib file --- Only important
    /// for simulation
    bool init_service(const std::string& calib_file);

    /// Returns the list of triggers to be considered for the prescaling
    const std::vector<std::string>& trigger_list() const;

    /// Returns the list of prescale items to be used for the unprescaling
    std::vector<std::shared_ptr<const TriggerPreScaleItem>> prescale_calibrators() const;

private:
    static TriggerPreScaleService* m_inst;

    /// Singleton implementation
    TriggerPreScaleService();
    TriggerPreScaleService(const TriggerPreScaleService&) = delete;
    TriggerPreScaleService(TriggerPreScaleService&&) = delete;
    TriggerPreScaleService& operator=(const TriggerPreScaleService&) = delete;
    TriggerPreScaleService& operator=(TriggerPreScaleService&&) = delete;

    std::vector<std::string> m_triggers{};
    std::vector<std::shared_ptr<TriggerPreScaleItem>> m_items{};
};

/// The trigger prescale can vary during a data-taking run
/// the RunPreScale contains the minimal information about a trigger
/// prescale, namely the run_number and the adjusted prescale itself
/// Later the particular lumi blocks are added. If the prescale is unique
/// for a given run, then the particular luminosity blocks are added.
class RunPreScale {
public:
    /// Standard constructor
    /// int: run number in which the prescale was set
    /// float: prescale value itself
    RunPreScale(int __run, float prescale);

    bool operator<(const RunPreScale& other) const;

    /// Returns the prescale
    float prescale() const;
    /// Return the run number
    int run_number() const;

    /// Restrict the prescale to a given lumi block range
    void add_lumi_block(int first_block, int last_block);

    /// Check whether the prescale setting is applied to the
    /// given run and lumi block
    bool in_range(int run_number, int lumi_block) const;

    /// Only one trigger prescale setting is set in this
    /// run. Use a short cut to evaluate in_range above
    void set_is_unique();

private:
    int m_run_number{-1};
    float m_prescale{1.};
    std::vector<std::pair<int, int>> m_lumi_blocks{};
    bool m_unique{false};
};

/// Class storing all the prescale information for a given trigger
class TriggerPreScaleItem {
public:
    /// Standard constructor
    /// trigger : name of the trigger item as in the TTree
    TriggerPreScaleItem(const std::string& trigger);
    /// Returns the name of the trigger item
    std::string trigger() const;
    /// Loads all prescaling information of the item from the calibration file
    void initialize(const std::shared_ptr<TFile>& file);

    /// Returns the prescale value of the trigger for a given
    /// run & lumi block. If the trigger was not active in the run
    /// FLT_MAX is returned
    float get_prescale(int run_number, int lumi_block) const;

    /// Unique identifier to match the prescale item with the
    /// requested trigger from the analysis
    const ObjectID& id() const;

private:
    /// Load the prescale information from the calibration file
    void load_prescales(const std::shared_ptr<TFile>& file);
    /// Load whether the trigger was actually active in the run
    void load_switch_info(const std::shared_ptr<TFile>& file);

    std::string m_item{};

    std::vector<RunPreScale> m_prescales;
    /// Map containing the information whether the given
    /// trigger was active in a certain run period
    std::map<unsigned int, bool> m_trig_switch;

    ObjectID m_obj_id{trigger()};
};

#endif
