#include "NtupleAnalysisUtils/NTAUTopLevelIncludes.h"

TH1D* thisIsDangerous(){
    /// obtain a `Plot` with a lifetime limited to this method 
    Plot<TH1D> localPlot(ConstructInPlace<TH1D>("H2","H2",200,0,20)); 
    return localPlot(); // here, we return the ROOT pointer from 'theCopy' - only valid within the method
}

TH1D* thisIsSafe(){
    /// obtain a `Plot` with a lifetime limited to this method 
    Plot<TH1D> localPlot(ConstructInPlace<TH1D>("H3","H3",200,0,20)); 
    return static_cast<TH1D*>(localPlot()->Clone()); // return a clone to ensure life-time beyond this method 
}

/// Note: This program is NOT intended to exit correctly.
/// It should segfault. 

int main (int, char**){

    Plot<TH1D> myHisto{ConstructInPlace<TH1D>("H1","H1",200,-2,2)};
    myHisto->FillRandom("gaus",100000); 
    SetAtlasStyle();

    TH1D* theRawROOTTH1 = myHisto();          /// Access raw pointer #1  
    TH1D* theSameOne = myHisto.getHisto();    /// Access raw pointer #2 - nicer syntax when working with pointers to Plot instances
    std::shared_ptr<TH1D> sharedPtr = myHisto.getSharedHisto();    /// Access shared pointer 

    // show that these three pointers are really all the same 
    std::cout << " Are they really the same? "<<theRawROOTTH1<<std::endl;
    std::cout << " Are they really the same? "<<theSameOne<<std::endl;
    std::cout << " Are they really the same? "<<sharedPtr.get()<<std::endl;

    /// Any changes we make are reflected when we access the Plot object - this is *not* a clone! 
    theRawROOTTH1->Fill(-1.75,10000.);

    TCanvas* c1 = new TCanvas("c1","c1",800,600);
    myHisto->Draw("PL");  /// now has a spike... with a big stat error ;-)
    c1->Draw(); 
    c1->SaveAs("Example_2_1_3.pdf");

    TH1D* validPointer = thisIsSafe(); 
    validPointer->Print(); // this is safe to do! 

    TH1D* invalidPointer = thisIsDangerous(); 
    std::cout << "About to do something stupid! Expect a segfault. "<<std::endl;
    /// this will access an invalid pointer 
    invalidPointer->Print(); 
    /// boom! We will never get here 
    std::cout << "If you read this, you are cheating"<<std::endl;

    return 0;
}