#include <AsgTools/AnaToolHandle.h>
//#include <MuonTPPostProcessing/DummyXAOD.h>
//#include <MuonTPPostProcessing/EventService.h>
//#include <MuonTPPostProcessing/LumiCalculator.h>
//#include <MuonTPPostProcessing/SmartAxisBinning.h>
//#include <MuonTPPostProcessing/TPHistoTemplater.h>
//#include <MuonTPPostProcessing/TreeVarReader.h>
//#include <MuonTPPostProcessing/Utils.h>
//#include <MuonTPPostProcessing/Weight.h>
#include <NtauHelpers/MuonTPMetaData.h>
#include <NtauHelpers/Utils.h>
#include <PMGAnalysisInterfaces/IPMGCrossSectionTool.h>
#include <TStopwatch.h>

#include <iostream>
namespace {
    constexpr const double PbToFb = 1000;
    std::function<size_t(size_t)> mod2_half = [](size_t x) { return (x - x % 2) / 2; };
    std::function<size_t(size_t)> mod2_half_p1 = [](size_t x) { return (x - x % 2) / 2 + x % 2; };
}  // namespace
namespace MuonTP {
    //###################################################################################################################
    //                                      MetaDataStore
    //###################################################################################################################
    MetaDataStore::MetaDataStore(unsigned int ID, bool IsData) : m_isData(IsData) {
        if (m_isData)
            m_runNumber = ID;
        else
            m_DSID = ID;
    }
    void MetaDataStore::SetPileUpLuminosity(double prwLumi) {
        if (m_Locked) {
            Warning("MetaDataStore::SetPileUpLuminosity()", "The storage is already locked");
            return;
        }
        m_prwLumi = prwLumi;
    }
    double MetaDataStore::prwLuminosity() const { return m_prwLumi; }
    void MetaDataStore::AddTotalEvents(Long64_t TotEvents) {
        if (m_Locked) {
            Warning("MetaDataStore::AddTotalEvents()", "The storage is already locked");
            return;
        }
        m_TotalEvents += TotEvents;
    }
    void MetaDataStore::AddProcessedEvents(Long64_t ProcessedEvents) {
        if (m_Locked) {
            Warning("MetaDataStore::AddTotalEvents()", "The storage is already locked");
            return;
        }
        m_ProcessedEvents += ProcessedEvents;
    }
    void MetaDataStore::AddSumW(double SumW) {
        if (m_isData || m_Locked) {
            Warning("MetaDataStore::AddSumW()", "The storage is already locked");
            return;
        }
        m_SumW += SumW;
    }
    void MetaDataStore::AddSumW2(double SumW2) {
        if (m_isData || m_Locked) {
            Warning("MetaDataStore::AddSumW2()", "The storage is already locked");
            return;
        }
        m_SumW2 += SumW2;
    }
    void MetaDataStore::SetRunNumber(unsigned int runNumber) {
        if (m_isData || m_Locked) {
            Warning("MetaDataStore::SetRunNumber()", "The storage is already locked");
            return;
        }
        m_runNumber = runNumber;
    }
    void MetaDataStore::SetxSectionInfo(double xSection, double kFactor, double Efficiency, const std::string& smp_name) {
        if (m_isData || m_Locked) {
            Warning("MetaDataStore::SetxSectionInfo()", "The storage is already locked");
            return;
        }
        m_xSection = xSection;
        m_kFactor = kFactor;
        m_Efficiency = Efficiency;
        m_name = smp_name;
    }
    void MetaDataStore::InsertTotalLumiBlock(unsigned int BCID) {
        if (!m_isData || m_Locked) {
            Warning("MetaDataStore::InsertTotalLumiBlock()", "The storage is already locked");
            return;
        }
        m_TotalLumiBlocks.push_back(BCID);
    }
    void MetaDataStore::InsertProcessedLumiBlock(unsigned int BCID) {
        if (!m_isData || m_Locked) {
            Warning("MetaDataStore::InsertProcessedLumiBlock()", "The storage is already locked");
            return;
        }
        m_ProcessedLumiBlocks.push_back(BCID);
    }
    void MetaDataStore::Lock() {
        m_Locked = true;
        ClearFromDuplicates(m_TotalLumiBlocks);
        ClearFromDuplicates(m_ProcessedLumiBlocks);
        std::sort(m_TotalLumiBlocks.begin(), m_TotalLumiBlocks.end());
        std::sort(m_ProcessedLumiBlocks.begin(), m_ProcessedLumiBlocks.end());
        /// Multiply the cross-section tables by PbToFb
        m_finalWeight = !m_isData ? (xSection() * kFactor() * FilterEfficiency() * PbToFb) / SumW() : 1.;
    }
    void MetaDataStore::ReserveSpaceTotalLumi(unsigned int N) { m_TotalLumiBlocks.reserve(N + m_TotalLumiBlocks.capacity()); }
    void MetaDataStore::ReserveSpaceProcessedLumi(unsigned int N) { m_ProcessedLumiBlocks.reserve(N + m_ProcessedLumiBlocks.capacity()); }
    unsigned int MetaDataStore::runNumber() const { return m_runNumber; }
    unsigned int MetaDataStore::DSID() const { return m_DSID; }
    double MetaDataStore::SumW() const { return m_SumW; }
    double MetaDataStore::SumW2() const { return m_SumW2; }
    Long64_t MetaDataStore::TotalEvents() const { return m_TotalEvents; }
    Long64_t MetaDataStore::ProcessedEvents() const { return m_ProcessedEvents; }
    double MetaDataStore::xSection() const { return m_xSection; }
    double MetaDataStore::kFactor() const { return m_kFactor; }
    double MetaDataStore::FilterEfficiency() const { return m_Efficiency; }
    const std::vector<unsigned int>& MetaDataStore::TotalLumiBlocks() const {
        if (!m_Locked) { Warning("MetaDataStore::TotalLumiBlocks()", "Store is not locked"); }
        return m_TotalLumiBlocks;
    }
    const std::vector<unsigned int>& MetaDataStore::ProcessedLumiBlocks() const {
        if (!m_Locked) { Warning("MetaDataStore::ProcessedLumiBlocks()", "Store is not locked"); }
        return m_ProcessedLumiBlocks;
    }
    bool MetaDataStore::isData() const { return m_isData; }
    bool MetaDataStore::isDerivedAOD() const { return m_isDerivedAOD; }
    void MetaDataStore::setDerivedAOD(bool B) { m_isDerivedAOD = B; }
    std::string MetaDataStore::SampleName() const { return m_name; }

    double MetaDataStore::Normalization() const { return 1. / SumW(); }
    double MetaDataStore::finalWeight() const { return m_finalWeight; }
    std::string MetaDataStore::WeightName() const { return m_weight_name; }
    unsigned int MetaDataStore::LheVariation() const { return m_lhe; }
    void MetaDataStore::SetLHEIndex(unsigned int lhe_idx) {
        if (m_Locked) {
            Warning("MetaDataStore::SetLHEIndex()", "Store is locked");
            return;
        }
        m_lhe = lhe_idx;
    }
    void MetaDataStore::SetLHEWeightName(const std::string& w_name) {
        if (m_Locked) {
            Warning("MetaDataStore::SetLHEWeightName()", "Store is locked");
            return;
        }
        m_weight_name = w_name;
    }
    //###################################################################################################################
    //                                      MonteCarloPeriodHandler
    //###################################################################################################################
    MonteCarloPeriodHandler::MonteCarloPeriodHandler(unsigned int DSID, unsigned int lhe_index) :
        m_periods(), m_summary_period(std::make_shared<MetaDataStore>(DSID, false)), m_locked(false) {
        m_periods.push_back(m_summary_period);
        m_summary_period->SetRunNumber(-1);
        m_summary_period->SetLHEIndex(lhe_index);
    }
    unsigned int MonteCarloPeriodHandler::LheVariation() const { return m_summary_period->LheVariation(); }
    unsigned int MonteCarloPeriodHandler::DSID() const { return m_summary_period->DSID(); }
    std::shared_ptr<MetaDataStore> MonteCarloPeriodHandler::getHandler(unsigned int run) const {
        if (run == m_summary_period->runNumber()) return m_summary_period;
        for (auto period : m_periods) {
            if (period->runNumber() == run) return period;
        }
        // return by default the summary period
        return m_summary_period;
    }
    std::shared_ptr<MetaDataStore> MonteCarloPeriodHandler::insertHandler(unsigned int run) {
        std::shared_ptr<MetaDataStore> handler = getHandler(run);
        // no handler has been assigned yet
        if (handler == m_summary_period && m_summary_period->runNumber() != run) {
            handler = std::make_shared<MetaDataStore>(DSID(), false);
            handler->SetRunNumber(run);
            handler->SetLHEIndex(LheVariation());
            m_periods.push_back(handler);
        }
        return handler;
    }

    void MonteCarloPeriodHandler::AddTotalEvents(Long64_t TotEvents, unsigned int run) {
        insertHandler(run)->AddTotalEvents(TotEvents);
        if (run != m_summary_period->runNumber()) m_summary_period->AddTotalEvents(TotEvents);
    }
    void MonteCarloPeriodHandler::AddProcessedEvents(Long64_t ProcessedEvents, unsigned int run) {
        insertHandler(run)->AddProcessedEvents(ProcessedEvents);
        if (run != m_summary_period->runNumber()) m_summary_period->AddProcessedEvents(ProcessedEvents);
    }
    void MonteCarloPeriodHandler::AddSumW(double SumW, unsigned int run) {
        insertHandler(run)->AddSumW(SumW);
        if (run != m_summary_period->runNumber()) m_summary_period->AddSumW(SumW);
    }
    void MonteCarloPeriodHandler::AddSumW2(double SumW2, unsigned int run) {
        insertHandler(run)->AddSumW2(SumW2);
        if (run != m_summary_period->runNumber()) m_summary_period->AddSumW2(SumW2);
    }
    void MonteCarloPeriodHandler::SetxSectionInfo(double xSection, double kFactor, double Efficiency, const std::string& smp_name) {
        insertHandler(-1)->SetxSectionInfo(xSection, kFactor, Efficiency, smp_name);
    }
    void MonteCarloPeriodHandler::SetLHEWeightName(const std::string& w_name) { insertHandler(-1)->SetLHEWeightName(w_name); }
    std::vector<unsigned int> MonteCarloPeriodHandler::getMCcampaigns() const {
        std::vector<unsigned int> campaigns;
        campaigns.reserve(m_periods.size() - 1);
        for (const auto& P : m_periods) {
            if (P != m_summary_period) campaigns.push_back(P->runNumber());
        }
        std::sort(campaigns.begin(), campaigns.end());
        return campaigns;
    }

    void MonteCarloPeriodHandler::Lock() {
        m_locked = true;
        for (auto& period : m_periods) period->Lock();
    }
    void MonteCarloPeriodHandler::SetPileUpLuminosity(double prwLumi, unsigned int run) {
        if (insertHandler(run)->prwLuminosity() > 0) return;
        insertHandler(run)->SetPileUpLuminosity(prwLumi);
        // Add the luminosity to the summary period
        if (run != m_summary_period->runNumber()) { m_summary_period->SetPileUpLuminosity(m_summary_period->prwLuminosity() + prwLumi); }
    }
    double MonteCarloPeriodHandler::prwTotalLuminosity() const { return m_summary_period->prwLuminosity(); }

    //###################################################################################################################
    //                                      NormalizationDataBase
    //###################################################################################################################
    NormalizationDataBase* NormalizationDataBase::m_Inst = nullptr;
    NormalizationDataBase* NormalizationDataBase::getDataBase() {
        if (!m_Inst) m_Inst = new NormalizationDataBase();
        return m_Inst;
    }
    NormalizationDataBase::NormalizationDataBase() :
        m_DB(), m_mc_DB(), m_ActMeta(nullptr), m_ActMCMeta(nullptr), m_init(false), m_Weighter(nullptr), m_xSecLoaded(false) {}
    NormalizationDataBase::DSIDStatus NormalizationDataBase::GetDSIDStatus(unsigned int DSID, unsigned int lheVar) {
        if (m_ActMeta && DSID == m_ActMeta->DSID() && lheVar == m_ActMCMeta->LheVariation()) {
            if (!m_ActMeta->isData())
                return NormalizationDataBase::DSIDStatus::Present;
            else {
                Warning("NormalizationDataBase::GetDSIDStatus()", "The current entry is no MC-metadata");
                return NormalizationDataBase::DSIDStatus::Failed;
            }
        }
        m_ActMCMeta = std::shared_ptr<MonteCarloPeriodHandler>();
        for (const auto& Store : m_mc_DB) {
            if (Store->DSID() == DSID && Store->LheVariation() == lheVar) {
                m_ActMeta = Store->getHandler(-1);
                m_ActMCMeta = Store;
                return NormalizationDataBase::DSIDStatus::Updated;
            }
        }
        return NormalizationDataBase::DSIDStatus::Failed;
    }
    NormalizationDataBase::DSIDStatus NormalizationDataBase::GetRunStatus(unsigned int runNumber) {
        if (m_ActMeta && runNumber == m_ActMeta->runNumber()) {
            if (m_ActMeta->isData())
                return NormalizationDataBase::DSIDStatus::Present;
            else {
                Warning("NormalizationDataBase::GetDSIDStatus()", "The current entry is no run-metadata");
                return NormalizationDataBase::DSIDStatus::Failed;
            }
        }
        m_ActMeta = nullptr;
        for (const auto& Meta : m_DB) {
            if (Meta->runNumber() == runNumber) {
                m_ActMeta = Meta;
                return NormalizationDataBase::DSIDStatus::Updated;
            }
        }
        return NormalizationDataBase::DSIDStatus::Failed;
    }
    bool NormalizationDataBase::init(const std::vector<std::string>& FileList) {
        if (m_init) return true;
        std::vector<std::string> to_check = FileList;
        ClearFromDuplicates(to_check);
        unsigned int NFiles = to_check.size(), readN = 0, Prompt = NFiles / 10;
        Info("NormalizationDataBase::init()", "Load the metadata from " + std::to_string(NFiles) + " input files.");
        TStopwatch tsw;
        tsw.Start();
        double t2 = 0;
        for (const auto File : to_check) {
            std::shared_ptr<TFile> F = PlotUtils::Open(File);
            if (!F)
                return false;
            else
                Info("NormalizationDataBase::init()", "Open file: " + File);
            TTree* MetaDataTree = 0;
            F->GetObject("MetaDataTree", MetaDataTree);
            if (!ReadTree(MetaDataTree)) return false;
            ++readN;
            if (NFiles > 10 && readN % Prompt == 0) {
                t2 = tsw.RealTime();
                Info("NormalizationDataBase::init()", "Successully read in file " + std::to_string(readN) + "/" + std::to_string(NFiles) +
                                                          ". Needed time " + TimeHMS(t2) + ". Will be finished in " +
                                                          TimeHMS(t2 * ((float)NFiles / (float)readN - 1)) + ".");
                tsw.Continue();
            }
        }
        if (m_DB.empty() && m_mc_DB.empty()) {
            Error("NormalizationDataBase::init()", "No metadata was found. Please check carefully the logs of your analysis jobs");
            return false;
        }
        if (isData()) LockStores();
        std::sort(m_mc_DB.begin(), m_mc_DB.end(),
                  [](const std::shared_ptr<MonteCarloPeriodHandler>& a, const std::shared_ptr<MonteCarloPeriodHandler>& b) {
                      if (a->DSID() != b->DSID()) return a->DSID() < b->DSID();
                      return a->LheVariation() < b->LheVariation();
                  });
        PromptMetaDataTree();
        // m_Weighter = WeightHandler::GetInstance(isData());
        m_init = true;
        m_ActMeta = nullptr;
        return true;
    }
    bool NormalizationDataBase::ReadTree(TTree* t) {
        if (!t) {
            Error("NormalizationDataBase::ReadTree()", "No MetaDataTree is given");
            return false;
        }
        Long64_t nEntries = t->GetEntries();
        if (nEntries == 0) {
            Warning("NormalizationDataBase::ReadTree()", "The metadata tree does not contain any entry");
            delete t;
            return true;
        }
        bool Data;
        if (t->SetBranchAddress("isData", &Data) < 0) return false;
        bool isDataInFirst = false;
        if (m_DB.empty()) {
            t->GetEntry(0);
            isDataInFirst = Data;
        } else
            isDataInFirst = isData();
        for (long int e = 0; t->GetEntry(e) && (e < nEntries); ++e) {
            if (isDataInFirst != Data) {
                Error("NormalizationDataBase::ReadTree()",
                      "It seems that the tree contains MC and data at the same time. Your input is invalid");
                delete t;
                return false;
            }
        }
        if (!Data) return ReadMCTree(t);
        return ReadDataTree(t);
    }
    bool NormalizationDataBase::ReadMCTree(TTree* t) {
        double SumWeights{0.}, SumWeights2{0.}, prwLumi{0.};
        int McChannel{-1};
        unsigned int runNumber{0}, lhe_id{0};
        Long64_t TotalEvents{0}, ProcessedEv{0};
        bool isDerivedAOD(false);
        std::string* weight_name{nullptr};
        bool has_w_name{true};

        if (t->SetBranchAddress("TotalSumW", &SumWeights) < 0) return false;
        if (t->SetBranchAddress("TotalSumW2", &SumWeights2) < 0) return false;
        if (t->SetBranchAddress("mcChannelNumber", &McChannel) < 0) return false;
        if (t->SetBranchAddress("TotalEvents", &TotalEvents) < 0) return false;
        if (t->SetBranchAddress("ProcessedEvents", &ProcessedEv) < 0) return false;
        if (t->SetBranchAddress("runNumber", &runNumber) < 0) return false;
        if (t->SetBranchAddress("isDerivedAOD", &isDerivedAOD) < 0) return false;

        if (t->SetBranchAddress("LheId", &lhe_id) < 0) { lhe_id = 0; }
        if (t->SetBranchAddress("LheWeightName", &weight_name) < 0) { has_w_name = false; }

        bool hasPRWLumi = t->GetBranch("prwLuminosity") != nullptr;
        if (hasPRWLumi && t->SetBranchAddress("prwLuminosity", &prwLumi) < 0) return false;

        Long64_t nEntries = t->GetEntries();
        for (long int e = 0; t->GetEntry(e) && (e < nEntries); ++e) {
            if (GetDSIDStatus(McChannel, lhe_id) == NormalizationDataBase::DSIDStatus::Failed) {
                m_mc_DB.emplace_back(std::make_shared<MonteCarloPeriodHandler>(McChannel, lhe_id));
                m_ActMeta = nullptr;
                if (GetDSIDStatus(McChannel, lhe_id) == NormalizationDataBase::DSIDStatus::Failed) {
                    Error("NormalizationDataBase::ReadMCTree()", "Something went wrong during storing the information");
                    return false;
                }
                if (has_w_name) m_ActMCMeta->SetLHEWeightName(*weight_name);
            }
            m_ActMCMeta->AddSumW(SumWeights, runNumber);
            m_ActMCMeta->AddSumW2(SumWeights2, runNumber);
            m_ActMCMeta->AddProcessedEvents(ProcessedEv, runNumber);
            m_ActMCMeta->AddTotalEvents(TotalEvents, runNumber);
            if (hasPRWLumi) m_ActMCMeta->SetPileUpLuminosity(prwLumi, runNumber);
            m_ActMeta->setDerivedAOD(isDerivedAOD);
        }
        delete t;
        return true;
    }
    bool NormalizationDataBase::ReadDataTree(TTree* t) {
        unsigned int runNumber(0);
        Long64_t TotalEvents(0), ProcessedEvents(0);
        std::set<unsigned int>* TotalLumiBlocks = nullptr;
        std::set<unsigned int>* ProcessedLumiBlocks = nullptr;
        bool isDerivedAOD(false);
        if (t->SetBranchAddress("TotalLumiBlocks", &TotalLumiBlocks) < 0) return false;
        if (t->SetBranchAddress("ProcessedLumiBlocks", &ProcessedLumiBlocks) < 0) return false;
        if (t->SetBranchAddress("ProcessedEvents", &ProcessedEvents) < 0) return false;
        if (t->SetBranchAddress("TotalEvents", &TotalEvents) < 0) return false;
        if (t->SetBranchAddress("runNumber", &runNumber) < 0) return false;
        if (t->SetBranchAddress("isDerivedAOD", &isDerivedAOD) < 0) return false;
        Long64_t nEntries = t->GetEntries();
        for (long int e = 0; t->GetEntry(e) && (e < nEntries); ++e) {
            if (GetRunStatus(runNumber) == NormalizationDataBase::DSIDStatus::Failed) {
                m_DB.emplace_back(std::make_shared<MetaDataStore>(runNumber, true));
                m_ActMeta = nullptr;
                if (GetRunStatus(runNumber) == NormalizationDataBase::DSIDStatus::Failed) {
                    Error("NormalizationDataBase::ReadDataTree()", "Something went wrong during storing the information");
                    return false;
                }
            }
            m_ActMeta->AddProcessedEvents(ProcessedEvents);
            m_ActMeta->AddTotalEvents(TotalEvents);
            m_ActMeta->ReserveSpaceTotalLumi(TotalLumiBlocks->size());
            m_ActMeta->ReserveSpaceProcessedLumi(ProcessedLumiBlocks->size());
            for (auto& BCID : *ProcessedLumiBlocks) m_ActMeta->InsertProcessedLumiBlock(BCID);
            for (auto& BCID : *TotalLumiBlocks) m_ActMeta->InsertTotalLumiBlock(BCID);
            m_ActMeta->setDerivedAOD(isDerivedAOD);
        }
        delete t;
        return true;
    }
    double NormalizationDataBase::getNormTimesXsec(unsigned int DSID, unsigned int lheVar) {
        NormalizationDataBase::DSIDStatus Status = GetDSIDStatus(DSID, lheVar);
        if (Status == NormalizationDataBase::DSIDStatus::Present || Status == NormalizationDataBase::DSIDStatus::Updated)
            return m_ActMeta->finalWeight();
        Error("NormalizationDataBase::getNormTimesXsec()", "MC sample not found " + std::to_string(DSID) + ". Return -1.");
        return -1.;
    }
    double NormalizationDataBase::getNormalization(unsigned int DSID, unsigned int lheVar) {
        NormalizationDataBase::DSIDStatus Status = GetDSIDStatus(DSID, lheVar);
        if (Status == NormalizationDataBase::DSIDStatus::Failed) {
            Error("NormalizationDataBase::getNormalization()", "MC sample not found " + std::to_string(DSID) + ". Return 0.");
            return 0.;
        }
        return m_ActMeta->Normalization();
    }
    const std::vector<unsigned int>& NormalizationDataBase::GetTotalLumiBlocks(unsigned int runNumber) {
        static std::vector<unsigned int> DummyLumis;
        if (GetRunStatus(runNumber) != NormalizationDataBase::DSIDStatus::Failed) return m_ActMeta->TotalLumiBlocks();
        Error("NormalizationDataBase::GetTotalLumiBlocks()", "No run-meta data found");
        return DummyLumis;
    }
    const std::vector<unsigned int>& NormalizationDataBase::GetProcessedLumiBlocks(unsigned int runNumber) {
        static std::vector<unsigned int> DummyLumis;
        if (GetRunStatus(runNumber) != NormalizationDataBase::DSIDStatus::Failed) return m_ActMeta->ProcessedLumiBlocks();
        Error("NormalizationDataBase::GetProcessedLumiBlocks()", "No run-meta data found");
        return DummyLumis;
    }
    std::vector<unsigned int> NormalizationDataBase::GetListOfMCSamples() const {
        std::vector<unsigned int> DSIDs;
        if (isData()) return DSIDs;
        for (auto& Meta : m_mc_DB) { DSIDs.push_back(Meta->DSID()); }
        ClearFromDuplicates(DSIDs);
        std::sort(DSIDs.begin(), DSIDs.end());
        return DSIDs;
    }
    Long64_t NormalizationDataBase::GetTotalEvents(unsigned int ID) {
        if (isData() && GetRunStatus(ID) != NormalizationDataBase::DSIDStatus::Failed)
            return m_ActMeta->TotalEvents();
        else if (!isData() && GetDSIDStatus(ID, 0) != NormalizationDataBase::DSIDStatus::Failed)
            return m_ActMeta->TotalEvents();
        Error("NormalizationDataBase::GetTotalEvents()", "Unkown identifier " + std::to_string(ID));
        return 0;
    }
    Long64_t NormalizationDataBase::GetProcessedEvents(unsigned int ID) {
        if (isData() && GetRunStatus(ID) != NormalizationDataBase::DSIDStatus::Failed)
            return m_ActMeta->ProcessedEvents();
        else if (!isData() && GetDSIDStatus(ID, 0) != NormalizationDataBase::DSIDStatus::Failed)
            return m_ActMeta->ProcessedEvents();
        Error("NormalizationDataBase::GetProcessedEvents()", "Unkown identifier " + std::to_string(ID));
        return 0;
    }
    double NormalizationDataBase::GetSumW(unsigned int DSID, unsigned int lhe_var) {
        if (GetDSIDStatus(DSID, lhe_var) == NormalizationDataBase::DSIDStatus::Failed) {
            Error("NormalizationDataBase::GetSumW()", "Unkown DSID " + std::to_string(DSID) + ".");
            return 0;
        }
        return m_ActMeta->SumW();
    }
    double NormalizationDataBase::GetSumW2(unsigned int DSID, unsigned int lhe_var) {
        if (GetDSIDStatus(DSID, lhe_var) == NormalizationDataBase::DSIDStatus::Failed) {
            Error("NormalizationDataBase::GetSumW2()", "Unkown DSID " + std::to_string(DSID) + ".");
            return 0;
        }
        return m_ActMeta->SumW2();
    }
    std::string NormalizationDataBase::GetSampleName(unsigned int DSID) {
        if (GetDSIDStatus(DSID, 0) == NormalizationDataBase::DSIDStatus::Failed) {
            Error("NormalizationDataBase::GetSampleName()", "Unkown DSID " + std::to_string(DSID) + ".");
            return "UNKOWN";
        }
        return m_ActMeta->SampleName();
    }

    unsigned int NormalizationDataBase::GetNumMCsamples() const { return GetListOfMCSamples().size(); }
    double NormalizationDataBase::GetxSection(unsigned int DSID) {
        if (GetDSIDStatus(DSID, 0) == NormalizationDataBase::DSIDStatus::Failed) {
            Error("NormalizationDataBase::GetxSection()", "Unkown DSID " + std::to_string(DSID) + ".");
            return 0;
        }
        return m_ActMeta->xSection();
    }
    double NormalizationDataBase::GetFilterEfficiency(unsigned int DSID) {
        if (GetDSIDStatus(DSID, 0) == NormalizationDataBase::DSIDStatus::Failed) {
            Error("NormalizationDataBase::GetFilterEfficiency()", "Unkown DSID " + std::to_string(DSID) + ".");
            return 0;
        }
        return m_ActMeta->FilterEfficiency();
    }

    double NormalizationDataBase::GetkFactor(unsigned int DSID) {
        if (GetDSIDStatus(DSID, 0) == NormalizationDataBase::DSIDStatus::Failed) {
            Error("NormalizationDataBase::GetkFactor()", "Unkown DSID " + std::to_string(DSID) + ".");
            return 0;
        }
        return m_ActMeta->kFactor();
    }
    void NormalizationDataBase::PromptMCMetaDataTree() {
        if (isData()) return;
        size_t Width_Ev{0}, Width_Pev{0}, Width_SumW{0}, Width_SumW2{0}, Width_xSec{0}, Width_kFac{0}, Width_Eff{0}, Width_Name{0};

        for (const std::shared_ptr<MonteCarloPeriodHandler>& mc_handler : m_mc_DB) {
            const std::shared_ptr<MetaDataStore> meta = mc_handler->getHandler();

            std::string TotEvents = Form("%llu", meta->TotalEvents());
            std::string ProcEvents = Form("%llu", meta->ProcessedEvents());
            std::string SumW = Form("%.4f", meta->SumW());
            std::string SumW2 = Form("%.4f", meta->SumW2());
            std::string xSec = Form("%.4f", meta->xSection());
            std::string kFac = Form("%.4f", meta->kFactor());
            std::string Eff = Form("%.4f", meta->FilterEfficiency());
            std::string SmpName = meta->SampleName();

            Width_Ev = std::max(Width_Ev, TotEvents.size());
            Width_Pev = std::max(Width_Pev, ProcEvents.size());
            Width_SumW = std::max(Width_SumW, SumW.size());
            Width_SumW2 = std::max(Width_SumW2, SumW2.size());
            Width_xSec = std::max(Width_xSec, xSec.size());
            Width_kFac = std::max(Width_kFac, kFac.size());
            Width_xSec = std::max(Width_xSec, xSec.size());
            Width_kFac = std::max(Width_kFac, kFac.size());
            Width_Eff = std::max(Width_Eff, Eff.size());
            Width_Name = std::max(Width_Name, SmpName.size());
        }
        std::cout << WhiteSpaces(80, "#") << std::endl;

        Info("NormalizationDataBase::PromptMCMetaDataTree()", WhiteSpaces(20) + "Prompt Monte Carlo meta data");
        std::cout << WhiteSpaces(80, "#") << std::endl;

        std::cout << "  DSID  | ";
        std::cout << WhiteSpaces(mod2_half_p1(Width_Name - 4)) << "Name" << WhiteSpaces(mod2_half(Width_Name - 4)) << " | ";
        std::cout << WhiteSpaces(mod2_half_p1(Width_Ev - 9)) << "TotEvents" << WhiteSpaces(mod2_half(Width_Ev - 9)) << " | ";
        std::cout << WhiteSpaces(mod2_half_p1(Width_Pev - 6)) << "ProcEv" << WhiteSpaces(mod2_half(Width_Pev - 6)) << " | ";
        std::cout << WhiteSpaces(mod2_half_p1(Width_SumW - 4)) << "SumW" << WhiteSpaces(mod2_half(Width_SumW - 4)) << " | ";
        std::cout << WhiteSpaces(mod2_half_p1(Width_SumW2 - 5)) << "SumW2" << WhiteSpaces(mod2_half(Width_SumW2 - 5)) << " | ";
        std::cout << WhiteSpaces(mod2_half_p1(Width_xSec - 8)) << "xSection" << WhiteSpaces(mod2_half(Width_xSec - 8)) << " | ";
        std::cout << WhiteSpaces(mod2_half_p1(Width_kFac - 8)) << "k-Factor" << WhiteSpaces(mod2_half(Width_kFac - 8)) << " | ";
        std::cout << WhiteSpaces(mod2_half_p1(Width_Eff - 9)) << "FilterEff" << WhiteSpaces(mod2_half(Width_Eff - 9)) << " | ";
        std::cout << "FilterEffxSecTimesEff" << std::endl;

        for (const std::shared_ptr<MonteCarloPeriodHandler>& mc_handler : m_mc_DB) {
            const std::shared_ptr<MetaDataStore> meta = mc_handler->getHandler();
            std::string TotEvents = Form("%llu", meta->TotalEvents());
            std::string ProcEvents = Form("%llu", meta->ProcessedEvents());
            std::string SumW = Form("%.4f", meta->SumW());
            std::string SumW2 = Form("%.4f", meta->SumW2());
            std::string xSec = Form("%.4f", meta->xSection());
            std::string kFac = Form("%.4f", meta->kFactor());
            std::string Eff = Form("%.4f", meta->FilterEfficiency());
            std::string SmpName = meta->SampleName();

            std::string xSecTimes = Form("%.4f", meta->xSection() * meta->kFactor() * meta->FilterEfficiency());

            std::cout << " " << meta->DSID() << " | ";
            std::cout << SmpName << WhiteSpaces(Width_Name - SmpName.size()) << " | ";
            std::cout << WhiteSpaces(mod2_half_p1(Width_Ev - TotEvents.size())) << TotEvents
                      << WhiteSpaces(mod2_half(Width_Ev - TotEvents.size())) << " | ";
            std::cout << WhiteSpaces(mod2_half_p1(Width_Pev - ProcEvents.size())) << ProcEvents
                      << WhiteSpaces(mod2_half(Width_Pev - ProcEvents.size())) << " | ";
            std::cout << WhiteSpaces(mod2_half_p1(Width_SumW - SumW.size())) << SumW << WhiteSpaces(mod2_half(Width_SumW - SumW.size()))
                      << " | ";
            std::cout << WhiteSpaces(mod2_half_p1(Width_SumW2 - SumW2.size())) << SumW2
                      << WhiteSpaces(mod2_half(Width_SumW2 - SumW2.size())) << " | ";
            std::cout << WhiteSpaces(mod2_half_p1(Width_xSec - xSec.size())) << xSec << WhiteSpaces(mod2_half(Width_xSec - xSec.size()))
                      << " | ";
            std::cout << WhiteSpaces(mod2_half_p1(Width_kFac - kFac.size())) << kFac << WhiteSpaces(mod2_half(Width_kFac - kFac.size()))
                      << " | ";
            std::cout << WhiteSpaces(mod2_half_p1(Width_Eff - Eff.size())) << Eff << WhiteSpaces(mod2_half(Width_Eff - Eff.size()))
                      << " | ";
            std::cout << xSecTimes << std::endl;
        }
    }
    void NormalizationDataBase::PromptRunMetaDataTree() {
        if (!isData()) return;
        std::vector<unsigned int> RunNumbers = GetRunNumbers();
        unsigned int Width_Ev(9), Width_Pev(6), Width_Run(3);
        for (const auto& R : RunNumbers) {
            std::string TotEvents = Form("%llu", GetTotalEvents(R));
            std::string ProcEvents = Form("%llu", GetProcessedEvents(R));
            std::string Run = Form("%u", R);
            if (TotEvents.size() > Width_Ev) Width_Ev = TotEvents.size();
            if (ProcEvents.size() > Width_Pev) Width_Pev = ProcEvents.size();
            if (Run.size() > Width_Run) Width_Run = Run.size();
        }
        std::cout << WhiteSpaces(80, "#") << std::endl;
        ;
        Info("NormalizationDataBase::PromptRunMetaDataTree()", WhiteSpaces(20) + "Prompt run meta data");
        std::cout << WhiteSpaces(80, "#") << std::endl;
        ;
        std::cout << " Run" << WhiteSpaces(Width_Run - 3) << " | TotEvents" << WhiteSpaces(Width_Ev - 9) << " | ProcEv"
                  << WhiteSpaces(Width_Pev - 6) << " | " << std::endl;
        for (const auto& R : RunNumbers) {
            std::string TotEvents = Form("%llu", GetTotalEvents(R));
            std::string ProcEvents = Form("%llu", GetProcessedEvents(R));
            std::string Run = Form("%u", R);
            std::cout << " " << Run << WhiteSpaces(Width_Run - Run.size()) << " | " << TotEvents << WhiteSpaces(Width_Ev - TotEvents.size())
                      << " | " << ProcEvents << WhiteSpaces(Width_Pev - ProcEvents.size()) << " | " << std::endl;
        }
    }
    void NormalizationDataBase::PromptMetaDataTree() {
        std::cout << WhiteSpaces(80, "#") << std::endl;
        PromptMCMetaDataTree();
        PromptRunMetaDataTree();
        std::cout << WhiteSpaces(80, "#") << std::endl;
    }
    bool NormalizationDataBase::isData() const {
        if (m_DB.empty() && m_mc_DB.empty()) {
            Warning("NormalizationDataBase::isData()", "The DB is uninitialized");
            return false;
        }
        return m_mc_DB.empty();
    }
    bool NormalizationDataBase::isInitialized() const { return m_init; }
    NormalizationDataBase::~NormalizationDataBase() {
        m_Inst = nullptr;
        // delete DummyXAOD::getDummyXAOD();
        // delete LumiCalculator::getCalculator();
        // delete WeightHandler::GetInstance();
        // delete TPHistoTemplater::GetInstance();
        // delete TreeReaderStorage::Instance();
        // delete EventService::getService();
        // delete SmartAxisBinMgr::get();
    }
    std::vector<unsigned int> NormalizationDataBase::GetRunNumbers() const {
        std::vector<unsigned int> runs;
        if (!isData()) return runs;
        for (const auto Meta : m_DB) runs.push_back(Meta->runNumber());
        std::sort(runs.begin(), runs.end());
        return runs;
    }
    unsigned int NormalizationDataBase::GetNumLheVariations(unsigned int dsid) const {
        /// Use the fact that the
        std::vector<std::shared_ptr<MonteCarloPeriodHandler>>::const_reverse_iterator itr = std::find_if(
            m_mc_DB.rbegin(), m_mc_DB.rend(), [&dsid](const std::shared_ptr<MonteCarloPeriodHandler>& ptr) { return dsid == ptr->DSID(); });
        return (itr != m_mc_DB.rend()) ? (*itr)->LheVariation() + 1 : 0;
    }
    void NormalizationDataBase::LoadxSections(const std::string& xSecDir) {
        if (!m_init) {
            Warning("NormalizationDataBase::LoadxSections()", "No metadata is read in thus far");
            return;
        }
        if (isData() || m_xSecLoaded) return;
        Info("NormalizationDataBase::LoadxSections()", "Load xSections from the DB located at " + xSecDir);
        asg::AnaToolHandle<PMGTools::IPMGCrossSectionTool> pmg_DB("PMGTools::PMGCrossSectionTool/BkgXsecTool");
        pmg_DB.retrieve().ignore();
        pmg_DB->readInfosFromFiles(GetPathResolvedFileList(std::vector<std::string>{xSecDir}));
        for (auto& Meta : m_mc_DB) {
            Meta->SetxSectionInfo(pmg_DB->getAMIXsection(Meta->DSID()), pmg_DB->getKfactor(Meta->DSID()),
                                  pmg_DB->getFilterEff(Meta->DSID()), pmg_DB->getSampleName(Meta->DSID()));
        }
        m_xSecLoaded = true;
        LockStores();
        PromptMCMetaDataTree();
    }
    void NormalizationDataBase::LockStores() {
        for (auto& Meta : m_DB) { Meta->Lock(); }
        for (auto& Meta : m_mc_DB) { Meta->Lock(); }
    }
    std::shared_ptr<MonteCarloPeriodHandler> NormalizationDataBase::getPeriodHandler(unsigned int DSID, unsigned int lhe_var) const {
        for (auto& Meta : m_mc_DB) {
            if (Meta->DSID() == DSID && Meta->LheVariation() == lhe_var) return Meta;
        }
        return nullptr;
    }
    void NormalizationDataBase::resetDataBase() { delete NormalizationDataBase::getDataBase(); }
}  // namespace MuonTP
