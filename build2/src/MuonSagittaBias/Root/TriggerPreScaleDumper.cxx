#include <MuonSagittaBias/TriggerPreScaleDumper.h>
#include <NtauHelpers/LumiCalculator.h>
PreScaleBlock::PreScaleBlock(const unsigned int __run, const unsigned int __lumi, const float __scale) :
    run_number{__run}, lumi_block{__lumi}, pre_scale{__scale} {}

bool PreScaleBlock::operator<(const PreScaleBlock& b) const {
    if (run_number != b.run_number) return run_number < b.run_number;
    if (is_augmented != b.is_augmented) return is_augmented > b.is_augmented;
    if (lumi_block != b.lumi_block) return lumi_block < b.lumi_block;
    return pre_scale > b.pre_scale;
}

bool PreScaleBlock::operator!() const { return pre_scale <= 1.; }
std::ostream& operator<<(std::ostream& os, const PreScaleBlock& block) {
    os << std::setw(8) << block.run_number << std::setw(10) << block.lumi_block << std::setw(10) << Form(" %.2f", block.pre_scale)
       << std::endl;
    return os;
}
std::string TrigPreScaleCache::trigger() const { return m_item; }
TrigPreScaleCache::TrigPreScaleCache(const std::string& trig_item) : m_item{trig_item} {}
void TrigPreScaleCache::fill(TriggerAnalyzer& t) {
    /// At the very first event determine the position of the trigger
    /// in the vector
    if (!m_initialized) {
        bool throw_except{true};
        for (; m_item_pos < t.triggers.size(); ++m_item_pos) {
            if (t.triggers[m_item_pos].trigger() == m_item) {
                throw_except = false;
                break;
            }
        }
        if (throw_except) { throw std::runtime_error("The trigger " + m_item + " is not saved in the n-tuples"); }
        m_initialized = true;
    }

    const unsigned int run_number = t.runNumber();
    const unsigned int lumi_block = t.lumiblock();

    TriggerAccess& acc = t.triggers[m_item_pos];
    std::vector<PreScaleBlock>& pre_caches = m_prescalers[run_number];
    /// Keep track of all lumi blocks
    std::set<unsigned int>& lumi_blocks = m_lumi_blocks[run_number];
    m_triggered_evt[run_number] |= acc.pass_decision();
    std::vector<PreScaleBlock>::reverse_iterator itr = std::find_if(
        pre_caches.rbegin(), pre_caches.rend(), [&lumi_block](const PreScaleBlock& bl) { return bl.lumi_block == lumi_block; });
    if (itr != pre_caches.rend()) {
        itr->pre_scale = std::max(itr->pre_scale, acc.prescale());
    } else {
        pre_caches.emplace_back(run_number, lumi_block, acc.prescale());
        clean_previous_cache(run_number);
    }
    if (acc.pass_decision()) lumi_blocks.insert(lumi_block);
}

void TrigPreScaleCache::clean_previous_cache(unsigned int current_run) {
    if (m_last_run == current_run) return;

    if (m_last_run == INT_MAX) {
        m_last_run = current_run;
        return;
    }
    std::vector<PreScaleBlock>& prescale_to_clean = m_prescalers[m_last_run];
    EraseFromVector<PreScaleBlock>(prescale_to_clean, [](const PreScaleBlock& bl) { return !bl; });
    std::sort(prescale_to_clean.begin(), prescale_to_clean.end());
    m_last_run = current_run;
}
bool TrigPreScaleCache::finalize() {
    clean_previous_cache(INT_MAX);
    /// Clear the cache from any run where no prescales were detected
    for (const auto& itr : m_prescalers) {
        if (itr.second.empty()) {
            std::map<unsigned int, std::set<unsigned int>>::iterator lumi_itr = m_lumi_blocks.find(itr.first);
            if (lumi_itr != m_lumi_blocks.end()) m_lumi_blocks.erase(lumi_itr);
        }
    }
    /// Now it becomes interesting to fill up the remaining lumi blocks
    MuonTP::LumiCalculator* lumi_cal = MuonTP::LumiCalculator::getCalculator();
    for (auto& prescaler_set : m_prescalers) {
        std::vector<PreScaleBlock>& pres_vec = prescaler_set.second;
        const int run_number = prescaler_set.first;
        if (pres_vec.empty()) continue;
        const float first_prescale = pres_vec.front().pre_scale;
        bool has_diff_prescale{false};
        std::set<unsigned int> processed_lumi_blocks;
        for (const PreScaleBlock& block : pres_vec) {
            has_diff_prescale |= std::abs(block.pre_scale - first_prescale) > 0.1;
            processed_lumi_blocks.insert(block.lumi_block);
        }
        /// Precaling was not adapted during the run
        if (!has_diff_prescale) continue;

        /// Grasp the potentially missing lumi blocks
        std::set<unsigned int> good_blocks = lumi_cal->goodLumiBlocks(run_number);

        std::vector<unsigned int> unprocessed_blocks{};
        unprocessed_blocks.reserve(good_blocks.size());
        for (const unsigned int& bl : good_blocks) {
            if (!processed_lumi_blocks.count(bl)) { unprocessed_blocks.emplace_back(bl); }
        }
        /// All lumi blocks were seen
        if (unprocessed_blocks.empty()) continue;

        processed_lumi_blocks.clear();
        /// Sort the lumi blocks according to their prescale
        std::map<float, std::set<unsigned int>> sorted_blocks;
        auto fill_sorting = [&processed_lumi_blocks, &sorted_blocks, &pres_vec, &run_number, &lumi_cal](float pres_value) {
            std::set<unsigned int>& to_fill_in = sorted_blocks[pres_value];
            for (const PreScaleBlock& block : pres_vec) {
                if (processed_lumi_blocks.count(block.lumi_block)) continue;
                if (std::abs(pres_value - block.pre_scale) > 0.1) continue;
                processed_lumi_blocks.insert(block.lumi_block);
                if (lumi_cal->isGoodLumiBlock(run_number, block.lumi_block)) to_fill_in.insert(block.lumi_block);
            }
        };
        for (const PreScaleBlock& block : pres_vec) {
            if (processed_lumi_blocks.count(block.lumi_block)) continue;
            fill_sorting(block.pre_scale);
        }
        /// Now we need to determine the average luminosity
        std::map<float, std::pair<float, float>> prescale_lumis;
        for (const auto& sort_bl_pair : sorted_blocks) {
            const std::set<unsigned int>& lumi_blocks = sort_bl_pair.second;
            if (lumi_blocks.empty()) continue;
            float avg_lumi{0.}, mean_dev{0.};
            for (const int block : lumi_blocks) {
                const float block_lumi = lumi_cal->getExpectedLumiFromRun(run_number, block);
                avg_lumi += block_lumi;
                mean_dev += std::pow(block_lumi, 2);
            }
            avg_lumi /= lumi_blocks.size();
            mean_dev = std::sqrt(std::max(0.f, mean_dev / lumi_blocks.size() - avg_lumi));
            prescale_lumis[sort_bl_pair.first] = std::make_pair(avg_lumi, mean_dev);
        }
        sorted_blocks.clear();

        std::cout << "Trigger: " << trigger() << " need to fill the following lumi blocks" << unprocessed_blocks << std::endl;
        std::cout << "Found for run " << run_number << " the following prescales, luminosities " << std::endl;
        for (const auto& pres_lumi : prescale_lumis) {
            std::cout << " prescale: " << pres_lumi.first << " luminosity: " << pres_lumi.second.first << " pm " << pres_lumi.second.second
                      << std::endl;
        }
    }

    return !m_lumi_blocks.empty();
}
void TrigPreScaleCache::write(const std::string& out_dir) {
    if (!DoesDirectoryExist(out_dir)) {
        std::cout << "dump_prescales()  -- Create directory " << out_dir << " to store the file there." << std::endl;
        gSystem->mkdir(out_dir.c_str(), true);
    } else {
        std::cout << "dump_prescales()  -- " << trigger() << " will be stored in " << out_dir << std::endl;
    }
    std::ofstream ostr(out_dir + "/" + trigger() + ".txt");
    if (!ostr.good()) {
        std::cerr << "dump_prescales() -- Failed to write output file in " << out_dir << std::endl;
        return;
    }
    if (finalize()) dump_prescales(ostr);
}
void TrigPreScaleCache::write(std::shared_ptr<TFile> out_file) {
    dump_prescales(out_file);
    dump_lumiblocks(out_file);
}
void TrigPreScaleCache::dump_prescales(std::ofstream& ostr) {
    ostr << WhiteSpaces(50, "#") << std::endl
         << "# Calibration file to store the recorded prescales in data for trigger :" << std::endl
         << "#       " << trigger() << std::endl
         << "# Run numbers detected during the input together with the lumi blocks" << std::endl;
    for (const auto& itr : m_lumi_blocks) {
        ostr << "# -- " << itr.first << ":";
        for (const int& lumi : itr.second) { ostr << " " << lumi; }
        ostr << std::endl;
    }
    ostr << WhiteSpaces(50, "#") << std::endl << WhiteSpaces(50, "#") << std::endl;
    for (const auto& itr : m_prescalers) {
        for (const PreScaleBlock& block : itr.second) { ostr << block; }
    }
}

void TrigPreScaleCache::dump_prescales(std::shared_ptr<TFile> out_file) {
    if (!finalize()) return;
    /// Create a TDirectory for each item
    TDirectory* out_dir = PlotUtils::mkdir(trigger() + "/", out_file.get());
    std::unique_ptr<TTree> out_tree = std::make_unique<TTree>("PreScales", Form("Tree to store the prescales for %s", trigger().c_str()));
    TriggerPreScaleNtuple out_ntuple{out_tree.get()};
    MuonTP::LumiCalculator* lumi_cal = MuonTP::LumiCalculator::getCalculator();

    for (const auto& itr : m_prescalers) {
        const std::vector<PreScaleBlock>& trig_blocks = itr.second;
        const PreScaleBlock* last_block{nullptr};
        std::set<unsigned int> good_blocks;
        for (const PreScaleBlock& block : trig_blocks) {
            if (good_blocks.empty()) good_blocks = lumi_cal->goodLumiBlocks(block.run_number);

            if (last_block && (last_block->run_number != block.run_number || std::abs(last_block->pre_scale - block.pre_scale) > 0.1 ||
                               last_block->is_augmented != block.is_augmented ||
                               (block.lumi_block - last_block->lumi_block > 1 &&
                                /// Only care about the new run range if the intermediate blocks are on the GRL
                                std::count_if(good_blocks.begin(), good_blocks.end(), [&last_block, &block](unsigned int bl) {
                                    return bl > last_block->lumi_block && bl < block.lumi_block;
                                })))) {
                out_ntuple.last_block.set(last_block->lumi_block);
                out_tree->Fill();
                if (last_block->run_number != block.run_number) good_blocks.clear();
                last_block = nullptr;
            }
            if (!last_block) {
                out_ntuple.first_block.set(block.lumi_block);
                out_ntuple.pre_scale.set(block.pre_scale);
                out_ntuple.run_number.set(block.run_number);
                out_ntuple.rec_evt.set(!block.is_augmented);
            }
            last_block = &block;
        }
        /// Dump the last luminosity block to the n-tuple
        if (last_block) {
            out_ntuple.last_block.set(last_block->lumi_block);
            out_tree->Fill();
        }
    }
    out_dir->WriteObject(out_tree.get(), out_tree->GetName());
}
void TrigPreScaleCache::dump_lumiblocks(std::shared_ptr<TFile> out_file) {
    TDirectory* out_dir = PlotUtils::mkdir(trigger() + "/", out_file.get());
    std::unique_ptr<TTree> out_tree =
        std::make_unique<TTree>("LumiBlocks", Form("Tree to keep track of all lumi blocks seen by %s", trigger().c_str()));
    TriggerLumiBlockNtuple lumi_ntuple{out_tree.get()};
    MuonTP::LumiCalculator* lumi_cal = MuonTP::LumiCalculator::getCalculator();

    for (const auto& trig_run : m_triggered_evt) {
        lumi_ntuple.run_number.set(trig_run.first);
        lumi_ntuple.trigger_fired.set(trig_run.second);
        const std::set<unsigned int> good_blocks = lumi_cal->goodLumiBlocks(trig_run.first);

        /// Now we need to find the smalles lumi block pairs
        std::set<unsigned int>& lumi_blocks = m_lumi_blocks[trig_run.first];
        std::vector<uint16_t> first_block_vec, last_block_vec;
        unsigned int last_block{0}, first_block{0};
        for (unsigned int current_block : lumi_blocks) {
            if (current_block - last_block > 1 &&
                std::count_if(good_blocks.begin(), good_blocks.end(),
                              [&last_block, &current_block](unsigned int bl) { return bl > last_block && bl < current_block; })) {
                first_block_vec.push_back(first_block);
                last_block_vec.push_back(last_block);
                first_block = current_block;
            }
            last_block = current_block;
        }
        if (last_block != first_block) {
            first_block_vec.push_back(first_block);
            last_block_vec.push_back(last_block);
        }
        if (lumi_blocks.empty()) {
            first_block_vec.push_back(0);
            last_block_vec.push_back(lumi_cal->GetNLumiBlocks(trig_run.first));
        }

        lumi_ntuple.start_lumi.set(std::move(first_block_vec));
        lumi_ntuple.end_lumi.set(std::move(last_block_vec));

        out_tree->Fill();
    }
    out_dir->WriteObject(out_tree.get(), out_tree->GetName());
}
