#ifndef NTAU_POSTPROCESSING_POPULATORS__H
#define NTAU_POSTPROCESSING_POPULATORS__H

/// Here, we define a set of populators which can be used for post-processing previously populated histograms.
/// They take one or several upstream populators as constructor arguments, and then act on the 
/// output of these populators.
///
/// Fun fact: The post processing populators are themselves viable inputs to further 
/// post-processing populators! So you can set up an entire work flow using 
/// this formalism, with the Plot objects eventually populated containing the final
/// results! 
///
/// You can pass the upstream populators either as IPlotPopulators (meaning other populators), 
/// or you can also pass Plot objects directly - their populators will be cloned. 
/// Note that the populator of the Plot object, not its actual content, will be used.  
///
/// Quite a few of these postprocessors are restricted to certain input / output types. 
/// For examples, ratios only make sense for TH1 output, and extracting an efficiency 
/// from something which is not a TEfficiency is not very sensible. 

#include "NtupleAnalysisUtils/Plot/IPlotPopulator.h"
#include "NtupleAnalysisUtils/Helpers/NTAUHelpersIncludes.h" 
#include "TH2D.h"

/// ==============================================================================
/// Ratio calculation.
/// This will always return a TH1, since ratio calculations only make sense for binned entities
/// ==============================================================================
template <class InputHistoType> class CalculateRatio: public IPlotPopulator<TH1>{
public:

    /// construct by providing numerator and denominator sources and a bin error strategy. 
    /// Both numeratorSource and denominatorSource can be either a Plot or another populator.
    CalculateRatio ( const IPopulatorSource<InputHistoType> & numeratorSource,  
                     const IPopulatorSource<InputHistoType> & denominatorSource,
                     PlotUtils::EfficiencyMode errorDefinition = PlotUtils::defaultErrors); 

    /// this will populate the upstream sources and then perform a ratio calculation
    std::shared_ptr<TH1> populate() override;
    std::shared_ptr<IPlotPopulator<TH1>> clonePopulator() const override;

private: 
    std::shared_ptr<IPlotPopulator<InputHistoType>> m_numeratorSource;
    std::shared_ptr<IPlotPopulator<InputHistoType>> m_denominatorSource;
    PlotUtils::EfficiencyMode m_errorDefinition{PlotUtils::defaultErrors};
};


/// ==============================================================================
/// Efficiency extraction from a TEfficiency object. This can be used to collapse a TEfficiency 
/// result into a primitive histo. 
/// Will return a TH1D (the type of TEfficiency's internal histos)
/// ==============================================================================
class ExtractEfficiency: public IPlotPopulator<TH1D>{
public:
    /// inputEff can be either a Plot or another populator.
    ExtractEfficiency( const IPopulatorSource<TEfficiency> & inputEff); 

    std::shared_ptr<TH1D> populate() override; 
    std::shared_ptr<IPlotPopulator<TH1D>> clonePopulator() const override; 

private: 
    std::shared_ptr<IPlotPopulator<TEfficiency>> m_inputEff;
};


/// ==============================================================================
/// Efficiency extraction from a two-dim. TEfficiency object. 
/// This can be used to collapse a TEfficiency result into a primitive histo. 
/// Will return a TH2D (the type of TEfficiency's internal histos)
/// ==============================================================================
class ExtractEfficiency2D: public IPlotPopulator<TH2D>{
public:
    /// inputEff can be either a Plot or another populator.
    ExtractEfficiency2D( const IPopulatorSource<TEfficiency> & inputEff);   

    std::shared_ptr<TH2D> populate() override; 
    std::shared_ptr<IPlotPopulator<TH2D>> clonePopulator() const override; 

private: 
    std::shared_ptr<IPlotPopulator<TEfficiency>> m_inputEff;
};

/// ==============================================================================
/// Normalise the bin contents relative to a reference width. 
/// The reference width can be set to autodetect the first bin width if desired, 
/// by passing a negative value for the respective argument
/// ==============================================================================
template <class HistoType> class NormaliseBinsToWidth: public IPlotPopulator<HistoType>{
public:
    /// inputSource can be either a Plot or another populator.
    NormaliseBinsToWidth( const IPopulatorSource<HistoType> & inputSource, double referenceWidth = 1.0);    

    std::shared_ptr<HistoType> populate() override; 
    std::shared_ptr<IPlotPopulator<HistoType>> clonePopulator() const override; 

private: 
    std::shared_ptr<IPlotPopulator<HistoType>> m_input;
    double m_refWidth; 
};

/// ==============================================================================
/// Scale everything by a constant factor
/// ==============================================================================
template <class HistoType> class ScaleByConstant: public IPlotPopulator<HistoType>{
public:

    /// inputSource can be either a Plot or another populator.
    ScaleByConstant( const IPopulatorSource<HistoType> & inputSource, double scaleFactor = 1.0); 

    std::shared_ptr<HistoType> populate() override; 
    std::shared_ptr<IPlotPopulator<HistoType>> clonePopulator() const override; 

private: 
    std::shared_ptr<IPlotPopulator<HistoType>> m_input;
    double m_scaleFactor; 
};


/// ==============================================================================
/// Add a second plot, with an optional pre-factor
/// ==============================================================================
template <class HistoType> class LinearCombination: public IPlotPopulator<HistoType>{
public:

    /// inputSource and toAdd can be either a Plot or another populator.
    /// The scale factor is applied to the second argument (toAdd) - so a -1 would give you subtraction
    LinearCombination( const IPopulatorSource<HistoType> & inputSource, const IPopulatorSource<HistoType> & toAdd, double scaleFactor = 1.0); 

    std::shared_ptr<HistoType> populate() override; 
    std::shared_ptr<IPlotPopulator<HistoType>> clonePopulator() const override; 

private: 
    std::shared_ptr<IPlotPopulator<HistoType>> m_input;
    std::shared_ptr<IPlotPopulator<HistoType>> m_toAdd;
    double m_scaleFactor; 
};

/// ==============================================================================
/// Normalise to a certain integral value
/// ==============================================================================
template <class HistoType> class NormaliseToIntegral: public IPlotPopulator<HistoType>{
public:

    /// inputSource can be either a Plot or another populator.
    NormaliseToIntegral( const IPopulatorSource<HistoType> & inputSource, 
                        double target_integral = 1.0, 
                        bool multByBinWidth = false, 
                        bool includeOverflow = true);   

    std::shared_ptr<HistoType> populate() override;
    std::shared_ptr<IPlotPopulator<HistoType>> clonePopulator() const override; 

private: 
    std::shared_ptr<IPlotPopulator<HistoType>> m_input;
    double m_targetIntegral; 
    bool m_includeOflow; 
    bool m_multByWidth;
};

/// ==============================================================================
/// Obtain the sum of a set of populators representing individual contributions.
/// Useful for example for obtaining a "sum(SM)" histogram from a number of samples. 
/// ==============================================================================
template <class HistoType> class SumContributions: public IPlotPopulator<HistoType>{
public:

    /// Construct by providing an arbitrary number of populators or plots to sum together.
    /// 
    /// Note that this signature looks a bit strange since C++ won't accept 
    /// a vector of const-refs as a function argument and I don't want to force 
    /// the user to create shared pointers. We use a recursive variadic template 
    /// to be able to process an arbitrary number of const-refs to populator sources. 
    ///
    /// tl;dr: Don't worry about the look of the signature, just pass all your sum components 
    /// as the list of arguments and it should work! :-)
    template < typename... Args> SumContributions (Args... args); 

    /// Auxiliary constructor used for the clonePopulator method. 
    /// You can also call this in user code, but the signature above 
    /// will typically be much easier to work with! 
    SumContributions (const std::vector<std::shared_ptr<IPlotPopulator<HistoType>>> & inputs); 

    /// this will populate the upstream sources and fill a histogram with the sum of them 
    std::shared_ptr<HistoType> populate() override;
    
    /// clone this populator 
    std::shared_ptr<IPlotPopulator<HistoType>> clonePopulator() const override;

private: 
    /// helper methods to achieve desired constructor behaviour. 
    /// general recursion step
    template <typename... Args> void collateInputs(const IPopulatorSource<HistoType> & s, Args... args); 
    /// recursion end
    void collateInputs(const IPopulatorSource<HistoType> & s); 
    /// vector of populators to sum over
    std::vector<std::shared_ptr<IPlotPopulator<HistoType>>> m_inputs;
};


/// ==============================================================================
/// This class is designed as a catch-all to support arbitrary post-processing, including histogram type conversion. 
/// Can cover use cases not featured above. 
/// The user is responsible for providing the desired functionality via a function object. 
/// ==============================================================================
template <class OutputHistoType, class InputHistoType> class GenericPostProcessing: public IPlotPopulator<OutputHistoType>{
public:
    GenericPostProcessing(
        /// input source - of any type
        const IPopulatorSource<InputHistoType> & inputSource, 
        /// function which produces the desired output given the input
        std::function<std::shared_ptr<OutputHistoType>(std::shared_ptr<InputHistoType>)> processingStep);
    
    std::shared_ptr<OutputHistoType> populate() override;
    std::shared_ptr<IPlotPopulator<OutputHistoType>> clonePopulator() const override;

private:
    std::shared_ptr<IPlotPopulator<InputHistoType>> m_input;
    std::function<std::shared_ptr<OutputHistoType>(std::shared_ptr<InputHistoType>)> m_processingStep; 
}; 

#include "PostProcessingPopulators.ixx"

#endif