#ifndef NTAU_COMMON__INCLUDES
#define NTAU_COMMON__INCLUDES

/// Top level header for the headers within the Common part of NTAU

#include "NtupleAnalysisUtils/Plot/Plot.h"
#include "NtupleAnalysisUtils/Plot/BasicPopulators.h"
#include "NtupleAnalysisUtils/Plot/PostProcessingPopulators.h"
#endif