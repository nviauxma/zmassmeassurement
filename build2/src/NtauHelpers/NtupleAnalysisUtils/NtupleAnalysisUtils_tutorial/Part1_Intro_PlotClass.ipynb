{
 "cells": [
  {
   "source": [
    "# Welcome to part 1 of the NtupleAnalysisUtils Tutorial! \n",
    "\n",
    "In this part, we will introduce the basic ideas of the package and start to try out the `Plot` class, the main interface to ROOT objects. \n",
    "\n",
    "Later parts will then cover more advanced topics. "
   ],
   "cell_type": "markdown",
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Notebook setup\n",
    "\n",
    "The following lines are *only* needed within the Jupyter world to set up the package. They are not needed in normal usage (unless you wish to use NtupleAnalysisUtils within a Swan notebook)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "init_cell": true
   },
   "outputs": [],
   "source": [
    "%alias getFromEOS if [[ -e %l ]]; then echo \"Obtaining %l via cp\"; cp -r %l . ;  else echo \"Obtaining %l via xrdcp\" ; xrdcp -r --force root://eosuser.cern.ch/%l .  ; fi "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import ROOT"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "getFromEOS /eos/user/g/goblirsc/NtupleAnalysisUtils/CI_Artifacts/lib/libNtupleAnalysisUtils.so"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "getFromEOS /eos/user/g/goblirsc/NtupleAnalysisUtils/CI_Artifacts/include"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "init_cell": true
   },
   "outputs": [],
   "source": [
    "%%cpp\n",
    "#pragma cling add_include_path(\"include/\")\n",
    "gSystem->Load(\"libNtupleAnalysisUtils\");"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Known limitations of this notebook\n",
    "\n",
    "This notebook lives in cling, which is an interpreter for C++ syntax provided with ROOT. \n",
    "\n",
    "As a consequence, we can not provide the real workflow of coding - compiling - running you would be using in practice. \n",
    "On the positive side, with the interpreted flavour it is very easy to manipulate things in place and experiment! \n",
    "\n",
    "Another consequence is that all C++ cells are wrapped in a \"%%cpp\" 'cell magic', which makes ROOT interpret them. \n",
    "This is of course another thing you would not do in 'real life'. \n",
    "\n",
    "I tried to provide compiled versions of most of the examples within the [examples](https://gitlab.cern.ch/goblirsc/NtupleAnalysisUtils/-/blob/master/examples/) folder of the repository - so you can still try these out in the standard workflow, just without the inline documentation! \n",
    "\n",
    "## Important - Requirements \n",
    "---\n",
    "\n",
    "Another thing to be aware of is that, since we are loading a pre-compiled C++ library, the **version of ROOT** you set up **SWAN** with must match the ROOT version that was used to compile the compiled C++ library provided for this tutorial. \n",
    "\n",
    "The library will always be based on the latest AnalysisBase. At the time of writing, this is **ROOT 6.20.06**, which you can get by **setting up version \"97a\" of the stack in the initial SWAN window**. \n",
    "\n",
    "---\n",
    "You can find the \"correct\" LCG release (stack version) for each ROOT by looking at [this page](http://lcginfo.cern.ch/pkg/ROOT/) and clicking on the version you need. \n",
    "\n",
    "If the versions do not match, the line above in Cell number 3 should print an error. If you don't see one, things are probably fine! \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "toc": true
   },
   "source": [
    "<h1>Table of Contents<span class=\"tocSkip\"></span></h1>\n",
    "<div class=\"toc\"><ul class=\"toc-item\"><li><ul class=\"toc-item\"><li><ul class=\"toc-item\"><li><span><a href=\"#Notebook-setup\" data-toc-modified-id=\"Notebook-setup-0.0.1\"><span class=\"toc-item-num\">0.0.1&nbsp;&nbsp;</span>Notebook setup</a></span></li><li><span><a href=\"#Known-limitations-of-this-notebook\" data-toc-modified-id=\"Known-limitations-of-this-notebook-0.0.2\"><span class=\"toc-item-num\">0.0.2&nbsp;&nbsp;</span>Known limitations of this notebook</a></span></li></ul></li><li><span><a href=\"#Important---Requirements\" data-toc-modified-id=\"Important---Requirements-0.1\"><span class=\"toc-item-num\">0.1&nbsp;&nbsp;</span>Important - Requirements</a></span></li></ul></li><li><span><a href=\"#Getting-started-with-NtupleAnalysisUtils\" data-toc-modified-id=\"Getting-started-with-NtupleAnalysisUtils-1\"><span class=\"toc-item-num\">1&nbsp;&nbsp;</span>Getting started with NtupleAnalysisUtils</a></span><ul class=\"toc-item\"><li><span><a href=\"#What-is-this-package?\" data-toc-modified-id=\"What-is-this-package?-1.1\"><span class=\"toc-item-num\">1.1&nbsp;&nbsp;</span>What is this package?</a></span></li><li><span><a href=\"#How-to-use-in-real-life\" data-toc-modified-id=\"How-to-use-in-real-life-1.2\"><span class=\"toc-item-num\">1.2&nbsp;&nbsp;</span>How to use in real life</a></span><ul class=\"toc-item\"><li><span><a href=\"#Supported-build-systems\" data-toc-modified-id=\"Supported-build-systems-1.2.1\"><span class=\"toc-item-num\">1.2.1&nbsp;&nbsp;</span>Supported build systems</a></span></li><li><span><a href=\"#Getting-access-to-the-package's-features-in-your-analysis-code\" data-toc-modified-id=\"Getting-access-to-the-package's-features-in-your-analysis-code-1.2.2\"><span class=\"toc-item-num\">1.2.2&nbsp;&nbsp;</span>Getting access to the package's features in your analysis code</a></span></li></ul></li></ul></li><li><span><a href=\"#The-Plot-class-and-how-to-fill-it\" data-toc-modified-id=\"The-Plot-class-and-how-to-fill-it-2\"><span class=\"toc-item-num\">2&nbsp;&nbsp;</span>The Plot class and how to fill it</a></span><ul class=\"toc-item\"><li><span><a href=\"#A-Plot-behaves-like-a-ROOT-object-pointer\" data-toc-modified-id=\"A-Plot-behaves-like-a-ROOT-object-pointer-2.1\"><span class=\"toc-item-num\">2.1&nbsp;&nbsp;</span>A Plot behaves like a ROOT object pointer</a></span></li><li><span><a href=\"#The-Plot-in-detail---populator-and-format\" data-toc-modified-id=\"The-Plot-in-detail---populator-and-format-2.2\"><span class=\"toc-item-num\">2.2&nbsp;&nbsp;</span>The Plot in detail - populator and format</a></span></li><li><span><a href=\"#Accessing-the-ROOT-object-wrapped-within-the-Plot\" data-toc-modified-id=\"Accessing-the-ROOT-object-wrapped-within-the-Plot-2.3\"><span class=\"toc-item-num\">2.3&nbsp;&nbsp;</span>Accessing the ROOT object wrapped within the Plot</a></span></li><li><span><a href=\"#More-on-populators---post-processing-chains\" data-toc-modified-id=\"More-on-populators---post-processing-chains-2.4\"><span class=\"toc-item-num\">2.4&nbsp;&nbsp;</span>More on populators - post-processing chains</a></span></li><li><span><a href=\"#More-on-the-PlotFormat\" data-toc-modified-id=\"More-on-the-PlotFormat-2.5\"><span class=\"toc-item-num\">2.5&nbsp;&nbsp;</span>More on the PlotFormat</a></span><ul class=\"toc-item\"><li><span><a href=\"#How-to-set-and-get-individual-settings\" data-toc-modified-id=\"How-to-set-and-get-individual-settings-2.5.1\"><span class=\"toc-item-num\">2.5.1&nbsp;&nbsp;</span>How to set and get individual settings</a></span></li><li><span><a href=\"#Colors-and-Styles\" data-toc-modified-id=\"Colors-and-Styles-2.5.2\"><span class=\"toc-item-num\">2.5.2&nbsp;&nbsp;</span>Colors and Styles</a></span></li><li><span><a href=\"#Draw-options-and-legends\" data-toc-modified-id=\"Draw-options-and-legends-2.5.3\"><span class=\"toc-item-num\">2.5.3&nbsp;&nbsp;</span>Draw options and legends</a></span></li><li><span><a href=\"#Custom-settings---when-you-need-to-pass-extra-information\" data-toc-modified-id=\"Custom-settings---when-you-need-to-pass-extra-information-2.5.4\"><span class=\"toc-item-num\">2.5.4&nbsp;&nbsp;</span>Custom settings - when you need to pass extra information</a></span></li></ul></li></ul></li><li><span><a href=\"#The-Ntuple-classes---when-you-want-to-talk-to-(T)Trees\" data-toc-modified-id=\"The-Ntuple-classes---when-you-want-to-talk-to-(T)Trees-3\"><span class=\"toc-item-num\">3&nbsp;&nbsp;</span>The Ntuple classes - when you want to talk to (T)Trees</a></span><ul class=\"toc-item\"><li><span><a href=\"#Working-with-individual-branches---the-NtupleBranch-class\" data-toc-modified-id=\"Working-with-individual-branches---the-NtupleBranch-class-3.1\"><span class=\"toc-item-num\">3.1&nbsp;&nbsp;</span>Working with individual branches - the NtupleBranch class</a></span><ul class=\"toc-item\"><li><span><a href=\"#Using-branches-for-output\" data-toc-modified-id=\"Using-branches-for-output-3.1.1\"><span class=\"toc-item-num\">3.1.1&nbsp;&nbsp;</span>Using branches for output</a></span></li></ul></li><li><span><a href=\"#Packaging-many-branches---the-NtupleBranchMgr\" data-toc-modified-id=\"Packaging-many-branches---the-NtupleBranchMgr-3.2\"><span class=\"toc-item-num\">3.2&nbsp;&nbsp;</span>Packaging many branches - the NtupleBranchMgr</a></span><ul class=\"toc-item\"><li><span><a href=\"#Basic-usage---a-container-for-branches\" data-toc-modified-id=\"Basic-usage---a-container-for-branches-3.2.1\"><span class=\"toc-item-num\">3.2.1&nbsp;&nbsp;</span>Basic usage - a container for branches</a></span></li><li><span><a href=\"#Branch-discovery-and-Ntuple-exploration\" data-toc-modified-id=\"Branch-discovery-and-Ntuple-exploration-3.2.2\"><span class=\"toc-item-num\">3.2.2&nbsp;&nbsp;</span>Branch discovery and Ntuple exploration</a></span></li><li><span><a href=\"#Code-generation\" data-toc-modified-id=\"Code-generation-3.2.3\"><span class=\"toc-item-num\">3.2.3&nbsp;&nbsp;</span>Code-generation</a></span></li></ul></li><li><span><a href=\"#Derived-virtual-branches\" data-toc-modified-id=\"Derived-virtual-branches-3.3\"><span class=\"toc-item-num\">3.3&nbsp;&nbsp;</span>Derived virtual branches</a></span></li><li><span><a href=\"#Branch-access-by-name---for-rarely-used-branches,-or-string-based-interfaces\" data-toc-modified-id=\"Branch-access-by-name---for-rarely-used-branches,-or-string-based-interfaces-3.4\"><span class=\"toc-item-num\">3.4&nbsp;&nbsp;</span>Branch access by name - for rarely used branches, or string-based interfaces</a></span></li><li><span><a href=\"#Bootstrap-weights\" data-toc-modified-id=\"Bootstrap-weights-3.5\"><span class=\"toc-item-num\">3.5&nbsp;&nbsp;</span>Bootstrap weights</a></span></li></ul></li><li><span><a href=\"#Efficient-NTuple-processing---the-HistoFiller\" data-toc-modified-id=\"Efficient-NTuple-processing---the-HistoFiller-4\"><span class=\"toc-item-num\">4&nbsp;&nbsp;</span>Efficient NTuple processing - the HistoFiller</a></span><ul class=\"toc-item\"><li><span><a href=\"#The-Sample\" data-toc-modified-id=\"The-Sample-4.1\"><span class=\"toc-item-num\">4.1&nbsp;&nbsp;</span>The Sample</a></span></li><li><span><a href=\"#The-Selection\" data-toc-modified-id=\"The-Selection-4.2\"><span class=\"toc-item-num\">4.2&nbsp;&nbsp;</span>The Selection</a></span></li><li><span><a href=\"#The-PlotFillInstruction\" data-toc-modified-id=\"The-PlotFillInstruction-4.3\"><span class=\"toc-item-num\">4.3&nbsp;&nbsp;</span>The PlotFillInstruction</a></span></li><li><span><a href=\"#Putting-it-together---running-the-histo-filler\" data-toc-modified-id=\"Putting-it-together---running-the-histo-filler-4.4\"><span class=\"toc-item-num\">4.4&nbsp;&nbsp;</span>Putting it together - running the histo filler</a></span></li></ul></li></ul></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Getting started with NtupleAnalysisUtils \n",
    "\n",
    "## What is this package? \n",
    "\n",
    "NtupleAnalysisUtils is a toolkit designed to help with the final stages of an analysis. This typically involves working with (not so) small ntuples or histograms to carry out studies and produce / visualise results. \n",
    "The tool of choice for this is ROOT, which is very powerful, but can be a pain to use due to its very power.  \n",
    "\n",
    "The main aim of NtupleAnalysisUtils is to help users with the ROOT boilerplate code which is required in standard workflows, to make life easier without losing any of the power ROOT provides. \n",
    "\n",
    "Tasks such as interfacing to ntuples, looping over events, filling histograms, configuring their appearance, writing/loading from files and saving final plots to output files are all supported and should become much easier than with out-of-the-box ROOT. \n",
    "\n",
    "Main historic use cases include \n",
    "*  Fast NTuple parsing on user resources (laptop / desktop) for physics analysis\n",
    "*  NTuple post-processing (writing of an extended copy of an input Ntuple) in analysis ntuple production \n",
    "*  Fast semi-automatic drawing of large numbers of plots in physics validation  \n",
    "\n",
    "The components of the package are intended to work nicely together while being sufficiently independent that users can pick and use only those useful to them. \n",
    "\n",
    "They broadly include: \n",
    "- A flexible `Plot` class that allows interaction with and on-the-fly population of ROOT 'data' objects (histograms, graphs, and related) \n",
    "- A series of classes intended to make it easy to read and write ROOT TTrees without boilerplate code \n",
    "- A multi-threaded, optimised ntuple event loop implementation capable of processing arbitrary trees, with support classes allowing for complex event selections, histogram filling and input samples to be configured up-front in a modular way. \n",
    "- A set of visualisation tools allowing for automatic creation of high-quality figures, including multi-page overview PDF files \n",
    "- A set of helper methods in a dedicated namespace that make the day-to-day work with ROOT easier. \n",
    "\n",
    "As a result of this approach, the package should **not** be considered as a framework in the sense of providing a pre-made runtime for the user to configure. \n",
    "Instead, the intended usage is as a utility library that user packages can link against, while the final programs are still being written (and under full control of) the user. \n",
    "The hope is that 95% of the real-life analysis / CP studies being carried out can be implemented within a few hundred lines of code when making full use of the available features. \n",
    "\n",
    "## How to use in real life\n",
    "\n",
    "### Supported build systems\n",
    "\n",
    "For this tutorial, we use an interactive notebook powered by Cling to demonstrate the usage of the package's features and allow to try things out in an easily accessible fashion. \n",
    "In real life, the code used in the following could for example appear in the `main` method of compiled programs. \n",
    "\n",
    "To compile your program against `NtupleAnalysisUtils`, we recommend to work within the standard ATLAS releases and the related `CMake` based build system. \n",
    "Since we only take ROOT and the compiler from the release, the package does not require any specific release and should work equally well in any recent (GCC 8 + CMake) release of either AnalysisBase, AthAnalysis, or even full Athena! \n",
    "It comes with a standard ATLAS-CMake CMakeLists, and compiles to a library that your analysis can link against.\n",
    "\n",
    "`RootCore` is **no longer** supported, neither is `cmt`. \n",
    "\n",
    "### What about python? \n",
    "Loading the library within pyROOT allows for some usage in pyROOT as well - recently, `NtupleAnalysisTools` has for example succesfully been used within python in the scope of a [Higgs analysis](https://gitlab.cern.ch/HZZ/HZZSoftware/HZZCPStudy/Run2/CPWorkspaces/-/tree/master/CPWorkspaces). \n",
    "And with the 'new' pyROOT as of ROOT 6.22, the many of the NTAU classes can be used transparently in pyROOT! \n",
    "\n",
    "However, as the maintainer tends to prefer C++ for his daily work, further exploration of the usability in python may be slow. Volunteers are welcome to join this effort!\n",
    "\n",
    "\n",
    "### Getting access to the package's features in your analysis code\n",
    "\n",
    "First, you need to add NtupleAnalysisUtils to the `LINK_LIBRARIES` argument of your own project's CMakeLists file. \n",
    "\n",
    "For example: \n",
    "```\n",
    "atlas_add_library( MyAnalysisPackageLib\n",
    "  MyAnalysisPackage/*.h Root/*.cxx\n",
    "  PUBLIC_HEADERS MyAnalysisPackage\n",
    "  INCLUDE_DIRS ${ROOT_INCLUDE_DIRS} \n",
    "  PRIVATE_INCLUDE_DIRS Root  \n",
    "  LINK_LIBRARIES NtupleAnalysisUtils ${ROOT_LIBRARIES}\n",
    "  )\n",
    "```\n",
    "\n",
    "Then, when coding, there is a _single point-of-entry header_ to include for all of the NTAU features. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "init_cell": true
   },
   "outputs": [],
   "source": [
    "%%cpp\n",
    "#include \"NtupleAnalysisUtils/NTAUTopLevelIncludes.h\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Any source file with this include will have access to the full set of features. \n",
    "\n",
    "# The Plot class and how to fill it\n",
    "\n",
    "The [Plot class](https://gitlab.cern.ch/goblirsc/NtupleAnalysisUtils/-/blob/master/NtupleAnalysisUtils/Plot/Plot.h) is used to interact with any ('binned data' type) ROOT object we may wish to create and work with during our analysis work. \n",
    "It acts like a **shared pointer** to the contained ROOT object.\n",
    "This means in particular: \n",
    " * You can interact with it like with a 'normal' ROOT object pointer, and it exposes all the features of the underlying ROOT object\n",
    " * It manages the lifetime of the contained ROOT object. The ROOT object is cleaned up as soon as no `Plot` objects referring to it remain. This replaces the default ROOT behaviour, where the object name and TDirectory decide when something is deleted.\n",
    " * It can be a `nullptr` - empty `Plot` objects are a possiblity. \n",
    " \n",
    "However, unlike a simple shared pointer, it also \n",
    " * takes care of obtaining and filling your object if desired\n",
    " * is aware of the desired [formatting](https://gitlab.cern.ch/goblirsc/NtupleAnalysisUtils/-/blob/master/NtupleAnalysisUtils/Configuration/PlotFormat.h), and can apply this format to the drawn object in one line of code \n",
    " \n",
    "The typical usage of `NtupleAnalysisUtils` when using `Plot` objects consists a declarative approach, in which we specify what we want our objects to contain, and then let `NtupleAnalysisUtils` take care of getting the required work done in the background.\n",
    "\n",
    "## A Plot behaves like a ROOT object pointer\n",
    "\n",
    "As mentioned, a `Plot` object can be used just like a pointer! For example, you can play with the following code: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "%%cpp\n",
    "// Set the ATLAS style to make our plots look 'official'...\n",
    "SetAtlasStyle();\n"
   ]
  },
  {
   "source": [
    "Let's instantiate our first plot. We will use a very simple version, where we construct an empty histogram in the same way the standard ROOT constructor would: "
   ],
   "cell_type": "markdown",
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%cpp\n",
    "// Instantiate a new empty histogram of user-configured binning\n",
    "Plot<TH1D> myHisto{ConstructInPlace<TH1D>(\"H1\",\"H1\",200,-2,2)}; /// \"ConstructInPlace\" will create a new, empty histogram according to the usual ROOT arguments. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will explain the arguments used in the next section. Notice how the Plot is templated to the type of object it should package. \n",
    "\n",
    "Now, let's draw it - the following should look just the same as drawing a ROOT histogram, showing the close connection between the two.\n",
    "\n",
    "Feel free to play with the following and try anything you would do with a histogram pointer - it should all work! "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%cpp\n",
    "// Give it some dummy content before drawing it.\n",
    "// Notice how we can work with the Plot like we would with a ROOT pointer!\n",
    "myHisto->FillRandom(\"gaus\",100000);  // Fill our histo a bit so that things become less boring\n",
    "\n",
    "TCanvas* c1 = new TCanvas(\"c1\",\"c1\",400,300); \n",
    "c1->Draw(); \n",
    "myHisto->Draw(); // this is TH1D::Draw, not some wrapper or substitute! \n",
    "myHisto->Print(); \n",
    "std::cout << myHisto->ClassName()<<std::endl; // and it even quacks like a TH1D!\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Just to prove that this is really a Plot instance, and not a sneaky ROOT pointer: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%cpp\n",
    "myHisto"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Yay! We have made our first plot! \n",
    "\n",
    "## The Plot in detail - populator and format\n",
    "\n",
    "In the example above, you saw me use `ConstructInPlace<TH1D>` as an argument to the `Plot` constructor to obtain a histogram with the desired binning within our plot. \n",
    "This is an example of a [PlotPopulator](https://gitlab.cern.ch/goblirsc/NtupleAnalysisUtils/-/blob/master/NtupleAnalysisUtils/Plot/BasicPopulators.h).\n",
    "\n",
    "Any `Plot` has such a `PlotPopulator`, and it is responsible for creating and filling (if you want it to) the ROOT object wrapped within the `Plot`. \n",
    "\n",
    "This can take many shapes, with a number of pre-made `PlotPopulator`s available (plus it is easy to make new ones). \n",
    "\n",
    "For example, you can \n",
    " * Create a new empty histogram from scratch, as above, using `ConstructInPlace`\n",
    " * Copy an existing histogram (for example an output of a tool), using `CopyExisting` \n",
    " * Create a histogram and immediately add a starting content, using `ConstructAndFillInPlace`\n",
    " * Load a histogram from a file, using `LoadFromFile`\n",
    " * Run an multi-threaded event loop on one or many input files and populate histograms with the result, using `RunHistoFiller` (see later for details!) \n",
    " \n",
    "---\n",
    " Useful hint: You can also chain populators together, with one taking the output from another as input. \n",
    " \n",
    " This allows to construct useful workflows, for example to calculate ratios, or extract efficiencies, or apply normalisation, or... - see the [Post-Processing populator chains](https://gitlab.cern.ch/goblirsc/NtupleAnalysisUtils/-/blob/master/NtupleAnalysisUtils/Plot/PostProcessingPopulators.h) discussed later in this tutorial.\n",
    " \n",
    "---\n",
    "In many cases, you will be able to define the *entire* work chain up to the final histogram via the populator, without any manual work needed. \n",
    "\n",
    "An important distinction from directly working with ROOT is that **a `Plot` is not immediately populated when you create it**. \n",
    "\n",
    "Instead, the `populator` will opportunistically do its work when you first attempt to interact with the `Plot`'s internal ROOT object. \n",
    "You can also manually trigger the population at any time by calling the `populate()` method.\n",
    "\n",
    "This has the advantage that you can first define all the work you need done - for example book a number of different histograms all to be filled using the same NTuple - and then have everything filled in one go. \n",
    "In particular for the 'ntuple processing' workflow, the code is smart enough to organise the work in the background to get everything done as efficiently as possible, and you will experience a much faster workflow than calling TTree::Draw several times would for example. \n",
    "\n",
    "The `Plot` class also takes a second argument, which we omitted in the example above: the [PlotFormat](https://gitlab.cern.ch/goblirsc/NtupleAnalysisUtils/-/blob/master/NtupleAnalysisUtils/Configuration/PlotFormat.h) \n",
    "This is responsible for steering how your nice new Plot will be visualised if you draw it. This includes\n",
    " * Colours for lines, markers, filling \n",
    " * Line and Marker styles, sizes, alpha values\n",
    " * The title and draw style to use in a Legend\n",
    " * Any draw options to use when drawing the object\n",
    " \n",
    "Since the format is a self-contained object, you can apply the same format to several `Plots`, making it easy to achieve a uniform look of your results!  \n",
    "You can also store a `PlotFormat` as a one-liner snippet to establish a common plotting style across your code or even with other users. \n",
    " \n",
    "Let's try it out!\n",
    "\n",
    "---\n",
    "**Note on the following example** \n",
    "\n",
    "You can also find the following example as a compiled program in [examples/NTAU_Example_Plot_2_1_2.cxx](https://gitlab.cern.ch/goblirsc/NtupleAnalysisUtils/-/blob/master/examples/NTAU_Example_Plot_2_1_2.cxx).\n",
    "\n",
    "---"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%cpp\n",
    "\n",
    "// For readability, we define the arguments separately before passing them into the Plot constructor.\n",
    "// If you want, you can do all this in one call ;-) \n",
    "\n",
    "// First, we define the format we would like. Here, we go for a nice dark blue. \n",
    "// We also prepare everything for drawing a legend (will not be done in this example)\n",
    "// See how we can daisy-chain arguments in arbitrary order to build our format? \n",
    "PlotFormat myFormat = PlotFormat().MarkerStyle(kOpenCircle).Color(kBlue+1).FillStyle(1001).FillAlpha(0.3).LegendTitle(\"My cool Gauss\").LegendOption(\"PL\"); \n",
    "// of course, this format can now be re-used if you are making many plots that should look the same! \n",
    "\n",
    "\n",
    "/// Now, we put together a plot via Populator and Format. \n",
    "/// Here, the populator will load a histogram stored in a file. To do so, we give it two arguments: \n",
    "///                                     file to read...                                                                          ...and name of \n",
    "///                                                                                                                              the object to load    \n",
    "Plot<TH1D> myPlot (LoadFromFile<TH1D>(\"MyNiceGauss.root\",\"MyNiceGauss\"), \n",
    "                   myFormat); \n",
    "\n",
    "/// We can also make changes to the format later on! \n",
    "myPlot.plotFormat().MarkerStyle(kFullDotLarge);  /// let's make the markers a bit nicer... \n",
    "\n",
    "/// Opportunistic population means the object will be read from the input file in the following \n",
    "/// line, triggered by our access to the content via the \"->\" operator! \n",
    "/// Notice that the format will be applied for us at the same time. \n",
    "c1->cd();\n",
    "c1->Draw(); \n",
    "myPlot->Draw(\"PL\");\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Please feel free to experiment a bit with the `PlotFormat`, and see how changing it affects the outcome. \n",
    "\n",
    "The format can also be manually applied or updated after drawing - for example if you decide to change it after populating a plot: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%cpp\n",
    "myPlot.plotFormat().Color(kRed).MarkerStyle(kFullDiamond).FillStyle(0).FillColor(0);\n",
    "myPlot.applyFormat(); /// this triggers a re-application of the updated format if the plot has already been populated.\n",
    "\n",
    "c1->cd();\n",
    "myPlot->Draw(\"PL\");\n",
    "c1->Draw(); "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Plots can also be copied, of course. One thing to keep in mind is that this gives you a **clone** of the contained object. \n",
    "\n",
    "This means that changing a copy of a `Plot` will not affect the original, unlike what would happen if you simply copied a smart pointer. \n",
    "So when I told you the `Plot` behaved just like a smart pointer, I may have lied a little bit... \n",
    "\n",
    "If the `Plot` being copied is already populated, the clone will be created **immediately**. \n",
    "\n",
    "Otherwise, the copy will be given a `Populator` which does the same work as the original `Plot`'s populator, resulting in a distinct histogram of identical properties after population. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%cpp\n",
    "Plot<TH1D> myOtherPlot = myPlot; /// copy via assignment operator. \n",
    "myOtherPlot.plotFormat().MarkerStyle(kOpenSquare).Color(kBlue-6).FillStyle(0);  /// change the format of the copy a bit\n",
    "myOtherPlot.applyFormat(); \n",
    "myOtherPlot->Scale(0.9); /// also scale it compared to the original\n",
    "\n",
    "/// Draw both - they will now be different, showing that both Plots contain separate ROOT objects. \n",
    "myPlot->Draw(\"PL\");\n",
    "myOtherPlot->Draw(\"PLSAME\");\n",
    "c1->Draw(); "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Accessing the ROOT object wrapped within the Plot\n",
    "Sometimes you need to get your hands on the raw ROOT object packaged within a `Plot` - for example when you want to interact with other tools requiring ROOT pointers. \n",
    "There are three things you can do: \n",
    "* Both the `()` operator and the `getHisto()` method return a *raw pointer* to the object. For example, a `Plot<TH1D>` will give you a `TH1D*`\n",
    "* `getSharedHisto()` gives you the original *shared pointer* to the contained object.  For example, a `Plot<TH1D>` will give you a `std::shared_ptr<TH1D>`\n",
    "\n",
    "Please keep in mind that since the `Plot` acts like a shared pointer, the raw pointers will become invalid if the `Plot` is destroyed or goes out of scope. \n",
    "\n",
    "For this reason, you may want to use ROOT's `Clone` method when you need to have a histogram with a lifetime longer than that of the `Plot`. \n",
    "If you get the share pointer, the lifetime of the object is guaranteed to extend to until both the plot and the shared pointer have gone out of scope. \n",
    "\n",
    "\n",
    "---\n",
    "**Note on the following example** \n",
    "\n",
    "You can also find the following example as a compiled program in [examples/NTAU_Example_Plot_2_1_3.cxx](https://gitlab.cern.ch/goblirsc/NtupleAnalysisUtils/-/blob/master/examples/NTAU_Example_Plot_2_1_3.cxx).\n",
    "\n",
    "---"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%cpp\n",
    "TH1D* theRawROOTTH1 = myOtherPlot();          /// Access raw pointer #1  \n",
    "TH1D* theSameOne = myOtherPlot.getHisto();    /// Access raw pointer #2 - nicer syntax when working with pointers to Plot instances\n",
    "std::shared_ptr<TH1D> sharedPtr = myOtherPlot.getSharedHisto();    /// Access shared pointer \n",
    "\n",
    "// show that these three pointers are really all the same \n",
    "std::cout << \" Are they really the same? \"<<theRawROOTTH1<<std::endl;\n",
    "std::cout << \" Are they really the same? \"<<theSameOne<<std::endl;\n",
    "std::cout << \" Are they really the same? \"<<sharedPtr.get()<<std::endl;\n",
    "\n",
    "/// Any changes we make via the pointers are reflected when we access the Plot object - this is *not* a clone! \n",
    "theRawROOTTH1->Fill(-1.75,10000.);\n",
    "\n",
    "myOtherPlot->Draw(\"PL\");  /// now has a spike... with a big stat error ;-)\n",
    "c1->Draw(); "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    " **Important** note on object lifetime\n",
    "\n",
    "If you get a raw ROOT pointer in the way described above, be aware **that this pointer will only be valid during the lifetime of the `Plot` object it belongs to**. \n",
    "This is because the `Plot`'s smart-pointer-like behaviour includes destroying its content when going out of scope. \n",
    "\n",
    "If you need a ROOT pointer with validity beyond the `Plot` object you take the object from, please use ROOT's `Clone` method. For example: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%cpp -d\n",
    "TH1D* thisIsDangerous(){\n",
    "    /// obtain a `Plot` with a lifetime limited to this method \n",
    "    Plot<TH1D> localPlot(ConstructInPlace<TH1D>(\"H2\",\"H2\",200,0,20)); \n",
    "    return localPlot(); // here, we return the ROOT pointer from 'theCopy' - only valid within the method\n",
    "}\n",
    "\n",
    "TH1D* thisIsSafe(){\n",
    "    /// obtain a `Plot` with a lifetime limited to this method \n",
    "    Plot<TH1D> localPlot(ConstructInPlace<TH1D>(\"H3\",\"H3\",200,0,20)); \n",
    "    return static_cast<TH1D*>(localPlot()->Clone()); // return a clone to ensure life-time beyond this method \n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "While clones will safely exceed the lifetime of their parent `Plot` objects..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%cpp\n",
    "TH1D* validPointer = thisIsSafe(); \n",
    "validPointer->Print(); // this is safe to do! "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "... trying to access the pointer to the content of a `Plot` which no longer exists would lead to a crash. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "raises-exception"
    ]
   },
   "outputs": [],
   "source": [
    "%%cpp\n",
    "TH1D* invalidPointer = thisIsDangerous(); \n",
    "invalidPointer->Print(); // boom!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "See, I warned you! \n"
   ]
  },
  {
   "source": [
    "# What now? \n",
    "\n",
    "Congratulations, you should now know the basics of how to create simple `Plot` objects, how to format and how to interact with them! \n",
    "\n",
    "In the following parts, we will look at how we can do more advanced work, including more extensive filling workflows, such as NTuple reading, and how to unlock all the features of the package. "
   ],
   "cell_type": "code",
   "metadata": {},
   "execution_count": null,
   "outputs": []
  }
 ],
 "metadata": {
  "celltoolbar": "Tags",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": true,
   "toc_position": {
    "height": "calc(100% - 180px)",
    "left": "10px",
    "top": "150px",
    "width": "165px"
   },
   "toc_section_display": true,
   "toc_window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}