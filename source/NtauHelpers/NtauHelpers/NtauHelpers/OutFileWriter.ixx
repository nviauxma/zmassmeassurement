#ifndef NTAUHELPERS_OUTFILEWRITER_IXX
#define NTAUHELPERS_OUTFILEWRITER_IXX

#include <NtauHelpers/OutFileWriter.h>
#include <NtauHelpers/Utils.h>

#include <mutex>
///###################################################################
///     Simple helper class which
///     takes care that Plots are written
///     to the TFile. Any kind of Plot is supported
///    The plots are automatically written as soon as
///    the object is destoryed. Plots with duplicate file-names
///
///#############################################
namespace {
    std::mutex destruct_mutex_plot_writer;
}

template <class T> template <class O> PlotWriter<T>::PlotWriter(const PlotWriter<O>& other) : PlotWriter(other.get()) {}
template <class T> PlotWriter<T>::PlotWriter(const std::shared_ptr<TFile>& out_file) : m_out_file{out_file}, m_to_write{} {}
/// Alternative constructor using the string_path
template <class T> PlotWriter<T>::PlotWriter(const std::string& out_path) : PlotWriter(make_out_file(out_path)) {}

template <class T> std::shared_ptr<TFile> PlotWriter<T>::get() const { return m_out_file; }
template <class T> void PlotWriter<T>::add_plot(const std::shared_ptr<T>& pl, const std::string& save_as) {
    add_plot(Plot<T>(CopyExisting(pl)), save_as);
}

template <class T> void PlotWriter<T>::add_plot(const Plot<T>& pl, const std::string& save_as) {
    if (!m_out_file) {
        throw std::runtime_error("Invalid TFile pointer was given");
        return;
    }
    if (save_as.empty()) {
        std::cerr << "PlotWriter::add_plot() -- Empty file name given " << std::endl;
        return;
    }
    if (save_as.rfind("/") == save_as.size() - 1) {
        std::cerr << "PlotWritter::add_plot() -- Only the directory is given but where is the name in  " << save_as << "?" << std::endl;
        return;
    }
    if (m_to_write.find(save_as) != m_to_write.end()) {
        std::cerr << "There already exists plot to be written " << save_as << std::endl;
        return;
    }
    m_to_write.insert(std::make_pair(save_as, pl));
}

template <class T> PlotWriter<T>::~PlotWriter() {
    if (!m_out_file) return;
    const std::lock_guard<std::mutex> lock(destruct_mutex_plot_writer);

    std::cout << "Closing file: " << m_out_file->GetName() << " will dump " << m_to_write.size() << " objects to it" << std::endl;
    /// It should be a std::pair<std::string,Plot<T>>
    for (auto obj : m_to_write) {
        /// Define the final directory

        TDirectory* write_dir = PlotUtils::mkdir(obj.first, m_out_file.get());
        std::string obj_name = obj.first.substr(obj.first.rfind("/") + 1, std::string::npos);
        if (write_dir->Get(obj_name.c_str())) {
            std::cerr << "File " << m_out_file->GetName() << " already contains an object " << obj.first << "." << std::endl;
            std::cerr << "Will not clobber with existing content. " << std::endl;
            m_to_write.clear();
            break;
        }
        obj.second.populate();
        std::shared_ptr<T> histo_ptr = obj.second.getSharedHisto();
        if (!histo_ptr) {
            std::cout << "Something went Horrible wrong when making this one " << obj.first << std::endl;
            continue;
        }
        write_dir->cd();
        histo_ptr->Write(obj_name.c_str());
    }
}

#endif
