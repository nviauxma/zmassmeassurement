#include "MuonSagittaBias/PhaseSpaceCuts.h"

PhaseSpaceCuts::PhaseSpaceCuts(const std::string& sel_name, const std::string& sel_title,
                               const std::function<bool(SagittaBiasAnalyzer&, int)>& sel_function) :
    m_cut_func(sel_function), m_sel_name(sel_name), m_sel_title(sel_title) {}

Selection<SagittaBiasAnalyzer> PhaseSpaceCuts::make_cut(int trk_type, std::pair<float, float> mass_window) const {
    /// copy first the function to another place in memory to avoid the pipe
    /// of this later in the construction of the selection
    std::function<bool(SagittaBiasAnalyzer&, int)> inner_function = m_cut_func;

    Selection<SagittaBiasAnalyzer> selection{[inner_function, trk_type, mass_window](SagittaBiasAnalyzer& t) {
        return inner_function(t, trk_type) && t.pass_base_kine_sel(trk_type, mass_window);
    }};
    selection.setID(selection_name(trk_type, mass_window));
    /// Optionally we can specify the name of the selection
    return selection;
}
std::string PhaseSpaceCuts::selection_name(int trk_type, std::pair<float, float> mass_window) const {
    return name() + MuonTrk::to_string(trk_type) + "Mll_" + std::to_string(mass_window.first) + "_to_" + std::to_string(mass_window.second);
}
std::string PhaseSpaceCuts::name() const { return m_sel_name; }
std::string PhaseSpaceCuts::title() const { return m_sel_title; }