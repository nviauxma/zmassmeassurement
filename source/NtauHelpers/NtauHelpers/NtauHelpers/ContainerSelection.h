#ifndef NTAUHELPERS_CONTAINERSELECTION_H
#define NTAUHELPERS_CONTAINERSELECTION_H
#include <NtauHelpers/Utils.h>

///     Helper class to create plots over particle selections represented by a
///     a set of std::vectors having each the same length per particle
///
template <class ProcessThis> class ContainerSelection {
public:
    /// Standard constructor with the minimal requirement to provide the selection
    ///  size_function  -> Function to return the container size
    ///  sel_function   -> Selection function to be applied on each element of the object collection
    /// Unique name of the selection use for the saving of the histograms into PDFs or TFiles
    /// Title: Title plotted on a canvas
    ContainerSelection(const std::function<size_t(ProcessThis&)>& size_function,
                       const std::function<bool(ProcessThis&, size_t)>& sel_function, const std::string& sel_name,
                       const std::string& sel_title);

    /// Returns the name of a selection
    std::string name() const;
    /// Returns the title of a selection
    std::string title() const;
    /// Set the title afterwards
    void set_title(const std::string& new_title);

    /// Returns the size function of a given selection
    const std::function<size_t(ProcessThis&)>& size() const;
    /// Returns the actual selection
    const std::function<bool(ProcessThis&, size_t)>& obj_cut() const;

private:
    std::string m_name{};

    std::string m_title{};

    std::function<size_t(ProcessThis&)> m_container_size;

    std::function<bool(ProcessThis&, size_t)> m_cut_func;
};

/// Generates a new Container selection object where each object has to satisfy
/// both selection criteria
///  name -> <name_A>__AND__<name_B>
/// selection -> <selection_A> #wedge <selection_b>
template <class ProcessThis>
ContainerSelection<ProcessThis> operator&&(const ContainerSelection<ProcessThis>& a, const ContainerSelection<ProcessThis>& b);
template <class ProcessThis>
ContainerSelection<ProcessThis> operator||(const ContainerSelection<ProcessThis>& a, const ContainerSelection<ProcessThis>& b);
/// Ceates a new selection as the logical NOT of the input object
template <class ProcessThis> ContainerSelection<ProcessThis> operator!(const ContainerSelection<ProcessThis>& to_invert);

#include <NtauHelpers/ContainerSelection.ixx>
#endif
