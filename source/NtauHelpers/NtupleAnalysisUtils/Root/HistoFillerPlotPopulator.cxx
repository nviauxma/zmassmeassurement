#include "NtupleAnalysisUtils/HistoFiller/HistoFillerPlotPopulator.h"


void AddHistos(std::shared_ptr<TH1> addTo, std::shared_ptr<TH1> addThis){
    addTo->Add(addThis.get());
}
void AddHistos(std::shared_ptr<TH2> addTo, std::shared_ptr<TH2> addThis){
    addTo->Add(addThis.get());
}
void AddHistos(std::shared_ptr<TH3> addTo, std::shared_ptr<TH3> addThis){
    addTo->Add(addThis.get());
}
void AddHistos(std::shared_ptr<TH1D> addTo, std::shared_ptr<TH1D> addThis){
    addTo->Add(addThis.get());
}
void AddHistos(std::shared_ptr<TH1F> addTo, std::shared_ptr<TH1F> addThis){
    addTo->Add(addThis.get());
}
void AddHistos(std::shared_ptr<TH2D> addTo, std::shared_ptr<TH2D> addThis){
    addTo->Add(addThis.get());
}
void AddHistos(std::shared_ptr<TH3D> addTo, std::shared_ptr<TH3D> addThis){
    addTo->Add(addThis.get());
}
void AddHistos(std::shared_ptr<TH2F> addTo, std::shared_ptr<TH2F> addThis){
    addTo->Add(addThis.get());
}
void AddHistos(std::shared_ptr<TProfile> addTo, std::shared_ptr<TProfile> addThis){
    addTo->Add(addThis.get());
}
void AddHistos(std::shared_ptr<TEfficiency> addTo, std::shared_ptr<TEfficiency> addThis){
    addTo->Add(*addThis.get());
}


void ResetHisto(std::shared_ptr<TH1> resetThis){
    resetThis->Reset();
}
void ResetHisto(std::shared_ptr<TH2> resetThis){
    resetThis->Reset();
}
void ResetHisto(std::shared_ptr<TH3> resetThis){
    resetThis->Reset();
}
void ResetHisto(std::shared_ptr<TH1D> resetThis){
    resetThis->Reset();
}
void ResetHisto(std::shared_ptr<TH1F> resetThis){
    resetThis->Reset();
}
void ResetHisto(std::shared_ptr<TH2D> resetThis){
    resetThis->Reset();
}
void ResetHisto(std::shared_ptr<TH3D> resetThis){
    resetThis->Reset();
}
void ResetHisto(std::shared_ptr<TH2F> resetThis){
    resetThis->Reset();
}
void ResetHisto(std::shared_ptr<TProfile> resetThis){
    resetThis->Reset();
}
void ResetHisto(std::shared_ptr<TEfficiency> resetThis){
    /// WHY OH WHY DOES TEFFICIENCY NOT SUPPORT A RESET? 
    std::unique_ptr<TH1> passed {resetThis->GetCopyPassedHisto()}; 
    std::unique_ptr<TH1> total {resetThis->GetCopyTotalHisto()}; 
    passed->Reset();
    total->Reset(); 
    /// This is so pointless... 
    resetThis->SetPassedHistogram(*passed,"f");
    resetThis->SetTotalHistogram(*total,"f");
 }