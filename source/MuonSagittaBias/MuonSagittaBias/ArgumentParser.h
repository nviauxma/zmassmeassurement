#ifndef NTAU__ArgumentParser__H
#define NTAU__ArgumentParser__H

#include <MuonSagittaBias/Utils.h>

class ArgumentParser {
public:
    ArgumentParser() = default;
    virtual ~ArgumentParser() = default;

    ArgumentParser(const ArgumentParser& other) = delete;
    ArgumentParser& operator=(const ArgumentParser& other) = delete;

    /// Parse the arguments
    bool parse_args(int argc, char* argv[]);
    bool parse_args(std::vector<std::string> arg_vec);

    ///  The following methods are only available if the parse_args method
    ///  was called before-hand

    /// Name of the application
    std::string app_name() const;

    /// Prepares the final sample to run the analysis on
    template <class ana_type> Sample<ana_type> prepare_sample() const;

    /// Returns the pointer to the final output file
    std::shared_ptr<TFile> out_file();

    /// Returns whether the input sample is data or simulation
    bool is_mc() const;
    /// If a Sagitta-injection file was given, a bitmask of all track-types is returned
    /// for which all sagitta constants were defined to be 0 in the injection file
    UserSetting<int, ArgumentParser> nominal_trks{MuonTrk::TrackType::IDTrk, this};

    /// Returns the list of track types to be stored in a ROOT file
    std::vector<int> get_trk_types_ROOT_file() const;

    /// Path of the tree to be read from the ROOT file. The current format stores all information
    /// in a single tree. So actually this argument is ideally never changed
    UserSetting<std::string, ArgumentParser> tree_name{"SagittaCalib/Reco/MuonCandidates_OC/TPTree_MuonCandidates_OC", this};
    /// File to be used to load the cross-section database
    /// Use the standard one for now which is hopefully no longer erased
    UserSetting<std::string, ArgumentParser> cross_sec_db{"dev/PMGTools/PMGxsecDB_mc16.txt", this};
    /// In NLO samples, theory uncertainties are assessed by reweighting the events, called Lhe variations
    /// The Lhe variations have their own sum of weights. This property
    UserSetting<unsigned int, ArgumentParser> lhe_variation{0, this};

    ///  Number of threads to be used at maximum on your machine
    UserSetting<unsigned int, ArgumentParser> num_threads{16, this};

    /// Sagitta injection file configuration file containing the names of each
    ///     sagitta bias to be injected in the analysis
    UserSetting<std::string, ArgumentParser> inject_config_file{"", this};

    /// Directory to store all plots
    UserSetting<std::string, ArgumentParser> out_dir{"Plots/", this};
    /// Name of the summary PDF
    UserSetting<std::string, ArgumentParser> summary_pdf{"AllPlots", this};
    /// Name of the output ROOT file
    UserSetting<std::string, ArgumentParser> out_root_file{"OutFile.root", this};

    /// Files to be used for the analysis
    UserSetting<std::vector<std::string>, ArgumentParser> in_files{{}, this};

    /// Skip the first n files of the input files for this job
    UserSetting<unsigned int, ArgumentParser> begin{0, this};
    /// Parse only the (begin -- end) files to this job
    UserSetting<unsigned int, ArgumentParser> end{0, this};

    /// Steers whether the executable shall create a PDF
    /// as output or not. That's useful for batch job where
    /// the input files are split across different computing nodes
    UserSetting<bool, ArgumentParser> make_pdfs{true, this};
    /// Steers whether the executable shall create a ROOT file
    ///  as output or not.
    UserSetting<bool, ArgumentParser> make_root_file{true, this};

    /// Generic properties to setup the prwTool for various puposes

    /// The lumicalc files are one ingredient to setup the PRW tool
    UserSetting<std::vector<std::string>, ArgumentParser> lumi_calc_files{
        {
            "GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root",
            "GoodRunsLists/data16_13TeV/20180129/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-009.root",
            "GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root",
            "GoodRunsLists/data18_13TeV/20190318/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-010.root",
        },
        this};

    UserSetting<std::vector<std::string>, ArgumentParser> prw_mc_cfg_files{
        {
            "dev/PileupReweighting/share/DSID361xxx/pileup_mc16a_dsid361107_FS.root",
            "dev/PileupReweighting/share/DSID361xxx/pileup_mc16d_dsid361107_FS.root",
            "dev/PileupReweighting/share/DSID361xxx/pileup_mc16e_dsid361107_FS.root",

        },
        this};
    /// Switch to setup the prw tool during initialization. The switch is useless if
    /// One of the above properties is userSet and not empty or if one of them is empty
    UserSetting<bool, ArgumentParser> initialize_prw{false, this};

    /// Defines the strategy for treating trigger prescale. The options are either
    ///     NotApplied : Do not account for different prescales in data & MC
    ///     UnPreScaleData: Assign an event weight to each data event  --> large stat error?
    ///     PreScaleMC: Assign the inverse weight to simluation. Requires then the parsing of the
    ///                 prescale calibration file
    UserSetting<int, ArgumentParser> prescale_strategy{TriggerPreScale::Strategy::NotApplied, this};

    /// Defines the path of the prescale calibration file. The argument parsing fails if the
    /// Strategy is to unprescale the simulation, the file does not exist and the job runs on simulation
    UserSetting<std::string, ArgumentParser> prescale_calib_file{"", this};

    /// List of triggers to be considered for the reweighting of each event
    UserSetting<std::string, ArgumentParser> prescaled_triggers{"MuonSagittaBias/triggers_for_prescaling.txt", this};

    /// Assembles the list of ROOT files to be run over considering the constraints
    /// coming from the --begin and --end arguments
    std::vector<std::string> get_job_files() const;

protected:
    // virtual bool eval_custom_args(const std::vector<std::string>& arg_vec, unsigned int& current_itr) ;

    virtual void print_help() const;

private:
    void reset_in_files();
    bool eval_args(const std::vector<std::string>& arg_vec);

    std::string m_app_name{"not yet parsed"};

    bool m_args_parsed{false};

    /// Helper
    bool m_in_files_cleared{false};
    std::shared_ptr<TFile> m_out_file{nullptr};
};

///
///
///
template <class ana_type> Sample<ana_type> ArgumentParser::prepare_sample() const {
    return make_sample<ana_type>(get_job_files(), tree_name());
}

#endif
