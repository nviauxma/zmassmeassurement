// Please add your package name here
#include "MuonSagittaBias/SagittaBiasTree.h"

SagittaBiasTree::SagittaBiasTree(TTree* t) : NtupleBranchMgr(t) {
    if (t) getMissedBranches(t);
}
