#include <MuonSagittaBias/DataTemplateHolder.h>
#include <MuonSagittaBias/Utils.h>
#include <NtauHelpers/OutFileWriter.h>
#include <TRandom3.h>

#include <chrono>
#include <ctime>
int main(int argc, char* argv[]) {
    SetAtlasStyle();
    std::string in_file_path = "SagittaTemplates.root";
    std::string out_dir = "Plots/";
    double lumi = 36.1;

    for (int k = 1; k < argc; ++k) {
        std::string current_arg(argv[k]);
        if (current_arg == "--inFile" && k + 1 < argc) {
            in_file_path = argv[k + 1];
            ++k;
        } else if (current_arg == "--outDir" && k + 1 < argc) {
            out_dir = argv[k + 1];
            ++k;
        } else if (current_arg == "--lumi" && k + 1 < argc) {
            lumi = atof(argv[k + 1]);
            ++k;
        }
    }
    DataTemplateHolder::set_lumi(lumi);

    CanvasOptions canvas_opts =
        CanvasOptions().LabelStatusTag("Simulation Internal").OutputDir(out_dir).LabelLumiTag(Form("%.0f fb^{-1}", lumi));

    auto multi_page = PlotUtils::startMultiPagePdfFile("AllTestTemplateFits", canvas_opts);
    std::vector<PlotContent<TGraph>> all_templates;

    /// Open the ROOT file
    std::shared_ptr<TFile> in_file = PlotUtils::Open(in_file_path);
    if (!in_file) { return EXIT_FAILURE; }
    /// Read the names of the contained histograms
    std::vector<std::string> histos_in_dir = PlotUtils::getAllObjectsFromDir<TH1>(in_file.get());
    EraseFromVector<std::string>(histos_in_dir, [](const std::string& to_erase) {
        int trk_type = MuonTrk::to_type(to_erase.substr(0, to_erase.find("/")));
        if (trk_type == MuonTrk::TrackType::TruthTrk) return true;
        return to_erase.find("All__PosLepEta/KinSel_etamuPlus_inRANGE_2.00_2/") == std::string::npos && to_erase.find("PseudoMass") != 0;
    });

    std::vector<std::shared_ptr<DataTemplateHolder>> templates;

    for (const std::string& h_template : histos_in_dir) {
        templates.emplace_back(std::make_shared<DataTemplateHolder>(in_file, h_template));
    }
    // templates.clear();
    /// Define a random element to be the pseudo data
    // int seed = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    // TRandom3 pseudo_data_drawer(seed);
    // int pseudo_idx = pseudo_data_drawer.Uniform(templates.size());
    //   auto pseudo_itr = std::find_if(templates.begin(), templates.end(),
    //                                  [](const std::shared_ptr<DataTemplateHolder>& holder) { return holder->inject_resolution() > 0.; });

    PlotWriter<TGraph> out_file{"TestChi2.root"};

    std::sort(templates.begin(), templates.end(),
              [](const std::shared_ptr<DataTemplateHolder>& a, const std::shared_ptr<DataTemplateHolder>& b) { return (*a) < (*b); });

    for (auto& holder : templates) {
        std::shared_ptr<TGraph> chi2_histo = extract_chi2(templates, *holder);
        FitResult sagitta_fit = fit_parabola(chi2_histo);
        if (!sagitta_fit.succeed) continue;
        Plot<TGraph> plot{CopyExisting(chi2_histo), PlotFormat().ExtraDrawOpts("*").MarkerStyle(kFullTriangleUp).Color(kRed)};
        Plot<TGraph> fit_plot{CopyExisting(sagitta_fit.fit_graph), PlotFormat().ExtraDrawOpts("C").Color(kBlack)};
        const std::string plot_name = Form("SAGITTA_%f", holder->inject_bias());
        out_file.add_plot(plot, holder->template_path() + plot_name);

        all_templates += PlotContent<TGraph>{
            {fit_plot, plot},
            holder->to_title() + std::vector<std::string>{Form("a=%.2f, b = %.3f#pm%.3f, c=%.2f", sagitta_fit.slope, sagitta_fit.x_shift,
                                                               sagitta_fit.one_sigma, sagitta_fit.offset),
                                                          Form("#chi^{2}/ n.d.f. %.2f / %u = %.2f ", sagitta_fit.chi2, sagitta_fit.nDoF,
                                                               (sagitta_fit.chi2 / sagitta_fit.nDoF))},
            plot_name,
            multi_page,
            canvas_opts};
    }
    for (auto& pc : all_templates) { DefaultPlotting::draw1DNoRatio(pc); }

    return EXIT_SUCCESS;
}