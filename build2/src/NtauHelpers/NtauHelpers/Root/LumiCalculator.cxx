#include <PileupReweighting/PileupReweightingTool.h>
/// Include the pileup reweighting as top
#include <NtauHelpers/LumiCalculator.h>
#include <NtauHelpers/MuonTPMetaData.h>
#include <NtauHelpers/TriggerHelper.h>
#include <NtauHelpers/Utils.h>

namespace MuonTP {
    //###########################################################
    //                  LumiCalculator
    //###########################################################
    LumiCalculator* LumiCalculator::m_Inst = nullptr;
    LumiCalculator* LumiCalculator::getCalculator() {
        if (!m_Inst) m_Inst = new LumiCalculator();
        return m_Inst;
    }
    bool LumiCalculator::isInitialized() const { return !m_recorded_runs.empty(); }
    LumiCalculator::LumiCalculator() = default;
    LumiCalculator::~LumiCalculator() { m_Inst = nullptr; }

    double LumiCalculator::getLumi2k1516k() const { return m_lumi_1516k; }
    double LumiCalculator::getLumi2k17() const { return m_lumi_17k; }
    double LumiCalculator::getLumi2k18() const { return m_lumi_18k; }
    double LumiCalculator::getLumi() const { return m_total_lumi; }

    double LumiCalculator::getLumi(unsigned int run) const {
        if (isMC16a(run))
            return getLumi2k1516k();
        else if (isMC16d(run))
            return getLumi2k17();
        else if (isMC16e(run))
            return getLumi2k18();
        return getLumi();
    }
    void LumiCalculator::SetPRWlumi(const std::vector<std::string>& lumi) { m_lumi_calc += lumi; }
    void LumiCalculator::SetPRWconfig(const std::vector<std::string>& config) { m_lumi_config += config; }

    double LumiCalculator::getExpectedLumiFromRun(unsigned int run) const {
        std::shared_ptr<CP::IPileupReweightingTool> prwTool = GetPrwTool(run);
        if (!prwTool) return std::nan("nan");
        if (!isElementInList(getRunsInLumiCalc(), run)) return 0.;
        return prwTool->expert()->GetIntegratedLumi(run, run);
    }
    double LumiCalculator::getExpectedLumiFromRun(unsigned int run, unsigned int block) const {
        std::shared_ptr<CP::IPileupReweightingTool> prwTool = GetPrwTool(run);
        if (!prwTool) return std::nan("nan");
        if (!isElementInList(getRunsInLumiCalc(), run)) return 0.;
        return prwTool->expert()->GetLumiBlockIntegratedLumi(run, block);
    }
    std::vector<unsigned int> LumiCalculator::getRunsInLumiCalc() const {
        std::vector<unsigned int> runs;
        runs.reserve(m_recorded_runs.size());
        for (const auto& run_lumi : m_recorded_runs) { runs.push_back(run_lumi.first); }
        return runs;
    }

    double LumiCalculator::getRecordedLumiFromRun(unsigned int run) const {
        std::shared_ptr<CP::IPileupReweightingTool> prwTool = GetPrwTool(run);
        if (!prwTool) return std::nan("nan");
        if (!isElementInList(NormalizationDataBase::getDataBase()->GetRunNumbers(), run)) return 0.;
        double Lumi = 0.;
        for (auto& Block : NormalizationDataBase::getDataBase()->GetTotalLumiBlocks(run)) {
            Lumi += prwTool->expert()->GetLumiBlockIntegratedLumi(run, Block);
        }
        return Lumi;
    }
    double LumiCalculator::getExpectedLumi(unsigned int begin, unsigned int end) const {
        double Lumi = 0.;
        for (auto& run : getRunsInLumiCalc()) {
            if (run > end) break;
            if (run < begin) continue;
            Lumi += getExpectedLumiFromRun(run);
        }
        return Lumi;
    }
    double LumiCalculator::getRecordedLumi(unsigned int begin, unsigned int end) const {
        double Lumi = 0.;
        for (auto& run : NormalizationDataBase::getDataBase()->GetRunNumbers()) {
            if (run > end) break;
            if (run < begin) continue;
            Lumi += getRecordedLumiFromRun(run);
        }
        return Lumi;
    }
    bool LumiCalculator::InitPileUpTool() {
        if (isInitialized()) return true;
        std::cout << "LumiCalculcator::InitPileUpTool() --- Starting to setup the prwTool(s)" << std::endl;
        std::cout << "LumiCalculcator::InitPileUpTool() --- Load the mc-profiles first" << std::endl;
        std::map<unsigned int, std::vector<std::string>> prw_channels = readMCchannelsPRW();
        std::cout << "LumiCalculcator::InitPileUpTool() --- Now load the lumi-calc files" << std::endl;
        std::map<unsigned int, std::vector<std::string>> ordered_lumicalcs = orderLumiCalcFiles();
        if ((prw_channels.empty() && m_lumi_config.empty()) || ordered_lumicalcs.empty()) {
            std::cerr << "LumiCalculator::SetupPRWTool() --- No channels could be extracted" << std::endl;
            return false;
        }
        if (!CreateTool(ordered_lumicalcs, prw_channels, TriggerHelper::year16, m_prwTool_1516k)) {
            return false;
        } else if (!CreateTool(ordered_lumicalcs, prw_channels, TriggerHelper::year17, m_prwTool_17k)) {
            return false;
        } else if (!CreateTool(ordered_lumicalcs, prw_channels, TriggerHelper::year18, m_prwTool_18k)) {
            return false;
        }

        if (!prw_channels.empty()) {
            m_lumi_config.clear();
            for (const auto& cfg : prw_channels) m_lumi_config += cfg.second;
            if (!CreateTool(m_prwTool, m_lumi_config, m_lumi_calc, "prwTool")) return false;
        }
        for (auto& run : getRunsInLumiCalc()) {
            if (TriggerHelper::is_2k15(run))
                m_lumi_1516k += getExpectedLumiFromRun(run);
            else if (TriggerHelper::is_2k16(run))
                m_lumi_1516k += getExpectedLumiFromRun(run);
            else if (TriggerHelper::is_2k17(run))
                m_lumi_17k += getExpectedLumiFromRun(run);
            else
                m_lumi_18k += getExpectedLumiFromRun(run);
        }
        m_total_lumi = m_lumi_1516k + m_lumi_17k + m_lumi_18k;
        m_lumi_calc.clear();
        m_lumi_config.clear();
        return true;
    }
    int LumiCalculator::CreateTool(const std::map<unsigned int, std::vector<std::string>>& ordered_lumi,
                                   std::map<unsigned int, std::vector<std::string>>& prw_config_fles, unsigned int period_num,
                                   std::shared_ptr<CP::IPileupReweightingTool>& prwTool) {
        std::map<unsigned int, std::vector<std::string>>::iterator cfg = prw_config_fles.find(period_num);
        std::map<unsigned int, std::vector<std::string>>::const_iterator lumi = ordered_lumi.find(period_num);
        if (cfg == prw_config_fles.end()) return -1;
        if (lumi == ordered_lumi.end()) return -1;
        if (!CreateTool(prwTool, cfg->second, lumi->second, Form("prwTool_%u", period_num))) return 0;
        prw_config_fles.erase(cfg);
        return 1;
    }

    bool LumiCalculator::CreateTool(std::shared_ptr<CP::IPileupReweightingTool>& toolToCreate, const std::vector<std::string>& config_files,
                                    const std::vector<std::string>& lumi_files, const std::string& toolName) {
        std::shared_ptr<CP::PileupReweightingTool> prwTool = std::make_shared<CP::PileupReweightingTool>(toolName);
        toolToCreate = prwTool;
        if (!NormalizationDataBase::getDataBase()->isData()) {
            int firstrun(-1), lastrun(-1);
            UInt_t periodNumber(-1);

            if (getRunsInLumiCalc().size()) {
                firstrun = getRunsInLumiCalc().at(0);
                lastrun = getRunsInLumiCalc().at(getRunsInLumiCalc().size() - 1);
                if (config_files.size()) {
                    std::shared_ptr<TFile> f = PlotUtils::Open(ResolveFilePath(config_files.at(0)));
                    if (f && f->IsOpen()) {
                        TTree* t = nullptr;
                        f->GetObject("PileupReweighting/MCPileupReweighting", t);
                        if (!t) {
                            std::cerr << "LumiCalculator::CreateTool() --- No pile up information in " << f->GetName() << std::endl;
                            return false;
                        }
                        t->SetBranchAddress("RunNumber", &periodNumber);
                        if (t->GetEntries()) t->GetEntry(0);
                    }
                }
            }
            if (firstrun != -1 && !isBulkMC(periodNumber) &&
                !prwTool->setProperty("PeriodAssignments", std::vector<int>({int(periodNumber), firstrun, lastrun})).isSuccess())
                return false;
        }
        if (!prwTool->setProperty("ConfigFiles", GetPathResolvedFileList(config_files)).isSuccess())
            return false;
        else if (!prwTool->setProperty("LumiCalcFiles", GetPathResolvedFileList(lumi_files)).isSuccess())
            return false;
        else if (!prwTool->setProperty("UnrepresentedDataThreshold", 0.2).isSuccess())
            return false;  // needed for checking high-luminosity runs, since there is much unrepresented data
        else if (!prwTool->initialize().isSuccess()) {
            std::cerr << "LumiCalculator::SetupPRWTool() --- Could not setup the PileupReweightingTool." << std::endl;
            return false;
        }
        std::cout << "LumiCalculator::SetupPRWTool() --- new Tool created " << prwTool->name()
                  << " using the following ilumi calc(---) & config files(***)" << std::endl;
        for (const auto& cfg : config_files) std::cout << "     ***" << cfg << std::endl;
        for (const auto& cfg : lumi_files) std::cout << "     ---" << cfg << std::endl;
        return true;
    }
    bool LumiCalculator::isGoodLumiBlock(unsigned int run, unsigned int block) const {
        std::shared_ptr<CP::IPileupReweightingTool> prwTool = m_prwTool;
        if (!prwTool) return false;
        return prwTool->expert()->GetLumiBlockIntegratedLumi(run, block) > 0.;
    }
    unsigned int LumiCalculator::GetNLumiBlocks(unsigned int run) const {
        for (const auto& record : m_recorded_runs) {
            if (record.first == run) return record.second;
        }
        return 0;
    }
    std::set<unsigned int> LumiCalculator::goodLumiBlocks(unsigned int run) const {
        std::set<unsigned int> lumi_blocks;
        unsigned int n_blocks = GetNLumiBlocks(run);
        for (unsigned int block = 0; block <= n_blocks; ++block) {
            if (isGoodLumiBlock(run, block)) lumi_blocks.insert(block);
        }
        return lumi_blocks;
    }

    bool LumiCalculator::isData() const { return NormalizationDataBase::getDataBase()->isData(); }
    std::shared_ptr<CP::IPileupReweightingTool> LumiCalculator::GetPrwTool(unsigned int run) const {
        if (m_prwTool_1516k && (TriggerHelper::is_2k15(run) || TriggerHelper::is_2k16(run))) { return m_prwTool_1516k; }
        if (m_prwTool_17k && TriggerHelper::is_2k17(run)) return m_prwTool_17k;
        if (m_prwTool_18k && TriggerHelper::is_2k18(run)) return m_prwTool_18k;
        return m_prwTool;
    }
    std::shared_ptr<CP::IPileupReweightingTool> LumiCalculator::GetPrwToolForMC(unsigned int period) const {
        if (m_prwTool_1516k && isMC16a(period)) { return m_prwTool_1516k; }
        if (m_prwTool_17k && isMC16d(period)) return m_prwTool_17k;
        if (m_prwTool_18k && isMC16e(period)) return m_prwTool_18k;
        return m_prwTool;
    }

    bool LumiCalculator::isMC16a(unsigned int period_number) const { return TriggerHelper::is_mc16a(period_number); }
    bool LumiCalculator::isMC16d(unsigned int period_number) const { return TriggerHelper::is_mc16d(period_number); }
    bool LumiCalculator::isMC16e(unsigned int period_number) const { return TriggerHelper::is_mc16e(period_number); }
    bool LumiCalculator::isBulkMC(unsigned int period_number) const {
        return isMC16a(period_number) || isMC16d(period_number) || isMC16e(period_number);
    }
    std::map<unsigned int, std::vector<std::string>> LumiCalculator::readMCchannelsPRW() const {
        std::vector<std::vector<std::string>> ordered_files;
        // Read the mc_channels per prw file to sort out
        // if mc16a and mc16d have been given simultaneously
        // https://gitlab.cern.ch/atlas/athena/blob/21.2/PhysicsAnalysis/AnalysisCommon/PileupReweighting/Root/TPileupReweighting.cxx#L308
        std::map<std::string, prwFileContent> mc_channels;
        for (const std::string& config_file : m_lumi_config) { ReadMCprwFile(config_file, mc_channels[config_file]); }
        for (const auto& channel : mc_channels) {
            if (coverDifferentProfile(channel.second, channel.second)) {
                std::cerr << "LumiCalculator::readMCchannelsPRW() --- The file " << channel.first
                          << " contais two prw periods, while only one per period is allowed" << std::endl;
                return {};
            }
            if (insertNewChannel(mc_channels, channel.first, ordered_files)) {
                ordered_files.push_back(std::vector<std::string>{channel.first});
            }
        }
        std::map<unsigned int, std::vector<std::string>> prw_channels;
        for (auto& config_files : ordered_files) {
            if (config_files.empty()) continue;
            const prwFileContent& prw_channel = mc_channels.at(*config_files.begin());
            if (prw_channel.empty()) continue;
            const int period_number = (*prw_channel.begin()).second;
            int key = 0;
            if (TriggerHelper::is_mc16a(period_number))
                key = TriggerHelper::year16;
            else if (TriggerHelper::is_mc16d(period_number))
                key = TriggerHelper::year17;
            else if (TriggerHelper::is_mc16e(period_number))
                key = TriggerHelper::year18;

            prw_channels.insert(std::pair<unsigned int, std::vector<std::string>>(key, config_files));
        }
        return prw_channels;
    }
    bool LumiCalculator::insertNewChannel(const std::map<std::string, prwFileContent>& prwConfig, const std::string& candidate,
                                          std::vector<std::string>& used_files) const {
        const prwFileContent& candidate_content = prwConfig.at(candidate);
        for (auto& config_file : used_files) {
            const prwFileContent& used_content = prwConfig.at(config_file);
            if (coverDifferentProfile(candidate_content, used_content)) return true;
        }
        used_files.push_back(candidate);
        return false;
    }
    bool LumiCalculator::insertNewChannel(const std::map<std::string, prwFileContent>& prwConfig, const std::string& candidate,
                                          std::vector<std::vector<std::string>>& used_files) const {
        for (auto& used : used_files) {
            if (!insertNewChannel(prwConfig, candidate, used)) return false;
        }
        return true;
    }
    void LumiCalculator::ReadLumiCalcFile(const std::string& path, std::vector<LumiCalculator::Run_Nblocks>& runset) const {
        std::shared_ptr<TFile> File = PlotUtils::Open(ResolveFilePath(path));
        if (!File || !File->IsOpen()) {
            std::cerr << "LumiCalculator::ReadLumiCalcFile() --- Could not open " << path << std::endl;
            return;
        }
        TTree* Tree{nullptr};
        File->GetObject("LumiMetaData", Tree);
        if (!Tree) {
            std::cerr << "LumiCalculator::ReadLumiCalcFile() --- Could not read the LumiMetaData tree" << std::endl;
            return;
        }
        unsigned int run{0};
        if (Tree->SetBranchAddress("RunNbr", &run) != 0) { return; }
        long nEntries = Tree->GetEntries();
        runset.reserve(runset.capacity() + nEntries);
        for (long i = 0; i < nEntries; ++i) {
            Tree->GetEntry(i);
            TH1* lumi_histo = nullptr;
            File->GetObject(Form("run%u_intlumi", run), lumi_histo);
            runset.push_back(Run_Nblocks(run, lumi_histo->GetXaxis()->GetBinUpEdge(lumi_histo->GetNbinsX())));
        }
        std::sort(runset.begin(), runset.end(), [](const Run_Nblocks& a, const Run_Nblocks& b) { return a.first < b.first; });
    }
    void LumiCalculator::ReadMCprwFile(const std::string& path, prwFileContent& channels) const {
        std::shared_ptr<TFile> File = PlotUtils::Open(ResolveFilePath(path));
        if (!File || !File->IsOpen()) {
            std::cerr << "LumiCalculator::ReadMCprwFile()  --- Could not open " << path << std::endl;
            return;
        }
        TTree* Tree = nullptr;
        File->GetObject("PileupReweighting/MCPileupReweighting", Tree);
        if (!Tree) {
            std::cerr << "LumiCalculator::ReadMCprwFile() --- Could not read the MCPileupReweighting tree" << std::endl;
            return;
        }
        unsigned int run{0};
        int mc{0};
        if (Tree->SetBranchAddress("RunNumber", &run) != 0 || Tree->SetBranchAddress("Channel", &mc) != 0) { return; }
        long nEntries = Tree->GetEntries();
        for (long i = 0; i < nEntries; ++i) {
            Tree->GetEntry(i);
            prwChannel entry(mc, run);
            if (!isElementInList(channels, entry)) channels.emplace_back(std::move(entry));
        }
        std::sort(channels.begin(), channels.end(), [](const prwChannel& a, const prwChannel& b) { return a.first < b.first; });
    }
    bool LumiCalculator::coverDifferentProfile(const prwFileContent& first_file, const prwFileContent& second_file) const {
        for (auto& first_mc_run : first_file) {
            for (auto& second_mc_run : second_file) {
                if (first_mc_run.second != second_mc_run.second) return true;
            }
        }
        return false;
    }
    std::map<unsigned int, std::vector<std::string>> LumiCalculator::orderLumiCalcFiles() {
        std::map<unsigned int, std::vector<std::string>> years_lumicalc;

        for (auto& lumi_file : m_lumi_calc) {
            std::vector<Run_Nblocks> runs;
            ReadLumiCalcFile(lumi_file, runs);
            if (runs.empty()) continue;
            unsigned int first_run{runs.begin()->first}, year{INT_MAX};
            if (TriggerHelper::is_2k15(first_run) || TriggerHelper::is_2k16(first_run)) {
                year = TriggerHelper::year16;
            } else if (TriggerHelper::is_2k17(first_run)) {
                year = TriggerHelper::year17;
            } else if (TriggerHelper::is_2k18(first_run)) {
                year = TriggerHelper::year18;
            }
            years_lumicalc[year] += lumi_file;
            m_recorded_runs += runs;
        }
        std::sort(m_recorded_runs.begin(), m_recorded_runs.end(),
                  [](const Run_Nblocks& a, const Run_Nblocks& b) { return a.first < b.first; });
        return years_lumicalc;
    }
    //######################################################
    //          LumiInterval
    //######################################################
    LumiInterval::LumiInterval(unsigned int bin) : m_bin{bin} {}

    void LumiInterval::set_start(unsigned int run, unsigned int lumi_block) {
        m_start_run = run;
        m_start_block = lumi_block;
    }
    void LumiInterval::set_end(unsigned int run, unsigned int lumi_block) {
        m_end_run = run;
        m_end_block = std::min(lumi_block, LumiCalculator::getCalculator()->GetNLumiBlocks(run));
    }

    bool LumiInterval::in_interval(unsigned int run, unsigned int lumi_block) const {
        if (run < start_run() || run > end_run())
            return false;
        else if (run == start_run() && lumi_block < start_block())
            return false;
        else if (run == end_run() && lumi_block > end_block())
            return false;
        return true;
    }

    unsigned int LumiInterval::start_run() const { return m_start_run; }
    unsigned int LumiInterval::end_run() const { return m_end_run; }
    unsigned int LumiInterval::start_block() const { return m_start_block; }
    unsigned int LumiInterval::end_block() const { return m_end_block; }
    unsigned int LumiInterval::bin_number() const { return m_bin; }
    void LumiInterval::set_lumi(double l) { m_lumi = l; }
    double LumiInterval::lumi() const { return m_lumi; }
    //#####################################################################
    //                      LumiSlicer
    //#####################################################################
    LumiSlicer::LumiSlicer(double slice_size) : m_blocks() {
        double int_lumi = 0;
        std::shared_ptr<LumiInterval> block = std::make_shared<LumiInterval>(0);
        for (const auto& run : LumiCalculator::getCalculator()->getRunsInLumiCalc()) {
            unsigned int n = LumiCalculator::getCalculator()->GetNLumiBlocks(run);
            for (unsigned int b = 0; b <= n; ++b) {
                if (block->start_run() > run) block->set_start(run, b);
                int_lumi += LumiCalculator::getCalculator()->getExpectedLumiFromRun(run, b);
                if (int_lumi < slice_size) continue;
                block->set_end(run, b);
                block->set_lumi(int_lumi);
                m_blocks.push_back(block);
                block = std::make_shared<LumiInterval>(block->bin_number() + 1);
                int_lumi = 0;
            }
        }
        block->set_lumi(int_lumi);
        m_blocks.push_back(block);
    }
    LumiSlicer::LumiSlicer() : m_blocks() {
        std::shared_ptr<LumiInterval> block = std::make_shared<LumiInterval>(0);
        for (const auto& run : LumiCalculator::getCalculator()->getRunsInLumiCalc()) {
            block->set_start(run, 0);
            block->set_end(run, LumiCalculator::getCalculator()->GetNLumiBlocks(run));
            block->set_lumi(LumiCalculator::getCalculator()->getExpectedLumi(run, run));
            m_blocks.push_back(block);
            block = std::make_shared<LumiInterval>(block->bin_number() + 1);
        }
    }
    unsigned int LumiSlicer::find_bin(unsigned int run, unsigned int lumi_block) const {
        for (const auto& block : m_blocks) {
            if (block->in_interval(run, lumi_block)) return block->bin_number();
        }
        return -1;
    }
    std::shared_ptr<LumiInterval> LumiSlicer::get_block(unsigned int bin) const {
        if (bin - 1 < size()) return m_blocks.at(bin - 1);
        return std::shared_ptr<LumiInterval>();
    }
    size_t LumiSlicer::size() const { return m_blocks.size(); }
}  // namespace MuonTP
