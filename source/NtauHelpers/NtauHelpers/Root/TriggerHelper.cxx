#include <NtauHelpers/TriggerHelper.h>

/*
[20:27][junggjo9@mppui4:source]$ python NtauHelpers/ClusterSubmission/python/PeriodRunConverter.py
INFO : Found 132 runs in year 15
INFO : Found 206 runs in year 16
INFO : Found 226 runs in year 17
INFO : Found 247 runs in year 18
Found the following periods for year: 15
Period D from                           *** 276073 --- 276954
Period E from                           *** 278727 --- 279928
Period F from                           *** 279932 --- 280422
Period G from                           *** 280423 --- 281075
Period H from                           *** 281130 --- 281411
Period I from                           *** 281662 --- 282482
Period J from                           *** 282625 --- 284484
Found the following periods for year: 16
Period A from                           *** 296939 --- 300287
Period B from                           *** 300345 --- 300908
Period C from                           *** 301912 --- 302393
Period D from                           *** 302737 --- 303560
Period E from                           *** 303638 --- 303892
Period F from                           *** 303943 --- 304494
Period G from                           *** 305291 --- 306714
Period H from                           *** 305359 --- 310216
Period I from                           *** 307124 --- 308084
Period J from                           *** 308979 --- 309166
Period K from                           *** 309311 --- 309759
Period L from                           *** 310015 --- 311481
Period Z from                           *** 299390 --- 310216
Found the following periods for year: 17
Period A from                           *** 324320 --- 325558
Period B from                           *** 325713 --- 328393
Period C from                           *** 329385 --- 330470
Period D from                           *** 330857 --- 332304
Period E from                           *** 332720 --- 334779
Period F from                           *** 334842 --- 335290
Period G from                           *** 335302 --- 335302
Period H from                           *** 336497 --- 336782
Period I from                           *** 336832 --- 337833
Period K from                           *** 338183 --- 340453
Period N from                           *** 341257 --- 341649
Found the following periods for year: 18
Period A from                           *** 348197 --- 348836
Period B from                           *** 348534 --- 361844
Period C from                           *** 349534 --- 350220
Period D from                           *** 350310 --- 352107
Period E from                           *** 352123 --- 352137
Period F from                           *** 352274 --- 352514
Period G from                           *** 354107 --- 354494
Period H from                           *** 354826 --- 355224
Period I from                           *** 355261 --- 355273
Period J from                           *** 355331 --- 355468
Period K from                           *** 355529 --- 356259
Period L from                           *** 357050 --- 359171
Period M from                           *** 359191 --- 360414
Period N from                           *** 361635 --- 361696
Period O from                           *** 361738 --- 363400
Period Q from                           *** 363664 --- 364292
Period R from                           *** 364485 --- 364485
*/
bool TriggerHelper::is_mc16a(int period_number) {
    constexpr int prw_period_mc16a = 284500;
    return period_number == prw_period_mc16a;
}
bool TriggerHelper::is_mc16d(int period_number) {
    constexpr int prw_period_mc16d = 300000;
    return period_number == prw_period_mc16d;
}
bool TriggerHelper::is_mc16e(int period_number) {
    constexpr int prw_period_mc16e = 310000;
    return period_number == prw_period_mc16e;
}
int TriggerHelper::get_period_year(int run_number) {
    int p_year{0};

    constexpr int last_run_15 = 284484;

    //   constexpr int first_run_16 = 296939;
    //   constexpr int last_run_16D3 = 302919;

    constexpr int first_run_17 = 324320;
    constexpr int first_run_18 = 348197;

    constexpr int first_run_21 = 364485 + 10;

    if (run_number <= last_run_15) {
        constexpr int last_run_d = 276954;
        constexpr int last_run_e = 279928;
        constexpr int last_run_f = 280422;
        constexpr int last_run_g = 281075;
        constexpr int last_run_h = 281411;
        constexpr int last_run_i = 282482;
        constexpr int last_run_j = 284484;

        p_year |= year15;
        if (run_number <= last_run_d)
            p_year |= periodD;
        else if (run_number <= last_run_e)
            p_year |= periodE;
        else if (run_number <= last_run_f)
            p_year |= periodF;
        else if (run_number <= last_run_g)
            p_year |= periodG;
        else if (run_number <= last_run_h)
            p_year |= periodH;
        else if (run_number <= last_run_i)
            p_year |= periodI;
        else if (run_number <= last_run_j)
            p_year |= periodJ;
    } else if (run_number < first_run_17) {
        constexpr int last_run_a = 300287;
        constexpr int last_run_b = 300908;
        constexpr int last_run_c = 302393;
        constexpr int last_run_d = 303560;
        constexpr int last_run_e = 303892;
        constexpr int last_run_f = 304494;
        constexpr int last_run_g = 306714;
        constexpr int last_run_h = 310216;
        constexpr int last_run_i = 308084;
        constexpr int last_run_j = 309166;
        constexpr int last_run_k = 309759;
        constexpr int last_run_l = 311481;
        constexpr int last_run_z = 310216;
        p_year |= year16;
        if (run_number <= last_run_a)
            p_year |= periodA;
        else if (run_number <= last_run_b)
            p_year |= periodB;
        else if (run_number <= last_run_c)
            p_year |= periodC;
        else if (run_number <= last_run_d)
            p_year |= periodD;
        else if (run_number <= last_run_e)
            p_year |= periodE;
        else if (run_number <= last_run_f)
            p_year |= periodF;
        else if (run_number <= last_run_g)
            p_year |= periodG;
        else if (run_number <= last_run_h)
            p_year |= periodH;
        else if (run_number <= last_run_i)
            p_year |= periodI;
        else if (run_number <= last_run_j)
            p_year |= periodJ;
        else if (run_number <= last_run_k)
            p_year |= periodK;
        else if (run_number <= last_run_l)
            p_year |= periodL;
        else if (run_number <= last_run_z)
            p_year |= periodZ;
    } else if (run_number < first_run_18) {
        p_year |= year17;

        constexpr int last_run_a = 325558;
        constexpr int last_run_b = 328393;
        constexpr int last_run_c = 330470;
        constexpr int last_run_d = 332304;
        constexpr int last_run_e = 334779;
        constexpr int last_run_f = 335290;
        constexpr int last_run_g = 335302;
        constexpr int last_run_h = 336782;
        constexpr int last_run_i = 337833;
        constexpr int last_run_k = 340453;
        constexpr int last_run_n = 341649;

        if (run_number <= last_run_a)
            p_year |= periodA;
        else if (run_number <= last_run_b)
            p_year |= periodB;
        else if (run_number <= last_run_c)
            p_year |= periodC;
        else if (run_number <= last_run_d)
            p_year |= periodD;
        else if (run_number <= last_run_e)
            p_year |= periodE;
        else if (run_number <= last_run_f)
            p_year |= periodF;
        else if (run_number <= last_run_g)
            p_year |= periodG;
        else if (run_number <= last_run_h)
            p_year |= periodH;
        else if (run_number <= last_run_i)
            p_year |= periodI;
        else if (run_number <= last_run_k)
            p_year |= periodK;
        else if (run_number <= last_run_n)
            p_year |= periodN;

    }

    else if (run_number < first_run_21) {
        constexpr int last_run_a = 348836;
        constexpr int last_run_b = 361844;
        constexpr int last_run_c = 350220;
        constexpr int last_run_d = 352107;
        constexpr int last_run_e = 352137;
        constexpr int last_run_f = 352514;
        constexpr int last_run_g = 354494;
        constexpr int last_run_h = 355224;
        constexpr int last_run_i = 355273;
        constexpr int last_run_j = 355468;
        constexpr int last_run_k = 356259;
        constexpr int last_run_l = 359171;
        constexpr int last_run_m = 360414;
        constexpr int last_run_n = 361696;
        constexpr int last_run_o = 363400;
        constexpr int last_run_q = 364292;
        constexpr int last_run_r = 364485;
        p_year |= year18;
        if (run_number <= last_run_a)
            p_year |= periodA;
        else if (run_number <= last_run_b)
            p_year |= periodB;
        else if (run_number <= last_run_c)
            p_year |= periodC;
        else if (run_number <= last_run_d)
            p_year |= periodD;
        else if (run_number <= last_run_e)
            p_year |= periodE;
        else if (run_number <= last_run_f)
            p_year |= periodF;
        else if (run_number <= last_run_g)
            p_year |= periodG;
        else if (run_number <= last_run_h)
            p_year |= periodH;
        else if (run_number <= last_run_i)
            p_year |= periodI;
        else if (run_number <= last_run_j)
            p_year |= periodJ;
        else if (run_number <= last_run_k)
            p_year |= periodK;
        else if (run_number <= last_run_l)
            p_year |= periodL;
        else if (run_number <= last_run_m)
            p_year |= periodM;
        else if (run_number <= last_run_n)
            p_year |= periodN;
        else if (run_number <= last_run_o)
            p_year |= periodO;
        else if (run_number <= last_run_q)
            p_year |= periodQ;
        else if (run_number <= last_run_r)
            p_year |= periodR;
    }
    return p_year;
}
bool TriggerHelper::is_2k15(int run_number) {
    int p_year = get_period_year(run_number);
    return (p_year & year15);
}
bool TriggerHelper::is_2k16(int run_number) {
    int p_year = get_period_year(run_number);
    return (p_year & year16);
}
bool TriggerHelper::is_2k17(int run_number) {
    int p_year = get_period_year(run_number);
    return (p_year & year17);
}
bool TriggerHelper::is_2k18(int run_number) {
    int p_year = get_period_year(run_number);
    return (p_year & year18);
}

std::string TriggerHelper::to_title(int p_year) {
    std::string year, period;
    if (p_year & years) {
        if ((p_year & years) == year15) {
            year = "data 2015";
        } else if ((p_year & years) == year16) {
            year = "data 2016";
        } else if ((p_year & years) == year17) {
            year = "data 2017";
        } else if ((p_year & years) == year18) {
            year = "data 2018";
        }
    }
    if (p_year & periods) {
        period = std::string{year.empty() ? "" : ", "} + "period ";
        std::string sub_periods{};
        if (p_year & periodA) sub_periods += std::string(sub_periods.empty() ? "" : ",") + "A";
        if (p_year & periodB) sub_periods += std::string(sub_periods.empty() ? "" : ",") + "B";
        if (p_year & periodC) sub_periods += std::string(sub_periods.empty() ? "" : ",") + "C";
        if (p_year & periodD) sub_periods += std::string(sub_periods.empty() ? "" : ",") + "D";
        if (p_year & periodE) sub_periods += std::string(sub_periods.empty() ? "" : ",") + "E";
        if (p_year & periodF) sub_periods += std::string(sub_periods.empty() ? "" : ",") + "F";
        if (p_year & periodG) sub_periods += std::string(sub_periods.empty() ? "" : ",") + "G";
        if (p_year & periodH) sub_periods += std::string(sub_periods.empty() ? "" : ",") + "H";
        if (p_year & periodI) sub_periods += std::string(sub_periods.empty() ? "" : ",") + "I";
        if (p_year & periodJ) sub_periods += std::string(sub_periods.empty() ? "" : ",") + "J";
        if (p_year & periodK) sub_periods += std::string(sub_periods.empty() ? "" : ",") + "K";
        if (p_year & periodL) sub_periods += std::string(sub_periods.empty() ? "" : ",") + "L";
        if (p_year & periodM) sub_periods += std::string(sub_periods.empty() ? "" : ",") + "M";
        if (p_year & periodN) sub_periods += std::string(sub_periods.empty() ? "" : ",") + "N";
        if (p_year & periodO) sub_periods += std::string(sub_periods.empty() ? "" : ",") + "O";
        if (p_year & periodQ) sub_periods += std::string(sub_periods.empty() ? "" : ",") + "Q";
        if (p_year & periodR) sub_periods += std::string(sub_periods.empty() ? "" : ",") + "R";
        if (p_year & periodZ) sub_periods += std::string(sub_periods.empty() ? "" : ",") + "Z";
        period += sub_periods;
    }
    return year + period;
}
std::string TriggerHelper::to_string(int p_year) {
    std::string str = to_title(p_year);
    str = PlotUtils::ReplaceExpInString(str, " ", "_");
    str = PlotUtils::ReplaceExpInString(str, ",", "");
    return str;
}
