#ifndef __PLOTDESCRIPTION__H
#define __PLOTDESCRIPTION__H


#include <map> 
#include "NtupleAnalysisUtils/Plot/Plot.h"
#include "NtupleAnalysisUtils/Helpers/NTAUHelpersIncludes.h" 
#include "NtupleAnalysisUtils/Configuration/CanvasOptions.h"
#include "NtupleAnalysisUtils/Visualisation/MultiPagePdfHandle.h"


/// The plot content class is used 
/// to collect everything needed to define one 
/// figure to be written to a file: 
///  - the individual histos to show up, with their formatting and legend options (Plot objects) 
///  - the labels to draw on the canvas 
///  - the file name to save under 
///  - options for configuring the canvas. 


/// Forward declare define a helper class to use when computing ratios 
class RatioEntry; 

template <class Histo> class PlotContent {
    public:

        // here, we pass the histos to be drawn, and the code will assume that
        // we simply want ratios of all plots w.r.t the first one, should we ask for them. 
        PlotContent(const std::vector<Plot<Histo>> & plots,             // each plot that should be drawn on the canvas.
                    const std::vector<std::string> & labels,            // labels that should be drawn in addition to atlas / lumi 
                    const std::string &      fname,                  // file name to save as. Provide without extension, will be added automatically
                    std::shared_ptr<MultiPagePdfHandle>      multiPagePdf = nullptr,  // additional file name for a multi page pdf, if you want to write to one. 
                    const CanvasOptions &    opts=CanvasOptions());        // options for formatting the canvas to draw on 
                    
        // this constructor allows to specifically choose which ratios we would like to have drawn. 
        // Each ratio entry is described by a RatioEntry object (see below). 
        PlotContent(const std::vector<Plot<Histo>> & plots,      // each plot that should be drawn on the canvas.
                    const std::vector<RatioEntry>  & ratios,     // Ratios between elements within the plots argument that should be drawn. 
                    const std::vector<std::string> & labels,     // labels that should be drawn in addition to atlas / lumi 
                    const std::string &      fname,      // file name to save as. Provide without extension, will be added automatically
                    std::shared_ptr<MultiPagePdfHandle>      multiPagePdf = nullptr,  // additional file name for a multi page pdf, if you want to write to one. 
                    const CanvasOptions &    opts=CanvasOptions());    // options for formatting the canvas to draw on
        
        /// This will populate all our plots
        void populateAll(bool applyFormat=true);

        /// This allows to retrieve the i-th plot
        Plot<Histo> & getPlot(size_t i);
        /// This allows to retrieve ALL the plots
        std::vector<Plot<Histo>> & getPlots();

        /// get the total number of plots
        size_t getNplots() const;

        /// get the ratios we booked
        std::vector<RatioEntry> & getRatioEntries();

        /// get the populated ratios 
        std::vector<Plot<TH1>> getRatios();

        //// get the labels we assigned
        std::vector<std::string> & getLabels();

        /// get or overwrite the file name 
        std::string getFileName() const;
        void setFileName(const std::string & fname);

        /// get or overwrite the multi page PDF file name
        std::shared_ptr<MultiPagePdfHandle> getMultiPagePdfHandle() const;
        void setMultiPagePdfHandle(std::shared_ptr<MultiPagePdfHandle> theHandle);

        /// get or overwrite the canvas options
        const CanvasOptions  & getCanvasOptions() const;
        /// this version allows in-place modification
              CanvasOptions  & getCanvasOptions();
        void setCanvasOptions(CanvasOptions opt);

    protected:
        void buildDefaultRatios();
        std::vector<Plot<Histo>> m_plots; 
        std::vector<RatioEntry> m_ratios; 
        std::vector<std::string> m_labels;
        std::string m_fname;
        std::shared_ptr<MultiPagePdfHandle> m_multiPagePdfHandle;
        CanvasOptions m_canvasOpts;
        bool m_isPopulated{false};
};


/// This is a very simple class representing a ratio within a PlotContent.
/// It is built by providing two indices within the plot vector of the 
/// same content to compare, the first being the numerator and the 
/// second the denominator of the ratio. 
/// The third argument is the error strategy to use. 

class RatioEntry{

public:
    /// Constructor.
    /// @arg num: Numerator, identified by its index within the plot vector
    /// @arg den: Denominator, identified by its index within the plot vector
    /// @arg errorStrat: Error calculation strategy to use. 
    RatioEntry(size_t num, 
               size_t den, 
               PlotUtils::EfficiencyMode errorStrat = PlotUtils::defaultErrors);

    size_t getNumIndex();
    size_t getDenomIndex();

    /// Helper - compute the ratio plot from a plot content instance
    template <class In> Plot<TH1> getRatio(PlotContent<In> & pc) const;

protected:
    size_t m_num;
    size_t m_den;
    PlotUtils::EfficiencyMode m_errorStrat;
};


#include "NtupleAnalysisUtils/Visualisation/PlotContent.ixx" 

#endif  // __PLOTDESCRIPTION__H

