#ifndef NTUPLEBRANCH__H
#define NTUPLEBRANCH__H

#include "TTree.h"
#include "TBranch.h"
#include "TRandom3.h"
#include "TLeaf.h"
#include "vector"
#include <exception>
#include <iostream>
#include <iomanip>
#include <memory>
#include "NtupleAnalysisUtils/Ntuple/NtupleBranchBase.h"
#include "NtupleAnalysisUtils/Ntuple/NtupleBranchMgr.h"

/// Here, we have the templated implementations of the NtupleBranchBase. 
/// They add proper accessors on top of the common interface. 
/// 
/// Due to the internals of ROOT I/O, a number of template specialisations exist. 
/// They all share the same user-facing behaviour. 

/// This is the NtupleBranch implementation for all scalar data types. 
template<typename branchType> class NtupleBranch : public NtupleBranchBase  { 
    
public: 
    /// Constructor. Provide the branch name to read/write, the tree to connect to, 
    /// and optionally a manager object to conveniently work with a multitude of branches. 
    NtupleBranch(const std::string & branchName, TTree* tree, NtupleBranchMgr* mgr = 0);
  
    /// Access the branch content. Will auto-attach at the first call. 
    /// The () operator and the get() method are fully equivalent. 
    inline const branchType & operator() ();
    inline const branchType & get();

    /// Access the branch once, but do not leave it connected. Useful for reducing 
    /// I/O and decompression overhead for low acceptances. 
    inline const branchType & readOnce ();

    /// Fill the branch when writing an output tree. 
    /// Will attach in write mode. 
    inline void set (branchType  in);
    /// this signature will copy the content of the passed branch
    inline void set (NtupleBranch<branchType> &  in);
    /// this will also copy the passed branch
    void set (NtupleBranchBase*  in) override;

    /// dummy method for the "array" size, returns 1 for this type of branch
    inline size_t size() override;

    /// set content to a dummy value for output writing
    inline void setDummy () override;

    /// Generate source code to describe this branch.  Used for branch manager generation
    std::string generateMemberString() const override;

protected:
    /// attach to the branch for reading 
    void attachReadPayload() override;
    /// attach to the branch for writing 
    TBranch* addPayloadAsBranch() override;

    /// data member - represents the payload
    branchType m_content;
};

/// This is close to a 1:1 repeat of the above, but specialised for std::string members.
/// These require a slightly different interaction with ROOT.  
template< > class NtupleBranch<std::string> : public NtupleBranchBase  { 
public: 
    /// constructor, providing the branch name used and the tree to read from. 
    /// If you pass an NtupleBranchMgr*, you can use that manager to handle the tree loop. 
    NtupleBranch(const std::string & branchName, TTree* tree, NtupleBranchMgr* mgr = 0);
    /// Access the branch content. Will auto-attach at the first call. 
    /// The () operator and the get() method are fully equivalent. 
    const std::string & operator() ();
    const std::string & get();
    /// Access the branch once, but do not leave it connected. Useful for reducing 
    /// I/O and decompression overhead for low acceptances. 
    const std::string & readOnce ();

    /// Fill the branch when writing an output tree. 
    /// Will attach in write mode. 
    void set (std::string  in);
    /// this signature will copy the content of the passed branch
    void set (NtupleBranch<std::string> &  in);
    /// this will also copy the passed branch
    void set (NtupleBranchBase*  in) override;
    
    /// dummy method for the "array" size, returns 1 for this type of branch
    size_t size() override;

    /// set content to a dummy value for output writing
    void setDummy () override;

    /// Generate source code to describe this branch.  Used for branch manager generation
    std::string generateMemberString() const override;

protected:
    /// attach our payload for reading
    void attachReadPayload() override;
    /// attach our payload for writing
    TBranch* addPayloadAsBranch() override;

    /// data member - represents the payload
    std::string* m_content;
};

/// template specialisation for vector branches/ 
/// These essentially behave like scalars, but provide additional 
/// convenient element-level accessors as well as iterators. 
template<typename branchType> class NtupleBranch<std::vector<branchType>> : public NtupleBranchBase  { 
    
public: 
    /// constructor, providing the branch name used and the tree to read from. 
    /// If you pass an NtupleBranchMgr*, you can use that manager to handle the tree loop. 
    /// If this starts to sound repetitive... it is! 
    NtupleBranch(const std::string & branchName, TTree* tree, NtupleBranchMgr* mgr = 0);
    ~NtupleBranch();

    /// Access the branch content. Will auto-attach at the first call. 
    /// The () operator and the get() method are fully equivalent. 
    inline const std::vector<branchType> & operator() ();
    inline const std::vector<branchType> & get();

    /// access to elements by index. Uses the vector::at call internally,
    /// so asking for beyond-bounds elements will get you a nice exception 
    inline branchType operator() (size_t i);
    inline branchType get(size_t i);

    /// You can also get const_iterators over the internal vector.
    /// Happy looping!
    inline const typename std::vector<branchType>::const_iterator begin();
    inline const typename std::vector<branchType>::const_iterator end();

    /// Access the branch once, but do not leave it connected. Useful for reducing 
    /// I/O and decompression overhead for low acceptances. 
    const std::vector<branchType> & readOnce ();

    /// Fill the branch when writing an output tree. 
    /// Will attach in write mode. 

    /// This allows to write a ready-made vector 
    inline void set (const std::vector<branchType> & in);
    /// This allows to set one element. Uses vector::at 
    /// for bounds checking with resulting exception behaviour
    void set (size_t index, const branchType & in);

    /// The next two allow to copy another vector branch into this one
    inline void set (NtupleBranch<std::vector<branchType> > &  in);
    void set (NtupleBranchBase*  in) override;

    /// allows to edit in-place when working in output mode. 
    inline std::vector<branchType> & outVec();

    /// allows to perform a push-back when in write mode
    inline void push_back(const branchType& t); 

    /// allows to clear the vector when in write mode
    inline void clear(); 

    // dummy method for the "array" size 
    size_t size() override;

    /// set content to a dummy value for output writing
    inline void setDummy () override;

    /// Generate source code to describe this branch.  Used for branch manager generation
    std::string generateMemberString() const override;

protected:
    /// attach our vector payload for reading
    void attachReadPayload() override;
    /// attach our vector payload for writing
    TBranch* addPayloadAsBranch() override;

    /// Data members 

    /// The vector payload of this branch
    std::vector<branchType>* m_content;

    /// preserve the original vector if it has been overwritten during read operations.
    /// clean up the memory once we go out of scope
    std::shared_ptr<std::vector<branchType> > m_cleanupHelper;
};

// template specialisation for branches containing arrays. Used in H4l
template <typename arrBranchType> class NtupleBranch<arrBranchType*>: public NtupleBranchBase { 
public:
    /// constructor, providing the branch name used and the tree to read from. 
    /// If you pass an NtupleBranchMgr*, you can use that manager to handle the tree loop. 
    /// If this starts to sound repetitive... it is! 
    NtupleBranch(const std::string & branchName, size_t length, TTree* tree, NtupleBranchMgr* mgr=0);
    virtual ~NtupleBranch();

    
    /// Access the branch content. Will auto-attach at the first call. 
    /// The () operator and the get() method are fully equivalent. 
    arrBranchType* operator()();
    arrBranchType* get();

    /// access the individual elements. 
    /// Here, unlike the real C-array, we actually perform 
    /// range checking. Will throw an std::out_of_range if 
    /// you exceed the array bounds. 
    /// The () operator and the get() method are again fully equivalent. 
    const arrBranchType & operator()(size_t n);
    const arrBranchType & get(size_t n);
    
    /// Access the branch once, but do not leave it connected. Useful for reducing 
    /// I/O and decompression overhead for low acceptances. 
    const arrBranchType* readOnce ();
    // get total size of the thing
    size_t size() override;

    /// Fill the branch when writing an output tree. 
    /// Will attach in write mode. 

    /// This allows element-wise setting. 
    /// Performs bound-checking and will throw an 
    /// std::out_of_range if you exceed the booked range
    void set (size_t index, arrBranchType  in);

    /// The next two allow to replace the entire array.

    /// This provides size-checking to prevent accidents 
    template <size_t N> void set (arrBranchType  (&in)[N]);

    /// This assumes you provide the correct array size.
    /// If the input is too long, will skip the last elements.
    /// If the input is too short, random memory might be read! 
    /// Don't like it? Don't use C-arrays! :-( 
    void set (arrBranchType* in);
    
    /// Copy another branch. Performs length checking to avoid mishaps. 
    void set (NtupleBranch <arrBranchType*> & in);
    void set (NtupleBranchBase*  in) override;

    /// set content to a dummy value for output writing
    void setDummy () override;
    
    /// Generate source code to describe this branch. Used for branch manager generation

    std::string generateMemberString() const override;
    // this generates a line for the class constructor,
    // in the Format "<name>('<name>',t,this)," 
    std::string generateInitString(unsigned int paddingWidth = 20) const override;

protected:
    // attach our array payload for reading
    void attachReadPayload() override;
    // attach our array payload for writing
    TBranch* addPayloadAsBranch() override;

    /// get the string needed for attaching
    std::string attachString();

    /// Data members

    /// our C-array
    arrBranchType* m_content;
    /// the array length
    const size_t m_length;
};

#include "NtupleBranch.ixx"
#endif // NTUPLEBRANCH__H
