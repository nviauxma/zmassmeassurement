#include <MuonSagittaBias/CollinsSoperUtils.h>
#include <MuonSagittaBias/Utils.h>

///#############################################################################
///     General set of helper functions needed for the sagitta bias analysis
///##############################################################################
///
namespace {
    ///
    constexpr float OneOverSqrt2 = 1. / std::sqrt(2);
    /// Centre of mass energy
    constexpr float SqrtS = 13.e6;
    constexpr float BeamE = SqrtS / 2;

}  // namespace
namespace CollinsSoper {

    ///####################################################
    ///                     RestFrame
    ///####################################################
    RestFrame::RestFrame(const PolarVector4D& lep_plus, const PolarVector4D& lep_minus) {
        make_coordinate_transform(Vector4D{lep_plus}, Vector4D{lep_minus});
    }
    RestFrame::RestFrame(const Vector4D& lep_plus, const Vector4D& lep_minus) { make_coordinate_transform(lep_plus, lep_minus); }
    void RestFrame::make_coordinate_transform(const Vector4D& lep_plus, const Vector4D& lep_minus) {
        static const Vector4D P4_Lab_Proton_A{0, 0, BeamE, BeamE};
        static const Vector4D P4_Lab_Proton_B{0, 0, -BeamE, BeamE};

        m_P4_ll = lep_plus + lep_minus;
        m_boost_rest = RestFrameBoost{boost_vector(m_P4_ll)};

        /// Tranform first the two protons into the restframe of the
        /// decaying Z
        const Vector4D P4_CS_Proton_A = m_boost_rest * P4_Lab_Proton_A;
        const Vector4D P4_CS_Proton_B = m_boost_rest * P4_Lab_Proton_B;

        /// The Z axis is defined to bisect the angle between proton_{A} and proton_{B}
        ///    where proton A is choosen to be colinear with the z-axis
        ///  -->  <pA, eZ> = <pB, eZ>
        ///  -->  <pA - pB, eZ> = 0
        ///  -->  e_{Z} =  (p_{A} + p_{B}) / norm | A +B|
        /// where <*,*> represent the 3D scalar product

        /// Calculate the magnitude of the momenta in the restframe

        const Vector3D V_CS_A = unit_vector(P4_CS_Proton_A);
        const Vector3D V_CS_B = -unit_vector(P4_CS_Proton_B);

        const Vector3D Z_axis = (invariantP4().Pz() < 0 ? -1 : 1) * unit_vector(V_CS_A + V_CS_B);
        /// The y-axis is defined as the normal vector between the two incoming protons
        const Vector3D Y_axis = unit_vector(cross(V_CS_A, V_CS_B)) * (invariantP4().Pz() < 0 ? 1. : -1.);
        /// Now we need to define the corresponding x-vector
        const Vector3D X_axis = cross(Y_axis, Z_axis);

        m_lab_rotation = SystemRotation{X_axis, Y_axis, Z_axis};
        m_rest_rotation = m_lab_rotation.Inverse();
    }

    const Vector4D& RestFrame::invariantP4() const { return m_P4_ll; }

}  // namespace CollinsSoper
