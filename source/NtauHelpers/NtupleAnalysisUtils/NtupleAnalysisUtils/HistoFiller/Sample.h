#ifndef __SAMPLE__H
#define __SAMPLE__H

#include <string> 
#include <mutex> 
#include <map> 
#include <vector> 
#include "TFile.h"
#include "NtupleAnalysisUtils/HistoFiller/Selection.h"
#include "NtupleAnalysisUtils/HistoFiller/HistoFillerInterfaces.h"

/// Simple class to model a Data/MC Sample. 
/// It collects one or several files to process, with the option of associating cuts (e.g. filters) to specific files.



/// The basic building block of a sample is one individual "InputFile" (one tree taken from some file). 
template <class ProcessThis> class InputFile: public IInputFile{
public:
    /// Construct by providing a file location, and a tree name (optional folders accepted). 
    /// optionally, a file-specific selection (for example phase-space cut / outlier weight removal) can be specified.  
    InputFile(const std::string & thefile, const std::string & treename, Selection<ProcessThis> specificCuts=Selection<ProcessThis>());
    /// set the name of the tree to find in the input files. 
    void setTreeName(const std::string & name);
    /// and the name of the file 
    void setFileName(const std::string & name);
    /// allow to replace the sample-specific cuts
    void setSelection(Selection<ProcessThis> sel);

    /// Getters for the key properties
    const std::string & getFileName() const; 
    const std::string & getTreeName() const;
    Selection<ProcessThis> getSelection() const;

    /// This will read the input from the file. 
    ProcessThis getTreeFromFile( std::shared_ptr<TFile> & f ) const;
    
    /// Create a task that the HistoFiller can work with based on this input file.  
    /// Note: this member is implemented in HistoFillerHelpers.ixx, since it relies on includes from there and is only used in conjunction with this operating mode. 
    virtual std::shared_ptr<IHistoFillTask> spawnTask (SelectionToFillableMap & payload, bool splitAndMergeMode);

    /// Create a clone 
    virtual std::shared_ptr<IInputFile> clone() const;

    /// Enable weights for the bootstrap method if needed
    virtual void setNBootstrapWeights(size_t n);
    virtual size_t getNBootstrapWeights();


protected:
    void updateDynamicID(); 
    std::string m_fileName;
    std::string m_treeName;
    Selection<ProcessThis> m_selection;
    size_t m_n_bootstrap{0};
};

/// the sample class itself can contain several files (each a InputFile), each of which may have its own specific selection / tree name. 
template <class ProcessThis> class Sample: public ISample{
    public:
        /// start with empty sample to start populating it downstream
        Sample();
        /// Sometimes useful: Quickly package a single file in a Sample object. 
        Sample(const std::string & thefile,  const std::string & treename, Selection<ProcessThis> specificCuts=Selection<ProcessThis>());
        /// Or directly instantiate a sample with a set of files! 
        Sample(std::vector<InputFile<ProcessThis>> files);


        /// Add a file to this sample. 
        /// You can add cuts specific to this file (for example if you need to filter out some phase space region). 
        /// The second version constructs a InputFile in-place
        void addFile(InputFile<ProcessThis> addMe);
        void addFile(const std::string & fname, 
                     const std::string & treename, 
                     Selection<ProcessThis> specificCuts=Selection<ProcessThis>());
        // add all files from another sample. 
        void addFilesFrom(const Sample<ProcessThis> & other);

        /// This is used to retrieve all connected files for this sample 
        std::vector<InputFile<ProcessThis>> getInputFiles() const;

        // this returns a version of the above sorted by descending file size, useful for optimising the processing order 
        std::vector<InputFile<ProcessThis>> getSortedInputFiles() const;

        // Open a file and read an input (typically: ntuple)
        // If you pass the file pointer, the method will reuse it in case this is the file matching fname, otherwise it will close 
        // the existing file and set f to the new file. 
        // Passing a nullptr will result in the ptr being set to the newly opened file
        template <class HistClass> static HistClass* getObjectFromFile(const std::string & fname, 
                                                                       const std::string & objectName, 
                                                                       std::shared_ptr<TFile> & f );

        // implement the ISample interface used by the HistoFiller
        virtual std::vector<std::shared_ptr<IInputFile> > getInputFiles();
        virtual std::shared_ptr<ISample> clone() const;
        
        // set bootstrap events to process 
        void setNBootstrapWeights(size_t n);
        size_t getNBootstrapWeights();

    protected:
        void updateDynamicID(); 
        std::vector<InputFile<ProcessThis>> m_files;
        size_t m_n_bootstrap{0};
    };


    #include "Sample.ixx"

#endif
