#include <FourMomUtils/xAODP4Helpers.h>
#include <MuonSagittaBias/ArgumentParser.h>
#include <MuonSagittaBias/AsymmetryPlots.h>
#include <MuonSagittaBias/PhaseSpaceCuts.h>
#include <MuonSagittaBias/PlotProvider.h>
#include <MuonSagittaBias/SagittaBiasAnalyzer.h>
#include <MuonSagittaBias/SlicedPlotProvider.h>
#include <MuonSagittaBias/Utils.h>
#include <NtauHelpers/HistoBinConfig.h>
#include <NtauHelpers/OutFileWriter.h>
#include <NtauHelpers/SelectionWithTitle.h>

int main(int argc, char* argv[]) {
    /// Standard piece of code to setup the analysis...
    ArgumentParser parser;
    parser.summary_pdf("AllPhiCSPlots").out_root_file("PhiCSTemplates.root").inject_config_file("MuonSagittaBias/sigma_injection.txt");
    if (!parser.parse_args(argc, argv)) return EXIT_FAILURE;

    /// Create the output file to store all of the histograms

    PlotWriter<TH1> out_file{parser.make_root_file() ? parser.out_file() : nullptr};
    if (parser.make_root_file() && !out_file.get()) { return EXIT_FAILURE; }
    if (!parser.make_root_file() && !parser.make_pdfs()) {
        std::cerr << parser.app_name() << "No output format" << std::endl;
        return EXIT_FAILURE;
    }
    Sample<SagittaBiasAnalyzer> sample_to_process = parser.prepare_sample<SagittaBiasAnalyzer>();

    CanvasOptions canvas_opts = CanvasOptions()
                                    .RatioAxis(AxisConfig().Title("to truth-level").ExtraTitleOffset(0.4).Min(0.9).Max(1.005))
                                    .YAxis(AxisConfig().Title("Entries").ExtraTitleOffset(0.4).TopPadding(0.7))
                                    // .XAxis(AxisConfig().ExtraTitleOffset(1.3))
                                    .LabelStatusTag("Data 2018, Internal")
                                    .OutputDir(parser.out_dir());

    std::shared_ptr<MultiPagePdfHandle> multi_page =
        parser.make_pdfs() ? PlotUtils::startMultiPagePdfFile(parser.summary_pdf(), canvas_opts) : nullptr;

    /// Select the sagitta biases for the plots
    const std::vector<int>& inject_types = SagittaBiasAnalyzer::injection_types();
    std::vector<int> biases_to_plot{MuonTrk::TrackType::IDTrk};
    if (inject_types.size() <= 5) {
        biases_to_plot += inject_types;
    } else {
        biases_to_plot += inject_types[SagittaBiasAnalyzer::smallest_inject_idx(MuonTrk::TrackType::IDTrk)];
        biases_to_plot += inject_types[SagittaBiasAnalyzer::largest_inject_idx(MuonTrk::TrackType::IDTrk)];
    }
    std::vector<PlotContent<TH1>> sagitta_biases;

    std::vector<PhaseSpaceCuts> selections;
    selections += PhaseSpaceCuts("All", "All pairs", [](SagittaBiasAnalyzer&, int) -> bool { return true; });

    std::vector<SlicedPlotProvider> slicers;
    std::vector<PlotFiller> fillers;

    /// Okay let's define the filler for the pseudo masses

    const std::vector<double> coarse_eta_bins{-2.5, 0., 2.5};
    const std::vector<double> fine_eta_bins = PlotUtils::getLinearBinning(-2.5, 2.5, 25);
    const std::vector<double> pt_bins =
        PlotUtils::getLinearBinning(20, 100, (100. - 20.) / 2.5) + PlotUtils::getLinearBinning(100, 250, 150 / 10.);
    const std::vector<double> phi_bins = PlotUtils::getLinearBinning(-M_PI, M_PI, 30);

    for (auto lep_charge : {SagittaBiasAnalyzer::LepCharge::Minus, SagittaBiasAnalyzer::LepCharge::Plus}) {
        const std::string q_sign = lep_charge == SagittaBiasAnalyzer::LepCharge::Minus ? "-" : "+";
        const std::string sign_name = lep_charge == SagittaBiasAnalyzer::LepCharge::Minus ? "Minus" : "Plus";
        std::vector<SlicedPlotProvider> phi_cs_slicers;

        phi_cs_slicers += SlicedPlotProvider(
            sample_to_process,
            [lep_charge](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
                return h->GetXaxis()->FindBin(t.eta(trk_type, lep_charge));
            },
            PlotUtils::HistoBinConfig()
                .x_bins(coarse_eta_bins)
                .name(sign_name + "Lep_EtaHemisphere")
                .x_title(Form("#eta(#mu^{%s})", q_sign.c_str())),
            selections[0]);

        /// Then let's continue to a finer eta bining
        // phi_cs_slicers += SlicedPlotProvider(
        // sample_to_process,
        // [lep_charge](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
        // return h->GetXaxis()->FindBin(t.eta(trk_type, lep_charge));
        // },
        // PlotUtils::HistoBinConfig().x_bins(fine_eta_bins).name(sign_name + "Lep_Eta").x_title(Form("#eta(#mu^{%s})", q_sign.c_str())),
        // selections[0]);
        /// Another plot against phi
        // phi_cs_slicers += SlicedPlotProvider(
        // sample_to_process,
        // [lep_charge](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
        // return h->GetXaxis()->FindBin(t.phi(trk_type, lep_charge));
        // },
        // PlotUtils::HistoBinConfig().x_bins(phi_bins).name(sign_name + "Lep_Phi").x_title(Form("#phi(#mu^{%s})", q_sign.c_str())),
        // selectons[0]);
        // / Against pt
        // phi_cs_slicers += SlicedPlotProvider(
        // sample_to_process,
        // [lep_charge](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) { return h->GetXaxis()->FindBin(t.pt(trk_type, lep_charge)); },
        // PlotUtils::HistoBinConfig().x_bins(pt_bins).name(sign_name + "Lep_Pt").x_title(Form("#pt(#mu^{%s}) [GeV]", q_sign.c_str())),
        // selections[0]);

        // / Eta phi - coarse version
        // phi_cs_slicers += SlicedPlotProvider(
        // sample_to_process,
        // [lep_charge](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
        // return h->GetBin(h->GetXaxis()->FindBin(t.eta(trk_type, lep_charge)), h->GetYaxis()->FindBin(t.phi(trk_type, lep_charge)));
        // },
        // PlotUtils::HistoBinConfig()
        // .x_bins(coarse_eta_bins)
        // .name(sign_name + "EtaHemispherePhi")
        // .x_title(Form("#eta(#mu^{%s})", q_sign.c_str()))
        // .y_bins(phi_bins)
        // .y_title(Form("#phi(#mu^{%s})", q_sign.c_str())),
        // selections[0]);

        // / Eta phi - fine version
        // phi_cs_slicers += SlicedPlotProvider(
        // sample_to_process,
        // [lep_charge](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
        // return h->GetBin(h->GetXaxis()->FindBin(t.eta(trk_type, lep_charge)), h->GetYaxis()->FindBin(t.phi(trk_type, lep_charge)));
        // },
        // PlotUtils::HistoBinConfig()
        // .x_bins(fine_eta_bins)
        // .name(sign_name + "EtaPhi")
        // .x_title(Form("#eta(#mu^{%s})", q_sign.c_str()))
        // .y_bins(phi_bins)
        // .y_title(Form("#phi(#mu^{%s})", q_sign.c_str())),
        // selections[0]);

        // / Eta - pt
        // phi_cs_slicers += SlicedPlotProvider(
        // sample_to_process,
        // [lep_charge](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
        // return h->GetBin(h->GetXaxis()->FindBin(t.pt(trk_type, lep_charge)), h->GetYaxis()->FindBin(t.eta(trk_type, lep_charge)));
        // },
        // PlotUtils::HistoBinConfig()
        // .x_bins(pt_bins)
        // .name(sign_name + "PtEta")
        // .x_title(Form("p_{T}(#mu^{%s}) [GeV]", q_sign.c_str()))
        // .y_bins(fine_eta_bins)
        // .y_title(Form("#eta(#mu^{%s})", q_sign.c_str())),
        // selections[0]);

     //   PlotFiller phi_cs_filler(
     //       [lep_charge](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
      //          return h->GetXaxis()->FindBin(t.phiCS(trk_type, lep_charge));
      //      },
      //      PlotUtils::HistoBinConfig()
      //          .name("PhiCS" + sign_name)
      //          .x_title(Form("#phi^{CS}_{%s} [rad]", q_sign.c_str()))
      //          .x_min(-M_PI)
      //          .x_max(M_PI)
       //         .x_num_bins(60));

        PlotFiller phi_cs_filler([lep_charge](const TH1* h, SagittaBiasAnalyzer& t,
                                              int trk_type) { return h->GetXaxis()->FindBin(t.phiCS(trk_type, lep_charge)); },
                                 PlotUtils::HistoBinConfig()
                                     .name("PhiCS" + sign_name)
                                     .x_title(Form("#phi^{CS}_{%s} [rad]", q_sign.c_str()))
                                     .x_min(-M_PI)
                                     .x_max(M_PI)
                                     .x_num_bins(60));
        PlotFiller theta_cs_filler(
            [lep_charge](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
                return h->GetXaxis()->FindBin(t.phiCS(trk_type, lep_charge));
            },
            PlotUtils::HistoBinConfig()
                .name("ThetaCS" + sign_name)
                .x_title(Form("#theta^{CS}_{%s} [rad]", q_sign.c_str()))
                .x_min(0)
                .x_max(M_PI)
                .x_num_bins(60));

        for (SlicedPlotProvider& prov : phi_cs_slicers) {
            if (parser.make_pdfs()) {
                sagitta_biases += prov.make_plots(phi_cs_filler, biases_to_plot, {}, {"Z#rightarrow#mu#mu 61-120 GeV", prov.title()}, "Raw_Z",
                                                  multi_page, canvas_opts, {61, 121});
                 sagitta_biases += prov.make_plots(phi_cs_filler, biases_to_plot, {}, {"Z#rightarrow#mu#mu 121-150 GeV", prov.title()}, "Raw_Z",
                                                  multi_page, canvas_opts, {121, 150});
                sagitta_biases += prov.make_plots(phi_cs_filler, biases_to_plot, {}, {"Z#rightarrow#mu#mu 150-250 GeV", prov.title()}, "Raw_Z",
                                                  multi_page, canvas_opts, {150, 250});         
                sagitta_biases += prov.make_plots(phi_cs_filler, biases_to_plot, {}, {"Z#rightarrow#mu#mu 250-500 GeV", prov.title()}, "Raw_Z",
                                                  multi_page, canvas_opts, {250, 500}); 
                sagitta_biases += prov.make_plots(phi_cs_filler, biases_to_plot, {}, {"Z#rightarrow#mu#mu 500-1000 GeV", prov.title()}, "Raw_Z",
                                                  multi_page, canvas_opts, {500, 1000});                                                                                                                                                                                           

                sagitta_biases += prov.make_plots(phi_cs_filler, biases_to_plot, {}, {"J/#psi#rightarrow#mu#mu", prov.title()},
                                                  "JPsi", multi_page, canvas_opts, {2.7, 3.5});
                sagitta_biases += prov.make_plots(theta_cs_filler, biases_to_plot, {}, {"J/#psi#rightarrow#mu#mu", prov.title()}, "JPsi",
                                                  multi_page, canvas_opts, {2.7, 3.5});
                sagitta_biases += prov.make_plots(theta_cs_filler, biases_to_plot, {}, {"J/#psi#rightarrow#mu#mu", prov.title()},
                                                  "JPsi", multi_page, canvas_opts, {2.7, 3.5});
            }
            
            //prov.add_plot(theta_cs_filler, parser.get_trk_types_ROOT_file(), out_file, {2.7, 3.5}); 
            prov.add_plot(theta_cs_filler, parser.get_trk_types_ROOT_file(), out_file, {61, 121});
             prov.add_plot(theta_cs_filler, parser.get_trk_types_ROOT_file(), out_file, {121, 150});
             prov.add_plot(theta_cs_filler, parser.get_trk_types_ROOT_file(), out_file, {150, 250});
             prov.add_plot(theta_cs_filler, parser.get_trk_types_ROOT_file(), out_file, {250, 500});
             prov.add_plot(theta_cs_filler, parser.get_trk_types_ROOT_file(), out_file, {500, 1000});           
                
            //prov.add_plot(phi_cs_filler, parser.get_trk_types_ROOT_file(), out_file, {2.7, 3.5});
            prov.add_plot(phi_cs_filler, parser.get_trk_types_ROOT_file(), out_file, {61, 121});                                                                                                                                                                                                                                                                                                                              
            prov.add_plot(phi_cs_filler, parser.get_trk_types_ROOT_file(), out_file, {121, 150});                                                                                                                                                                                                                                                                                                                                  
            prov.add_plot(phi_cs_filler, parser.get_trk_types_ROOT_file(), out_file, {150, 250});                                                                                                                                                                                                                                                                                                                                  
            prov.add_plot(phi_cs_filler, parser.get_trk_types_ROOT_file(), out_file, {250, 500});                                                                                                                                                                                                                                                                                                                                  
            prov.add_plot(phi_cs_filler, parser.get_trk_types_ROOT_file(), out_file, {500, 1000});
        }
    }
    std::sort(sagitta_biases.begin(), sagitta_biases.end(),
              [](const PlotContent<TH1>& a, const PlotContent<TH1>& b) { return a.getFileName() < b.getFileName(); });
    for (auto& pc : sagitta_biases) {
        if (count_non_empty(pc)) DefaultPlotting::draw1D(pc);
    }
    return EXIT_SUCCESS;
}
