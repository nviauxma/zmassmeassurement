# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.18

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Disable VCS-based implicit rules.
% : %,v


# Disable VCS-based implicit rules.
% : RCS/%


# Disable VCS-based implicit rules.
% : RCS/%,v


# Disable VCS-based implicit rules.
% : SCCS/s.%


# Disable VCS-based implicit rules.
% : s.%


.SUFFIXES: .hpux_make_needs_suffix_list


# Command-line flag to silence nested $(MAKE).
$(VERBOSE)MAKESILENT = -s

#Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/Cmake/3.18.3/Linux-x86_64/bin/cmake

# The command to remove a file.
RM = /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/Cmake/3.18.3/Linux-x86_64/bin/cmake -E rm -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /afs/cern.ch/user/n/nviauxma/ZmassCalibration3/source

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /afs/cern.ch/user/n/nviauxma/ZmassCalibration3/build

# Utility rule file for MuonSagittaBiasHeaderInstall.

# Include the progress variables for this target.
include MuonSagittaBias/CMakeFiles/MuonSagittaBiasHeaderInstall.dir/progress.make

x86_64-centos7-gcc8-opt/include/MuonSagittaBias:
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/afs/cern.ch/user/n/nviauxma/ZmassCalibration3/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Generating ../x86_64-centos7-gcc8-opt/include/MuonSagittaBias"
	cd /afs/cern.ch/user/n/nviauxma/ZmassCalibration3/build/MuonSagittaBias && /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/Cmake/3.18.3/Linux-x86_64/bin/cmake -E make_directory /afs/cern.ch/user/n/nviauxma/ZmassCalibration3/build/x86_64-centos7-gcc8-opt/include
	cd /afs/cern.ch/user/n/nviauxma/ZmassCalibration3/build/MuonSagittaBias && /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/Cmake/3.18.3/Linux-x86_64/bin/cmake -E create_symlink ../../../source/MuonSagittaBias/MuonSagittaBias /afs/cern.ch/user/n/nviauxma/ZmassCalibration3/build/x86_64-centos7-gcc8-opt/include/MuonSagittaBias

MuonSagittaBiasHeaderInstall: x86_64-centos7-gcc8-opt/include/MuonSagittaBias
MuonSagittaBiasHeaderInstall: MuonSagittaBias/CMakeFiles/MuonSagittaBiasHeaderInstall.dir/build.make

.PHONY : MuonSagittaBiasHeaderInstall

# Rule to build all files generated by this target.
MuonSagittaBias/CMakeFiles/MuonSagittaBiasHeaderInstall.dir/build: MuonSagittaBiasHeaderInstall

.PHONY : MuonSagittaBias/CMakeFiles/MuonSagittaBiasHeaderInstall.dir/build

MuonSagittaBias/CMakeFiles/MuonSagittaBiasHeaderInstall.dir/clean:
	cd /afs/cern.ch/user/n/nviauxma/ZmassCalibration3/build/MuonSagittaBias && $(CMAKE_COMMAND) -P CMakeFiles/MuonSagittaBiasHeaderInstall.dir/cmake_clean.cmake
.PHONY : MuonSagittaBias/CMakeFiles/MuonSagittaBiasHeaderInstall.dir/clean

MuonSagittaBias/CMakeFiles/MuonSagittaBiasHeaderInstall.dir/depend:
	cd /afs/cern.ch/user/n/nviauxma/ZmassCalibration3/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /afs/cern.ch/user/n/nviauxma/ZmassCalibration3/source /afs/cern.ch/user/n/nviauxma/ZmassCalibration3/source/MuonSagittaBias /afs/cern.ch/user/n/nviauxma/ZmassCalibration3/build /afs/cern.ch/user/n/nviauxma/ZmassCalibration3/build/MuonSagittaBias /afs/cern.ch/user/n/nviauxma/ZmassCalibration3/build/MuonSagittaBias/CMakeFiles/MuonSagittaBiasHeaderInstall.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : MuonSagittaBias/CMakeFiles/MuonSagittaBiasHeaderInstall.dir/depend

