template <class Fillable, class FillFrom> PlotFillInstruction<Fillable,FillFrom>::PlotFillInstruction (std::function<void(Fillable*, FillFrom &)> fillFunc): 
        m_fill(fillFunc){
}

template <class Fillable, class FillFrom> PlotFillInstruction<Fillable,FillFrom>::PlotFillInstruction (
                        std::function <size_t (FillFrom &)> numberOfEntriesToFill, 
                        std::function <bool(FillFrom &, size_t k)> selectionFuncPerElement,
                        std::function <void(Fillable*, FillFrom &, size_t k)> fillFuncPerElement ):
    PlotFillInstruction(
        [numberOfEntriesToFill, fillFuncPerElement, selectionFuncPerElement](Fillable* h, FillFrom & t){
        const size_t n_entries = numberOfEntriesToFill(t); 
        for (size_t p =0; p < n_entries; ++p){
            if (selectionFuncPerElement(t,p)) fillFuncPerElement(h,t,p);
        }
    }){
}

template <class Fillable, class FillFrom>  
    std::function<void(Fillable*, FillFrom &)> PlotFillInstruction<Fillable,FillFrom>::getFillFunc() const {
    return m_fill;
}
    // pipes calls to filler method
template <class Fillable, class FillFrom>  
    void PlotFillInstruction<Fillable,FillFrom>::fill(Fillable* target, 
                                                             FillFrom & input) const {
    m_fill(target,input);
}
    // alternative fill signature
template <class Fillable, class FillFrom>  
    void PlotFillInstruction<Fillable,FillFrom>::operator()(Fillable* target, 
                                                                   FillFrom & input) const {
    fill(target,input);
}




template <class Fillable, class FillFrom> PlotFillInstructionWithRef<Fillable,FillFrom>::PlotFillInstructionWithRef (
                    std::function<void(Fillable*, FillFrom &)> fillFunc, 
                    Fillable* refForFill): 
                        PlotFillInstruction<Fillable,FillFrom>(fillFunc),
                        m_refHist(dynamic_cast<Fillable*>(refForFill->Clone())){
}
template <class Fillable, class FillFrom> PlotFillInstructionWithRef<Fillable,FillFrom>::PlotFillInstructionWithRef (
                 std::function <size_t (FillFrom &)> numberOfEntriesToFill, 
                 std::function <bool(FillFrom &, size_t k)> selectionFuncPerElement,
                 std::function <void(Fillable*, FillFrom &, size_t k)> fillFuncPerElement,
                 Fillable* refForFill): 
                        PlotFillInstruction<Fillable,FillFrom>(numberOfEntriesToFill,selectionFuncPerElement,fillFuncPerElement),
                        m_refHist(dynamic_cast<Fillable*>(refForFill->Clone())){
 }

template <class Fillable, class FillFrom> PlotFillInstructionWithRef<Fillable,FillFrom>::PlotFillInstructionWithRef (
                const PlotFillInstruction<Fillable,FillFrom> base, 
                Fillable* refForFill): 
                    PlotFillInstruction<Fillable,FillFrom>(base),
                    m_refHist(dynamic_cast<Fillable*>(refForFill->Clone())){
    /// copy the ID from the base object since the fill logic is equivalent.
    /// The populator will add a separate field for the reference histo. 
    this->ISortableByID::setID(base->ISortableByID::getID()); 
}


template <class Fillable, class FillFrom> PlotFillInstructionWithRef<Fillable,FillFrom>::PlotFillInstructionWithRef (
                    std::function<void(Fillable*, FillFrom &)> fillFunc, 
                    std::shared_ptr<Fillable> refForFill): 
                        PlotFillInstruction<Fillable,FillFrom>(fillFunc),
                        m_refHist(dynamic_cast<Fillable*>(refForFill->Clone())){
}
template <class Fillable, class FillFrom> PlotFillInstructionWithRef<Fillable,FillFrom>::PlotFillInstructionWithRef (
                 std::function <size_t (FillFrom &)> numberOfEntriesToFill, 
                 std::function <bool(FillFrom &, size_t k)> selectionFuncPerElement,
                 std::function <void(Fillable*, FillFrom &, size_t k)> fillFuncPerElement,
                 std::shared_ptr<Fillable> refForFill): 
                        PlotFillInstruction<Fillable,FillFrom>(numberOfEntriesToFill,selectionFuncPerElement,fillFuncPerElement),
                        m_refHist(dynamic_cast<Fillable*>(refForFill->Clone())){
 }

template <class Fillable, class FillFrom> PlotFillInstructionWithRef<Fillable,FillFrom>::PlotFillInstructionWithRef (
                const PlotFillInstruction<Fillable,FillFrom> base, 
                std::shared_ptr<Fillable> refForFill): 
                    PlotFillInstruction<Fillable,FillFrom>(base),
                    m_refHist(dynamic_cast<Fillable*>(refForFill->Clone())){
    /// copy the ID from the base object since the fill logic is equivalent.
    /// The populator will add a separate field for the reference histo. 
    this->ISortableByID::setID(base->ISortableByID::getID()); 
}

template <class Fillable, class FillFrom> template <class... Args> PlotFillInstructionWithRef<Fillable,FillFrom>::PlotFillInstructionWithRef (
                std::function<void(Fillable*, FillFrom &)> fillFunc, 
                Args&& ... args): 
                    PlotFillInstruction<Fillable,FillFrom>(fillFunc),
                    m_refHist(std::make_shared<Fillable>(args...)){
}
template <class Fillable, class FillFrom> template <class... Args> PlotFillInstructionWithRef<Fillable,FillFrom>::PlotFillInstructionWithRef (
                 std::function <size_t (FillFrom &)> numberOfEntriesToFill, 
                 std::function <bool(FillFrom &, size_t k)> selectionFuncPerElement,
                 std::function <void(Fillable*, FillFrom &, size_t k)> fillFuncPerElement,
                 Args&& ... args): 
                    PlotFillInstruction<Fillable,FillFrom>(numberOfEntriesToFill,selectionFuncPerElement,fillFuncPerElement),
                    m_refHist(std::make_shared<Fillable>(args...)){
}
template <class Fillable, class FillFrom> template <class... Args> PlotFillInstructionWithRef<Fillable,FillFrom>::PlotFillInstructionWithRef (
                const PlotFillInstruction<Fillable,FillFrom> base, 
                Args&& ... args): 
                    PlotFillInstruction<Fillable,FillFrom>(base),
                    m_refHist(std::make_shared<Fillable>(args...)){
    /// copy the ID from the base object since the fill logic is equivalent.
    /// The populator will add a separate field for the reference histo. 
    this->ISortableByID::setID(base->ISortableByID::getID()); 
}

template <class Fillable, class FillFrom>  Fillable* PlotFillInstructionWithRef<Fillable,FillFrom>::getRefHist() const {
    return m_refHist.get();
}
