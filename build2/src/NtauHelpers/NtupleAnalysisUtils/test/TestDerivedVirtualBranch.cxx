#include "NtupleAnalysisUtils/NTAUTopLevelIncludes.h"
 
/// Build an input tree class with a derived virtual branch
class myTestTree: public NtupleBranchMgr{
    public: 
    myTestTree(TTree* t);
    NtupleBranch<int> var1; 
    NtupleBranch<int> var2; 
    NtupleBranch<int> var3; 
    DerivedVirtualBranch<int,myTestTree> combinedVar; 
};

/// define the calculation to perform in the c-tor
myTestTree::myTestTree(TTree* t):
    var1("var1",t,this),
    var2("var2",t,this),
    var3("var3",t,this),
    combinedVar([](myTestTree &t){return t.var1() + t.var2() + t.var3();},this){
}   


int main (int, char**){

    /////////////////////
    /// write a test tree
    ///////////////////// 
    TFile* fout = new TFile("testDerived.root","RECREATE");
    TTree* tout = new TTree("outTree","aTree");

    int in1 = 0;
    int in2 = 0;
    int in3 = 0;
    tout->Branch("var1",&in1); 
    tout->Branch("var2",&in2); 
    tout->Branch("var3",&in3); 
    
    /// one set of inputs
    int in1_1 = 1;
    int in2_1 = 2;
    int in3_1 = 3;
    /// and another
    int in1_2 = 4;
    int in2_2 = 5;
    int in3_2 = 6;
    
    
    /// fill the first entry
    in1 = in1_1;
    in2 = in2_1;
    in3 = in3_1;
    tout->Fill();

    /// fill the second entry
    in1 = in1_2;
    in2 = in2_2;
    in3 = in3_2;
    tout->Fill();

    // Write it to the file and close it 
    fout->Write();
    fout->Close();


    ///////////////
    /// read back
    ///////////////
    TFile* fin = TFile::Open("testDerived.root","READ");
    TTree* tr = nullptr;
    fin->GetObject("outTree",tr);
    if (!tr){
        std::cerr << "unable to load tree"<<std::endl;
        return 1;
    }

    /// instantiate our test tree
    myTestTree theTree(tr); 

    /// get the first entry and check if our derived var looks right
    theTree.getEntry(0);
    if (theTree.combinedVar() != in1_1 + in2_1 + in3_1) return 1; 
    std::cout << "derived virtual branch - first entry OK "<<std::endl; 

    /// check if the entry updates correctly by also testing the second tree entry
    theTree.getEntry(1);
    if (theTree.combinedVar() != in1_2 + in2_2 + in3_2) return 1; 
    std::cout << "derived virtual branch - second entry OK "<<std::endl; 

    return 0; 
}