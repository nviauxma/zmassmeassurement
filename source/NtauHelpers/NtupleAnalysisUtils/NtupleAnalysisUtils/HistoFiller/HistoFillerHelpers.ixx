#include "NtupleAnalysisUtils/Ntuple/NtupleBranch.h"
#include "NtupleAnalysisUtils/HistoFiller/HistoFillerHelpers.h"
#include "TStopwatch.h"
#include "TFile.h"
#include <future>
#include <map>

namespace { 
    static std::mutex g_mutex_ROOTFileOpenClose;
} 

template <class ProcessThis> FillTaskFragment<ProcessThis>::FillTaskFragment(std::shared_ptr<IFillableFrom<ProcessThis>> parentPlot, bool splitAndMergeMode):
    m_splitAndMerge(splitAndMergeMode),
    m_parentPlot(parentPlot){
}

template <class ProcessThis> inline void FillTaskFragment<ProcessThis>::Fill(ProcessThis &t){
    m_internalPlot->Fill(t); 
}

template <class ProcessThis> void FillTaskFragment<ProcessThis>::print(const std::string & spacer){
    std::cout << spacer << "--> Fillable with ID "<< m_parentPlot->getID()<<std::endl;
}

template <class ProcessThis> void FillTaskFragment<ProcessThis>::initForFill(){
    if (m_splitAndMerge){
        m_internalPlot = std::dynamic_pointer_cast<IFillableFrom<ProcessThis>>(m_parentPlot->clone()); 
        m_internalPlot->Reset();
    }
    else m_internalPlot = m_parentPlot; 
}
template <class ProcessThis> void FillTaskFragment<ProcessThis>::recombine(){
    if (m_splitAndMerge){ 
        m_parentPlot->CollectClone(m_internalPlot); 
        m_internalPlot.reset(); 
    }
} 

template <class ProcessThis> SelectionWithPlots<ProcessThis>::SelectionWithPlots(Selection<ProcessThis> sel, const std::vector<FillTaskFragment<ProcessThis>> & toFill): 
    m_Selection(sel), m_toFill(toFill){
}
template <class ProcessThis> inline void SelectionWithPlots<ProcessThis>::runFill(ProcessThis &t ){
    /// check the outcome of the selection once 
    if (m_Selection(t)){       
        /// if passed, we fill all of the plots
        for (auto & p : m_toFill){ 
            p.Fill(t);
        }
    }
}
template <class ProcessThis> void SelectionWithPlots<ProcessThis>::print(const std::string & spacer){
    std::cout << spacer << "--> Selection with ID "<<m_Selection.getID()<< " and "<<m_toFill.size()<<" objects to fill :"<<std::endl;
    for (auto & X : m_toFill){
        X.print(spacer+spacer); 
    }      
}

template <class ProcessThis> void SelectionWithPlots<ProcessThis>::initForFill(){
    for (auto & X : m_toFill){
        X.initForFill(); 
    }
}
template <class ProcessThis> void SelectionWithPlots<ProcessThis>::recombine(){
    for (auto & X : m_toFill){
        X.recombine(); 
    }
}


template <class ProcessThis> HistoFillTask<ProcessThis>::HistoFillTask (InputFile<ProcessThis> theSE, std::vector<SelectionWithPlots<ProcessThis>> toDo, int nBootStrap):
    m_InputFile(theSE),
    m_todo(toDo),
    m_n_bootstrap(nBootStrap){
}

template <class ProcessThis> bool HistoFillTask<ProcessThis>::validateInput(Long64_t & nevents_found){
    std::shared_ptr<TFile> f{};
    /// attempt to read the ntuple from the input file
    ProcessThis mmt = m_InputFile.getTreeFromFile(f); 
    /// Catch failure cases -  either no file or no ntuple in the file
    if (!f  || !f->IsOpen() || !mmt.getTree()){
        nevents_found = 0;
        return false; 
    }
    /// count entries 
    nevents_found = mmt.getEntries();
    /// close the file if we successfully opened it
    if (f && f->IsOpen()) {
        /// this needs to be guarded as ROOT I/O was not thread-safe
        /// at the time of writing 
        std::lock_guard<std::mutex> guardCloseFile(g_mutex_ROOTFileOpenClose);
        f->Close();
    }
    /// if we didn't find any entries, treat as failure
    if (nevents_found == 0) return false; 
    return true; 
}

template <class ProcessThis> void HistoFillTask<ProcessThis>::run(std::shared_ptr<IFillProgressTracker> tracker){

    /// copy the selection specific to this input file (filtering).
    Selection<ProcessThis> cuts = m_InputFile.getSelection();

    /// load the input file. 
    /// We do not need to validate the result since this has been done earlier
    /// using the dedicated method
    std::shared_ptr<TFile> f;
    ProcessThis mmt = m_InputFile.getTreeFromFile(f); 
    Long64_t nentries = mmt.getEntries();
    /// if the user has requested boot-strap weights, pass this information 
    /// to the underlying ntuple object. 
    if (m_n_bootstrap >0){
        mmt.setNBootstrapWeights(m_n_bootstrap);
        /// Seed a seed equal to the number of entries (unique per sample). 
        /// This should result in reproducible results across multiple runs. 
        mmt.seedBootStrap(nentries);
    }

    /// Set up the temporary object clones we need: 
    
    for (auto & selWithPlots : m_todo){
        selWithPlots.initForFill();
    }
    /// now we are ready for the event loop. 
    /// set up an incremental progress counter
    Long64_t ndone = 0;
    for (Long64_t entry = 0;  entry < nentries ; ++entry){
        /// update the ntuple branches to the new entry number 
        mmt.getEntry(entry);
        /// take care of progress reporting if the user-set 
        /// interval has passed. 
        if (tracker && ++ndone > m_reportingInterval){
            tracker->reportProgress(ndone); 
            /// reset the incremental counter
            ndone=0;
        }
        /// now check if we passed the file-level filtering cuts. 
        if (!cuts(mmt)){
            continue;
        }
        /// for each selection provided by the user, 
        /// check the outcome and fill all plots. 
        for (auto & selWithPlots : m_todo){
            selWithPlots.runFill(mmt);
        }
    }   /// end of the event loop. 

    /// If the file was successfully opened, we should close it.
    /// TODO: Consider adding a file management to re-use open files 
    ///       when reading multiple trees...
    if (f && f->IsOpen()) {
        /// guard ROOT I/O
        std::lock_guard<std::mutex> guardCloseFile(g_mutex_ROOTFileOpenClose);
        f->Close();
    }

    /// merge our outputs back into the parent plots
    for (auto & selWithPlots : m_todo){
        selWithPlots.recombine();
    }

    /// if the progress tracker is attached, we tell it we are done
    /// and update the event counter one final time
    if (tracker) {
        tracker->reportFileComplete();
        tracker->reportProgress(ndone);
    }
    
    return;
}

template <class ProcessThis> void HistoFillTask<ProcessThis>::setReportingInterval(Long64_t interval){
    m_reportingInterval = interval;
}

template <class ProcessThis> void HistoFillTask<ProcessThis>::print(const std::string & spacer){
    std::cout << spacer <<" --> HistoFillTask with the following properties: "<<std::endl; 
    std::cout << spacer << spacer<<"ID: "<<m_InputFile.getID()<<std::endl; 
    std::cout << spacer << spacer<<"File: "<<m_InputFile.getFileName()<<std::endl; 
    std::cout << spacer << spacer<<"Tree: "<<m_InputFile.getTreeName()<<std::endl; 
    std::cout << spacer << spacer<<"Bootstrap Toys: "<<m_n_bootstrap<<std::endl; 
    std::cout << spacer << spacer<<"Reporting Interval: "<<m_reportingInterval<<std::endl; 
    std::cout << spacer << spacer<<"Number of selections to process: "<<m_todo.size()<<std::endl; 
    for (auto & todo: m_todo){
        todo.print(spacer+spacer); 
    }
}

template <class ProcessThis>  std::shared_ptr<IHistoFillTask> InputFile<ProcessThis>::spawnTask (SelectionToFillableMap & payload, bool splitAndMerge){
    /// prepare a vector to store our selections with associated plots
    std::vector<SelectionWithPlots<ProcessThis>> selectionsWithPlots; 

        /// now walk through all the requested payloads for this input 
    for (auto & KV : payload){
        /// first get the selection
        auto selection = std::dynamic_pointer_cast<Selection<ProcessThis>>(KV.first);
        /// if the selection can not be matched to the underlying ntuple type, something 
        /// is wrong in the setup. This is usually indicative of a bug, since the 
        /// toolchain doesn't allow this to happen under normal usage
        if (!selection){
            std::cerr <<"Type mismatch when assigning a selection to the processing batch for input file "<<getFileName()<<" and Tree " <<getTreeName() << std::endl;
            continue;
        }
        /// for this selection, prepare another vector for all the plots that should be filled. 
        std::vector< FillTaskFragment<ProcessThis> > fillableForSelection;
        /// loop over all of the plots associated to the selection 
        for (auto fillable : KV.second){
            /// cast to the object type to fill 
            auto fillMe = std::dynamic_pointer_cast<IFillableFrom<ProcessThis>>(fillable);
            /// again mainly bug-catching, not expected in production
            if (!fillMe){
                std::cerr <<"Type mismatch when assigning a plot to a processing batch for input file "<<getFileName()<<" and Tree " <<getTreeName() << std::endl;
                continue;
            }
            /// now we can push the result of our cast into the vector
            fillableForSelection.emplace_back(fillMe, splitAndMerge);
        } 
        /// and then create the SelectionWithPlots instance in-place
        selectionsWithPlots.push_back(SelectionWithPlots<ProcessThis>(*selection, fillableForSelection));
    }
    /// finally, assemble a task an return it 
    return std::make_shared<HistoFillTask<ProcessThis> >(*this, selectionsWithPlots, m_n_bootstrap);
}
