/*
 * CutFlow.h
 *
 *  Created on: Nov 3, 2015
 *      Author: goblirsc
 */

#ifndef CUTFLOW_H_
#define CUTFLOW_H_

#include "TH1D.h"
#include <map>
#include <vector>

// Cut flow class
// stores selection progress, prints outcomes and writes histos
// can dynamically include cuts as it encounters them, or be initialized with a fixed list.
class CutFlow{
public:
    /// Constructor. 
    /// Can pass a predefined list of cuts, or leave empty to have the
    /// cut flow 'learn' cuts in the order it encounters them
    CutFlow(std::vector<std::string>  ordered_cuts = {});
    
    /// constructor that imports an existing cut flow
    /// as starting point. This will always 'learn' any additional cuts encountered
    CutFlow(const std::string & fname, const std::string & histoname, int first = 0, int last = -1);

    /// Fill the cut flow based on cut name (first argument). 
    /// Second argument: Event weight
    void Fill(const std::string & cut, Double_t weight =1.);

    /// Print the cut flow via std::cout
    void Print();

    /// import from a previously saved histo.
    /// Uses the bins in the range [first, last]. If last < 0, go until the last bin.
    /// In case the cut flow already has some defined bins, 
    /// this will insert not previously defined bins *behind* the imported ones
    void Import(const std::string & fname, const std::string & histoname, int first = 0, int last = -1);
    void Import(TH1F* theHist, int first = 0, int last = -1);
    
    /// Write the cut flow into a LaTex file
    void WriteLatex(const std::string & filename = "CutFlow.tex");

    /// Returns a TH1F with the cut flow outcome.
    /// Caller takes ownership of TH1
    TH1D* CutFlowHist(const std::string & name = "CutFlow");

    /// Returns a TH1F with relative cut efficiencies (i.e. fraction of events reaching the cut that are accepted)
    /// Caller takes ownership of TH1
    TH1D* RelEffHist(const std::string & name = "Eff_Rel");

    /// Returns a TH1F with absolute cut efficiencies (i.e. fraction of processed after cut)
    /// Caller takes ownership of TH1
    TH1D* AbsEffHist(const std::string & name = "Eff_Abs");

    /// use this to store Sum2 and Sumw2 for each bin
    typedef std::pair<Double_t,Double_t> ValWithSw2;


protected:
    // helper method. Returns Efficiency with error. Uses normal error approximation.
    std::pair<Double_t,Double_t> Eff (ValWithSw2 numerator, ValWithSw2 denominator);
    // if true, the Cut Flow dynamically includes newly encountered cuts. Otherwise, it sticks to the predefined list.
    bool m_learn_mode;
    // map containing the actual cut flow. Stores Sumw and Sumw2
    std::map <std::string, ValWithSw2> m_at_step;
    // vector of cuts in the intended order
    std::vector<std::string> m_ordered_cuts;

};  // class CutFLow

#endif /* CUTFLOW_H_ */
