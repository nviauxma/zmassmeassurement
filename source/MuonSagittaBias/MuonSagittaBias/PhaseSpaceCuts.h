#ifndef NTAU__PhaseSpaceCuts__H
#define NTAU__PhaseSpaceCuts__H

#include "MuonSagittaBias/SagittaBiasAnalyzer.h"
#include "NtauHelpers/Utils.h"

///     Small helper class to duplicate plots into different phase space regions
///     The complication of this analysis are to access easily the different track
///     properties via dedicated getter functions satisfying the pattern
///         <return_type>  property( int trk_sel); or
///         <return_type>  property( int trk_sel, bool pos_charge_selection);
///    Once this pattern is established duplication can be easily established by
///    augmenting the selection functions by a second argument selecting the
///   desired track particle.

class PhaseSpaceCuts {
public:
    /// Basic constructor
    ///      sel_name: String naming the selection meant for the saving of the file to disk
    ///      sel_title: Selection title to be plotted on the Canvas. Can contain TLatex formatting
    ///                 characters
    ///     sel_function: Function to evaluate the phase bin selection...
    ///                   The first argument is the refernce to the tree and the second one
    ///                   is the track selection
    PhaseSpaceCuts(const std::string& sel_name, const std::string& sel_title,
                   const std::function<bool(SagittaBiasAnalyzer&, int)>& sel_function);

    /// Creates an analysis selection object from the track specification
    Selection<SagittaBiasAnalyzer> make_cut(int trk_type, std::pair<float, float> mass_window) const;

    /// Returns the name of the object
    std::string name() const;
    /// Returns the selection title of the object
    std::string title() const;

    std::string selection_name(int trk_type, std::pair<float, float> mass_window) const;

private:
    std::function<bool(SagittaBiasAnalyzer&, int)> m_cut_func;
    std::string m_sel_name;
    std::string m_sel_title;
};

#endif
