#include <FourMomUtils/xAODP4Helpers.h>
#include <MuonSagittaBias/ArgumentParser.h>
#include <MuonSagittaBias/AsymmetryPlots.h>
#include <MuonSagittaBias/PhaseSpaceCuts.h>
#include <MuonSagittaBias/PlotProvider.h>
#include <MuonSagittaBias/SagittaBiasAnalyzer.h>
#include <MuonSagittaBias/SlicedPlotProvider.h>
#include <MuonSagittaBias/Utils.h>
#include <NtauHelpers/HistoBinConfig.h>
#include <NtauHelpers/OutFileWriter.h>
#include <NtauHelpers/SelectionWithTitle.h>

int main(int argc, char* argv[]) {
    /// Standard piece of code to setup the analysis...
    ArgumentParser parser;
    parser.summary_pdf("AllPseudoMassPlots")
        .out_root_file("PseudoMassTemplates.root")
        .inject_config_file("source/MuonSagittaBias/data/bias_injection_file0.txt");
    if (!parser.parse_args(argc, argv)) return EXIT_FAILURE;

    /// Create the output file to store all of the histograms

    PlotWriter<TH1> out_file{parser.make_root_file() ? parser.out_file() : nullptr};
    if (parser.make_root_file() && !out_file.get()) { return EXIT_FAILURE; }
    if (!parser.make_root_file() && !parser.make_pdfs()) {
        std::cerr << parser.app_name() << "No output format" << std::endl;
        return EXIT_FAILURE;
    }
    Sample<SagittaBiasAnalyzer> sample_to_process = parser.prepare_sample<SagittaBiasAnalyzer>();

    CanvasOptions canvas_opts = CanvasOptions()
                                    .RatioAxis(AxisConfig().Title("to truth-level").ExtraTitleOffset(0.4).Min(0.9).Max(1.005))
                                    .YAxis(AxisConfig().Title("Entries").ExtraTitleOffset(0.4).TopPadding(0.7))
                                    // .XAxis(AxisConfig().ExtraTitleOffset(1.3))
                                    .LabelStatusTag("Simulation Internal")
                                    .OutputDir(parser.out_dir());

    std::shared_ptr<MultiPagePdfHandle> multi_page =
        parser.make_pdfs() ? PlotUtils::startMultiPagePdfFile(parser.summary_pdf(), canvas_opts) : nullptr;

    /// Select the sagitta biases for the plots
    const std::vector<int>& inject_types = SagittaBiasAnalyzer::injection_types();
    std::vector<int> biases_to_plot{MuonTrk::TrackType::IDTrk};
    if (inject_types.size() > 1) {
        biases_to_plot += inject_types[SagittaBiasAnalyzer::smallest_inject_idx(MuonTrk::TrackType::IDTrk)];
        biases_to_plot += inject_types[SagittaBiasAnalyzer::largest_inject_idx(MuonTrk::TrackType::IDTrk)];
    }
    std::vector<PlotContent<TH1>> sagitta_biases;

    /// Require that the positive / negative lepton passes the Tight selection criteria
    std::vector<PhaseSpaceCuts> selections;
    selections += PhaseSpaceCuts("All_PlusTP", "All pairs", [](SagittaBiasAnalyzer& t, int) -> bool {
        return t.pass_id_wp(SagittaBiasAnalyzer::MuonWP::Tight, SagittaBiasAnalyzer::LepCharge::Plus);
    });

    selections += PhaseSpaceCuts("All_MinusTP", "All pairs", [](SagittaBiasAnalyzer& t, int) -> bool {
        return t.pass_id_wp(SagittaBiasAnalyzer::MuonWP::Tight, SagittaBiasAnalyzer::LepCharge::Minus);
    });

    std::vector<SlicedPlotProvider> slicers;
    std::vector<PlotFiller> fillers;

    /// Okay let's define the filler for the pseudo masses

    const std::vector<double> coarse_eta_bins{-2.5, 0., 2.5};
    const std::vector<double> fine_eta_bins = PlotUtils::getLinearBinning(-2.5, 2.5, 50);
    const std::vector<double> phi_bins = PlotUtils::getLinearBinning(-M_PI, M_PI, 1);

    const std::vector<double> pt_bins =
        PlotUtils::getLinearBinning(20, 100, (100. - 20.) / 2.5) + PlotUtils::getLinearBinning(100, 250, 150 / 10.);

    for (auto lep_charge : {SagittaBiasAnalyzer::LepCharge::Minus, SagittaBiasAnalyzer::LepCharge::Plus}) {
        const std::string q_sign = lep_charge == SagittaBiasAnalyzer::LepCharge::Minus ? "-" : "+";
        const std::string sign_name = lep_charge == SagittaBiasAnalyzer::LepCharge::Minus ? "Minus" : "Plus";
        const int sel_idx = lep_charge == SagittaBiasAnalyzer::LepCharge::Minus ? 0 : 1;

        std::vector<SlicedPlotProvider> pseudo_mass_slicers;

        pseudo_mass_slicers += SlicedPlotProvider(
            sample_to_process,
            [lep_charge](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
                return h->GetXaxis()->FindBin(t.eta(trk_type, lep_charge));
            },
            PlotUtils::HistoBinConfig()
                .x_bins(fine_eta_bins)
                .name(sign_name + "Lep_EtaHemisphere")
                .x_title(Form("#eta(#mu^{%s})", q_sign.c_str())),
            selections[sel_idx]);

        /// Then let's continue to a finer eta bining
        pseudo_mass_slicers += SlicedPlotProvider(
            sample_to_process,
            [lep_charge](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
                return h->GetXaxis()->FindBin(t.eta(trk_type, lep_charge));
            },
            PlotUtils::HistoBinConfig().x_bins(fine_eta_bins).name(sign_name + "Lep_Eta").x_title(Form("#eta(#mu^{%s})", q_sign.c_str())),
            selections[sel_idx]);
        /// Another plot against phi
        // pseudo_mass_slicers += SlicedPlotProvider(
        // sample_to_process,
        // [lep_charge](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
        // return h->GetXaxis()->FindBin(t.phi(trk_type, lep_charge));
        // },
        // PlotUtils::HistoBinConfig().x_bins(phi_bins).name(sign_name + "Lep_Phi").x_title(Form("#phi(#mu^{%s})", q_sign.c_str())),
        // selections[sel_idx]);
        /// Against pt
        // pseudo_mass_slicers += SlicedPlotProvider(
        // sample_to_process,
        // [lep_charge](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) { return h->GetXaxis()->FindBin(t.pt(trk_type, lep_charge)); },
        // PlotUtils::HistoBinConfig().x_bins(pt_bins).name(sign_name + "Lep_Pt").x_title(Form("#pt(#mu^{%s}) [GeV]", q_sign.c_str())),
        // selections[sel_idx]);

        /// Eta phi - coarse version
        pseudo_mass_slicers += SlicedPlotProvider(
            sample_to_process,
            [lep_charge](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
                return h->GetBin(h->GetXaxis()->FindBin(t.eta(trk_type, lep_charge)), h->GetYaxis()->FindBin(t.phi(trk_type, lep_charge)));
            },
            PlotUtils::HistoBinConfig()
                .x_bins(fine_eta_bins)
                .name(sign_name + "EtaHemispherePhi")
                .x_title(Form("#eta(#mu^{%s})", q_sign.c_str()))
                .y_bins(phi_bins)
                .y_title(Form("#phi(#mu^{%s})", q_sign.c_str())),
            selections[sel_idx]);

        /// Eta phi - fine version
        pseudo_mass_slicers += SlicedPlotProvider(
            sample_to_process,
            [lep_charge](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
                return h->GetBin(h->GetXaxis()->FindBin(t.eta(trk_type, lep_charge)), h->GetYaxis()->FindBin(t.phi(trk_type, lep_charge)));
            },
            PlotUtils::HistoBinConfig()
                .x_bins(fine_eta_bins)
                .name(sign_name + "EtaPhi")
                .x_title(Form("#eta(#mu^{%s})", q_sign.c_str()))
                .y_bins(phi_bins)
                .y_title(Form("#phi(#mu^{%s})", q_sign.c_str())),
            selections[sel_idx]);

        /// Eta - pt
        // pseudo_mass_slicers += SlicedPlotProvider(
        // sample_to_process,
        // [lep_charge](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
        // return h->GetBin(h->GetXaxis()->FindBin(t.pt(trk_type, lep_charge)), h->GetYaxis()->FindBin(t.eta(trk_type, lep_charge)));
        // },
        // PlotUtils::HistoBinConfig()
        // .x_bins(pt_bins)
        // .name(sign_name + "PtEta")
        // .x_title(Form("p_{T}(#mu^{%s}) [GeV]", q_sign.c_str()))
        // .y_bins(fine_eta_bins)
        // .y_title(Form("#eta(#mu^{%s})", q_sign.c_str())),
        // selections[sel_idx]);

        /// The selections are done now, we can devote our time to the pseudo mass
        PlotFiller pseudo_mass_filler_z([lep_charge](const TH1* h, SagittaBiasAnalyzer& t,
                                                     int trk_type) { return h->GetXaxis()->FindBin(t.pseudo_mass(trk_type, lep_charge)); },
                                        PlotUtils::HistoBinConfig()
                                            .name("PseudoMass" + sign_name)
                                            .x_title(Form("M^{pseudo}_{#mu^{%s}} [GeV]", q_sign.c_str()))
                                            .x_min(45)
                                            .x_max(151)
                                            .x_num_bins(53));

        PlotFiller pseudo_mass_filler_jpsi(
            [lep_charge](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
                return h->GetXaxis()->FindBin(t.pseudo_mass(trk_type, lep_charge));
            },
            PlotUtils::HistoBinConfig()
                .name("PseudoMass" + sign_name)
                .x_title(Form("M^{pseudo}_{#mu^{%s}} [GeV]", q_sign.c_str()))
                .x_min(0.0)
                .x_max(10.5)
                .x_num_bins(100));

                               

        PlotFiller invariant_pt_filler_jpsi(
            [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
                return h->GetXaxis()->FindBin(t.invariant_pt(trk_type));
            },
            PlotUtils::HistoBinConfig()
                .name("pT")
                .x_title(Form("p_{T}^{Z} [GeV]"))
                .x_min(0.0)
                .x_max(100.0)
                .x_num_bins(100));       



        PlotFiller invariant_mass_filler_jpsi(
            [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
                return h->GetXaxis()->FindBin(t.invariant_m(trk_type));
            },
            PlotUtils::HistoBinConfig()
                .name("Mass")
                .x_title(Form("M_{#mumu} [GeV]"))
                .x_min(2.7)
                .x_max(4.0)
                .x_num_bins(100));

         PlotFiller invariant_mass_filler_zmumu(
            [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
                return h->GetXaxis()->FindBin(t.invariant_m(trk_type));
            },
            PlotUtils::HistoBinConfig()
                .name("Mass")
                .x_title(Form("M_{#mumu} [GeV]"))
                .x_min(61.0)
                .x_max(121.0)
                .x_num_bins(100));
                               

        PlotFiller invariant_pt_filler_zmumu(
            [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
                return h->GetXaxis()->FindBin(t.invariant_pt(trk_type));
            },
            PlotUtils::HistoBinConfig()
                .name("pT")
                .x_title(Form("p_{T}^{Z} [GeV]"))
                .x_min(0.0)
                .x_max(20.0)
                .x_num_bins(100));       





        PlotFiller invariant_eta_filler_jpsi(
            [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
                return h->GetXaxis()->FindBin(t.invariant_eta(trk_type));
            },
            PlotUtils::HistoBinConfig()
                .name("invariant_eta_jpsi")
                .x_title(Form("#eta^{J/#psi}"))
                .x_min(-2.5)
                .x_max(2.5)
                .x_num_bins(100));   

        PlotFiller invariant_Y_filler_jpsi(
            [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
                return h->GetXaxis()->FindBin(t.invariant_Y(trk_type));
            },
            PlotUtils::HistoBinConfig()
                .name("invariant_Y_jpsi")
                .x_title(Form("#eta^{J/#psi}"))
                .x_min(-2.5)
                .x_max(2.5)
                .x_num_bins(100));                   

        PlotFiller invariant_eta_filler_zmumu(
            [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
                return h->GetXaxis()->FindBin(t.invariant_eta(trk_type));
            },
            PlotUtils::HistoBinConfig()
                .name("invariant_eta_zmumu")
                .x_title(Form("#eta^{Zmumu}"))
                .x_min(-2.5)
                .x_max(2.5)
                .x_num_bins(100));   

                

        for (SlicedPlotProvider& prov : pseudo_mass_slicers) {
            if (parser.make_pdfs()) {
                sagitta_biases += prov.make_plots(invariant_eta_filler_zmumu, biases_to_plot, {}, {"Z#rightarrow#mu#mu", prov.title()}, "Raw_Z",
                                                  multi_page, canvas_opts, {-2.5, 2.5});
                sagitta_biases += prov.make_plots(pseudo_mass_filler_z, biases_to_plot, {}, {"Z#rightarrow#mu#mu", prov.title()}, "Raw_Z",
                                                  multi_page, canvas_opts, {61, 121});
                sagitta_biases += prov.make_plots(pseudo_mass_filler_z, biases_to_plot, {}, {"Z#rightarrow#mu#mu", prov.title()}, "Raw_Z",
                                                  multi_page, canvas_opts, {121, 150});
                sagitta_biases += prov.make_plots(pseudo_mass_filler_z, biases_to_plot, {}, {"Z#rightarrow#mu#mu", prov.title()}, "Raw_Z",
                                                  multi_page, canvas_opts, {150, 250});
                sagitta_biases += prov.make_plots(pseudo_mass_filler_z, biases_to_plot, {}, {"Z#rightarrow#mu#mu", prov.title()}, "Raw_Z",
                                                  multi_page, canvas_opts, {250, 500});
                sagitta_biases += prov.make_plots(pseudo_mass_filler_z, biases_to_plot, {}, {"Z#rightarrow#mu#mu", prov.title()}, "Raw_Z",
                                                  multi_page, canvas_opts, {500, 1000});         
                sagitta_biases += prov.make_plots(invariant_pt_filler_zmumu, biases_to_plot, {}, {"Z/#rightarrow#mu#mu", prov.title()}, "Raw_Z",
                                                  multi_page, canvas_opts, {0.0, 20.0});                                       
                 sagitta_biases += prov.make_plots(invariant_mass_filler_zmumu, biases_to_plot, {}, {"Z/#rightarrow#mu#mu", prov.title()}, "Raw_Z",
                                                  multi_page, canvas_opts, {61.0, 121.0});                                                                                                                                                                            
                sagitta_biases += prov.make_plots(pseudo_mass_filler_jpsi, biases_to_plot, {}, {"J/#psi#rightarrow#mu#mu", prov.title()},
                                                  "JPsi", multi_page, canvas_opts, {2.7, 3.5});
                sagitta_biases += prov.make_plots(invariant_mass_filler_jpsi, biases_to_plot, {}, {"J/#psi#rightarrow#mu#mu", prov.title()},
                                                  "JPsi", multi_page, canvas_opts, {2.7, 4.0});                       
                sagitta_biases += prov.make_plots(invariant_pt_filler_jpsi, biases_to_plot, {}, {"J/#psi#rightarrow#mu#mu", prov.title()},
                                                  "JPsi", multi_page, canvas_opts, {0.0, 100.0}); 
                sagitta_biases += prov.make_plots(invariant_eta_filler_jpsi, biases_to_plot, {}, {"J/#psi#rightarrow#mu#mu", prov.title()},
                                                  "JPsi", multi_page, canvas_opts, {-2.5, 2.5});   
                sagitta_biases += prov.make_plots(invariant_Y_filler_jpsi, biases_to_plot, {}, {"J/#psi#rightarrow#mu#mu", prov.title()},
                                                  "JPsi", multi_page, canvas_opts, {-2.5, 2.5});                                                                                 
                sagitta_biases += prov.make_plots(pseudo_mass_filler_jpsi, biases_to_plot, {}, {"J/#psi#rightarrow#mu#mu", prov.title()},
                                                  "JPsi", multi_page, canvas_opts, {0.0, 1.0});
                sagitta_biases += prov.make_plots(pseudo_mass_filler_jpsi, biases_to_plot, {}, {"J/#psi#rightarrow#mu#mu", prov.title()},
                                                  "JPsi", multi_page, canvas_opts, {1.0, 2.0});
                sagitta_biases += prov.make_plots(pseudo_mass_filler_jpsi, biases_to_plot, {}, {"J/#psi#rightarrow#mu#mu", prov.title()},
                                                  "JPsi", multi_page, canvas_opts, {2.0, 2.7});
                sagitta_biases += prov.make_plots(pseudo_mass_filler_jpsi, biases_to_plot, {}, {"J/#psi#rightarrow#mu#mu", prov.title()},
                                                  "JPsi", multi_page, canvas_opts, {3.5, 4.5});
                sagitta_biases += prov.make_plots(pseudo_mass_filler_jpsi, biases_to_plot, {}, {"J/#psi#rightarrow#mu#mu", prov.title()},
                                                  "JPsi", multi_page, canvas_opts, {4.5, 5.5});                                                                                                                                                                          
            }

            prov.add_plot(invariant_eta_filler_zmumu, parser.get_trk_types_ROOT_file(), out_file, {-2.5, 2.5});
            prov.add_plot(pseudo_mass_filler_z, parser.get_trk_types_ROOT_file(), out_file, {61, 121});
            prov.add_plot(pseudo_mass_filler_z, parser.get_trk_types_ROOT_file(), out_file, {121, 150});
            prov.add_plot(pseudo_mass_filler_z, parser.get_trk_types_ROOT_file(), out_file, {150, 250});
            prov.add_plot(pseudo_mass_filler_z, parser.get_trk_types_ROOT_file(), out_file, {250, 500});
            prov.add_plot(pseudo_mass_filler_z, parser.get_trk_types_ROOT_file(), out_file, {500, 1000});
            prov.add_plot(invariant_mass_filler_zmumu, parser.get_trk_types_ROOT_file(), out_file, {61.0, 121.0});
            prov.add_plot(invariant_pt_filler_zmumu, parser.get_trk_types_ROOT_file(), out_file, {0.0, 100.0});
            prov.add_plot(invariant_mass_filler_jpsi, parser.get_trk_types_ROOT_file(), out_file, {2.7, 4.0});
            prov.add_plot(invariant_pt_filler_jpsi, parser.get_trk_types_ROOT_file(), out_file, {0.0, 50.0});
            prov.add_plot(invariant_eta_filler_jpsi, parser.get_trk_types_ROOT_file(), out_file, {-2.5, 2.5});
            prov.add_plot(invariant_Y_filler_jpsi, parser.get_trk_types_ROOT_file(), out_file, {-2.5, 2.5});
            prov.add_plot(pseudo_mass_filler_jpsi, parser.get_trk_types_ROOT_file(), out_file, {2.7, 3.5});
            prov.add_plot(pseudo_mass_filler_jpsi, parser.get_trk_types_ROOT_file(), out_file, {0.0, 1.0});
            prov.add_plot(pseudo_mass_filler_jpsi, parser.get_trk_types_ROOT_file(), out_file, {1.0, 2.0});
            prov.add_plot(pseudo_mass_filler_jpsi, parser.get_trk_types_ROOT_file(), out_file, {2.0, 2.7});
            prov.add_plot(pseudo_mass_filler_jpsi, parser.get_trk_types_ROOT_file(), out_file, {3.5, 4.5});
            prov.add_plot(pseudo_mass_filler_jpsi, parser.get_trk_types_ROOT_file(), out_file, {4.5, 5.5});

        }
    }
    std::sort(sagitta_biases.begin(), sagitta_biases.end(),
              [](const PlotContent<TH1>& a, const PlotContent<TH1>& b) { return a.getFileName() < b.getFileName(); });
    for (auto& pc : sagitta_biases) {
        if (count_non_empty(pc)) DefaultPlotting::draw1D(pc);
    }
    return EXIT_SUCCESS;
}
