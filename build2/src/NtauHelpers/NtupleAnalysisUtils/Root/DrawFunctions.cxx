#include "NtupleAnalysisUtils/Helpers/DrawFunctions.h"
#include "NtupleAnalysisUtils/Helpers/StringHelpers.h"
#include "TArrow.h" 


using namespace PlotUtils; 

TLatex* PlotUtils::drawTLatex(double x, double y, const std::string & text, const DrawObjectFormat & format){
    TLatex tl;
    
    /// the default font size is taken from gStyle
    int theSize = gStyle->GetTextSize(); 
    /// If we have a user-specified size, override. 
    /// This is added because the default "Size" value 
    /// is intended for markers and set to 1  
    if (format.Size.isUserSet()){
        theSize = format.Size(); 
    }   
    tl.SetTextSize (theSize);
    tl.SetTextFont (format.Font());
    tl.SetTextColor(format.Color());
    tl.SetTextAlign(format.Align());
    tl.SetNDC(format.NDC());
    return tl.DrawLatex(x,y,text.c_str());
}

TLatex* PlotUtils::drawAtlas(double x, double y, const std::string & status, const DrawObjectFormat & format){
    auto auxFormat = format; 
    if (!format.Font.isUserSet()) auxFormat.Font(43); 
    return drawTLatex(x,y,"#font[72]{ATLAS} "+status,auxFormat);
}
TLatex* PlotUtils::drawLumiSqrtS(double x, double y, const std::string & lumi, const std::string & sqrts, const DrawObjectFormat & format){
    auto auxFormat = format; 
    if (!format.Font.isUserSet()) auxFormat.Font(43); 
    return drawTLatex(x,y,"#sqrt{s}="+sqrts+(lumi.empty()?"":", ")+lumi,auxFormat);
}

std::vector<TLatex*> PlotUtils::drawMultipleLabels (double x, double y0, double dy, const std::vector<std::string> & labels, const DrawObjectFormat & format){
    double y = y0; 
    std::vector<TLatex*> drawnObjects; 
    for (auto l : labels){
        drawnObjects.push_back(drawTLatex(x, y, l, format));
        y-=dy;
    }
    return drawnObjects;
}

// draw a legend
TLegend* PlotUtils::drawLegend(std::vector<LegendEntry> entries,  const CanvasOptions & opts, const DrawObjectFormat & format){
    TLegend* leg = new TLegend(toPadCoordsX(opts.LegendStartX()),toPadCoordsY(opts.LegendStartY()),toPadCoordsX(opts.LegendEndX()),toPadCoordsY(opts.LegendEndY()));
    leg->SetFillStyle(0);
    leg->SetBorderSize(0);
    leg->SetTextFont(43);
    leg->SetTextSize(opts.LabelSize()); 
    /// for legends, we don't resort to defaults but only use explicitly user-set options. 
    if (format.Font.isUserSet()) leg->SetTextFont(format.Font()); 
    if (format.Size.isUserSet()) leg->SetTextSize(format.Size()); 
    if (format.Align.isUserSet()) leg->SetTextAlign(format.Align()); 
    if (format.Color.isUserSet()) leg->SetTextColor(format.Color()); 
    if (format.Style.isUserSet()) leg->SetLineStyle(format.Style()); 
    if (format.Width.isUserSet()) leg->SetLineWidth(format.Width()); 
    for (LegendEntry & entry : entries){
        leg->AddEntry(entry.obj, entry.title.c_str(),entry.drawOpt.c_str());
    }
    leg->Draw();
    return leg;
}
void PlotUtils::drawMarker(double x, double y, const DrawObjectFormat & format){
    TMarker m (x,y,format.Style());
    m.SetMarkerStyle(format.Style());
    m.SetMarkerSize(format.Size());
    m.SetMarkerColor(format.Color());
    m.DrawMarker(x,y);
}
TLine*  PlotUtils::drawLine(double x1, double y1, double x2, double y2, const DrawObjectFormat & format ){
    TLine l (x1,y1,x2,y2);
    l.SetLineStyle(format.Style());
    l.SetLineWidth(format.Width());
    l.SetLineColor(format.Color());
    return l.DrawLine(x1,y1,x2,y2);
}

// create a 2-pad canvas, useful for 1D plots with a ratio panel 
MultiPadCanvas PlotUtils::prepareTwoPadCanvas (const CanvasOptions & opts){

    std::shared_ptr<TCanvas> can = prepareCanvas(opts); 
    
    double verticalSplit = opts.VerticalSplit();
    std::shared_ptr<TPad> p1 = std::make_shared<TPad>("p1", "p1", 0, verticalSplit, 1, 1);
    std::shared_ptr<TPad> p2 = std::make_shared<TPad>("p2", "p2", 0, 0, 1, verticalSplit);

    double topMargin     = opts.TopMargin();
    double bottomMargin  = opts.BottomMargin();
    double rightMargin   = opts.RightMargin();
    double leftMargin    = opts.LeftMargin(); 

    p1->SetBottomMargin(0);
    p1->SetTopMargin(topMargin/(1.-verticalSplit));
    p1->SetRightMargin(rightMargin);
    p1->SetLeftMargin(leftMargin);

    p2->Draw();
    p2->SetTopMargin(0);
    p2->SetRightMargin(rightMargin);
    p2->SetBottomMargin(bottomMargin/verticalSplit);
    p2->SetLeftMargin(leftMargin);
    p2->SetGridy();

    can->cd();
    p1->Draw();
    p2->Draw();


    if (opts.YAxis().Log()) p1->SetLogy();
    if (opts.RatioAxis().Log()) p2->SetLogy();
    if (opts.XAxis().Log()) {
        p1->SetLogx();
        p2->SetLogx();
    }
    return MultiPadCanvas(can,{p1,p2});
}

// create a 3-pad canvas, useful for 1D plots with a ratio panel 
MultiPadCanvas PlotUtils::prepareThreePadCanvas (const CanvasOptions & opts ){

    std::shared_ptr<TCanvas> can = prepareCanvas(opts); 
    
    double verticalSplit = opts.VerticalSplit();

    double topMargin     = opts.TopMargin();
    double bottomMargin  = opts.BottomMargin();
    double rightMargin   = opts.RightMargin();
    double leftMargin    = opts.LeftMargin(); 

    /// here, we split the bottom half in two even-sized chunks 
    double y_lowerSplit = 0.5 * (verticalSplit + bottomMargin);

    std::shared_ptr<TPad> p1 = std::make_shared<TPad>("p1", "p1", 0, verticalSplit, 1, 1);
    std::shared_ptr<TPad> p2 = std::make_shared<TPad>("p2", "p2", 0, y_lowerSplit, 1, verticalSplit);
    std::shared_ptr<TPad> p3 = std::make_shared<TPad>("p3", "p3", 0, 0, 1, y_lowerSplit);

    p1->SetBottomMargin(0);
    p1->SetTopMargin(topMargin/(1.-verticalSplit));
    p1->SetLeftMargin(leftMargin);
    p1->SetRightMargin(rightMargin);

    p2->Draw();
    p2->SetTopMargin(0);
    p2->SetBottomMargin(0.0);
    p2->SetLeftMargin(leftMargin);
    p2->SetRightMargin(rightMargin);

    p2->SetGridy();

    p3->Draw();
    p3->SetTopMargin(0);
    p3->SetBottomMargin(bottomMargin / (1. - y_lowerSplit));
    p3->SetLeftMargin(leftMargin);
    p3->SetRightMargin(rightMargin);

    p3->SetGridy();

    can->cd();
    p1->Draw();
    p2->Draw();
    p3->Draw();
    return MultiPadCanvas(can,{p1,p2,p3});
}


MultiPadCanvas PlotUtils::prepare2x2Canvas (const CanvasOptions & opts ){   
    std::shared_ptr<TCanvas> can = prepareCanvas(opts); 
    can->cd();
    std::vector<std::shared_ptr<TPad>> pads = prepareNx2Canvas(2, opts);
    return MultiPadCanvas(can,pads);
}

MultiPadCanvas PlotUtils::prepare3x2Canvas (const CanvasOptions & opts ){
    std::shared_ptr<TCanvas> can = prepareCanvas(opts); 
    can->cd();
    std::vector<std::shared_ptr<TPad>> pads = prepareNx2Canvas(3, opts);
    return MultiPadCanvas(can,pads);
}
MultiPadCanvas PlotUtils::prepare4x2Canvas (const CanvasOptions & opts ){
    std::shared_ptr<TCanvas> can = prepareCanvas(opts); 
    can->cd();
    std::vector<std::shared_ptr<TPad>> pads = prepareNx2Canvas(4, opts);
    return MultiPadCanvas(can,pads);
}


std::shared_ptr<TCanvas> PlotUtils::prepareCanvas(const CanvasOptions & opts){
    double w = opts.CanSizeX();
    double h = opts.CanSizeY();
    return std::make_shared<TCanvas>(RandomString(6).c_str(), "",w,h); 
}

std::vector<std::shared_ptr<TPad>> PlotUtils::prepareNx2Canvas (int nrows, const CanvasOptions & opts ){
    std::vector<std::shared_ptr<TPad>> mypads;
    if (nrows == 0){
        std::cerr << "zero rows. Are you kidding me? "<<std::endl; return mypads;
    }

    double hSplit = opts.HorizontalSplit(); 

    double topMargin     = opts.TopMargin();
    double bottomMargin  = opts.BottomMargin();
    double rightMargin   = opts.RightMargin();
    double leftMargin    = opts.LeftMargin(); 

    double x_split = leftMargin + hSplit * (1. - leftMargin - rightMargin);
    double vertPadSize = (1. - topMargin - bottomMargin) / nrows; 

    for (int k = 0; k < nrows; ++k){
        // create pads for this row. Height varies if we are in the top or bottom row, to fit in the margins. 
        double ylow  = 1. - topMargin - (k+1) * vertPadSize; 
        double yhigh = 1. - topMargin -     k * vertPadSize;

        if (k == (nrows - 1)) ylow  -= bottomMargin; 
        if (k==0)             yhigh += topMargin; 

        std::shared_ptr<TPad> left =  std::make_shared<TPad>(Form("%s_%i_left", gPad->GetName(), k),"aPad",0,       ylow, x_split, yhigh);
        std::shared_ptr<TPad> right = std::make_shared<TPad>(Form("%s_%i_right",gPad->GetName(), k),"aPad",x_split, ylow, 1.,      yhigh);

        // set margins to give us the desired margins 
        left->SetRightMargin(0);
        right->SetLeftMargin(0);

        right->SetRightMargin(rightMargin / (1.-x_split));
        left->SetLeftMargin  (leftMargin  / (   x_split));

        double padTopMargin = 0; 
        if (k ==0 ){
            if (nrows == 1) padTopMargin = topMargin;
            else padTopMargin = topMargin / (topMargin + vertPadSize);
        }
        double padBottomMargin = 0; 
        if (k ==nrows - 1 ){
            if (nrows == 1) padBottomMargin = bottomMargin;
            else padBottomMargin = bottomMargin / (vertPadSize+bottomMargin);
        }
        left->SetTopMargin(padTopMargin);
        right->SetTopMargin(padTopMargin);
        left->SetBottomMargin(padBottomMargin);
        right->SetBottomMargin(padBottomMargin);

        // add grid lines
        left->SetGridx();
        right->SetGridx();
        right->SetGridy();

        // set log options 
        left->SetLogy();
        left->SetLogx();
        right->SetLogx();

        left->Draw();
        right->Draw();

        mypads.push_back(left);
        mypads.push_back(right);

    }
    return mypads;
}

void PlotUtils::addAxisLabels(std::vector<double> locations, TAxis* reference, bool isLog, double dy){
    double min = reference->GetXmin();
    double max = reference->GetXmax();
    double interval = (isLog ? log(max)-log(min) : max-min);
    // I tried to figure this out, but TGaxis::PaintAxis is plain unreadable. 
    // So fallback to an experimentally determined additional contribution based on the label size. 
    // Feel free to fix if you can figure out the proper way! 
    double y = gPad->GetBottomMargin() - (reference->GetLabelOffset() * gPad->GetWw() * gPad->GetAbsWNDC() + 0.13 * reference->GetLabelSize()) / (gPad->GetAbsHNDC() * gPad->GetWh()) + dy;
    double loc_min = gPad->GetLeftMargin(); 
    double loc_max = 1.-gPad->GetRightMargin();
    double xrange = loc_max - loc_min; 

    for (auto & l : locations){
        double x = loc_min + (isLog ? log(l) - log(min) : l-min )/interval * xrange; 
        drawTLatex(x,y,Form("%.0f",l),DrawObjectFormat().Size(reference->GetLabelSize()).Font(43).Align(23));
    }
}

void PlotUtils::adaptLabels (TH1* hist, const CanvasOptions & opts){
    if (!hist) return;

    size_t labelSize = opts.LabelSize();
    size_t titleSize = opts.TitleSize();

    hist->GetXaxis()->SetLabelFont(43);
    hist->GetXaxis()->SetLabelSize(labelSize);
    hist->GetXaxis()->SetTitleFont(43);
    hist->GetXaxis()->SetTitleSize(titleSize);

    hist->GetYaxis()->SetLabelFont(43);
    hist->GetYaxis()->SetLabelSize(labelSize);
    hist->GetYaxis()->SetTitleFont(43);
    hist->GetYaxis()->SetTitleSize(titleSize);
}


void PlotUtils::adaptLabels (std::vector<TH1*> histos, const CanvasOptions & opts){
    for (TH1* hist : histos){
        adaptLabels(hist, opts);
    }
}
// draw arrows into a ratio
void PlotUtils::addArrowsToRatio(TH1* ratio, double cutoff_low, double cutoff_up, int firstbin, int lastbin ){
    for (int p = 1; p < ratio->GetNbinsX()+1; ++p){
        if ( p < firstbin || p > lastbin) continue;
        double yu = ratio->GetBinContent(p) - ratio->GetBinError(p);
        double yd = ratio->GetBinContent(p) + ratio->GetBinError(p);
        double xarr = ratio->GetXaxis()->GetBinCenter(p);
        bool drawMe = false;
        double y1=0,y2=0;
        if (yu > cutoff_up ){
            double ru = cutoff_up - 1; 
            y1 = 1 + 0.8 * ru; 
            y2 = cutoff_up; 
            drawMe=true;
        }
        if (yd < cutoff_low){
            double rd = 1 - cutoff_low; 
            y1 = 1 - 0.8 * rd; 
            y2 = cutoff_low; 
            drawMe=true;
        }
        if (ratio->GetBinContent(p) == 0){
            drawMe=false;
        }
        if (drawMe){
            TArrow* arr = new TArrow(xarr, y1,xarr,y2,0.02,"-|>");
            arr->SetFillColor(kBlue);
            arr->SetLineColor(kBlue);
            arr->SetLineWidth(2);
            arr->Draw(); 
        }   
    }
}
void PlotUtils::updateTopFrame(TH1* frame, const CanvasOptions & opts){
    PlotUtils::adaptLabels(frame,opts);
    frame->GetXaxis()->SetTitleOffset(frame->GetXaxis()->GetTitleOffset()*(1.+opts.XAxis().ExtraTitleOffset()));
    frame->GetYaxis()->SetTitleOffset(frame->GetYaxis()->GetTitleOffset()*(1.+opts.YAxis().ExtraTitleOffset()));
    frame->GetXaxis()->SetTitleSize(opts.TitleSize());
    frame->GetYaxis()->SetTitleSize(opts.TitleSize());
    frame->GetXaxis()->SetLabelSize(opts.LabelSize());
    frame->GetYaxis()->SetLabelSize(opts.LabelSize());
}

void PlotUtils::updateRatioFrame(TH1* frame, const CanvasOptions & opts){

    PlotUtils::adaptLabels(frame);
    frame->GetXaxis()->SetTitleOffset(frame->GetXaxis()->GetTitleOffset()*(1.+opts.XAxis().ExtraTitleOffset()));
    frame->GetYaxis()->SetTitleOffset(frame->GetYaxis()->GetTitleOffset()*(1.+opts.RatioAxis().ExtraTitleOffset()));
    frame->GetXaxis()->SetTitleSize(opts.TitleSize());
    frame->GetYaxis()->SetTitleSize(opts.TitleSize());
    frame->GetXaxis()->SetLabelSize(opts.LabelSize());
    frame->GetYaxis()->SetLabelSize(opts.LabelSize());
}


std::vector<TLatex*> PlotUtils::drawLabels(const std::vector<std::string> & extra_labels,
                                           const CanvasOptions & opts,
                                           const DrawObjectFormat & format){

    std::vector<TLatex*> labels;
    // a) Atlas Label


    /// Update the format based on the canvas options
    auto localFormat = format; 
    bool sizesFromCanvasOpts = !format.Size.isUserSet(); 

    if (opts.DoAtlasLabel()){
        if (sizesFromCanvasOpts) localFormat.Size(opts.TitleSize());
         labels.push_back(PlotUtils::drawAtlas(toPadCoordsX(opts.AtlasLabelX()), 
                                               toPadCoordsY(opts.AtlasLabelY()),
                                               opts.LabelStatusTag(),
                                               localFormat));
    }
    double x = toPadCoordsX(opts.OtherLabelX());
    double y = toPadCoordsY(opts.OtherLabelStartY());
    double dy = opts.OtherLabelStepY();
    
    if (sizesFromCanvasOpts) localFormat.Size(opts.LabelSize());

    // b) sqrt(s) 
    if (opts.DoLumiSqrtSLabel()){
        labels.push_back(PlotUtils::drawLumiSqrtS(x,y, opts.LabelLumiTag(), opts.LabelSqrtsTag(),localFormat));
        y -= dy;
    }

    // c) anything else
    auto moreLabels = PlotUtils::drawMultipleLabels(x, y, dy, extra_labels, localFormat);
    labels.insert(labels.end(),moreLabels.begin(),moreLabels.end());
    return labels; 
} 
double PlotUtils::toPadCoordsY(double y_internal){
    double offset = (y_internal < 0 ? 1.-gPad->GetTopMargin() : gPad->GetBottomMargin());
    return offset + y_internal;
} 
double PlotUtils::toPadCoordsX(double x_internal){
    double offset = (x_internal < 0 ? 1.-gPad->GetRightMargin() : gPad->GetLeftMargin());
    return offset + x_internal;
} 
