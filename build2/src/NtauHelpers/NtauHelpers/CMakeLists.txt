# Declare the package name:
atlas_subdir( NtauHelpers)

# Declare the package's dependencies:
atlas_depends_on_subdirs(
   PUBLIC
    MuonSpectrometer/MuonIdHelpers
     Event/xAOD/xAODMuon
     Tools/PathResolver
     ClusterSubmission
     NtupleAnalysisUtils
     PhysicsAnalysis/Interfaces/PMGAnalysisInterfaces
     MuonSpectrometer/MuonIdHelpers
     PhysicsAnalysis/MCTruthClassifier/MCTruthClassifier
     PhysicsAnalysis/AnalysisCommon/PileupReweighting
     DetectorDescription/IdDictParser
     PRIVATE
     Control/AthToolSupport/AsgTools
     Control/AthenaBaseComps
     Control/CxxUtils
     GaudiKernel)

# External dependencies:
find_package( ROOT COMPONENTS Core Tree RIO Hist Physics Gpad RooFitCore RooFit Minuit Graf )
find_package( Boost )
# Libraries in the package:
atlas_add_library( NtauHelpersLib
   NtauHelpers/*.h Root/*.cxx
   PUBLIC_HEADERS NtauHelpers
   INCLUDE_DIRS ${Boost_INCLUDE_DIRS}
   PRIVATE_INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
   LINK_LIBRARIES  ${ROOT_LIBRARIES}  ${Boost_LIBRARIES} PathResolver NtupleAnalysisUtils MuonIdHelpersLib MCTruthClassifier PileupReweightingLib 
			             xAODMuon PMGAnalysisInterfacesLib)


# build exes 
file(GLOB_RECURSE files "util/*.cxx")
  foreach(_exeFile ${files})
      get_filename_component(_theExec ${_exeFile} NAME_WE)
      get_filename_component(_theLoc ${_exeFile} DIRECTORY)
      # we specify a folder for programs we do not want to compile. Useful during r21 transition...
      if(${_theLoc} MATCHES "DoNotBuild")
      else()
        atlas_add_executable( ${_theExec}
        ${_exeFile}
        INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
        LINK_LIBRARIES ${ROOT_LIBRARIES} NtauHelpersLib)
      endif()
  endforeach()
