#ifndef NTAU__SAMPLE__IXX
#define NTAU__SAMPLE__IXX

#include <algorithm>
#include <iostream>
#include "TTree.h"
// #include "NtupleAnalysisUtils/Sample.h"
// #include "NtupleAnalysisUtils/HistoFillerHelpers.h"
#include "NtupleAnalysisUtils/Helpers/IOFunctions.h"

template <class ProcessThis> InputFile<ProcessThis>::InputFile(const std::string & thefile, const std::string & treename, Selection<ProcessThis> specificCuts): m_fileName(thefile),
                   m_treeName(treename), 
                   m_selection(specificCuts){
    /// the input file class gets a deterministic ID, to make samples reproducible
    updateDynamicID(); 
}
template <class ProcessThis> ProcessThis InputFile<ProcessThis>::getTreeFromFile(std::shared_ptr<TFile> & f ) const{
    return ProcessThis( Sample<ProcessThis>::template getObjectFromFile<TTree>(m_fileName, m_treeName, f));
}
template <class ProcessThis> std::shared_ptr<IInputFile> InputFile<ProcessThis>::clone() const{
    auto got =  std::make_shared<InputFile<ProcessThis>>(*this);
    return got;
}
template <class ProcessThis> void InputFile<ProcessThis>::setTreeName(const std::string & name){
    m_treeName = name;
    updateDynamicID();
}
    /// and the name of the file 
template <class ProcessThis> void InputFile<ProcessThis>::setFileName(const std::string & name){
    m_fileName = name;
    updateDynamicID();
}
template <class ProcessThis> void InputFile<ProcessThis>::setSelection(Selection<ProcessThis> sel){
    m_selection = sel;
    updateDynamicID();
}
template <class ProcessThis> const std::string & InputFile<ProcessThis>::getFileName() const { 
    return m_fileName;
} 
template <class ProcessThis> const std::string & InputFile<ProcessThis>::getTreeName() const { 
    return m_treeName;
} 
template <class ProcessThis> Selection<ProcessThis> InputFile<ProcessThis>::getSelection() const {
    return m_selection;
}
template <class ProcessThis> void InputFile<ProcessThis>::setNBootstrapWeights(size_t n){
    m_n_bootstrap = n;
}
template <class ProcessThis> size_t InputFile<ProcessThis>::getNBootstrapWeights(){
    return m_n_bootstrap;
}
template <class ProcessThis> void InputFile<ProcessThis>::updateDynamicID(){
    /// the input file class gets a deterministic ID, to make samples reproducible
    ObjectID compositeID(
        {ObjectID(m_fileName+m_treeName), 
        m_selection.getID()
        }
    ); 
    this->ISortableByID::setID(compositeID); 
}

template <class ProcessThis> Sample<ProcessThis>::Sample(){
}  

template <class ProcessThis> Sample<ProcessThis>::Sample(const std::string & thefile, 
                                                         const std::string & treename, 
                                                         Selection<ProcessThis> specificCuts){
    addFile(thefile, treename, specificCuts);
}  
template <class ProcessThis> Sample<ProcessThis>::Sample(std::vector<InputFile<ProcessThis>> files ):
    m_files(files){
        updateDynamicID();
}

template <class ProcessThis> void Sample<ProcessThis>::addFilesFrom(const Sample<ProcessThis> & other){
    auto addUs = other.getInputFiles();
    m_files.insert(m_files.end(), addUs.begin(), addUs.end());
    // update bootstrap weights for all files 
    setNBootstrapWeights(m_n_bootstrap);
    updateDynamicID();
}

template <class ProcessThis> void Sample<ProcessThis>::addFile(InputFile<ProcessThis> addMe){
    addMe.setNBootstrapWeights(m_n_bootstrap); 
    m_files.push_back(addMe);
    updateDynamicID();
}
template <class ProcessThis> void Sample<ProcessThis>::addFile(const std::string & fname, 
                                                               const std::string & treename, 
                                                            Selection<ProcessThis> specificCuts){ 
            addFile(InputFile<ProcessThis>(fname,treename,specificCuts));
            updateDynamicID(); 
}

template <class ProcessThis> std::vector<InputFile<ProcessThis>> Sample<ProcessThis>::getInputFiles() const {
    return m_files;
}


template <class ProcessThis> std::vector<InputFile<ProcessThis>> Sample<ProcessThis>::getSortedInputFiles() const{
    /// make a vector for the size-sorted files 
    std::vector<std::pair <Long64_t, InputFile<ProcessThis> > > FilesWithSizes; 
    /// prepare the output 
    std::vector<InputFile<ProcessThis>> out;
    /// now open each of the files in turn
    std::shared_ptr<TFile> f;
    for (auto & entry : m_files){
        /// for each, get the tree and check the number of entries. Dump info into the vector we booked
        auto tree = entry.getTreeFromFile(f);
        FilesWithSizes.emplace_back(std::make_pair(tree.getEntries(), entry));
    }
    /// now we can sort our files by the number of entries
    std::sort(FilesWithSizes.begin(), 
              FilesWithSizes.end(), 
              [](std::pair <Long64_t, InputFile<ProcessThis>> & a, 
                 std::pair <Long64_t, InputFile<ProcessThis>> & b){
        return a.first > b.first;
    });
    /// insert into output in order of sorting
    for (auto & f : FilesWithSizes){
        out.push_back(f.second);
    }
    /// and return
    return out;
}

template <class ProcessThis>  template <class HistClass> HistClass* Sample<ProcessThis>::getObjectFromFile(const std::string & fname, const std::string & objectName, std::shared_ptr<TFile> & f ) {
    // see if we can keep the existing file
    if (f){ 
        /// if this is not the file we are looking for, close it 
        if (fname != f->GetName()){ 
            f.reset();
        }
    }
    /// if no file open, we open the one we want now 
    if (!f){ 
        f = PlotUtils::Open(fname);
    }
    /// if we failed, we return an error
    if (!f){
        std::cerr << "getObjectFromFile: Could not open "<<fname<<std::endl;
        return nullptr;
    }  
    /// attempt to read our object from the file 
    HistClass* obj = nullptr; 
    f->GetObject(objectName.c_str(),obj);
    /// if we failed, report an error 
    if (!obj){
        std::cerr << "Could not read the object "<<objectName<<" from "<<fname<<std::endl;
        return nullptr;
    }
    /// return what we got 
    return obj;
}

/// return a copy of the input files 
template <class ProcessThis>  std::vector<std::shared_ptr<IInputFile> > Sample<ProcessThis>::getInputFiles(){
    std::vector<std::shared_ptr<IInputFile> > files;
    files.reserve(m_files.size());
    for (auto & f : m_files){
        files.push_back(f.clone());
    }
    return files;
}
template <class ProcessThis>  std::shared_ptr<ISample> Sample<ProcessThis>::clone() const{
    auto newSample = std::make_shared<Sample<ProcessThis> >(*this);
    /// ensure same ID
    newSample->setID(getID()); 
    return newSample; 
}

template <class ProcessThis>  void Sample<ProcessThis>::setNBootstrapWeights(size_t n){
    m_n_bootstrap = n; 
    for (auto & file : m_files){
        file.setNBootstrapWeights(m_n_bootstrap);
    }
}
template <class ProcessThis> size_t Sample<ProcessThis>::getNBootstrapWeights(){
    return m_n_bootstrap;
}
template <class ProcessThis> void Sample<ProcessThis>::updateDynamicID(){
    std::vector<ObjectID> ids; 
    for (auto & f : m_files){
        ids.push_back(f.getID()); 
    }
    this->ISortableByID::setID(ObjectID(ids)); 

}


#endif
