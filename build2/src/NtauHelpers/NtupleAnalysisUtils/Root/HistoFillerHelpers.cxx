#include "NtupleAnalysisUtils/HistoFiller/HistoFillerHelpers.h" 

/// on construction, we populate the "to-do" list with our task list 
 ProcessingQueue::ProcessingQueue(const std::vector<std::shared_ptr<IHistoFillTask>> & taskList){
    m_toDo_remaining.insert(m_toDo_remaining.end(), taskList.begin(), taskList.end());
    /// and update the counter used for progress reporting 
    m_n_files_total = m_toDo_remaining.size();
    /// start the clock for timing 
    m_sw.Start();
 } 

/// This is responsible for popping the next item off our queue
std::shared_ptr<IHistoFillTask> ProcessingQueue::getNext(){
    /// guard the procedure with a mutex 
    std::lock_guard<std::mutex> guard{m_mx_getNext};
    /// if we have nothing left to do, return a nullptr  
    if (m_toDo_remaining.size() == 0) return nullptr; 
    /// get the last remaining task, 
    auto last = m_toDo_remaining.back();
    /// remove it from the vector
    m_toDo_remaining.pop_back();
    /// and return
    return last; 
} 
size_t ProcessingQueue::numberOfTasks() const { return m_n_files_total;}

/// progress reporting. boring. 
void ProcessingQueue::reportFileComplete(){
    ++m_n_files_done; 
}


void ProcessingQueue::reportProgress(Long64_t nFinished){
    m_n_evt_processed+=nFinished; 
    m_n_untilPrintout+=nFinished; 
    if (m_printInterval>0 && (m_n_untilPrintout >= m_printInterval)) printProgress(); 
}

/// Try to validate our input files 
bool ProcessingQueue::performInputValidation(){
    /// this runs on a dedicated timer 
    m_sw.Start();
    std::cout <<"Starting input file validation..."<<std::endl; 
    /// prepare event counters
    m_n_evt_total = 0; 
    Long64_t nevt = 0;
    bool ok=true;
    /// loop over all attached work items 
    for (auto & thing : m_toDo_remaining){
        /// and validate each individually, collecting the number of events
        ok &= (thing->validateInput(nevt)); 
        m_n_evt_total += nevt; 
    }
    /// If at least one failed, report an error
    if (!ok) {
        std::cerr << "Failed to validate all of the input files for the selected samples! Will not run the event loop, please fix this. "<<std::endl;
        return false;
    }   
    // apply the progress printout option if one is set
    if (m_specialInterval != unset){
        switch (m_specialInterval){
            case every1Percent:
                m_printInterval = 0.01 * m_n_evt_total;
                break;
            case every2Percent:
                m_printInterval = 0.02 * m_n_evt_total;
                break;
            case every5Percent:
                m_printInterval = 0.05 * m_n_evt_total;
                break;
            case every10Percent:
                m_printInterval = 0.1 * m_n_evt_total;
                break;
            case every25Percent: 
                m_printInterval = 0.25 * m_n_evt_total; 
                break;
            case every50Percent: 
                m_printInterval = 0.5 * m_n_evt_total; 
                break;
            case noPrintout:
                m_printInterval = -1;
                break;
            default:
                m_printInterval = -1;
                break;
        }
    }
    /// stop the timer and report a successful outcome
    m_sw.Stop(); 
    std::cout <<"Finished input validation in "<<m_sw.CpuTime()<<" seconds"<<std::endl;
    // now propagate the reporting intervals to the workers
    for (auto & task : m_toDo_remaining){
        task->setReportingInterval(0.2 * m_printInterval); 
    }
    /// and restart the clock for timing the event loop
    m_sw.Reset();
    m_sw.Start();
    return ok;
}

/// this method will print the progress after every <interval> events
void ProcessingQueue::setPrintInterval(int interval){m_printInterval = interval;}
/// this method will print the progress after an interval determined by the definitions 
/// of specialPrintInterval (every X percent or not at all)
void ProcessingQueue::setPrintInterval(specialPrintInterval interval){m_specialInterval = interval;}

void ProcessingQueue::printProgress(){

    m_n_untilPrintout = 0; 
    std::lock_guard<std::mutex> guard{m_mx_regDone}; 
    double tr = m_sw.RealTime();
    double relProgress = 100.*(double)m_n_evt_processed / (double)m_n_evt_total;
    double eta = (100.-relProgress) / relProgress * tr; 
   
    auto time_to_str = [](double time) -> std::string{
        std::string time_str{};
        if (time > 3600.){
            int h = (int)time/(int)3600;
            int m = ((int)(time - h * 3600))/(int)60; 
            int s = std::round(time - h * 3600 - m * 60); 
            time_str = std::to_string(h)+":"+(m < 10 ? "0" : "")+std::to_string(m)+":"+(s < 10 ? "0" : "")+std::to_string(s)+" h"; 
        } else if (time > 60){
            int m = ((int)(time ))/(int)60; 
            int s = std::round(time - m * 60); 
            time_str = std::to_string(m)+":"+(s < 10 ? "0" : "")+std::to_string(s)+" min"; 
        } else {
            time_str = std::to_string((int)(std::round( time)))+" s";
        }
        return time_str;
    };
    
    std::string etaWithUnit = time_to_str(eta);
    std::string elapsedWithUnit = time_to_str(tr); 
   
   
    std::string rateString; 
    double rate = (double)m_n_evt_processed / tr; 
    if (rate > 1e6){
        rate /= 1.e6; 
        rateString = Form("%.2f MHz", rate); 
    }
    else if (rate > 1e3){
        rate /= 1.e3; 
        rateString = Form("%.2f kHz", rate); 
    }
    else {
        rateString = Form("%.2f Hz", rate); 
    }
    std::cout <<">> "<<m_n_evt_processed<<" / "<<m_n_evt_total<<" Events ("<<Form("%.2f",relProgress)<<"%) and "<<m_n_files_done<<"/"<<m_n_files_total
              <<" Files done after "<<elapsedWithUnit<<" processing time. "<< std::setw(8)<< rateString <<" - E.T.A "<<etaWithUnit<<"                 " << std::flush;
    std::cout << '\r' << std::flush;  // make sure any other couts are properly displayed

    m_sw.Continue(); 

}


void ProcessingQueue::print(const std::string & spacer){
    std::cout << spacer <<" --> Processing queue with "<<m_toDo_remaining.size()<<" work items:"<<std::endl;
    for (auto & todo: m_toDo_remaining){
        todo->print(spacer); 
    }
}