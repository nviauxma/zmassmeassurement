#!/usr/bin/python
import os, argparse, logging
from ClusterSubmission.Utils import CheckConfigPaths, id_generator, WriteList, AppendToList, ReadListFromFile, IsROOTFile, setup_engine, setupBatchSubmitArgParser
from random import shuffle
from distutils.spawn import find_executable


class ClusterAnalysisSubmit(object):
    def __init__(
        self,
        cluster_engine=None,
        ### Input configuration file
        input_cfgs=["/afs/cern.ch/user/n/nviauxma/ZmassCalibration3/build/x86_64-centos7-gcc8-opt/data/MuonSagittaBias/FileLists/CERN/Jpsimu4mu4.conf"],
        ### Sagitta injection configuration file
        sagitta_inject="/afs/cern.ch/user/n/nviauxma/ZmassCalibration3/build/x86_64-centos7-gcc8-opt/data/MuonSagittaBias/sigma_injection.txt",
        ### How many injections shall be processed per job
        n_inject_per_job=-1,
        ### List of jobs on which the main job shall wait
        hold_jobs=[],
        ### Number of input files per job
        nFilesPerJob=-1,
        ### Run time of the analysis jobs
        runTime='24:00:00',
        ### Aquired memory by the analysis jobs
        runMem=1400,
        ### Executable to run
        executable="PlotPseudoMass",
        ### How many temproray files shall be handled by the first
        ### merge job
        files_per_merge=2,
        ### Not yet supported
        submit_meta_jobs=False,
    ):

        self.__cluster_engine = cluster_engine

        self.__job_file = id_generator(20)
        self.__injection_files = self.__split_list(list_entries=ReadListFromFile(sagitta_inject), n_split=n_inject_per_job)
        self.__exec = executable

        self.__sheduled_jobs = 0
        #### Splitting variables
        self.__n_filesPerJob = nFilesPerJob

        ### Meta data
        self.__submit_meta_jobs = False and submit_meta_jobs
        self.__meta_cfgs = []

        ### Hold jobs
        self.__hold_jobs = [H for H in hold_jobs]
        ### Merging
        self.__merge_interfaces = []
        self.__files_per_merge_itr = files_per_merge
        ### Memory & run time
        self.__run_time = runTime
        self.__vmem = runMem
        self.__write_job_cfgs(in_cfgs=input_cfgs)

    ### Splits the content
    def __split_list(self, list_entries=[], n_split=10, ending="txt"):
        if len(list_entries) == 0: return []
        ### Define the split according to the number of jobs per file
        begin = 0
        n_entries = len(list_entries)
        end = n_entries if n_split < 0 or n_entries < n_split else n_split

        out_lists = []
        while end <= n_entries:
            out_lists += [WriteList(list_entries[begin:end], "%s/%s.%s" % (self.engine().config_dir(), id_generator(40), ending))]
            begin = end
            if n_split < 0: end += 1
            elif end + n_split <= n_entries: end += n_split
            elif end < n_entries: end = n_entries
            else: end += 1
        return out_lists

    ### This method writes the job configuration to launch histo jobs
    def __write_job_cfgs(self, in_cfgs=[]):
        if not self.engine().submit_hook(): return False
        if len(in_cfgs) == 0:
            logging.error("No valid input configs were given")
            return False

        # sort input configs to submit data first, then sorted MC periods
        #in_cfgs.sort(cmp=lambda x, y: InputCfgSorter(x, y))

        #### Next step is to create a configuration list
        for cfg in in_cfgs:
            if not self.__assemble_job_cfg(cfg): return False
        return True

    def __assemble_job_cfg(self, incfg):
        import ROOT
        ### Read the ROOT files.
        root_files = ReadListFromFile(incfg)
        n_files = len(root_files)
        if len(root_files) == 0:
            logging.error("%s does not seem to contain any ROOT file" % (incfg))
            return False
        linked_in = self.engine().link_to_copy_area(incfg)

        logging.info("<assemble_job_cfg>: Assemble the job for %s" % (incfg))

        if self.__submit_meta_jobs:
            meta_file = "%s/%s.root" % (self.engine().tmp_dir(), id_generator(44))
            self.__meta_cfgs += [(meta_file, linked_in)]
            #AppendToList(["MetaInput %s" % (meta_file)], linked_in)

        ### Define the split according to the number of jobs per file

        begin = 0
        end = n_files if self.__n_filesPerJob < 0 or n_files < self.__n_filesPerJob else self.__n_filesPerJob

        ### Define how the input is split according to the number
        ### of files per job
        in_file_split = []
        while end <= n_files:
            in_file_split += [" --begin %d --end %d " % (begin, end)]
            begin = end
            if self.__n_filesPerJob < 0: end += 1
            elif end + self.__n_filesPerJob <= n_files: end += self.__n_filesPerJob
            elif end < n_files: end = n_files
            else: end += 1

        ##
        ### Injection split
        ##
        inject_opts = [" --injectionConfig %s " % (inj) for inj in self.injection_cfgs()]
        extra_opts = []
        if len(inject_opts) == 0:
            extra_opts = in_file_split
        else:
            for inj in inject_opts:
                for roo_file in in_file_split:
                    extra_opts += [inj + roo_file]
        ### Now every job has its own configuration in terms of injection files
        ### and also in terms of input range
        num_opts = len(extra_opts)
        #### That is the number of files we must merge in the end
        out_name = incfg[incfg.rfind("/") + 1:incfg.rfind(".")]

        ### First write the lists to the disk
        ### Append everything to the end in order to preserve the dependency
        ### of previously configured jobs in the chain
        AppendToList([linked_in for _ in range(num_opts)], self.input_job_cfg())
        AppendToList(extra_opts, self.run_job_cfg())

        files_to_merge = ["%s/%s_%d.root" % (self.engine().tmp_dir(), out_name, i) for i in range(num_opts)]

        #### Merge jobs are submitted in the old way, however we must tell on which chain in the array
        #### It should depend. Otherwise users will just wait forever until all analysis jobs are done.
        #### There is no guarantee that the jobs will all succeed in the end. So we should make the
        #### output files as soon as possible
        AppendToList(files_to_merge, self.tmp_file_name_cfg())
        #### Submit the merge jobs
        self.__merge_interfaces += [
            self.engine().create_merge_interface(out_name=out_name,
                                                 files_to_merge=files_to_merge,
                                                 hold_jobs=[(self.job_name(), [self.__sheduled_jobs + i + 1 for i in range(num_opts)])],
                                                 files_per_job=self.__files_per_merge_itr)
        ]
        self.__sheduled_jobs += num_opts
        return True

    def engine(self):
        return self.__cluster_engine

    def job_name(self):
        return self.engine().job_name()

    def injection_cfgs(self):
        return self.__injection_files

    def executable(self):
        return self.__exec

    ### Location where the input job cfg is stored
    def input_job_cfg(self):
        return "%s/InCfgs_%s.conf" % (self.engine().config_dir(), self.__job_file)

    ### Location where the temporary files are stored
    def tmp_file_name_cfg(self):
        return "%s/out_fileNames_%s.conf" % (self.engine().config_dir(), self.__job_file)

    ### Location where the run job cfg is stored
    def run_job_cfg(self):
        return "%s/JobCfg_%s.conf" % (self.engine().config_dir(), self.__job_file)

    def common_meta_data(self):
        return self.__submit_meta_jobs

    def n_sheduled(self):
        return self.__sheduled_jobs

    def run_time(self):
        return self.__run_time

    def memory(self):
        return self.__vmem

    # def submit_meta_jobs(self):
    #     if self.common_meta_data():
    #         meta_exec_list = "%s/%s" % (self.engine().config_dir(), id_generator(32))
    #         WriteList(["TemporaryTPMetaTree -o %s -i %s" % (meta_file, in_cfg) for meta_file, in_cfg in self.__meta_cfgs], meta_exec_list)
    #         if not self.engine().submit_array(script="ClusterSubmission/Run.sh",
    #                                           sub_job="MetaData",
    #                                           mem=750,
    #                                           run_time="01:59:59",
    #                                           hold_jobs=self.hold_jobs(),
    #                                           env_vars=[("ListOfCmds", meta_exec_list)],
    #                                           array_size=len(self.__meta_cfgs)):
    #             return False
    #     return True

    def hold_jobs(self):
        return self.__hold_jobs

    def submit(self):
        if self.n_sheduled() == 0:
            logging.error("<submit> -- Nothing has been scheduled")
            return False
        if not self.engine().submit_build_job(): return False
        ### if not self.submit_meta_jobs(): return False
        ### Submit the meta data jobs
        if not self.engine().submit_array(
                script="MuonSagittaBias/BatchAnalysis.sh",
                mem=self.memory(),
                run_time=self.run_time(),
                hold_jobs=self.hold_jobs() if not self.__submit_meta_jobs else [self.engine().subjob_name("MetaData")],
                env_vars=[
                    ("JobCfg", self.run_job_cfg()),
                    ("OutCfg", self.tmp_file_name_cfg()),
                    ("InCfg", self.input_job_cfg()),
                    ("Exec", self.executable()),
                ],
                array_size=self.n_sheduled()):
            return False
        to_hold = []
        for merge in self.__merge_interfaces:
            if not merge.submit_job(): return False
            to_hold += [self.engine().subjob_name("merge-%s" % (merge.outFileName()))]
        return self.engine().submit_clean_all(hold_jobs=to_hold)


def setupAnalysisParser():
    parser = setupBatchSubmitArgParser()
    parser.set_defaults(Merge_vmem=4000)
    parser.set_defaults(MergeTime="03:59:59")
    parser.add_argument('--inputConf', '-I', nargs="+", default=[], help="Input files required to run", required=True)
    parser.add_argument('--Exec', help="Which executable must be run", required=True)

    parser.add_argument('--sagittaInjectionFile', '-sI', help="Configuration file used for the sagitta injections ", default='')
    parser.add_argument('--nInjectionPerJob',
                        type=int,
                        default=10,
                        help="Defines how many injections from the injection file are handled by one batch job")

    parser.add_argument('--HoldJob', default=[], nargs="+", help='Specify job names which should be finished before your job is starting. ')
    parser.add_argument('--RunTime', help='Changes the RunTime of the analysis Jobs', default='07:59:59')
    parser.add_argument('--vmem', help='Changes the virtual memory needed by each jobs', type=int, default=3000)
    parser.add_argument('--nFilesPerJob', help="How many input files are processed per job", type=int, default=10)
    parser.add_argument("--FilesPerMerge", help="How many files per merge iteration should be taken", type=int, default=20)

    parser.add_argument("--noCommonMetaData",
                        help="Disable that a temporary meta data tree is written before the analysis jobs start",
                        action='store_false',
                        default=True)
    parser.add_argument("--SpareWhatsProcessedIn",
                        help="If the cluster decided to die during production, you can skip the processed files in the directories",
                        default=[],
                        nargs="+")

    return parser


if __name__ == '__main__':
    RunOptions = setupAnalysisParser().parse_args()
    Spared_Configs = []
    #### The previous round of cluster screwed up. But had some results. There is no
    #### reason to reprocess them. So successful files are not submitted twice
    if len(RunOptions.SpareWhatsProcessedIn) > 0:
        logging.info("Cluster did not perform so well last time? This little.. buttefingered..")
        for dirToSpare in RunOptions.SpareWhatsProcessedIn:
            if not os.path.isdir(dirToSpare):
                logging.error("I need a directory to look up %s" % (dirToSpare))
                exit(1)
            for finished in os.listdir(dirToSpare):
                if not IsROOTFile(finished): continue
                logging.info("Yeah... %s has already beeen processed. Let's skip it.." % (finished))
                Spared_Configs.append(finished[:finished.rfind(".root")])

    cluster_engine = setup_engine(RunOptions)
    input_cfgs = [C for C in CheckConfigPaths(RunOptions.inputConf) if C[C.rfind("/") + 1:C.rfind(".")] not in Spared_Configs]
    shuffle(input_cfgs)

    if not find_executable(RunOptions.Exec):
        logging.error("The executable %s to produce historgams is not installed." % (RunOptions.Exec))
        exit(1)

    submit_job = ClusterAnalysisSubmit(
        cluster_engine=cluster_engine,
        input_cfgs=input_cfgs,
        executable=RunOptions.Exec,
        ### Number of run configs per array
        nFilesPerJob=RunOptions.nFilesPerJob,
        runTime=RunOptions.RunTime,
        runMem=RunOptions.vmem,
        ###
        sagitta_inject=RunOptions.sagittaInjectionFile,
        ### How many injections shall be processed per job
        n_inject_per_job=RunOptions.nInjectionPerJob,
        #submit_meta_jobs=RunOptions.noCommonMetaData,
        files_per_merge=RunOptions.FilesPerMerge,
        hold_jobs=RunOptions.HoldJob)

    submit_job.submit()
    cluster_engine.finish()
