#ifndef NTAU__AXISCONFIG__H
#define NTAU__AXISCONFIG__H

#include "NtupleAnalysisUtils/Configuration/UserSetting.h"

/// The AxisConfig class is a helper to hold configuration of a 
/// frame axis. It can set the range to draw (dynamically or fixed), 
/// the title and linear versus logarithmic drawing. 

/// Used within the CanvasOptions. 

class AxisConfig{
    public:
    AxisConfig()=default; 
    AxisConfig(const AxisConfig & other); 
    void operator= (const AxisConfig & other); 

    /// This either represents a *lower* bound on the minimum of a 
    /// dynamic range, or the hard-coded minimum value 
    UserSetting<double,AxisConfig> Min{-std::numeric_limits<double>::max(),this}; 
    /// This either represents an *upper* bound on the minimum of a 
    /// dynamic range, or the hard-coded minimum value 
    UserSetting<double,AxisConfig> Max{std::numeric_limits<double>::max(),this}; 
    /// This enables a symmetrisation around a common point
    UserSetting<bool,AxisConfig> Symmetric {false,this}; 
    /// This sets the desired symmetrisation point
    UserSetting<double,AxisConfig> SymmetrisationPoint{1.,this}; 
    /// This enabled aligning a symmetrised axis to a few "good-looking" predetermined values
    UserSetting<bool,AxisConfig> Aligned   {false,this}; 
    /// This replaces the dynamic range completely by a fixed Min...Max interval
    UserSetting<bool,AxisConfig> Fixed     {false,this}; 
    /// This enables a logarithmic axis
    UserSetting<bool,AxisConfig> Log     {false,this}; 
    /// This adds a an additional fraction of the dynamic range on top / bottom 
    /// of the dynamically determined intervals as "padding", for a nicer 
    /// visual appearance
    UserSetting<double,AxisConfig> TopPadding     {0.1,this}; 
    UserSetting<double,AxisConfig> BottomPadding     {0.05,this}; 
    /// This is the desired axis title. 
    /// Will override the histogram title if set. 
    UserSetting<std::string,AxisConfig> Title     {"",this}; 
    /// This can be used to specify an extra title offset, if the 
    /// title is too close to the axis.
    UserSetting<double,AxisConfig> ExtraTitleOffset     {0,this}; 
}; 

#endif //NTAU__AXISCONFIG__H