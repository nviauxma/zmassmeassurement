
#ifndef BOOTSTRAPPOISSONHELPER_H
#define BOOTSTRAPPOISSONHELPER_H

#include "TRandom3.h"
#include <vector> 

/// A helper to avoid having to resort to the TRandom3::Poisson
/// Provides support for drawing Poisson(1) distributed random numbers 
/// from the TRandom3::Uniform.
/// 
/// Optimized for fast generation of large numbers of random numbers 
/// in one go, as required for example in the bootstrap method.
/// 
/// If you just need a single poisson per call, TMath::Poisson may
/// be faster
class BootstrapPoissonHelper{
public:
    BootstrapPoissonHelper();

    /// This allows to re-seed the random number generation
    void reseed(ULong_t seed);

    /// This is the main method for the user to call.
    /// It will populate the passed vector with random weights,
    /// each of them poisson-distributed with a mean value of 1. 
    /// The size of the input vector determines the number of weights to generate. 
    void populateRandom(std::vector<double>& fillMe);
protected:
    TRandom3 m_random3;

};

#endif
