#include "NtupleAnalysisUtils/Helpers/BinningHelpers.h" 
#include <iostream>
#include <cmath>

std::vector<double> PlotUtils::getLogLinearBinning(double start, double end, int nbins){
    std::vector<double> out;
    if (start <=0 || end <=0){
        std::cerr << "CAREFUL: Negative start ("<<start<<") or end ("<<end<<") passed to getLogLinearBinning. Returning empty vector"<<std::endl;
        return out;
    }
    if (nbins <= 0){
        std::cerr<< "CAREFUL: Invalid number of bins ("<<nbins<<") passed to getLogLinearBinning. Returning empty vector"<<std::endl; 
        return out;
    }
    double logstart = std::log(start);
    double logend = std::log(end); 
    double logstep = (logend - logstart)/(double)nbins;
    for (int i = 0; i <= nbins; ++i) {
        out.push_back(std::exp(logstart + logstep * i));
    }
    return out;
}
std::vector<double> PlotUtils::getLinearBinning(double start, double end, int nbins){
    std::vector<double> out;
    if (start  >= end ){
        std::cerr << "CAREFUL: start ("<<start<<") is larger than  or equal to end ("<<end<<") passed to getLinearBinning. Returning empty vector"<<std::endl;
        return out;
    }
    if (nbins <= 0){
        std::cerr<< "CAREFUL: Invalid number of bins ("<<nbins<<") passed to getLogLinearBinning. Returning empty vector"<<std::endl; 
        return out;
    }
    double step = (end - start)/(double)nbins;
    for (int i = 0; i <= nbins; ++i) {
        out.push_back(start + i *step);
    }
    return out;
}

