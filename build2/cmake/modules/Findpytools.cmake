# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#
# Sets:
#  PYTOOLS_BINARY_PATH
#  PYTOOLS_PYTHON_PATH
#
# Can be steered by PYTOOLS_LCGROOT.
#

# The LCG include(s).
include( LCGFunctions )

# Find dependent packages. Which are allowed to be missing.
find_package( chardet )
find_package( idna )
find_package( urllib3 )

# Find it.
lcg_python_external_module( NAME pytools
   PYTHON_NAMES yaml/__init__.py simplejson/__init__.py decorator.py
   BINARY_NAMES ipython jupyter
   BINARY_SUFFIXES bin )

# Handle the standard find_package arguments.
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( pytools DEFAULT_MSG
   _PYTOOLS_PYTHON_PATH _PYTOOLS_BINARY_PATH )

# Set up the RPM dependency.
lcg_need_rpm( pytools )
