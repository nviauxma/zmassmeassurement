#include <MuonSagittaBias/DataTemplateHolder.h>
#include <NtauHelpers/HistoUtils.h>
#include <TString.h>
double DataTemplateHolder::s_max_chi2 = 20;
double DataTemplateHolder::s_int_lumi = 1.;

void DataTemplateHolder::set_lumi(double lumi) { s_int_lumi = lumi; }

DataTemplateHolder::DataTemplateHolder(const std::shared_ptr<TFile>& in_file, const std::string& data_path) {
    m_file = in_file;
    m_data_path = data_path;
    m_template = load_histo(data_path, in_file);

    std::vector<std::string> path_tokens = tokenize(data_path, "/");
    m_var_name = path_tokens[1];
    m_prim_sel = path_tokens[2];
    extract_kinematic_bin(path_tokens[3]);
    extract_mass_bin(path_tokens[4]);
    /// Extract the sagitta bias  and resolution
    const std::string template_name = path_tokens[path_tokens.size() - 1];
    m_calib = MuonTrk::calib_constants{template_name};
    m_calib.trk_type = MuonTrk::to_type(path_tokens[0]);

    /// Now extract the file name without absolute paths and .root ending
    std::string tmp_file_name = m_file->GetName();
    m_file_name = tmp_file_name.substr(tmp_file_name.rfind("/") + 1, tmp_file_name.rfind("."));

    m_nDoF = getNbinsVis(get());
}
void DataTemplateHolder::extract_kinematic_bin(const std::string& sel_str) {
    if (sel_str.find("KinSel") != 0) return;
    /// The string contains either
    ///      -  KineSel_VariableX_inRANGE_<low>_<high>
    ///      -  KineSel_Inclusive
    ///      -  KineSel_GlobalBin_  <--- should ideally not  happen
    /// Do nothing if the primary kinematic bin inclusive
    if (sel_str == "KineSel_Inclusive") return;
    /// This should give us the dimensions
    std::vector<std::string> border_tokens = tokenize(sel_str.substr(sel_str.find("_") + 1, std::string::npos), "__");
    /// Now loop over each token and let's see what happens
    for (const std::string& bin_str : border_tokens) {
        std::vector<std::string> bin_token = tokenize(bin_str, "_");
        if (bin_token.size() == 2)  // We're at the GlobalBin_blah stage
        {
            const float bin_number = atof(bin_token[1].c_str());
            m_kine_sel.emplace_back("", bin_number, bin_number);
        } else {
            const float low_cut = atof(bin_token[bin_token.size() - 2].c_str());
            const float high_cut = atof(bin_token[bin_token.size() - 1].c_str());
            m_kine_sel.emplace_back(bin_token[0], low_cut, high_cut);
        }
    }
    std::sort(m_kine_sel.begin(), m_kine_sel.end());
}
void DataTemplateHolder::extract_mass_bin(const std::string& sel_str) {
    if (sel_str.find("MLL") != 0) return;
    std::vector<std::string> mass_tokens = tokenize(sel_str, "_");
    if (mass_tokens.size() != 3) return;
    m_mass_window = std ::make_pair(atof(mass_tokens[1].c_str()), atof(mass_tokens[2].c_str()));
}
float DataTemplateHolder::mll_low() const { return m_mass_window.first; }
float DataTemplateHolder::mll_high() const { return m_mass_window.second; }
std::string DataTemplateHolder::template_path() const { return m_data_path.substr(0, m_data_path.rfind("/") + 1); }
std::string DataTemplateHolder::primary_selection() const { return m_var_name; }
std::shared_ptr<TH1> DataTemplateHolder::get() const { return m_template; }
int DataTemplateHolder::trk_type() const { return m_calib.trk_type; }
float DataTemplateHolder::inject_bias() const { return m_calib.sagitta_bias; }
float DataTemplateHolder::inject_resolution() const { return m_calib.res_const; }
std::string DataTemplateHolder::variable_name() const { return m_var_name; }
std::string DataTemplateHolder::file_name() const { return m_file_name; }
DataTemplateHolder::Chi2TestResult DataTemplateHolder::chi2(const DataTemplateHolder& mc_template) const {
    return chi2(mc_template, get());
}
DataTemplateHolder::Chi2TestResult DataTemplateHolder::chi2(const DataTemplateHolder& mc_template,
                                                            const std::shared_ptr<TH1>& data_template) const {
    Chi2TestResult to_ret;
    if (!same_binning(data_template, mc_template.get())) {
        std::cout << "WARNING - Different binnings detected" << std::endl;
        to_ret.chi2 = DBL_MAX;
        return to_ret;
    }
    to_ret.residuals.resize(nDoF());
    int igood{0};
    to_ret.p_val = data_template->Chi2TestX(mc_template.get().get(), to_ret.chi2, to_ret.ndf, igood, "UW", to_ret.residuals.data());
    return to_ret;
}
double DataTemplateHolder::ReducedChi2(const DataTemplateHolder& mc_template, const std::shared_ptr<TH1>& data_template) const {
    Chi2TestResult to_ret = chi2(mc_template, data_template);
    return to_ret.chi2 / to_ret.ndf;
}
double DataTemplateHolder::ReducedChi2(const DataTemplateHolder& mc_template) const { return ReducedChi2(mc_template, get()); }
unsigned int DataTemplateHolder::nDoF() const { return m_nDoF; }
double DataTemplateHolder::GetBinContent(int bin) const { return get()->GetBinContent(bin); }
double DataTemplateHolder::GetBinError(int bin) const { return get()->GetBinError(bin); }

bool DataTemplateHolder::operator<(const DataTemplateHolder& other) const {
    //// First sort by sample
    if (m_file != other.m_file) return file_name() < other.file_name();
    if (variable_name() != other.variable_name()) return variable_name() < other.variable_name();
    return m_calib < other.m_calib;
}
std::shared_ptr<TGraph> DataTemplateHolder::extract_chi2(const std::vector<std::shared_ptr<DataTemplateHolder>>& mc_templates) const {
    // Find the boundaries of the biases
    std::shared_ptr<TGraph> chi2_graph = std::make_shared<TGraph>();
    chi2_graph->SetName(PlotUtils::RandomString(24).c_str());
    /// Convert the template to a data one with integer counts
    std::shared_ptr<TH1> data_template = clone(get());
    for (unsigned int data_bin = 0; data_bin <= PlotUtils::getNbins(data_template.get()); ++data_bin) {
        data_template->SetBinContent(data_bin, std::floor(s_int_lumi * data_template->GetBinContent(data_bin)));
        data_template->SetBinError(data_bin, std::sqrt(data_template->GetBinContent(data_bin)));
    }
    for (const std::shared_ptr<DataTemplateHolder>& holder : mc_templates) {
        if (!is_compatible(*holder)) continue;
        double chi2 = ReducedChi2(*holder, data_template);
        if (chi2 > s_max_chi2 || chi2 == 0.) continue;
        chi2_graph->SetPoint(chi2_graph->GetN(), holder->inject_bias() * 1.e3, chi2);
    }
    chi2_graph->GetXaxis()->SetTitle("#deltas[TeV^{-1}]");
    chi2_graph->GetYaxis()->SetTitle("#chi^{2} / n.d.f");
    return chi2_graph;
}
bool DataTemplateHolder::is_compatible(const DataTemplateHolder& other) const {
    if (other.m_calib == m_calib) return false;
    if (other.trk_type() != trk_type()) return false;
    if (other.mll_low() != mll_low()) return false;
    if (other.mll_high() != mll_high()) return false;
    if (variable_name() != other.variable_name()) return false;
    if (primary_selection() != other.primary_selection()) return false;
    if (m_kine_sel != other.m_kine_sel) return false;
    return true;
}
std::vector<std::string> DataTemplateHolder::to_title() const {
    std::vector<std::string> leg_entry{
        m_calib.to_title(),
        Form("%.0f< m_{#mu#mu} < %.0f", mll_low(), mll_high()),
    };
    for (const KinematicBin& b : m_kine_sel) { leg_entry += b.to_title(); }
    return leg_entry;
}
///###################################################
///                 KinematicBin
///###################################################

DataTemplateHolder::KinematicBin::KinematicBin(const std::string& _var, float _low, float _high) {
    var_name = _var;
    low_cut = _low;
    high_cut = _high;
}
bool DataTemplateHolder::KinematicBin::operator==(const KinematicBin& other) const {
    if (other.low_cut != low_cut) return false;
    if (other.high_cut != high_cut) return false;
    if (other.var_name != var_name) return false;
    return true;
}
bool DataTemplateHolder::KinematicBin::operator<(const KinematicBin& other) const {
    if (other.var_name != var_name) return var_name < other.var_name;
    if (other.low_cut != low_cut) return low_cut < other.low_cut;
    return high_cut < other.high_cut;
}
bool DataTemplateHolder::KinematicBin::operator!=(const KinematicBin& other) const { return !(*this == other); }

std::string DataTemplateHolder::KinematicBin::to_title() const { return Form("%s#in[%.2f;%2.f]", var_name.c_str(), low_cut, high_cut); }
