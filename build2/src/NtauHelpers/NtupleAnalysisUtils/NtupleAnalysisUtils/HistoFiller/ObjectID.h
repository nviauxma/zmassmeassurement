#ifndef NTAU__OBJID__H
#define NTAU__OBJID__H 

#include  <memory>
#include  <vector>
#include  <atomic>
#include  <iostream>

/// This header defines some helpers to obtain unique identifiers for the building blocks 
/// of our plots, used to check if two plots are equivalent in terms of filling. 

class ObjectID{
public:

    typedef long long int ID_t;  

    /// construct an identifier from an object name.
    ObjectID(const std::string & name);
    /// constructs an identifier as a fixed number
    ObjectID(ID_t ID);

    /// generate a composite ID from several components. 
    /// This will compare to different-sized IDs based on length,
    /// and to same sized IDs based on the first no-equal element. 
    ObjectID(std::vector<ObjectID> ids);

    static ObjectID nextID(); 

    /// comparison operators. 
    bool operator< (const ObjectID & other) const;
    bool operator==(const ObjectID & other) const;
    bool operator> (const ObjectID & other) const;

    /// for printout 
    const std::vector<ID_t> & allIDs() const;
private: 
    std::vector<ID_t> m_theID{}; 
};

/// printout support
std::ostream & operator<< (std::ostream& stream, const ObjectID & theID);

#endif // NTAU_OBJID__H.