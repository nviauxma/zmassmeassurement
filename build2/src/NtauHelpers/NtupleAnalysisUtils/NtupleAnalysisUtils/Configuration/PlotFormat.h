#ifndef NTAU__PLOTFORMAT__H
#define NTAU__PLOTFORMAT__H 

#include "TColor.h"
#include "TAttLine.h"
#include "TAttFill.h"
#include "TAttMarker.h"
#include "TStyle.h"
#include <string> 
#include <map> 
#include <vector>
#include "NtupleAnalysisUtils/Configuration/UserSetting.h"


/// class to package the desired formatting of a ROOT object (typically, a histogram). 
/// will then be auto-applied in one go if desired. 
/// Designed to easily chain format instructions, for example, 
/// PlotFormat().Color(kBlue).MarkerStyle(kFullDotLarge).applyTo(myHistogram);

class PlotFormat{
public:

    /// C-tor starts with an empty format (no user-settings)
    PlotFormat();

    PlotFormat(const PlotFormat & other); 
    void operator=(const PlotFormat & other);  

    PlotFormat Copy(){return PlotFormat(*this);}    // copy method, for easily obtaining a modified copy  

    /// format a given ROOT object according to this object
    /// Please see below for the supported settings. 
    template <class Histo> void applyTo(Histo* H) const;

    ///===================================================
    /// The following is the set of NTAU-supported settings 
    /// These are used for auto-formating the object when drawing
    /// and for creating legends etc. 
    ///
    /// Please see the UserSetting.h header for documentation
    /// on their usage.  
    ///===================================================

    /// A global color setting - overriden by the specific ones below 
    /// if those are also set 
    UserSetting<Color_t, PlotFormat> Color{gStyle->GetMarkerColor(),this}; 

    /// Specific settings for fill, marker and line colors. Each can be combined with an alpha value below
    UserSetting<Color_t, PlotFormat> FillColor{gStyle->GetFillColor(),this};
    UserSetting<Color_t, PlotFormat> MarkerColor{gStyle->GetMarkerColor(), this};
    UserSetting<Color_t, PlotFormat> LineColor{gStyle->GetLineColor(), this};

    /// Alpha settings for fill,marker,line
    UserSetting<double, PlotFormat> FillAlpha{1.,this};
    UserSetting<double, PlotFormat> MarkerAlpha{1.,this};
    UserSetting<double, PlotFormat> LineAlpha{1.,this};

    /// Style for fill,marker,line
    UserSetting<Style_t, PlotFormat> FillStyle{0,this};
    UserSetting<Style_t, PlotFormat> MarkerStyle{gStyle->GetMarkerStyle(),this};
    UserSetting<Style_t, PlotFormat> LineStyle{gStyle->GetLineStyle(), this};

    /// Size for marker and width for line. 
    /// For the marker, we can also apply a relative scaling
    UserSetting<Size_t, PlotFormat> MarkerSize{gStyle->GetMarkerSize(),this};
    UserSetting<double, PlotFormat> MarkerScale{1.,this};
    UserSetting<Width_t, PlotFormat> LineWidth{gStyle->GetLineWidth(), this};

    /// Additional parameters to pass to the Draw() method
    UserSetting<std::string, PlotFormat> ExtraDrawOpts{"",this};
    /// Legend settings - draw mode and title to use
    UserSetting<std::string, PlotFormat> LegendOption{"PL",this};
    UserSetting<std::string, PlotFormat> LegendTitle{"",this};

    /// ============================================
    /// These are holders for any custom information the user may 
    /// wish to attach to the format object. 
    /// This will not be used by NtupleAnalysisUtils directly,
    /// but can be useful to package information 
    /// for user-code in the same 
    /// fashion as xAOD decorations would. 
    /// ============================================
    CustomUserSettings<int, PlotFormat> CustomInt{this}; 
    CustomUserSettings<double, PlotFormat> CustomFloat{this}; 
    CustomUserSettings<std::string, PlotFormat> CustomString{this}; 

    /// does this format have any user-specified settings? 
    bool hasUserSettings() const {return m_hasUserSettings;}
    void setHasUserSettings(){m_hasUserSettings=true;}


private:

    // check if anything was set by the user
    bool m_hasUserSettings; 
};


// here, we define a few premade plot formats that the user can work with 
namespace PremadePlotFormats{
    PlotFormat DataMarkers();

    PlotFormat Markers1();
    PlotFormat Markers2();
    PlotFormat Markers3();
    PlotFormat Markers4();
    PlotFormat Markers5();
    PlotFormat Markers6();

    PlotFormat Lines1();
    PlotFormat Lines2();
    PlotFormat Lines3();
    PlotFormat Lines4();
    PlotFormat Lines5();
    PlotFormat Lines6();

}


#include "PlotFormat.ixx"

#endif
