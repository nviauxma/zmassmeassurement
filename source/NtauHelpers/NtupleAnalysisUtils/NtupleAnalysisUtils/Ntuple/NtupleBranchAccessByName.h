#ifndef NTAU__NTUPLEBRANCH__ACCESS_BY_NAME__H
#define NTAU__NTUPLEBRANCH__ACCESS_BY_NAME__H

#include "NtupleAnalysisUtils/Ntuple/NtupleBranch.h" 
#include "NtupleAnalysisUtils/Ntuple/NtupleBranchMgr.h" 
#include <mutex>
#include "TFile.h"

/// This class provides a mechanism of accessing a branch from a manager
/// without using an explicit NtupleBranch member. 
/// 
/// The NtupleBranchAccessByName works with the autodetection
/// feature of the NtupleBranchMgr, so it is able 
/// to read branches that have not been explicitly 
/// declared as members of the manager-derived class. 
/// 
/// The intended use case is the reading from ntuples
/// for which no dedicate class has been generated (for 
/// example due to singular use). 
/// 
/// Performance is optimised via a look-up table across input
/// trees. This is moderately fast, but for optimal performance 
/// dedicated NtupleBranch instances are still faster (save 
/// a map lookup). 
///
/// One NtupleBranchAccessByName can be used with several 
/// NtupleBranchMgr instances (trees) simultaneously. 
template <typename branchType> class NtupleBranchAccessByName{
public:
    /// construct by giving the name of the branch. 
    NtupleBranchAccessByName(const std::string & branchName);
    /// copy c-tor and assignment operators - need to be specified because of the mutex member
    NtupleBranchAccessByName(const NtupleBranchAccessByName & other);
    NtupleBranchAccessByName & operator= (const NtupleBranchAccessByName & other);

    /// Default c-tor, creates an object that will *not* successfully read anything - call setBranchName below 
    /// to point it at something useful 
    NtupleBranchAccessByName();

    /// Set the branch name that we are pointing to 
    /// Also empties the known-branch cache to avoid loading the wrong branch from cache
    void setBranchName(const std::string & name);
    /// get the branch name this instance points to
    std::string getBranchName();

    /// Get the NtupleBranch for a given manager instance as a pointer. 
    /// Will return a nullptr if the lookup fails. 
    /// Faster than NtupleBranchMgr::getBranch() 
    /// Can be used to permanently store the branch object
    /// for future re-use to gain speed. 
    NtupleBranch<branchType>* getBranch(NtupleBranchMgr & t);

    /// This is the accessor for the branch content. 
    /// We return a boolean indicating whether the read
    /// attempt was succesful. 
    /// The first argument is the branch manager to 
    /// attempt to read from, 
    /// the second argument will be populated with 
    /// the branch content in case of a successful read. 
    bool getVal (NtupleBranchMgr &t, branchType & ret);

protected:
    /// Internal helper - adds a branch to our internal cache for a newly encountered tree
    typename std::map<TTree*, NtupleBranch<branchType>*>::iterator addBranch(NtupleBranchMgr & t);
    /// Data members: 

    /// Cache the ntuple branch we found for each Tree 
    std::map<TTree*, NtupleBranch<branchType>* > m_branchMap;
    /// A pair of mutexes needed to protect our non-safe ops 
    std::mutex m_mx_loadSafe;
    std::mutex m_mx_getVarSafe;
    /// the user-requested branch name
    std::string m_branchName;
};

#include "NtupleBranchAccessByName.ixx"

#endif //NTAU__NTUPLEBRANCH__ACCESS_BY_NAME__H
