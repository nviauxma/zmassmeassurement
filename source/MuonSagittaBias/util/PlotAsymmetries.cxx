#include <FourMomUtils/xAODP4Helpers.h>
#include <MuonSagittaBias/ArgumentParser.h>
#include <MuonSagittaBias/AsymmetryPlots.h>
#include <MuonSagittaBias/PhaseSpaceCuts.h>
#include <MuonSagittaBias/PlotProvider.h>
#include <MuonSagittaBias/SagittaBiasAnalyzer.h>
#include <MuonSagittaBias/SlicedPlotProvider.h>
#include <MuonSagittaBias/Utils.h>
#include <NtauHelpers/HistoBinConfig.h>
#include <NtauHelpers/OutFileWriter.h>

int main(int argc, char* argv[]) {
    ArgumentParser parser;
    parser.summary_pdf("AllAsymmetries")
        .out_root_file("AsymmetryTemplates.root");  //.inject_config_file("MuonSagittaBias/bias_injection_file.txt");
    if (!parser.parse_args(argc, argv)) return EXIT_FAILURE;

    /// Create the output file to store all of the histograms
    PlotWriter<TH1> out_file{parser.make_root_file() ? parser.out_file() : nullptr};
    if (parser.make_root_file() && !out_file.get()) { return EXIT_FAILURE; }
    if (!parser.make_root_file() && !parser.make_pdfs()) {
        std::cerr << parser.app_name() << "No output format" << std::endl;
        return EXIT_FAILURE;
    }

    CanvasOptions canvas_opts = CanvasOptions()
                                    .RatioAxis(AxisConfig().Title("to truth-level").ExtraTitleOffset(0.4).Min(0.9).Max(1.005))
                                    .YAxis(AxisConfig().Title("Entries").ExtraTitleOffset(0.4).TopPadding(0.7))
                                    // .XAxis(AxisConfig().ExtraTitleOffset(1.3))
                                    .LabelStatusTag("Simulation Internal")
                                    .OutputDir(parser.out_dir());

    std::shared_ptr<MultiPagePdfHandle> multi_page =
        parser.make_pdfs() ? PlotUtils::startMultiPagePdfFile(parser.summary_pdf(), canvas_opts) : nullptr;

    /// Select the sagitta biases for the plots
    const std::vector<int>& inject_types = SagittaBiasAnalyzer::injection_types();
    std::vector<int> biases_to_plot{MuonTrk::IDTrk};
    if (inject_types.size() > 1) {
        biases_to_plot += inject_types[SagittaBiasAnalyzer::smallest_inject_idx(MuonTrk::TrackType::IDTrk)];
        biases_to_plot += inject_types[SagittaBiasAnalyzer::largest_inject_idx(MuonTrk::TrackType::IDTrk)];
    }
    // if (parser.is_mc()) biases_to_plot += (int)MuonTrk::TruthTrk;

    Sample<SagittaBiasAnalyzer> sample_to_process = parser.prepare_sample<SagittaBiasAnalyzer>();

    std::vector<PlotContent<TH1>> sagitta_biases;

    std::vector<PhaseSpaceCuts> selections;

    ///
    selections += PhaseSpaceCuts("All", "All pairs", [](SagittaBiasAnalyzer&, int) -> bool { return true; });

    std::vector<SlicedPlotProvider> slicers;
    {
        const std::vector<double> coarse_eta_bins{-2.5, 0., 2.5};
        const std::vector<double> fine_eta_bins = PlotUtils::getLinearBinning(-2.5, 2.5, 25);

        slicers += SlicedPlotProvider(
            sample_to_process,
            [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
                return h->GetBin(h->GetXaxis()->FindBin(t.eta(trk_type, SagittaBiasAnalyzer::LepCharge::Plus)),
                                 h->GetYaxis()->FindBin(t.eta(trk_type, SagittaBiasAnalyzer::LepCharge::Minus)));
            },
            PlotUtils::HistoBinConfig()
                .x_bins(coarse_eta_bins)
                .y_bins(coarse_eta_bins)
                .name("EtaHemisphere")
                .x_title("#eta(#mu^{+})")
                .y_title("#eta(#mu^{-})"),
            selections[0]);

        slicers += SlicedPlotProvider(
            sample_to_process,
            [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
                return h->GetBin(h->GetXaxis()->FindBin(t.eta(trk_type, SagittaBiasAnalyzer::LepCharge::Plus)),
                                 h->GetYaxis()->FindBin(t.eta(trk_type, SagittaBiasAnalyzer::LepCharge::Minus)));
            },
            PlotUtils::HistoBinConfig()
                .x_bins(fine_eta_bins)
                .y_bins(fine_eta_bins)
                .name("EtaLeptons")
                .x_title("#eta(#mu^{+})")
                .y_title("#eta(#mu^{-})"),
            selections[0]);
    }

    PlotFiller z_mass_plot{
        [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) { return h->GetXaxis()->FindBin(t.invariant_m(trk_type)); },
        std::make_shared<TH1D>("InvariantMass", "dummy;m_{#mu#mu} [GeV];a.u.", 60, 61, 121)};

    PlotFiller jpsi_mass_plot{
        [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) { return h->GetXaxis()->FindBin(t.invariant_m(trk_type)); },
        std::make_shared<TH1D>("InvariantMass", "dummy;m_{#mu#mu} [GeV];a.u.", 32, 2.7, 3.5)};

    for (auto& eta_slicer : slicers) {
        if (parser.make_pdfs()) {
            sagitta_biases += eta_slicer.make_plots(z_mass_plot, biases_to_plot, {}, {"Z#rightarrow#mu#mu", eta_slicer.title()}, "Zmumu",
                                                    multi_page, canvas_opts, {61, 121});
            sagitta_biases += eta_slicer.make_plots(jpsi_mass_plot, biases_to_plot, {}, {"J/#psi#rightarrow#mu#mu", eta_slicer.title()},
                                                    "JPsi", multi_page, canvas_opts, {2.7, 3.5});

            sagitta_biases += eta_slicer.make_asymmetry_plots(z_mass_plot, biases_to_plot, {}, {"Z#rightarrow#mu#mu", eta_slicer.title()},
                                                              "Asym_Zmumu", multi_page, canvas_opts, {61, 121});
            sagitta_biases +=
                eta_slicer.make_asymmetry_plots(jpsi_mass_plot, biases_to_plot, {}, {"J/#psi#rightarrow#mu#mu", eta_slicer.title()},
                                                "Asym_JPsi", multi_page, canvas_opts, {2.7, 3.5});
        }

        eta_slicer.add_plot(z_mass_plot, parser.get_trk_types_ROOT_file(), out_file, {61, 121});
        eta_slicer.add_plot(jpsi_mass_plot, parser.get_trk_types_ROOT_file(), out_file, {2.7, 3.5});

        eta_slicer.add_asymmetry_plots(z_mass_plot, parser.get_trk_types_ROOT_file(), out_file, {61, 121});
        eta_slicer.add_asymmetry_plots(jpsi_mass_plot, parser.get_trk_types_ROOT_file(), out_file, {2.7, 3.5});
    }

    std::sort(sagitta_biases.begin(), sagitta_biases.end(),
              [](const PlotContent<TH1>& a, const PlotContent<TH1>& b) { return a.getFileName() < b.getFileName(); });
    for (auto& pc : sagitta_biases) {
        if (count_non_empty(pc)) DefaultPlotting::draw1D(pc);
    }

    return EXIT_SUCCESS;
}
