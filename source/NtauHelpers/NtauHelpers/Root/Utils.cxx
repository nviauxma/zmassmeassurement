#include <NtauHelpers/Utils.h>
#include <PathResolver/PathResolver.h>
#include <TFile.h>
#include <TTree.h>
#include <dirent.h>

#include <algorithm>
#include <cmath>
#include <iomanip>  //for std::setw
#include <locale>
using namespace PlotUtils;

std::vector<std::string> ReadList(const std::string &list_path) {
    std::vector<std::string> out;
    std::ifstream file_stream;
    file_stream.open(PlotUtils::GetFilePath(list_path));
    if (!file_stream.good()) return out;
    std::string line;
    while (GetLine(file_stream, line)) { out.push_back(line); }

    return out;
}

std::string LowerString(const std::string &str) {
    std::string Low;
    std::locale loc;
    for (std::string::size_type i = 0; i < str.length(); ++i) Low += std::tolower(str[i], loc);
    return Low;
}

std::string EraseWhiteSpaces(std::string str) {
    str.erase(std::remove(str.begin(), str.end(), '\t'), str.end());
    if (str.find(" ") == 0) return EraseWhiteSpaces(str.substr(1, str.size()));
    if (str.size() > 0 && str.find(" ") == str.size() - 1) return EraseWhiteSpaces(str.substr(0, str.size() - 1));
    return str;
}

bool GetLine(std::ifstream &inf, std::string &line) {
    if (!std::getline(inf, line)) return false;
    line = EraseWhiteSpaces(line);
    if (line.find("#") == 0 || line.size() < 1) return GetLine(inf, line);
    return true;
}

std::string GetWordFromStream(std::stringstream &sstr, bool Reset) {
    std::stringstream::pos_type Pos = sstr.tellg();
    std::string word;
    sstr >> word;
    if (Reset) {
        sstr.clear();
        sstr.seekg(Pos, sstr.beg);
    }
    return word;
}
std::string WhiteSpaces(int N, const std::string &space) {
    std::string White;
    for (int n = 0; n < N; ++n) White += space;
    return White;
}

bool DoesDirectoryExist(const std::string &Path) {
    DIR *dir = opendir(Path.c_str());
    bool Exist = dir != nullptr;
    if (Exist) closedir(dir);
    return Exist;
}
bool DoesFileExist(const std::string &Path) { return std::ifstream(Path).is_open(); }

std::vector<std::string> tokenize(std::string str, const std::string &delimiter) {
    std::vector<std::string> out;
    size_t delim_pos{0};
    while ((delim_pos = str.find(delimiter)) != std::string::npos) {
        out.push_back(str.substr(0, delim_pos));
        str = str.substr(delim_pos + 1, std::string::npos);
    }
    out.push_back(str);
    EraseFromVector<std::string>(out, [](const std::string &v_str) { return v_str.empty(); });
    return out;
}
bool passWildCard(const std::string &candidate, const std::string &wildcard) {
    std::vector<std::string> tokens = tokenize(wildcard, "*");
    if (tokens.empty()) return true;
    size_t last_pos{0}, n_tok{0}, last_tok{tokens.size() - 1};
    for (const std::string &tok : tokens) {
        last_pos = candidate.find(tok, last_pos);
        if (last_pos == std::string::npos) return false;
        /// account for tokens with *blah vs blah*
        if (n_tok == 0 && last_pos != 0 && wildcard[0] != '*')
            return false;
        else if (n_tok == last_tok && wildcard[wildcard.size() - 1] != '*' && last_pos + tok.size() != candidate.size())
            return false;
        ++n_tok;
    }
    return true;
}
std::vector<std::string> ListDirectory(const std::string &Path, std::string const &WildCard) {
    std::vector<std::string> List;
    DIR *dir{nullptr};
    dirent *ent{nullptr};
    std::string dir_path = Path.size() ? Path : "./";
    bool wild_card_in_path{false};
    /// Someone has given my_stonjek/*/ as file_name
    if (dir_path.find("*") != std::string::npos) {
        wild_card_in_path = true;
        std::vector<std::string> tokenized_path = tokenize(dir_path, "*");
    }

    /// Read out the directory
    if ((dir = opendir(dir_path.c_str())) != nullptr) {
        while ((ent = readdir(dir)) != nullptr) {
            std::string Entry = ent->d_name;
            if (Entry == "." || Entry == "..") continue;  // skip the .. and . lines in ls

            if (passWildCard(Entry, WildCard)) List.push_back(Path + "/" + Entry);
        }
        closedir(dir);
    }
    return List;
}

std::string TimeHMS(float t) {
    std::stringstream ostr;
    ostr << std::setw(2) << std::setfill('0') << (int)((t / 60. / 60.)) % 24 << ":" << std::setw(2) << std::setfill('0')
         << ((int)(t / 60.)) % 60 << ":" << std::setw(2) << std::setfill('0') << ((int)t) % 60;
    return ostr.str();
}
std::string ReplaceChars(std::string str, const std::string &with, const std::vector<std::string> &to_replace) {
    for (auto &exp : to_replace) {
        if (str.find(exp) != std::string::npos) str = ReplaceExpInString(str, exp, with);
    }
    return str;
}

std::string ResolveFilePath(const std::string &Config) {
    if (Config.empty()) {
        Error("ResolveFilePath()", "Nothing was given");
        return std::string{};
    }
    /// This file is very likely invoking some protocols like http://, root:// etc.
    if (Config.find("://") == Config.find("/") - 1) { return Config; }
    std::string Path{""};
    std::vector<std::string> Trials{ResolveEnviromentVariables(Config), Config,
                                    // Try the PathResolver
                                    PathResolverFindCalibFile(ResolveEnviromentVariables(Config)),
                                    // TestArea
                                    ResolveEnviromentVariables("${TestArea}/" + Config),
                                    ResolveEnviromentVariables("${WorkDir_DIR}/data/" + Config),
                                    ResolveEnviromentVariables("${WorkDir_DIR}/data/" + ReplaceExpInString(Config, "data/", ""))};

    // The out of the box config does not work
    for (auto &T : Trials) {
        Path = T;
        std::ifstream Import;
        Import.open(T);
        if (Import.good()) { return ReplaceExpInString(T, "//", "/"); }
    }
    Error("ResolveFilePath()", "Could not find a valid config path " + Config);
    return std::string{};
}

std::vector<std::string> GetPathResolvedFileList(const std::vector<std::string> &FileList) {
    std::vector<std::string> ResolvedList;
    for (auto &File : FileList) {
        std::string F = ResolveFilePath(File);
        if (!F.empty()) ResolvedList.push_back(F);
    }
    return ResolvedList;
}

int GetIntegerFromStream(std::stringstream &sstr, bool Reset) {
    std::stringstream::pos_type Pos = sstr.tellg();
    int integer;
    sstr >> integer;
    if (sstr.fail()) integer = INT_MIN;
    if (Reset) {
        sstr.clear();
        sstr.seekg(Pos, sstr.beg);
    }
    return integer;
}
float GetFloatFromStream(std::stringstream &sstr, bool Reset) {
    std::stringstream::pos_type Pos = sstr.tellg();
    float floating;
    sstr >> floating;
    if (sstr.fail()) floating = FLT_MIN;

    if (Reset) {
        sstr.clear();
        sstr.seekg(Pos, sstr.beg);
    }
    return floating;
}
double GetDoubleFromStream(std::stringstream &sstr, bool Reset) {
    std::stringstream::pos_type Pos = sstr.tellg();
    double db;
    sstr >> db;
    if (sstr.fail()) db = DBL_MIN;

    if (Reset) {
        sstr.clear();
        sstr.seekg(Pos, sstr.beg);
    }
    return db;
}

std::vector<std::string> GetTreesInFile(const std::string &file) { return getAllObjectsFromFile<TTree>(file); }

std::vector<int> FillRange(int low, int high) {
    std::vector<int> numbers;
    FillRange(numbers, low, high);
    return numbers;
}
void FillRange(std::vector<int> &numbers, int low, int high) {
    numbers.reserve(high - low > 0 ? high - low : 0);
    for (int i = low; i < high; ++i) numbers.push_back(i);
}

int gcd(int a, int b) { return b == 0 ? a : gcd(b, a % b); }

unsigned int binomial(unsigned int n, unsigned int k) {
    std::vector<unsigned int> C(k + 1, 0);
    C[0] = 1;  // nC0 is 1
    for (unsigned int i = 1; i <= n; i++) {
        // Compute next row of pascal triangle using
        // the previous row
        for (unsigned int j = std::min(i, k); j > 0; j--) C[j] = C[j] + C[j - 1];
    }
    return C[k];
}

void mkdir(const std::string &dir_path) {
    if (!DoesDirectoryExist(dir_path)) {
        std::cout << "mkdir()  -- Create directory " << dir_path << ". " << std::endl;
        gSystem->mkdir(dir_path.c_str(), true);
    }
}

std::shared_ptr<TFile> make_out_file(const std::string &path) {
    if (path.rfind("/") != std::string::npos) {
        std::string OutDir = path.substr(0, path.rfind("/"));
        mkdir(OutDir);
    }
    if (path.find(".root") == std::string::npos || path.rfind(".root") != path.rfind(".")) {
        std::cout << "make_out_file() -- The File " << path << " has no .root suffix." << std::endl;
        return nullptr;
    }
    std::cout << "make_out_file()  -- Create new file " << path << " for writing job output " << std::endl;
    std::shared_ptr<TFile> out_file = std::make_shared<TFile>(path.c_str(), "RECREATE");
    if (!out_file->IsOpen()) out_file.reset();
    return out_file;
}
