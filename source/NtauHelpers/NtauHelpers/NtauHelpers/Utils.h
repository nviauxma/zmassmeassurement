#ifndef NTAUHELPERS_UTILS_H
#define NTAUHELPERS_UTILS_H

#include <MuonIdHelpers/MuonStationIndex.h>
#include <NtupleAnalysisUtils/NTAUTopLevelIncludes.h>
#include <TFile.h>
#include <TH1.h>
#include <TString.h>
#include <TSystem.h>
#include <TTree.h>
#include <xAODMuon/Muon.h>

#include <chrono>
#include <fstream>
#include <future>
#include <iostream>
#include <map>
#include <mutex>
#include <set>
#include <sstream>
#include <string>
#include <thread>
#include <tuple>
#include <vector>

// Defined in PlotUtils
//      --   std::string RandomString(size_t length);
//      --   TDirectory* mkdir(const std::string &name, TDirectory *in_dir);
//      --   std::shared_ptr<TFile> Open(const std::string &Path);
//      --   std::string ResolveEnviromentVariables(std::string str);
//      --   std::string ReplaceExpInString(std::string str, const std::string &exp, const std::string &rep);

///
///  Fetches a line from an in-file stream and pipes it
///  to the string variable. Lines are skipped if they start with #
///  and leading /  trailing white spaces are removed from the string
///  Returns false if the end of the instream is reached
bool GetLine(std::ifstream &inf, std::string &line);

/// Erases all leading and trailing white space characters from a string
std::string EraseWhiteSpaces(std::string str);

/// Opens a txt file and pipes its content to a string-vector where each
/// element represents a single line. The list is read following the rules from
/// GetLine method
std::vector<std::string> ReadList(const std::string &list_path);

/// Creates a new string which is the same as the input with all characters lowered
std::string LowerString(const std::string &str);
/// Returns a string with N white spaces
std::string WhiteSpaces(int N = 0, const std::string &space = " ");
/// Tokenizes a string according to a given delimiter
std::vector<std::string> tokenize(std::string str, const std::string &delimiter);

/// Returns whether a given string passes a wild card. I.e.
/// if Majo* is contained in Majonaise
bool passWildCard(const std::string &string, const std::string &wildcard);
/// Returns the binomial coefficient of N over M
unsigned int binomial(unsigned int n, unsigned int m);
/// Returns a std::vector from low to high - 1
std::vector<int> FillRange(int low, int high);
void FillRange(std::vector<int> &to_fill, int low, int high);
/// Returns the greates common divisor of two numbers.
int gcd(int a, int b);
/// Returns the sign of a number
char sign(double N);

/// Returns the posiiton of the most left bit which is set to 1
template <typename T> int max_bit(const T &number);
/// Returns the posiiton of the most right bit which is set to 1
template <typename T> int min_bit(const T &number);
/// Removes duplicate elements from the vector. The first occurance of each element is kept without changing the order
///     toClear: vector to be cleaned
///     equal: Lambda expression defining when two elements are treated as equal
template <typename T> void ClearFromDuplicates(std::vector<T> &toClear, std::function<bool(const T &, const T &)> equal);
/// Removes duplicate elements from the vector as above but using operator ==() of the elements
template <typename T> void ClearFromDuplicates(std::vector<T> &toClear);

/// Removes all elements from a vector in which the func expression returns true.
template <typename T> void EraseFromVector(std::vector<T> &vec, std::function<bool(const T &)> func);

/// Returns true if the number is not (std::nan or std::inf)
template <typename T> bool IsFinite(const T &N);

/// Short wrapper for std::find(container.begin(), container.end(), elemet)
template <typename T> bool isElementInList(const std::vector<T> &vec, const T &ele);
/// Returns true if the elements numbers in the list have all the same distance
template <typename T> bool isEquiDistant(const std::vector<T> &vec);
/// If the contained data is separated by equal distances, then the function returns
/// the number of bins needed to pack the same data into a histogram. Otherwise 0
/// It is assumed that the number are orded ascending
template <typename T> size_t calculateNbins(const std::vector<T> &vec);

/// Equality operator of two vectors. They are the same if they are the same and each element
/// is also the same
template <typename T> bool operator==(const std::vector<T> &vec1, const std::vector<T> &vec2);

constexpr int MsgMethodChars = 35;
template <class... Args> void Info(const std::string &method, const std::string &info_str, Args &&...args);
template <class... Args> void Warning(const std::string &method, const std::string &info_str, Args &&...args);
template <class... Args> void Error(const std::string &method, const std::string &info_str, Args &&...args);

/// Issues the >> operator on a stringstream and returns the obtained string.
///      If Reset is set to true the stringstream leaves the function in the same
///      state as it entered the method.
std::string GetWordFromStream(std::stringstream &sstr, bool Reset = false);
/// Pipes the word from the stringstream into an integer  variable
int GetIntegerFromStream(std::stringstream &sstr, bool Reset = false);
///  Pipes the word from the stringstream into a float variable
float GetFloatFromStream(std::stringstream &sstr, bool Reset = false);
///  Pipes the word from the stringstream into a double variable
double GetDoubleFromStream(std::stringstream &sstr, bool Reset = false);

std::vector<std::string> ListDirectory(const std::string &Path, const std::string &WildCard);

bool DoesFileExist(const std::string &Path);
bool DoesDirectoryExist(const std::string &Path);

void mkdir(const std::string &dir_path);

/// Transforms a floating point number into hours, minutes and seconds HH:MM:SS
std::string TimeHMS(float t);

/// Replace characters
std::string ReplaceChars(std::string str, const std::string &with = "_",
                         const std::vector<std::string> &exp = std::vector<std::string>{" ", "-", "(", ")", "]", "[", "{", "}", "__"});

/// Create a TFile pointer usable for writing the analysis output. If the out-file shall be saved in a given directory the
/// directory is created if needed.
std::shared_ptr<TFile> make_out_file(const std::string &path);

/// Tries to resolve a file path
///     --- Environment variables can be encoded via ${my_env_var}/my_stonjek_path/...
///     --- The ATLAS package data-file system can be invoked by <my_atlas_package/my_data_file.txt
///     --- xrootd files... I.e. files starting with <>:// are never resolved but assumed to exist
std::string ResolveFilePath(const std::string &Path);

std::vector<std::string> GetPathResolvedFileList(const std::vector<std::string> &FileList);

/// Returns the number of plots in a PlotContent having at least one entry
template <class T> unsigned int count_non_empty(PlotContent<T> &h);

/// Creates a Plot Sample from a file_list & tree_name. The file list is read and each item is
/// checked for validity I.e required to contain the given tree and then piped to the Sample
template <class InputType> Sample<InputType> make_sample(const std::string &file_list, const std::string &tree_name);
template <class InputType> Sample<InputType> make_sample(const std::vector<std::string> &files, const std::string &tree_name);

#include <NtauHelpers/Utils.ixx>
#endif
