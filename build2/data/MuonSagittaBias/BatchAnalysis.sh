#!/bin/bash

# some initial output
echo "###############################################################################################"
echo "					 Enviroment variables"
echo "###############################################################################################"
export
echo "###############################################################################################"
echo " "
echo "###############################################################################################"
if [ -f "${ClusterControlModule}" ]; then
    source ${ClusterControlModule}
fi
ID=`get_task_ID`
echo "Got TaskID ${ID}"
if [ -z ${ID} ];then
    echo "Something went horrible wrong in the task assignment"
    send_to_failure
fi
InFile=""
if [ -f "${InCfg}" ];then
    echo "InFile=`sed -n \"${ID}{p;q;}\" ${InCfg}`"
    InFile=`sed -n "${ID}{p;q;}" ${InCfg}`
fi

if  [ -z "${InFile}" ]; then
   echo "Invalid input config given"
   send_to_failure
fi

OutFile=""
if [ -f "${OutCfg}" ];then
    echo "OutFile=`sed -n \"${ID}{p;q;}\" ${OutCfg}`"
    OutFile=`sed -n "${ID}{p;q;}" ${OutCfg}`
fi

if  [ -z "${OutFile}" ]; then
   echo "Invalid out file given"
   send_to_failure
fi
ExtraOpts=""
if [ -f "${JobCfg}" ];then
    echo "ExtraOpts=`sed -n \"${ID}{p;q;}\" ${JobCfg}`"
    ExtraOpts=`sed -n "${ID}{p;q;}" ${JobCfg}`
fi
if [ -z "${ExtraOpts}" ]; then
    echo "No extra options were given"
    send_to_failure
fi
sleeptime=$(bc -l <<<  ${ID}'/100')
echo "I am so tired (and the cluster does not work anyway), I have to sleep for ${sleeptime}s"
sleep $sleeptime
echo 'Hi, I am back to work'
if [ -z ${ATLAS_LOCAL_ROOT_BASE} ];then
	echo "###############################################################################################"
	echo "					Setting up the enviroment"
	echo "###############################################################################################"
	echo "cd ${TMPDIR}"
	cd ${TMPDIR}
	echo "export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase"
	export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
	echo "Setting Up the ATLAS Enviroment:"
	echo "source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh" 
	source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
	if [ -d "${OriginalArea}" ]; then
            echo "asetup ${OriginalProject},${OriginalPatch},${OriginalVersion},here"
            asetup ${OriginalProject},${OriginalPatch},${OriginalVersion},here
            if [ -f "${OriginalArea}/../build/${BINARY_TAG}/setup.sh" ]; then
				echo "source ${OriginalArea}/../build/${BINARY_TAG}/setup.sh"
				source ${OriginalArea}/../build/${BINARY_TAG}/setup.sh
		        WORKDIR=${OriginalArea}/../build/${BINARY_TAG}/bin/
		    elif [ -f ${OriginalArea}/../build/${WorkDir_PLATFORM}/setup.sh ];then
		    	echo "source ${OriginalArea}/../build/${WorkDir_PLATFORM}/setup.sh"
                source ${OriginalArea}/../build/${WorkDir_PLATFORM}/setup.sh
				WORKDIR=${OriginalArea}/../build/${WorkDir_PLATFORM}/bin/
			 elif [ -f ${OriginalArea}/../build/${AthAnalysis_PLATFORM}/setup.sh ];then
		    	echo "source ${OriginalArea}/../build/${AthAnalysis_PLATFORM}/setup.sh"
                source ${OriginalArea}/../build/${AthAnalysis_PLATFORM}/setup.sh
				WORKDIR=${OriginalArea}/../build/${AthAnalysis_PLATFORM}/bin/
             elif [ -z "${CMTBIN}" ];then
				source  ${OriginalArea}/../build/x86_64*/setup.sh
                if [ $? -ne 0 ];then
                    echo "Something strange happens?!?!?!"
                    export
                    echo " ${OriginalArea}/../build/${BINARY_TAG}/setup.sh"
                    echo " ${OriginalArea}/../build/${WorkDir_PLATFORM}/setup.sh"            
                    send_to_failure 
                fi
			fi		   
	#Neither ROOTCORE or Athena area found
	else
        export
		echo "Something strange happens?!?!?!"
		send_to_failure				
	fi    
fi
cd ${TMPDIR}
#Avoid writing the efficiencies and UnMatches as they are rewritten during merging anyway....
echo "${Exec} --fileList ${InFile}  --outDir ${TMPDIR} --outFile tmp_out_${ID}.root --singleThread --noPDFs ${ExtraOpts}"
${Exec} --fileList ${InFile}  --outDir ${TMPDIR} --outFile tmp_out_${ID}.root --singleThread --noPDFs ${ExtraOpts}

if [ $? -eq 0 ]; then
	echo "###############################################################################################"
	echo "						${Exec} terminated successfully"
	echo "###############################################################################################"
	ls -lh
	echo "mv ${TMPDIR}/tmp_out_${ID}.root ${OutDir}/${OutFile}"
	mv ${TMPDIR}/tmp_out_${ID}.root ${OutDir}/${OutFile}	
else
	echo "###############################################################################################"
	echo "					${Exec} job has experienced an error"
	echo "###############################################################################################"
	send_to_failure    
fi
