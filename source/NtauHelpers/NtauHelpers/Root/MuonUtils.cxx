#include <NtauHelpers/MuonUtils.h>
#ifndef XAOD_ANALYSIS
#include <IdDictParser/IdDictParser.h>
#include <MuonIdHelpers/MdtIdHelper.h>
#endif
std::string to_string(xAOD::Muon::Quality WP) {
    if (WP == xAOD::Muon::Quality::VeryLoose) return "VeryLoose";
    if (WP == xAOD::Muon::Quality::Loose) return "Loose";
    if (WP == xAOD::Muon::Quality::Medium) return "Medium";
    if (WP == xAOD::Muon::Quality::Tight) return "Tight";
    return "Unknown";
}
std::string to_string(xAOD::Muon::TrackParticleType TP) {
    if (TP == xAOD::Muon::TrackParticleType::Primary) return "PR";
    if (TP == xAOD::Muon::TrackParticleType::InnerDetectorTrackParticle) return "ID";
    if (TP == xAOD::Muon::TrackParticleType::MuonSpectrometerTrackParticle) return "MS";
    if (TP == xAOD::Muon::TrackParticleType::ExtrapolatedMuonSpectrometerTrackParticle) return "ME";
    if (TP == xAOD::Muon::TrackParticleType::MSOnlyExtrapolatedMuonSpectrometerTrackParticle) return "MSOE";
    if (TP == xAOD::Muon::TrackParticleType::CombinedTrackParticle) return "CB";
    return "Unknown";
}
std::string to_string(xAOD::Muon::Author A) {
    if (A == xAOD::Muon::MuidCo) return "MuidCo";
    if (A == xAOD::Muon::STACO) return "STACO";
    if (A == xAOD::Muon::MuTag) return "MuTag";
    if (A == xAOD::Muon::MuTagIMO) return "MuTagIMO";
    if (A == xAOD::Muon::MuidSA) return "MuidSA";
    if (A == xAOD::Muon::MuGirl) return "MuGirl";
    if (A == xAOD::Muon::MuGirlLowBeta) return "MuGirlLowBeta";
    if (A == xAOD::Muon::CaloTag) return "CaloTag";
    if (A == xAOD::Muon::CaloLikelihood) return "CaloLikelihood";
    if (A == xAOD::Muon::ExtrapolateMuonToIP) return "ExtrapolateMuonToIP";
    return "Unknown";
}

///############################################################################
///                 IdentifierService
///############################################################################

IdentifierService::~IdentifierService() { m_inst = nullptr; }

IdentifierService* IdentifierService::m_inst = nullptr;
#ifndef XAOD_ANALYSIS

IdentifierService::IdentifierService(const std::string& muon_dict) :
    m_dict_parser{std::make_shared<IdDictParser>()}, m_mdt_helper{std::make_shared<MdtIdHelper>()} {
    m_dict_parser->register_external_entity("MuonSpectrometer", muon_dict);
    m_mdt_helper->initialize_from_dictionary(m_dict_parser->parse("IdDictParser/ATLAS_IDS.xml"));
}
#endif
