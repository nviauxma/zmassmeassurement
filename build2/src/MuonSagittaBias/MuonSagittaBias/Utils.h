#ifndef MUONSAGITTABIAS__Utils__H
#define MUONSAGITTABIAS__Utils__H
#include <NtauHelpers/HistoUtils.h>
#include <NtauHelpers/MuonUtils.h>
#include <NtauHelpers/Utils.h>
#include <TF1.h>
#include <TGraph.h>
namespace MuonTrk {

    class calib_constants {
    public:
        calib_constants() = default;
        calib_constants(const calib_constants& other) = default;
        calib_constants(calib_constants&& other) = default;

        calib_constants& operator=(const calib_constants& other) = default;
        calib_constants& operator=(calib_constants&& other) = default;

        /// Constructor from a string name
        calib_constants(const std::string& calib_str);
        /// Constructor only specifiying the type
        calib_constants(int _trk_type);

        static int to_trk_type(const std::string& str);

        bool has_bias() const;

        bool has_resolution() const;

        /// Dumb the calibartion constants to a string used in writing
        /// the associated histograms
        std::string to_string() const;
        /// Dumb the calibration constants to a string in LateX format
        std::string to_title() const;

        // Comparison operators
        bool operator==(const calib_constants& other) const;
        bool operator!=(const calib_constants& other) const;
        bool operator<(const calib_constants& b) const;

        /// Sagitta bias 1 /p = s -> s + delta*q

        float sagitta_bias{0.};
        /// Radial bias
        ///   p_{T} -> p_{T}*(1 + rho)
        ///   tan(theta)  = tan(theta) *(1 +rho)
        float radial_bias{0.};
        /// Magnetic bias
        ///     p --> p (1 + lambda)
        float magnetic_bias{0.};

        /// Constant resolution term
        ///     -- The particle momentum is smeared by a constant number
        float res_const{0.};
        ///   --- Linear resolution term
        ///       --> A pt dependent smearing is applied
        float res_lin{0.};
        ///       --> A smearing propotional to pt^{2} is applied
        float res_quad{0.};
        ///       --> A smearing propotional to the inverse pt is applied
        float res_hyp{0.};

        int trk_type{0};

    private:
        void extract_value(const std::vector<std::string>& tokens, const std::string& key, float& put_val_to) const;
    };

    enum TrackType {

        Primary = 1 << 0,  /// I.e. combined
        IDTrk = 1 << 1,
        MSTrk = 1 << 2,
        METrk = 1 << 3,
        MSOETrk = 1 << 4,
        CmbTrk = 1 << 5,
        TruthTrk = 1 << 6,

        /// Define the different biases of the track
        SagittaBias = 1 << 7,
        RadialBias = 1 << 8,
        MagneticBias = 1 << 9,
        DetectorResolution = 1 << 10,
        BiasedTrk = SagittaBias | RadialBias | MagneticBias | DetectorResolution,

        NTypes = 11,

        /// Unbiased tracks from the TTree
        OrdinaryTrks = Primary | IDTrk | MSTrk | METrk | MSOETrk | CmbTrk | TruthTrk,
    };

    /// Transforms the track type into string
    std::string to_string(int trk_type);

    // Revert operation of the one above
    int to_type(const std::string& str);
    /// Transforms the track type into a string
    /// printed on a Canvas
    std::string to_title(int trk_type);

}  // namespace MuonTrk

/// Enum to specify the strategy for the trigger
/// prescaling
namespace TriggerPreScale {
    enum Strategy {
        NotApplied = 0,
        PreScaleMC = 1,
        UnPreScaleData = 2,
    };
}

class DataTemplateHolder;
///
std::shared_ptr<TGraph> extract_chi2(const std::vector<std::shared_ptr<DataTemplateHolder>>& mc_templates,
                                     const DataTemplateHolder& data_template);

struct FitResult {
    /// Fit parameters from the parabola functional form
    ///     c + a*(x-b)^{2}
    ///     x_shit = b
    ///     slope = a
    ///     offset = c
    double x_shift{0.};
    double slope{0.};
    double offset{0.};
    ///
    double one_sigma{0.};
    /// Final chi2 of the parabola fit
    double chi2{DBL_MAX};
    /// How many iterations did the fit need
    unsigned int num_iter{0};
    /// Number of degrees of freedom available in the fit
    unsigned int nDoF{0};

    std::shared_ptr<TGraph> fit_graph{};
    ////
    bool succeed{false};
};

FitResult fit_parabola(const std::shared_ptr<TGraph>& chi2_histo);

void set_2D_color_palette();

void draw_2D_plot(PlotContent<TH1>& pc);

#endif