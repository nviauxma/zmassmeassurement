# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#
# This file is here to intercept find_package(Qt5) calls, and
# massage the paths produced by the system module, to make them relocatable.
#

# The LCG include(s):
include( LCGFunctions )

# Qt5 needs at least one component to be requested from it. So let's make
# Core compulsory.
list( APPEND Qt5_FIND_COMPONENTS Core )
list( REMOVE_DUPLICATES Qt5_FIND_COMPONENTS )

# Temporarily clean out CMAKE_MODULE_PATH, so that we could pick up
# FindQt5.cmake from CMake:
set( _modulePathBackup ${CMAKE_MODULE_PATH} )
set( CMAKE_MODULE_PATH )

# Make the code ignore the system path(s):
if( QT5_ROOT )
   set( CMAKE_SYSTEM_IGNORE_PATH /usr/include /usr/bin /usr/lib /usr/lib32
      /usr/lib64 )
endif()

# Let Qt's own CMake code be found:
find_package( Qt5 )

# Restore CMAKE_MODULE_PATH:
set( CMAKE_MODULE_PATH ${_modulePathBackup} )
unset( _modulePathBackup )
set( CMAKE_SYSTEM_IGNORE_PATH )

# Massage the paths found by the system module:
if( Qt5_FOUND AND NOT GAUDI_ATLAS )

   # Make sure that the lib/ directory is added to the environment:
   set( _relocatableLibDir "${QT5_LCGROOT}/lib" )
   _lcg_make_paths_relocatable( _relocatableLibDir )
   set( QT5_LIBRARY_DIRS
      $<BUILD_INTERFACE:${QT5_LCGROOT}/lib>
      $<INSTALL_INTERFACE:${_relocatableLibDir}>
      CACHE PATH "Library directories for Qt5" )
   unset( _relocatableLibDir )

   # Make sure that the include directories are added to the environment:
   set( _incDirs )
   foreach( component ${_qt5_components} )
      foreach( _inc ${Qt5${component}_INCLUDE_DIRS} )
         set( _relocatableIncDir ${_inc} )
         _lcg_make_paths_relocatable( _relocatableIncDir )
         list( APPEND _incDirs
            $<BUILD_INTERFACE:${_inc}>
            $<INSTALL_INTERFACE:${_relocatableIncDir}> )
         unset( _relocatableIncDir )
      endforeach()
   endforeach()
   set( QT5_INCLUDE_DIRS ${_incDirs}
      CACHE PATH "Include directories for Qt5" )
   unset( _incDirs )
endif()


# Set the environment variables needed for the Qt5 runtime:
# -- Set the QT_PLUGIN_PATH environment variable:
if( Qt5_FOUND )

    # get the location of the Qt5Core lib and set some variables
    get_target_property( QtCore_location Qt5::Core LOCATION )

    # set the env vars needed at runtime
    set( QT5_ENVIRONMENT
        PREPEND QT_PLUGIN_PATH ${QT5_LCGROOT}/plugins
        PREPEND QTDIR ${QT5_LCGROOT}
        PREPEND QTINC ${QT5_LCGROOT}/include
        PREPEND QTLIB ${QT5_LCGROOT}/lib )

    # handle Qt5 output as the other standard CMake packages
    include( FindPackageHandleStandardArgs )
    find_package_handle_standard_args( Qt5ATLAS
        FOUND_VAR Qt5ATLAS_FOUND
        REQUIRED_VARS QtCore_location
        VERSION_VAR ${QT5_LCGVERSION} )

endif()
