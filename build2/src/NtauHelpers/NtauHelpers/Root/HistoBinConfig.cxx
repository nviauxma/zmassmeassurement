#include <NtauHelpers/HistoBinConfig.h>
#include <NtauHelpers/HistoUtils.h>
#include <NtupleAnalysisUtils/Helpers/BinningHelpers.h>
namespace PlotUtils {

    HistoBinConfig::HistoBinConfig(const HistoBinConfig& other) :
        name(other.name, this),
        title(other.title, this),
        x_title(other.x_title, this),
        y_title(other.y_title, this),
        z_title(other.z_title, this),

        x_bins(other.x_bins, this),
        x_min(other.x_min, this),
        x_max(other.x_max, this),
        x_num_bins(other.x_num_bins, this),
        x_log_binning(other.x_log_binning, this),
        x_bin_labels(other.x_bin_labels, this),

        y_bins(other.y_bins, this),
        y_min(other.y_min, this),
        y_max(other.y_max, this),
        y_num_bins(other.y_num_bins, this),
        y_log_binning(other.y_log_binning, this),
        y_bin_labels(other.y_bin_labels, this),

        z_bins(other.z_bins, this),
        z_min(other.z_min, this),
        z_max(other.z_max, this),
        z_num_bins(other.z_num_bins, this),
        z_log_binning(other.z_log_binning, this),
        z_bin_labels(other.x_bin_labels, this) {}

    HistoBinConfig& HistoBinConfig::operator=(const HistoBinConfig& other) {
        if (&other != this) {
            name.copyFrom(other.name, this);
            title.copyFrom(other.title, this);

            x_title.copyFrom(other.x_title, this);
            y_title.copyFrom(other.y_title, this);
            z_title.copyFrom(other.z_title, this);

            x_bins.copyFrom(other.x_bins, this);
            x_min.copyFrom(other.x_min, this);
            x_max.copyFrom(other.x_max, this);
            x_num_bins.copyFrom(other.x_num_bins, this);
            x_log_binning.copyFrom(other.x_log_binning, this);

            y_bins.copyFrom(other.y_bins, this);
            y_min.copyFrom(other.y_min, this);
            y_max.copyFrom(other.y_max, this);
            y_num_bins.copyFrom(other.y_num_bins, this);
            y_log_binning.copyFrom(other.y_log_binning, this);

            z_bins.copyFrom(other.z_bins, this);
            z_min.copyFrom(other.z_min, this);
            z_max.copyFrom(other.z_max, this);
            z_num_bins.copyFrom(other.z_num_bins, this);
            z_log_binning.copyFrom(other.z_log_binning, this);

            x_bin_labels.copyFrom(other.x_bin_labels, this);
            y_bin_labels.copyFrom(other.y_bin_labels, this);
        }
        return *this;
    }
    std::shared_ptr<TH1> HistoBinConfig::make_histo() {
        if (!x_bins.isUserSet()) {
            if (x_min.isUserSet() && x_max.isUserSet() && x_num_bins.isUserSet()) {
                x_bins(x_log_binning() ? getLogLinearBinning(x_min(), x_max(), x_num_bins())
                                       : getLinearBinning(x_min(), x_max(), x_num_bins()));
            }
        }
        if (!y_bins.isUserSet()) {
            if (y_min.isUserSet() && y_max.isUserSet() && y_num_bins.isUserSet()) {
                y_bins(y_log_binning() ? getLogLinearBinning(y_min(), y_max(), y_num_bins())
                                       : getLinearBinning(y_min(), y_max(), y_num_bins()));
            }
        }
        if (!z_bins.isUserSet()) {
            if (z_min.isUserSet() && z_max.isUserSet() && z_num_bins.isUserSet()) {
                z_bins(z_log_binning() ? getLogLinearBinning(z_min(), z_max(), z_num_bins())
                                       : getLinearBinning(z_min(), z_max(), z_num_bins()));
            }
        }
        std::shared_ptr<TH1> bin_template = MakeTH1(x_bins(), y_bins(), z_bins());
        if (bin_template) {
            if (name.isUserSet()) bin_template->SetName(name().c_str());
            if (title.isUserSet()) bin_template->SetTitle(title().c_str());
            if (x_title.isUserSet()) bin_template->GetXaxis()->SetTitle(x_title().c_str());
            if (y_title.isUserSet()) bin_template->GetYaxis()->SetTitle(y_title().c_str());
            if (z_title.isUserSet()) bin_template->GetZaxis()->SetTitle(z_title().c_str());

            if (!x_bin_labels().empty()) set_bin_labels(bin_template->GetXaxis(), x_bin_labels());
            if (!y_bin_labels().empty()) set_bin_labels(bin_template->GetYaxis(), y_bin_labels());
            if (!z_bin_labels().empty()) set_bin_labels(bin_template->GetZaxis(), z_bin_labels());
        }
        return bin_template;
    }

}  // namespace PlotUtils
