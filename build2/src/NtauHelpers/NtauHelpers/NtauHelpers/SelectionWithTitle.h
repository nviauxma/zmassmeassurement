#ifndef SELECTIONWITH_TITLE_H
#define SELECTIONWITH_TITLE_H
#include <NtauHelpers/Utils.h>
#include <NtupleAnalysisUtils/NTAUTopLevelIncludes.h>

template <class ProcessThis> class SelectionWithTitle : public Selection<ProcessThis> {
public:
    // Standard constructor for everyday use - give the selection (cut) a name and something to do
    SelectionWithTitle(const std::string& title, std::function<bool(ProcessThis&)> toApply);
    // copy constructor
    SelectionWithTitle(const SelectionWithTitle<ProcessThis>& other);
    // default, provides a dummy selection accepting everything
    SelectionWithTitle();

    std::shared_ptr<ISelection> clone() const override;
    void setTitle(const std::string& title);
    std::string getTitle() const;

private:
    std::string m_title;
};
#include <NtauHelpers/SelectionWithTitle.ixx>
#endif
