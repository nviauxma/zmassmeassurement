#ifndef NTAU__HISTOEVALUATION__H
#define NTAU__HISTOEVALUATION__H

#include "NtupleAnalysisUtils/Plot/Plot.h"
#include "NtupleAnalysisUtils/Configuration/AxisConfig.h"
#include "NtupleAnalysisUtils/Configuration/CanvasOptions.h"
#include "TEfficiency.h"
#include <vector> 
#include "TH1.h"
#include "TGraph.h"

/// This header defines a set of helper methods used to extract information
/// from histograms. This can include ranges or titles. 

namespace PlotUtils{

    /// ================================
    /// Helpers to extract axis ranges 
    /// ================================

    /// This extracts the x-axis range to fit a set of plots provided as the first
    /// arguments. Will try to find the interval fitting all of them, modified
    /// by the settings provided for the X axis within the canvas options. 
    template <class Histo> std::pair<double, double> getXRange (const std::vector<Plot<Histo>> & h, const CanvasOptions & opts=CanvasOptions());


    /// This extracts the y-axis range to fit a set of plots provided as the first
    /// arguments. Will try to find the interval fitting all of them, modified
    /// by the settings provided for the Y axis within the canvas options. 
    template <class Histo> std::pair<double, double> getYRange (const std::vector<Plot<Histo>> & h, const CanvasOptions & opts=CanvasOptions());

    /// This extracts the z-axis range to fit a set of plots provided as the first
    /// arguments. Will try to find the interval fitting all of them, modified
    /// by the settings provided for the Z axis within the canvas options. 
    template <class Histo> std::pair<double, double> getZRange (const std::vector<Plot<Histo>> & h, const CanvasOptions & opts=CanvasOptions());

    /// This extracts the ratio-axis range to fit a set of plots provided as the first
    /// arguments. Will try to find the interval fitting all of them, modified
    /// by the settings provided for the ratio axis within the canvas options. 
    template <class Histo> std::pair<double, double> getRatioRange (const std::vector<Plot<Histo>> & h, const CanvasOptions & opts=CanvasOptions());

    /// ================================
    /// Helpers to extract axis titles 
    /// ================================

    /// Will return the x axis title set in the plot, unless an override 
    /// is specified in the canvas options, in which case this will be returned. 
    template <class H> std::string getYtitle(const Plot<H> plot, 
                                             const CanvasOptions & opts = CanvasOptions());

    /// Will return the y axis title set in the plot, unless an override 
    /// is specified in the canvas options, in which case this will be returned. 
    template <class H> std::string getXtitle(const Plot<H> plot, 
                                             const CanvasOptions & opts = CanvasOptions());

    /// Will return the ratio axis title set in the plot, unless an override 
    /// is specified in the canvas options, in which case this will be returned. 
    template <class H> std::string getRatioTitle(const Plot<H>       plot, const CanvasOptions & opts = CanvasOptions());
    
    /// template specialisations for the X,Y axes in case of a TEfficiency.
    std::string getYtitle(const Plot<TEfficiency>     plot, const CanvasOptions & opts = CanvasOptions());
    std::string getXtitle(const Plot<TEfficiency>     plot, const CanvasOptions & opts = CanvasOptions());
    
    /// ================================
    /// Helpers for internal use
    /// ================================

    /// helper struct to determine the raw axis range of a given axis, and 
    /// how it was obtained
    struct rawAxisRange{
        double min{0};  /// axis minimum
        double max{0};  /// axis maximum
        bool fromBinning{false};    /// does axis limit come from binning or free range? 
    };

    /// Starting from an initial range representing the axis min/max, 
    /// perform the modifications configured
    /// by the user in an axis configuration to determine the final axis range. 
    /// These can for example include padding, hard-coding limits or symmetrisation. 
    std::pair<double,double> updateAxisRange(rawAxisRange histo_range, const AxisConfig & opts=AxisConfig());



    /// This method extracts the raw "min...max" interval for a given axis. 
    /// The axis to consider is determined by the second argument, where 
    /// 0 refers to the X-axis, 1 the Y-axis and 2 the Z-axis.
    /// The Min and Max attributes of the axis config are interpreted as min/max
    /// thresholds for the extrema search.
    
    template <class H> rawAxisRange getRawRange (std::vector<Plot<H>> p, size_t axis,const AxisConfig & axisCfg = AxisConfig());

    /// Template specialisations for extracting the range as described above. 
   rawAxisRange getRawRange (const TH1* p, size_t axis,const AxisConfig & axisCfg = AxisConfig());
   rawAxisRange getRawRange (const TGraph* p, size_t axis,const AxisConfig & axisCfg = AxisConfig());
   rawAxisRange getRawRange (const TEfficiency* p, size_t axis,const AxisConfig & axisCfg = AxisConfig());
    template <class H> rawAxisRange getRawRange (const H* p, size_t axis,const AxisConfig & axisCfg = AxisConfig());


    /// helper to access a given histogram axis of the passed object. 
    /// 0 refers to the X-axis, 1 the Y-axis and 2 the Z-axis.
    template <class H> const TAxis* getAxis(const H* p, size_t axis);

}

#include "HistoEvaluation.ixx"

#endif // NTAU__HISTOEVALUATION__H