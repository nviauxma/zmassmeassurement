#ifndef TRIGGER_PRESCALE_DUMPER_H
#define TRIGGER_PRESCALE_DUMPER_H
#include <NtauHelpers/Utils.h>

#include "MuonSagittaBias/TriggerAccess.h"
#include "MuonSagittaBias/TriggerAnalyzer.h"

/// Helper struct to store the prescales for each lumi block
struct PreScaleBlock {
public:
    PreScaleBlock(const unsigned int __run, const unsigned int __lumi, const float __scale);
    PreScaleBlock(const PreScaleBlock& other) = default;
    PreScaleBlock(PreScaleBlock&& other) = default;

    PreScaleBlock& operator=(const PreScaleBlock& other) = default;
    PreScaleBlock& operator=(PreScaleBlock&& other) = default;

    /// Ordering operator. Sort the prescale blocks according to their
    /// run and then lumi block number
    bool operator<(const PreScaleBlock& b) const;
    /// An invalid PreScale block are those who are actually having a prescale at all
    bool operator!() const;
    /// Comparison operator
    bool operator==(const PreScaleBlock& b) const {
        return run_number == b.run_number && lumi_block == b.lumi_block && pre_scale == b.pre_scale;
    }
    /// Run in which the data event is recorded
    unsigned int run_number{0};
    /// Exact luminosity block
    unsigned int lumi_block{0};
    /// Prescale taken from the data
    float pre_scale{0};
    /// No event in the luminosity block has been recorded
    /// the prescale is extrapolated based on the prescale configurations
    /// in this run
    bool is_augmented{false};
};
std::ostream& operator<<(std::ostream& os, const PreScaleBlock& block);

/// helper class to read the PreScales from a TriggerAnalyzer tree
/// to cache them during the event loop and then to finally dump them
/// into either plain txt or ROOT file format
/// The TrigPreScaleCache stores the prescales for a particular trigger only
///     the item must be an actual branch in the TriggerAnalyzer otherwise an
///     exception is thrown.
/// Lumi blocks without any prescale, i.e., trigPreScale <=1. are pruned from the
/// output
class TrigPreScaleCache {
public:
    TrigPreScaleCache(const std::string& trig_item);

    std::string trigger() const;

    void fill(TriggerAnalyzer& t);

    /// Dump the prescales & seen lumi-blocks to a calibration file
    ///  -> plain text format
    void write(const std::string& out_dir);
    /// Dump the prescale & seen lumi-block to a calibratin file
    /// --> Write a directory structured TTree
    void write(std::shared_ptr<TFile> out_file);

private:
    /// Dump the prescales to a calibration file
    ///  --> plain text format
    void dump_prescales(std::ofstream& ostr);
    /// --> Creates a subdirectory in the file and
    ///     saves there a TTree
    void dump_prescales(std::shared_ptr<TFile> out_file);
    void dump_lumiblocks(std::shared_ptr<TFile> out_file);

    void clean_previous_cache(unsigned int current_run);
    bool finalize();

    std::string m_item{};
    size_t m_item_pos{0};
    bool m_initialized{false};
    /// Sort the Prescale blocks according to the run number
    std::map<unsigned int, std::vector<PreScaleBlock>> m_prescalers{};
    std::map<unsigned int, std::set<unsigned int>> m_lumi_blocks{};
    std::map<unsigned int, bool> m_triggered_evt{};
    unsigned int m_last_run{INT_MAX};
};

/// TTree format of the Trigger prescales. One TTree is
/// created per trigger in a dedicated subdirectory.
/// The are called PreScales
class TriggerPreScaleNtuple : public NtupleBranchMgr {
public:
    TriggerPreScaleNtuple(TTree* t) : NtupleBranchMgr(t) {}
    // run number in which the trigger was recorded
    NtupleBranch<unsigned int> run_number{"runNumber", m_tree, this};
    /// First lumi block in which the prescale is valid
    NtupleBranch<uint16_t> first_block{"FirstLumiBlock", m_tree, this};
    /// Very last lumi block in which the prescale setting is valid
    NtupleBranch<uint16_t> last_block{"LastLumiBlock", m_tree, this};
    /// The prescale setting itself
    NtupleBranch<float> pre_scale{"preScale", m_tree, this};
    /// Is there one event recorded in this lumi block range at all
    NtupleBranch<bool> rec_evt{"OneEvtRecorded", m_tree, this};
};

class TriggerLumiBlockNtuple : public NtupleBranchMgr {
public:
    TriggerLumiBlockNtuple(TTree* t) : NtupleBranchMgr(t) {}

    NtupleBranch<unsigned int> run_number{"runNumber", m_tree, this};
    NtupleBranch<std::vector<uint16_t>> start_lumi{"startLumiBlock", m_tree, this};
    NtupleBranch<std::vector<uint16_t>> end_lumi{"endLumiBlock", m_tree, this};
    NtupleBranch<bool> trigger_fired{"triggeredRecord", m_tree, this};
};

#endif