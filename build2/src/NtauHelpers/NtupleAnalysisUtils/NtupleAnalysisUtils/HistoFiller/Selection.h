#ifndef SELECTION__H
#define SELECTION__H

#include <functional> 
#include <memory>
#include <string>

#include "NtupleAnalysisUtils/HistoFiller/HistoFillerInterfaces.h"


/// Very generic class for selection cuts.
/// 
/// The template parameter ProcessThis is the class of whatever we want to apply the selection on -
/// for example a MiniTree, an xAOD object, a primitive type or whatever you like! 
/// 
/// Note that the implementation of the cut itself is not done in here. 
/// We use the abstract std::function, which can support for example functors, lambda expressions, etc. 
/// The purpose of this class is to wrap this cut implementation with the logic for combining cuts
/// and with a unique identifier. 
/// 
/// Apart from an operator() to evaluate the outcome, we provide operators for the most common
/// logic operations when working with selection cuts.
/// 
/// We can combine cuts via AND or OR operators and invert them (for example, for an ABCD method implementations). 
/// 


template <class ProcessThis> class Selection: public ISelection {
public:
    /// Standard constructor for everyday use. 
    /// The argument should return an acceptance result for the current
    /// state of the input object. 
    Selection(std::function <bool(ProcessThis &)> toApply);
    /// Copy constructor
    Selection(const Selection<ProcessThis> & other);
    /// Default, provides a dummy selection accepting everything
    Selection();
    /// evaluate outcome of selection for a the current status of the input
    inline bool operator() (ProcessThis & input) const;
    /// combine with another selection via an OR
    Selection<ProcessThis> operator+ (const Selection<ProcessThis> & other) const ;
    Selection<ProcessThis> operator|| (const Selection<ProcessThis> & other) const;
    /// combine with another selection via an AND
    Selection<ProcessThis> operator* (const Selection<ProcessThis> & other) const;
    Selection<ProcessThis> operator&& (const Selection<ProcessThis> & other) const;
    /// invert a selection 
    Selection<ProcessThis> operator! ();

    /// implement ISelection interface 
    std::shared_ptr<ISelection> clone() const;
protected:
    std::function <bool(ProcessThis &)> m_cut;
    bool m_isDummy;
};


#include "Selection.ixx"

#endif // SELECTION__H
