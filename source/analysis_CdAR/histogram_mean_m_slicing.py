#############################################################################################################
# THIS SCRIPT:
#   -> uploads sin_theta_rho2 vs m_mu_mu histograms, calculates mean m_mu_mu for each bin-wide 
#      sin_theta_rho2 slice using a gaussian fit, and plots it.
#   NOTE_: the procedure is repeated for different eta ranges (i.e. for muon pairs in barrel, end-cap, etc)
#
# by Carolina de Almeida Rossi
# 14/07/21
############################################################################################################

import ROOT as r
import numpy as np
import matplotlib.pyplot as plt

r.gROOT.SetBatch(True)

#user setup:
is_mc = int(input("Data or MC? [0 = data / 1 = MC] "))
if is_mc == 1:
    root_file_name = "MC/SagittaTemplatesMC.root"
    folder_name = 'main_plots_mc'
    year = 'MC'
elif is_mc==0:
    year = str(input('year? (16, 17 or 18)  '))
    root_file_name = year+"/SagittaTemplates"+year+"quality1.root"
    folder_name = 'main_plots_data_'+year

#note: straight line fit currently turned off due to some missfits 
#note 2: currently using 2018 data through Johannes downloads due to some permission issues (being corrected)

#SWITCHES:=========================================================================
sigma_restrictions = True #thresholds: sigma=5 for Z, sigma=0.5 for J/Psi
crystalball = False #else, Gaussian
alpha_version = False #else, classic version of sin2thetarho
both_sin_theta_rho_2_versions = True #else, only the type of fit selected 
just_barrel = False #else, all selections

#J/Psi switches --------------------------------------------------------------------
J_psi = True #else, Z mass range
tree_m_mu = False #invariant_m from tree, else invariant_m_mu_mass using 4-momentums
massive_m_mu = True #invariant_m_mu_mass for massive muons, else massless muons
all_invariant_m_versions = True 
#-----------------------------------------------------------------------------------



#Parameters for plotting:
params = {
   'font.family': 'Arial',
   'mathtext.default': 'regular',
   'axes.labelsize': 20,
   'font.size': 20,
   'legend.fontsize': 12,
   'figure.titlesize': 25,
   'font.family': 'Arial',
   'xtick.labelsize': 18,
   'ytick.labelsize': 18,
   'figure.figsize': [20,9]
   } 
plt.rcParams.update(params)

looping_2 = 0 #LOPPING OVER J_Psi sin_theta_rho_2 invariant mass methods
if all_invariant_m_versions:
    tree_m_mu = True #start with tree_m_mu
if both_sin_theta_rho_2_versions:
    alpha_version = True #start with alpha version
if J_psi is False:
    all_invariant_m_versions = False

while looping_2 < 3:
    if J_psi: #TREE / MASSIVE / MASSLESS
        m_lower_threshold = 1
        sigma_threshold = 0.5
        if tree_m_mu:
            m_method = ' TREE'
            file_name = 'Plot_Jpsi_TREE'
        elif massive_m_mu:
            m_method = ' MASSIVE'
            file_name = 'Plot_Jpsi_MASSIVE'
        else: #MASSLESS:
            m_method = ' MASSLESS'
            file_name = 'Plot_Jpsi_MASSLESS'
        mass_range = '3_4'
        crystal_fit_params = [2,3,0.1,0.001,0.001]
    else:
        m_lower_threshold = 80
        m_method = ''
        sigma_threshold = 5
        file_name = 'Plot'
        mass_range = '61_121'
        crystal_fit_params = [3,90,3,2,4]


    looping = 0 #LOOPING OVER sin_theta_rho_2 AND sin_theta_rho_2_alpha METHODS   
    while looping < 2:
        # READING HISTOGRAMS =============================================================================================================
        if alpha_version:
            method = r'$\alpha$'
            my_file = r.TFile.Open("build/Plots_"+root_file_name, "READ")
            my_hist1 = my_file.Get("ID/Sin_theta_rho_2_m_alpha_{}/All_pairs/KinSel_Inclusive/MLL_{}/Template_SAGBIAS_0.000000_RADBIAS_0.000000_MAGBIAS_0.000000_SIGMA_0.000000".format(file_name, mass_range))
            my_hist0 = my_file.Get("ID/Sin_theta_rho_2_m_alpha_{}/Barrel_pairs/KinSel_Inclusive/MLL_{}/Template_SAGBIAS_0.000000_RADBIAS_0.000000_MAGBIAS_0.000000_SIGMA_0.000000".format(file_name, mass_range))
            my_hist2 = my_file.Get("ID/Sin_theta_rho_2_m_alpha_{}/End_cap_pairs/KinSel_Inclusive/MLL_{}/Template_SAGBIAS_0.000000_RADBIAS_0.000000_MAGBIAS_0.000000_SIGMA_0.000000".format(file_name, mass_range))
            my_hist3 = my_file.Get("ID/Sin_theta_rho_2_m_alpha_{}/Barrel_endcap_pairs/KinSel_Inclusive/MLL_{}/Template_SAGBIAS_0.000000_RADBIAS_0.000000_MAGBIAS_0.000000_SIGMA_0.000000".format(file_name, mass_range))
        else:
            method = 'std'
            my_file = r.TFile.Open("build/Plots_"+root_file_name, "READ")
            my_hist1 = my_file.Get("ID/Sin_theta_rho_2_m_{}/All_pairs/KinSel_Inclusive/MLL_{}/Template_SAGBIAS_0.000000_RADBIAS_0.000000_MAGBIAS_0.000000_SIGMA_0.000000".format(file_name, mass_range))
            my_hist0 = my_file.Get("ID/Sin_theta_rho_2_m_{}/Barrel_pairs/KinSel_Inclusive/MLL_{}/Template_SAGBIAS_0.000000_RADBIAS_0.000000_MAGBIAS_0.000000_SIGMA_0.000000".format(file_name, mass_range))
            my_hist2 = my_file.Get("ID/Sin_theta_rho_2_m_{}/End_cap_pairs/KinSel_Inclusive/MLL_{}/Template_SAGBIAS_0.000000_RADBIAS_0.000000_MAGBIAS_0.000000_SIGMA_0.000000".format(file_name, mass_range))
            my_hist3 = my_file.Get("ID/Sin_theta_rho_2_m_{}/Barrel_endcap_pairs/KinSel_Inclusive/MLL_{}/Template_SAGBIAS_0.000000_RADBIAS_0.000000_MAGBIAS_0.000000_SIGMA_0.000000".format(file_name, mass_range))

        #list of hists to loop ove in analysis and naming configs:
        
        if just_barrel:
            hists = [my_hist0]
        else:
            hists = [my_hist0, my_hist1, my_hist2, my_hist3]
            #open figure to plot all histograms simultaneously:
            joint_plot = plt.figure('All_Eta_Selections')
            plt.title(r'All $\eta$ selections - ' + year)
            plt.xlabel(r'$sin^{2}(\theta_{\rho})$')
            plt.ylabel(r'<$m_{\mu\mu}$> (GeV)')  
        hists_titles = [r'Barrel / Barrel $\mu$-pairs ',r'All $\mu$-pairs ', r'End-cap / End-cap $\mu$-pairs ', r'Barrel / End-cap $\mu$-pairs '] 
        hists_pdf_titles = ['from_barrel_pairs_hist','from_all_pairs_hist', 'from_endcap_pairs_hist', 'from_endcap_barrel_pairs_hist']
        #============================================================================================================

        #another on for all chi squareds:
        chi2_plot = plt.figure('Chi_squared') 
        plt.xlabel(r'$m_{\mu\mu}$ slice number')
        plt.ylabel(r'$\chi_{2}$') 

        #start m slicing, looping over all available histograms:
        for my_hist in hists:

            print("HIST_{}-------------------------------------------------------------------------------------------------".format(hists.index(my_hist)))
            print("\n\nThere are {} sin_sqr_theta bins \n".format(my_hist.GetNbinsX()))

            if crystalball:
                #(alpha, n, sigma, mean)
                fit_function = r.TF1( 'Gaussian Fit Function', 'crystalball', 0, 120)
                fit_function.SetParameters(crystal_fit_params[0],crystal_fit_params[1],crystal_fit_params[2],crystal_fit_params[3],crystal_fit_params[4])
                index_mean=1
                dofs = 5

            else: #Gaussian
                #gauss(0) has params [0,1,2], y = [0]*exp(-0.5*((x-[1])/[2])**2)
                fit_function = r.TF1( 'Gaussian Fit Function', 'gaus(0)', 0, 120)
                index_mean=1
                dofs = 3

            
            print("STARTING SLICING AND FITTING...\n")
            my_hist_clone = my_hist.Clone('my_hist_clone')
            sin_theta2_centers = []
            generic_yslice = my_hist_clone.ProjectionX('generic_yslice')
            mean_m_vals = []
            mean_m_errs = []
            fit_sigmas = []
            fit_sigmas_errs = []
            chi_squareds = []
            for x in range(my_hist.GetNbinsX()): #range is number of x bins
                x_min = x
                x_max = x + 1
                current_xslice = my_hist.ProjectionY('current_slice',x_min,x_max)
                no_counts = current_xslice.GetCumulative().GetMaximum()
                
                if no_counts <1000: 
                    #DEFINE MIN NO OF TOTAL COUNTS TO GET RID OF (ALMOST) EMPTY SLICES
                    print("{} readings in slice {} - Skip it".format(no_counts,x))
                    chi_squareds.append(0.0)
                    continue

                else:
                    #ELSE, STORE SLICE AND FIT IT
                    print("\n\nSlice {}".format(x))
                    current_xslice.Fit(fit_function)
                    current_xslice.SetLineColor(r.kBlack)
                    mean_m = fit_function.GetParameter(index_mean)
                    if mean_m < m_lower_threshold:
                        print(mean_m,' - dimuon in wrong invariant mass range.')
                        continue                    
                    if sigma_restrictions:
                        current_sigma = fit_function.GetParameter(2)
                        if current_sigma<=sigma_threshold:
                            sin_theta2_centers.append(generic_yslice.GetBinCenter(x))
                            fit_sigmas.append(current_sigma)
                            fit_sigmas_errs.append(fit_function.GetParError(2)) 
                            mean_m_vals.append(mean_m)
                            mean_m_errs.append(fit_function.GetParError(index_mean))
                        else:
                            print('\n ################### SIGMA RESTRICTIONS APPLIED: sigma = {}\n'.format(current_sigma))
                            continue

                    else:
                        sin_theta2_centers.append(generic_yslice.GetBinCenter(x))
                        fit_sigmas.append(fit_function.GetParameter(2))
                        fit_sigmas_errs.append(fit_function.GetParError(2)) 
                        mean_m_vals.append(mean_m)
                        mean_m_errs.append(fit_function.GetParError(index_mean))

                    chi_squareds.append(fit_function.GetChisquare()/(no_counts-dofs))
                    hist_canv = r.TCanvas("Fitted Sliced Histograms", "Slice", 800,600)
                    hist_canv.cd() 
                    current_xslice.Draw("HIST E2")
                    fit_function.Draw('same')
                    hist_canv.SaveAs("analysis_CdAR/Outcome_Plots/{}/Fitted_Histogram_Slices/Slice_{}.pdf".format(hists_pdf_titles[hists.index(my_hist)],x))

            #take sin_theta2_errors to be half of the constant bin width:
            sin_theta2_errors = np.empty(len(mean_m_vals))
            sin_theta2_errors.fill(generic_yslice.GetBinWidth(0)/2)

            if len(mean_m_vals)==0:
                print("No mean m values found for these sigma restrictions.")
            else:

                #straight line fit: y=ax+b coeffs=[a,b]
                coeffs, cov = np.polyfit(sin_theta2_centers, mean_m_vals, 1, cov=True)
                fitted_m_vals = []
                for sin in sin_theta2_centers:
                    fitted_m_vals.append(coeffs[0]*sin + coeffs[1])

                #plot each case in separate figure:
                plot = plt.figure('Hist_{}'.format(hists.index(my_hist)))
                ax = plt.subplot(111)
                box = ax.get_position()
                ax.set_position([box.x0, box.y0, box.width*0.95, box.height])
                plt.xlabel(r'$sin^{2}(\theta_{\rho})$')
                plt.ylabel(r'<$m_{\mu\mu}$> (GeV)') 
                plt.errorbar(sin_theta2_centers, mean_m_vals, yerr = mean_m_errs , xerr = sin_theta2_errors, label = method + m_method)
                plt.plot(sin_theta2_centers, fitted_m_vals,'--', label = r'$sin^{2}(\theta_{\rho})$ = '+ str(round(coeffs[0],4)) + u'\u00b1' +str(round(np.sqrt(cov[0,0]),4))+ r'<$m_{\mu\mu}$> +' + str(round(coeffs[1],4)) + u'\u00b1' + str(round(np.sqrt(cov[1,1]),4)) + ' ({})'.format(method))
                plt.title(hists_titles[hists.index(my_hist)]+ year)
                plt.legend(bbox_to_anchor=(1.,1.), loc = 'best')
                plt.plot(sin_theta2_centers, mean_m_vals, 'x')
                plt.savefig('analysis_CdAR/Outcome_Plots/'+folder_name+'/{}'.format(hists_pdf_titles[hists.index(my_hist)]))
                #print it:
                #print('\n\nBARREL-BARREL STRAIGHT LINE FIT: ############################# ')
                #print("FIT: y = {}x + {}".format(*coeffs))
                #print("COV MATRIX:", cov)

                if just_barrel is False:
                    #add plot to joint figure:
                    plt.figure('All_Eta_Selections') 
                    ax = plt.subplot(111)
                    box = ax.get_position()
                    ax.set_position([box.x0, box.y0, box.width*0.99, box.height])
                    plt.errorbar(sin_theta2_centers, mean_m_vals, yerr = mean_m_errs , xerr = sin_theta2_errors, label = hists_titles[hists.index(my_hist)] + method + m_method)
                    plt.legend(bbox_to_anchor=(1.,1.), loc = 'best')
                    plt.plot(sin_theta2_centers, mean_m_vals, 'x')

                #plot chi squareds:
                plt.figure('Chi_squared')
                ax = plt.subplot(111)
                box = ax.get_position()
                ax.set_position([box.x0, box.y0, box.width*0.99, box.height])
                plt.plot(chi_squareds, label = hists_titles[hists.index(my_hist)] + method + m_method)
                plt.legend(bbox_to_anchor=(1.,1.), loc = 'best')
            


                #Special barrel-barrel analysis:##########################################
                if hists_pdf_titles[hists.index(my_hist)]=='from_barrel_pairs_hist':
                    #FWHM sin2thetarho dependence in barrel-barrel case: (FULL THETA RANGE)
                    #works as expected: distinction between 
                    #-----close to 1, we have two back-to-back muons, and a Z boson with small dilepton pT, pT(Z) < m(Z)/2
                    #-----in the small sin2thetarho region, we have instead a boosted dilepton system, where the two leptons are not anymore back-to-back, pT(Z) > m(Z)/2
                    fwhm = []
                    fwhm_errs = []
                    for j in range(len(fit_sigmas)):
                        fwhm.append(fit_sigmas[j]*2*np.sqrt(2*np.log(2)))
                        fwhm_errs.append(fit_sigmas_errs[j]*2*np.sqrt(2*np.log(2)))
                    barrel_barrel_FWHM_sin_dependence = plt.figure('barrel_barrel_FWHM_sin_dependence')
                    ax = plt.subplot(111)
                    box = ax.get_position()
                    ax.set_position([box.x0, box.y0, box.width*0.99, box.height])
                    plt.xlabel(r'$sin^{2}(\theta_{\rho})$')
                    plt.ylabel(r'$FWHM_{<m_{\mu\mu}> (GeV)}$') 
                    plt.errorbar(sin_theta2_centers, fwhm, yerr = fwhm_errs , xerr = sin_theta2_errors, label = method+ m_method)
                    plt.title(r'FWHM - $sin^{2}(\theta_{\rho})$ dependence'+ year)
                    plt.legend(bbox_to_anchor=(1.,1.), loc = 'best')
                    plt.plot(sin_theta2_centers, fwhm, 'x')
                    plt.savefig('analysis_CdAR/Outcome_Plots/'+folder_name+'/{}'.format('barrel_barrel_FWHM_sin_dependence'))  
                    
                    #Look at sin2thetarho>0.5 and barrel barel only:
                    #(lengthy cutting procedure, but just making sure no mistakes if no bin at exactly .5 in the future)
                    sin_theta2_centers_cut=[]
                    mean_m_vals_cut=[]
                    mean_m_errs_cut=[]
                    for i in range(len(sin_theta2_centers)):
                        sin = sin_theta2_centers[i]
                        if sin>=0.5:
                            sin_theta2_centers_cut.append(sin)
                            mean_m_vals_cut.append(mean_m_vals[i])
                            mean_m_errs_cut.append(mean_m_errs[i])
                    sin_theta2_errors_cut = sin_theta2_errors[:len(sin_theta2_centers_cut)]


                    #plot it:
                    if J_psi is False:
                        barrel_barrel_plot = plt.figure('barrel_barrel_restricted_range_plot')
                        ax = plt.subplot(111)
                        box = ax.get_position()
                        ax.set_position([box.x0, box.y0, box.width*0.99, box.height])                        
                        plt.xlabel(r'$sin^{2}(\theta_{\rho})$')
                        plt.ylabel(r'<$m_{\mu\mu}$> (GeV)') 
                        plt.errorbar(sin_theta2_centers_cut, mean_m_vals_cut, yerr = mean_m_errs_cut , xerr = sin_theta2_errors_cut, label = method + m_method)
                        plt.title(r'barrel $\mu$-pairs - Restricted Range '+ year)
                        plt.legend(bbox_to_anchor=(1.,1.), loc = 'best')
                        plt.plot(sin_theta2_centers_cut, mean_m_vals_cut, 'x')
                        plt.savefig('analysis_CdAR/Outcome_Plots/{}/{}'.format(hists_pdf_titles[hists.index(my_hist)],'barrel_barrel_restricted_range_plot'))
                        
        
        if just_barrel is False:
            if crystalball:
                plt.figure('All_Eta_Selections')
                plt.savefig('analysis_CdAR/Outcome_Plots/'+folder_name+'/All_Eta_Selections_CrystalBall')
                plt.figure('Chi_squared')
                plt.savefig('analysis_CdAR/Outcome_Plots/'+folder_name+'/Chi_Squareds_CristalBall')
            else:
                plt.figure('All_Eta_Selections')
                plt.savefig('analysis_CdAR/Outcome_Plots/'+folder_name+'/All_Eta_Selections_Gaussian')
                plt.figure('Chi_squared')
                plt.savefig('analysis_CdAR/Outcome_Plots/'+folder_name+'/Chi_Squareds_Gaussian')

        if both_sin_theta_rho_2_versions:
            looping +=1 
            if alpha_version:
                alpha_version = False
            else:
                alpha_version = True
        else:
            looping = 2
    
    if all_invariant_m_versions:
        looping_2 +=1
        if alpha_version:
            alpha_version = False
            tree_m_mu = True
        if tree_m_mu:
            tree_m_mu = False
            massive_m_mu = True
        elif massive_m_mu:
            massive_m_mu = False
    else:
        looping_2 = 3

#plt.show()

