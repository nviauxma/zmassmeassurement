#!/usr/bin/env python
import os, sys, time, ROOT, logging
from ClusterSubmission.RucioListBuilder import downloadDataSets, RUCIO_RSE
from ClusterSubmission.Utils import ReadListFromFile, setupBatchSubmitArgParser, ExecuteCommands, CreateDirectory, setup_engine, id_generator, WriteList, CheckRucioSetup, ResolvePath, ClearFromDuplicates
#from ClusterSubmission.UpdateSampleLists import GetAMITagsMC
#from ClusterSubmission.AMIDataBase import getAmiClient
from ClusterSubmission.DatasetDownloader import RucioContainerHandler

############################################################################
#       Logical sample names                                               #
############################################################################
rangeID = lambda start, end: range(start, end + 1)
SampleNames = {
    "Zmumu": [361107],
    "Ztautau": [361108, 361669],
    "TTbar": [410470, 410471],  # usually, the dilepton sample (410009) is used
    "TTbar_dil": [410472],
    "Jpsimu2p5mu2p5": [300000],
    "Jpsimu2mu2": [424101],
    "Jpsimu4mu4": [424100],
    "Jpsimu4mu6": [424108],
    "bbJPsimu3p5mu3p5": [300203],
    "Wjets": [361101, 361104, 361105, 361102],
    "DrellYan": rangeID(301020, 301038) + [361666, 361667],
    "VZxxmumu": [361601, 361603, 361604, 361607, 361610, 361084, 361086],
    "WWlvlv": [361600],
    "VVlnmm": [303062],
    "VVllmm": [303054],
    "VVmnmn": [303046],
    "QCD": [361250, 361251],
    "Sherpa221_Zmumu": rangeID(364198, 364203) + rangeID(364100, 364113) + [344441],
    "Sherpa221_Zee": rangeID(364204, 364209) + rangeID(364114, 364127) + [344442],
    "Sherpa221_Ztautau": rangeID(364210, 364215) + rangeID(364128, 364141) + [344443],
    "Upsilon1S": [424102],
    "Upsilon2S": [424105],
    "Upsilon3S": [424106],
    "Sherpa2211_Zmumu": rangeID(700323, 700325),
    "Sherpa2211_Ztautau": rangeID(700326, 700331),
    "Sherpa2211_Wmunu": rangeID(700341, 700343),
    "Sherpa2211_Wtaunu": rangeID(700344, 700346),
    "Jpsi2S": [800384],
    "Jpsi1S_mu6mu4": [801115],
}


def DSIDFromName(DataSet):
    DS = DataSet
    Perf = "perf-muons"
    Det = "det-muon"
    while DS.find(Perf) != -1:
        DS = DS[DS.find(Perf) + len(Perf) + 1:]
    while DS.find(Det) != -1:
        DS = DS[DS.find(Det) + len(Det) + 1:]

    if not "mc" in DS.split(".")[0]: return -1
    return int(DS.split(".")[1])


def GetPeriodFromName(DataSet):
    DS = DataSet

    scopes = ["perf-muons", "det-muon"]
    if DS.find(":") != -1: scopes += [DS[:DS.find(":")]]
    for s in scopes:
        while DS.find(s) != -1:
            DS = DS[DS.find(s) + len(s) + 1:]

    if not "data" in DS.split(".")[0]: return None

    split_ds = DS.split(".")
    y = int([s for s in split_ds if "13TeV" in s][0][4:6]) + 2000
    #y = int(DS.split(".")[0][4:6]) + 2000
    return "data_%d" % (y)


def GetLogicalSample(DS):
    DSID = DSIDFromName(DS)
    if DSID == -1: return GetPeriodFromName(DS)
    for Name, DataSets in SampleNames.iteritems():
        if DSID in DataSets: return Name
    return DS


class MuonTPMergeSample(RucioContainerHandler):
    def __init__(
        self,
        dest_rse,
        download=False,
        merge=False,
        download_dir="/tmp/download",
        destination_dir="/tmp",
        cluster_engine=None,
        max_merged_size=1 * 1024 * 1024 * 1024,
        protocol="root",
        hold_jobs=[],
        logical_name="",
        ### Directory where the config file is going to be stored
        config_dir="",
    ):
        RucioContainerHandler.__init__(self,
                                       dest_rse=dest_rse,
                                       download=download,
                                       merge=merge,
                                       download_dir=download_dir,
                                       destination_dir=destination_dir,
                                       cluster_engine=cluster_engine,
                                       max_merged_size=max_merged_size,
                                       protocol=protocol,
                                       hold_jobs=hold_jobs,
                                       logical_name=logical_name)
        self.__config_dir = config_dir
        #############################################
        #       Extract the meta-data information
        #############################################
        self.__channels = []
        self.__smp_names = []
        self.__runs = []
        self.__mc_campaigns = []
        self.__is_data = -1

    def load_meta(self):
        if self.__is_data != -1: return True
        try:
            db = GetNormalizationDB(self.root_files(), True)
        except:
            logging.warning("Could not setup metadata base for sample %s" % (self.logical_name()))
            return False
        self.__is_data = db.isData()
        self.__channels = [dsid for dsid in db.GetListOfMCSamples()]
        self.__runs = [run for run in db.GetRunNumbers()]

        if not self.__is_data:
            self.__mc_campaigns = getPrwPeriods()
            self.__runs = ClearFromDuplicates([mc_prw[1] for mc_prw in self.__mc_campaigns])
            self.__smp_names = [db.GetSampleName(dsid) for dsid in self.__channels]
        return True

    def write_cfg_files(self):
        ### load the meta-data first
        if len(self.root_files()) == 0: return
        # Obtain the prw config files
        ConfigFiles = []

        CreateDirectory(self.__config_dir, False)
        out_path = "%s/%s.conf" % (self.__config_dir, self.logical_name().replace(
            ".", "_")) if not self.__is_data else "%s/%s.conf" % (self.__config_dir, self.logical_name().replace(".", "_"))
        with open(out_path, "w") as TheFile:
            TheFile.write("#########################################################################################\n")
            TheFile.write("#File generated at %s by %s\n" % (time.strftime("%Y-%m-%d %H:%M:%S"), os.getenv("USER")))
            TheFile.write("#########################################################################################\n")
            TheFile.write("#Used rucio containers:\n")
            for C in self.rucio_container():
                TheFile.write("#    --- %s\n" % (C))
            TheFile.write("#########################################################################################\n")
            if not self.__is_data:
                TheFile.write("# Monte Carlo samples in this config file:\n")
                for i, dsid in enumerate(self.__channels):
                    TheFile.write("#     --- %d (%s)\n" % (dsid, self.__smp_names[i]))
            else:
                TheFile.write("# Data-taking runs in this config file:\n")
                for run in self.__runs:
                    TheFile.write("#     --- %d\n" % (run))
            TheFile.write("#########################################################################################\n")
            TheFile.write("#InputFiles:\n")
            for F in sorted(self.merged_files()):
                TheFile.write("%s\n" % (F))
            TheFile.write("#########################################################################################\n")
            TheFile.write("#########################################################################################\n")
            TheFile.close()


if __name__ == '__main__':

    CheckRucioSetup()
    #getAmiClient()
    # list disk (e.g. CAF data):     python ListDisk.py -r CERN-PROD_PERF-MUONS -p data17_13TeV. Main MCPTP m1824_c1119
    # create input config from list: python CreateInputConfigs.py -i <list> -o <folder name> --readFromRSE -r CERN-PROD_PERF-MUONS

    parser = setupBatchSubmitArgParser()
    parser.set_defaults(MergeTime="03:59:59")
    parser.set_defaults(Merge_vmem=16000)
    parser.add_argument('--HoldJob', default=[], nargs="+", help='Specify job names which should be finished before your job is starting. ')
    parser.add_argument('--input', '-i', dest='indir', help='Folder with files', required=True, nargs='+', default=[])
    parser.add_argument('--output', '-o', dest='outdir', help='Output directory to put file list(s) into', required=True)

    ###############################
    #       read mode
    ###############################
    parser.add_argument('--readROOTFiles',
                        help='Directly read from folder containing merged root files',
                        action='store_true',
                        default=False)
    parser.add_argument('--readFromRSE', help='Directly read from folder containing merged root files', action='store_true', default=False)
    parser.add_argument('--mergeFromRSE',
                        help='Submit a merge job to the cluster first and write the final paths to the config files',
                        action='store_true',
                        default=False)

    parser.add_argument('--downloadBeforeMerge',
                        help='Download the datasets before they are merged together',
                        action='store_true',
                        default=False)
    #parser.add_argument('--makeCfgPerYear', help='Only the configs per data-taking year are made', action='store_true', default=True)

    #################################################
    #       options to create the input config      #
    #################################################
    #parser.add_argument('--isLowMu',
    #                    help='Specify whether you want to create configs for the low mu runs',
    #                    action='store_true',
    #                    default=False)
    #parser.add_argument('--noGRL', help='Do not add a GRL to the input configs', action='store_true', default=False)
    #parser.add_argument('--noIlumi', help='Do not add a ilumicalc file to the input configs', action='store_true', default=False)
    ###############################################
    #       Arguments to read from RSE
    ###############################################
    parser.add_argument('--rse', '-r', '-R', '--RSE', help='specify RSE storage element which should be read', default=RUCIO_RSE)
    parser.add_argument('--protocols', '-p', '-P', help="Specify the protocols you want to use for the file list creation.", default="root")
    Options = parser.parse_args()

    DataSetFiles = []
    if not Options.readROOTFiles:
        for In in Options.indir:
            DataSetFiles += [
                x for x in ReadListFromFile(In) if not x.endswith("EXT1") and  ### Usually these are the SCALE NTUPLES
                not x.endswith("MCPCALIB")  ### Usually these are the SCALE NTUPLES
            ]
    else:
        for In in Options.indir:
            if os.path.isfile(In): DataSetFiles.append(In)
            elif not os.path.isdir(In):
                logging.error('%s is neither a file nor a directory!' % (In))
                continue
            for F in os.listdir(In):
                DataSetFiles.append(In + '/' + F)

    if len(DataSetFiles) == 0: sys.exit(1)
    submit_engine = setup_engine(Options)
    # copy the merged files in a separate MCP directory
    if Options.outdir.endswith("/"): Options.outdir = Options.outdir[:-1]
    finalOutDir = "%s/MCP_InFiles/%s/" % (Options.BaseFolder, Options.outdir[Options.outdir.rfind("/") + 1:])
    file_mergers = {}

    download_dir = "/%s/MCP_InFiles/to_download/" % (Options.BaseFolder)
    if Options.downloadBeforeMerge:
        downloadDataSets(InputDatasets=DataSetFiles, Destination=download_dir, RSE="", use_singularity=True)
    for DS in DataSetFiles:
        Key = GetLogicalSample(DS)
        if not Key in file_mergers.iterkeys():
            file_mergers[Key] = MuonTPMergeSample(logical_name=Key,
                                                  dest_rse=Options.rse,
                                                  download=Options.downloadBeforeMerge,
                                                  merge=Options.mergeFromRSE,
                                                  download_dir=download_dir,
                                                  destination_dir=finalOutDir,
                                                  cluster_engine=submit_engine,
                                                  protocol=Options.protocols,
                                                  config_dir=Options.outdir)
        file_mergers[Key].add_container(DS)

    if Options.mergeFromRSE and len(file_mergers) > 0:
        jobs_prepared = []
        hold_jobs = []
        n_files = 0
        ### Prepare the merging of the jobs
        for merging in file_mergers.itervalues():
            merging.set_hold_jobs(hold_jobs + Options.HoldJob)
            if not merging.submit_merge(): exit(1)
            jobs_prepared += [submit_engine.subjob_name("Move-%s" % (merging.logical_name()))]
            n_files += len(merging.merged_files())
            if n_files > 10:
                n_files = 0
                hold_jobs = [h for h in jobs_prepared]
                jobs_prepared = []
        if Options.downloadBeforeMerge:
            submit_engine.submit_clean_job(hold_jobs=jobs_prepared + hold_jobs, to_clean=[download_dir], sub_job="clean_primary")
        submit_engine.submit_clean_all(jobs_prepared + hold_jobs)
        submit_engine.finish()

    for ds in file_mergers.itervalues():
        ds.write_cfg_files()
