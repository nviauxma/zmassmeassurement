#ifndef NTAUHELPERS_OUTFILEWRITER_H
#define NTAUHELPERS_OUTFILEWRITER_H

#include <NtupleAnalysisUtils/NTAUTopLevelIncludes.h>
#include <TFile.h>
#include <TH1.h>

#include <map>
#include <memory>
#include <string>
///###################################################################
///     Simple helper class which
///     takes care that Plots are written
///     to the TFile. Any kind of Plot is supported
///    The plots are automatically written as soon as
///    the object is destoryed. Plots with duplicate file-names
///
///#############################################

template <class T> class PlotWriter {
public:
    /// Usual constructor with a TFile
    PlotWriter(const std::shared_ptr<TFile>& out_file);
    /// Alternative constructor using the string_path
    PlotWriter(const std::string& out_path);
    // finally we sould like to construct the out_file writer from another type
    template <class O> PlotWriter(const PlotWriter<O>& other);
    // Returns the associated TFile pointer
    std::shared_ptr<TFile> get() const;
    /// Add the plot to the list of savables. The save_as is the final
    /// destiniation inside the ROOT file and must not be empty or
    /// sssigned twice
    void add_plot(const Plot<T>& pl, const std::string& save_as);

    void add_plot(const std::shared_ptr<T>& pl, const std::string& save_as);

    /// Destructor calling the populate methods and then
    /// also performing the actual write out of the plots;
    ~PlotWriter();

private:
    std::shared_ptr<TFile> m_out_file;
    std::map<std::string, Plot<T>> m_to_write;
};
#include <NtauHelpers/OutFileWriter.ixx>
#endif
