#include <MuonSagittaBias/ArgumentParser.h>
#include <MuonSagittaBias/PhaseSpaceCuts.h>
#include <MuonSagittaBias/PlotProvider.h>
#include <MuonSagittaBias/SagittaBiasAnalyzer.h>
#include <MuonSagittaBias/SlicedPlotProvider.h>
#include <NtauHelpers/PlotPostProcessors.h>
std::vector<Plot<TH1>> merge_charges(const std::vector<Plot<TH1>>& pos_plots, const std::vector<Plot<TH1>>& neg_plots);
std::vector<RatioEntry> make_contributions(size_t n);
std::vector<std::shared_ptr<IPlotPopulator<TH1>>> retrievePopulators(const std::vector<Plot<TH1>>& plots);

void assign_plot_labels(const SlicedPlotProvider& slicer, std::vector<Plot<TH1>>& plots);

int main(int argc, char* argv[]) {
    /// Standard piece of code to setup the analysis...
    ArgumentParser parser;
    parser.summary_pdf("AllTRTPlots").make_root_file(false);

    if (!parser.parse_args(argc, argv)) return EXIT_FAILURE;

    Sample<SagittaBiasAnalyzer> sample_to_process = parser.prepare_sample<SagittaBiasAnalyzer>();

    CanvasOptions canvas_opts = CanvasOptions()
                                    .RatioAxis(AxisConfig().Title("efficiency").ExtraTitleOffset(0.1))
                                    .YAxis(AxisConfig().Title("Entries").ExtraTitleOffset(0.4).TopPadding(0.7))
                                    // .XAxis(AxisConfig().ExtraTitleOffset(1.3))
                                    .LabelStatusTag("Simulation Internal")
                                    .OutputDir(parser.out_dir());

    std::shared_ptr<MultiPagePdfHandle> multi_page = PlotUtils::startMultiPagePdfFile(parser.summary_pdf(), canvas_opts);

    PhaseSpaceCuts within_trt("WithinTRT", "|#eta|<2.05", [](SagittaBiasAnalyzer& t, int trk_type) -> bool {
        return std::max(std::abs(t.eta(trk_type, SagittaBiasAnalyzer::LepCharge::Minus)),
                        std::abs(t.eta(trk_type, SagittaBiasAnalyzer::LepCharge::Plus))) < 2.05;
    });
    const std::pair<float, float> no_mass = {1, 1.e12};
    /// Store the fillers in pairs representing the positive and negative
    /// lepton filler. The first entry is the filler for the positive lepton
    /// and the second for the negative lepton
    std::vector<std::pair<PlotFiller, PlotFiller>> lepton_filler{};

    /// Number of TRT hits
    lepton_filler +=
        std::make_pair(PlotFiller{[](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
                                      return h->GetXaxis()->FindBin(t.numberOfTRTHits(trk_type, SagittaBiasAnalyzer::LepCharge::Plus));
                                  },
                                  PlotUtils::HistoBinConfig().name("TRTHits_Plus").x_title("N_{TRT}").x_min(0).x_max(50).x_num_bins(50)},

                       PlotFiller{[](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
                                      return h->GetXaxis()->FindBin(t.numberOfTRTHits(trk_type, SagittaBiasAnalyzer::LepCharge::Minus));
                                  },
                                  PlotUtils::HistoBinConfig().name("TRTHits_Minus").x_title("N_{TRT}").x_min(0).x_max(50).x_num_bins(50)});
    /// Number of TRT outliers
    lepton_filler += std::make_pair(
        PlotFiller{[](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
                       return h->GetXaxis()->FindBin(t.numberOfTRTOutliers(trk_type, SagittaBiasAnalyzer::LepCharge::Plus));
                   },
                   PlotUtils::HistoBinConfig().name("TRTOutLiers_Plus").x_title("N_{TRT}^{outlier}").x_min(0).x_max(10).x_num_bins(10)},

        PlotFiller{[](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
                       return h->GetXaxis()->FindBin(t.numberOfTRTOutliers(trk_type, SagittaBiasAnalyzer::LepCharge::Minus));
                   },
                   PlotUtils::HistoBinConfig().name("TRTOutLiers_Minus").x_title("N_{TRT}^{outlier}").x_min(0).x_max(10).x_num_bins(10)});

    /// p_{T} distirbutions
    lepton_filler +=
        std::make_pair(PlotFiller{[](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
                                      return h->GetXaxis()->FindBin(t.pt(trk_type, SagittaBiasAnalyzer::LepCharge::Plus));
                                  },
                                  PlotUtils::HistoBinConfig().name("Pt_Plus").x_title("p_{T} [GeV]").x_min(0).x_max(100).x_num_bins(50)},

                       PlotFiller{[](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
                                      return h->GetXaxis()->FindBin(t.pt(trk_type, SagittaBiasAnalyzer::LepCharge::Minus));
                                  },
                                  PlotUtils::HistoBinConfig().name("Pt_Minus").x_title("p_{T} [GeV]").x_min(0).x_max(100).x_num_bins(50)});

    // The q over p significance as the object we actually we want to measure
    lepton_filler += std::make_pair(
        PlotFiller{[](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
                       return h->GetXaxis()->FindBin(std::abs(t.err_q_over_p(trk_type, SagittaBiasAnalyzer::LepCharge::Plus) /
                                                              t.q_over_p(trk_type, SagittaBiasAnalyzer::LepCharge::Plus)));
                   },
                   PlotUtils::HistoBinConfig().name("RelQoverP_Plus").x_title("#sigma(q/p) / (q/p)").x_min(0).x_max(0.05).x_num_bins(25)},
        PlotFiller{[](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
                       return h->GetXaxis()->FindBin(std::abs(t.err_q_over_p(trk_type, SagittaBiasAnalyzer::LepCharge::Minus) /
                                                              t.q_over_p(trk_type, SagittaBiasAnalyzer::LepCharge::Minus)));
                   },
                   PlotUtils::HistoBinConfig().name("RelQoverP_Minus").x_title("#sigma(q/p) /(q/p)").x_min(0).x_max(0.05).x_num_bins(25)});

    /// Look at the q_over_p against eta
    lepton_filler += std::make_pair(
        PlotFiller{[](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
                       return h->GetBin(h->GetXaxis()->FindBin(t.eta(trk_type, SagittaBiasAnalyzer::LepCharge::Plus)),
                                        h->GetYaxis()->FindBin(std::abs(t.err_q_over_p(trk_type, SagittaBiasAnalyzer::LepCharge::Plus) /
                                                                        t.q_over_p(trk_type, SagittaBiasAnalyzer::LepCharge::Plus))));
                   },
                   PlotUtils::HistoBinConfig()
                       .name("Eta_RelQoverP_Plus")
                       .y_title("#sigma(q/p) / (q/p)")
                       .y_min(0)
                       .y_max(0.25)
                       .y_num_bins(250)
                       .x_min(-2.05)
                       .x_max(2.05)
                       .x_num_bins(41)
                       .x_title("#eta")},
        PlotFiller{[](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
                       return h->GetBin(h->GetXaxis()->FindBin(t.eta(trk_type, SagittaBiasAnalyzer::LepCharge::Minus)),
                                        h->GetYaxis()->FindBin(std::abs(t.err_q_over_p(trk_type, SagittaBiasAnalyzer::LepCharge::Minus) /
                                                                        t.q_over_p(trk_type, SagittaBiasAnalyzer::LepCharge::Minus))));
                   },
                   PlotUtils::HistoBinConfig()
                       .name("Eta_RelQoverP_Plus")
                       .y_title("#sigma(q/p) / (q/p)")
                       .y_min(0)
                       .y_max(0.25)
                       .y_num_bins(250)
                       .x_min(-2.05)
                       .x_max(2.05)
                       .x_num_bins(41)
                       .x_title("#eta")});

    //// Phase space slicer
    std::vector<std::pair<SlicedPlotProvider, SlicedPlotProvider>> phase_space_slicers{};

    std::vector<double> coarse_eta{0.1, 0.5, 0.8, 1.15, 2.05};
    phase_space_slicers +=
        std::make_pair(SlicedPlotProvider(
                           sample_to_process,
                           [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
                               return h->GetBin(h->GetXaxis()->FindBin(std::abs(t.eta(trk_type, SagittaBiasAnalyzer::LepCharge::Plus))));
                           },
                           PlotUtils::HistoBinConfig().x_bins(coarse_eta).name("EtaPlus").x_title("#eta(#mu^{+})"), within_trt),
                       SlicedPlotProvider(
                           sample_to_process,
                           [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
                               return h->GetBin(h->GetXaxis()->FindBin(std::abs(t.eta(trk_type, SagittaBiasAnalyzer::LepCharge::Minus))));
                           },
                           PlotUtils::HistoBinConfig().x_bins(coarse_eta).name("EtaMinus").x_title("#eta(#mu^{-})"), within_trt));

    std::vector<PlotContent<TH1>> plot_contents;

    for (const auto& filler_pair : lepton_filler) {
        const PlotFiller& pos_filler = filler_pair.first;
        const PlotFiller& neg_filler = filler_pair.second;

        for (auto& cut_pair : phase_space_slicers) {
            SlicedPlotProvider& pos_slicer = cut_pair.first;
            SlicedPlotProvider& neg_slicer = cut_pair.second;

            /// Create the charge symmetric plots
            std::vector<Plot<TH1>> binned_plots = merge_charges(pos_slicer.get_slices(pos_filler, MuonTrk::IDTrk, no_mass),
                                                                neg_slicer.get_slices(neg_filler, MuonTrk::IDTrk, no_mass));
            assign_plot_labels(pos_slicer, binned_plots);
            /// Pull again all slices into a summary plot
            Plot<TH1> summed_plot{SumContributions<TH1>(retrievePopulators(binned_plots)),
                                  PlotFormat().LegendTitle("All tracks").LegendOption("PL").MarkerStyle(kFullTriangleUp).Color(kBlack)};
            /// Put the summary plot at the very first
            binned_plots.insert(binned_plots.begin(), std::move(summed_plot));
            if (pos_filler.dimension() == 2) {
                std::vector<Plot<TH1>> mean_plots;
                for (Plot<TH1>& plt : binned_plots) { mean_plots.emplace_back(MeanCalculator(plt), plt.plotFormat()); }
                binned_plots = std::move(mean_plots);
            }
            plot_contents += PlotContent<TH1>{binned_plots,       make_contributions(binned_plots.size()),
                                              {"TRT selections"}, "TRT_ResolutionStudy" + pos_filler.name(),
                                              multi_page,         canvas_opts};
        }
    }

    lepton_filler.clear();
    phase_space_slicers.clear();
    //// Now we want to perform 2D plots of the
    lepton_filler += std::make_pair(
        PlotFiller{[](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
                       return h->GetBin(h->GetXaxis()->FindBin(t.eta(trk_type, SagittaBiasAnalyzer::LepCharge::Plus)),
                                        h->GetYaxis()->FindBin(t.phi(trk_type, SagittaBiasAnalyzer::LepCharge::Plus)),
                                        h->GetZaxis()->FindBin(std::abs(t.err_q_over_p(trk_type, SagittaBiasAnalyzer::LepCharge::Plus) /
                                                                        t.q_over_p(trk_type, SagittaBiasAnalyzer::LepCharge::Plus))));
                   },
                   PlotUtils::HistoBinConfig()
                       .name("EtaPhiQoverP_Plus")
                       .x_title("#eta")
                       .x_min(-2.5)
                       .x_max(2.5)
                       .x_num_bins(1000)
                       .y_title("#phi")
                       .y_min(-M_PI)
                       .y_max(M_PI)
                       .y_num_bins(1000)
                       .z_min(0)
                       .z_max(0.1)
                       .z_num_bins(50)
                       .z_title("#sigma(q/p) / q/p")},

        PlotFiller{[](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
                       // std::cout<<" "<<std::endl;
                       return h->GetBin(h->GetXaxis()->FindBin(t.eta(trk_type, SagittaBiasAnalyzer::LepCharge::Minus)),
                                        h->GetYaxis()->FindBin(t.phi(trk_type, SagittaBiasAnalyzer::LepCharge::Minus)),
                                        h->GetZaxis()->FindBin(std::abs(t.err_q_over_p(trk_type, SagittaBiasAnalyzer::LepCharge::Minus) /
                                                                        t.q_over_p(trk_type, SagittaBiasAnalyzer::LepCharge::Minus))));
                   },
                   PlotUtils::HistoBinConfig()
                       .name("EtaPhiQoverP_Minus")
                       .x_title("#eta")
                       .x_min(-2.5)
                       .x_max(2.5)
                       .x_num_bins(1000)
                       .y_title("#phi")
                       .y_min(-M_PI)
                       .y_max(M_PI)
                       .y_num_bins(1000)
                       .z_min(0)
                       .z_max(0.1)
                       .z_num_bins(50)
                       .z_title("#sigma(q/p) / q/p")

        });

    PhaseSpaceCuts all_pairs("AllPairs", "all pairs", [](SagittaBiasAnalyzer&, int) -> bool { return true; });

    const std::vector<double> pt_bins = PlotUtils::getLinearBinning(5, 25, (25. - 5.) / 2.5);
    phase_space_slicers +=
        std::make_pair(SlicedPlotProvider(
                           sample_to_process,
                           [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
                               return h->GetBin(h->GetXaxis()->FindBin(t.pt(trk_type, SagittaBiasAnalyzer::LepCharge::Plus)));
                           },
                           PlotUtils::HistoBinConfig().x_bins(pt_bins).name("PtPlus").x_title("p_{T} [GeV]"), all_pairs),
                       SlicedPlotProvider(
                           sample_to_process,
                           [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
                               return h->GetBin(h->GetXaxis()->FindBin(t.pt(trk_type, SagittaBiasAnalyzer::LepCharge::Minus)));
                           },
                           PlotUtils::HistoBinConfig().x_bins(pt_bins).name("PtMinus").x_title("p_{T} [GeV]"), all_pairs));

    for (const auto& filler_pair : lepton_filler) {
        break;
        const PlotFiller& pos_filler = filler_pair.first;
        const PlotFiller& neg_filler = filler_pair.second;

        for (auto& cut_pair : phase_space_slicers) {
            SlicedPlotProvider& pos_slicer = cut_pair.first;
            SlicedPlotProvider& neg_slicer = cut_pair.second;
            /// Create the charge symmetric plots
            std::vector<Plot<TH1>> binned_plots = merge_charges(pos_slicer.get_slices(pos_filler, MuonTrk::IDTrk, no_mass),
                                                                neg_slicer.get_slices(neg_filler, MuonTrk::IDTrk, no_mass));
            /// Pull again all slices into a summary plot
            Plot<TH1> summed_plot{SumContributions<TH1>(retrievePopulators(binned_plots)),
                                  PlotFormat().LegendTitle("All tracks").LegendOption("PL").MarkerStyle(kFullTriangleUp).Color(kBlack)};
            /// Put the summary plot at the very first
            binned_plots.insert(binned_plots.begin(), std::move(summed_plot));

            std::vector<Plot<TH1>> rms_plots{};
            for (const Plot<TH1>& plt : binned_plots) { rms_plots.emplace_back(MeanCalculator(plt), plt.plotFormat()); }
            plot_contents += PlotContent<TH1>{rms_plots,  {},         {"TRT selections"}, "TRT_ResolutionStudy_RMS_" + pos_filler.name(),
                                              multi_page, canvas_opts};
        }
    }

    for (auto& pc : plot_contents) {
        if (count_non_empty(pc)) {
            const Plot<TH1>& pl = pc.getPlots()[0];
            if (pl->GetDimension() == 1) {
                DefaultPlotting::draw1D(pc);

            } else {
                draw_2D_plot(pc);
            }
        }
    }
    return EXIT_SUCCESS;
}

std::vector<Plot<TH1>> merge_charges(const std::vector<Plot<TH1>>& pos_plots, const std::vector<Plot<TH1>>& neg_plots) {
    std::vector<Plot<TH1>> merged{};
    for (size_t n = 0; n < pos_plots.size(); ++n) {
        merged.emplace_back(LinearCombination(pos_plots[n], neg_plots[n]), pos_plots[n].plotFormat());
    }
    return merged;
}

std::vector<RatioEntry> make_contributions(size_t n) {
    std::vector<RatioEntry> ratios;
    for (size_t i = 1; i < n; ++i) { ratios.emplace_back(i, 0); }
    return ratios;
}

std::vector<std::shared_ptr<IPlotPopulator<TH1>>> retrievePopulators(const std::vector<Plot<TH1>>& plots) {
    std::vector<std::shared_ptr<IPlotPopulator<TH1>>> populators;
    for (const Plot<TH1>& pl : plots) { populators.push_back(pl.getPopulator()); }
    return populators;
}
void assign_plot_labels(const SlicedPlotProvider& slicer, std::vector<Plot<TH1>>& plots) {
    static const std::vector<int> colour_map{
        kCyan - 2, kOrange, kGreen + 1, kRed, kBlue,
    };
    static const std::vector<int> marker_map{
        kFullTriangleDown,
        kFullDoubleDiamond,
        kFullDiamond,
        kFullFourTrianglesPlus,
    };

    std::vector<int> vis_bins = visibileBinsVector(slicer.get_template());

    auto legend_label = [&slicer, &vis_bins](int idx) {
        std::string label;
        std::vector<std::string> bin_labels = slicer.get_boundaries_legend(vis_bins[idx]);
        for (const std::string& b_label : bin_labels) { label += b_label + " "; }
        return label;
    };

    for (unsigned int bin = 0; bin < vis_bins.size(); ++bin) {
        PlotFormat& format = plots[bin].plotFormat();
        format.LegendTitle.modify() = legend_label(bin);
        format.Color.modify() = colour_map[bin % colour_map.size()];
        format.MarkerStyle.modify() = marker_map[bin % marker_map.size()];
    }
}
