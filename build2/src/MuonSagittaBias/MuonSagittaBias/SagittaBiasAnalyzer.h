#ifndef NTAU__SagittaBiasAnalyzer__H
#define NTAU__SagittaBiasAnalyzer__H

#include <MuonSagittaBias/CollinsSoperUtils.h>

#include "MuonSagittaBias/SagittaBiasTree.h"
#include "MuonSagittaBias/Utils.h"
#include "MuonSagittaBias/WeightBranches.h"

class SagittaInjector;
class TrigSelectionBranch;
namespace CollinsSoper {
    class PseudoCollinsSoperFrame;

}
class SagittaBiasAnalyzer : public SagittaBiasTree {
public:
    SagittaBiasAnalyzer(TTree* t);
    virtual ~SagittaBiasAnalyzer();

    /// Returns the pseudo mass calculated from either the positive or
    enum LepCharge {
        Plus = true,
        Minus = false,
    };
    /// Returns the pseudo mass of the lepton pair
    ///  M^{\pm} = p^{\pm}*p_{T}^{\pm}*(1 - cos(alpha))*cosh{eta^{\mp}}
    float pseudo_mass(int trk_type, bool pos_lep);
    /// Returns cosTheta collins sopper
    float cosThetaCS(int trk_type, bool pos_lep = LepCharge::Minus);
    /// Returns theta collins sopper
    float thetaCS(int trk_type, bool pos_lep = LepCharge::Minus);
    /// Returns phi collins sopper
    float phiCS(int trk_type, bool pos_lep = LepCharge::Minus);

    const CollinsSoper::Vector4D& lep_p4_pseudo_CS(int trk_type, bool pos_lep = LepCharge::Minus);

    ///  Returns pt of the track in the lab frame
    float pt(int trk_type, bool pos_lep);
    /// Returns eta of the track in the lab frame
    float eta(int trk_type, bool pos_lep);
    /// Returns theta of the track in the lab frame
    float theta(int trk_type, bool pos_lep);

    float sinTheta(int trk_type, bool pos_lep);

    /// Returns phi of the track in the lab frame
    float phi(int trk_type, bool pos_lep);
    /// Return electric charge of the track
    int q(int trk_type, bool pos_lep);
    /// Calculates q_over_p of the corresponding track
    float q_over_p(int trk_type, bool pos_lep);

    /// The following methods are invalid for truth tracks
    /// Returns the z0 of the track particle
    float z0(int trk_type, bool pos_lep);
    /// Returns d0 of the track particle
    float d0(int trk_type, bool pos_lep);
    /// Returns d0 significance
    float d0_sig(int trk_type, bool pos_lep);
    /// Returns the z0 difference between the positive and negative lepton track
    float delta_z0(int trk_type);
    /// Returns the d0 difference between the negative and the positive lepton track
    float delta_d0(int trk_type);
    /// Returns the difference in the azimuthal phi angle of a track pair (pos - neg)
    float delta_phi(int trk_type);

    /// Returns whether the corresponding lepton pair could be succesfully filled
    ///  Decision is taken accroding to the DiLepMass which is -1 when the
    ///  pair is (partially) incomplete.
    bool is_valid(int trk_type);

    enum class MuonWP { Loose, Medium, Tight, LowPt, HighPt };
    /// Returns whether the muon satisfies a given working point
    bool pass_id_wp(MuonWP wp, bool pos_lep);
    /// Returns the reconstruction quality of a given Muon
    unsigned short lep_quality(bool pos_lep);

    /// Calculates error in q_over_p of the corresponding track
    double err_q_over_p(int trk_type, bool pos_lep);
    /// Returns no of TRT hits
    unsigned char numberOfTRTHits(int trk_type, bool pos_lep);
    /// Returns the number of TRT outliers
    unsigned char numberOfTRTOutliers(int trk_type, bool pos_lep);

    ///  Returns the invariant mass of a track pair. In cases where one track from the
    ///     pair is missing -1 is returned
    float invariant_m(int trk_type);

    /// returns invariant mass without neglecting the muon mass
    double invariant_m_mu_mass(int trk_type, bool massive_mu);

    ///  Returns the invariant momentum of a track pair.
    float invariant_pt(int trk_type);
    float invariant_Y(int trk_type);
 
    ///  Returns the invariant eta of a track pair
    float invariant_eta(int trk_type);
    /// Returns the invariant phi of a track pair
    float invariant_phi(int trk_type);

    /// Returns the sin_theta_rho_2 value of a track pair
    double sin_theta_rho_2(int trk_type, bool tree_m, bool massive_mu);

    ///  Returns the sin_theta_rho_2 value of a track pair obtained from alpha between track pairs.
    double sin_theta_rho_2_alpha(int trk_type);

    /// Returns the angle alpha between lepton of the track pair
    double alpha(int trk_type);

    /// Returns the current event weight
    /// which is unity for data and cross-section / SumW for Monte Carlo
    DerivedVirtualBranch<double, SagittaBiasAnalyzer> event_weight;

    using calib_constants = MuonTrk::calib_constants;

    /// Adds a gloabl sagitta injection parameter
    /// The parameter is not added if it is zero, has been added before
    /// or an instance of the class has been created
    /// The list of sagitta parameters is then sorted
    static void add_sagitta_injection(calib_constants calib);

    /// Returns the sagitta correction parameters
    static const std::vector<calib_constants>& sagitta_injections();

    /// Transforms the sagitte correction parameters into a vector of integers
    /// which can be piped to the methods of the interface
    /// The integers are assembled together like
    ///   (i << NTrackTypes |  TrkTyp | BiasedTrk)
    static std::vector<int> injection_types();
    /// Returns the index to the element containing the smallest injected bias
    /// for a given track type . If trk type is not in list of sagitta biases -1 is returned
    static int smallest_inject_idx(int trk_type);
    static int largest_inject_idx(int trk_type);
    static int num_injections(int trk_type);

    /// Transforms the given input track type into
    /// the integer pointing to the injection value
    /// in sgitta_injections()
    static int inject_from_type(int trk_type);

    /// Returns whether the tree runs on simulation or not
    bool is_mc() const;
    /// Returns whether the branch has a random run number branch
    bool has_random_run_number() const;

    /// Returns a boolean to apply very basic kinematic cuts on the leptons
    ///  -> eta > 0.1 && eta < 2.5
    ///  -> Applied ID cuts on the track
    ///  -> Cuts on the impact parameters of the leptons
    bool pass_base_kine_sel(int trk_type, const std::pair<float, float>& mass_window);

    /// Returns the year of data taking of the current event
    int period_year();
    /// Returns the run number of the current event
    int run_number();
    /// Returns the lumi block number of the current event
    int lumi_block_number();

    GenWeightBranch gen_weight{this};
    PileUpWeightBranch prw_weight{this};

    TrigPreScaleWeightBranch prescale_weight;

    /// Map containing the possible accepted triggers
    using TrigSelPtr = std::shared_ptr<TrigSelectionBranch>;
    using TrigSelVector = std::vector<TrigSelPtr>;

    DerivedVirtualBranch<bool, SagittaBiasAnalyzer> trig_decision;

    /// Returns the list of triggers to be considered for the event
    /// decision. The list is sorted by the resonance mass & by the current data_taking event
    TrigSelVector& potential_triggers();

private:
    SagittaInjector* get_injector(int trk_type);

    std::vector<std::unique_ptr<SagittaInjector>> m_injectors;

    std::map<int, std::unique_ptr<CollinsSoper::PseudoCollinsSoperFrame>> m_pseud_cs;

    static std::vector<calib_constants> s_sagitta_inject_params;

    static std::atomic<bool> s_constructor_called;

    bool select_trigger_Z(const float pt_plus, const float pt_minus, const int year);
    bool select_trigger_JPsi(const float pt_plus, const float pt_minus, const int year);

    bool m_has_rnd_run_number{false};

    void setup_z_triggers();
    void setup_jpsi_triggers();
    TrigSelPtr SINGLE_TRIGGER(const std::string& trig_item, const float pt_thresh);
    TrigSelPtr DILEP_TRIGGER(const std::string& trig_item, const float low_thresh, const float high_thresh);

    TrigSelVector m_def_trigs;
    std::map<int, TrigSelVector> m_trigs_z;
    std::map<int, TrigSelVector> m_trigs_jpsi;
};
#endif  // NTAU__SAGITTABIASTREE__H
