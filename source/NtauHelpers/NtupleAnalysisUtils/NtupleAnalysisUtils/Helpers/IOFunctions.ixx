#include "NtupleAnalysisUtils/Helpers/VectorHelpers.h"
template <class T> T* PlotUtils::readFromFile (const std::string fname,const std::string & histoname){
    std::shared_ptr<TFile> f = Open(fname);
    if (!f){
        return nullptr;
    }
    return readFromFile<T>(f,histoname);
}
template <class T> T* PlotUtils::readFromFile (TFile* f, const std::string & histoname){
    if (!f){
        std::cerr<<"NULL file pointer passed to PlotUtils::readFromFile"<<std::endl;
        return nullptr;
    }
    T* h_out_tmp =  nullptr;
    f->GetObject(histoname.c_str(), h_out_tmp);
    if (!h_out_tmp){
        std::cerr<<" Unable to get the object "<<histoname<<" from the file "<<f->GetName()<<std::endl;
        return nullptr;
    }
    /// return a clone not belonging to this file. Allows us to safely close the file. 
    TH1::AddDirectory(false); 
    return dynamic_cast<T*>(h_out_tmp->Clone()); 
}

template <class T> T* PlotUtils::readFromFile (std::shared_ptr<TFile> f, const std::string & histoname ){
    return readFromFile<T>(f.get(), histoname);
}
template <class T> std::vector<std::string> PlotUtils::getAllObjectsFromFile(const std::string &fname){
    std::shared_ptr<TFile> f = Open(fname);
    return getAllObjectsFromDir<T>(f.get());
}
template <class T> std::vector<std::string> PlotUtils::getAllObjectsFromDir(TDirectory *dir, const std::string &base_path, bool recursive){
    std::vector<std::string> content;
    /// Protection against nullptrs
    if (!dir) return content;
    /// Loop over all Keys in the directory
    for (const auto &key : *dir->GetListOfKeys()) {
        TDirectory *sub_dir = nullptr;
        T* candidate = nullptr;
        /// Test whether the Key can be cases to a TDirectory --> subdirectory
        dir->GetObject(key->GetName(), sub_dir);
        /// Or if the key represents our desired TObject type
        dir->GetObject(key->GetName(), candidate);
        /// assemble the full path in the file
        const std::string obj_path = base_path + (base_path.empty() ? "" : "/") + std::string(key->GetName());
        /// If it's a TDirectory go to the next layer
        if (sub_dir && recursive) {
            std::vector<std::string> content_in_dir = getAllObjectsFromDir<T>(sub_dir, obj_path);
            content += content_in_dir;
        /// Otherwise add the path back to the list
        } else if (candidate) {
             content.push_back(obj_path);
        }
    }
    std::sort(content.begin(), content.end());
    return content;
}

template <class T> std::vector<std::string> PlotUtils::getObjectsUntilLayer(const std::string& file, unsigned int layer_max, unsigned int layer_min){
    const std::vector<std::string> all_obj = getAllObjectsFromFile<T>(file);
    std::vector<std::string> to_return;
    std::copy_if(all_obj.begin(), all_obj.end(), to_return.begin(), [layer_max,layer_min](const std::string& full_path){
        unsigned int n_layers = 0;
        for (size_t s = 0; s < full_path.size(); ++s) n_layers += full_path[s] == '/';
        return n_layers >= layer_min && n_layers<= layer_max;
    });
    return to_return;
}

