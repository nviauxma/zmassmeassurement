#ifndef NTAU_NTUPLEBRANCHMGR__H
#define NTAU_NTUPLEBRANCHMGR__H

#include "NtupleAnalysisUtils/Ntuple/NtupleBranchBase.h" 
#include "NtupleAnalysisUtils/Ntuple/BootstrapPoissonHelper.h" 

// The NtupleBranchMgr represents a management structure for NtupleBranch objects from a given tree. 
// It can be used to easily organise, manipulate and access a large number of attached branches.
// In an analysis, this can be used to represent the entirety of an Ntuple's content
// in a single object, for example with the "Selection" and "Plot" classes. 

// The HistoFiller class is designed to work with this. 

// The NtupleBranchMgr also has "autodetect" functionality to find branches in an arbitrary input tree.
// This can be useful for postprocessing (copy all branches) or short jobs

// It is intended to be base class of minitree classes in analysis use. 

class NtupleBranchMgr{
public:
    // t: TTree to read from or write to 
    NtupleBranchMgr(TTree* t = 0);
    virtual ~NtupleBranchMgr();

    // attaches an NtupleBranch to this manager. After doing this, fill() / getEntry() on the manager will also affect the newly attached branch.
    // You will also be able to access it using getBranch if you want to.
    void attach(NtupleBranchBase * ntb);

    // entry numbers - set / get current entry, get total #entries 

    // set all attached NtupleBranches to the entry number given. 
    Long64_t getEntry(Long64_t entry);
    // get the current entry number.
    inline Long64_t getCurrEntry () const {return m_currEntry;}
    // get the number of entries in the connected tree
    Long64_t getEntries(){ return (m_tree ? m_tree->GetEntries() : -1);}
    
    // writing operations

    // call the fill method of all attached branches. Make sure you set them to something first :-) 
    void fill();

    // get the underlying ROOT TTree object 
    TTree* getTree() const {return m_tree;}

    // get branches by name 

    // check if we have a given branch in the connected TTree object. Does *NOT* look at the attached NtupleBranch list.
    bool hasBranchInTree (const std::string name);
    // get a branch by name. Return null if no such branch esists. 
    NtupleBranchBase* getBranch (const std::string name);
    // get *all* branches starting with a certain prefix 
    // useful for identifying sys variations! 
    std::map<std::string, NtupleBranchBase*> getBranchesStartingWith(const std::string & start);  
    std::map<std::string, NtupleBranchBase*> getBranchesEndingWith(const std::string & end);  
    // some shortcuts for getting the branches in a nicer way. 
    // This is still using string comparisons, so best cache the branch at some point! 
    // this one is like the getBranch above, with a free dynamic cast...
    template<typename T> NtupleBranch<T>* getBranch (const std::string name){return dynamic_cast<NtupleBranch<T>*>(getBranch(name));}

    // this one sets t to the branch content if the branch exists. 
    template<typename T> void operator()(const std::string name, T & t){NtupleBranch<T>* b = getBranch<T>(name); if (b) t=b->get();}
    // this is used when accessing array branches to set t to the i-th element of the array branch
    template<typename T> void operator()(const std::string name, size_t i, T & t){
        NtupleBranch<T*>* b = getBranch<T*>(name); 
        if (b) {
            t=b->get(i);
        } 
        else {
            NtupleBranch<std::vector<T> >* b2 = getBranch<std::vector<T>>(name); 
            if (b2) t = b2->get(i);
        }
    }

    // used for autodetection

    // picks up all branches in t that are not yet assigned to this mgr instance.
    // It will attempt to automatically identify the correct branch types. 
    // Use to auto-populate trees.
    // You can then access any such branches via getBranch
    // The prefix is assigned to all branches, added in front of the name as it is in the TTree. Useful for derived classes which might use more than a single tree... 
    void getMissedBranches(TTree* t, std::string prefix="");

   // this method will copy (by name) all branches already present in the passed tree.
   // Note that any other branches need to be filled by hand! 
   void populateCopy(NtupleBranchMgr & tree);
   void addSkippedBranch(const std::string & skipMe);
   void addSkippedBranches(std::vector<std::string> & skipThose);

    // methods to steer bootstrap weight generation 
    void setNBootstrapWeights(size_t N);
    void generateBootstrapWeights();
    size_t getNBoostrapWeights() const { return m_n_bootstrap;}
    double getBootstrapWeight(size_t i);
    void seedBootStrap (ULong_t seed){ m_bootstrapHelper.reseed(seed);}

    // this is a callback for use by NtupleBranchBase - the branch will register itself using this method
    void registerActive(NtupleBranchBase * ntb);
    // this will do the opposite from the method above
    void detachActive(NtupleBranchBase* ntb);


    /// Code generation. 
    // The className can be used to set the name of the class to be generated (will otherwise default to the name of the input tree of this manager). 
    void GenerateManagerClass(const std::string & className="");
    
    ///
    /// Returns the list of active branches
    ///
    std::vector<NtupleBranchBase *> get_active_branches() const;
protected:

    // methods used for branch autodetection

    // generate ntuple branch appropriate to a given TLeaf. The prefix is attached in fromt of the branch name.
    // The manager will take posession of the branch
    // t is the tree that the branch will be assigned to
    // this is designed for output
    NtupleBranchBase* generateBranch(TLeaf* leaf, TTree* t=0, const std::string & prefix = "");


    // data members
    Long64_t m_currEntry;
    std::map<std::string, NtupleBranchBase *> m_mapBranches;
    std::vector<NtupleBranchBase *> m_vecBranches;
    std::vector<NtupleBranchBase *> m_vecActive;
    std::vector<std::unique_ptr<NtupleBranchBase> > m_cleanUp;
    TTree* m_tree;
    size_t m_n_bootstrap;
    BootstrapPoissonHelper m_bootstrapHelper;
    std::vector<double> m_bootstrapWeights;
    NtupleBranch<int>* m_aux_bootstrap_run;
    NtupleBranch<ULong64_t>* m_aux_bootstrap_event;

   bool m_loadedCopyables{false};
   std::map<std::string, NtupleBranchBase*> m_branchesToCopy;
   std::vector<std::string> m_skipBranchList;
};


#endif //NTAU_NTUPLEBRANCHMGR__H
