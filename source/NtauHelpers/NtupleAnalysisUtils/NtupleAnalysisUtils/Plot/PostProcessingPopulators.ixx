template <class InputHistoType> CalculateRatio<InputHistoType>::CalculateRatio( 
                    const IPopulatorSource<InputHistoType> & numeratorSource,  
                    const IPopulatorSource<InputHistoType> & denominatorSource,
                    PlotUtils::EfficiencyMode errorDefinition):
        m_numeratorSource(numeratorSource.clonePopulator()),
        m_denominatorSource(denominatorSource.clonePopulator()),
        m_errorDefinition(errorDefinition){
}
template <class InputHistoType>  std::shared_ptr<TH1> CalculateRatio<InputHistoType>::populate() {
    auto numerator = m_numeratorSource->populate();
    auto denominator = m_denominatorSource->populate();
    if (!numerator){
        std::cerr << "Failed to compute a ratio - numerator is null histogram"<<std::endl;
        return nullptr;
    }
    if (!denominator){
        std::cerr << "Failed to compute a ratio - denominator is null histogram"<<std::endl;
        return nullptr;
    }
    return std::shared_ptr<TH1>{PlotUtils::getRatio(numerator.get(),denominator.get(),m_errorDefinition)}; 
}   
template <class InputHistoType>  std::shared_ptr<IPlotPopulator<TH1>> CalculateRatio<InputHistoType>::clonePopulator() const  {
    return std::make_shared<CalculateRatio<InputHistoType>>(*m_numeratorSource, *m_denominatorSource, m_errorDefinition);
}



template <class HistoType> NormaliseBinsToWidth<HistoType>::NormaliseBinsToWidth( const IPopulatorSource<HistoType> & inputSource, double referenceWidth):
    m_input(inputSource.clonePopulator()),
    m_refWidth(referenceWidth){     
}  

template <class HistoType> std::shared_ptr<HistoType> NormaliseBinsToWidth<HistoType>::populate() {
    auto inHist = m_input->populate();
    if (!inHist){
        std::cerr << "Failed to normalise to bin width - input is null histogram"<<std::endl;
        return nullptr;
    }
    PlotUtils::normaliseToBinWidth(inHist.get(),m_refWidth); 
    return inHist; 
}
template <class HistoType> std::shared_ptr<IPlotPopulator<HistoType>> NormaliseBinsToWidth<HistoType>::clonePopulator() const {
    return std::make_shared<NormaliseBinsToWidth<HistoType>>(*m_input, m_refWidth);
}



template <class HistoType> ScaleByConstant<HistoType>::ScaleByConstant( const IPopulatorSource<HistoType> & inputSource, double scaleFactor):
    m_input(inputSource.clonePopulator()),
    m_scaleFactor(scaleFactor){    
}  
template <class HistoType> std::shared_ptr<HistoType> ScaleByConstant<HistoType>::populate() {
    auto inHist = m_input->populate();
    if (!inHist){
        std::cerr << "Failed to scale an object - input is null histogram"<<std::endl;
        return nullptr;
    }
    inHist->Scale(m_scaleFactor); 
    return inHist; 
}
template <class HistoType> std::shared_ptr<IPlotPopulator<HistoType>> ScaleByConstant<HistoType>::clonePopulator() const {
    return std::make_shared<ScaleByConstant<HistoType>>(*m_input, m_scaleFactor);
}



template <class HistoType> LinearCombination<HistoType>::LinearCombination( const IPopulatorSource<HistoType> & inputSource, const IPopulatorSource<HistoType> & toAdd,  double scaleFactor):
    m_input(inputSource.clonePopulator()),
    m_toAdd(toAdd.clonePopulator()),
    m_scaleFactor(scaleFactor){    
}  
template <class HistoType> std::shared_ptr<HistoType> LinearCombination<HistoType>::populate() {
    auto inHist = m_input->populate();
    auto addMe = m_toAdd->populate();
    if (!inHist){
        std::cerr << "Failed to perform addition - first argument of sum is null histogram"<<std::endl;
        return nullptr;
    }
    if (!addMe){
        std::cerr << "Failed to perform addition - second argument of sum is null histogram"<<std::endl;
        return nullptr;
    }
    inHist->Add(addMe.get(), m_scaleFactor); 
    return inHist; 
}

template <class HistoType> std::shared_ptr<IPlotPopulator<HistoType>> LinearCombination<HistoType>::clonePopulator() const {
    return std::make_shared<LinearCombination<HistoType>>(*m_input, *m_toAdd, m_scaleFactor);
}



template <class HistoType> NormaliseToIntegral<HistoType>::NormaliseToIntegral( const IPopulatorSource<HistoType> & inputSource, 
                                                                                double targetIntegral, 
                                                                                bool multiplyByWidth,
                                                                                bool includeOflow):
    m_input(inputSource.clonePopulator()),
    m_targetIntegral(targetIntegral),
    m_includeOflow(includeOflow),
    m_multByWidth(multiplyByWidth){    
}  
template <class HistoType> std::shared_ptr<HistoType> NormaliseToIntegral<HistoType>::populate() {
    auto inHist = m_input->populate();
    if (!inHist){
        std::cerr << "Failed to normalise an object to integral - input is null histogram"<<std::endl;
        return nullptr;
    }    
    PlotUtils::normaliseToIntegral(inHist.get(), m_targetIntegral, m_multByWidth, m_includeOflow); 
    return inHist; 
}
template <class HistoType> std::shared_ptr<IPlotPopulator<HistoType>> NormaliseToIntegral<HistoType>::clonePopulator() const {
    return std::make_shared<NormaliseToIntegral<HistoType>>(*(m_input.get()), m_targetIntegral, m_multByWidth, m_includeOflow);
}



/// This constructor uses the auxiliary method "collateInputs" to pacakge the received contributions 
/// to the input vector (see below)
template <class HistoType> template<typename... Args>  SumContributions<HistoType>::SumContributions (Args... args){
    collateInputs(args...); 
}
/// here we just copy the vector
template <class HistoType> SumContributions<HistoType>::SumContributions (const std::vector<std::shared_ptr<IPlotPopulator<HistoType>>> & inputs):
    m_inputs(inputs){
}
/// this will populate the upstream sources and then perform a ratio calculation
template <class HistoType> std::shared_ptr<HistoType> SumContributions<HistoType>::populate() {
    std::shared_ptr<HistoType> out = nullptr;
    for (auto & in : m_inputs){
        auto theInput = in->populate();
        if (!theInput){
            std::cerr << "SumContributions encountered an empty histo, not adding to sum. Please check your setup!"<<std::endl; 
            continue; 
        }
        if (!out) out = std::shared_ptr<HistoType>(dynamic_cast<HistoType*>(theInput->Clone())); 
        else out->Add(theInput.get()); 
    }
    return out; 
}
template <class HistoType>  std::shared_ptr<IPlotPopulator<HistoType>> SumContributions<HistoType>::clonePopulator() const{
    return std::make_shared<SumContributions<HistoType>>(m_inputs);
}
/// Recursive argument collection, default step: Package one arg, then repeat for the remaining
template <typename HistoType> template<typename... Args> void SumContributions<HistoType>::collateInputs(const IPopulatorSource<HistoType> & H, Args... args){
    m_inputs.push_back(H.clonePopulator()); 
    collateInputs(args...); 
}
/// Recursive argument collection, final step: package the one remaining arg
template <typename HistoType> void SumContributions<HistoType>::collateInputs(const IPopulatorSource<HistoType>& H){
    m_inputs.push_back(H.clonePopulator()); 
}



template <class OutputHistoType, class InputHistoType>  GenericPostProcessing<OutputHistoType, InputHistoType>::GenericPostProcessing(
        const IPopulatorSource<InputHistoType> & inputSource, 
        std::function<std::shared_ptr<OutputHistoType>(std::shared_ptr<InputHistoType>)> processingStep){
    m_input = inputSource.clonePopulator(); 
    m_processingStep = processingStep;
}
template <class OutputHistoType, class InputHistoType>  std::shared_ptr<OutputHistoType> GenericPostProcessing<OutputHistoType, InputHistoType>::populate() {
    std::shared_ptr<InputHistoType> inputHist = m_input->populate(); 
    return m_processingStep(inputHist); 
}
template <class OutputHistoType, class InputHistoType>  std::shared_ptr<IPlotPopulator<OutputHistoType>> GenericPostProcessing<OutputHistoType, InputHistoType>::clonePopulator() const  {
    return std::make_shared<GenericPostProcessing<OutputHistoType,InputHistoType>>(*m_input, m_processingStep);
}

