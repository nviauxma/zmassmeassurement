#ifndef NTAUHELPERS_LUMICALCULATOR_H_
#define NTAUHELPERS_LUMICALCULATOR_H_
#include <NtauHelpers/Utils.h>

#include <string>
#include <vector>
namespace CP {
    class IPileupReweightingTool;
}

namespace MuonTP {

    // Helper class to slice the total recorded luminosity in
    // blocks of equal luminosity
    class LumiInterval;

    class LumiSlicer {
    public:
        // Size of each lumiInterval in pb^{-1}
        LumiSlicer(double slice_size);
        // Slice the luminosity per run
        LumiSlicer();
        unsigned int find_bin(unsigned int run, unsigned int lumi_block) const;
        std::shared_ptr<LumiInterval> get_block(unsigned int bin) const;

        size_t size() const;

    private:
        std::vector<std::shared_ptr<LumiInterval>> m_blocks;
    };
    class LumiInterval {
    public:
        LumiInterval(unsigned int bin);

        void set_start(unsigned int run, unsigned int lumi_block);
        void set_end(unsigned int run, unsigned int lumi_block);

        unsigned int bin_number() const;
        bool in_interval(unsigned int run, unsigned int lumi_block) const;

        unsigned int start_run() const;
        unsigned int end_run() const;

        unsigned int start_block() const;
        unsigned int end_block() const;

        void set_lumi(double l);
        double lumi() const;

    private:
        unsigned int m_start_run{INT_MAX};
        unsigned int m_start_block{INT_MAX};
        unsigned int m_end_run{INT_MAX};
        unsigned int m_end_block{INT_MAX};
        double m_lumi{0};
        unsigned int m_bin{0};
    };

    // Helper class to calculate the luminosity from the prwTool in python
    class LumiCalculator {
    public:
        static LumiCalculator* getCalculator();

        // Luminosity calculated from the ilumicalc file entirely
        double getExpectedLumiFromRun(unsigned int run) const;
        double getExpectedLumiFromRun(unsigned int run, unsigned int block) const;
        double getRecordedLumiFromRun(unsigned int run) const;

        double getExpectedLumi(unsigned int begin, unsigned int end) const;
        double getRecordedLumi(unsigned int begin, unsigned int end) const;

        bool isGoodLumiBlock(unsigned int run, unsigned int block) const;
        std::set<unsigned int> goodLumiBlocks(unsigned int run) const;

        // Helper functinos to initialize  the pileup tool -> wrapper of the Weighter
        bool InitPileUpTool();
        bool isInitialized() const;

        void SetPRWlumi(const std::vector<std::string>& lumi);
        void SetPRWconfig(const std::vector<std::string>& config);
        ~LumiCalculator();

        std::vector<unsigned int> getRunsInLumiCalc() const;
        unsigned int GetNLumiBlocks(unsigned int run) const;
        double getLumi2k1516k() const;
        double getLumi2k17() const;
        double getLumi2k18() const;

        // Returns the luminosity of the period
        double getLumi(unsigned int period_num) const;
        // Return the total luminosity
        double getLumi() const;

        // Returns the prwTool based on the run_number of data-taking or the random run_number
        // The same tool assignment is also used for the getExpectedLumi methods
        std::shared_ptr<CP::IPileupReweightingTool> GetPrwTool(unsigned int run_number = -1) const;
        // Returns the prwTool based on the MC period number. The three big campaigns are
        // assigned each to one tool. There is one fall-back tool used if the MC is not part
        // of them
        std::shared_ptr<CP::IPileupReweightingTool> GetPrwToolForMC(unsigned int period_number) const;

        bool isMC16e(unsigned int period_number) const;
        bool isMC16d(unsigned int period_number) const;
        bool isMC16a(unsigned int period_number) const;
        // Returns whether the MC belongs to a common MC campaign i.e. mc16a/mc16d/mc16e
        bool isBulkMC(unsigned int period_number) const;

    private:
        static LumiCalculator* m_Inst;
        typedef std::pair<unsigned int, unsigned int> Run_Nblocks;

        LumiCalculator();
        LumiCalculator(const LumiCalculator&) = delete;
        void operator=(const LumiCalculator&) = delete;

        typedef std::pair<unsigned int, unsigned int> prwChannel;
        typedef std::vector<prwChannel> prwFileContent;

        //  Methods to read the prw config files. Config files of up to two campagins
        //  can be given to MuonTPPostProcessing. These methods read out the particular files and
        //  order them by period number.
        std::map<unsigned int, std::vector<std::string>> readMCchannelsPRW() const;

        bool coverDifferentProfile(const prwFileContent& first_file, const prwFileContent& second_file) const;
        bool insertNewChannel(const std::map<std::string, prwFileContent>& prwConfig, const std::string& candidate,
                              std::vector<std::string>& used_files) const;
        bool insertNewChannel(const std::map<std::string, prwFileContent>& prwConfig, const std::string& candidate,
                              std::vector<std::vector<std::string>>& used_files) const;
        void ReadMCprwFile(const std::string& path, prwFileContent& channels) const;

        //  Methods to read and order the prw lumi calc files. Based on the assumption that each lumi calc-file
        //  cannot contain periods of two different years, the lumi-calc files are grouped into year of data-taking
        //  using the first found runNumber
        std::map<unsigned int, std::vector<std::string>> orderLumiCalcFiles();
        void ReadLumiCalcFile(const std::string& Path, std::vector<Run_Nblocks>& runs) const;

        // method to create & configure the prwTool
        bool CreateTool(std::shared_ptr<CP::IPileupReweightingTool>& prwTool, const std::vector<std::string>& lumi_files,
                        const std::vector<std::string>& config_files, const std::string& toolName = "prwTool");

        int CreateTool(const std::map<unsigned int, std::vector<std::string>>& ordered_lumi,
                       std::map<unsigned int, std::vector<std::string>>& prw_config_fles, unsigned int period_num,
                       std::shared_ptr<CP::IPileupReweightingTool>& prwTool);

        bool isData() const;
        std::vector<std::string> m_lumi_calc{};
        std::vector<std::string> m_lumi_config{};

        std::shared_ptr<CP::IPileupReweightingTool> m_prwTool{nullptr};
        std::shared_ptr<CP::IPileupReweightingTool> m_prwTool_1516k{nullptr};
        std::shared_ptr<CP::IPileupReweightingTool> m_prwTool_17k{nullptr};
        std::shared_ptr<CP::IPileupReweightingTool> m_prwTool_18k{nullptr};

        std::vector<Run_Nblocks> m_recorded_runs{};

        double m_lumi_1516k{0.};
        double m_lumi_17k{0.};
        double m_lumi_18k{0.};
        double m_total_lumi{0.};
    };
}  // namespace MuonTP

#endif /* MUONTPPOSTPROCESSING_LUMICALCULATOR_H_ */
