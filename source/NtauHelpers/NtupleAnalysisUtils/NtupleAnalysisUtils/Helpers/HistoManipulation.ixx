
#include "NtupleAnalysisUtils/Plot/BasicPopulators.h"


template <class T, class S>  TH1* PlotUtils::getRatio(const T* num, const S* den, PlotUtils::EfficiencyMode errors ){
    /// try to resort to the TH1 signature
    const TH1* n = dynamic_cast<const TH1*>(num);
    const TH1* d = dynamic_cast<const TH1*>(den);
    if (n && d) return getRatio(n,d,errors);
    /// otherwise give up and return a null pointer
    else {
        std::cerr <<" Sorry, no ratio calculation supported for these histogram types :-( " <<std::endl;
        return nullptr;
    }
}


template <typename H> Plot<TH1> PlotUtils::getRatio(Plot<H> & num, Plot<H> & den, PlotUtils::EfficiencyMode errmode){
    /// For Plot objects, we generate a new plot with the format of the old one, and perform 
    /// the ratio calculation using the template methods 
    Plot<TH1> thePlot(CopyExisting<TH1>(dynamic_cast<TH1*>(getRatio(num(),den(),errmode))), num.plotFormat());
    thePlot.populate();
    return thePlot; 
}