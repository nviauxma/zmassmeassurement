#include <FourMomUtils/xAODP4Helpers.h>
#include <MuonSagittaBias/AsymmetryPlots.h>
#include <MuonSagittaBias/PhaseSpaceCuts.h>
#include <MuonSagittaBias/PlotProvider.h>
#include <MuonSagittaBias/SagittaBiasAnalyzer.h>
#include <MuonSagittaBias/SlicedPlotProvider.h>
#include <MuonSagittaBias/Utils.h>
#include <NtauHelpers/MuonTPMetaData.h>
#include <NtauHelpers/OutFileWriter.h>
#include <NtauHelpers/SelectionWithTitle.h>

// user setup:=====================================================
std::string init_filenames() {
    int data_or_mc;
    std::string year;
    std::cout << "Data or MC? [0 = data / 1 = MC] ";
    std::cin >> data_or_mc;
    if (data_or_mc == 1) {
        year = "MC";
    } else if (data_or_mc == 0) {
        std::cout << "year? (16, 17 or 18)  ";
        std::cin >> year;
    }
    return year;
}
//=================================================================
std::string file_name(std::string year, int which_one) {
    std::string my_file_name, my_file_name2;
    my_file_name = "SagittaTemplates" + year + "quality4.root";
    my_file_name2 = "Plots_lep_qual_" + year;

    if (which_one == 1) {
        return my_file_name;
    } else if (which_one == 2) {
        return my_file_name2;
    } else {
        std::string output_str = "wrong input";
        return output_str;
    }
}
//=================================================================

int main(int argc, char* argv[]) {
    SetAtlasStyle();                                   // styling of histogram
    HistoFiller::getDefaultFiller()->setNThreads(16);  // no of files to be processed simultaneously
    // HistoFiller::getDefaultFiller()->setFillMode(HistoFillMode::singleThread);
    std::string tree_path = "SagittaCalib/Reco/MuonCandidates_OC/TPTree_MuonCandidates_OC";  // retrieving tree from file defining the path
    //########################
    std::string file_name_arg = init_filenames();
    std::string template_file = file_name(file_name_arg, 1);  //"SagittaTemplatesMC.root";// creating empty file to fill in later
    //##########################
    std::vector<std::string> all_mc_files{};

    bool z_files_cleared{false};
    std::vector<std::string> z_sample{};
    //##########################
    std::string out_dir = file_name(file_name_arg, 2);  // fallback to NtupleAnalysisUtils default scheme if not specified
    //##########################
    for (int k = 1; k < argc; ++k) {  // some changes to the input arguments configured by command line
        std::string current_arg(argv[k]);
        // if (current_arg == "--inFile" && k + 1 < argc)
        //     in_file = argv[k + 1];
        // else
        if (current_arg == "--outDir" && k + 1 < argc) {
            out_dir = argv[k + 1];
            ++k;
        }
        /// change the name of the ROOT file containing all of the histograms
        else if (current_arg == "--rootFile" && k + 1 < argc) {
            template_file = argv[k + 1];
            ++k;
        }
        /// Change the name of the tree to be read. For the moment this is not needed
        else if (current_arg == "--treePath" && k + 1 < argc) {
            tree_path = argv[k + 1];
            ++k;
        }
        /// Add a list of samples to the z_sample list
        else if (current_arg == "--signalFileList" && k + 1 < argc) {
            if (!z_files_cleared) {
                z_sample.clear();
                z_files_cleared = true;
            }
            z_sample += ReadList(argv[k + 1]);
            ++k;
        }

        else if (current_arg == "--signalDir" && k + 1 < argc) {
            if (!z_files_cleared) {
                z_sample.clear();
                z_files_cleared = true;
            }
            z_sample += ListDirectory(argv[k + 1], "*.root");
            ++k;
        }
    }

    all_mc_files = z_sample;
    /// Initialize the monte carlo data base
    // deciding if it runs on real data or mc sim data (now real)
    if (!MuonTP::NormalizationDataBase::getDataBase()->init(all_mc_files)) { return EXIT_FAILURE; }
    /// Load the cross-sections from the central PMG area... Hopefully they're not destroyed
    MuonTP::NormalizationDataBase::getDataBase()->LoadxSections("dev/PMGTools/PMGxsecDB_mc16.txt");

    /// Create the output file to store all of the histograms

    PlotWriter<TH1> out_file{make_out_file(out_dir + "/" + template_file)};
    if (!out_file.get()) { return EXIT_FAILURE; }  // defining output file (later)
    bool is_mc = !MuonTP::NormalizationDataBase::getDataBase()->isData();
    /// Inject the sagitta biases

    Sample<SagittaBiasAnalyzer> z_reco_sample = make_sample<SagittaBiasAnalyzer>(z_sample, tree_path);  // loading input files (samples)

    // styling
    CanvasOptions canvas_opts = CanvasOptions()
                                    .RatioAxis(AxisConfig().Title("to truth-level").ExtraTitleOffset(0.4).Min(0.9).Max(1.005))
                                    .YAxis(AxisConfig().Title("Entries").ExtraTitleOffset(0.4).TopPadding(0.7))
                                    // .XAxis(AxisConfig().ExtraTitleOffset(1.3))
                                    .LabelStatusTag("Simulation Internal")
                                    .OutputDir(out_dir);

    auto multi_page = PlotUtils::startMultiPagePdfFile("AllSagittaPlots", canvas_opts);  // summary pdf with all plots

    //===================================================================================================================================
    std::vector<PlotContent<TH1>> radial_biases;  // which plots to include (not needed now)
    std::vector<PhaseSpaceCuts> selections_both_tight;
    std::vector<PhaseSpaceCuts> selections_one_tight;

    // START QUALITY CRITERIA:

    selections_both_tight += PhaseSpaceCuts("All_pairs_tt", "All_pairs_tt", [](SagittaBiasAnalyzer& t, int trk_type) -> bool {
        const double neg_eta = t.eta(trk_type, SagittaBiasAnalyzer::LepCharge::Minus);
        const double pos_eta = t.eta(trk_type, SagittaBiasAnalyzer::LepCharge::Plus);

        const double abs_neg_eta = std::abs(neg_eta);
        const double abs_pos_eta = std::abs(pos_eta);  // here!
        return abs_pos_eta > 0.1 && abs_pos_eta < 2.5 && abs_neg_eta > 0.1 && abs_neg_eta < 2.5 && t.delta_z0(trk_type) < 1 &&
               t.lep_quality(true) == 0 && t.lep_quality(false) == 0; /*&&
std::min(t.pt(trk_type, SagittaBiasAnalyzer::LepCharge::Plus), t.pt(trk_type, SagittaBiasAnalyzer::LepCharge::Minus)) > 25*/
        ;                                                             // remove 25 gev cut
    });

    // endcap 1.17 < |eta| < 2.5
    selections_both_tight += PhaseSpaceCuts("End_cap_pairs_tt", "End_cap_pairs_tt", [](SagittaBiasAnalyzer& t, int trk_type) -> bool {
        const double neg_eta = t.eta(trk_type, SagittaBiasAnalyzer::LepCharge::Minus);
        const double pos_eta = t.eta(trk_type, SagittaBiasAnalyzer::LepCharge::Plus);

        const double abs_neg_eta = std::abs(neg_eta);
        const double abs_pos_eta = std::abs(pos_eta);

        return abs_pos_eta > 1.17 && abs_pos_eta < 2.5 && abs_neg_eta > 1.17 && abs_neg_eta < 2.5 && t.delta_z0(trk_type) < 1 &&
               t.delta_z0(trk_type) < 1 && t.lep_quality(true) == 0 && t.lep_quality(false) == 0; /*&&
std::min(t.pt(trk_type, SagittaBiasAnalyzer::LepCharge::Plus), t.pt(trk_type, SagittaBiasAnalyzer::LepCharge::Minus)) > 25*/
    });

    // barrel 0.1 < |eta| < 1.17 -> use 0.1 instead like before
    selections_both_tight += PhaseSpaceCuts("Barrel_pairs_tt", "Barrel_pairs_tt", [](SagittaBiasAnalyzer& t, int trk_type) -> bool {
        const double neg_eta = t.eta(trk_type, SagittaBiasAnalyzer::LepCharge::Minus);
        const double pos_eta = t.eta(trk_type, SagittaBiasAnalyzer::LepCharge::Plus);

        const double abs_neg_eta = std::abs(neg_eta);
        const double abs_pos_eta = std::abs(pos_eta);
        return abs_pos_eta > 0.1 && abs_pos_eta < 1.17 && abs_neg_eta > 0.1 && abs_neg_eta < 1.17 && t.delta_z0(trk_type) < 1 &&
               t.delta_z0(trk_type) < 1 && t.lep_quality(true) == 0 && t.lep_quality(false) == 0; /*&&
std::min(t.pt(trk_type, SagittaBiasAnalyzer::LepCharge::Plus), t.pt(trk_type, SagittaBiasAnalyzer::LepCharge::Minus)) > 25*/
    });

    // pos in barrel and neg in endcap
    selections_both_tight +=
        PhaseSpaceCuts("Barrel_endcap_pairs_tt", "Barrel_endcap_pairs_tt", [](SagittaBiasAnalyzer& t, int trk_type) -> bool {
            const double neg_eta = t.eta(trk_type, SagittaBiasAnalyzer::LepCharge::Minus);
            const double pos_eta = t.eta(trk_type, SagittaBiasAnalyzer::LepCharge::Plus);

            const double abs_neg_eta = std::abs(neg_eta);
            const double abs_pos_eta = std::abs(pos_eta);
            if (std::min(abs_pos_eta, abs_neg_eta) < 0.1 || std::max(abs_pos_eta, abs_neg_eta) > 2.5) return false;
            return ((abs_pos_eta < 1.17 && abs_neg_eta > 1.17) || (abs_pos_eta > 1.17 && abs_neg_eta < 1.17)) && t.delta_z0(trk_type) < 1 &&
                   t.delta_z0(trk_type) < 1 && t.lep_quality(true) == 0 && t.lep_quality(false) == 0; /*&&
std::min(t.pt(trk_type, SagittaBiasAnalyzer::LepCharge::Plus), t.pt(trk_type, SagittaBiasAnalyzer::LepCharge::Minus)) > 25*/
        });

    selections_one_tight += PhaseSpaceCuts("All_pairs_mt", "All_pairs_mt", [](SagittaBiasAnalyzer& t, int trk_type) -> bool {
        const double neg_eta = t.eta(trk_type, SagittaBiasAnalyzer::LepCharge::Minus);
        const double pos_eta = t.eta(trk_type, SagittaBiasAnalyzer::LepCharge::Plus);

        const double abs_neg_eta = std::abs(neg_eta);
        const double abs_pos_eta = std::abs(pos_eta);  // here!
        return abs_pos_eta > 0.1 && abs_pos_eta < 2.5 && abs_neg_eta > 0.1 && abs_neg_eta < 2.5 && t.delta_z0(trk_type) < 1 &&
               (t.lep_quality(true) == 0 || t.lep_quality(false) == 0); /*&&
std::min(t.pt(trk_type, SagittaBiasAnalyzer::LepCharge::Plus), t.pt(trk_type, SagittaBiasAnalyzer::LepCharge::Minus)) > 25*/
        ;                                                               // remove 25 gev cut
    });

    // endcap 1.17 < |eta| < 2.5
    selections_one_tight += PhaseSpaceCuts("End_cap_pairs_mt", "End_cap_pairs_mt", [](SagittaBiasAnalyzer& t, int trk_type) -> bool {
        const double neg_eta = t.eta(trk_type, SagittaBiasAnalyzer::LepCharge::Minus);
        const double pos_eta = t.eta(trk_type, SagittaBiasAnalyzer::LepCharge::Plus);

        const double abs_neg_eta = std::abs(neg_eta);
        const double abs_pos_eta = std::abs(pos_eta);

        return abs_pos_eta > 1.17 && abs_pos_eta < 2.5 && abs_neg_eta > 1.17 && abs_neg_eta < 2.5 && t.delta_z0(trk_type) < 1 &&
               t.delta_z0(trk_type) < 1 && (t.lep_quality(true) == 0 || t.lep_quality(false) == 0); /*&&
std::min(t.pt(trk_type, SagittaBiasAnalyzer::LepCharge::Plus), t.pt(trk_type, SagittaBiasAnalyzer::LepCharge::Minus)) > 25*/
    });

    // barrel 0.1 < |eta| < 1.17 -> use 0.1 instead like before
    selections_one_tight += PhaseSpaceCuts("Barrel_pairs_mt", "Barrel_pairs_mt", [](SagittaBiasAnalyzer& t, int trk_type) -> bool {
        const double neg_eta = t.eta(trk_type, SagittaBiasAnalyzer::LepCharge::Minus);
        const double pos_eta = t.eta(trk_type, SagittaBiasAnalyzer::LepCharge::Plus);

        const double abs_neg_eta = std::abs(neg_eta);
        const double abs_pos_eta = std::abs(pos_eta);
        return abs_pos_eta > 0.1 && abs_pos_eta < 1.17 && abs_neg_eta > 0.1 && abs_neg_eta < 1.17 && t.delta_z0(trk_type) < 1 &&
               t.delta_z0(trk_type) < 1 && (t.lep_quality(true) == 0 || t.lep_quality(false) == 0); /*&&
std::min(t.pt(trk_type, SagittaBiasAnalyzer::LepCharge::Plus), t.pt(trk_type, SagittaBiasAnalyzer::LepCharge::Minus)) > 25*/
    });

    // pos in barrel and neg in endcap
    selections_one_tight +=
        PhaseSpaceCuts("Barrel_endcap_pairs_mt", "Barrel_endcap_pairs_mt", [](SagittaBiasAnalyzer& t, int trk_type) -> bool {
            const double neg_eta = t.eta(trk_type, SagittaBiasAnalyzer::LepCharge::Minus);
            const double pos_eta = t.eta(trk_type, SagittaBiasAnalyzer::LepCharge::Plus);

            const double abs_neg_eta = std::abs(neg_eta);
            const double abs_pos_eta = std::abs(pos_eta);
            if (std::min(abs_pos_eta, abs_neg_eta) < 0.1 || std::max(abs_pos_eta, abs_neg_eta) > 2.5) return false;
            return ((abs_pos_eta < 1.17 && abs_neg_eta > 1.17) || (abs_pos_eta > 1.17 && abs_neg_eta < 1.17)) && t.delta_z0(trk_type) < 1 &&
                   t.delta_z0(trk_type) < 1 && (t.lep_quality(true) == 0 || t.lep_quality(false) == 0); /*&&
std::min(t.pt(trk_type, SagittaBiasAnalyzer::LepCharge::Plus), t.pt(trk_type, SagittaBiasAnalyzer::LepCharge::Minus)) > 25*/
        });

    //-------------------------------------------------------------------------------------------------------------------------

    std::vector<PlotProvider> slicers;
    std::vector<PlotFiller> fillers_JPsi;
    std::vector<PlotFiller> fillers_2d_JPsi;

    // for J/psi mass cut:  ===================================================================================================

    //  1. sin_theta_rho_2 USING TREE invariant_m
    fillers_2d_JPsi += PlotFiller(
        [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
            return h->GetBin(h->GetXaxis()->FindBin(t.sin_theta_rho_2(trk_type, true, true)),
                             h->GetYaxis()->FindBin(t.invariant_m(trk_type)));
        },
        std::make_shared<TH2D>("Sin_theta_rho_2_m_Plot_Jpsi_TREE",
                               "sin^{2}(#theta_{#rho}) against m_{#mu#mu} (TREE);sin^{2}(#theta_{#rho});m_{#mu#mu} (TREE)", 60, 0, 1., 60,
                               2.7, 3.5));

    // TH2D (const char *name, const char *title, Int_t nbinsx, const Double_t *xbins, Int_t nbinsy, const Double_t*ybins)
    // TH1D (const char *name, const char *title, Int_t nbinsx, const Double_t *xbins)

    //  2. sin_theta_rho_2 USING 4-MOM invariant_m_mu_mass FOR MASSLESS MUONS
    fillers_2d_JPsi += PlotFiller(
        [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
            return h->GetBin(h->GetXaxis()->FindBin(t.sin_theta_rho_2(trk_type, false, false)),
                             h->GetYaxis()->FindBin(t.invariant_m_mu_mass(trk_type, false)));
        },
        std::make_shared<TH2D>("Sin_theta_rho_2_m_Plot_Jpsi_MASSLESS",
                               "sin^{2}(#theta_{#rho}) against m_{#mu#mu} (MASSLESS);sin^{2}(#theta_{#rho});m_{#mu#mu} (MASSLESS)", 60, 0,
                               1., 60, 2.7, 3.5));

    //  3. sin_theta_rho_2 USING 4-MOM invariant_m_mu_mass FOR MASSIVE MUONS
    fillers_2d_JPsi += PlotFiller(
        [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
            return h->GetBin(h->GetXaxis()->FindBin(t.sin_theta_rho_2(trk_type, false, true)),
                             h->GetYaxis()->FindBin(t.invariant_m_mu_mass(trk_type, true)));
        },
        std::make_shared<TH2D>("Sin_theta_rho_2_m_Plot_Jpsi_MASSIVE",
                               "sin^{2}(#theta_{#rho}) against m_{#mu#mu} (MASSIVE);sin^{2}(#theta_{#rho});m_{#mu#mu} (MASSIVE) ", 60, 0,
                               1., 60, 2.7, 3.5));

    // CREATE 2D FILLER FOR ALPHA m_mumu--------------------------------------------------------------------------------

    //  1. sin_theta_rho_2 USING TREE invariant_m
    fillers_2d_JPsi += PlotFiller(
        [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
            return h->GetBin(h->GetXaxis()->FindBin(t.sin_theta_rho_2_alpha(trk_type)), h->GetYaxis()->FindBin(t.invariant_m(trk_type)));
        },
        std::make_shared<TH2D>("Sin_theta_rho_2_m_alpha_Plot_Jpsi_TREE",
                               "sin^{2}(#theta_{#rho#alpha}) against m_{#mu#mu} (TREE);sin^{2}(#theta_{#rho#alpha}); m_{#mu#mu} (TREE) ",
                               60, 0, 1., 60, 2.7, 3.5));

    //  2. sin_theta_rho_2 USING 4-MOM invariant_m_mu_mass FOR MASSLESS MUONS
    fillers_2d_JPsi += PlotFiller(
        [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
            return h->GetBin(h->GetXaxis()->FindBin(t.sin_theta_rho_2_alpha(trk_type)),
                             h->GetYaxis()->FindBin(t.invariant_m_mu_mass(trk_type, false)));
        },
        std::make_shared<TH2D>(
            "Sin_theta_rho_2_m_alpha_Plot_Jpsi_MASSLESS",
            "sin^{2}(#theta_{#rho#alpha}) against m_{#mu#mu} (MASSLESS) ;sin^{2}(#theta_{#rho#alpha}); m_{#mu#mu} (MASSLESS) ", 60, 0, 1.,
            60, 2.7, 3.5));

    //  3. sin_theta_rho_2 USING 4-MOM invariant_m_mu_mass FOR MASSIVE MUONS
    fillers_2d_JPsi += PlotFiller(
        [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
            return h->GetBin(h->GetXaxis()->FindBin(t.sin_theta_rho_2_alpha(trk_type)),
                             h->GetYaxis()->FindBin(t.invariant_m_mu_mass(trk_type, true)));
        },
        std::make_shared<TH2D>(
            "Sin_theta_rho_2_m_alpha_Plot_Jpsi_MASSIVE",
            "sin^{2}(#theta_{#rho#alpha}) against m_{#mu#mu} (MASSIVE);sin^{2}(#theta_{#rho#alpha}); m_{#mu#mu} (MASSIVE)", 60, 0, 1., 60,
            2.7, 3.5));

    /* QUALITY CRITERIA FILLERS
    fillers_2d_JPsi += PlotFiller([](const TH1* h, SagittaBiasAnalyzer& t, int trk_type)
                          { return h->GetBin(h->GetXaxis()->FindBin(t.q_over_p(trk_type, true)),
   h->GetYaxis()->FindBin(t.err_q_over_p(trk_type,true)));}, std::make_shared<TH2D>("q_over_p_vs_err_muon_plus", "q/p vs q/p error (+ve
   muons);q/p; q/p error", 60, 0, 10., 60,0, 10.));

    fillers_2d_JPsi += PlotFiller([](const TH1* h, SagittaBiasAnalyzer& t, int trk_type)
                          { return h->GetBin(h->GetXaxis()->FindBin(t.q_over_p(trk_type, false)),
   h->GetYaxis()->FindBin(t.err_q_over_p(trk_type,false)));}, std::make_shared<TH2D>("q_over_p_vs_err_muon_minus", "q/p vs q/p error (-ve
   muons);q/p; q/p error", 60, 0, 10., 60,0, 10.)); fillers_2d_JPsi += PlotFiller([](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
   return h->GetBin(h->GetXaxis()->FindBin(t.q_over_p(trk_type, true)),
   h->GetYaxis()->FindBin(t.err_q_over_p(trk_type,true)/t.q_over_p(trk_type, true)));},
   std::make_shared<TH2D>("q_over_p_vs_err_muon_plus_fractional", "q/p vs fractional q/p error (+ve muons);q/p; fractional q/p error", 60,
   0, 10., 60,0, 1.5));
    */

    // 1D FILLERS:
    fillers_JPsi += PlotFiller([](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) { return h->GetXaxis()->FindBin(t.phiCS(trk_type)); },
                               std::make_shared<TH1D>("PhiCS_JPsi", "dummy;#phi^{CS} [rad];a.u.", 60, -M_PI, M_PI));
    fillers_JPsi += PlotFiller(
        [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) { return h->GetXaxis()->FindBin(t.sin_theta_rho_2(trk_type, true, true)); },
        std::make_shared<TH1D>("Sin_theta_rho_2_1D_Plot_JPsi", "dummy;sin^{2}(#theta_{#rho})", 60, 0, 1.5));
    fillers_JPsi +=
        PlotFiller([](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) { return h->GetXaxis()->FindBin(t.invariant_m(trk_type)); },
                   std::make_shared<TH1D>("Invariant_mass_1D_Plot_JPsi", "dummy;m_{#mu#mu} ", 60, 0, 5));
    // specially important when running MC - QUALITY PARAMETERS
    /*fillers_JPsi += PlotFiller([](const TH1* h, SagittaBiasAnalyzer& t, int trk_type)
                           {return h->GetXaxis()->FindBin(t.d0Err_ID(trk_type,true));},
                           std:: make_shared<TH1D>("d0err_+_1D_Plot", "dummy;d_{0} error (#mu_{+})", 500, 0, 10));
    fillers_JPsi += PlotFiller([](const TH1* h, SagittaBiasAnalyzer& t, int trk_type)
                           {return h->GetXaxis()->FindBin(t.d0Err_ID(trk_type,false));},
                           std:: make_shared<TH1D>("d0err_-_1D_Plot", "dummy;d_{0} error (#mu_{-}) ", 500, 0, 10));*/
    fillers_JPsi +=
        PlotFiller([](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) { return h->GetXaxis()->FindBin(t.delta_z0(trk_type)); },
                   std::make_shared<TH1D>("delta_z0_1D_Plot", "dummy;#Delta z_{0} ", 500, 0, 2));
    fillers_JPsi +=
        PlotFiller([](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) { return h->GetXaxis()->FindBin(t.delta_d0(trk_type)); },
                   std::make_shared<TH1D>("delta_d0_1D_Plot", "dummy;#Delta d_{0}", 500, 0, 2));
    fillers_JPsi +=
        PlotFiller([](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) { return h->GetXaxis()->FindBin(t.lep_quality(true)); },
                   std::make_shared<TH1D>("lep_quality_pos", "dummy;#Lepton Quality (pos)", 500, 0, 2));
    fillers_JPsi +=
        PlotFiller([](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) { return h->GetXaxis()->FindBin(t.lep_quality(false)); },
                   std::make_shared<TH1D>("lep_quality_neg", "dummy;#Lepton Quality (neg)", 500, 0, 2));
    fillers_JPsi += PlotFiller(
        [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
            return h->GetXaxis()->FindBin(std::abs(t.err_q_over_p(trk_type, true) / t.q_over_p(trk_type, true)));
        },
        std::make_shared<TH1D>("fractional_err_q_over_p_pos", "dummy;#(q/p err) / (q/p)", 10000, 0, 0.6));
    fillers_JPsi += PlotFiller(
        [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) {
            return h->GetXaxis()->FindBin(std::abs(t.err_q_over_p(trk_type, false) / t.q_over_p(trk_type, false)));
        },
        std::make_shared<TH1D>("fractional_err_q_over_p_neg", "dummy;#(q/p err) / (q/p)", 10000, 0, 0.6));

    fillers_JPsi += PlotFiller(
        [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) { return h->GetXaxis()->FindBin(t.numberOfTRTHits(trk_type, true)); },
        std::make_shared<TH1D>("TRT_hits_pos", "dummy;#TRT hits (pos)", 1000, 0, 60));
    fillers_JPsi += PlotFiller(
        [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) { return h->GetXaxis()->FindBin(t.numberOfTRTHits(trk_type, false)); },
        std::make_shared<TH1D>("TRT_hits_neg", "dummy;#TRT hits (neg)", 1000, 0, 60));
    ///////
    fillers_JPsi +=
        PlotFiller([](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) { return h->GetXaxis()->FindBin(t.pt(trk_type, true)); },
                   std::make_shared<TH1D>("pt_plus", "dummy;#p_{t+}", 500, 0, 100));
    fillers_JPsi +=
        PlotFiller([](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) { return h->GetXaxis()->FindBin(t.pt(trk_type, false)); },
                   std::make_shared<TH1D>("pt_minus", "dummy;#p_{t-}", 500, 0, 100));

    // combines fillers (varibles) with phase space cuts###################################################

    // 1D JPsi fillers - selections_both_tight
    for (auto& fill : fillers_JPsi) {
        for (PhaseSpaceCuts& phase_space : selections_both_tight) {
            PlotProvider my_provider(z_reco_sample, fill, phase_space);
            radial_biases += PlotContent<TH1>(
                {
                    my_provider.make_plot(MuonTrk::IDTrk, {2.7, 3.5}),  //{}cut in invariant masses
                },
                {}, {"Initial Plots - J/Psi - tight/tight Muons"}, my_provider.name(), multi_page, canvas_opts);
        }
    }

    // 2D J/PSI fillers - selections_both_tight
    for (auto& fill : fillers_2d_JPsi) {
        for (PhaseSpaceCuts& phase_space : selections_both_tight) {
            PlotProvider my_provider(z_reco_sample, fill, phase_space);
            my_provider.add_plot(MuonTrk::IDTrk, out_file, {2.7, 3.5});
        }
    }
    // 1D JPsi fillers selections_one_tight
    for (auto& fill : fillers_JPsi) {
        for (PhaseSpaceCuts& phase_space : selections_one_tight) {
            PlotProvider my_provider(z_reco_sample, fill, phase_space);
            radial_biases += PlotContent<TH1>(
                {
                    my_provider.make_plot(MuonTrk::IDTrk, {2.7, 3.5}),  //{}cut in invariant masses
                },
                {}, {"Initial Plots - J/Psi - medium/tight Muons"}, my_provider.name(), multi_page, canvas_opts);
        }
    }

    // 2D J/PSI fillers selections_one_tight
    for (auto& fill : fillers_2d_JPsi) {
        for (PhaseSpaceCuts& phase_space : selections_one_tight) {
            PlotProvider my_provider(z_reco_sample, fill, phase_space);
            my_provider.add_plot(MuonTrk::IDTrk, out_file, {2.7, 3.5});
        }
    }

    ///
    ///         Add the pseudo mass for positive and negative charges
    ///

    std::sort(radial_biases.begin(), radial_biases.end(),

              [](const PlotContent<TH1>& a, const PlotContent<TH1>& b) { return a.getFileName() < b.getFileName(); });
    for (auto& pc : radial_biases) {
        if (count_non_empty(pc) == pc.getPlots().size()) DefaultPlotting::draw1D(pc);
    }
    return EXIT_SUCCESS;
}
