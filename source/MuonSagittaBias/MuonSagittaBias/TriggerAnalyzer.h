#ifndef NTAU__TriggerAnalyzer__H
#define NTAU__TriggerAnalyzer__H

#include "MuonSagittaBias/SagittaBiasTree.h"
#include "MuonSagittaBias/TriggerAccess.h"
#include "MuonSagittaBias/Utils.h"
#include "MuonSagittaBias/WeightBranches.h"

class TriggerAnalyzer : public SagittaBiasTree {
public:
    TriggerAnalyzer(TTree* t);
    virtual ~TriggerAnalyzer();

    /// Returns the pseudo mass calculated from either the positive or
    enum LepCharge {
        Plus = true,
        Minus = false,
    };
    ///  Returns pt of the track in the lab frame
    float pt(bool pos_lep);
    /// Returns eta of the track in the lab frame
    float eta(bool pos_lep);
    /// Returns phi of the track in the lab frame
    float phi(bool pos_lep);
    /// Return electric charge of the track
    int q(bool pos_lep);

    /// The following methods are invalid for truth tracks
    /// Returns the z0 of the track particle
    float z0(bool pos_lep);
    /// Returns d0 of the track particle
    float d0(bool pos_lep);
    /// Returns d0 significance
    float d0_sig(bool pos_lep);

    /// Returns whether the corresponding lepton pair could be succesfully filled
    ///  Decision is taken accroding to the DiLepMass which is -1 when the
    ///  pair is (partially) incomplete.
    bool is_valid();
    ///  Returns the invariant mass of a track pair. In cases where one track from the
    ///     pair is missing -1 is returned
    float invariant_m();

    /// Returns the current event weight
    /// which is unity for data and cross-section / SumW for Monte Carlo
    float event_weight();

    /// Returns whether the tree runs on simulation or not
    bool is_mc() const;
    /// Returns whether the branch has a random run number branch
    bool has_random_run_number() const;

    /// Returns the year in which the event was recorded
    ///    This is of particular interest for the trigger selection of the lowest unprescaled trigger
    ///      c.f https://twiki.cern.ch/twiki/bin/viewauth/Atlas/LowestUnprescaled#Muon_AN2
    enum data_period {

        data15 = 1,
        data16_AD3 = 1 << 1,
        data16_D4I4 = 1 << 2,
        data16 = data16_AD3 | data16_D4I4,

        data17_MainStream = 1 << 3,
        data17_PEB = 1 << 4,
        data17 = data17_MainStream | data17_PEB,

        data18_MainStream = 1 << 5,
        data18_PEB = 1 << 6,
        data18 = data18_MainStream | data18_PEB,

    };
    static std::string period_to_string(int period);

    /// Returns the year of data taking of the current event
    int period_year();

    int run_number();

    std::vector<TriggerAccess> triggers{TriggerAccess::make_full_trig_list(*this)};

    DerivedVirtualBranch<bool, TriggerAnalyzer> only_one_trig_fired;
    GenWeightBranch gen_weight{this};

private:
    bool m_is_mc{false};
    bool m_has_rnd_run_number{false};
};
#endif  // NTAU__SAGITTABIASTREE__H
