#ifndef NTAU__HISTOFILLERINTERFACES__H
#define NTAU__HISTOFILLERINTERFACES__H 

#include <string> 
#include <vector> 
#include <map> 
#include <memory> 

#include "NtupleAnalysisUtils/HistoFiller/ObjectID.h"


/// This header defines a few interfaces that are useful in order to 
/// decouple the HistoFiller from having to know the nature of the things
/// it is running on (also support different trees in one loop). 
/// 
/// Not normally needed by the end user, unless you are debugging something.

/// forward-declare
class IHistoFillTask;
class IFillable;


/// In the HistoFiller context, we often need to use our objects as map keys, 
/// or check if two objects are equal. 
/// For these occasions, we use a UID for comparison operations. 
class ISortableByID{
public:
    /// this will default to a unique ID not yet in use. 
    ISortableByID();
    /// comparison operator between two shared pointers based on the identifier
    struct lessForSharedPtr{
        bool operator()(const std::shared_ptr<ISortableByID> & s1, 
                        const std::shared_ptr<ISortableByID> & s2) const; 
    };

    /// retrieve the identifier
    ObjectID getID() const;

    /// allow to overwrite the identifier. Used to establish identity where appropriate
    void setID(ObjectID id);
    /// syntactic shortcut - allows to set the ID directly to a string (will be hashed) - emulate
    /// behaviour of the names in old NtupleAnalysisUtils versions
    void setID(const std::string & id);

    // implementation of sorting operators to enable std::less and equality checks for non-shared-pointer cases
    inline bool operator < (const ISortableByID & other) const; 
    inline bool operator == (const ISortableByID & other) const;
    inline bool operator > (const ISortableByID & other) const;

    protected:
    ObjectID m_ID{ObjectID::nextID()}; 
};

// for selections, the only thing the HistoFiller needs to be able to do is to 
// access its ID (via ISortableByID), and to clone it 
class ISelection: public ISortableByID{
public: 
    virtual std::shared_ptr<ISelection> clone() const =0;
};


/// the input file interface has to do three things: 
/// a) permit sorting by ID as for the selection (ISortableByID)
/// b) allow cloning
/// c) spawn tasks which internally set up the correct instantiation of the 
///    templated selection and PlotFillInstruction objects. 
class IInputFile: public ISortableByID{
public: 
    /// the maps use the ISortableByID::lessForSharedPtr comparison operator (by ID rather than pointer address)
    typedef  std::map <std::shared_ptr<ISelection>, 
                        std::vector<std::shared_ptr<IFillable>>, 
                        ISortableByID::lessForSharedPtr> SelectionToFillableMap;
    typedef  std::map <std::shared_ptr<IInputFile>, 
                       SelectionToFillableMap, 
                       ISortableByID::lessForSharedPtr > FileToSelectionListMap;
    
    /// the IInputFile is responsible for spawning tasks that internally use the 
    /// proper instantiation of plot / selection / fill instruction
    virtual std::shared_ptr<IInputFile> clone() const =0;
    /// this method represents the connection between the non-templated interface classes 
    /// and the templated implementations
    virtual std::shared_ptr<IHistoFillTask> spawnTask (SelectionToFillableMap & payload, bool splitAndMergeMode)=0;

    /// Allows to enable the creation of boot-strap weights 
    virtual size_t getNBootstrapWeights() = 0;
    virtual void setNBootstrapWeights(size_t n) = 0;

};

/// From a sample, we need to be able to obtain naming information as well as a list of contained files.
/// As for all other objects, we also need to clone it
class ISample: public ISortableByID{
public:

    virtual std::shared_ptr<ISample> clone() const =0;
    virtual std::vector<std::shared_ptr<IInputFile> > getInputFiles()=0;
};

/// interface for an object that can be filled by the HistoFiller.
/// Here, we need the associated selection and sample, and a clone method.
/// we also need to be able to collect clones after parallel running and to check if an 
/// add method is supported. Finally, provide a check whether a plot has 
/// already been filled or not. 
class IFillable: public ISortableByID{
public:
    /// comparison function, used to check if two plots are equivalent
    virtual bool equalTo (std::shared_ptr<IFillable> & other) const =0;
    /// clone the associated selection
    virtual std::shared_ptr<ISelection> getSelectionClone() const=0;
    /// clone the associated sample
    virtual std::shared_ptr<ISample> getSampleClone() const=0; 
    /// clone the plot itself in order to process several files simultaneously
    virtual std::shared_ptr<IFillable > clone() const=0;
    /// reunite itself with a set of filled clones by adding them back to the parent (after clearing it)
    virtual void CollectClones(std::vector<std::shared_ptr<IFillable> >  &clonesToCollect )=0; 
    /// absorb a single filled clone by adding it back to the parent
    virtual void CollectClone(std::shared_ptr<IFillable>cloneToCollect )=0; 
    /// check if this object type can be added together from contributions
    virtual bool canAdd() const =0;
    /// check if this object has already been filled
    virtual bool hasBeenFilled() const = 0;
    /// toggle the flag for having been filled
    virtual void setHasBeenFilled(bool val = true) = 0;
    /// reset the content 
    virtual void Reset() = 0; 

};

// interface to report filling progress. 

class IFillProgressTracker{
    public: 
    
    /// This enum allows to define some custom progress reporting
    /// strategies in addition to the standard "every N events"
    typedef enum {
        unset=-1,
        noPrintout=0,
        every1Percent,
        every2Percent,
        every5Percent,
        every10Percent,
        every25Percent,
        every50Percent
    }  specialPrintInterval;
    /// allow clients to log their progress 
    virtual void reportProgress (long long nFinished)=0; 
    virtual void reportFileComplete ()=0; 

    /// this method will print the progress after every <interval> events
    virtual void setPrintInterval(int interval)=0;

    /// this method will print the progress after an interval determined by the definitions 
    /// of specialPrintInterval (every X percent or not at all)
    virtual void setPrintInterval(specialPrintInterval interval)=0;
};

// This is the interface with which the HistoFiller can talk to its tasks. 
// We want to be able to make the tasks run, as well as to steer printout.
class IHistoFillTask{
public:
    /// will make the task execute, reporting progress back to the queue if desired
    virtual void run(std::shared_ptr<IFillProgressTracker> theQueue = nullptr) =0;
    /// this will make the task validate its input object
    virtual bool validateInput(long long & nevents_found)=0;
    /// this steers how often the task will report back to the queue if desired
    virtual void setReportingInterval(long long interval)=0;
    /// debug printout
    virtual void print(const std::string & spacer="")=0; 
};

// here we need an additional level of templating for the IFillable class to ensure that the FillHistoTask can do its job
template <class fillFrom> class IFillableFrom: public IFillable{
public:
    virtual void Fill(fillFrom &t) =0; 
};

#include "HistoFillerInterfaces.ixx"

#endif //NTAU__HISTOFILLERINTERFACES__H
