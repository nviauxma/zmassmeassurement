#ifndef NTAUHELPERS_TRIGGERHELPER_H
#define NTAUHELPERS_TRIGGERHELPER_H
#include <NtauHelpers/Utils.h>

///#############################################################
///     Simple helper class to map the periods of data-taking
///     to the particular run numbers
///#############################################################
class TriggerHelper {
public:
    enum data_period {
        /// Reserve first 4 bits for the year of data-taking
        year15 = 1,
        year16 = 1 << 1,
        year17 = 1 << 2,
        year18 = 1 << 3,

        /// The next few bits are reserved for the particular periods
        periodA = 1 << 4,
        periodB = 1 << 5,
        periodC = 1 << 6,
        periodD = 1 << 7,
        periodE = 1 << 8,
        periodF = 1 << 9,
        periodG = 1 << 10,
        periodH = 1 << 11,
        periodI = 1 << 12,
        periodJ = 1 << 13,
        periodK = 1 << 14,
        periodL = 1 << 15,
        periodM = 1 << 16,
        periodN = 1 << 17,
        periodO = 1 << 18,

        periodQ = 1 << 19,
        periodR = 1 << 20,

        periodZ = 1 << 21,

        data15 = year15 | periodD | periodE | periodF | periodG | periodH | periodI | periodJ,
        data16 = year16 | periodA | periodB | periodC | periodD | periodE | periodF | periodG | periodH | periodI | periodJ | periodK |
                 periodL | periodZ,
        data17 = year17 | periodA | periodB | periodC | periodD | periodE | periodF | periodG | periodH | periodI | periodK | periodN,
        data18 = year18 | periodA | periodB | periodC | periodE | periodF | periodG | periodH | periodI | periodJ | periodK | periodL |
                 periodM | periodM | periodN | periodO | periodQ | periodR,

        years = year15 | year16 | year17 | year18,
        periods = (data15 | data16 | data17 | data18) & (~years)
    };

    static bool is_mc16a(int period_number);
    static bool is_mc16d(int period_number);
    static bool is_mc16e(int period_number);

    static int get_period_year(int run_number);

    static bool is_2k15(int run_number);
    static bool is_2k16(int run_number);
    static bool is_2k17(int run_number);
    static bool is_2k18(int run_number);

    static std::string to_string(int run_number);
    static std::string to_title(int run_number);
};
#endif