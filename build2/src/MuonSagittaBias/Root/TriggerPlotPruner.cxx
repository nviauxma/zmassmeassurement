#include "MuonSagittaBias/TriggerPlotPruner.h"

#include "MuonSagittaBias/TriggerAccess.h"
TriggerPlotPruner::TriggerPlotPruner(Plot<TH1> trig_plot, Plot<TH1> match_plot, const PlotUtils::HistoBinConfig& trig_histo,
                                     enum TriggerPlotPruner::ToReturn to_ret) :
    m_trig_plot{trig_plot}, m_match_plot{match_plot}, m_to_ret{to_ret}, m_trig_histo{trig_histo} {}
std::shared_ptr<TH1> TriggerPlotPruner::populate() {
    if (!m_trig_plot.populate() || (m_to_ret == ToReturn::MatchPlot && !m_match_plot.populate())) { return nullptr; }
    std::shared_ptr<TH1> trig_histo = m_trig_plot.getSharedHisto();
    /// Find first the bins which have non-zero entries
    std::vector<int> good_bins;
    good_bins.reserve(trig_histo->GetNbinsX());
    for (int bin = 1; bin < trig_histo->GetNbinsX(); ++bin) {
        if (std::abs(trig_histo->GetBinContent(bin)) > DBL_EPSILON) good_bins.push_back(bin);
    }
    if (good_bins.empty()) return trig_histo;
    /// Sort the trigger list such that the most important ones are on the left hand side
    std::sort(good_bins.begin(), good_bins.end(),
              [&trig_histo](const int a, const int b) { return trig_histo->GetBinContent(a) > trig_histo->GetBinContent(b); });
    /// Next let's generate the pruned histo binning
    std::shared_ptr<TH1> pruned_histo = MakeTH1(PlotUtils::getLinearBinning(0, good_bins.size(), good_bins.size()));
    if (m_to_ret == ToReturn::MatchPlot) trig_histo = m_match_plot.getSharedHisto();

    set_bin_labels(trig_histo->GetXaxis(), m_trig_histo.x_bin_labels());
    for (unsigned int bin = 0; bin < good_bins.size(); ++bin) {
        std::string bin_label = trig_histo->GetXaxis()->GetBinLabel(good_bins[bin]);
        const int target_bin = bin + 1;
        pruned_histo->SetBinContent(target_bin, trig_histo->GetBinContent(good_bins[bin]));
        pruned_histo->SetBinError(target_bin, trig_histo->GetBinError(good_bins[bin]));
        pruned_histo->GetXaxis()->SetBinLabel(target_bin, bin_label.c_str());
    }
    return pruned_histo;
}
std::shared_ptr<IPlotPopulator<TH1>> TriggerPlotPruner::clonePopulator() const { return std::make_shared<TriggerPlotPruner>(*this); }
