#ifndef MUONSAGITTABIAS_ASYMMETRYPLOTS_H
#define MUONSAGITTABIAS_ASYMMETRYPLOTS_H
#include "MuonSagittaBias/SagittaBiasAnalyzer.h"

/// Plot populator class to create the
template <class InputHistoType> class AsymmetryPlot : public IPlotPopulator<TH1> {
public:
    /// construct by providing numerator and denominator sources and a bin error strategy.
    /// Both numeratorSource and denominatorSource can be either a Plot or another populator.
    AsymmetryPlot(const std::string& plot_name, const IPopulatorSource<InputHistoType>& forwardMuons,
                  const IPopulatorSource<InputHistoType>& backwardMuons);

    /// this will populate the upstream sources and then perform a ratio calculation
    std::shared_ptr<TH1> populate() override;
    std::shared_ptr<IPlotPopulator<TH1>> clonePopulator() const override;

    std::string plot_name() const;

private:
    std::shared_ptr<IPlotPopulator<InputHistoType>> m_forward;
    std::shared_ptr<IPlotPopulator<InputHistoType>> m_backward;
    std::string m_plot_name;
};

template <class InputHistoType>
AsymmetryPlot<InputHistoType> make_asymmetry_plot(const std::string& plot_name,
                                                  Sample<SagittaBiasAnalyzer> sample,        // Sample this plot belongs to
                                                  Selection<SagittaBiasAnalyzer> selection,  // Selection this plot belongs to
                                                  PlotFillInstructionWithRef<InputHistoType, SagittaBiasAnalyzer>
                                                      fillInstruction,  //  Define how a fill step should look, and the desired binning
                                                  int trk_type);

#include "MuonSagittaBias/AsymmetryPlots.ixx"

#endif
