#include "MuonSagittaBias/TriggerAccess.h"

#include <NtauHelpers/TriggerHelper.h>
namespace {
    constexpr float MeVtoGeV = 1.e-3;
}
///
///
///
PlotUtils::HistoBinConfig TriggerAccess::extract_binning(TTree* t_tree) {
    SagittaBiasTree t{t_tree};
    const std::vector<TriggerAccess> triggers{make_full_trig_list(t)};
    PlotUtils::HistoBinConfig cfg;
    /// One extra bin to determine the selection efficiency of each trigger.
    const int n_bins = triggers.size() + 1;
    cfg.x_min(0).x_max(n_bins).x_num_bins(n_bins);

    std::map<int, std::string>& labels = cfg.x_bin_labels.modify();
    labels[1] = "All";
    for (const TriggerAccess& acc : triggers) { labels[labels.size() + 1] = acc.trigger(); }
    return cfg;
}

std::vector<TriggerAccess> TriggerAccess::make_full_trig_list(SagittaBiasTree& t) {
    std::vector<TriggerAccess> trig_list;

    std::map<std::string, NtupleBranchBase*> trig_map = t.getBranchesStartingWith("trig_HLT");
    trig_list.reserve(trig_map.size());
    for (const auto& map_itr : trig_map) {
        std::string trig_item = map_itr.first.substr(map_itr.first.find("_") + 1, std::string::npos);
        trig_list.emplace_back(trig_item, t);
    }

    return trig_list;
}

TriggerAccess::TriggerAccess(const std::string& trigger_item, SagittaBiasTree& t) :
    m_item{trigger_item},
    m_decision{*t.getBranch<bool>("trig_" + trigger_item)},
    m_negLep_match{*t.getBranch<bool>("negLep_matched_" + trigger_item)},
    m_posLep_match{*t.getBranch<bool>("posLep_matched_" + trigger_item)} {
    m_prescale = t.getBranch<float>("trigPreScale_" + trigger_item);
}
std::string TriggerAccess::trigger() const { return m_item; }
bool TriggerAccess::pass_decision() { return m_decision(); }

bool TriggerAccess::pass_matching_dR() { return pos_lep_matched_dR() || neg_lep_matched_dR(); }

bool TriggerAccess::pos_lep_matched_dR() { return m_posLep_match(); }
bool TriggerAccess::neg_lep_matched_dR() { return m_negLep_match(); }

float TriggerAccess::prescale() { return m_prescale ? (*m_prescale)() : 1.; }

///
///
///

TrigSelectionBranch::TrigSelectionBranch(SagittaBiasTree* tree, const std::string& trig_name, float pt_threshold) :
    TrigSelectionBranch(tree, trig_name, pt_threshold, pt_threshold) {
    m_is_single_lepton = true;
}
TrigSelectionBranch::TrigSelectionBranch(SagittaBiasTree* tree, const std::string& trig_name, const float low_pt_threshold,
                                         const float high_pt_threshold) :
    DerivedVirtualBranch([this](SagittaBiasTree& t) -> bool { return is_accepted(t); }, tree),
    m_access{trig_name, *tree},
    m_low_pt{std::min(low_pt_threshold, high_pt_threshold)},
    m_high_pt{std::max(low_pt_threshold, high_pt_threshold)} {}
/// Return true whether the event is accepted by the trigger decision or not
bool TrigSelectionBranch::is_accepted(SagittaBiasTree& t) {
    const float pt_minus = t.negLep_pt() * MeVtoGeV;
    const float pt_plus = t.posLep_pt() * MeVtoGeV;

    if (m_is_single_lepton) {
        return (m_access.pos_lep_matched_dR() && pt_plus >= m_low_pt) || (m_access.neg_lep_matched_dR() && pt_minus >= m_low_pt);
    }

    const float pt_min = std::min(pt_minus, pt_plus);
    const float pt_max = std::max(pt_minus, pt_plus);
    return pt_min >= m_low_pt && pt_max >= m_high_pt && m_access.neg_lep_matched_dR() && m_access.pos_lep_matched_dR();
}

/// Name of the trigger
std::string TrigSelectionBranch::trigger() const { return m_access.trigger(); }

bool TrigSelectionBranch::is_single_lepton() const { return m_is_single_lepton; }

float TrigSelectionBranch::low_threshold() const { return m_low_pt; }
float TrigSelectionBranch::high_threshold() const { return m_high_pt; }
const ObjectID& TrigSelectionBranch::id() const { return m_obj_id; }
TriggerAccess& TrigSelectionBranch::access() { return m_access; }