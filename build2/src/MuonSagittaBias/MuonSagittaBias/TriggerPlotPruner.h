#ifndef MUONSAGITTABIAS__TriggerPlotPruner__H
#define MUONSAGITTABIAS__TriggerPlotPruner__H

#include "NtauHelpers/HistoBinConfig.h"
#include "NtauHelpers/Utils.h"

/// Helper class to get rid of the dead triggers in the histogram
/// and to sort them such that the most important triggers are on the left
/// hand side of the histo
class TriggerPlotPruner : public IPlotPopulator<TH1> {
public:
    enum class ToReturn {
        DecisionPlot,
        MatchPlot,

    };
    /// Standard constructor
    ///     trig_plot: Plot containing the trigger counts. Taken as reference
    ///     match_plot: Plot having the trigger accounts
    TriggerPlotPruner(Plot<TH1> trig_plot, Plot<TH1> match_plot, const PlotUtils::HistoBinConfig& trig_histo, enum ToReturn to_ret);

    std::shared_ptr<TH1> populate() override;
    std::shared_ptr<IPlotPopulator<TH1>> clonePopulator() const override;

private:
    Plot<TH1> m_trig_plot;
    Plot<TH1> m_match_plot;
    enum ToReturn m_to_ret;
    PlotUtils::HistoBinConfig m_trig_histo;
};
#endif