#include <MuonSagittaBias/ArgumentParser.h>
#include <MuonSagittaBias/PhaseSpaceCuts.h>
#include <MuonSagittaBias/PlotProvider.h>
#include <MuonSagittaBias/SagittaBiasAnalyzer.h>
#include <NtauHelpers/TriggerHelper.h>

#include "MuonSagittaBias/TriggerAccess.h"
#define DILEP_TRIGGER_ANA(ITEM, LOW, HIGH, PERIOD)                                                                                        \
    if (data_period == PERIOD) {                                                                                                          \
        PhaseSpaceCuts match_cuts{#ITEM + TriggerHelper::to_string(PERIOD), #ITEM, [](SagittaBiasAnalyzer& t, int) -> bool {              \
                                      TrigSelectionBranch trig_branch{&t, #ITEM, LOW, HIGH};                                              \
                                      return (t.period_year() & PERIOD) && trig_branch();                                                 \
                                  }};                                                                                                     \
        PhaseSpaceCuts pt_match_cuts{                                                                                                     \
            TriggerHelper::to_string(PERIOD) + "DiLepPt" + std::to_string(LOW) + "_" + std::to_string(HIGH), #ITEM,                       \
            [](SagittaBiasAnalyzer& t, int trk_type) -> bool {                                                                            \
                const float pt_plus{t.pt(trk_type, SagittaBiasAnalyzer::Plus)}, pt_minus{t.pt(trk_type, SagittaBiasAnalyzer::Minus)};     \
                return (t.period_year() & PERIOD) && std::min(pt_plus, pt_minus) > LOW && std::max(pt_plus, pt_minus) > HIGH;             \
            }};                                                                                                                           \
        PlotProvider match_prov{sample_to_process, fill, match_cuts};                                                                     \
        PlotProvider pt_prov{sample_to_process, fill, pt_match_cuts};                                                                     \
        Plot<TH1> match_pl = match_prov.make_plot(MuonTrk::TrackType::Primary, {2.7, 3.5});                                               \
        Plot<TH1> match_pl_ID = match_prov.make_plot(MuonTrk::TrackType::IDTrk, {2.7, 3.5});                                              \
        Plot<TH1> pt_pl = pt_prov.make_plot(MuonTrk::TrackType::Primary, {2.7, 3.5});                                                     \
        Plot<TH1> pt_pl_ID = pt_prov.make_plot(MuonTrk::TrackType::IDTrk, {2.7, 3.5});                                                    \
        match_pl.plotFormat().Color(kRed).LegendTitle("trigger matched - CB").MarkerStyle(kFullFourTrianglesPlus).ExtraDrawOpts("HIST");  \
        match_pl_ID.plotFormat().Color(kBlue).MarkerStyle(kFullFourTrianglesX).LegendTitle("trigger matched - ID").ExtraDrawOpts("HIST"); \
        pt_pl.plotFormat().Color(kCyan - 2).LegendTitle("p_{T} threshold - CB").MarkerStyle(kFullDiamond).ExtraDrawOpts("HIST");          \
        pt_pl_ID.plotFormat().Color(kGreen + 2).MarkerStyle(kFullDoubleDiamond).LegendTitle("p_{T} matched - ID").ExtraDrawOpts("HIST");  \
        plot_contents += PlotContent<TH1>{{all_pl, match_pl, all_pl_ID, match_pl_ID, pt_pl, pt_pl_ID},                                    \
                                          {RatioEntry(1, 0), RatioEntry(3, 2), RatioEntry(4, 0), RatioEntry(5, 2)},                       \
                                          {"Trigger selection study", match_cuts.title(), TriggerHelper::to_title(PERIOD)},               \
                                          TriggerHelper::to_string(PERIOD) + "TrigEval_" + #ITEM + "_" + fill.name(),                     \
                                          multi_page,                                                                                     \
                                          canvas_opts};                                                                                   \
    }

int main(int argc, char* argv[]) {
    /// Standard piece of code to setup the analysis...
    ArgumentParser parser;
    parser.summary_pdf("AllTriggerPlots").make_root_file(false);

    if (!parser.parse_args(argc, argv)) return EXIT_FAILURE;

    Sample<SagittaBiasAnalyzer> sample_to_process = parser.prepare_sample<SagittaBiasAnalyzer>();

    CanvasOptions canvas_opts = CanvasOptions()
                                    .RatioAxis(AxisConfig().Title("efficiency").ExtraTitleOffset(0.1))
                                    .YAxis(AxisConfig().Title("Entries").ExtraTitleOffset(0.4).TopPadding(0.7))
                                    // .XAxis(AxisConfig().ExtraTitleOffset(1.3))
                                    .LabelStatusTag("Simulation Internal")
                                    .OutputDir(parser.out_dir());

    std::shared_ptr<MultiPagePdfHandle> multi_page = PlotUtils::startMultiPagePdfFile(parser.summary_pdf(), canvas_opts);

    std::vector<PlotFiller> fillers;
    fillers += PlotFiller(
        [](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) { return h->GetXaxis()->FindBin(t.invariant_m(trk_type)); },
        PlotUtils::HistoBinConfig().name("InvariantMass").x_title(Form("m_{#mu#mu} [GeV]")).x_min(2.7).x_max(3.5).x_num_bins(32));
    fillers +=
        PlotFiller([](const TH1* h, SagittaBiasAnalyzer& t, int trk_type) { return h->GetXaxis()->FindBin(t.invariant_pt(trk_type)); },
                   PlotUtils::HistoBinConfig().name("DiLepPt").x_title(Form("p_{T} (#mu#mu) [GeV]")).x_min(5).x_max(55).x_num_bins(20));

    fillers +=
        PlotFiller([](const TH1* h, SagittaBiasAnalyzer& t,
                      int trk_type) { return h->GetXaxis()->FindBin(t.pt(trk_type, SagittaBiasAnalyzer::LepCharge::Plus)); },
                   PlotUtils::HistoBinConfig().name("PosLepPt").x_title(Form("p_{T} (#mu^{+}) [GeV]")).x_min(5).x_max(30).x_num_bins(250));
    fillers +=
        PlotFiller([](const TH1* h, SagittaBiasAnalyzer& t,
                      int trk_type) { return h->GetXaxis()->FindBin(t.pt(trk_type, SagittaBiasAnalyzer::LepCharge::Minus)); },
                   PlotUtils::HistoBinConfig().name("NegLepPt").x_title(Form("p_{T} (#mu^{-}) [GeV]")).x_min(5).x_max(30).x_num_bins(250));

    std::vector<PlotContent<TH1>> plot_contents;

    constexpr float trigmatch_thresh_mu4 = 4. * 1.05;
    constexpr float trigmatch_thresh_mu6 = 6. * 1.05;
    constexpr float trigmatch_thresh_mu8 = 8. * 1.05;
    constexpr float trigmatch_thresh_mu10 = 10. * 1.05;
    constexpr float trigmatch_thresh_mu11 = 11. * 1.05;
    constexpr float trigmatch_thresh_mu14 = 14. * 1.05;
    constexpr float trigmatch_thresh_mu20 = 20. * 1.05;
    constexpr float trigmatch_thresh_mu22 = 22. * 1.05;

    for (const int data_period :
         {TriggerHelper::year15, TriggerHelper::year16, TriggerHelper::year17, TriggerHelper::year18, TriggerHelper::years}) {
        PhaseSpaceCuts all_pairs("All" + TriggerHelper::to_string(data_period), "All pairs",
                                 [data_period](SagittaBiasAnalyzer& t, int) -> bool { return t.period_year() & data_period; });
        PhaseSpaceCuts trig_pairs(
            "Decision" + TriggerHelper::to_string(data_period), "Pass trigger scheme",
            [data_period](SagittaBiasAnalyzer& t, int) -> bool { return t.period_year() & data_period && t.trig_decision(); });
        for (const PlotFiller& fill : fillers) {
            PlotProvider all_prov{sample_to_process, fill, all_pairs};
            PlotProvider trig_prov{sample_to_process, fill, trig_pairs};

            Plot<TH1> all_pl = all_prov.make_plot(MuonTrk::TrackType::Primary, {2.7, 3.5});
            Plot<TH1> trig_pl = trig_prov.make_plot(MuonTrk::TrackType::Primary, {2.7, 3.5});

            Plot<TH1> all_pl_ID = all_prov.make_plot(MuonTrk::TrackType::IDTrk, {2.7, 3.5});
            Plot<TH1> trig_pl_ID = trig_prov.make_plot(MuonTrk::TrackType::IDTrk, {2.7, 3.5});

            all_pl.plotFormat().LegendTitle("All written events - CB").ExtraDrawOpts("HIST");
            trig_pl.plotFormat().Color(kRed).LegendTitle("trigger matched - CB").MarkerStyle(kFullFourTrianglesPlus).ExtraDrawOpts("HIST");

            all_pl_ID.plotFormat().Color(kOrange).MarkerStyle(kFullDiamond).LegendTitle("All written events - ID").ExtraDrawOpts("HIST");
            trig_pl_ID.plotFormat().Color(kBlue).MarkerStyle(kFullFourTrianglesX).LegendTitle("trigger matched - ID").ExtraDrawOpts("HIST");

            plot_contents += PlotContent<TH1>{{all_pl, trig_pl, all_pl_ID, trig_pl_ID},
                                              {RatioEntry(1, 0), RatioEntry(3, 2)},
                                              {"Trigger selection study", TriggerHelper::to_title(data_period)},
                                              TriggerHelper::to_string(data_period) + "TrigEval_" + fill.name(),
                                              multi_page,
                                              canvas_opts};

            DILEP_TRIGGER_ANA(HLT_mu6_mu4_bJpsimumu, trigmatch_thresh_mu4, trigmatch_thresh_mu6, TriggerHelper::year15);
            DILEP_TRIGGER_ANA(HLT_2mu4_bJpsimumu, trigmatch_thresh_mu4, trigmatch_thresh_mu4, TriggerHelper::year15);
            DILEP_TRIGGER_ANA(HLT_mu20_msonly_mu6noL1_msonly_nscan05, trigmatch_thresh_mu6, trigmatch_thresh_mu20, TriggerHelper::year15);
            DILEP_TRIGGER_ANA(HLT_2mu10, trigmatch_thresh_mu10, trigmatch_thresh_mu10, TriggerHelper::year15);
            DILEP_TRIGGER_ANA(HLT_mu4_mu4_idperf_bJpsimumu_noid, trigmatch_thresh_mu4, trigmatch_thresh_mu4, TriggerHelper::year15);
            DILEP_TRIGGER_ANA(HLT_2mu14, trigmatch_thresh_mu14, trigmatch_thresh_mu14, TriggerHelper::year15);

            DILEP_TRIGGER_ANA(HLT_mu10_mu6_bJpsimumu_delayed, trigmatch_thresh_mu6, trigmatch_thresh_mu10, TriggerHelper::year16);
            DILEP_TRIGGER_ANA(HLT_mu6_mu4_bJpsimumu_Lxy0_delayed, trigmatch_thresh_mu4, trigmatch_thresh_mu6, TriggerHelper::year16);
            DILEP_TRIGGER_ANA(HLT_mu6_mu4_bBmumuxv2_delayed, trigmatch_thresh_mu4, trigmatch_thresh_mu6, TriggerHelper::year16);
            DILEP_TRIGGER_ANA(HLT_mu10_mu6_bBmumuxv2_delayed, trigmatch_thresh_mu6, trigmatch_thresh_mu10, TriggerHelper::year16);
            DILEP_TRIGGER_ANA(HLT_mu6_mu4_bJpsimumu_L1BPH_dash_2M8_dash_MU6MU4_BPH_dash_0DR15_dash_MU6MU4, trigmatch_thresh_mu4,
                              trigmatch_thresh_mu6, TriggerHelper::year16);
            DILEP_TRIGGER_ANA(HLT_mu6_mu4_bJpsimumu_delayed, trigmatch_thresh_mu4, trigmatch_thresh_mu6, TriggerHelper::year16);
            DILEP_TRIGGER_ANA(HLT_mu6_mu4_bJpsimumu_Lxy0_delayed_L1BPH_dash_2M8_dash_MU6MU4_BPH_dash_0DR15_dash_MU6MU4,
                              trigmatch_thresh_mu4, trigmatch_thresh_mu6, TriggerHelper::year16);
            DILEP_TRIGGER_ANA(HLT_2mu6_bJpsimumu_L1BPH_dash_2M9_dash_2MU6_BPH_dash_2DR15_dash_2MU6, trigmatch_thresh_mu6,
                              trigmatch_thresh_mu6, TriggerHelper::year16);
            DILEP_TRIGGER_ANA(HLT_mu6_mu4_bJpsimumu, trigmatch_thresh_mu4, trigmatch_thresh_mu6, TriggerHelper::year16);
            DILEP_TRIGGER_ANA(HLT_mu10_mu6_bJpsimumu, trigmatch_thresh_mu6, trigmatch_thresh_mu10, TriggerHelper::year16);
            DILEP_TRIGGER_ANA(HLT_mu20_nomucomb_mu6noL1_nscan03, trigmatch_thresh_mu6, trigmatch_thresh_mu20, TriggerHelper::year16);
            DILEP_TRIGGER_ANA(HLT_mu22_mu8noL1, trigmatch_thresh_mu8, trigmatch_thresh_mu22, TriggerHelper::year16);

            DILEP_TRIGGER_ANA(HLT_mu6_mu4_bJpsimumu_delayed_L1BPH_dash_2M8_dash_MU6MU4_BPH_dash_0DR15_dash_MU6MU4, trigmatch_thresh_mu4,
                              trigmatch_thresh_mu6, TriggerHelper::year16);
            DILEP_TRIGGER_ANA(HLT_mu6_mu4_bBmumux_BsmumuPhi_delayed_L1BPH_dash_2M8_dash_MU6MU4_BPH_dash_0DR15_dash_MU6MU4,
                              trigmatch_thresh_mu4, trigmatch_thresh_mu6, TriggerHelper::year16);
            DILEP_TRIGGER_ANA(HLT_2mu14, trigmatch_thresh_mu14, trigmatch_thresh_mu14, TriggerHelper::year16);
            DILEP_TRIGGER_ANA(HLT_mu4_mu4_idperf_bJpsimumu_noid, trigmatch_thresh_mu4, trigmatch_thresh_mu4, TriggerHelper::year16);

            DILEP_TRIGGER_ANA(HLT_2mu6_bJpsimumu_L1BPH_dash_2M9_dash_2MU6_BPH_dash_2DR15_dash_2MU6, trigmatch_thresh_mu6,
                              trigmatch_thresh_mu6, TriggerHelper::year17);
            DILEP_TRIGGER_ANA(HLT_2mu6_bJpsimumu_Lxy0_L1BPH_dash_2M9_dash_2MU6_BPH_dash_2DR15_dash_2MU6, trigmatch_thresh_mu6,
                              trigmatch_thresh_mu6, TriggerHelper::year17);
            DILEP_TRIGGER_ANA(HLT_mu11_mu6_bDimu, trigmatch_thresh_mu6, trigmatch_thresh_mu11, TriggerHelper::year17);
            DILEP_TRIGGER_ANA(HLT_mu11_mu6_bJpsimumu, trigmatch_thresh_mu6, trigmatch_thresh_mu11, TriggerHelper::year17);
            DILEP_TRIGGER_ANA(HLT_mu11_mu6_bDimu_Lxy0, trigmatch_thresh_mu6, trigmatch_thresh_mu11, TriggerHelper::year17);
            DILEP_TRIGGER_ANA(HLT_mu11_mu6_bJpsimumu_Lxy0, trigmatch_thresh_mu6, trigmatch_thresh_mu11, TriggerHelper::year17);
            DILEP_TRIGGER_ANA(HLT_2mu6_bBmumux_BpmumuKp_L1BPH_dash_2M9_dash_2MU6_BPH_dash_2DR15_dash_2MU6, trigmatch_thresh_mu6,
                              trigmatch_thresh_mu6, TriggerHelper::year17);
            DILEP_TRIGGER_ANA(HLT_mu11_mu6_bBmumuxv2, trigmatch_thresh_mu6, trigmatch_thresh_mu11, TriggerHelper::year17);
            DILEP_TRIGGER_ANA(HLT_mu11_mu6_bBmumux_BpmumuKp, trigmatch_thresh_mu6, trigmatch_thresh_mu11, TriggerHelper::year17);

            DILEP_TRIGGER_ANA(HLT_mu22_mu8noL1, trigmatch_thresh_mu8, trigmatch_thresh_mu22, TriggerHelper::year17);
            DILEP_TRIGGER_ANA(HLT_mu4_mu4_idperf_bJpsimumu_noid, trigmatch_thresh_mu4, trigmatch_thresh_mu4, TriggerHelper::year17);
            DILEP_TRIGGER_ANA(HLT_mu6_mu4_bBmumux_BsmumuPhi_L1BPH_dash_2M9_dash_MU6MU4_BPH_dash_0DR15_dash_MU6MU4, trigmatch_thresh_mu4,
                              trigmatch_thresh_mu6, TriggerHelper::year17);
            DILEP_TRIGGER_ANA(HLT_2mu14, trigmatch_thresh_mu14, trigmatch_thresh_mu14, TriggerHelper::year17);

            DILEP_TRIGGER_ANA(HLT_2mu6_bJpsimumu_L1BPH_dash_2M9_dash_2MU6_BPH_dash_2DR15_dash_2MU6, trigmatch_thresh_mu6,
                              trigmatch_thresh_mu6, TriggerHelper::year18);
            DILEP_TRIGGER_ANA(HLT_2mu6_bJpsimumu_Lxy0_L1BPH_dash_2M9_dash_2MU6_BPH_dash_2DR15_dash_2MU6, trigmatch_thresh_mu6,
                              trigmatch_thresh_mu6, TriggerHelper::year18);
            DILEP_TRIGGER_ANA(HLT_mu6_mu4_bJpsimumu_Lxy0_L1BPH_dash_2M9_dash_MU6MU4_BPH_dash_0DR15_dash_MU6MU4, trigmatch_thresh_mu4,
                              trigmatch_thresh_mu6, TriggerHelper::year18);
            DILEP_TRIGGER_ANA(HLT_mu11_mu6_bDimu, trigmatch_thresh_mu6, trigmatch_thresh_mu11, TriggerHelper::year18);
            DILEP_TRIGGER_ANA(HLT_mu11_mu6_bDimu_L1LFV_dash_MU11, trigmatch_thresh_mu6, trigmatch_thresh_mu11, TriggerHelper::year18);
            DILEP_TRIGGER_ANA(HLT_mu11_mu6_bJpsimumu, trigmatch_thresh_mu6, trigmatch_thresh_mu11, TriggerHelper::year18);
            DILEP_TRIGGER_ANA(HLT_mu11_mu6_bJpsimumu_L1LFV_dash_MU11, trigmatch_thresh_mu6, trigmatch_thresh_mu11, TriggerHelper::year18);
            DILEP_TRIGGER_ANA(HLT_2mu6_bBmumux_BpmumuKp_L1BPH_dash_2M9_dash_2MU6_BPH_dash_2DR15_dash_2MU6, trigmatch_thresh_mu6,
                              trigmatch_thresh_mu6, TriggerHelper::year18);
            DILEP_TRIGGER_ANA(HLT_mu11_mu6_bDimu_Lxy0, trigmatch_thresh_mu6, trigmatch_thresh_mu11, TriggerHelper::year18);
            DILEP_TRIGGER_ANA(HLT_mu11_mu6_bJpsimumu_Lxy0, trigmatch_thresh_mu6, trigmatch_thresh_mu11, TriggerHelper::year18);
            DILEP_TRIGGER_ANA(HLT_mu11_mu6_bDimu_Lxy0_L1LFV_dash_MU11, trigmatch_thresh_mu6, trigmatch_thresh_mu11, TriggerHelper::year18);
            DILEP_TRIGGER_ANA(HLT_mu11_mu6_bJpsimumu_Lxy0_L1LFV_dash_MU11, trigmatch_thresh_mu6, trigmatch_thresh_mu11,
                              TriggerHelper::year18);
            DILEP_TRIGGER_ANA(HLT_mu11_mu6_bBmumuxv2, trigmatch_thresh_mu6, trigmatch_thresh_mu11, TriggerHelper::year18);
            DILEP_TRIGGER_ANA(HLT_mu11_mu6_bBmumuxv2_L1LFV_dash_MU11, trigmatch_thresh_mu6, trigmatch_thresh_mu11, TriggerHelper::year18);
            DILEP_TRIGGER_ANA(HLT_mu11_mu6_bBmumux_BpmumuKp, trigmatch_thresh_mu6, trigmatch_thresh_mu11, TriggerHelper::year18);
            DILEP_TRIGGER_ANA(HLT_mu11_mu6_bBmumux_BpmumuKp_L1LFV_dash_MU11, trigmatch_thresh_mu6, trigmatch_thresh_mu11,
                              TriggerHelper::year18);
            DILEP_TRIGGER_ANA(HLT_mu6_mu4_bJpsimumu_L1BPH_dash_2M9_dash_MU6MU4_BPH_dash_0DR15_dash_MU6MU4, trigmatch_thresh_mu4,
                              trigmatch_thresh_mu6, TriggerHelper::year18);
            DILEP_TRIGGER_ANA(HLT_mu22_mu8noL1, trigmatch_thresh_mu8, trigmatch_thresh_mu22, TriggerHelper::year18);
            DILEP_TRIGGER_ANA(HLT_2mu14, trigmatch_thresh_mu14, trigmatch_thresh_mu4, TriggerHelper::year18);
            DILEP_TRIGGER_ANA(HLT_mu6_mu4_bBmumux_BsmumuPhi_L1BPH_dash_2M9_dash_MU6MU4_BPH_dash_0DR15_dash_MU6MU4, trigmatch_thresh_mu4,
                              trigmatch_thresh_mu6, TriggerHelper::year18);

            ///   SINGLE_TRIGGER(HLT_mu20_iloose_L1MU15, trigmatch_thresh_mu20, TriggerHelper::year15);
            ///   SINGLE_TRIGGER(HLT_mu4_bJpsi_Trkloose, trigmatch_thresh_mu4, TriggerHelper::year15);
        }
    }

    for (auto& pc : plot_contents) {
        if (count_non_empty(pc)) DefaultPlotting::draw1D(pc);
    }

    return EXIT_SUCCESS;
}