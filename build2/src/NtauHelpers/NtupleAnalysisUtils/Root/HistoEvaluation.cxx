#include "NtupleAnalysisUtils/Helpers/HistoEvaluation.h"
#include "NtupleAnalysisUtils/Helpers/HistoManipulation.h" 
#include "NtupleAnalysisUtils/Plot/Plot.h"
#include "NtupleAnalysisUtils/Plot/BasicPopulators.h"

using namespace PlotUtils; 


std::string PlotUtils::getYtitle(const Plot<TEfficiency> plot, const CanvasOptions & opts){
    return getYtitle(Plot<TH1D>(CopyExisting(dynamic_cast<const TH1D*>(plot->GetPassedHistogram()))), opts); 
}
std::string PlotUtils::getXtitle(const Plot<TEfficiency> plot, const CanvasOptions & opts){
    return getXtitle(Plot<TH1D>(CopyExisting(dynamic_cast<const TH1D*>(plot->GetPassedHistogram()))), opts); 
}

PlotUtils::rawAxisRange PlotUtils::getRawRange (const TH1* p, size_t axis,const AxisConfig & axisCfg){
    size_t dimensionality = p->GetDimension(); 
    double thMin = axisCfg.Min();
    double thMax = axisCfg.Max();
    if (axis < dimensionality){
        return rawAxisRange  {std::max(thMin, getAxis(p,axis)->GetXmin()), 
                              std::min(thMax, getAxis(p,axis)->GetXmax()),
                              true
        }; 
    }
    else{
        /// appropriate thresholds for log axes
        if (axisCfg.Log()){
            if (thMin < 0) thMin = 0; 
            if (thMax < 0) thMax = 0; 
        }
        double Max = p->GetMaximum(thMax); 
        double Min = p->GetMinimum(thMin); 
        return rawAxisRange{Min,Max,false}; 
    }
}

PlotUtils::rawAxisRange PlotUtils::getRawRange (const TGraph* p, size_t axis,const AxisConfig & axisCfg){
    std::vector<double> elements;
    Double_t* cont = (axis == 0 ? p->GetX() : p->GetY()); 
    elements.insert(elements.begin(), cont, cont + p->GetN()); 
    double thMin = axisCfg.Min();
    double thMax = axisCfg.Max();
    if (axisCfg.Log()){
        if (thMin < 0) thMin = 0; 
        if (thMax < 0) thMax = 0; 
    }
    std::sort(elements.begin(),elements.end()); 
    auto lower = std::upper_bound(elements.begin(),elements.end(),thMin);
    auto upper = std::lower_bound(elements.begin(),elements.end(),thMax);
    if (lower == elements.end()) --lower; 
    if (upper == elements.end()) --upper; 
    return rawAxisRange{*lower, *upper,false}; 
}

PlotUtils::rawAxisRange PlotUtils::getRawRange (const TEfficiency* p, size_t axis,const AxisConfig & axisCfg){
    return getRawRange(
        PlotUtils::extractEfficiency(p).get(), 
        axis, 
        axisCfg
    );
}

std::pair<double,double> PlotUtils::updateAxisRange(PlotUtils::rawAxisRange histo_range, const AxisConfig & axisConf){
    double x1 = histo_range.min;
    double x2 = histo_range.max;
    
    /// if the range was obtained from a histo binning axis, we do not apply any further modifiers 
    if (histo_range.fromBinning){
        return std::make_pair(x1,x2);
    }
    double xSymm = axisConf.SymmetrisationPoint();

    /// now, start checking modifiers
    if (axisConf.Log()){
        x1 = std::log(x1); 
        x2 = std::log(x2);
        xSymm = std::log(xSymm); 
    }
    /// first, apply possible symmetrisation 
    if (axisConf.Symmetric()){
        xSymm = axisConf.SymmetrisationPoint();
        double dx1 = x1 - xSymm;
        double dx2 = x2 - xSymm;
        double dx = std::max(std::abs(dx1),std::abs(dx2)); 
        x1 = xSymm - dx;
        x2 = xSymm + dx; 
    }
    /// now, we add a possibly padding on top and below
    double dynRange = x2 - x1; 
    x1 -= axisConf.BottomPadding() * dynRange; 
    x2 += axisConf.TopPadding() * dynRange; 

    /// having applied all modifiers, we can go back to linear values 
    if (axisConf.Log()){
        x1 = std::exp(x1); 
        x2 = std::exp(x2); 
    }

    /// TODO: Implement snapping to pre-chosen values via "Fixed" option!

    /// overrride by user-specified settings if so requested
    if (axisConf.Fixed() && axisConf.Min.isUserSet()) x1 = axisConf.Min();  
    if (axisConf.Fixed() && axisConf.Max.isUserSet()) x2 = axisConf.Max();  
    return std::make_pair(x1,x2);

}

